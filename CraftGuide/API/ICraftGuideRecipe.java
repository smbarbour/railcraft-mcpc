package CraftGuide.API;

import net.minecraft.server.ItemStack;

public interface ICraftGuideRecipe
{
    ItemStack getItem(int var1);

    ICraftGuideRecipe.ItemSlot getSlotUnderMouse(int var1, int var2);

    boolean containsItem(ItemStack var1);

    boolean containsItem(ItemStack var1, boolean var2);

    ItemStack[] getItems();

    public static class ItemSlot
    {
        public int x;
        public int y;
        public int width;
        public int height;
        public int index;
        public boolean drawQuantity;

        public ItemSlot(int var1, int var2, int var3, int var4, int var5)
        {
            this(var1, var2, var3, var4, var5, false);
        }

        public ItemSlot(int var1, int var2, int var3, int var4, int var5, boolean var6)
        {
            this.x = var1;
            this.y = var2;
            this.width = var3;
            this.height = var4;
            this.index = var5;
            this.drawQuantity = var6;
        }
    }
}
