package CraftGuide.API;

import net.minecraft.server.ItemStack;

public class ExtraSlot extends ICraftGuideRecipe.ItemSlot
{
    public ItemStack displayed;

    public ExtraSlot(int var1, int var2, int var3, int var4, int var5, ItemStack var6)
    {
        super(var1, var2, var3, var4, var5);
        this.displayed = var6;
    }
}
