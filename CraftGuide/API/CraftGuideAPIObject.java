package CraftGuide.API;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CraftGuideAPIObject
{
    public CraftGuideAPIObject()
    {
        try
        {
            Class var1 = Class.forName("net.minecraft.server.CraftGuide.ReflectionAPI");
            Method var2 = var1.getMethod("registerAPIObject", new Class[] {Object.class});
            var2.invoke((Object)null, new Object[] {this});
        }
        catch (ClassNotFoundException var3)
        {
            ;
        }
        catch (NoSuchMethodException var4)
        {
            ;
        }
        catch (SecurityException var5)
        {
            ;
        }
        catch (InvocationTargetException var6)
        {
            ;
        }
        catch (IllegalAccessException var7)
        {
            ;
        }
        catch (IllegalArgumentException var8)
        {
            ;
        }
    }
}
