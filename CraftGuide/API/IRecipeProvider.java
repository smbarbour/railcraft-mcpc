package CraftGuide.API;

public interface IRecipeProvider
{
    void generateRecipes(IRecipeGenerator var1);
}
