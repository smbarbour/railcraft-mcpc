package CraftGuide.API;

import net.minecraft.server.ItemStack;

public interface IRecipeGenerator
{
    IRecipeTemplate createRecipeTemplate(ICraftGuideRecipe.ItemSlot[] var1, String var2, int var3, int var4, int var5, int var6);

    IRecipeTemplate createRecipeTemplate(ICraftGuideRecipe.ItemSlot[] var1, String var2, int var3, int var4, String var5, int var6, int var7);

    IRecipeTemplate createRecipeTemplate(ICraftGuideRecipe.ItemSlot[] var1, ItemStack var2, String var3, int var4, int var5, int var6, int var7);

    IRecipeTemplate createRecipeTemplate(ICraftGuideRecipe.ItemSlot[] var1, ItemStack var2, String var3, int var4, int var5, String var6, int var7, int var8);

    void addRecipe(IRecipeTemplate var1, ItemStack[] var2);
}
