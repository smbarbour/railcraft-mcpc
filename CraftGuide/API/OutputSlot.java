package CraftGuide.API;

public class OutputSlot extends ICraftGuideRecipe.ItemSlot
{
    public OutputSlot(int var1, int var2, int var3, int var4, int var5, boolean var6)
    {
        super(var1, var2, var3, var4, var5, var6);
    }

    public OutputSlot(int var1, int var2, int var3, int var4, int var5)
    {
        super(var1, var2, var3, var4, var5);
    }
}
