package net.minecraft.server;

import net.minecraft.server.MinecraftServer;
import java.util.logging.Level;
import forge.NetworkMod;
import railcraft.common.ChunkManager;
import railcraft.common.RailcraftConstants;
import railcraft.common.RailcraftInstallationException;
import railcraft.common.RailcraftSaveData;
import railcraft.common.api.EnumSignalAspect;
import railcraft.common.modules.ModuleFactory;
import railcraft.common.modules.ModuleIC2;
import railcraft.common.modules.RailcraftModuleManager;

public final class mod_Railcraft extends NetworkMod
{

    public static final String VERSION = "5.4.7";
    public static mod_Railcraft instance;
    private int update = 0;

    public static String getRailcraftVersion()
    {
        return VERSION;
    }

    public mod_Railcraft()
    {
        instance = this;
//        MinecraftForge.versionDetect(getNameAndVersion(), 1, 4, 0);
        if(ModLoader.isModLoaded("mod_Railcraft")) {
            ModLoader.getLogger().log(Level.SEVERE, "{0}: Railcraft is already loaded, please check your mod folder for old versions.", getNameAndVersion());
            throw new RailcraftInstallationException(getNameAndVersion() + ": Mulitple versions found!"
                + "\n************************************************************************************"
                + "\n You failed to read the installation instructions."
                + "\n Railcraft is already loaded, please check your mod folder for old versions."
                + "\n************************************************************************************"
                + "\n");
        }
    }

    @Override
    public void load()
    {
        RailcraftModuleManager.load();

        ModLoader.setInGameHook(this, true, true);
    }

    @Override
    public void modsLoaded()
    {
        RailcraftModuleManager.modsLoaded();
    }

    @Override
    public String getPriorities()
    {
        return RailcraftConstants.LOAD_ORDER;
    }

    @Override
    public String getVersion()
    {
        return getRailcraftVersion();
    }

    public String getNameAndVersion()
    {
        return getName() + " " + getVersion();
    }

    @Override
    public int addFuel(int i, int meta)
    {
        return ModuleFactory.getFuelValues(i);
    }

    @Override
    public boolean onTickInGame(MinecraftServer minecraft)
    {
        ModuleIC2.integrateWithIC2();

        ChunkManager.getInstance().updateTick();

        update++;
        if(update % 10 == 0) {
            EnumSignalAspect.invertBlinkState();
        }
        if(update % 10 == 0) {
            RailcraftSaveData.saveData();
        }
        return true;
    }

    @Override
    public boolean clientSideRequired()
    {
        return true;
    }

    @Override
    public boolean serverSideRequired()
    {
        return false;
    }
}