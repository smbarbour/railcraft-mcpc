package forestry.api.liquids;

import net.minecraft.server.Block;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.NBTTagCompound;

/**
 * 
 * Forestry internal ItemStack substitute for liquids
 * 
 * @author SirSengir
 */
public class LiquidStack {
	public int itemID;
	public int liquidAmount;
	public int itemMeta;

	public NBTTagCompound stackTagCompound;

	private LiquidStack() {
	}

	public LiquidStack(int itemID, int liquidAmount) {
		this(itemID, liquidAmount, 0);
	}

	public LiquidStack(Item item, int liquidAmount) {
		this(item.id, liquidAmount, 0);
	}

	public LiquidStack(Block block, int liquidAmount) {
		this(block.id, liquidAmount, 0);
	}

	public LiquidStack(int itemID, int liquidAmount, int itemDamage) {
		this.itemID = itemID;
		this.liquidAmount = liquidAmount;
		this.itemMeta = itemDamage;
	}

	public NBTTagCompound writeToNBT(NBTTagCompound nbttagcompound) {
		nbttagcompound.setShort("Id", (short) itemID);
		nbttagcompound.setInt("Amount", liquidAmount);
		nbttagcompound.setShort("Meta", (short) itemMeta);
		if (stackTagCompound != null)
			nbttagcompound.set("Tag", stackTagCompound);
		return nbttagcompound;
	}

	public void readFromNBT(NBTTagCompound nbttagcompound) {
		itemID = nbttagcompound.getShort("Id");
		liquidAmount = nbttagcompound.getInt("Amount");
		itemMeta = nbttagcompound.getShort("Meta");
		if (nbttagcompound.hasKey("Tag"))
			stackTagCompound = nbttagcompound.getCompound("tag");
	}

	public LiquidStack copy() {
		LiquidStack copy = new LiquidStack(itemID, liquidAmount, itemMeta);
		if (stackTagCompound != null) {
			copy.stackTagCompound = (NBTTagCompound) stackTagCompound.clone();
			if (!copy.stackTagCompound.equals(stackTagCompound))
				return copy;
		}
		return copy;
	}

	public NBTTagCompound getTagCompound() {
		return stackTagCompound;
	}

	public void setTagCompound(NBTTagCompound nbttagcompound) {
		stackTagCompound = nbttagcompound;
	}

	public boolean isLiquidEqual(LiquidStack other) {
		return itemID == other.itemID && itemMeta == other.itemMeta;
	}

	public boolean isLiquidEqual(ItemStack other) {
		return itemID == other.id && itemMeta == other.getData();
	}

	/**
	 * @return An ItemStack representation of this LiquidStack
	 */
	public ItemStack asItemStack() {
		return new ItemStack(itemID, 1, itemMeta);
	}

	/**
	 * Reads a liquid stack from the passed nbttagcompound and returns it.
	 * 
	 * @param nbttagcompound
	 * @return
	 */
	public static LiquidStack loadLiquidStackFromNBT(NBTTagCompound nbttagcompound) {
		LiquidStack liquidstack = new LiquidStack();
		liquidstack.readFromNBT(nbttagcompound);
		return liquidstack.itemID == 0 ? null : liquidstack;
	}

}
