package ic2.api;

import net.minecraft.server.ItemStack;

public final class Items
{
    public static ItemStack getItem(String var0)
    {
        try
        {
            Object var1 = Class.forName(getPackage() + ".common.Ic2Items").getField(var0).get((Object)null);
            return var1 instanceof ItemStack ? (ItemStack)var1 : null;
        }
        catch (Exception var2)
        {
            System.out.println("IC2 API: Call getItem failed for " + var0);
            return null;
        }
    }

    private static String getPackage()
    {
        Package var0 = Items.class.getPackage();
        return var0 != null ? var0.getName().substring(0, var0.getName().lastIndexOf(46)) : "ic2";
    }
}
