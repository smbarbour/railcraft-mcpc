package ic2.api;

import net.minecraft.server.TileEntity;
import net.minecraft.server.World;

public final class EnergyNet
{
    Object energyNetInstance;

    public static EnergyNet getForWorld(World var0)
    {
        try
        {
            return new EnergyNet(Class.forName(getPackage() + ".common.EnergyNet").getMethod("getForWorld", new Class[] {World.class}).invoke((Object)null, new Object[] {var0}));
        }
        catch (Exception var2)
        {
            throw new RuntimeException(var2);
        }
    }

    private EnergyNet(Object var1)
    {
        this.energyNetInstance = var1;
    }

    public void addTileEntity(TileEntity var1)
    {
        try
        {
            Class.forName(getPackage() + ".common.EnergyNet").getMethod("addTileEntity", new Class[] {TileEntity.class}).invoke(this.energyNetInstance, new Object[] {var1});
        }
        catch (Exception var3)
        {
            throw new RuntimeException(var3);
        }
    }

    public void removeTileEntity(TileEntity var1)
    {
        try
        {
            Class.forName(getPackage() + ".common.EnergyNet").getMethod("removeTileEntity", new Class[] {TileEntity.class}).invoke(this.energyNetInstance, new Object[] {var1});
        }
        catch (Exception var3)
        {
            throw new RuntimeException(var3);
        }
    }

    public int emitEnergyFrom(IEnergySource var1, int var2)
    {
        try
        {
            return ((Integer)Class.forName(getPackage() + ".common.EnergyNet").getMethod("emitEnergyFrom", new Class[] {IEnergySource.class, Integer.TYPE}).invoke(this.energyNetInstance, new Object[] {var1, Integer.valueOf(var2)})).intValue();
        }
        catch (Exception var4)
        {
            throw new RuntimeException(var4);
        }
    }

    public long getTotalEnergyConducted(TileEntity var1)
    {
        try
        {
            return ((Long)Class.forName(getPackage() + ".common.EnergyNet").getMethod("getTotalEnergyConducted", new Class[] {TileEntity.class}).invoke(this.energyNetInstance, new Object[] {var1})).longValue();
        }
        catch (Exception var3)
        {
            throw new RuntimeException(var3);
        }
    }

    private static String getPackage()
    {
        Package var0 = EnergyNet.class.getPackage();
        return var0 != null ? var0.getName().substring(0, var0.getName().lastIndexOf(46)) : "ic2";
    }
}
