package ic2.api;

import net.minecraft.server.TileEntity;

public enum Direction
{
    XN(0),
    XP(1),
    YN(2),
    YP(3),
    ZN(4),
    ZP(5);
    private int dir;

    private Direction(int var3)
    {
        this.dir = var3;
    }

    public TileEntity applyToTileEntity(TileEntity var1)
    {
        int[] var2 = new int[] {var1.x, var1.y, var1.z};
        var2[this.dir / 2] += this.getSign();
        return var1.world != null && var1.world.isLoaded(var2[0], var2[1], var2[2]) ? var1.world.getTileEntity(var2[0], var2[1], var2[2]) : null;
    }

    public Direction getInverse()
    {
        int var1 = this.dir - this.getSign();
        Direction[] var2 = values();
        int var3 = var2.length;

        for (int var4 = 0; var4 < var3; ++var4)
        {
            Direction var5 = var2[var4];

            if (var5.dir == var1)
            {
                return var5;
            }
        }

        return this;
    }

    public int toSideValue()
    {
        return (this.dir + 4) % 6;
    }

    private int getSign()
    {
        return this.dir % 2 * 2 - 1;
    }
}
