package ic2.api;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.ItemStack;

public final class ElectricItem
{
    public static int charge(ItemStack var0, int var1, int var2, boolean var3, boolean var4)
    {
        try
        {
            return ((Integer)Class.forName(getPackage() + ".common.ElectricItem").getMethod("charge", new Class[] {ItemStack.class, Integer.TYPE, Integer.TYPE, Boolean.TYPE, Boolean.TYPE}).invoke((Object)null, new Object[] {var0, Integer.valueOf(var1), Integer.valueOf(var2), Boolean.valueOf(var3), Boolean.valueOf(var4)})).intValue();
        }
        catch (Exception var6)
        {
            throw new RuntimeException(var6);
        }
    }

    public static int discharge(ItemStack var0, int var1, int var2, boolean var3, boolean var4)
    {
        try
        {
            return ((Integer)Class.forName(getPackage() + ".common.ElectricItem").getMethod("discharge", new Class[] {ItemStack.class, Integer.TYPE, Integer.TYPE, Boolean.TYPE, Boolean.TYPE}).invoke((Object)null, new Object[] {var0, Integer.valueOf(var1), Integer.valueOf(var2), Boolean.valueOf(var3), Boolean.valueOf(var4)})).intValue();
        }
        catch (Exception var6)
        {
            throw new RuntimeException(var6);
        }
    }

    public static boolean canUse(ItemStack var0, int var1)
    {
        try
        {
            return ((Boolean)Class.forName(getPackage() + ".common.ElectricItem").getMethod("canUse", new Class[] {ItemStack.class, Integer.TYPE}).invoke((Object)null, new Object[] {var0, Integer.valueOf(var1)})).booleanValue();
        }
        catch (Exception var3)
        {
            throw new RuntimeException(var3);
        }
    }

    public static boolean use(ItemStack var0, int var1, EntityHuman var2)
    {
        try
        {
            return ((Boolean)Class.forName(getPackage() + ".common.ElectricItem").getMethod("use", new Class[] {ItemStack.class, Integer.TYPE, EntityHuman.class}).invoke((Object)null, new Object[] {var0, Integer.valueOf(var1), var2})).booleanValue();
        }
        catch (Exception var4)
        {
            throw new RuntimeException(var4);
        }
    }

    public static void chargeFromArmor(ItemStack var0, EntityHuman var1)
    {
        try
        {
            Class.forName(getPackage() + ".common.ElectricItem").getMethod("chargeFromArmor", new Class[] {ItemStack.class, EntityHuman.class}).invoke((Object)null, new Object[] {var0, var1});
        }
        catch (Exception var3)
        {
            throw new RuntimeException(var3);
        }
    }

    private static String getPackage()
    {
        Package var0 = ElectricItem.class.getPackage();
        return var0 != null ? var0.getName().substring(0, var0.getName().lastIndexOf(46)) : "ic2";
    }
}
