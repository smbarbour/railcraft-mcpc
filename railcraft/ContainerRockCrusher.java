package railcraft;

import net.minecraft.server.*;
import railcraft.common.util.slots.SlotOutput;
import railcraft.common.util.slots.SlotCokeOven;
import railcraft.common.util.slots.SlotRockCrusher;
import railcraft.common.utility.TileRockCrusher;

public class ContainerRockCrusher extends Container
{

    private TileRockCrusher crusher;
    private int lastCookTime;

    public ContainerRockCrusher(PlayerInventory inventoryplayer, TileRockCrusher crusher)
    {
    	setPlayer(inventoryplayer.player);
        lastCookTime = 0;
        this.crusher = crusher;
        for(int i = 0; i < 3; i++) {
            for(int k = 0; k < 3; k++) {
                a(new SlotRockCrusher(crusher, i * 3 + k, 17 + k * 18, 21 + i * 18));
            }
        }
        for(int i = 0; i < 3; i++) {
            for(int k = 0; k < 3; k++) {
                a(new SlotOutput(crusher, 9 + i * 3 + k, 107 + k * 18, 21 + i * 18));
            }
        }
        for(int i = 0; i < 3; i++) {
            for(int k = 0; k < 9; k++) {
                a(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++) {
            a(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }

    }

    @Override
    public void a()
    {
        super.a();
        for(int i = 0; i < listeners.size(); i++) {
            ICrafting icrafting = (ICrafting)listeners.get(i);
            if(lastCookTime != crusher.getProcessTime()) {
                icrafting.setContainerData(this, 0, crusher.getProcessTime());
            }
        }

        lastCookTime = crusher.getProcessTime();
    }

//    @Override
//    public void onCraftGuiOpened(ICrafting icrafting)
//    {
//        super.onCraftGuiOpened(icrafting);
//        icrafting.updateCraftingInventoryInfo(this, 0, oven.getProcessTime());
//    }
    public void updateProgressBar(int i, int j)
    {
        if(i == 0) {
            crusher.setProcessTime(j);
        }
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        return crusher.a(entityplayer);
    }

    @Override
    public ItemStack a(int i)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)e.get(i);
        if(slot != null && slot.c()) {
            ItemStack itemstack1 = slot.getItem();
            itemstack = itemstack1.cloneItemStack();
            if(i >= 9 && i < 54 && SlotRockCrusher.canPlaceItem(itemstack)) {
                if(!a(itemstack1, 0, 9, false)) {
                    return null;
                }
            } else if(i >= 18 && i < 45) {
                if(!a(itemstack1, 45, 54, false)) {
                    return null;
                }
            } else if(i >= 45 && i < 54) {
                if(!a(itemstack1, 18, 45, false)) {
                    return null;
                }
            } else if(!a(itemstack1, 18, 54, false)) {
                return null;
            }
            if(itemstack1.count == 0) {
                slot.set(null);
            } else {
                slot.d();
            }
            if(itemstack1.count != itemstack.count) {
                slot.c(itemstack1);
            } else {
                return null;
            }
        }
        return itemstack;
    }
    
    public IInventory getInventory() {
    	return crusher;
    }
    
}
