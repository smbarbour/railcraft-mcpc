package railcraft;

import net.minecraft.server.*;
import railcraft.common.util.slots.SlotOutput;
import railcraft.common.util.slots.SlotCokeOven;
import railcraft.common.util.slots.SlotLiquidContainerEmpty;
import railcraft.common.utility.TileCokeOven;

public class ContainerCokeOven extends Container
{

    private TileCokeOven tile;
    private int lastCookTime;

    public ContainerCokeOven(PlayerInventory inventoryplayer, TileCokeOven tile)
    {
    	this.setPlayer(inventoryplayer.player);
        lastCookTime = 0;
        this.tile = tile;
        a(new SlotCokeOven(tile, 0, 16, 43));
        a(new SlotOutput(tile, 1, 62, 43));
        a(new SlotOutput(tile, 2, 149, 57));
        a(new SlotLiquidContainerEmpty(tile, 3, 149, 22));
        for(int i = 0; i < 3; i++) {
            for(int k = 0; k < 9; k++) {
                a(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++) {
            a(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }

    }

    @Override
    public void a()
    {
        super.a();
        for(int i = 0; i < listeners.size(); i++) {
            ICrafting icrafting = (ICrafting)listeners.get(i);
            if(lastCookTime != tile.getCookTime()) {
                icrafting.setContainerData(this, 0, tile.getCookTime());
            }
        }

        lastCookTime = tile.getCookTime();
    }

    @Override
    public void addSlotListener(ICrafting icrafting)
    {
        super.addSlotListener(icrafting);
        icrafting.setContainerData(this, 0, tile.getCookTime());
    }

    public void updateProgressBar(int i, int j)
    {
        if(i == 0) {
            tile.setCookTime(j);
        }
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        return tile.a(entityplayer);
    }

    @Override
    public ItemStack a(int i)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)e.get(i);
        if(slot != null && slot.c()) {
            ItemStack itemstack1 = slot.getItem();
            itemstack = itemstack1.cloneItemStack();
            if(i >= 4 && i < 40 && SlotCokeOven.canPlaceItem(itemstack)) {
                if(!a(itemstack1, 0, 1, false)) {
                    return null;
                }
            } else if(i >= 4 && i < 40 && SlotLiquidContainerEmpty.canPlaceItem(itemstack)) {
                if(!a(itemstack1, 3, 4, false)) {
                    return null;
                }
            } else if(i >= 4 && i < 31) {
                if(!a(itemstack1, 31, 40, false)) {
                    return null;
                }
            } else if(i >= 31 && i < 40) {
                if(!a(itemstack1, 4, 31, false)) {
                    return null;
                }
            } else if(!a(itemstack1, 4, 40, false)) {
                return null;
            }
            if(itemstack1.count == 0) {
                slot.set(null);
            } else {
                slot.d();
            }
            if(itemstack1.count != itemstack.count) {
                slot.c(itemstack1);
            } else {
                return null;
            }
        }
        return itemstack;
    }
    
    public IInventory getInventory() {
    	return tile;
    }
}
