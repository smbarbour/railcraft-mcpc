package railcraft;

import net.minecraft.server.Container;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import forge.IGuiHandler;
import railcraft.common.*;
import railcraft.common.carts.ContainerBore;
import railcraft.common.carts.ContainerCartEnergy;
import railcraft.common.carts.ContainerTankCart;
import railcraft.common.carts.ContainerTrackRelayer;
import railcraft.common.carts.ContainerUndercutter;
import railcraft.common.carts.ContainerWorkCart;
import railcraft.common.carts.EntityCartEnergy;
import railcraft.common.carts.EntityCartTank;
import railcraft.common.carts.EntityCartTrackRelayer;
import railcraft.common.carts.EntityCartUndercutter;
import railcraft.common.carts.EntityCartWork;
import railcraft.common.carts.EntityTunnelBore;
import railcraft.common.detector.ContainerDetectorSheep;
import railcraft.common.detector.ContainerDetectorTank;
import railcraft.common.detector.TileDetectorSheep;
import railcraft.common.detector.TileDetectorTank;
import railcraft.common.utility.*;

public class GuiHandler implements IGuiHandler
{

    public static void openGui(EnumGui gui, EntityHuman player, World world, int i, int j, int k)
    {
        if(gui.hasContainer()) {
            player.openGui(Railcraft.getMod(), gui.getId(), world, i, j, k);
        }
    }

    public static void openGui(EnumGui gui, EntityHuman player, World world, Entity entity)
    {
        if(gui.hasContainer()) {
            player.openGui(Railcraft.getMod(), gui.getId(), world, entity.id, -1, 0);
        }
    }

    @Override
    public Container getGuiElement(int ID, EntityHuman player, World world, int i, int j, int k)
    {
        if(j >= 0) {
            TileEntity tile = world.getTileEntity(i, j, k);
            switch (EnumGui.fromId(ID)) {
                case LOADER_ITEM:
                    if(tile instanceof IItemLoader) {
                        return new ContainerItemLoader(player.inventory, (IItemLoader)tile);
                    }
                    break;
                case LOADER_LIQUID:
                    if(tile instanceof TileLiquidLoader) {
                        return new ContainerLiquidLoader(player.inventory, (TileLiquidLoader)tile);
                    }
                    break;
                case UNLOADER_LIQUID:
                    if(tile instanceof TileLiquidUnloader) {
                        return new ContainerLiquidUnloader(player.inventory, (TileLiquidUnloader)tile);
                    }
                    break;
                case LOADER_ENERGY:
                    if(tile instanceof TileEnergyLoader) {
                        return new ContainerEnergyLoader(player.inventory, (TileEnergyLoader)tile);
                    }
                    break;
                case UNLOADER_ENERGY:
                    if(tile instanceof TileEnergyUnloader) {
                        return new ContainerEnergyLoader(player.inventory, (TileEnergyUnloader)tile);
                    }
                    break;
                case DETECTOR_TANK:
                    if(tile instanceof TileDetectorTank) {
                        return new ContainerDetectorTank(player.inventory, (TileDetectorTank)tile);
                    }
                    break;
                case DETECTOR_SHEEP:
                    if(tile instanceof TileDetectorSheep) {
                        return new ContainerDetectorSheep(player.inventory, (TileDetectorSheep)tile);
                    }
                    break;
                case CART_DISPENSER:
                    if(tile instanceof TileDispenserCart) {
                        return new ContainerDispenserCart(player.inventory, (TileDispenserCart)tile);
                    }
                    break;
                case TRAIN_DISPENSER:
                    if(tile instanceof TileDispenserTrain) {
                        return new ContainerDispenserTrain(player.inventory, (TileDispenserTrain)tile);
                    }
                    break;
                case COKE_OVEN:
                    if(tile instanceof TileCokeOven) {
                        return new ContainerCokeOven(player.inventory, (TileCokeOven)tile);
                    }
                    break;
                case BLAST_FURNACE:
                    if(tile instanceof TileBlastFurnace) {
                        return new ContainerBlastFurnace(player.inventory, (TileBlastFurnace)tile);
                    }
                    break;
                case ROCK_CRUSHER:
                    if(tile instanceof TileRockCrusher) {
                        return new ContainerRockCrusher(player.inventory, (TileRockCrusher)tile);
                    }
                    break;
                case WATER_TANK:
                    if(tile instanceof TileWaterTank) {
                        return new ContainerWaterTank(player.inventory, (TileWaterTank)tile);
                    }
                    break;
                case ROLLING_MACHINE:
                    if(tile instanceof TileRollingMachine) {
                        return new ContainerRollingMachine(player.inventory, (TileRollingMachine)tile);
                    }
                    break;
                case FEED_STATION:
                    if(tile instanceof TileFeedStation) {
                        return new ContainerFeedStation(player.inventory, (TileFeedStation)tile);
                    }
                    break;
            }
        } else {
            Entity entity = Railcraft.getEntity(world, i);
            switch (EnumGui.fromId(ID)) {
                case CART_BORE:
                    if(entity instanceof EntityTunnelBore) {
                        return new ContainerBore(player.inventory, (EntityTunnelBore)entity);
                    }
                    break;
                case CART_ENERGY:
                    if(entity instanceof EntityCartEnergy) {
                        return new ContainerCartEnergy(player.inventory, (EntityCartEnergy)entity);
                    }
                    break;
                case CART_TANK:
                    if(entity instanceof EntityCartTank) {
                        return new ContainerTankCart(player.inventory, (EntityCartTank)entity);
                    }
                    break;
                case CART_WORK:
                    if(entity instanceof EntityCartWork) {
                        return new ContainerWorkCart(player.inventory, (EntityCartWork)entity);
                    }
                    break;
                case CART_TRACK_RELAYER:
                    if(entity instanceof EntityCartTrackRelayer) {
                        return new ContainerTrackRelayer(player.inventory, (EntityCartTrackRelayer)entity);
                    }
                    break;
                case CART_UNDERCUTTER:
                    if(entity instanceof EntityCartUndercutter) {
                        return new ContainerUndercutter(player.inventory, (EntityCartUndercutter)entity);
                    }
                    break;
            }
        }
        return null;
    }
}
