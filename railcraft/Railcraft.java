package railcraft;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.server.FMLBukkitHandler;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.*;
import railcraft.common.structures.EnumStructure;

public class Railcraft
{

    public static final String CONFIG_FILE_NAME = "railcraft.cfg";

    public static String getVersion()
    {
        return mod_Railcraft.instance.getVersion();
    }

    public static String getNameAndVersion()
    {
        return "Railcraft Client " + getVersion();
    }

    public static File getMinecraftDir()
    {
        return new File(".");
    }

    public static File getConfigDir()
    {
        File dir = new File(getBaseConfigDir(), "railcraft");
        if(!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    public static File getBaseConfigDir()
    {
        File configFile = new File(getMinecraftDir(), "config");
        return configFile;
    }

    public static File getConfigFile()
    {
        return new File(getConfigDir(), CONFIG_FILE_NAME);
    }

    public static File getWorldSaveDir(World world)
    {
        File file = new File(ModLoader.getMinecraftServerInstance().getWorldName());
        if(file.isDirectory()) {
            return file;
        }
        throw new RuntimeException(file.getAbsolutePath() + " is not a directoy.");
    }

    public static void log(Level level, String msg, Object... params)
    {
        String m = "[Railcraft] " + msg;

        for(int i = 0; i < params.length; i++) {
            m = m.replace("{" + i + "}", params[i].toString());
        }

        FMLCommonHandler.instance().getFMLLogger().log(level, m);
    }

    public static boolean gameIsHost()
    {
        return true;
    }

    public static boolean gameIsNotHost()
    {
        return false;
    }

    public static boolean gameIsServer()
    {
        return true;
    }

    public static boolean gameIsClient()
    {
        return false;
    }

    public static mod_Railcraft getMod()
    {
        return mod_Railcraft.instance;
    }

    public static void addName(Object o, String name)
    {
    }

    public static int getUniqueBlockModelID()
    {
        return 0;
    }

    public static void registerStructureRenderer(EnumStructure type)
    {
    }

    public static void registerStructureRenderer()
    {
    }

    public static void registerMetalPostRenderer(int id)
    {
    }

    public static void registerTankCartItemRenderer(int itemId)
    {
    }

    public static World getWorld()
    {
        return null;
    }

    public static World getWorld(int dimension)
    {
        MinecraftServer mc = ModLoader.getMinecraftServerInstance();
        if(mc != null) {
            return mc.getWorldServer(dimension);
        }
        return null;
    }

    public static Entity getEntity(World world, int entityId)
    {
        if(world != null && world instanceof WorldServer) {
            return ((WorldServer)world).getEntity(entityId);
        }
        return null;
    }

    public static void removeEntity(Entity entity)
    {
        entity.die();
    }

    // Client only function
    public static void sendPacket(Packet packet)
    {
    }

    // Server only function
    public static void sendPacketToAll(Packet packet)
    {
//        ModLoaderMp.sendPacketToAll(mod_Railcraft.instance, packet);
    }

    // Server only function
    public static void sendPacketToPlayersAround(Packet packet, double i, double j, double k, int dim)
    {
        FMLBukkitHandler.instance().getServer().serverConfigurationManager.sendPacketNearby(i, j, k, 80, dim, packet);
    }
}
