package railcraft.common;

import net.minecraft.server.*;
import forge.ITextureProvider;
import ic2.api.IBoxable;
import railcraft.common.api.ICrowbar;

public class ItemCrowbar extends ItemTool implements ITextureProvider, ICrowbar, IBoxable
{

    public ItemCrowbar(int i)
    {
        super(i, 3, EnumToolMaterial.IRON, new Block[]{
                Block.RAILS, Block.DETECTOR_RAIL, Block.GOLDEN_RAIL
            });
//		  setMaxDamage(500);
        d(8);
        a("crowbarRC");
    }

    @Override
    public String getTextureFile()
    {
        return "/railcraft/textures/railcraft.png";
    }

    @Override
    public boolean canBeStoredInToolbox(ItemStack itemstack)
    {
        return true;
    }
}
