package railcraft.common;

import java.util.List;

import org.bukkit.craftbukkit.entity.CraftHumanEntity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.InventoryHolder;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;

/**
 * Creates a deep copy of an existing IInventory.
 *
 * Useful for performing inventory manipulations and then
 * examining the results without affecting the original inventory.
 * 
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class InventoryCopy implements IInventory
{

    private IInventory orignal;
    private ItemStack contents[];

    public InventoryCopy(IInventory orignal)
    {
        this.orignal = orignal;
        contents = new ItemStack[orignal.getSize()];
        for(int i = 0; i < contents.length; i++) {
            ItemStack stack = orignal.getItem(i);
            if(stack != null) {
                contents[i] = stack.cloneItemStack();
            }
        }
    }

    @Override
    public int getSize()
    {
        return contents.length;
    }

    @Override
    public ItemStack getItem(int i)
    {
        return contents[i];
    }

    @Override
    public ItemStack splitStack(int i, int j)
    {
        if(contents[i] != null) {
            if(contents[i].count <= j) {
                ItemStack itemstack = contents[i];
                contents[i] = null;
                update();
                return itemstack;
            }
            ItemStack itemstack1 = contents[i].a(j);
            if(contents[i].count <= 0) {
                contents[i] = null;
            }
            update();
            return itemstack1;
        } else {
            return null;
        }
    }

    @Override
    public void setItem(int i, ItemStack itemstack)
    {
        contents[i] = itemstack;
        if(itemstack != null && itemstack.count > getMaxStackSize()) {
            itemstack.count = getMaxStackSize();
        }
        update();
    }

    @Override
    public String getName()
    {
        return orignal.getName();
    }

    @Override
    public int getMaxStackSize()
    {
        return orignal.getMaxStackSize();
    }

    @Override
    public void update()
    {
    }

    @Override
    public boolean a(EntityHuman entityplayer)
    {
        return true;
    }

    @Override
    public void f()
    {
    }

    @Override
    public void g()
    {
    }

    @Override
    public ItemStack splitWithoutUpdate(int slot)
    {
        return orignal.splitWithoutUpdate(slot);
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public InventoryHolder getOwner() {
		return null;
	}

	@Override
	public List<HumanEntity> getViewers() {
		return null;
	}

	@Override
	public void onClose(CraftHumanEntity arg0) {
	}

	@Override
	public void onOpen(CraftHumanEntity arg0) {
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
