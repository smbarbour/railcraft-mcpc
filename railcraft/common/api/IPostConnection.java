package railcraft.common.api;

import net.minecraft.server.World;

/**
 * If you want your block to connect to posts (like the Concrete Block does),
 * implement this interface.
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public interface IPostConnection
{

    /**
     * Return true if the block at this location should connect to a post.
     * @param world The World
     * @param i x-Coord
     * @param j y-Coord
     * @param k z-Coord
     * @param side Side to connect to
     * @return true if connect
     */
    public boolean connectsAt(World world, int i, int j, int k, int side);
}
