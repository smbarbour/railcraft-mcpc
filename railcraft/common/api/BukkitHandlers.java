package railcraft.common.api;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ItemInWorldManager;
import net.minecraft.server.ItemStack;
import net.minecraft.server.ModLoader;
import net.minecraft.server.World;

import org.bukkit.Server;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.block.CraftBlockState;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class BukkitHandlers {

	public static boolean removeBlockWithEvent(Server server, World world, int i, int j, int k, String actorName) {
		Block bukkitBlock = world.getWorld().getBlockAt(i, j, k);
		Player actor = server.getPlayer(actorName);
		if (actor == null) {
			actor = getEntityByName(world, actorName).getBukkitEntity();
		}
		BlockBreakEvent event = new BlockBreakEvent(bukkitBlock, actor);
		server.getPluginManager().callEvent(event);
		//System.out.println("event.isCancelled: " + event.isCancelled());
		if (event.isCancelled()) {
			return false;
		} else {
			world.setTypeId(i, j, k, 0);
			return true;
		}
	}
	
	public static boolean placeBlockWithEvent(Server server, World world, ItemStack stack, int i, int j, int k, int clickedX, int clickedY, int clickedZ, boolean withData, EntityHuman actor) {
		if (actor == null) {
			actor = getEntityByName(world, "[Railcraft]");
		}
		CraftBlockState replacedBlockState = CraftBlockState.getBlockState(world, i, j, k);
		world.suppressPhysics = true;
		boolean result;
		if (withData) {
			result = world.setTypeIdAndData(i, j, k, stack.id, stack.getData());
		} else {
			result = world.setTypeId(i, j, k, stack.id);
		}
		BlockPlaceEvent event = CraftEventFactory.callBlockPlaceEvent(world, actor, replacedBlockState, clickedX, clickedY, clickedZ);
		//System.out.println("After: " + world.getTypeId(i, j, k));
		replacedBlockState.update(true);
		//System.out.println("After replacedBlockState.update: " + world.getTypeId(i, j, k));
		world.suppressPhysics = false;
		//System.out.println("Railcraft place ID: " + stack.id + " with data " + stack.getData() + " at (" + i + ", " + j + ", " + k + ") by " + actor.name + " resulting in isCancelled: " + event.isCancelled() + " canBuild: " + event.canBuild() + " Set succeeded: " + result);
		if (event.isCancelled() || !event.canBuild()) {
			return false;
		} else {
			if (withData) {
				world.setTypeIdAndData(i, j, k, stack.id, stack.getData());
			} else {
				world.setTypeId(i, j, k, stack.id);
			}			
			return true;
		}
			
	}
	
	public static EntityPlayer getEntityByName(World world, String actorName) {
		return new EntityPlayer(ModLoader.getMinecraftServerInstance(), world, actorName, new ItemInWorldManager(world));
	}
}
