package railcraft.common.api;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityMinecart;
import net.minecraft.server.ItemStack;
import net.minecraft.server.World;

/**
 * Abstract minecart class that implements the IItemTransfer
 * interface for convenience and as example for others who wish
 * to create carts that implements IItemTransfer.
 * This particular implementation assumes a simple inventory
 * and will attempt to pass along offers and requests to linked carts
 * if it cannot fulfill them itself.
 * <br/>
 * <br/>
 * Classes that extend this class:<br/>
 * EntityCartChest<br/>
 * EntityCartAnchor<br/>
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public abstract class TransferCartBase extends CartBase implements IItemTransfer
{

    /**
     * If passThrough == true, this cart will only pass requests along, it wont attempt to fulfill them.
     */
    protected boolean passThrough = false;

    public TransferCartBase(World world) {
    	super(world);
    }

    public TransferCartBase(World world, String player)
    {
        super(world, player);
    }

    @Override
    public ItemStack offerItem(Object source, ItemStack offer)
    {
        if(!passThrough && getSize() > 0) {
            offer = InventoryTools.moveItemStack(offer, this);
            if(offer == null) {
                return null;
            }
        }

        ILinkageManager lm = CartTools.getLinkageManager();

        EntityMinecart linkedCart = lm.getLinkedCartA(this);
        if(linkedCart != source && linkedCart instanceof IItemTransfer) {
            offer = ((IItemTransfer)linkedCart).offerItem(this, offer);
        }

        if(offer == null) {
            return null;
        }

        linkedCart = lm.getLinkedCartB(this);
        if(linkedCart != source && linkedCart instanceof IItemTransfer) {
            offer = ((IItemTransfer)linkedCart).offerItem(this, offer);
        }

        return offer;
    }

    @Override
    public ItemStack requestItem(Object source)
    {
        ItemStack result = null;
        if(!passThrough && getSize() > 0) {
            result = InventoryTools.removeOneItem(this);
            if(result != null) {
                return result;
            }
        }

        ILinkageManager lm = CartTools.getLinkageManager();

        EntityMinecart linkedCart = lm.getLinkedCartA(this);
        if(linkedCart != source && linkedCart instanceof IItemTransfer) {
            result = ((IItemTransfer)linkedCart).requestItem(this);
        }

        if(result != null) {
            return result;
        }

        linkedCart = lm.getLinkedCartB(this);
        if(linkedCart != source && linkedCart instanceof IItemTransfer) {
            result = ((IItemTransfer)linkedCart).requestItem(this);
        }

        return result;
    }

    @Override
    public ItemStack requestItem(Object source, ItemStack request)
    {
        ItemStack result = null;
        if(!passThrough && getSize() > 0) {
            result = InventoryTools.removeOneItem(this, request);
            if(result != null) {
                return result;
            }
        }

        ILinkageManager lm = CartTools.getLinkageManager();

        EntityMinecart linkedCart = lm.getLinkedCartA(this);
        if(linkedCart != source && linkedCart instanceof IItemTransfer) {
            result = ((IItemTransfer)linkedCart).requestItem(this, request);
        }

        if(result != null) {
            return result;
        }

        linkedCart = lm.getLinkedCartB(this);
        if(linkedCart != source && linkedCart instanceof IItemTransfer) {
            result = ((IItemTransfer)linkedCart).requestItem(this, request);
        }

        return result;
    }

    @Override
    public ItemStack requestItem(Object source, EnumItemType request)
    {
        ItemStack result = null;
        if(!passThrough && getSize() > 0) {
            result = InventoryTools.removeOneItem(this, request);
            if(result != null) {
                return result;
            }
        }

        ILinkageManager lm = CartTools.getLinkageManager();

        EntityMinecart linkedCart = lm.getLinkedCartA(this);
        if(linkedCart != source && linkedCart instanceof IItemTransfer) {
            result = ((IItemTransfer)linkedCart).requestItem(this, request);
        }

        if(result != null) {
            return result;
        }

        linkedCart = lm.getLinkedCartB(this);
        if(linkedCart != source && linkedCart instanceof IItemTransfer) {
            result = ((IItemTransfer)linkedCart).requestItem(this, request);
        }

        return result;
    }
}
