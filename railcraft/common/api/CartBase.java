package railcraft.common.api;

import net.minecraft.server.EntityMinecart;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;

public abstract class CartBase extends EntityMinecart
{
	public String owner;

    public CartBase(World world)
    {
        super(world);
    }
    
    public CartBase(World world, String player)
    {
    	this(world);
  		this.owner = player;
    }

    @Override
    public float getMaxSpeedRail()
    {
        NBTTagCompound data = getEntityData();
        if(data.getBoolean("SpeedTest")) {
            return super.getMaxSpeedRail();
        }
        float speed = data.getFloat("TrainSpeed");
        return speed > 0 ? speed : super.getMaxSpeedRail();
    }
    
    @Override
    protected void b(NBTTagCompound data)
    {
    	super.b(data);
    	data.setString("owner", owner);
    }
    
    @Override
    protected void a(NBTTagCompound data)
    {
    	super.a(data);
    	this.owner = data.getString("owner");
    }
}