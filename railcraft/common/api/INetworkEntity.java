package railcraft.common.api;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.World;

public interface INetworkEntity
{

    public World getWorld();

    public void writePacketData(DataOutputStream data) throws IOException;

    public void readPacketData(DataInputStream data) throws IOException;
}
