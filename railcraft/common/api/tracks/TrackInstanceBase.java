package railcraft.common.api.tracks;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.Block;
import net.minecraft.server.BlockMinecartTrack;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityLiving;
import net.minecraft.server.EntityMinecart;
import net.minecraft.server.ItemStack;
import net.minecraft.server.MathHelper;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.MinecartTrackLogic;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import railcraft.common.api.ICrowbar;

/**
 * All ITrackInstances should extend this class. It contains a number of
 * default functions and standard behavior for Tracks that should
 * greatly simplify implementing new Tracks when using this API.
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 * @see ITrackInstance
 * @see TrackRegistry
 * @see TrackSpec
 */
public abstract class TrackInstanceBase implements ITrackInstance
{

    private Block block;
    public TileEntity tileEntity;

    private Block getBlock()
    {
        if(block == null) {
            int id = tileEntity.world.getTypeId(getX(), getY(), getZ());
            block = Block.byId[id];
        }
        return block;
    }

    @Override
    public void setTile(TileEntity tile)
    {
        tileEntity = tile;
    }

    @Override
    public int getBasicRailMetadata(EntityMinecart cart)
    {
        return tileEntity.k();
    }

    @Override
    public void onMinecartPass(EntityMinecart cart)
    {
    }

    @Override
    public boolean blockActivated(EntityHuman player)
    {
        if(this instanceof ITrackReversable) {
            ItemStack current = player.U();
            if(current != null && current.getItem() instanceof ICrowbar) {
                ITrackReversable track = (ITrackReversable)this;
                track.setReversed(!track.isReversed());
                markBlockNeedsUpdate();
                if(current.d()) {
                    current.damage(1, player);
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBlockPlaced(int side)
    {
        if(tileEntity.world.isStatic) {
            return;
        }
        switchTrack(true);
        testPower();
        markBlockNeedsUpdate();
    }

    @Override
    public void onBlockPlacedBy(EntityLiving entityliving)
    {
        if(tileEntity.world.isStatic || entityliving == null) {
            return;
        }
        if(this instanceof ITrackReversable) {
            int dir = MathHelper.floor((double)((entityliving.yaw * 4F) / 360F) + 0.5D) & 3;
            ((ITrackReversable)this).setReversed(dir == 0 || dir == 1);
        }
        markBlockNeedsUpdate();
    }

    public void markBlockNeedsUpdate()
    {
        tileEntity.world.notify(tileEntity.x, tileEntity.y, tileEntity.z);
    }

    protected boolean isRailValid(World world, int i, int j, int k, int meta)
    {
        boolean valid = true;
        if(!world.isBlockSolidOnSide(i, j - 1, k, 1)) {
            valid = false;
        }
        if(meta == 2 && !world.isBlockSolidOnSide(i + 1, j, k, 1)) {
            valid = false;
        } else if(meta == 3 && !world.isBlockSolidOnSide(i - 1, j, k, 1)) {
            valid = false;
        } else if(meta == 4 && !world.isBlockSolidOnSide(i, j, k - 1, 1)) {
            valid = false;
        } else if(meta == 5 && !world.isBlockSolidOnSide(i, j, k + 1, 1)) {
            valid = false;
        }
        return valid;
    }

    @Override
    public void onNeighborBlockChange(int id)
    {
        int meta = tileEntity.k();
        boolean valid = isRailValid(tileEntity.world, tileEntity.x, tileEntity.y, tileEntity.z, meta);
        if(!valid) {
            Block blockTrack = getBlock();
            blockTrack.b(tileEntity.world, tileEntity.x, tileEntity.y, tileEntity.z, 0, 0);
            tileEntity.world.setTypeId(tileEntity.x, tileEntity.y, tileEntity.z, 0);
            return;
        }

        BlockMinecartTrack blockTrack = (BlockMinecartTrack)getBlock();
        if(id > 0 && Block.byId[id].isPowerSource() && isFlexibleRail() && MinecartTrackLogic.a(new MinecartTrackLogic(blockTrack, tileEntity.world, tileEntity.x, tileEntity.y, tileEntity.z)) == 3) {
            switchTrack(false);
        }
        testPower();
    }

    protected void switchTrack(boolean flag)
    {
        if(tileEntity.world.isStatic) {
            return;
        }
        int i = tileEntity.x;
        int j = tileEntity.y;
        int k = tileEntity.z;
        BlockMinecartTrack blockTrack = (BlockMinecartTrack)getBlock();
        (new MinecartTrackLogic(blockTrack, tileEntity.world, i, j, k)).a(tileEntity.world.isBlockIndirectlyPowered(i, j, k), flag);
    }

    protected void testPower()
    {
        if(!(this instanceof ITrackPowered)) {
            return;
        }
        int i = tileEntity.x;
        int j = tileEntity.y;
        int k = tileEntity.z;
        ITrackPowered r = (ITrackPowered)this;
        int meta = tileEntity.k();
        boolean powered = tileEntity.world.isBlockIndirectlyPowered(i, j, k) || tileEntity.world.isBlockIndirectlyPowered(i, j + 1, k) || testPowerPropagation(tileEntity.world, i, j, k, getTrackSpec(), meta, r.getPowerPropagation());
        if(powered != r.isPowered()) {
            r.setPowered(powered);
            Block blockTrack = getBlock();
            tileEntity.world.applyPhysics(i, j, k, blockTrack.id);
            tileEntity.world.applyPhysics(i, j - 1, k, blockTrack.id);
            if(meta == 2 || meta == 3 || meta == 4 || meta == 5) {
                tileEntity.world.applyPhysics(i, j + 1, k, blockTrack.id);
            }
            markBlockNeedsUpdate();
            // System.out.println("Setting power [" + i + ", " + j + ", " + k + "]");
        }
    }

    protected boolean testPowerPropagation(World world, int i, int j, int k, TrackSpec spec, int meta, int maxDist)
    {
        return isConnectedRailPowered(world, i, j, k, spec, meta, true, 0, maxDist) || isConnectedRailPowered(world, i, j, k, spec, meta, false, 0, maxDist);
    }

    protected boolean isConnectedRailPowered(World world, int i, int j, int k, TrackSpec spec, int meta, boolean dir, int dist, int maxDist)
    {
        if(dist >= maxDist) {
            return false;
        }
        boolean powered = true;
        switch (meta) {
            case 0: // '\0'
                if(dir) {
                    k++;
                } else {
                    k--;
                }
                break;

            case 1: // '\001'
                if(dir) {
                    i--;
                } else {
                    i++;
                }
                break;

            case 2: // '\002'
                if(dir) {
                    i--;
                } else {
                    i++;
                    j++;
                    powered = false;
                }
                meta = 1;
                break;

            case 3: // '\003'
                if(dir) {
                    i--;
                    j++;
                    powered = false;
                } else {
                    i++;
                }
                meta = 1;
                break;

            case 4: // '\004'
                if(dir) {
                    k++;
                } else {
                    k--;
                    j++;
                    powered = false;
                }
                meta = 0;
                break;

            case 5: // '\005'
                if(dir) {
                    k++;
                    j++;
                    powered = false;
                } else {
                    k--;
                }
                meta = 0;
                break;
        }
        if(testPowered(world, i, j, k, spec, dir, dist, maxDist, meta)) {
            return true;
        }
        return powered && testPowered(world, i, j - 1, k, spec, dir, dist, maxDist, meta);
    }

    protected boolean testPowered(World world, int i, int j, int k, TrackSpec spec, boolean dir, int dist, int maxDist, int orientation)
    {
        // System.out.println("Testing Power at <" + i + ", " + j + ", " + k + ">");
        int id = world.getTypeId(i, j, k);
        Block blockTrack = getBlock();
        if(id == blockTrack.id) {
            int meta = world.getData(i, j, k);
            TileEntity tile = world.getTileEntity(i, j, k);
            if(tile instanceof ITrackTile) {
                ITrackInstance track = ((ITrackTile)tile).getTrackInstance();
                if(!(track instanceof ITrackPowered) || track.getTrackSpec() != spec) {
                    return false;
                }
                if(orientation == 1 && (meta == 0 || meta == 4 || meta == 5)) {
                    return false;
                }
                if(orientation == 0 && (meta == 1 || meta == 2 || meta == 3)) {
                    return false;
                }
                if(((ITrackPowered)track).isPowered()) {
                    if(world.isBlockIndirectlyPowered(i, j, k) || world.isBlockIndirectlyPowered(i, j + 1, k)) {
                        return true;
                    } else {
                        return isConnectedRailPowered(world, i, j, k, spec, meta, dir, dist + 1, maxDist);
                    }
                }
            }
        }
        return false;
    }

    @Override
    public int getTextureIndex()
    {
        return getTrackSpec().getTextureIndex();
    }

    @Override
    public void writeToNBT(NBTTagCompound data)
    {
    }

    @Override
    public void readFromNBT(NBTTagCompound data)
    {
    }

    @Override
    public boolean canUpdate()
    {
        return false;
    }

    @Override
    public void q_()
    {
    }

    @Override
    public float getExplosionResistance(double srcX, double srcY, double srcZ, Entity exploder)
    {
        return 3.5f;
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
    }

    @Override
    public World getWorld()
    {
        return tileEntity.world;
    }

    @Override
    public int getX()
    {
        return tileEntity.x;
    }

    @Override
    public int getY()
    {
        return tileEntity.y;
    }

    @Override
    public int getZ()
    {
        return tileEntity.z;
    }

    @Override
    public boolean canPlaceRailAt(World world, int i, int j, int k)
    {
        return false;
    }

    /**
     * Return true if the rail can make corners.
     * Used by placement logic.
     * @return true if the rail can make corners.
     */
    @Override
    public boolean isFlexibleRail()
    {
        return false;
    }

    /**
     * Returns true if the rail can make up and down slopes.
     * Used by placement logic.
     * @return true if the rail can make slopes.
     */
    @Override
    public boolean canMakeSlopes()
    {
        return true;
    }

    /**
     * Returns the max speed of the rail.
     * @param cart The cart on the rail, may be null.
     * @return The max speed of the current rail.
     */
    @Override
    public float getRailMaxSpeed(EntityMinecart cart)
    {
        return 0.4f;
    }
}
