package railcraft.common.api.tracks;

/**
 * Implementing this interface will allow your track to be
 * powered via Redstone.
 *
 * And so long as you inherit from TrackInstanceBase, all the code for updating
 * the power state is already in place (including propagation).
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public interface ITrackPowered extends ITrackInstance
{

    public boolean isPowered();

    public void setPowered(boolean powered);

    public int getPowerPropagation();
}
