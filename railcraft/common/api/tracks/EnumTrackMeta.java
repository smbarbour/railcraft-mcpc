package railcraft.common.api.tracks;

public enum EnumTrackMeta
{

    NORTH_SOUTH(0),
    EAST_WEST(1),
    EAST_SLOPE(2),
    WEST_SLOPE(3),
    NORTH_SLOPE(4),
    SOUTH_SLOPE(5),
    EAST_SOUTH_CORNER(6),
    WEST_SOUTH_CORNER(7),
    WEST_NORTH_CORNER(8),
    EAST_NORTH_CORNER(9);
    private int meta;

    private EnumTrackMeta(int meta)
    {
        this.meta = meta;
    }

    public int getValue()
    {
        return meta;
    }

    public boolean isEqual(int i)
    {
        if(getValue() == i) {
            return true;
        }
        return false;
    }

    public static EnumTrackMeta fromInt(int i)
    {
        for(EnumTrackMeta entry : values()) {
            if(i == entry.meta) {
                return entry;
            }
        }
        return NORTH_SOUTH;
    }
}
