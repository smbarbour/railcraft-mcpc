package railcraft.common.api.tracks;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * The TrackRegistry is part of a system that allows 3rd party addons to simply,
 * quickly, and easily define new Tracks with unique behaviors without requiring
 * that any additional block ids be used.
 *
 * All the tracks in Railcraft are implemented using this system 100%
 * (except for Gated Tracks and Switch Tracks which have some custom render code).
 *
 * To define a new track, you need to define a TrackSpec and create a ITrackInstance.
 *
 * The TrackSpec contains basic constant information about the Track, while the TrackInstace
 * controls how an individual Track block interact with the world.
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 * @see TrackSpec
 * @see ITrackInstance
 * @see TrackInstanceBase
 */
public class TrackRegistry
{

    private static Map<Integer, TrackSpec> trackSpecs = new HashMap<Integer, TrackSpec>();

    public static void registerTrackSpec(TrackSpec trackSpec)
    {
        if(trackSpecs.put(trackSpec.getTrackId(), trackSpec) != null) {
            throw new RuntimeException("TrackId conflict detected, please adjust your config or contact the author of the " + trackSpec.getTrackTag());
        }
    }

    /**
     * Returns a cached copy of a TrackSpec object.
     *
     * @param trackId
     * @return
     */
    public static TrackSpec getTrackSpec(int trackId)
    {
        TrackSpec spec = trackSpecs.get(trackId);
        if(spec == null) {
            throw new RuntimeException("Unknown Track Spec = " + trackId);
        }
        return spec;
    }

    /**
     * Returns a shallow copied list of all Registered TrackSpecs.
     * @return list of TrackSpecs
     */
    public static Set<TrackSpec> getTrackSpecs()
    {
        return new HashSet<TrackSpec>(trackSpecs.values());
    }
}
