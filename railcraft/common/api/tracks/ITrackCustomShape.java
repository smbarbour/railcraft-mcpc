package railcraft.common.api.tracks;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.Vec3D;

/**
 * Used by rails that modify the bounding boxes.
 *
 * For example, the Gated Rails.
 *
 * Not very useful since there is no system in place to insert custom render code.
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public interface ITrackCustomShape extends ITrackInstance
{

    public AxisAlignedBB getCollisionBoundingBoxFromPool();

    public AxisAlignedBB getSelectedBoundingBoxFromPool();

    public MovingObjectPosition collisionRayTrace(Vec3D vec3d, Vec3D vec3d1);
}
