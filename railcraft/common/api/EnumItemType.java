package railcraft.common.api;

import net.minecraft.server.BlockMinecartTrack;
import net.minecraft.server.ItemBlock;
import net.minecraft.server.ItemStack;
import forge.MinecraftForge;

/**
 * This interface is used with several of the functions in InventoryTools
 * to provide a convenient means of dealing with entire classes of items without
 * having to specify each item individually.
 * @author CovertJaguar <railcraft.wikispaces.com>
 * @see InventoryTools
 */
public enum EnumItemType
{

    FUEL, RAIL, MINECART, LIQUID, BALLAST;

    public static boolean isItemType(ItemStack stack, EnumItemType filter)
    {
        if(stack == null || filter == null) {
            return false;
        }
        switch (filter) {
            case FUEL:
                return GeneralTools.getItemBurnTime(stack) > 0;
            case RAIL:
                return RailTools.isTrackItem(stack) || (stack.getItem() instanceof ItemBlock && BlockMinecartTrack.d(stack.id));
            case MINECART:
                return MinecraftForge.getCartClassForItem(stack) != null || stack.getItem() instanceof IMinecartItem;
//            case LIQUID:
//                return LiquidManager.getInstance().isContainer(stack);
            case BALLAST:
                return BallastRegistry.isItemBallast(stack);
            default:
                return false;
        }
    }
}
