package railcraft.common.api;

import net.minecraft.server.EntityMinecart;
import net.minecraft.server.ItemStack;

/**
 * Some helper functions to make interacting with carts simpler.
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public interface IMinecart
{

    /**
     * Returns true if the Minecart matches the item provided.
     * Generally just InventoryTools.isItemEqual(stack, cart.getCartItem()),
     * but some carts may need more control (the Tank Cart for example).
     *
     * @param stack the Filter
     * @param cart the Cart
     * @return true if the item matches the cart
     */
    public boolean doesCartMatchFilter(ItemStack stack, EntityMinecart cart);
}
