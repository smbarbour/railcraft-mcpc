package railcraft.common.api.crafting;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.minecraft.server.CraftingRecipe;
import net.minecraft.server.ItemStack;

/**
 * Fyi, I haven't tested this.
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public abstract class RailcraftCraftingManager
{

    /**
     * Adds a new Coke Oven recipe.
     *
     * @param inputId
     * @param inputDamage use -1 for wildcard
     */
    public static void addCokeOvenRecipe(int inputId, int inputDamage, ItemStack output, int liquidId, int liquidQty)
    {
        try {
            Class c = Class.forName("railcraft.common.util.crafting.CokeOvenCraftingManager");
            Method add = c.getMethod("addRecipe", int.class, int.class, ItemStack.class, int.class, int.class);
            add.invoke(null, inputId, inputDamage, output, liquidId, liquidQty);
        } catch (Exception ex) {
        }
    }

    /**
     * Returns a list of all Coke Oven recipes.
     * To be used with in-game crafting list mods.
     *
     * Modifying the returned list will not modify the internal recipe list,
     * if you need this feature let me know.
     *
     * @return
     */
    public static List<ICokeOvenRecipe> getCokeOvenRecipes()
    {
        try {
            Class c = Class.forName("railcraft.common.util.crafting.CokeOvenCraftingManager");
            Method getRecipe = c.getMethod("getRecipes");
            return (List<ICokeOvenRecipe>)getRecipe.invoke(null);
        } catch (Exception ex) {
        }
        return new ArrayList<ICokeOvenRecipe>();
    }

    /**
     * Adds a new Blast Furnace recipe.
     *
     * @param inputId
     * @param inputDamage use -1 for wildcard
     * @param cookTime number of ticks the recipe takes, Steel takes 320 ticks (16 seconds)
     */
    public static void addBlastFurnaceRecipe(int inputId, int inputDamage, int cookTime, ItemStack output)
    {
        try {
            Class c = Class.forName("railcraft.common.util.crafting.BlastFurnaceCraftingManager");
            Method add = c.getMethod("addRecipe", int.class, int.class, int.class, ItemStack.class);
            add.invoke(null, inputId, inputDamage, cookTime, output);
        } catch (Exception ex) {
        }
    }

    /**
     * Returns a list of all Blast Furnace recipes.
     * To be used with in-game crafting list mods.
     *
     * Modifying the returned list will not modify the internal recipe list,
     * if you need this feature let me know.
     *
     * @return
     */
    public static List<IBlastFurnaceRecipe> getBlastFurnaceRecipes()
    {
        try {
            Class c = Class.forName("railcraft.common.util.crafting.BlastFurnaceCraftingManager");
            Method getRecipe = c.getMethod("getRecipes");
            return (List<IBlastFurnaceRecipe>)getRecipe.invoke(null);
        } catch (Exception ex) {
        }
        return new ArrayList<IBlastFurnaceRecipe>();
    }

    /**
     * Adds a new Rock Crusher recipe.
     *
     * @param input
     * @param output a mapping of ItemStacks and their chance of being outputted, no more than 9 different item types, but there may be more than 9 entries if you list the same item more than once
     */
    public static void addRockCrusherRecipe(ItemStack input, HashMap<ItemStack, Float> output)
    {
        try {
            Class c = Class.forName("railcraft.common.util.crafting.RockCrusherCraftingManager");
            Method add = c.getMethod("addRecipe", ItemStack.class, HashMap.class);
            add.invoke(null, input, output);
        } catch (Exception ex) {
        }
    }

    /**
     * Returns a list of all Rock Crusher recipes.
     * To be used with in-game crafting list mods.
     *
     * Modifying the returned list will not modify the internal recipe list,
     * if you need this feature let me know.
     *
     * @return
     */
    public static List<IRockCrusherRecipe> getRockCrusherRecipes()
    {
        try {
            Class c = Class.forName("railcraft.common.util.crafting.RockCrusherCraftingManager");
            Method getRecipe = c.getMethod("getRecipes");
            return (List<IRockCrusherRecipe>)getRecipe.invoke(null);
        } catch (Exception ex) {
        }
        return new ArrayList<IRockCrusherRecipe>();
    }

    /**
     * Adds a new shaped Rolling Machine recipe.
     *
     * Parameters are the same as a normal crafting recipe.
     *
     */
    public static void addShapedRollingMachineRecipe(ItemStack output, Object components[])
    {
        try {
            Class c = Class.forName("railcraft.common.util.crafting.RollingMachineCraftingManager");
            Method inst = c.getMethod("getInstance");
            Object instance = inst.invoke(null);
            Method add = c.getMethod("addRecipe", ItemStack.class, Object[].class);
            add.invoke(instance, output, components);
        } catch (Exception ex) {
        }
    }

    /**
     * Adds a new shapeless Rolling Machine recipe.
     *
     * Parameters are the same as a normal crafting recipe.
     *
     */
    public static void addShapelessRollingMachineRecipe(ItemStack output, Object components[])
    {
        try {
            Class c = Class.forName("railcraft.common.util.crafting.RollingMachineCraftingManager");
            Method inst = c.getMethod("getInstance");
            Object instance = inst.invoke(null);
            Method add = c.getMethod("addShapelessRecipe", ItemStack.class, Object[].class);
            add.invoke(instance, output, components);
        } catch (Exception ex) {
        }
    }

    /**
     * Returns the Rolling Machine recipe list.
     *
     * Modifications to this list will be reflected in game.
     *
     * @return
     */
    public static List<CraftingRecipe> getRollingMachineRecipes()
    {
        try {
            Class c = Class.forName("railcraft.common.util.crafting.RollingMachineCraftingManager");
            Method inst = c.getMethod("getInstance");
            Object instance = inst.invoke(null);
            Method getRecipeList = c.getMethod("getRecipeList");
            return (List<CraftingRecipe>)getRecipeList.invoke(instance);
        } catch (Exception ex) {
        }
        return new ArrayList<CraftingRecipe>();
    }
}
