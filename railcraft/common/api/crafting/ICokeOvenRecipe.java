package railcraft.common.api.crafting;

import forestry.api.liquids.LiquidStack;
import net.minecraft.server.ItemStack;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public interface ICokeOvenRecipe
{

    public int getCookTime();

    public ItemStack getInput();

    public LiquidStack getLiquidOutput();

    public ItemStack getOutput();
}
