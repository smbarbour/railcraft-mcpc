package railcraft.common.api;

import cpw.mods.fml.common.FMLCommonHandler;
import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.EntityLiving;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IBlockAccess;
import net.minecraft.server.Item;
import net.minecraft.server.ItemBlock;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Material;
import net.minecraft.server.MathHelper;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.TileEntity;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import forge.ForgeHooks;
import forge.MinecraftForge;

public abstract class GeneralTools
{

    private static Random rand = new Random();

    /**
     * Returns a Random instance.
     * @return Random
     */
    public static Random getRand()
    {
        return rand;
    }

    /**
     * Replacement function for fuel values
     * since most users seem to overwrite TileEntityFurnace.
     * @param stack The item to test
     * @return The fuel value
     */
    public static int getItemBurnTime(ItemStack stack)
    {
        if(stack == null) {
            return 0;
        } else {
            int id = stack.getItem().id;
            if(stack.getItem() instanceof ItemBlock && Block.byId[id].material == Material.WOOD) {
                return 300;
            }
            if(id == Item.STICK.id) {
                return 100;
            }
            if(id == Item.COAL.id) {
                return 1600;
            }
            if(id == Item.LAVA_BUCKET.id) {
                return 20000;
            }
            if(id == Block.SAPLING.id) {
                return 100;
            }
            if(id == Item.BLAZE_ROD.id) {
                return 2400;
            }
            int ret = ForgeHooks.getItemBurnTime(stack);
            return (ret > 0 ? ret : FMLCommonHandler.instance().fuelLookup(id, stack.g()));
        }
    }

    /**
     * Performs a ray trace to determine which side of the block is under the cursor.
     * Used with crowbar rotations.
     *
     * @param player EntityPlayer
     * @return a side value 0-5
     */
    public static byte getCurrentMousedOverSide(EntityHuman player)
    {
        double distance = player.abilities.canInstantlyBuild ? 5.0F : 4.5F;
        Vec3D posVec = Vec3D.create(player.locX, player.locY, player.locZ);
        Vec3D lookVec = player.f(1);
        if(!MinecraftForge.isClient()){
            posVec.b += player.getHeadHeight();
        }
        lookVec = posVec.add(lookVec.a * distance, lookVec.b * distance, lookVec.c * distance);
        MovingObjectPosition mouseOver = player.world.a(posVec, lookVec);
        if(mouseOver != null) {
            return (byte)mouseOver.face;
        }
        return 0;
    }

    /**
     * Returns the side closest to the player. Used in placement logic for blocks.
     * @param world
     * @param i
     * @param j
     * @param k
     * @param entityplayer
     * @return a side
     */
    public static byte getSideClosestToPlayer(World world, int i, int j, int k, EntityLiving entityplayer)
    {
        if(MathHelper.abs((float)entityplayer.locX - (float)i) < 2.0F && MathHelper.abs((float)entityplayer.locZ - (float)k) < 2.0F) {
            double d = (entityplayer.locY + 1.8200000000000001D) - (double)entityplayer.height;
            if(d - (double)j > 2D) {
                return EnumDirection.UP.getValue();
            }
            if((double)j - d > 0.0D) {
                return EnumDirection.DOWN.getValue();
            }
        }
        int l = MathHelper.floor((double)((entityplayer.yaw * 4F) / 360F) + 0.5D) & 3;
        if(l == 0) {
            return EnumDirection.NORTH.getValue();
        }
        if(l == 1) {
            return EnumDirection.EAST.getValue();
        }
        if(l == 2) {
            return EnumDirection.SOUTH.getValue();
        }
        return l != 3 ? EnumDirection.DOWN.getValue() : EnumDirection.WEST.getValue();
    }

    /**
     * This function unlike getSideClosestToPlayer can only return north, south, east, west.
     * @param world
     * @param x
     * @param y
     * @param z
     * @param player
     * @return a side
     */
    public static byte getHorizontalSideClosestToPlayer(World world, int i, int j, int k, EntityLiving player)
    {
        EnumDirection facing = EnumDirection.NORTH;
        int dir = MathHelper.floor((double)((player.yaw * 4.0F) / 360.0F) + 0.5) & 3;
        if(dir == 0) {
            facing = EnumDirection.NORTH;
        }
        if(dir == 1) {
            facing = EnumDirection.EAST;
        }
        if(dir == 2) {
            facing = EnumDirection.SOUTH;
        }
        if(dir == 3) {
            facing = EnumDirection.WEST;
        }
        return facing.getValue();
    }

    // Helper functions to simplify logic involving adjecent blocks.
    public static byte getOppositeSide(int side)
    {
        return (byte)(side % 2 == 0 ? side + 1 : side - 1);
    }

    public static int getYOnSide(int j, int side)
    {
        if(side == 0) {
            return j - 1;
        } else if(side == 1) {
            return j + 1;
        }
        return j;
    }

    public static int getXOnSide(int i, int side)
    {
        if(side == 4) {
            return i - 1;
        } else if(side == 5) {
            return i + 1;
        }
        return i;
    }

    public static int getZOnSide(int k, int side)
    {
        if(side == 2) {
            return k - 1;
        } else if(side == 3) {
            return k + 1;
        }
        return k;
    }

    public static int getBlockMetadataFromSide(World world, int i, int j, int k, int side)
    {
        return world.getData(getXOnSide(i, side), getYOnSide(j, side), getZOnSide(k, side));
    }

    public static int getBlockIdOnSide(IBlockAccess world, int i, int j, int k, int side)
    {
        return world.getTypeId(getXOnSide(i, side), getYOnSide(j, side), getZOnSide(k, side));
    }

    public static TileEntity getBlockTileEntityFromSide(World world, int i, int j, int k, int side)
    {
        return world.getTileEntity(getXOnSide(i, side), getYOnSide(j, side), getZOnSide(k, side));
    }

    public static void notifyBlocksOfNeighborChangeOnSide(World world, int i, int j, int k, int blockID, int side)
    {
        world.applyPhysics(getXOnSide(i, side), getYOnSide(j, side), getZOnSide(k, side), blockID);
    }
}