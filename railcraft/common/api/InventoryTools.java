package railcraft.common.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import net.minecraft.server.BlockMinecartTrack;
import net.minecraft.server.IInventory;
import net.minecraft.server.InventoryLargeChest;
import net.minecraft.server.Item;
import net.minecraft.server.ItemBlock;
import net.minecraft.server.ItemStack;
import net.minecraft.server.TileEntity;
import net.minecraft.server.TileEntityChest;
import net.minecraft.server.World;
import buildcraft.api.ISpecialInventory;
import buildcraft.api.Orientations;
import forge.ISidedInventory;
import forge.MinecraftForge;

public abstract class InventoryTools
{

    public static List<IInventory> getAdjacentInventories(World world, int i, int j, int k)
    {
        return getAdjacentInventories(world, i, j, k, null);
    }

    public static List<IInventory> getAdjacentInventories(World world, int i, int j, int k, Class<? extends IInventory> type)
    {
        List<IInventory> list = new ArrayList<IInventory>(5);
        for(int side = 0; side < 6; side++) {
            IInventory inv = getInventoryFromSide(world, i, j, k, side, type);
            if(inv != null) {
                list.add(inv);
            }
        }
        return list;
    }

    public static Map<Integer, IInventory> getAdjacentInventoryMap(World world, int i, int j, int k)
    {
        return getAdjacentInventoryMap(world, i, j, k, null);
    }

    public static Map<Integer, IInventory> getAdjacentInventoryMap(World world, int i, int j, int k, Class<? extends IInventory> type)
    {
        Map<Integer, IInventory> map = new TreeMap<Integer, IInventory>();
        for(int side = 0; side < 6; side++) {
            IInventory inv = getInventoryFromSide(world, i, j, k, side, type);
            if(inv != null) {
                map.put(side, inv);
            }
        }
        return map;
    }

    public static IInventory getInventoryFromSide(World world, int i, int j, int k, int side, Class<? extends IInventory> type)
    {
        i = GeneralTools.getXOnSide(i, side);
        j = GeneralTools.getYOnSide(j, side);
        k = GeneralTools.getZOnSide(k, side);

        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile == null || !(tile instanceof IInventory) || (type != null && !type.isAssignableFrom(tile.getClass()))) {
            return null;
        }
        IInventory inv = (IInventory)tile;
        if(inv instanceof ISpecialInventory) {
        } else if(inv instanceof ISidedInventory) {
            int s = GeneralTools.getOppositeSide(side);
            ISidedInventory sinv = (ISidedInventory)inv;
            inv = new InventoryMapper(inv, sinv.getStartInventorySide(s), sinv.getSizeInventorySide(s));
        } else if(inv instanceof TileEntityChest) {
            for(int s = 2; s < 6; s++) {
                tile = GeneralTools.getBlockTileEntityFromSide(world, i, j, k, s);
                if(tile instanceof TileEntityChest) {
                    IInventory inv2 = (IInventory)tile;
                    return new InventoryLargeChest("Large Chest", inv, inv2);
                }
            }
        }
        return inv;
    }

    public static boolean isInventoryEmpty(IInventory inv)
    {
        ItemStack stack = null;
        for(int slot = 0; slot < inv.getSize(); slot++) {
            stack = inv.getItem(slot);
            if(stack != null) {
                break;
            }
        }
        return stack == null;
    }

    public static boolean isInventoryEmpty(IInventory inv, int side)
    {
        if(inv instanceof ISidedInventory) {
            ISidedInventory sided = (ISidedInventory)inv;
            inv = new InventoryMapper(inv, sided.getStartInventorySide(side), sided.getSizeInventorySide(side));
        }
        ItemStack stack = null;
        for(int slot = 0; slot < inv.getSize(); slot++) {
            stack = inv.getItem(slot);
            if(stack != null) {
                break;
            }
        }
        return stack == null;
    }

    public static boolean isInventoryFull(IInventory inv)
    {
        ItemStack stack = null;
        for(int slot = 0; slot < inv.getSize(); slot++) {
            stack = inv.getItem(slot);
            if(stack == null) {
                break;
            }
        }
        return stack != null;
    }

    public static boolean isInventoryFull(IInventory inv, int side)
    {
        if(inv instanceof ISidedInventory) {
            ISidedInventory sided = (ISidedInventory)inv;
            inv = new InventoryMapper(inv, sided.getStartInventorySide(side), sided.getSizeInventorySide(side));
        }
        ItemStack stack = null;
        for(int slot = 0; slot < inv.getSize(); slot++) {
            stack = inv.getItem(slot);
            if(stack == null) {
                break;
            }
        }
        return stack != null;
    }

    public static int countItems(IInventory inv)
    {
        if(inv instanceof ISpecialInventory) {
            return 0;
        }
        int count = 0;
        ItemStack stack = null;
        for(int slot = 0; slot < inv.getSize(); slot++) {
            stack = inv.getItem(slot);
            if(stack != null) {
                count += stack.count;
            }
        }
        return count;
    }

    public static int countItems(IInventory inv, int side)
    {
        if(inv instanceof ISpecialInventory) {
            return 0;
        }
        if(inv instanceof ISidedInventory) {
            ISidedInventory sided = (ISidedInventory)inv;
            inv = new InventoryMapper(inv, sided.getStartInventorySide(side), sided.getSizeInventorySide(side));
        }
        return countItems(inv);
    }

    public static int countItems(IInventory inv, int side, ItemStack... filters)
    {
        if(inv instanceof ISpecialInventory) {
            return 0;
        }
        if(inv instanceof ISidedInventory) {
            ISidedInventory sided = (ISidedInventory)inv;
            inv = new InventoryMapper(inv, sided.getStartInventorySide(side), sided.getSizeInventorySide(side));
        }
        return countItems(inv, filters);
    }

    public static int countItems(IInventory inv, ItemStack... filters)
    {
        if(inv instanceof ISpecialInventory) {
            return 0;
        }
        boolean hasFilter = false;
        for(ItemStack filter : filters) {
            if(filter != null) {
                hasFilter = true;
                break;
            }
        }

        if(!hasFilter) {
            return countItems(inv);
        }

        int count = 0;
        ItemStack stack = null;
        for(int slot = 0; slot < inv.getSize(); slot++) {
            stack = inv.getItem(slot);
            if(stack != null) {
                for(ItemStack filter : filters) {
                    if(filter != null && isItemEqual(stack, filter)) {
                        count += stack.count;
                        break;
                    }
                }
            }
        }
        return count;
    }

    /**
     * Returns true if the inventory contains the specified item.
     *
     * @param inv The IIinventory to check
     * @param item The ItemStack to look for
     * @return true is exists
     */
    public static boolean containsItem(IInventory inv, ItemStack item)
    {
        return countItems(inv, item) > 0;
    }

    /**
     * Returns a map backed by an <code>ItemStackMap</code> that lists the total number
     * of each type of item in the inventory.
     *
     * @param inv The <code>IInventory</code> to generate the manifest for
     * @return A <code>Map</code> that lists how many of each item is in the <code>IInventory</code>
     * @see ItemStackMap
     */
    public static Map<ItemStack, Integer> getManifest(IInventory inv)
    {
        Map<ItemStack, Integer> manifest = new ItemStackMap<Integer>();
        for(int i = 0; i < inv.getSize(); i++) {
            ItemStack slot = inv.getItem(i);
            if(slot != null) {
                Integer count = manifest.get(slot);
                if(count == null) {
                    count = 0;
                }
                count += slot.count;
                manifest.put(slot, count);
            }
        }
        return manifest;
    }

    /**
     * Attempts to move a single item from one inventory to another.
     * If the input inventory is an instance of ISpecialInventory,
     * it is possible that this function will move more than one item.
     * @param input
     * @param output
     * @return true if the operation was successful, false if it was not.
     */
    public static boolean moveOneItem(IInventory input, IInventory output)
    {
        if(input instanceof ISpecialInventory) {
            ItemStack inputStack = ((ISpecialInventory)input).extractItem(true, Orientations.Unknown);
            if(inputStack != null) {
                int size = inputStack.count;
                ItemStack result = moveItemStack(inputStack, output);
                if(result == null || result.count != size) {
                    return true;
                }
            }
            return false;
        }
        for(int inputSlot = 0; inputSlot < input.getSize(); inputSlot++) {
            ItemStack inputStack = input.getItem(inputSlot);
            if(inputStack == null) {
                continue;
            }
            ItemStack tempStack = inputStack.cloneItemStack();
            tempStack.count = 1;
            ItemStack result = moveItemStack(tempStack, output);
            if(result == null) {
                input.splitStack(inputSlot, 1);
                return true;
            }
        }
        return false;
    }

    /**
     * Attempts to move a single item from one inventory to another.
     * This function is not compatible with ISpecalInventory at this time.
     * @param input
     * @param output
     * @param filer an ItemStack to match against
     * @return true if the operation was successful, false if it was not.
     */
    public static boolean moveOneItem(IInventory input, IInventory output, ItemStack filter)
    {
        if(input instanceof ISpecialInventory || output instanceof ISpecialInventory) {
            return false;
        }
        for(int inputSlot = 0; inputSlot < input.getSize(); inputSlot++) {
            ItemStack inputStack = input.getItem(inputSlot);
            if(inputStack == null || !isItemEqual(filter, inputStack)) {
                continue;
            }
            ItemStack tempStack = inputStack.cloneItemStack();
            tempStack.count = 1;
            ItemStack result = moveItemStack(tempStack, output);
            if(result == null) {
                input.splitStack(inputSlot, 1);
                return true;
            }
        }
        return false;
    }

    /**
     * Attempts to move a single item from one inventory to another,
     * but only if it matches the provided filter.
     * This function is ISpecialInventory and ISidedInventory aware,
     * but may move more than one item in the case of ISpecialInventory.
     * @param input
     * @param output
     * @param filer an ItemStack to match against
     * @return true if the operation was successful, false if it was not.
     */
    public static boolean moveOneItem(IInventory input, int inSide, IInventory output, int outSide)
    {
        return moveOneItem(input, inSide, output, outSide, null);
    }

    /**
     * Attempts to move a single item from one inventory to another,
     * but only if it matches the provided filter.
     * This function is ISpecialInventory and ISidedInventory aware,
     * but may move more than one item in the case of an input ISpecialInventory.
     * @param input
     * @param output
     * @param filer an ItemStack to match against
     * @return true if the operation was successful, false if it was not.
     */
    public static boolean moveOneItem(IInventory input, int inSide, IInventory output, int outSide, ItemStack filter)
    {
        ItemStack inputStack = null;
        int inputSlot = 0;
        if(input instanceof ISpecialInventory) {
            ISpecialInventory special = (ISpecialInventory)input;
            inputStack = special.extractItem(false, Orientations.values()[inSide]);
            if(inputStack == null || (filter != null && !isItemEqual(filter, inputStack))) {
                return false;
            }
        } else {
            if(input instanceof ISidedInventory) {
                ISidedInventory sided = (ISidedInventory)input;
                input = new InventoryMapper(input, sided.getStartInventorySide(inSide), sided.getSizeInventorySide(inSide));
            }
            for(inputSlot = 0; inputSlot < input.getSize(); inputSlot++) {
                inputStack = input.getItem(inputSlot);
                if(inputStack != null && (filter == null || isItemEqual(filter, inputStack))) {
                    break;
                }
            }
        }
        if(inputStack != null) {
            ItemStack tempStack = inputStack.cloneItemStack();
            if(!(input instanceof ISpecialInventory)) {
                tempStack.count = 1;
            }
            ItemStack result = moveItemStack(tempStack, output, outSide);
            if(result == null) {
                if(input instanceof ISpecialInventory) {
                    inputStack = ((ISpecialInventory)input).extractItem(true, Orientations.values()[inSide]);
                } else {
                    input.splitStack(inputSlot, 1);
                }
                return true;
            }
        }
        return false;
    }

    /**
     * A more robust item comparison function. Supports items with damage = -1
     * matching any sub-type.
     * @param a An ItemStack
     * @param b An ItemStack
     * @return True if equal
     */
    public static boolean isItemEqual(ItemStack a, ItemStack b)
    {
        if(a == null || b == null) {
            return false;
        }
        if(a.id != b.id) {
            return false;
        }
        if(a.usesData() && (a.getData() == -1 || b.getData() == -1)) {
            return true;
        }
        if(a.usesData() && a.getData() != b.getData()) {
            return false;
        }
        return true;
    }

    /**
     * Returns true if the item matches the passed item type.
     * @param stack An ItemStack
     * @param filter The type of item
     * @return True if the item is the correct type
     */
    public static boolean isItemType(ItemStack stack, EnumItemType filter)
    {
        return EnumItemType.isItemType(stack, filter);
    }

    /**
     * Places an ItemStack in a destination IInventory. Will attempt to move as
     * much of the stack as possible, returning any remainder.
     * @param stack The ItemStack to put in the inventory.
     * @param dest The destination IInventory.
     * @return Null if itemStack was completely moved, the itemStack with remaining count if part or none of the stack was moved.
     */
    public static ItemStack moveItemStack(ItemStack stack, IInventory dest)
    {
        if(stack == null) {
            return null;
        }
        stack = stack.cloneItemStack();
        if(dest == null) {
            return stack;
        }
        if(dest instanceof ISpecialInventory) {
            ((ISpecialInventory)dest).addItem(stack, true, Orientations.Unknown);
            if(stack.count <= 0) {
                return null;
            } else {
                return stack;
            }
        }
        boolean movedItem = false;
        do {
            movedItem = false;
            ItemStack destStack = null;
            for(int ii = 0; ii < dest.getSize(); ii++) {
                destStack = dest.getItem(ii);
                if(destStack != null && destStack.doMaterialsMatch(stack)) {
                    int maxStack = Math.min(destStack.getMaxStackSize(), dest.getMaxStackSize());
                    int room = maxStack - destStack.count;
                    if(room > 0) {
                        int move = Math.min(room, stack.count);
                        destStack.count += move;
                        stack.count -= move;
                        if(stack.count <= 0) {
                            return null;
                        }
                        movedItem = true;
                    }
                }
            }
            if(!movedItem) {
                for(int ii = 0; ii < dest.getSize(); ii++) {
                    destStack = dest.getItem(ii);
                    if(destStack == null) {
                        if(stack.count > dest.getMaxStackSize()) {
                            dest.setItem(ii, stack.a(dest.getMaxStackSize()));
                        } else {
                            dest.setItem(ii, stack);
                            return null;
                        }
                        movedItem = true;
                    }
                }
            }
        } while(movedItem);
        return stack;
    }

    /**
     * Places an ItemStack in a destination IInventory. Will attempt to move as
     * much of the stack as possible, returning any remainder.
     * @param stack The ItemStack to put in the inventory.
     * @param dest The destination IInventory.
     * @return Null if itemStack was completely moved, the itemStack with remaining count if part or none of the stack was moved.
     */
    public static ItemStack moveItemStack(ItemStack stack, IInventory dest, int side)
    {
        if(stack == null) {
            return null;
        }
        stack = stack.cloneItemStack();
        if(dest == null) {
            return stack;
        }
        if(dest instanceof ISpecialInventory) {
            ((ISpecialInventory)dest).addItem(stack, true, Orientations.values()[side]);
            if(stack.count <= 0) {
                return null;
            } else {
                return stack;
            }
        }
        if(dest instanceof ISidedInventory) {
            ISidedInventory sided = (ISidedInventory)dest;
            dest = new InventoryMapper(dest, sided.getStartInventorySide(side), sided.getSizeInventorySide(side));
        }
        return moveItemStack(stack, dest);
    }

    /**
     * Checks if there is room for the ItemStack in the inventory.
     *
     * @param stack The ItemStack
     * @param dest The IInventory
     * @return true if room for stack
     */
    public static boolean isRoomForStack(ItemStack stack, IInventory dest)
    {
        if(stack == null) {
            return false;
        }
        if(dest == null) {
            return false;
        }
        if(dest instanceof ISpecialInventory) {
            return ((ISpecialInventory)dest).addItem(stack, false, Orientations.Unknown);
        }

        for(int ii = 0; ii < dest.getSize(); ii++) {
            ItemStack slot = dest.getItem(ii);
            if(slot == null) {
                return true;
            } else if(slot.doMaterialsMatch(stack)) {
                int maxStack = Math.min(slot.getMaxStackSize(), dest.getMaxStackSize());
                int room = maxStack - slot.count;
                if(room >= stack.count) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Removes and returns a single item from the inventory.
     * @param inv The inventory
     * @return An ItemStack
     */
    public static ItemStack removeOneItem(IInventory inv)
    {
        for(int i = 0; i < inv.getSize(); i++) {
            ItemStack slot = inv.getItem(i);
            if(slot != null) {
                return inv.splitStack(i, 1);
            }
        }
        return null;
    }

    /**
     * Removes and returns a single item from the inventory that matches the filter.
     * @param inv The inventory
     * @param filter ItemStack to match against
     * @return An ItemStack
     */
    public static ItemStack removeOneItem(IInventory inv, ItemStack filter)
    {
        for(int i = 0; i < inv.getSize(); i++) {
            ItemStack slot = inv.getItem(i);
            if(slot != null && isItemEqual(slot, filter)) {
                return inv.splitStack(i, 1);
            }
        }
        return null;
    }

    /**
     * Removes and returns a single item from the inventory that matches the filter.
     * @param inv The inventory
     * @param filter EnumItemType to match against
     * @return An ItemStack
     */
    public static ItemStack removeOneItem(IInventory inv, EnumItemType filter)
    {
        for(int i = 0; i < inv.getSize(); i++) {
            ItemStack slot = inv.getItem(i);
            if(slot != null && isItemType(slot, filter)) {
                return inv.splitStack(i, 1);
            }
        }
        return null;
    }
}
