package railcraft.common.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import net.minecraft.server.Block;
import net.minecraft.server.ItemBlock;
import net.minecraft.server.ItemStack;

/**
 * Register an item here to designate it as a possible
 * ballast that can be used in the Bore.
 *
 * It is expected that ballast is affected by gravity.
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public abstract class BallastRegistry
{

    private static Set<ItemStack> ballastRegistry = new ItemStackSet();

    static {
        registerBallast(new ItemStack(Block.GRAVEL));
        registerBallast(ItemRegistry.getItem("cube.crushed.obsidian", 1));
    }

    public static void registerBallast(ItemStack ballast)
    {
        if(ballast.getItem() instanceof ItemBlock) {
            ballastRegistry.add(ballast);
        } else {
            throw new RuntimeException("Attempted to register an invalid ballast, must be an ItemBlock item.");
        }
    }

    public static boolean isItemBallast(ItemStack ballast)
    {
        return ballastRegistry.contains(ballast);
    }

    public static List<ItemStack> getRegisteredBallasts()
    {
        return new ArrayList<ItemStack>(ballastRegistry);
    }
}
