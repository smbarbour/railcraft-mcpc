package railcraft.common.api;

import java.util.List;

import org.bukkit.craftbukkit.entity.CraftHumanEntity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.InventoryHolder;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;

/**
 * Wrapper class used to specify part of an existing
 * inventory to be treated as a complete inventory.
 * Used primarily to map a side of an ISidedInventory,
 * but it is also helpful for complex inventories
 * such as the Tunnel Bore.
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class InventoryMapper implements IInventory {

	 private final IInventory inv;
	 private final int start;
	 private final int size;

     /**
      * Creates a new InventoryMapper
      * @param inv The backing inventory
      * @param start The starting index
      * @param size The size of the new inventory, take care not to exceed the end of the backing inventory
      */
     public InventoryMapper(IInventory inv, int start, int size) {
		  this.inv = inv;
		  this.start = start;
		  this.size = size;
	 }

	 @Override
	 public int getSize() {
		  return size;
	 }

	 @Override
	 public ItemStack getItem(int i) {
		  return inv.getItem(start + i);
	 }

	 @Override
	 public ItemStack splitStack(int i, int j) {
		  return inv.splitStack(start + i, j);
	 }

	 @Override
	 public void setItem(int i, ItemStack itemstack) {
		  inv.setItem(start + i, itemstack);
	 }

	 @Override
	 public String getName() {
		  return inv.getName();
	 }

	 @Override
	 public int getMaxStackSize() {
		  return inv.getMaxStackSize();
	 }

	 @Override
	 public void update() {
		  inv.update();
	 }

	 @Override
	 public boolean a(EntityHuman entityplayer) {
		  return inv.a(entityplayer);
	 }

     @Override
	 public void f() {
		  inv.f();
	 }

     @Override
	 public void g() {
		  inv.g();
	 }

    @Override
    public ItemStack splitWithoutUpdate(int i)
    {
        return inv.splitWithoutUpdate(start + i);
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public InventoryHolder getOwner() {
		return null;
	}

	@Override
	public List<HumanEntity> getViewers() {
		return null;
	}

	@Override
	public void onClose(CraftHumanEntity arg0) {
	}

	@Override
	public void onOpen(CraftHumanEntity arg0) {
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
