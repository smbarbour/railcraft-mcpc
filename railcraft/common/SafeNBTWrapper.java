package railcraft.common;

import net.minecraft.server.NBTBase;
import net.minecraft.server.NBTTagByte;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagDouble;
import net.minecraft.server.NBTTagFloat;
import net.minecraft.server.NBTTagInt;
import net.minecraft.server.NBTTagShort;

public class SafeNBTWrapper
{

    private final NBTTagCompound data;

    public SafeNBTWrapper(NBTTagCompound data)
    {
        this.data = data;
    }

    /**
     * Retrieves a byte value using the specified key, or 0 if no such key was stored.
     */
    public byte getByte(String key)
    {
        if(data.hasKey(key)) {
            NBTBase tag = data.get(key);
            if(tag instanceof NBTTagByte) {
                return ((NBTTagByte)tag).data;
            } else if(tag instanceof NBTTagShort) {
                return (byte)((NBTTagShort)tag).data;
            } else if(tag instanceof NBTTagInt) {
                return (byte)((NBTTagInt)tag).data;
            }
        }
        return 0;
    }

    /**
     * Retrieves a short value using the specified key, or 0 if no such key was stored.
     */
    public short getShort(String key)
    {
        if(data.hasKey(key)) {
            NBTBase tag = data.get(key);
            if(tag instanceof NBTTagShort) {
                return ((NBTTagShort)tag).data;
            } else if(tag instanceof NBTTagInt) {
                return (short)((NBTTagInt)tag).data;
            } else if(tag instanceof NBTTagByte) {
                return ((NBTTagByte)tag).data;
            }
        }
        return 0;
    }

    /**
     * Retrieves an integer value using the specified key, or 0 if no such key was stored.
     */
    public int getInteger(String key)
    {
        if(data.hasKey(key)) {
            NBTBase tag = data.get(key);
            if(tag instanceof NBTTagInt) {
                return ((NBTTagInt)tag).data;
            } else if(tag instanceof NBTTagShort) {
                return ((NBTTagShort)tag).data;
            } else if(tag instanceof NBTTagByte) {
                return ((NBTTagByte)tag).data;
            }
        }
        return 0;
    }

    /**
     * Retrieves a float value using the specified key, or 0 if no such key was stored.
     */
    public float getFloat(String key)
    {
        if(data.hasKey(key)) {
            NBTBase tag = data.get(key);
            if(tag instanceof NBTTagFloat) {
                return ((NBTTagFloat)tag).data;
            } else if(tag instanceof NBTTagDouble) {
                return (float)((NBTTagDouble)tag).data;
            }
        }
        return 0;
    }

    /**
     * Retrieves a double value using the specified key, or 0 if no such key was stored.
     */
    public double getDouble(String key)
    {
        if(data.hasKey(key)) {
            NBTBase tag = data.get(key);
            if(tag instanceof NBTTagFloat) {
                return ((NBTTagFloat)tag).data;
            } else if(tag instanceof NBTTagDouble) {
                return ((NBTTagDouble)tag).data;
            }
        }
        return 0;
    }
}
