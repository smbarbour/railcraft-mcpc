package railcraft.common.util.crafting;

import forestry.api.liquids.LiquidStack;
import railcraft.common.api.crafting.ICokeOvenRecipe;
import java.util.ArrayList;
import java.util.List;
import CraftGuide.API.CraftGuideAPIObject;
import CraftGuide.API.ExtraSlot;
import CraftGuide.API.ICraftGuideRecipe.ItemSlot;
import CraftGuide.API.IRecipeGenerator;
import CraftGuide.API.IRecipeProvider;
import CraftGuide.API.IRecipeTemplate;
import CraftGuide.API.OutputSlot;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import railcraft.common.util.liquids.LiquidManager;
import railcraft.common.utility.EnumUtility;

public class CokeOvenCraftingManager extends CraftGuideAPIObject implements IRecipeProvider
{

    private static CokeOvenCraftingManager instance;
    private final List<CokeOvenRecipe> recipes = new ArrayList<CokeOvenRecipe>();
    private final ItemSlot[] slots;

    public CokeOvenCraftingManager()
    {
        slots = new ItemSlot[4];
        slots[0] = new ItemSlot(8, 8, 16, 16, 0, true);
        slots[1] = new OutputSlot(55, 8, 16, 16, 1, true);
        slots[2] = new OutputSlot(55, 34, 16, 16, 2, true);

        ItemStack oven = EnumUtility.COKE_OVEN.getItem();
        slots[3] = new ExtraSlot(8, 34, 16, 16, 3, oven);
    }

    private static CokeOvenCraftingManager getInstance()
    {
        if(instance == null) {
            instance = new CokeOvenCraftingManager();
        }
        return instance;
    }

    public static List<ICokeOvenRecipe> getRecipes()
    {
        List<ICokeOvenRecipe> list = new ArrayList<ICokeOvenRecipe>(getInstance().recipes);
        return list;
    }

    @Override
    public void generateRecipes(IRecipeGenerator generator)
    {
        ItemStack oven = EnumUtility.COKE_OVEN.getItem();
        if(oven != null) {
            IRecipeTemplate template = generator.createRecipeTemplate(slots, oven, "/railcraft/textures/gui_craft_guide.png", 1, 1, 82, 1);

            for(CokeOvenRecipe recipe : recipes) {
                ItemStack[] items = new ItemStack[4];
                items[0] = recipe.getInput();
                items[0].count *= LiquidManager.BUCKET_VOLUME / recipe.getLiquidOutput().liquidAmount;
                items[1] = recipe.getOutput();
                items[1].count *= LiquidManager.BUCKET_VOLUME / recipe.getLiquidOutput().liquidAmount;
                items[2] = LiquidManager.getInstance().fillLiquidContainer(recipe.getLiquidOutput(), new ItemStack(Item.BUCKET));
                items[3] = oven;
                generator.addRecipe(template, items);
            }
        }
    }

    public static class CokeOvenRecipe implements ICokeOvenRecipe
    {

        private final int inputId;
        private final int inputDamage;
        private final LiquidStack liquidOutput;
        private final int cookTime;
        private final ItemStack output;

        public CokeOvenRecipe(int inputId, int inputDamage, ItemStack output, LiquidStack liquid, int cookTime)
        {
            this.inputId = inputId;
            this.inputDamage = inputDamage;
            this.output = output;
            this.liquidOutput = liquid;
            this.cookTime = cookTime;
        }

        @Override
        public ItemStack getInput()
        {
            return new ItemStack(inputId, 1, inputDamage);
        }

        @Override
        public ItemStack getOutput()
        {
            if(output == null) {
                return null;
            }
            return output.cloneItemStack();
        }

        @Override
        public LiquidStack getLiquidOutput()
        {
            return liquidOutput;
        }

        @Override
        public int getCookTime()
        {
            return cookTime;
        }
    }

    public static void addRecipe(int inputId, int inputDamage, ItemStack output, LiquidStack liquidOutput, int cookTime)
    {
        getInstance().recipes.add(new CokeOvenRecipe(inputId, inputDamage, output, liquidOutput, cookTime));
    }

    public static void addRecipe(int inputId, ItemStack output, LiquidStack liquidOutput, int cookTime)
    {
        addRecipe(inputId, -1, output, liquidOutput, cookTime);
    }

    public static CokeOvenRecipe getRecipe(int inputId, int inputDamage)
    {
        for(CokeOvenRecipe r : getInstance().recipes) {
            if(r.inputId == inputId && r.inputDamage == inputDamage) {
                return r;
            }
        }
        for(CokeOvenRecipe r : getInstance().recipes) {
            if(r.inputId == inputId && r.inputDamage == -1) {
                return r;
            }
        }
        return null;
    }
}
