package railcraft.common.util.crafting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.minecraft.server.Block;
import CraftGuide.API.CraftGuideAPIObject;
import CraftGuide.API.ExtraSlot;
import CraftGuide.API.ICraftGuideRecipe.ItemSlot;
import CraftGuide.API.IRecipeGenerator;
import CraftGuide.API.IRecipeProvider;
import CraftGuide.API.IRecipeTemplate;
import CraftGuide.API.OutputSlot;
import net.minecraft.server.CraftingRecipe;
import net.minecraft.server.InventoryCrafting;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.ShapedRecipes;
import net.minecraft.server.ShapelessRecipes;
import railcraft.common.utility.EnumUtility;

public class RollingMachineCraftingManager extends CraftGuideAPIObject implements IRecipeProvider
{

    private static final int NUM_SLOTS = 11;
    private final ItemSlot[] slots;
    private static RollingMachineCraftingManager instance;
    private final List<CraftingRecipe> recipes;
    private final List<ItemStack[]> recipesCraftGuide;

    public static RollingMachineCraftingManager getInstance()
    {
        if(instance == null) {
            instance = new RollingMachineCraftingManager();
        }
        return instance;
    }

    private RollingMachineCraftingManager()
    {
        recipes = new ArrayList<CraftingRecipe>();
        recipesCraftGuide = new ArrayList<ItemStack[]>();

        slots = new ItemSlot[NUM_SLOTS];
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                slots[i + j * 3] = new ItemSlot(i * 18 + 3, j * 18 + 3, 16, 16, i + j * 3);
            }
        }
        slots[9] = new OutputSlot(59, 31, 16, 16, 9, true);

        ItemStack machine = EnumUtility.ROLLING_MACHINE.getItem();
        slots[10] = new ExtraSlot(59, 11, 16, 16, 10, machine);
    }

    public void addRecipe(ItemStack output, Object components[])
    {
        String s = "";
        int i = 0;
        int j = 0;
        int k = 0;
        if(components[i] instanceof String[]) {
            String as[] = (String[])components[i++];
            for(int l = 0; l < as.length; l++) {
                String s2 = as[l];
                k++;
                j = s2.length();
                s = (new StringBuilder()).append(s).append(s2).toString();
            }

        } else {
            while(components[i] instanceof String) {
                String s1 = (String)components[i++];
                k++;
                j = s1.length();
                s = (new StringBuilder()).append(s).append(s1).toString();
            }
        }
        HashMap hashmap = new HashMap();
        for(; i < components.length; i += 2) {
            Character character = (Character)components[i];
            ItemStack itemstack1 = null;
            if(components[i + 1] instanceof Item) {
                itemstack1 = new ItemStack((Item)components[i + 1]);
            } else if(components[i + 1] instanceof Block) {
                itemstack1 = new ItemStack((Block)components[i + 1], 1, -1);
            } else if(components[i + 1] instanceof ItemStack) {
                itemstack1 = (ItemStack)components[i + 1];
            }
            hashmap.put(character, itemstack1);
        }

        ItemStack recipeArray[] = new ItemStack[j * k];
        for(int i1 = 0; i1 < j * k; i1++) {
            char c = s.charAt(i1);
            if(hashmap.containsKey(Character.valueOf(c))) {
                recipeArray[i1] = ((ItemStack)hashmap.get(Character.valueOf(c))).cloneItemStack();
            } else {
                recipeArray[i1] = null;
            }
        }

        recipes.add(new ShapedRecipes(j, k, recipeArray, output));

        ItemStack[] recipe = new ItemStack[NUM_SLOTS];
        System.arraycopy(recipeArray, 0, recipe, 0, recipeArray.length);
        recipe[9] = output;
        recipesCraftGuide.add(recipe);
    }

    public void addShapelessRecipe(ItemStack output, Object compenents[])
    {
        List<ItemStack> ingredients = new ArrayList<ItemStack>();
        for(int j = 0; j < compenents.length; j++) {
            Object obj = compenents[j];
            if(obj instanceof ItemStack) {
                ingredients.add(((ItemStack)obj).cloneItemStack());
                continue;
            }
            if(obj instanceof Item) {
                ingredients.add(new ItemStack((Item)obj));
                continue;
            }
            if(obj instanceof Block) {
                ingredients.add(new ItemStack((Block)obj));
            } else {
                throw new RuntimeException("Invalid shapeless recipe!");
            }
        }

        recipes.add(new ShapelessRecipes(output, ingredients));

        ItemStack[] recipe = new ItemStack[NUM_SLOTS];
        int i = 0;
        for(ItemStack ingredient : ingredients) {
            recipe[i] = ingredient;
            i++;
        }
        recipe[9] = output;
        recipesCraftGuide.add(recipe);
    }

    public ItemStack findMatchingRecipe(InventoryCrafting inventorycrafting)
    {
        int i = 0;
        ItemStack itemstack = null;
        ItemStack itemstack1 = null;
        for(int j = 0; j < inventorycrafting.getSize(); j++) {
            ItemStack itemstack2 = inventorycrafting.getItem(j);
            if(itemstack2 == null) {
                continue;
            }
            if(i == 0) {
                itemstack = itemstack2;
            }
            if(i == 1) {
                itemstack1 = itemstack2;
            }
            i++;
        }

        if(i == 2 && itemstack.id == itemstack1.id && itemstack.count == 1 && itemstack1.count == 1 && Item.byId[itemstack.id].isRepairable()) {
            Item item = Item.byId[itemstack.id];
            int l = item.getMaxDurability() - itemstack.g();
            int i1 = item.getMaxDurability() - itemstack1.g();
            int j1 = l + i1 + (item.getMaxDurability() * 10) / 100;
            int k1 = item.getMaxDurability() - j1;
            if(k1 < 0) {
                k1 = 0;
            }
            return new ItemStack(itemstack.id, 1, k1);
        }
        for(int k = 0; k < recipes.size(); k++) {
            CraftingRecipe irecipe = (CraftingRecipe)recipes.get(k);
            if(irecipe.a(inventorycrafting)) {
                return irecipe.b(inventorycrafting);
            }
        }

        return null;
    }

    public List<CraftingRecipe> getRecipeList()
    {
        return recipes;
    }

    @Override
    public void generateRecipes(IRecipeGenerator generator)
    {
        ItemStack machine = EnumUtility.ROLLING_MACHINE.getItem();
        if(machine != null) {
            IRecipeTemplate template = generator.createRecipeTemplate(slots, machine, "/gui/CraftGuideRecipe.png", 163, 1, 163, 61);

//        System.out.println("BrewGuide: " + recipes.size() + " recipes added");

            for(ItemStack[] recipe : recipesCraftGuide) {
                recipe[10] = machine;
                generator.addRecipe(template, recipe);
            }
        }
    }
}
