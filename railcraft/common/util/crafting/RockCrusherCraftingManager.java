package railcraft.common.util.crafting;

import railcraft.common.api.crafting.IRockCrusherRecipe;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import CraftGuide.API.CraftGuideAPIObject;
import CraftGuide.API.ExtraSlot;
import CraftGuide.API.ICraftGuideRecipe.ItemSlot;
import CraftGuide.API.IRecipeGenerator;
import CraftGuide.API.IRecipeProvider;
import CraftGuide.API.IRecipeTemplate;
import CraftGuide.API.OutputSlot;
import net.minecraft.server.ItemStack;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.InventoryTools;
import railcraft.common.utility.EnumUtility;

public class RockCrusherCraftingManager extends CraftGuideAPIObject implements IRecipeProvider
{

    private static RockCrusherCraftingManager instance;
    private final List<CrusherRecipe> recipes = new ArrayList<CrusherRecipe>();
    private final ItemSlot[] slots;

    public RockCrusherCraftingManager()
    {
        slots = new ItemSlot[11];
        slots[0] = new ItemSlot(4, 11, 16, 16, 0, true);
        for(int y = 0; y < 3; y++) {
            for(int x = 0; x < 3; x++) {
                slots[1 + y * 3 + x] = new OutputSlot(24 + x * 18, 3 + y * 18, 16, 16, 1 + y * 3 + x, true);
            }
        }

        ItemStack crafter = EnumUtility.ROCK_CRUSHER.getItem();
        slots[10] = new ExtraSlot(4, 34, 16, 16, 10, crafter);
    }

    public static RockCrusherCraftingManager getInstance()
    {
        if(instance == null) {
            instance = new RockCrusherCraftingManager();
        }
        return instance;
    }

    public static List<IRockCrusherRecipe> getRecipes()
    {
        List<IRockCrusherRecipe> list = new ArrayList<IRockCrusherRecipe>(getInstance().recipes);
        return list;
    }

    @Override
    public void generateRecipes(IRecipeGenerator generator)
    {
        ItemStack crafter = EnumUtility.ROCK_CRUSHER.getItem();
        if(crafter != null) {
            IRecipeTemplate template = generator.createRecipeTemplate(slots, crafter, "/railcraft/textures/gui_craft_guide.png", 1, 61, 82, 61);

            for(CrusherRecipe recipe : recipes) {
                ItemStack[] items = new ItemStack[11];
                items[0] = recipe.getInput();
                List<ItemStack> output = recipe.getPossibleOuput();
                for(int i = 0; i < 9; i++) {
                    if(i >= output.size()) {
                        break;
                    }
                    items[1 + i] = output.get(i);
                }

                items[10] = crafter;
                generator.addRecipe(template, items);
            }
        }
    }

    public static class CrusherRecipe implements IRockCrusherRecipe
    {

        private final int inputId;
        private final int inputDamage;
        private final Map<ItemStack, Float> output;

        public CrusherRecipe(int inputId, int inputDamage, HashMap<ItemStack, Float> output)
        {
            this.inputId = inputId;
            this.inputDamage = inputDamage;
            this.output = output;
        }

        @Override
        public ItemStack getInput()
        {
            return new ItemStack(inputId, 1, inputDamage);
        }

        public Map<ItemStack, Float> getOutputs()
        {
            return output;
        }

        @Override
        public List<ItemStack> getPossibleOuput()
        {
            List<ItemStack> list = new ArrayList<ItemStack>();
            for(ItemStack item : output.keySet()) {
                for(ItemStack saved : list) {
                    if(InventoryTools.isItemEqual(saved, item)) {
                        if(saved.count + item.count <= saved.getMaxStackSize()) {
                            saved.count += item.count;
                            item = null;
                        } else {
                            int diff = saved.getMaxStackSize() - saved.count;
                            saved.count = saved.getMaxStackSize();
                            item.count -= diff;
                        }
                        break;
                    }
                }
                if(item != null) {
                    list.add(item.cloneItemStack());
                }
            }
            return list;
        }

        @Override
        public List<ItemStack> getRandomizedOuput()
        {
            List<ItemStack> list = new ArrayList<ItemStack>();
            for(Map.Entry<ItemStack, Float> entry : output.entrySet()) {
                if(GeneralTools.getRand().nextFloat() <= entry.getValue()) {
                    list.add(entry.getKey().cloneItemStack());
                }
            }
            return list;
        }

        public int getNumberOfOutputs()
        {
            return output.size();
        }
    }

    /**
     *
     * @param inputId
     * @param inputDamage metadata or -1 for wildcard
     * @param output A map of outputs and chances. If more than 9 types of items, there will be unexpected behavior.
     */
    public static void addRecipe(int inputId, int inputDamage, HashMap<ItemStack, Float> output)
    {
        getInstance().recipes.add(new CrusherRecipe(inputId, inputDamage, output));
    }

    /**
     *
     * @param input
     * @param output A map of outputs and chances.  If more than 9 types of items, there will be unexpected behavior.
     */
    public static void addRecipe(ItemStack input, HashMap<ItemStack, Float> output)
    {
        getInstance().recipes.add(new CrusherRecipe(input.id, input.getData(), output));
    }

    public static CrusherRecipe getRecipe(ItemStack input)
    {
        return getRecipe(input.id, input.getData());
    }

    public static CrusherRecipe getRecipe(int inputId, int inputDamage)
    {
        for(CrusherRecipe r : getInstance().recipes) {
            if(r.inputId == inputId && r.inputDamage == inputDamage) {
                return r;
            }
        }
        for(CrusherRecipe r : getInstance().recipes) {
            if(r.inputId == inputId && r.inputDamage == -1) {
                return r;
            }
        }
        return null;
    }
}
