package railcraft.common.util.crafting;

import railcraft.common.api.crafting.IBlastFurnaceRecipe;
import java.util.ArrayList;
import java.util.List;
import CraftGuide.API.CraftGuideAPIObject;
import CraftGuide.API.ExtraSlot;
import CraftGuide.API.ICraftGuideRecipe.ItemSlot;
import CraftGuide.API.IRecipeGenerator;
import CraftGuide.API.IRecipeProvider;
import CraftGuide.API.IRecipeTemplate;
import CraftGuide.API.OutputSlot;
import net.minecraft.server.ItemStack;
import railcraft.common.utility.EnumUtility;

public class BlastFurnaceCraftingManager extends CraftGuideAPIObject implements IRecipeProvider
{

    private static BlastFurnaceCraftingManager instance;
    private final List<BlastFurnaceRecipe> recipes = new ArrayList<BlastFurnaceRecipe>();
    private final ItemSlot[] slots;

    public BlastFurnaceCraftingManager()
    {
        slots = new ItemSlot[3];
        slots[0] = new ItemSlot(13, 21, 16, 16, 0, true);
        slots[1] = new OutputSlot(50, 21, 16, 16, 1, true);

        ItemStack furnace = EnumUtility.BLAST_FURNACE.getItem();
        slots[2] = new ExtraSlot(31, 39, 16, 16, 2, furnace);
    }

    private static BlastFurnaceCraftingManager getInstance()
    {
        if(instance == null) {
            instance = new BlastFurnaceCraftingManager();
        }
        return instance;
    }

    public static List<IBlastFurnaceRecipe> getRecipes()
    {
        List<IBlastFurnaceRecipe> list = new ArrayList<IBlastFurnaceRecipe>(getInstance().recipes);
        return list;
    }

    @Override
    public void generateRecipes(IRecipeGenerator generator)
    {
        ItemStack furnace = EnumUtility.BLAST_FURNACE.getItem();
        if(furnace != null) {
            IRecipeTemplate template = generator.createRecipeTemplate(slots, furnace, "/gui/CraftGuideRecipe.png", 1, 181, 82, 181);

            for(BlastFurnaceRecipe recipe : recipes) {
                ItemStack[] items = new ItemStack[4];
                items[0] = recipe.getInput();
                items[1] = recipe.getOutput();
                items[2] = furnace;
                generator.addRecipe(template, items);
            }
        }
    }

    public static class BlastFurnaceRecipe implements IBlastFurnaceRecipe
    {

        private final int inputId;
        private final int inputDamage;
        private final int cookTime;
        private final ItemStack output;

        public BlastFurnaceRecipe(int inputId, int inputDamage, int cookTime, ItemStack output)
        {
            this.inputId = inputId;
            this.inputDamage = inputDamage;
            this.cookTime = cookTime;
            this.output = output;
        }

        public boolean isRoomForOutput(ItemStack out)
        {
            if((out == null || output == null || (out.doMaterialsMatch(output)) && out.count + output.count <= output.getMaxStackSize())) {
                return true;
            }
            return false;
        }

        @Override
        public ItemStack getInput()
        {
            return new ItemStack(inputId, 1, inputDamage);
        }

        @Override
        public ItemStack getOutput()
        {
            if(output == null) {
                return null;
            }
            return output.cloneItemStack();
        }

        public int getOutputStackSize()
        {
            if(output == null) {
                return 0;
            }
            return output.count;
        }

        @Override
        public int getCookTime()
        {
            return cookTime;
        }
    }

    public static void addRecipe(int inputId, int inputDamage, int cookTime, ItemStack output)
    {
        getInstance().recipes.add(new BlastFurnaceRecipe(inputId, inputDamage, cookTime, output));
    }

    public static void addRecipe(int inputId, int cookTime, ItemStack output)
    {
        addRecipe(inputId, -1, cookTime, output);
    }

    public static BlastFurnaceRecipe getRecipe(int inputId, int inputDamage)
    {
        for(BlastFurnaceRecipe r : getInstance().recipes) {
            if(r.inputId == inputId && r.inputDamage == inputDamage) {
                return r;
            }
        }
        for(BlastFurnaceRecipe r : getInstance().recipes) {
            if(r.inputId == inputId && r.inputDamage == -1) {
                return r;
            }
        }
        return null;
    }

    public static BlastFurnaceRecipe getRecipe(ItemStack stack)
    {
        if(stack == null) {
            return null;
        }
        return getRecipe(stack.id, stack.getData());
    }
}
