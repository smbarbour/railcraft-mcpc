package railcraft.common.util.slots;

import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;

public class SlotLiquidMinecartSingle extends SlotMinecart
{

    public SlotLiquidMinecartSingle(IInventory iinventory, int slotIndex, int posX, int posY)
    {
        super(iinventory, slotIndex, posX, posY);
    }

    @Override
    public int a()
    {
        return 1;
    }

    @Override
    public boolean isAllowed(ItemStack stack)
    {
//        if(stack == null){
//            return false;
//        }
//        Class<? extends EntityMinecart> cartClass = MinecraftForge.getCartClassForItem(stack);
//        if(cartClass == null){
//            return false;
//        }
//        return super.isItemValid(stack) && ILiquidContainer.class.isAssignableFrom(cartClass);
        return super.isAllowed(stack);
    }
}
