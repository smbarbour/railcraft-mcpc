package railcraft.common.util.slots;

import net.minecraft.server.BlockMinecartTrack;
import net.minecraft.server.IInventory;
import net.minecraft.server.ItemBlock;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;
import railcraft.common.api.tracks.ITrackItem;

public class SlotTrack extends Slot
{

    public SlotTrack(IInventory iinventory, int slotIndex, int posX, int posY)
    {
        super(iinventory, slotIndex, posX, posY);
    }

    @Override
    public int a()
    {
        return 64;
    }

    @Override
    public boolean isAllowed(ItemStack stack)
    {
        return canPlaceItem(stack);
    }

    public static boolean canPlaceItem(ItemStack stack)
    {
        if(stack != null && (stack.getItem() instanceof ITrackItem || (stack.getItem() instanceof ItemBlock && BlockMinecartTrack.d(stack.id)))) {
            return true;
        }
        return false;
    }
}
