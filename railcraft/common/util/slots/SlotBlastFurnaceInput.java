package railcraft.common.util.slots;

import railcraft.common.util.crafting.BlastFurnaceCraftingManager;
import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;

public class SlotBlastFurnaceInput extends Slot
{

    public SlotBlastFurnaceInput(IInventory iinventory, int slotIndex, int posX, int posY)
    {
        super(iinventory, slotIndex, posX, posY);
    }

    @Override
    public int a()
    {
        return 64;
    }

    @Override
    public boolean isAllowed(ItemStack stack)
    {
        return canPlaceItem(stack);
    }

    public static boolean canPlaceItem(ItemStack stack)
    {
        if(BlastFurnaceCraftingManager.getRecipe(stack) != null) {
            return true;
        }
        return false;
    }
}
