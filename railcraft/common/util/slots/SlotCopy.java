package railcraft.common.util.slots;

import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;
import railcraft.common.api.InventoryTools;

public class SlotCopy extends Slot
{

    private int copyIndex;

    public SlotCopy(IInventory iinventory, int slotIndex, int posX, int posY, int copyIndex)
    {
        super(iinventory, slotIndex, posX, posY);
        this.copyIndex = copyIndex;
    }

    @Override
    public int a()
    {
        return 64;
    }

    @Override
    public boolean isAllowed(ItemStack stack)
    {
        ItemStack master = inventory.getItem(copyIndex);
        return InventoryTools.isItemEqual(stack, master);
    }
}
