package railcraft.common.util.slots;

import net.minecraft.server.IInventory;
import net.minecraft.server.ItemMinecart;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;
import railcraft.common.api.IMinecartItem;

public class SlotMinecart extends Slot
{

    public SlotMinecart(IInventory iinventory, int slotIndex, int posX, int posY)
    {
        super(iinventory, slotIndex, posX, posY);
    }

    @Override
    public boolean isAllowed(ItemStack stack)
    {
        if(stack == null) {
            return false;
        }
        if(stack.getItem() instanceof IMinecartItem) {
            return ((IMinecartItem)stack.getItem()).canBePlacedByNonPlayer(stack);
        }
        if(stack.getItem() instanceof ItemMinecart) {
            return true;
        }
        return false;
    }
}
