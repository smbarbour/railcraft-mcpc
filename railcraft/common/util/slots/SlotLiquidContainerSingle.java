package railcraft.common.util.slots;

import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;

public class SlotLiquidContainerSingle extends SlotLiquidContainer
{

    public SlotLiquidContainerSingle(IInventory iinventory, int slotIndex, int posX, int posY)
    {
        super(iinventory, slotIndex, posX, posY);
    }

    @Override
    public int a()
    {
        return 1;
    }

    public static boolean canPlaceItem(ItemStack itemstack)
    {
        return SlotLiquidContainer.canPlaceItem(itemstack);
    }
}
