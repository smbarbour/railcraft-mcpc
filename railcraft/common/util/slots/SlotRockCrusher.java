package railcraft.common.util.slots;

import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;
import railcraft.common.util.crafting.RockCrusherCraftingManager;

public class SlotRockCrusher extends Slot
{

    public SlotRockCrusher(IInventory iinventory, int slotIndex, int posX, int posY)
    {
        super(iinventory, slotIndex, posX, posY);
    }

    @Override
    public int a()
    {
        return 64;
    }

    @Override
    public boolean isAllowed(ItemStack stack)
    {
        return canPlaceItem(stack);
    }

    public static boolean canPlaceItem(ItemStack stack)
    {
        if(stack != null && RockCrusherCraftingManager.getRecipe(stack.id, stack.getData()) != null) {
            return true;
        }
        return false;
    }
}
