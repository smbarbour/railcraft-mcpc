package railcraft.common.util.slots;

import forestry.api.liquids.LiquidStack;
import net.minecraft.server.Block;
import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;
import railcraft.common.util.liquids.LiquidManager;

public class SlotWaterOrEmpty extends Slot
{

    public SlotWaterOrEmpty(IInventory iinventory, int slotIndex, int posX, int posY)
    {
        super(iinventory, slotIndex, posX, posY);
    }

    @Override
    public boolean isAllowed(ItemStack itemstack)
    {
        return canPlaceItem(itemstack);
    }

    public static boolean canPlaceItem(ItemStack itemstack)
    {
        if(itemstack == null) {
            return false;
        }
        if(LiquidManager.getInstance().isEmptyContainer(itemstack))  {
            return true;
        }
        LiquidStack liquid = LiquidManager.getInstance().getLiquidInContainer(itemstack);
        if(liquid != null && liquid.itemID == Block.STATIONARY_WATER.id) {
            return true;
        }
        return false;
    }
}
