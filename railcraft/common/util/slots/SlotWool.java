package railcraft.common.util.slots;

import net.minecraft.server.Block;
import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;

public class SlotWool extends Slot
{

    public SlotWool(IInventory iinventory, int slotIndex, int posX, int posY)
    {
        super(iinventory, slotIndex, posX, posY);
    }

    @Override
    public boolean isAllowed(ItemStack itemstack)
    {
        return canPlaceItem(itemstack);
    }

    public static boolean canPlaceItem(ItemStack itemstack)
    {
        if(itemstack == null) {
            return false;
        }
        if(itemstack.id == Block.WOOL.id) {
            return true;
        }
        return false;
    }
}
