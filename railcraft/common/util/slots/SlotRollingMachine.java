package railcraft.common.util.slots;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;
import buildcraft.api.Orientations;
import railcraft.common.utility.TileRollingMachine;

public class SlotRollingMachine extends Slot
{

    private final TileRollingMachine tile;
    private EntityHuman player;

    public SlotRollingMachine(EntityHuman entityplayer, TileRollingMachine tile, IInventory inv, int i, int j, int k)
    {
        super(inv, i, j, k);
        player = entityplayer;
        this.tile = tile;
    }

    @Override
    public boolean isAllowed(ItemStack itemstack)
    {
        return false;
    }

    @Override
    public void c(ItemStack itemstack)
    {
        // itemstack.onCrafting(player.worldObj, player);
        tile.extractItem(true, Orientations.Unknown, true);
    }
}
