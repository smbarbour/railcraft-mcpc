package railcraft.common.util.slots;

import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;
import ic2.api.IElectricItem;

public class SlotEnergy extends Slot
{

    public SlotEnergy(IInventory iinventory, int slotIndex, int posX, int posY)
    {
        super(iinventory, slotIndex, posX, posY);
    }

    @Override
    public int a()
    {
        return 64;
    }

    @Override
    public boolean isAllowed(ItemStack stack)
    {
        return canPlaceItem(stack);
    }

    public static boolean canPlaceItem(ItemStack stack)
    {
        if(stack != null && stack.getItem() instanceof IElectricItem) {
            return true;
        }
        return false;
    }
}
