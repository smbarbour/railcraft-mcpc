package railcraft.common.util.slots;

import net.minecraft.server.IInventory;
import net.minecraft.server.ItemBlock;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;

public class SlotBlockSingle extends Slot
{

    public SlotBlockSingle(IInventory iinventory, int slotIndex, int posX, int posY)
    {
        super(iinventory, slotIndex, posX, posY);
    }

    @Override
    public int a()
    {
        return 1;
    }


    @Override
    public boolean isAllowed(ItemStack stack)
    {
        return canPlaceItem(stack);
    }

    public static boolean canPlaceItem(ItemStack stack)
    {
        if(stack != null && stack.getItem() instanceof ItemBlock) {
            return true;
        }
        return false;
    }
}
