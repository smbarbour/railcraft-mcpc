package railcraft.common.util.slots;

import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;
import railcraft.common.util.liquids.LiquidManager;

public class SlotLiquidContainerEmpty extends Slot
{

    public SlotLiquidContainerEmpty(IInventory iinventory, int slotIndex, int posX, int posY)
    {
        super(iinventory, slotIndex, posX, posY);
    }

    @Override
    public boolean isAllowed(ItemStack itemstack)
    {
        return canPlaceItem(itemstack);
    }

    public static boolean canPlaceItem(ItemStack itemstack)
    {
        if(itemstack == null) {
            return false;
        }
        if(LiquidManager.getInstance().isEmptyContainer(itemstack)) {
            return true;
        }
        return false;
    }
}
