package railcraft.common.util.slots;

import net.minecraft.server.IInventory;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;
import railcraft.common.RailcraftToolItems;
import railcraft.common.api.InventoryTools;

public class SlotBlastFurnaceFuel extends Slot
{

    public SlotBlastFurnaceFuel(IInventory iinventory, int slotIndex, int posX, int posY)
    {
        super(iinventory, slotIndex, posX, posY);
    }

    @Override
    public int a()
    {
        return 64;
    }

    @Override
    public boolean isAllowed(ItemStack stack)
    {
        return canPlaceItem(stack);
    }

    public static boolean canPlaceItem(ItemStack stack)
    {
        return stack != null && ((stack.getItem() == Item.COAL && stack.getData() == 1) || InventoryTools.isItemEqual(stack, RailcraftToolItems.getCoalCoke()));
    }
}
