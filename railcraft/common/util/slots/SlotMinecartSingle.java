package railcraft.common.util.slots;

import net.minecraft.server.IInventory;

public class SlotMinecartSingle extends SlotMinecart
{

    public SlotMinecartSingle(IInventory iinventory, int slotIndex, int posX, int posY)
    {
        super(iinventory, slotIndex, posX, posY);
    }

    @Override
    public int a()
    {
        return 1;
    }
}
