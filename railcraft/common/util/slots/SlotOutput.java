package railcraft.common.util.slots;

import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;

public class SlotOutput extends Slot
{

    public SlotOutput(IInventory iinventory, int slotIndex, int posX, int posY)
    {
        super(iinventory, slotIndex, posX, posY);
    }

    @Override
    public boolean isAllowed(ItemStack itemstack)
    {
        return false;
    }
}
