package railcraft.common.util.slots;

import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;
import ic2.api.Items;
import railcraft.common.modules.ModuleIC2;

public class SlotUpgrade extends Slot
{

    public SlotUpgrade(IInventory iinventory, int slotIndex, int posX, int posY)
    {
        super(iinventory, slotIndex, posX, posY);
    }

    @Override
    public int a()
    {
        return 64;
    }

    @Override
    public boolean isAllowed(ItemStack stack)
    {
        return canPlaceItem(stack);
    }

    public static boolean canPlaceItem(ItemStack stack)
    {
        ItemStack storage = Items.getItem("energyStorageUpgrade");
        ItemStack overclocker = Items.getItem("overclockerUpgrade");
        ItemStack transformer = Items.getItem("transformerUpgrade");
        ItemStack lapotron = ModuleIC2.getLapotronUpgrade();

        if(stack != null) {
            if(storage != null && stack.doMaterialsMatch(storage)) {
                return true;
            } else if(overclocker != null && stack.doMaterialsMatch(overclocker)) {
                return true;
            } else if(transformer != null && stack.doMaterialsMatch(transformer)) {
                return true;
            } else if(lapotron != null && stack.doMaterialsMatch(lapotron)) {
                return true;
            }
        }
        return false;
    }
}
