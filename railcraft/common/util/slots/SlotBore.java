package railcraft.common.util.slots;

import railcraft.common.api.IBoreHead;
import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;

public class SlotBore extends Slot
{

    public SlotBore(IInventory iinventory, int slotIndex, int posX, int posY)
    {
        super(iinventory, slotIndex, posX, posY);
    }

    @Override
    public int a()
    {
        return 1;
    }

    @Override
    public boolean isAllowed(ItemStack stack)
    {
        return canPlaceItem(stack);
    }

    public static boolean canPlaceItem(ItemStack stack)
    {
        return stack != null && stack.getItem() instanceof IBoreHead;
    }
}
