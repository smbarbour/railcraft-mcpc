package railcraft.common.util.slots;

import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;
import railcraft.common.api.EnumItemType;
import railcraft.common.api.InventoryTools;

public class SlotFuel extends Slot
{

    public SlotFuel(IInventory iinventory, int slotIndex, int posX, int posY)
    {
        super(iinventory, slotIndex, posX, posY);
    }

    @Override
    public int a()
    {
        return 64;
    }

    @Override
    public boolean isAllowed(ItemStack stack)
    {
        return canPlaceItem(stack);
    }

    public static boolean canPlaceItem(ItemStack stack)
    {
        return InventoryTools.isItemType(stack, EnumItemType.FUEL);
    }
}
