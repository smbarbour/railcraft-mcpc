package railcraft.common.util.network;

import net.minecraft.server.NetworkManager;
import net.minecraft.server.Packet1Login;
import forge.IConnectionHandler;
import forge.MessageManager;

public class RailcraftConnectionHandler implements IConnectionHandler
{

    private RailcraftPacketHandler handler = new RailcraftPacketHandler();

    @Override
    public void onConnect(NetworkManager network)
    {
    }

    @Override
    public void onLogin(NetworkManager network, Packet1Login login)
    {
        MessageManager.getInstance().registerChannel(network, handler, RailcraftPacket.CHANNEL_NAME);
    }

    @Override
    public void onDisconnect(NetworkManager network, String message, Object[] args)
    {
        MessageManager.getInstance().unregisterChannel(network, handler, RailcraftPacket.CHANNEL_NAME);
    }
}
