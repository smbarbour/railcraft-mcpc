package railcraft.common.util.network;

import java.io.DataInputStream;
import java.io.IOException;

public interface IUpdatableTile
{
    public void onUpdatePacket(DataInputStream data) throws IOException;

    public int getX();

    public int getY();

    public int getZ();
}
