package railcraft.common.util.network;

import railcraft.common.api.INetworkEntity;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.Entity;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import forge.DimensionManager;
import railcraft.Railcraft;

public class PacketGuiReturn extends RailcraftPacket
{

    private INetworkEntity obj;

    public PacketGuiReturn()
    {
        super();
    }

    public PacketGuiReturn(INetworkEntity obj)
    {
        this();
        this.obj = obj;
    }

    @Override
    public void writeData(DataOutputStream data) throws IOException
    {
        data.writeByte(obj.getWorld().worldProvider.dimension);
        if(obj instanceof TileEntity) {
            TileEntity tile = (TileEntity)obj;
            data.writeBoolean(true);
            data.writeInt(tile.x);
            data.writeInt(tile.y);
            data.writeInt(tile.z);
        } else if(obj instanceof Entity) {
            Entity entity = (Entity)obj;
            data.writeBoolean(false);
            data.writeInt(entity.id);
        } else {
            return;
        }
        obj.writePacketData(data);
    }

    @Override
    public void readData(DataInputStream data) throws IOException
    {
        byte dim = data.readByte();
        World world = DimensionManager.getWorld(dim);
        boolean tileReturn = data.readBoolean();
        if(tileReturn) {
            int x = data.readInt();
            int y = data.readInt();
            int z = data.readInt();

            TileEntity t = world.getTileEntity(x, y, z);

            if(t instanceof INetworkEntity) {
                ((INetworkEntity)t).readPacketData(data);
            }

        } else {
            int entityId = data.readInt();
            Entity entity = Railcraft.getEntity(world, entityId);

            if(entity instanceof INetworkEntity) {
                ((INetworkEntity)entity).readPacketData(data);
            }
        }
    }

    @Override
    public int getID()
    {
        return GUI_RETURN;
    }
}
