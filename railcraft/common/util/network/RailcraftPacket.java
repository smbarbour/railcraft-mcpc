package railcraft.common.util.network;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.Packet;
import net.minecraft.server.Packet250CustomPayload;

public abstract class RailcraftPacket
{

    public final static String CHANNEL_NAME = "Railcraft";
    public final static int TILE_ENTITY = 0;
    public final static int GUI_RETURN = 1;
    public final static int TILE_UPDATE = 2;

    public Packet getPacket()
    {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        DataOutputStream data = new DataOutputStream(bytes);
        try {
            data.writeByte(getID());
            writeData(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Packet250CustomPayload pkt = new Packet250CustomPayload();
        pkt.tag = CHANNEL_NAME;
        pkt.data = bytes.toByteArray();
        pkt.length = pkt.data.length;
        return pkt;
    }

    public abstract void writeData(DataOutputStream data) throws IOException;

    public abstract void readData(DataInputStream data) throws IOException;

    public abstract int getID();

    public String toString(boolean full)
    {
        return toString();
    }

    @Override
    public String toString()
    {
        return getID() + " " + getClass().getSimpleName();
    }
}
