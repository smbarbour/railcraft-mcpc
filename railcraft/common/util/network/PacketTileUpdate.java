package railcraft.common.util.network;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import railcraft.Railcraft;

public class PacketTileUpdate extends RailcraftPacket
{

    private IUpdatableTile tile;
    private ByteArrayOutputStream bytes;
    private DataOutputStream data;

    public PacketTileUpdate()
    {
        super();
    }

    public PacketTileUpdate(IUpdatableTile tile)
    {
        this.tile = tile;
    }

    public DataOutputStream getDataStream()
    {
        if(data == null) {
            bytes = new ByteArrayOutputStream();
            data = new DataOutputStream(bytes);
        }
        return data;
    }

    @Override
    public void writeData(DataOutputStream data) throws IOException
    {
        data.writeInt(tile.getX());
        data.writeInt(tile.getY());
        data.writeInt(tile.getZ());
        data.write(bytes.toByteArray());
    }

    @Override
    public void readData(DataInputStream data) throws IOException
    {
        World world = Railcraft.getWorld();
        int x = data.readInt();
        int y = data.readInt();
        int z = data.readInt();

        TileEntity t = world.getTileEntity(x, y, z);

        if(t instanceof IUpdatableTile) {
            ((IUpdatableTile)t).onUpdatePacket(data);
        }
    }

    @Override
    public int getID()
    {
        return TILE_UPDATE;
    }
}
