package railcraft.common.util.network;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.logging.Level;
import net.minecraft.server.NetworkManager;
import forge.IPacketHandler;
import railcraft.Railcraft;

public class RailcraftPacketHandler implements IPacketHandler
{

    @Override
    public void onPacketData(NetworkManager network, String channel, byte[] bytes)
    {
        DataInputStream data = new DataInputStream(new ByteArrayInputStream(bytes));
        try {
            RailcraftPacket pkt = null;

            byte packetID = data.readByte();

//            System.out.println("Packet Received: " + packetID);

            switch (packetID) {
                case RailcraftPacket.TILE_ENTITY:
                    pkt = new PacketTileEntity();
                    pkt.readData(data);
                    break;
                case RailcraftPacket.GUI_RETURN:
                    pkt = new PacketGuiReturn();
                    pkt.readData(data);
                    break;
                case RailcraftPacket.TILE_UPDATE:
                    pkt = new PacketTileUpdate();
                    pkt.readData(data);
                    break;
            }
        } catch (IOException e) {
            Railcraft.log(Level.SEVERE, "Exception in PacketHandler.onPacketData {0}", e);
            e.printStackTrace();
        }
    }
}
