package railcraft.common.util.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.Block;
import net.minecraft.server.Packet;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import railcraft.Railcraft;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftTileEntity;
import railcraft.common.api.tracks.ITrackInstance;
import railcraft.common.tracks.TileTrack;
import railcraft.common.api.tracks.TrackRegistry;

public class PacketTileEntity extends RailcraftPacket
{

    private RailcraftTileEntity tile;

    public PacketTileEntity()
    {
        super();
    }

    public PacketTileEntity(RailcraftTileEntity tile)
    {
        this.tile = tile;
//        System.out.println("Created Tile Packet: " + tile.getClass().getSimpleName());
    }

    @Override
    public Packet getPacket()
    {
        Packet pkt = super.getPacket();
        pkt.lowPriority = true;
        return pkt;
    }

    @Override
    public void writeData(DataOutputStream data) throws IOException
    {
        data.writeInt(tile.x);
        data.writeInt(tile.y);
        data.writeInt(tile.z);
        data.writeInt(tile.getId());
        tile.writePacketData(data);
    }

    @Override
    public void readData(DataInputStream data) throws IOException
    {
        World world = Railcraft.getWorld();
        int x = data.readInt();
        int y = data.readInt();
        int z = data.readInt();
        int id = data.readInt();

        TileEntity te = world.getTileEntity(x, y, z);

        if(te instanceof RailcraftTileEntity) {
            this.tile = (RailcraftTileEntity)te;
            if(this.tile.getId() != id) {
                this.tile = null;
            }
        }else{
            this.tile = null;
        }

        if(this.tile == null) {
            int blockId = world.getTypeId(x, y, z);
            Block block = RailcraftBlocks.getBlockTrack();
            if(block != null && block.id == blockId) {
                ITrackInstance track = TrackRegistry.getTrackSpec(id).createInstanceFromSpec();
                this.tile = new TileTrack(track);
                world.setTileEntity(x, y, z, this.tile);
            } else {
                world.q(x, y, z);
                te = world.getTileEntity(x, y, z);
                if(te instanceof RailcraftTileEntity) {
                    this.tile = (RailcraftTileEntity)te;
                }
            }
        }

        if(this.tile == null) {
            return;
        }

        System.out.println("Class: " + tile.getClass().getSimpleName());
        this.tile.readPacketData(data);
    }

    @Override
    public int getID()
    {
        return TILE_ENTITY;
    }
}
