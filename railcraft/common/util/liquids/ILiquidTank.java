package railcraft.common.util.liquids;

import buildcraft.api.ILiquidContainer;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public interface ILiquidTank extends ILiquidContainer
{

    public boolean canExtractLiquid();
}
