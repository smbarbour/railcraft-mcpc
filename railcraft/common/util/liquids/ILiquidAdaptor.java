package railcraft.common.util.liquids;

import forestry.api.liquids.LiquidContainer;
import java.util.List;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
interface ILiquidAdaptor
{

    public List<LiquidContainer> getLiquidContainers();

    public void registerContainer(LiquidContainer container);
}
