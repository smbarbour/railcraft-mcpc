package railcraft.common.util.liquids;

import forestry.api.liquids.LiquidContainer;
import forestry.api.liquids.LiquidStack;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.Block;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class RailcraftLiquidAdapter implements ILiquidAdaptor
{

    private final List<LiquidContainer> liquids = new ArrayList<LiquidContainer>();
    private static RailcraftLiquidAdapter instance;

    public static RailcraftLiquidAdapter getInstance()
    {
        if(instance == null) {
            instance = new RailcraftLiquidAdapter();
        }
        return instance;
    }

    private RailcraftLiquidAdapter()
    {
        liquids.add(new LiquidContainer(new LiquidStack(Block.STATIONARY_WATER, LiquidManager.BUCKET_VOLUME), new ItemStack(Item.WATER_BUCKET), new ItemStack(Item.BUCKET), true));
        liquids.add(new LiquidContainer(new LiquidStack(Block.STATIONARY_LAVA, LiquidManager.BUCKET_VOLUME), new ItemStack(Item.LAVA_BUCKET), new ItemStack(Item.BUCKET), true));
    }

    @Override
    public List<LiquidContainer> getLiquidContainers()
    {
        return liquids;
    }

    @Override
    public void registerContainer(LiquidContainer container)
    {
        liquids.add(container);
    }
}
