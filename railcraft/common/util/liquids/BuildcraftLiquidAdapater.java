package railcraft.common.util.liquids;

import forestry.api.liquids.LiquidContainer;
import forestry.api.liquids.LiquidStack;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class BuildcraftLiquidAdapater implements ILiquidAdaptor
{

    private static BuildcraftLiquidAdapater instance;

    public static BuildcraftLiquidAdapater getInstance()
    {
        if(instance == null) {
            instance = new BuildcraftLiquidAdapater();
        }
        return instance;
    }

    @Override
    public List<LiquidContainer> getLiquidContainers()
    {
        List<LiquidContainer> containers = new ArrayList<LiquidContainer>();
        for(Object liquidData : getLiquidData()) {

            int liquidId = 0;
            try {
                Field liquidIdField = liquidData.getClass().getField("liquidId");
                liquidId = ((Integer)liquidIdField.get(liquidData));
            } catch (Exception ex) {
                continue;
            }

            ItemStack filled = null;
            try {
                Field filledField = liquidData.getClass().getField("filled");
                filled = ((ItemStack)filledField.get(liquidData)).cloneItemStack();
            } catch (Exception ex) {
            }
            if(filled == null) {
                try {
                    Field filledField = liquidData.getClass().getField("filledBucketId");
                    filled = new ItemStack((Integer)filledField.get(liquidData), 1, 0);
                } catch (Exception ex) {
                    continue;
                }
            }

            ItemStack container = null;
            try {
                Field containerField = liquidData.getClass().getField("container");
                container = ((ItemStack)containerField.get(liquidData)).cloneItemStack();
            } catch (Exception ex) {
                continue;
            }
            if(container == null) {
                container = new ItemStack(Item.BUCKET);
            }

            if(liquidId <= 0 || filled == null || container == null) {
                continue;
            }

            containers.add(new LiquidContainer(new LiquidStack(liquidId, LiquidManager.BUCKET_VOLUME), filled, container, container.id == Item.BUCKET.id));
        }
        return containers;
    }

    private List getLiquidData()
    {
        try {
            Class api = Class.forName("buildcraft.api.BuildCraftAPI");

            if(api == null) {
                api = Class.forName("buildcraft.api.API");
            }

            if(api != null) {
                Field[] fields = api.getFields();
                for(Field f : fields) {
                    if(f.getName().equals("liquids")) {
                        Object obj = f.get(null);
                        if(obj instanceof List) {
                            return (List)obj;
                        }
                    }
                }
            }
        } catch (Exception ex) {
        }
        return new ArrayList();
    }

    @Override
    public void registerContainer(LiquidContainer container)
    {
        try {
            Class liquidData = Class.forName("buildcraft.api.LiquidData");
            Constructor constructor = null;
            try {
                constructor = liquidData.getConstructor(int.class, int.class, ItemStack.class, ItemStack.class);
                Object inst = constructor.newInstance(container.liquid.itemID, 0, container.filled, container.empty);
                getLiquidData().add(inst);
            } catch (Exception ex) {
                constructor = liquidData.getConstructor(int.class, int.class);
                Object inst = constructor.newInstance(container.liquid.itemID, container.filled.id);
                getLiquidData().add(inst);
            }
        } catch (Exception ex) {
        }
    }
}
