package railcraft.common.util.liquids;

import forestry.api.core.ItemInterface;
import forestry.api.liquids.LiquidContainer;
import forestry.api.liquids.LiquidStack;
import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import net.minecraft.server.Block;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import forge.Configuration;
import forge.Property;
import railcraft.Railcraft;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public final class LiquidManager implements ILiquidAdaptor
{

    public static final int BUCKET_VOLUME = 1000;
    private static final String FILTER_CATEGORY = "filters";
    private static LiquidManager instance;
    private List<ILiquidAdaptor> adapters = new ArrayList<ILiquidAdaptor>();
    private final Configuration liquidFilters;

    private LiquidManager()
    {
        adapters.add(RailcraftLiquidAdapter.getInstance());
        adapters.add(ForestryLiquidAdapater.getInstance());
        adapters.add(BuildcraftLiquidAdapater.getInstance());

        File file = new File(Railcraft.getConfigDir(), "liquid_filters.cfg");
        liquidFilters = new Configuration(file);
        liquidFilters.categories.put(FILTER_CATEGORY, new TreeMap<String, Property>(new Comparator<String>()
        {

            @Override
            public int compare(String o1, String o2)
            {
                return Integer.valueOf(o1).compareTo(Integer.valueOf(o2));
            }
        }));
        liquidFilters.load();
        liquidFilters.categories.remove(Configuration.CATEGORY_BLOCK);
        liquidFilters.categories.remove(Configuration.CATEGORY_GENERAL);
        liquidFilters.categories.remove(Configuration.CATEGORY_ITEM);

        registerBottle(new LiquidStack(Block.STATIONARY_WATER, BUCKET_VOLUME), new ItemStack(Item.POTION));
    }

    public static LiquidManager getInstance()
    {
        if(instance == null) {
            instance = new LiquidManager();
        }
        return instance;
    }

    public boolean isBucket(ItemStack stack)
    {
        ItemStack bucket = new ItemStack(Item.BUCKET);
        if(bucket.doMaterialsMatch(stack)) {
            return true;
        }
        for(LiquidContainer data : getLiquidContainers()) {
            if(data.filled.doMaterialsMatch(stack) && data.empty.doMaterialsMatch(bucket)) {
                return true;
            }
        }
        return false;
    }

    public boolean isContainer(ItemStack stack)
    {
        for(LiquidContainer data : getLiquidContainers()) {
            if(data.filled.doMaterialsMatch(stack) || data.empty.doMaterialsMatch(stack)) {
                return true;
            }
        }
        return false;
    }

    public boolean isFilledContainer(ItemStack stack)
    {
        for(LiquidContainer data : getLiquidContainers()) {
            if(data.filled.doMaterialsMatch(stack)) {
                return true;
            }
        }
        return false;
    }

    public boolean isEmptyContainer(ItemStack stack)
    {
        for(LiquidContainer data : getLiquidContainers()) {
            if(data.empty.doMaterialsMatch(stack)) {
                return true;
            }
        }
        return false;
    }

    public ItemStack fillLiquidContainer(LiquidStack liquid, ItemStack empty)
    {
        for(LiquidContainer data : getLiquidContainers()) {
            if(data.liquid.isLiquidEqual(liquid) && data.empty.doMaterialsMatch(empty)) {
                return data.filled.cloneItemStack();
            }
        }
        return null;
    }

    public LiquidStack getLiquidInContainer(ItemStack stack)
    {
        for(LiquidContainer data : getLiquidContainers()) {
            if(data.filled.doMaterialsMatch(stack)) {
                return data.liquid;
            }
        }
        return null;
    }

    @Override
    public List<LiquidContainer> getLiquidContainers()
    {
        List<LiquidContainer> liquids = new ArrayList<LiquidContainer>();
        for(ILiquidAdaptor adaptor : adapters) {
            liquids.addAll(adaptor.getLiquidContainers());
        }
        return liquids;
    }

    public int getLiquidFilterId(ItemStack filter)
    {
        if(filter == null) {
            return 0;
        }
        String tag = filter.id + ":" + filter.getData();
        for(Map.Entry<String, Property> entry : liquidFilters.categories.get(FILTER_CATEGORY).entrySet()) {
            if(entry.getValue().value.equals(tag)) {
                return Integer.valueOf(entry.getKey());
            }
        }
        int id = getUnusedFilterId();
        if(id > 0) {
            liquidFilters.getOrCreateProperty(Integer.toString(id), FILTER_CATEGORY, tag);
            liquidFilters.save();
        }
        return id;
    }

    private int getUnusedFilterId()
    {
        for(int id = 1; id < 1024; id++) {
            if(!liquidFilters.categories.get(FILTER_CATEGORY).containsKey(Integer.toString(id))) {
                return id;
            }
        }
        return 0;
    }

    public ItemStack getItemFromFilterId(int id)
    {
        Property prop = liquidFilters.categories.get(FILTER_CATEGORY).get(Integer.toString(id));
        if(prop == null) {
            return null;
        }
        String[] tag = prop.value.split(":");
        ItemStack filter = new ItemStack(Integer.valueOf(tag[0]), 1, Integer.valueOf(tag[1]));
        if(filter.id <= 0 || filter.getItem() == null) {
            liquidFilters.categories.get(FILTER_CATEGORY).remove(Integer.toString(id));
            liquidFilters.save();
            return null;
        }
        return filter;
    }

    public boolean isLiquidEqual(LiquidStack L1, LiquidStack L2)
    {
        if(L1 == null || L2 == null) {
            return false;
        }
        return L1.isLiquidEqual(L2);
    }

    public void registerBucket(LiquidStack liquid, ItemStack filled)
    {
        ItemStack empty = new ItemStack(Item.BUCKET);
        LiquidContainer container = new LiquidContainer(liquid, filled, empty, true);
        registerContainer(container);
    }

    public boolean registerWax(LiquidStack liquid, ItemStack filled)
    {
        ItemStack empty = ItemInterface.getItem("waxCapsule");
        if(empty != null) {
            LiquidContainer container = new LiquidContainer(liquid, filled, empty, false);
            registerContainer(container);
            return true;
        }
        return false;
    }

    public boolean registerRefactory(LiquidStack liquid, ItemStack filled)
    {
        ItemStack empty = ItemInterface.getItem("refractoryEmpty");
        if(empty != null) {
            LiquidContainer container = new LiquidContainer(liquid, filled, empty, false);
            registerContainer(container);
            return true;
        }
        return false;
    }

    public boolean registerCan(LiquidStack liquid, ItemStack filled)
    {
        ItemStack empty = ItemInterface.getItem("canEmpty");
        if(empty != null) {
            LiquidContainer container = new LiquidContainer(liquid, filled, empty, false);
            registerContainer(container);
            return true;
        }
        return false;
    }

    public void registerBottle(LiquidStack liquid, ItemStack filled)
    {
        ItemStack empty = new ItemStack(Item.GLASS_BOTTLE);
        LiquidContainer container = new LiquidContainer(liquid, filled, empty, false);
        registerContainer(container);
    }

    @Override
    public void registerContainer(LiquidContainer container)
    {
        for(ILiquidAdaptor adapter : adapters) {
            adapter.registerContainer(container);
        }
    }
}
