package railcraft.common.util.liquids;

import forestry.api.core.ItemInterface;
import forestry.api.liquids.LiquidContainer;
import forestry.api.recipes.RecipeManagers;
import java.util.List;
import net.minecraft.server.Block;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import railcraft.common.api.InventoryTools;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class ForestryLiquidAdapater implements ILiquidAdaptor
{

    private static ForestryLiquidAdapater instance;

    public static ForestryLiquidAdapater getInstance()
    {
        if(instance == null) {
            instance = new ForestryLiquidAdapater();
        }
        return instance;
    }

    private ForestryLiquidAdapater()
    {
    }

    @Override
    public List<LiquidContainer> getLiquidContainers()
    {
        return forestry.api.liquids.LiquidManager.liquidContainers;
    }

    @Override
    public void registerContainer(LiquidContainer container)
    {
        forestry.api.liquids.LiquidManager.registerLiquidContainer(container);

        if(RecipeManagers.bottlerManager != null) {
            RecipeManagers.bottlerManager.addRecipe(5, container.liquid, container.empty, container.filled);
        }

        ItemStack recycle = null;
        int chance = 0;
        if(InventoryTools.isItemEqual(container.empty, ItemInterface.getItem("canEmpty"))) {
            recycle = ItemInterface.getItem("ingotTin");
            chance = 5;
        } else if(InventoryTools.isItemEqual(container.empty, ItemInterface.getItem("waxCapsule"))) {
            recycle = ItemInterface.getItem("beeswax");
            chance = 10;
        } else if(InventoryTools.isItemEqual(container.empty, ItemInterface.getItem("refractoryEmpty"))) {
            recycle = ItemInterface.getItem("refractoryWax");
            chance = 10;
        } else if(container.empty.id == Item.GLASS_BOTTLE.id) {
            recycle = new ItemStack(Block.GLASS);
            chance = 10;
        }

        if(RecipeManagers.squeezerManager != null && !container.isBucket) {
            if(recycle != null) {
                RecipeManagers.squeezerManager.addRecipe(10, new ItemStack[]{container.filled}, container.liquid, recycle, chance);
            } else {
                RecipeManagers.squeezerManager.addRecipe(10, new ItemStack[]{container.filled}, container.liquid);
            }
        }
    }
}
