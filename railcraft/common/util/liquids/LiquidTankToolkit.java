package railcraft.common.util.liquids;

import forestry.api.liquids.LiquidStack;
import java.lang.reflect.Method;
import net.minecraft.server.Block;
import net.minecraft.server.TileEntity;
import buildcraft.api.ILiquidContainer;
import buildcraft.api.LiquidSlot;
import buildcraft.api.Orientations;

/**
 * This class provide a cohesive interface for the different Buildcraft API versions.
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class LiquidTankToolkit implements ILiquidTank
{

    private ILiquidContainer tank;

    public LiquidTankToolkit(ILiquidContainer c)
    {
        tank = c;
    }

    public int getLiquidQty(int id)
    {
        try {
            LiquidSlot[] slots = tank.getLiquidSlots();
            for(LiquidSlot slot : slots) {
                if(slot.getLiquidId() == id) {
                    return slot.getLiquidQty();
                }
            }
            return 0;
        } catch (NoSuchMethodError e) {
        }
        if(tank.getLiquidId() == id) {
            return tank.getLiquidQuantity();
        }
        return 0;
    }

    public boolean isTankEmpty(int id)
    {
        try {
            LiquidSlot[] slots = tank.getLiquidSlots();
            for(LiquidSlot slot : slots) {
                if(slot.getLiquidId() == id || slot.getLiquidId() == 0) {
                    return slot.getLiquidQty() == 0;
                }
            }
            return false;
        } catch (NoSuchMethodError e) {
        }
        if(tank.getLiquidId() == id || tank.getLiquidId() == 0) {
            return tank.getLiquidQuantity() == 0;
        }
        return false;
    }

    public boolean isTankFull(int id)
    {
        try {
            LiquidSlot[] slots = tank.getLiquidSlots();
            for(LiquidSlot slot : slots) {
                if(slot.getLiquidId() == id) {
                    return slot.getLiquidQty() >= slot.getCapacity();
                }
            }
            return false;
        } catch (NoSuchMethodError e) {
        }
        if(tank.getLiquidId() == id) {
            return tank.getLiquidQuantity() >= tank.getCapacity();
        }
        return false;
    }

    public boolean isTankFull()
    {
        try {
            LiquidSlot[] slots = tank.getLiquidSlots();
            for(LiquidSlot slot : slots) {
                if(slot.getLiquidQty() < slot.getCapacity()) {
                    return false;
                }
            }
            return true;
        } catch (NoSuchMethodError e) {
        }
        return tank.getLiquidQuantity() >= tank.getCapacity();
    }

    public boolean isTankPartiallyFull()
    {
        try {
            LiquidSlot[] slots = tank.getLiquidSlots();
            for(LiquidSlot slot : slots) {
                boolean canFill = canPutLiquid(Orientations.YPos, slot.getLiquidId());
                boolean empty = slot.getLiquidQty() == 0;
                if(canFill && !empty) {
                    return true;
                }
            }
            return false;
        } catch (NoSuchMethodError e) {
        }
        return tank.getLiquidQuantity() > 0 && tank.getLiquidQuantity() < tank.getCapacity();
    }

    public boolean canPutLiquid(Orientations from, int id)
    {
        return fill(from, 1, id, false) > 0;
    }

    @Override
    public int fill(Orientations from, int quantity, int id, boolean doFill)
    {
        return tank.fill(from, quantity, id, doFill);
    }

    @Override
    public int empty(int quantityMax, boolean doEmpty)
    {
        return tank.empty(quantityMax, doEmpty);
    }

    @Override
    public int getLiquidQuantity()
    {
        return tank.getLiquidQuantity();
    }

    @Override
    public int getLiquidId()
    {
        return tank.getLiquidId();
    }

    public LiquidStack getLiquid()
    {
        return new LiquidStack(tank.getLiquidId(), getLiquidQuantity());
    }

    @Override
    public int getCapacity()
    {
        return tank.getCapacity();
    }

    @Override
    public LiquidSlot[] getLiquidSlots()
    {
        return tank.getLiquidSlots();
    }

    @Override
    public boolean canExtractLiquid()
    {
        if(tank instanceof ILiquidTank) {
            return ((ILiquidTank)tank).canExtractLiquid();
        }

        try {
            Class pipeLogicWood = Class.forName("PipeLogicWood");
            Method isExcludedFromExtraction = pipeLogicWood.getMethod("isExcludedFromExtraction", Block.class);
            TileEntity tile = (TileEntity)tank;
            int id = tile.world.getTypeId(tile.x, tile.y, tile.z);
            return !(Boolean)isExcludedFromExtraction.invoke(null, Block.byId[id]);
        } catch (Exception ex) {
        }

        return true;
    }
}
