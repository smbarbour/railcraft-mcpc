package railcraft.common.util.liquids;

import java.util.ArrayList;
import net.minecraft.server.ItemStack;
import forge.ITextureProvider;
import railcraft.common.ItemRailcraft;
import railcraft.common.RailcraftConstants;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class ItemRailcraftLiquid extends ItemRailcraft implements ITextureProvider
{

    private boolean addCreative = true;

    public ItemRailcraftLiquid(int id)
    {
        super(id);
    }

    @Override
    public String getTextureFile()
    {
        return RailcraftConstants.LIQUID_TEXTURE_FILE;
    }

    public void setAddToCreative(boolean add)
    {
        addCreative = add;
    }

    @Override
    public void addCreativeItems(ArrayList itemList)
    {
        if(addCreative) {
            itemList.add(new ItemStack(this));
        }
    }
}
