package railcraft.common.util.liquids;

import forestry.api.liquids.LiquidStack;
import net.minecraft.server.Block;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import forge.MinecraftForge;
import railcraft.common.RailcraftConfig;
import railcraft.common.RailcraftLanguage;
import railcraft.common.api.ItemRegistry;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public abstract class LiquidItems
{

    private static Item itemCreosoteOil;
    private static Item itemCreosoteOilBucket;
    private static Item itemCreosoteOilBottle;
    private static ItemRailcraftLiquid itemCreosoteOilCan;
    private static ItemRailcraftLiquid itemCreosoteOilWax;
    private static ItemRailcraftLiquid itemCreosoteOilRefactory;

    public static ItemStack getCreosoteOil()
    {
        Item item = itemCreosoteOil;
        if(item == null) {
            String tag = "item.creosote.liquid";
            int id = RailcraftConfig.getItemId(tag);

            item = new ItemRailcraftLiquid(id).d(0).a(tag);
            RailcraftLanguage.getInstance().registerItemName(item, tag);

            ItemRegistry.registerItem(tag, new ItemStack(item));

            itemCreosoteOil = item;
        }
        return new ItemStack(item);
    }

    public static LiquidStack getCreosoteOilLiquid(int qty)
    {
        return new LiquidStack(getCreosoteOil().id, qty);
    }

    public static ItemStack getCreosoteOilBucket()
    {
        Item item = itemCreosoteOilBucket;
        if(item == null) {
            String tag = "item.creosote.bucket";
            int id = RailcraftConfig.getItemId(tag);

            item = new ItemRailcraftLiquid(id).d(1).a(tag).e(1).a(Item.BUCKET);
            RailcraftLanguage.getInstance().registerItemName(item, tag);

            LiquidManager.getInstance().registerBucket(new LiquidStack(getCreosoteOil().id, LiquidManager.BUCKET_VOLUME), new ItemStack(item));

            ItemRegistry.registerItem(tag, new ItemStack(item));

            itemCreosoteOilBucket = item;
        }
        return new ItemStack(item);
    }

    public static ItemStack getCreosoteOilBottle()
    {
        return getCreosoteOilBottle(1);
    }

    public static ItemStack getCreosoteOilBottle(int qty)
    {
        Item item = itemCreosoteOilBottle;
        if(item == null) {
            String tag = "item.creosote.bottle";
            int id = RailcraftConfig.getItemId(tag);

            item = new ItemRailcraftLiquid(id).d(2).a(tag);
            RailcraftLanguage.getInstance().registerItemName(item, tag);

            MinecraftForge.addDungeonLoot(new ItemStack(item), RailcraftConfig.getLootChance("creosote"), 4, 16);

            LiquidManager.getInstance().registerBottle(new LiquidStack(getCreosoteOil().id, LiquidManager.BUCKET_VOLUME), new ItemStack(item));

            ItemRegistry.registerItem(tag, new ItemStack(item));

            itemCreosoteOilBottle = item;
        }
        return new ItemStack(item, qty);
    }

    public static ItemStack getCreosoteOilCan()
    {
        return getCreosoteOilCan(1);
    }

    public static ItemStack getCreosoteOilCan(int qty)
    {
        ItemRailcraftLiquid item = itemCreosoteOilCan;
        if(item == null) {
            String tag = "item.creosote.can";
            int id = RailcraftConfig.getItemId(tag);

            item = new ItemRailcraftLiquid(id);
            item.d(3);
            item.a(tag);

            RailcraftLanguage.getInstance().registerItemName(item, tag);

            boolean forestry = LiquidManager.getInstance().registerCan(new LiquidStack(getCreosoteOil().id, LiquidManager.BUCKET_VOLUME), new ItemStack(item));
            item.setAddToCreative(forestry);

            ItemRegistry.registerItem(tag, new ItemStack(item));

            itemCreosoteOilCan = item;
        }
        return new ItemStack(item, qty);
    }

    public static ItemStack getCreosoteOilWax()
    {
        return getCreosoteOilWax(1);
    }

    public static ItemStack getCreosoteOilWax(int qty)
    {
        ItemRailcraftLiquid item = itemCreosoteOilWax;
        if(item == null) {
            String tag = "item.creosote.wax";
            int id = RailcraftConfig.getItemId(tag);

            item = new ItemRailcraftLiquid(id);
            item.d(4);
            item.a(tag);

            RailcraftLanguage.getInstance().registerItemName(item, tag);

            boolean forestry = LiquidManager.getInstance().registerWax(new LiquidStack(getCreosoteOil().id, LiquidManager.BUCKET_VOLUME), new ItemStack(item));
            item.setAddToCreative(forestry);

            ItemRegistry.registerItem(tag, new ItemStack(item));

            itemCreosoteOilWax = item;
        }
        return new ItemStack(item, qty);
    }

    public static ItemStack getCreosoteOilRefactory()
    {
        return getCreosoteOilRefactory(1);
    }

    public static ItemStack getCreosoteOilRefactory(int qty)
    {
        ItemRailcraftLiquid item = itemCreosoteOilRefactory;
        if(item == null) {
            String tag = "item.creosote.refactory";
            int id = RailcraftConfig.getItemId(tag);

            item = new ItemRailcraftLiquid(id);
            item.d(5);
            item.a(tag);

            RailcraftLanguage.getInstance().registerItemName(item, tag);

            boolean forestry = LiquidManager.getInstance().registerRefactory(new LiquidStack(getCreosoteOil().id, LiquidManager.BUCKET_VOLUME), new ItemStack(item));
            item.setAddToCreative(forestry);

            ItemRegistry.registerItem(tag, new ItemStack(item));

            itemCreosoteOilRefactory = item;
        }
        return new ItemStack(item, qty);
    }
}
