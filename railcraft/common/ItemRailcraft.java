package railcraft.common;

import net.minecraft.server.Item;
import forge.ITextureProvider;

public class ItemRailcraft extends Item implements ITextureProvider
{

    public ItemRailcraft(int i)
    {
        super(i);
    }

    @Override
    public String getTextureFile()
    {
        return RailcraftConstants.MAIN_TEXTURE_FILE;
    }
}
