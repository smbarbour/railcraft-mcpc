package railcraft.common.tracks;


import net.minecraft.server.EntityMinecart;
import railcraft.common.api.tracks.ITrackPowered;
import railcraft.common.api.tracks.ITrackLockdown;

public class TrackHoldingTrain extends TrackHolding implements ITrackPowered, ITrackLockdown
{

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.HOLDING_TRAIN;
    }

    @Override
    protected byte getDelayTIme()
    {
        return 10;
    }

    @Override
    protected void checkCart(EntityMinecart cart)
    {
        if(delay > 0) {
            delay = getDelayTIme();
            if(cart != lockedCart || reset == 0) {
                lockedCart = cart;
                setLaunchDirection(cart);
            }
        }
        reset = 20;
    }
}
