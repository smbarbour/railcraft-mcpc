package railcraft.common.tracks;

import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.Material;

public class MaterialElevator extends Material
{

    public MaterialElevator()
    {
        super(MaterialMapColor.h);
    }

    @Override
    public boolean isSolid()
    {
        return true;
    }

    @Override
    public boolean isBuildable()
    {
        return false;
    }

    @Override
    public boolean blocksLight()
    {
        return false;
    }
}
