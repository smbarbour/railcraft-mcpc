package railcraft.common.tracks;

import railcraft.common.api.tracks.ITrackPowered;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityMinecart;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;

public class TrackDisembark extends TrackBaseRailcraft implements ITrackPowered
{

    private static final int TIME_TILL_NEXT_MOUNT = 40;
    private boolean powered = false;

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.DISEMBARK;
    }

    @Override
    public int getTextureIndex()
    {
        if(isPowered()) {
            return getTrackType().getTextureIndex();
        }
        return getTrackType().getTextureIndex() + 1;
    }

    @Override
    public void onMinecartPass(EntityMinecart cart)
    {
        if(powered && cart.canBeRidden() && cart.passenger != null) {
            Entity entity = cart.passenger;
            cart.passenger.setPassengerOf(cart);
            cart.getEntityData().setInt("MountPrevention", TIME_TILL_NEXT_MOUNT);
        }
    }

    @Override
    public boolean isPowered()
    {
        return powered;
    }

    @Override
    public void setPowered(boolean powered)
    {
        this.powered = powered;
    }

    @Override
    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setBoolean("powered", powered);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        powered = nbttagcompound.getBoolean("powered");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(powered);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        boolean p = data.readBoolean();

        if(p != powered) {
            powered = p;
            markBlockNeedsUpdate();
        }
    }
}
