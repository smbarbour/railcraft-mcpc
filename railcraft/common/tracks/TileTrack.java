package railcraft.common.tracks;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import railcraft.common.api.tracks.ITrackTile;
import railcraft.common.api.tracks.TrackRegistry;
import net.minecraft.server.*;
import railcraft.common.RailcraftTileEntity;
import railcraft.common.api.tracks.ITrackInstance;

public class TileTrack extends RailcraftTileEntity implements ITrackTile
{

    public ITrackInstance track;

    public TileTrack()
    {
    }

    public TileTrack(ITrackInstance t)
    {
        track = t;
        track.setTile(this);
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);

        data.setInt("trackId", getId());

        track.writeToNBT(data);
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);

        track = TrackRegistry.getTrackSpec(data.getInt("trackId")).createInstanceFromSpec();
        track.setTile(this);

        track.readFromNBT(data);
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        if(track != null) {
            track.writePacketData(data);
        }
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);
        
        if(track != null) {
            track.readPacketData(data);
        }
    }

    @Override
    public boolean canUpdate()
    {
        return track.canUpdate();
    }

    @Override
    public void q_()
    {
        track.q_();
    }

    @Override
    public int getId()
    {
        return track.getTrackSpec().getTrackId();
    }

    @Override
    public ITrackInstance getTrackInstance()
    {
        track.setTile(this);
        return track;
    }
}
