package railcraft.common.tracks;

import railcraft.common.api.tracks.ITrackReversable;
import railcraft.common.api.tracks.ITrackPowered;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;

public class TrackOneWay extends TrackBaseRailcraft implements ITrackPowered, ITrackReversable
{

    private boolean reversed = false;
    private boolean powered = false;
    private static final double LOSS_FACTOR = 0.49D;

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.ONEWAY;
    }

    @Override
    public int getTextureIndex()
    {
        if(!isPowered()) {
            if(isReversed()) {
                return getTrackType().getTextureIndex() + 3;
            }
            return getTrackType().getTextureIndex() + 1;
        }
        if(isReversed()) {
            return getTrackType().getTextureIndex() + 2;
        }
        return getTrackType().getTextureIndex();
    }

    @Override
    public void onMinecartPass(EntityMinecart cart)
    {
        int meta = tileEntity.k();
        if(isPowered()) {
            if(meta == 1 || meta == 2 || meta == 3) {
                if((isReversed() && cart.motX > 0.0D) || (!isReversed() && cart.motX < 0.0D)) {
                    double distX = cart.locX - (tileEntity.x + 0.5D);
//                    System.out.println("cartX=" + cart.posX + ", railX=" + (i + 0.5D) + ", railDir=" + isReversed());
                    if(!isReversed() && distX < -0.01 || isReversed() && distX > 0.01) {
//                        System.out.println("Setting Position");
                        cart.setPosition(tileEntity.x + 0.5D, cart.locY, cart.locZ);
                    }
//                    System.out.println("mX= " + cart.motionX + ", dist=" + distX);
                    if(!isReversed()) {
                        cart.motX = Math.abs(cart.motX) * LOSS_FACTOR;
                    } else {
                        cart.motX = -Math.abs(cart.motX) * LOSS_FACTOR;
                    }
                }
            } else if(meta == 0 || meta == 4 || meta == 5) {
                if((isReversed() && cart.motZ < 0.0D) || (!isReversed() && cart.motZ > 0.0D)) {
                    double distZ = cart.locZ - (tileEntity.z + 0.5D);
//                    System.out.println("cartZ=" + cart.posZ + ", railZ=" + (k + 0.5D) + ", railDir=" + isReversed());
                    if(isReversed() && distZ < -0.01 || !isReversed() && distZ > 0.01) {
//                        System.out.println("Setting Position");
                        cart.setPosition(cart.locX, cart.locY, tileEntity.z + 0.5D);
                    }
//                    System.out.println("mZ= " + cart.motionZ + ", dist=" + distZ);
                    if(isReversed()) {
                        cart.motZ = Math.abs(cart.motZ) * LOSS_FACTOR;
                    } else {
                        cart.motZ = -Math.abs(cart.motZ) * LOSS_FACTOR;
                    }
                }
            }
        }
    }

    @Override
    public boolean isReversed()
    {
        return reversed;
    }

    @Override
    public void setReversed(boolean reversed)
    {
        this.reversed = reversed;
    }

    @Override
    public boolean isPowered()
    {
        return powered;
    }

    @Override
    public void setPowered(boolean powered)
    {
        this.powered = powered;
    }

    @Override
    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setBoolean("direction", reversed);
        nbttagcompound.setBoolean("powered", powered);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        reversed = nbttagcompound.getBoolean("direction");
        powered = nbttagcompound.getBoolean("powered");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(powered);
        data.writeBoolean(reversed);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        powered = data.readBoolean();
        reversed = data.readBoolean();

        markBlockNeedsUpdate();
    }
}
