package railcraft.common.tracks;

import railcraft.common.tracks.speedcontroller.SpeedControllerReinforced;

public class TrackReinforcedSwitch extends TrackSwitch
{

    public TrackReinforcedSwitch()
    {
        speedController = SpeedControllerReinforced.getInstance();
    }

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.REINFORCED_SWITCH;
    }
}
