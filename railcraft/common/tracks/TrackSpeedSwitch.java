package railcraft.common.tracks;

import railcraft.common.tracks.speedcontroller.SpeedControllerHighSpeed;

public class TrackSpeedSwitch extends TrackSwitch
{

    public TrackSpeedSwitch()
    {
        speedController = SpeedControllerHighSpeed.getInstance();
    }

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.SPEED_SWITCH;
    }
}
