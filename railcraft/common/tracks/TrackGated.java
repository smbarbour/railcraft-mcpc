package railcraft.common.tracks;

import railcraft.common.api.tracks.ITrackPowered;
import railcraft.common.api.tracks.ITrackReversable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;
import railcraft.common.api.tracks.ITrackCustomShape;

// Referenced classes of package net.minecraft.server:
//            TileEntity, NBTTagCompound, World
public class TrackGated extends TrackBaseRailcraft implements ITrackReversable, ITrackPowered, ITrackCustomShape
{

    protected boolean powered;
    protected boolean open;
    protected boolean reversed;

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.GATED;
    }

    @Override
    public int getTextureIndex()
    {
        return getTrackType().getTextureIndex();
    }

    @Override
    public boolean blockActivated(EntityHuman player)
    {
        if(!super.blockActivated(player)) {
            setOpen(!open);
        }
        return true;
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool()
    {
        return AxisAlignedBB.b(tileEntity.x, tileEntity.y, tileEntity.z, tileEntity.x + 1, tileEntity.y + 1, tileEntity.z + 1);
    }

    @Override
    public MovingObjectPosition collisionRayTrace(Vec3D vec3d, Vec3D vec3d1)
    {
        int i = tileEntity.x;
        int j = tileEntity.y;
        int k = tileEntity.z;
        vec3d = vec3d.add(-i, -j, -k);
        vec3d1 = vec3d1.add(-i, -j, -k);
        Vec3D vec3d2 = vec3d.a(vec3d1, 0);
        Vec3D vec3d3 = vec3d.a(vec3d1, 1);
        Vec3D vec3d4 = vec3d.b(vec3d1, 0);
        Vec3D vec3d5 = vec3d.b(vec3d1, 1);
        Vec3D vec3d6 = vec3d.c(vec3d1, 0);
        Vec3D vec3d7 = vec3d.c(vec3d1, 1);
        if(!isVecInsideYZBounds(vec3d2)) {
            vec3d2 = null;
        }
        if(!isVecInsideYZBounds(vec3d3)) {
            vec3d3 = null;
        }
        if(!isVecInsideXZBounds(vec3d4)) {
            vec3d4 = null;
        }
        if(!isVecInsideXZBounds(vec3d5)) {
            vec3d5 = null;
        }
        if(!isVecInsideXYBounds(vec3d6)) {
            vec3d6 = null;
        }
        if(!isVecInsideXYBounds(vec3d7)) {
            vec3d7 = null;
        }
        Vec3D vec3d8 = null;
        if(vec3d2 != null && (vec3d8 == null || vec3d.b(vec3d2) < vec3d.b(vec3d8))) {
            vec3d8 = vec3d2;
        }
        if(vec3d3 != null && (vec3d8 == null || vec3d.b(vec3d3) < vec3d.b(vec3d8))) {
            vec3d8 = vec3d3;
        }
        if(vec3d4 != null && (vec3d8 == null || vec3d.b(vec3d4) < vec3d.b(vec3d8))) {
            vec3d8 = vec3d4;
        }
        if(vec3d5 != null && (vec3d8 == null || vec3d.b(vec3d5) < vec3d.b(vec3d8))) {
            vec3d8 = vec3d5;
        }
        if(vec3d6 != null && (vec3d8 == null || vec3d.b(vec3d6) < vec3d.b(vec3d8))) {
            vec3d8 = vec3d6;
        }
        if(vec3d7 != null && (vec3d8 == null || vec3d.b(vec3d7) < vec3d.b(vec3d8))) {
            vec3d8 = vec3d7;
        }
        if(vec3d8 == null) {
            return null;
        }
        byte byte0 = -1;
        if(vec3d8 == vec3d2) {
            byte0 = 4;
        }
        if(vec3d8 == vec3d3) {
            byte0 = 5;
        }
        if(vec3d8 == vec3d4) {
            byte0 = 0;
        }
        if(vec3d8 == vec3d5) {
            byte0 = 1;
        }
        if(vec3d8 == vec3d6) {
            byte0 = 2;
        }
        if(vec3d8 == vec3d7) {
            byte0 = 3;
        }
        return new MovingObjectPosition(i, j, k, byte0, vec3d8.add(i, j, k));
    }

    private boolean isVecInsideYZBounds(Vec3D vec3d)
    {
        if(vec3d == null) {
            return false;
        } else {
            return vec3d.b >= 0 && vec3d.b <= 1 && vec3d.c >= 0 && vec3d.c <= 1;
        }
    }

    private boolean isVecInsideXZBounds(Vec3D vec3d)
    {
        if(vec3d == null) {
            return false;
        } else {
            return vec3d.a >= 0 && vec3d.a <= 1 && vec3d.c >= 0 && vec3d.c <= 1;
        }
    }

    private boolean isVecInsideXYBounds(Vec3D vec3d)
    {
        if(vec3d == null) {
            return false;
        } else {
            return vec3d.a >= 0 && vec3d.a <= 1 && vec3d.b >= 0 && vec3d.b <= 1;
        }
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool()
    {
        int meta = tileEntity.k();
        if(isGateOpen()) {
            return null;
        }
        if(meta == 0) {
            return AxisAlignedBB.b(tileEntity.x, tileEntity.y, (float)tileEntity.z + 0.375F, tileEntity.x + 1, (float)tileEntity.y + 1.5F, (float)tileEntity.z + 0.625F);
        } else {
            return AxisAlignedBB.b((float)tileEntity.x + 0.375F, tileEntity.y, tileEntity.z, (float)tileEntity.x + 0.625F, (float)tileEntity.y + 1.5F, tileEntity.z + 1);
        }
    }

    @Override
    public boolean canMakeSlopes()
    {
        return false;
    }

    public boolean isOpen()
    {
        return open;
    }

    public void setOpen(boolean open)
    {
        boolean state = isGateOpen();
        this.open = open;
        if(state != isGateOpen()) {
            playSound();
            markBlockNeedsUpdate();
        }
    }

    @Override
    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setBoolean("powered", powered);
        nbttagcompound.setBoolean("open", open);
        nbttagcompound.setBoolean("reversed", reversed);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        powered = nbttagcompound.getBoolean("powered");
        open = nbttagcompound.getBoolean("open");
        reversed = nbttagcompound.getBoolean("reversed");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(powered);
        data.writeBoolean(reversed);
        data.writeBoolean(open);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        setPowered(data.readBoolean());
        setReversed(data.readBoolean());
        setOpen(data.readBoolean());
    }

    @Override
    public boolean isReversed()
    {
        return reversed;
    }

    @Override
    public void setReversed(boolean reversed)
    {
        if(this.reversed != reversed) {
            markBlockNeedsUpdate();
        }
        this.reversed = reversed;
    }

    @Override
    public boolean isPowered()
    {
        return powered;
    }

    @Override
    public void setPowered(boolean powered)
    {
        boolean state = isGateOpen();
        this.powered = powered;
        if(state != isGateOpen()) {
            playSound();
            markBlockNeedsUpdate();
        }
    }

    private void playSound()
    {
        tileEntity.world.triggerEffect(1003, tileEntity.x, tileEntity.y, tileEntity.z, 0);
    }

    public boolean isGateOpen()
    {
        return isPowered() || isOpen();
    }
}
