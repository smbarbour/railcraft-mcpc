package railcraft.common.tracks;

import net.minecraft.server.*;
import forge.*;
import railcraft.common.RailcraftBlocks;
import railcraft.common.api.BukkitHandlers;
import railcraft.common.api.tracks.ITrackInstance;
import railcraft.common.api.tracks.ITrackItem;
import railcraft.common.api.tracks.TrackSpec;
import railcraft.common.api.tracks.TrackRegistry;

public class ItemTrack extends ItemBlock implements ITextureProvider, ITrackItem
{

    private String textureFile = "";

    public ItemTrack(int id)
    {
        super(id);
        d(9);
        setMaxDurability(0);
        a(true);
        a("railcraftTrack");
    }

    public int getIconFromDamage(int damage)
    {
        TrackSpec trackSpec = TrackRegistry.getTrackSpec(damage);
        textureFile = trackSpec.getTextureFile();
        return trackSpec.getTextureIndex();
    }

    @Override
    public int filterData(int i)
    {
        return i;
    }

    @Override
    public String a(ItemStack stack)
    {
        return TrackRegistry.getTrackSpec(stack.getData()).getTrackTag();
    }

    @Override
    public String getTextureFile()
    {
        return textureFile;
    }

    @Override
    public int getPlacedBlockId()
    {
        return RailcraftBlocks.getBlockTrack().id;
    }

    @Override
    public boolean isPlacedTileEntity(ItemStack stack, TileEntity tile)
    {
        if(tile instanceof TileTrack) {
            TileTrack track = (TileTrack)tile;
            if(track.getTrackInstance().getTrackSpec().getTrackId() == stack.getData()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean placeTrack(ItemStack stack, World world, int i, int j, int k, String actor)
    {
        return placeTrack(stack, world, i, j, k, 1, actor);
    }

    private boolean placeTrack(ItemStack stack, World world, int i, int j, int k, int side, String actor)
    {
        Block blockTrack = RailcraftBlocks.getBlockTrack();
        if(blockTrack == null) {
            return false;
        }
        if(j >= world.getHeight() - 1) {
            return false;
        }
    	ItemStack tempStack = new ItemStack(blockTrack, 1);
        TrackSpec spec = TrackRegistry.getTrackSpec(stack.getData());
        ITrackInstance track = spec.createInstanceFromSpec();
        if(world.mayPlace(blockTrack.id, i, j, k, true, side) || track.canPlaceRailAt(world, i, j, k)) {
            boolean placed = BukkitHandlers.placeBlockWithEvent(world.getServer(), world, tempStack, i, j, k, i, j, k, false, BukkitHandlers.getEntityByName(world, actor)); //world.setTypeId(i, j, k, blockTrack.id);
            //System.out.println("placeBlockWithEvent returned: " + placed + " - ID at (" + i + ", " + j + ", " + k + ") = " + world.getTypeId(i, j, k));
            //System.out.println("Block placement attempted");
            if(placed) {
                if(world.getTypeId(i, j, k) == blockTrack.id) {
                    TileTrack tile = new TileTrack(track);
                    world.setTileEntity(i, j, k, tile);
                    //System.out.println("Calling placement code with ID = " + mod_Railcraft.advancedRailId);
                    blockTrack.postPlace(world, i, j, k, side);
                }
                world.makeSound((float)i + 0.5F, (float)j + 0.5F, (float)k + 0.5F, blockTrack.stepSound.getName(), (blockTrack.stepSound.getVolume1() + 1.0F) / 2.0F, blockTrack.stepSound.getVolume2() * 0.8F);
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean interactWith(ItemStack stack, EntityHuman player, World world, int i, int j, int k, int side)
    {
        if(stack.count <= 0) {
            return false;
        }

        int id = world.getTypeId(i, j, k);
        if(id == Block.SNOW.id) {
            side = 1;
        } else if(id != Block.VINE.id && id != Block.LONG_GRASS.id && id != Block.DEAD_BUSH.id) {
            if(side == 0) {
                j--;
            }
            if(side == 1) {
                j++;
            }
            if(side == 2) {
                k--;
            }
            if(side == 3) {
                k++;
            }
            if(side == 4) {
                i--;
            }
            if(side == 5) {
                i++;
            }
        }

        if(player != null && !player.d(i, j, k)) {
            return false;
        }

        boolean success = placeTrack(stack, world, i, j, k, side, player.name);
        if(success) {
            Block blockTrack = RailcraftBlocks.getBlockTrack();
            if(player != null) {
                blockTrack.postPlace(world, i, j, k, player);
            }
            stack.count--;
        }
        return success;
    }
}
