package railcraft.common.tracks;

import net.minecraft.server.Entity;
import railcraft.common.tracks.speedcontroller.SpeedControllerReinforced;

public class TrackReinforced extends TrackBaseRailcraft
{

    public static final float RESISTANCE = 2000f;

    public TrackReinforced()
    {
        speedController = SpeedControllerReinforced.getInstance();
    }

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.REINFORCED;
    }

    @Override
    public int getTextureIndex()
    {
        int meta = tileEntity.k();
        if(meta >= 6) {
            return getTrackType().getTextureIndex() - 16;
        }
        return getTrackType().getTextureIndex();
    }

    @Override
    public boolean isFlexibleRail()
    {
        return true;
    }

    @Override
    public float getExplosionResistance(double srcX, double srcY, double srcZ, Entity exploder)
    {
        return RESISTANCE;
    }
}
