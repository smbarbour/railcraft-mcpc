package railcraft.common.tracks;

import railcraft.common.api.tracks.ITrackPowered;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;
import railcraft.common.RailcraftConfig;
import railcraft.GuiHandler;
import railcraft.common.EnumGui;
import railcraft.common.SafeNBTWrapper;
import railcraft.common.api.ICrowbar;

public class TrackLauncher extends TrackBaseRailcraft implements ITrackPowered
{

    private boolean powered = false;
    private byte launchForce = 5;
    public static final int MIN_LAUNCH_FORCE = 5;
    private static final float LAUNCH_THRESHOLD = 0.01f;

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.LAUNCHER;
    }

    @Override
    public int getTextureIndex()
    {
        if(!isPowered()) {
            return getTrackType().getTextureIndex() + 1;
        }
        return getTrackType().getTextureIndex();
    }

    @Override
    public boolean blockActivated(EntityHuman player)
    {
        ItemStack current = player.U();
        if(current != null && current.getItem() instanceof ICrowbar) {
            GuiHandler.openGui(EnumGui.RAIL_LAUNCHER, player, tileEntity.world, tileEntity.x, tileEntity.y, tileEntity.z);
            if(current.d()) {
                current.damage(1, player);
            }
            return true;
        }
        return false;
    }

    @Override
    public void onMinecartPass(EntityMinecart cart)
    {
        if(isPowered()) {
            if(Math.abs(cart.motX) > LAUNCH_THRESHOLD) {
                cart.motX = Math.copySign(0.6f, cart.motX);
            }
            if(Math.abs(cart.motZ) > LAUNCH_THRESHOLD) {
                cart.motZ = Math.copySign(0.6f, cart.motZ);
            }
            cart.setMaxSpeedAirLateral(0.6f);
            cart.setMaxSpeedAirVertical(0.5f);
            cart.setDragAir(0.99999);
            cart.motY = getLaunchForce() * 0.1;
            cart.getEntityData().setInt("Launched", 1);
            cart.setCanUseRail(false);
            cart.move(cart.motX, 1.5, cart.motZ);
        }
    }

    @Override
    public boolean isPowered()
    {
        return powered;
    }

    @Override
    public void setPowered(boolean powered)
    {
        this.powered = powered;
    }

    @Override
    public void writeToNBT(NBTTagCompound data)
    {
        super.writeToNBT(data);
        data.setBoolean("powered", powered);
        data.setByte("force", getLaunchForce());
    }

    @Override
    public void readFromNBT(NBTTagCompound data)
    {
        super.readFromNBT(data);

        powered = data.getBoolean("powered");

        SafeNBTWrapper safe = new SafeNBTWrapper(data);

        setLaunchForce(safe.getByte("force"));
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(powered);
        data.writeByte(launchForce);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        powered = data.readBoolean();
        launchForce = data.readByte();

        markBlockNeedsUpdate();
    }

    public byte getLaunchForce()
    {
        return launchForce;
    }

    public void setLaunchForce(int force)
    {
        force = Math.max(force, MIN_LAUNCH_FORCE);
        force = Math.min(force, RailcraftConfig.getLaunchRailMaxForce());
        launchForce = (byte)force;
    }
}
