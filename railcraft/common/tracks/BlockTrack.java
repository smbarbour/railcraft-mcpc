package railcraft.common.tracks;

import cpw.mods.fml.common.registry.FMLRegistry;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import net.minecraft.server.*;
import forge.*;
import railcraft.Railcraft;
import railcraft.common.RailcraftConstants;
import railcraft.common.api.tracks.ITrackInstance;
import railcraft.common.api.tracks.ITrackCustomShape;
import railcraft.common.api.tracks.TrackRegistry;
import railcraft.common.api.tracks.TrackSpec;

public class BlockTrack extends BlockMinecartTrack implements ISpecialResistance
{

    protected final int renderType;

    public BlockTrack(int blockID, int modelID)
    {
        super(blockID, 255, false);
        renderType = modelID;
        a(0.0F, 0.0F, 0.0F, 1.0F, 0.125F, 1.0F);
        b(3.5F);
        c(1.05F);
        a(i);
        j();
        FMLRegistry.registerTileEntity(TileTrack.class, "RailcraftTrackTile");
    }

    @Override
    @SuppressWarnings("unchecked")
    public void addCreativeItems(ArrayList itemList)
    {
        Set<TrackSpec> railcraftSpecs = new HashSet<TrackSpec>();
        for(EnumTrack t : EnumTrack.getCreativeList()) {
            railcraftSpecs.add(TrackRegistry.getTrackSpec(t.getId()));
            if(t.isEnabled()) {
                itemList.add(t.getItem());
            }
        }

        Set<TrackSpec> specs = TrackRegistry.getTrackSpecs();
        specs.removeAll(railcraftSpecs);
        for(TrackSpec spec : specs) {
            itemList.add(new ItemStack(this, 1, spec.getTrackId()));
        }
    }

    @Override
    public int c()
    {
        return renderType;
    }

    @Override
    public boolean hasTileEntity(int metadata)
    {
        return true;
    }

    @Override
    public int g()
    {
        return 0;
    }

    @Override
    public AxisAlignedBB e(World world, int i, int j, int k)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileTrack) {
            ITrackInstance track = ((TileTrack)tile).getTrackInstance();
            if(track instanceof ITrackCustomShape) {
                return ((ITrackCustomShape)track).getCollisionBoundingBoxFromPool();
            }
        }
        return null;
    }

    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int i, int j, int k)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileTrack) {
            ITrackInstance track = ((TileTrack)tile).getTrackInstance();
            if(track instanceof ITrackCustomShape) {
                return ((ITrackCustomShape)track).getSelectedBoundingBoxFromPool();
            }
        }
        return AxisAlignedBB.b((double)i + minX, (double)j + minY, (double)k + minZ, (double)i + maxX, (double)j + maxY, (double)k + maxZ);
    }

    @Override
    public boolean a()
    {
        return false;
    }

    @Override
    public MovingObjectPosition a(World world, int i, int j, int k, Vec3D vec3d, Vec3D vec3d1)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileTrack) {
            ITrackInstance track = ((TileTrack)tile).getTrackInstance();
            if(track instanceof ITrackCustomShape) {
                return ((ITrackCustomShape)track).collisionRayTrace(vec3d, vec3d1);
            }
        }
        return super.a(world, i, j, k, vec3d, vec3d1);
    }

    @Override
    public void updateShape(IBlockAccess iblockaccess, int i, int j, int k)
    {
        int l = iblockaccess.getData(i, j, k);
        if(l >= 2 && l <= 5) {
            a(0.0F, 0.0F, 0.0F, 1.0F, 0.625F, 1.0F);
        } else {
            a(0.0F, 0.0F, 0.0F, 1.0F, 0.125F, 1.0F);
        }
    }

    @Override
    public boolean canPlace(World world, int i, int j, int k)
    {
        if(world.isBlockSolidOnSide(i, j - 1, k, 1) && !BlockMinecartTrack.g(world, i, j + 1, k)) {
            return true;
        }
        return false;
    }

    public boolean b()
    {
        return false;
    }

    @Override
    public void onMinecartPass(World world, EntityMinecart cart, int i, int j, int k)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileTrack) {
            ((TileTrack)tile).getTrackInstance().onMinecartPass(cart);
        }
    }

    @Override
    public int getBasicRailMetadata(IBlockAccess world, EntityMinecart cart, int i, int j, int k)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileTrack) {
            return ((TileTrack)tile).getTrackInstance().getBasicRailMetadata(cart);
        }
        return world.getData(i, j, k);
    }

    @Override
    public float getRailMaxSpeed(World world, EntityMinecart cart, int i, int j, int k)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileTrack) {
            return ((TileTrack)tile).getTrackInstance().getRailMaxSpeed(cart);
        }
        return 0.4f;
    }

    @Override
    public boolean interact(World world, int i, int j, int k, EntityHuman player)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileTrack) {
            return ((TileTrack)tile).getTrackInstance().blockActivated(player);
        }
        return false;
    }

    @Override
    public boolean isFlexibleRail(World world, int i, int j, int k)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileTrack) {
            return ((TileTrack)tile).getTrackInstance().isFlexibleRail();
        }
        return false;
    }

    @Override
    public boolean canMakeSlopes(World world, int i, int j, int k)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileTrack) {
            return ((TileTrack)tile).getTrackInstance().canMakeSlopes();
        }
        return true;
    }

    @Override
    public boolean hasPowerBit(World world, int i, int j, int k)
    {
        return false;
    }

    @Override
    public String getTextureFile()
    {
        return RailcraftConstants.TRACK_TEXTURE_FILE;
    }

    @Override
    public int a(int side, int meta)
    {
        return textureId;
    }

    public int getBlockTexture(IBlockAccess world, int i, int j, int k, int side)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileTrack) {
            return ((TileTrack)tile).getTrackInstance().getTextureIndex();

        }
        return 255;
    }

    @Override
    public ArrayList<ItemStack> getBlockDropped(World world, int i, int j, int k, int md, int fortune)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        ArrayList<ItemStack> items = new ArrayList<ItemStack>();
        if(tile instanceof TileTrack) {
            items.add(new ItemStack(this, 1, ((TileTrack)tile).getTrackInstance().getTrackSpec().getTrackId()));
        } else {
            Railcraft.log(Level.WARNING, "Rail Tile was invalid when harvesting rail");
            items.add(new ItemStack(Block.RAILS));
        }
        return items;
    }

    @Override
    public int quantityDropped(int meta, int fortune, Random random)
    {
        return 1;
    }

    @Override
    public int getDropType(int i, Random random, int j)
    {
        Railcraft.log(Level.WARNING, "Wrong function called when harvesting rail");
        return Block.RAILS.getDropType(i, random, j);
    }

    public TileEntity getBlockEntity(int md)
    {
        return null;
    }

    // Determine direction here
    @Override
    public void postPlace(World world, int i, int j, int k, EntityLiving entityliving)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileTrack) {
            ((TileTrack)tile).getTrackInstance().onBlockPlacedBy(entityliving);
        }
    }

    @Override
    public void postPlace(World world, int i, int j, int k, int side)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileTrack) {
            ((TileTrack)tile).getTrackInstance().onBlockPlaced(side);
        }
    }

    @Override
    public void a(World world, EntityHuman entityplayer, int i, int j, int k, int l)
    {
    }

    @Override
    public boolean removeBlockByPlayer(World world, EntityHuman player, int i, int j, int k)
    {
        player.a(StatisticList.C[id], 1);
        player.c(0.025F);
        if(Railcraft.gameIsHost() && !player.abilities.canInstantlyBuild) {
            b(world, i, j, k, 0, 0);
        }
        return world.setTypeId(i, j, k, 0);
    }

    @Override
    public void remove(World world, int i, int j, int k)
    {
        super.remove(world, i, j, k);
        world.q(i, j, k);
    }

    @Override
    public void doPhysics(World world, int i, int j, int k, int id)
    {
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        TileEntity t = world.getTileEntity(i, j, k);
        if(t instanceof TileTrack) {
            TileTrack tile = (TileTrack)t;
            tile.getTrackInstance().onNeighborBlockChange(id);
        }
    }

    @Override
    public float getSpecialExplosionResistance(World world, int i, int j, int k, double srcX, double srcY, double srcZ, Entity exploder)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileTrack) {
            return ((TileTrack)tile).getTrackInstance().getExplosionResistance(srcX, srcY, srcZ, exploder);
        }
        return a(exploder);
    }
}