package railcraft.common.tracks;

import railcraft.common.tracks.speedcontroller.SpeedController;
import net.minecraft.server.EntityMinecart;
import railcraft.common.api.tracks.TrackSpec;
import railcraft.common.api.tracks.TrackInstanceBase;
import railcraft.common.api.tracks.TrackRegistry;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public abstract class TrackBaseRailcraft extends TrackInstanceBase
{

    public SpeedController speedController;

    public abstract EnumTrack getTrackType();

    @Override
    public float getRailMaxSpeed(EntityMinecart cart)
    {
        if(speedController == null) {
            speedController = SpeedController.getInstance();
        }
        return speedController.getMaxSpeed(this, cart);
    }

    @Override
    public TrackSpec getTrackSpec()
    {
        return TrackRegistry.getTrackSpec(getTrackType().getId());
    }

    public int getPowerPropagation()
    {
        return 0;
    }
}
