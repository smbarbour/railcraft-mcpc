package railcraft.common.tracks;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import net.minecraft.server.*;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftConfig;
import railcraft.common.RailcraftPartItems;
import railcraft.common.RailcraftToolItems;
import railcraft.common.api.tracks.TrackRegistry;
import railcraft.common.api.tracks.TrackSpec;
import railcraft.common.modules.RailcraftModuleManager;
import railcraft.common.modules.RailcraftModuleManager.Module;
import railcraft.common.tracks.old.TileRailBoarding;
import railcraft.common.tracks.old.TileRailBoardingTrain;
import railcraft.common.tracks.old.TileRailControl;
import railcraft.common.tracks.old.TileRailCoupler;
import railcraft.common.tracks.old.TileRailDecoupler;
import railcraft.common.tracks.old.TileRailDisembark;
import railcraft.common.tracks.old.TileRailGated;
import railcraft.common.tracks.old.TileRailGatedOneWay;
import railcraft.common.tracks.old.TileRailHolding;
import railcraft.common.tracks.old.TileRailHoldingTrain;
import railcraft.common.tracks.old.TileRailJunction;
import railcraft.common.tracks.old.TileRailLauncher;
import railcraft.common.tracks.old.TileRailOneway;
import railcraft.common.tracks.old.TileRailPriming;
import railcraft.common.tracks.old.TileRailSlow;
import railcraft.common.tracks.old.TileRailSlowBooster;
import railcraft.common.tracks.old.TileRailSlowJunction;
import railcraft.common.tracks.old.TileRailSlowSwitch;
import railcraft.common.tracks.old.TileRailSpeed;
import railcraft.common.tracks.old.TileRailSpeedBoost;
import railcraft.common.tracks.old.TileRailSpeedSwitch;
import railcraft.common.tracks.old.TileRailSpeedTransition;
import railcraft.common.tracks.old.TileRailSuspended;
import railcraft.common.tracks.old.TileRailSwitch;

public enum EnumTrack
{

    BOARDING(Module.TRACKS, 0, "Boarding Track", "boarding", "RCBoardingRailTile", 8, TileRailBoarding.class, TrackBoarding.class),
    HOLDING(Module.TRACKS, 4, "Holding Track", "holding", "RCHoldingRailTile", 8, TileRailHolding.class, TrackHolding.class),
    ONEWAY(Module.TRACKS, 1 * 16, "One-Way Track", "oneway", "RCOneWayRailTile", 8, TileRailOneway.class, TrackOneWay.class),
    CONTROL(Module.TRACKS, 100, "Control Track", "control", "RCControlRailTile", 16, TileRailControl.class, TrackControl.class),
    LAUNCHER(Module.EXTRAS, 36, "Launcher Track", "launcher", "RCLauncherRailTile", 1, TileRailLauncher.class, TrackLauncher.class),
    PRIMING(Module.EXTRAS, 68, "Priming Track", "priming", "RCPrimingRailTile", 8, TileRailPriming.class, TrackPriming.class),
    JUNCTION(Module.TRACKS, 7 + 32, "Junction Track", "junction", "RCJunctionRailTile", 8, TileRailJunction.class, TrackJunction.class),
    SWITCH(Module.SIGNALS, 3 * 16, "Switch Track", "switch", "RCSwitchRailTile", 8, TileRailSwitch.class, TrackSwitch.class),
    DISEMBARK(Module.TRACKS, 116, "Disembarking Track", "disembarking", "RCDisembarkingRailTile", 8, TileRailDisembark.class, TrackDisembark.class),
    SUSPENDED(Module.EXTRAS, 253, "Suspended Track", "suspended", "RCSuspendedRailTile", 8, TileRailSuspended.class, TrackSuspended.class),
    GATED_ONEWAY(Module.TRACKS, 132, "Gated One-Way Track", "gated.oneway", "RCGatedOneWayRailTile", 4, TileRailGatedOneWay.class, TrackGatedOneWay.class),
    GATED(Module.TRACKS, 254, "Gated Track", "gated", "RCGatedRailTile", 4, TileRailGated.class, TrackGated.class),
    SLOW(Module.TRACKS_WOOD, 6 + 16, "Wooden Track", "slow", "RCSlowRailTile", 16, TileRailSlow.class, TrackSlow.class),
    SLOW_BOOSTER(Module.TRACKS_WOOD, 52, "Wooden Booster Track", "slow.boost", "RCSlowBoosterRailTile", 8, TileRailSlowBooster.class, TrackSlowBooster.class),
    SLOW_JUNCTION(Module.TRACKS_WOOD, 6 + 32, "Wooden Junction Track", "slow.junction", "RCSlowJunctionRailTile", 8, TileRailSlowJunction.class, TrackSlowJunction.class),
    SLOW_SWITCH(Module.SIGNALS, 4 * 16, "Wooden Switch Track", "slow.switch", "RCSlowSwitchRailTile", 8, TileRailSlowSwitch.class, TrackSlowSwitch.class),
    SPEED(Module.TRACKS_HIGHSPEED, 7 + 16, "H.S. Track", "speed", "RCSpeedRailTile", 16, TileRailSpeed.class, TrackSpeed.class),
    SPEED_BOOST(Module.TRACKS_HIGHSPEED, 20, "H.S. Booster Track", "speed.boost", "RCSpeedBoostRailTile", 8, TileRailSpeedBoost.class, TrackSpeedBoost.class),
    SPEED_TRANSITION(Module.TRACKS_HIGHSPEED, 2 * 16, "H.S. Transition Track", "speed.transition", "RCSpeedTransitionRailTile", 8, TileRailSpeedTransition.class, TrackSpeedTransition.class),
    SPEED_SWITCH(Module.SIGNALS, 5 * 16, "H.S. Switch Track", "speed.switch", "RCSpeedSwitchRailTile", 8, TileRailSpeedSwitch.class, TrackSpeedSwitch.class),
    BOARDING_TRAIN(Module.TRAIN, 6 * 16, "Train Boarding Track", "boarding.train", "RCBoardingRailTrainTile", 8, TileRailBoardingTrain.class, TrackBoardingTrain.class),
    HOLDING_TRAIN(Module.TRAIN, 148, "Train Holding Track", "holding.train", "RCHoldingRailTrainTile", 8, TileRailHoldingTrain.class, TrackHoldingTrain.class),
    COUPLER(Module.TRACKS, 7 * 16, "Coupler Track", "coupler", "RCCouplerTrackTile", 8, TileRailCoupler.class, TrackCoupler.class),
    DECOUPLER(Module.TRACKS, 7 * 16 + 2, "Decoupler Track", "decoupler", "RCDecouplerTrackTile", 8, TileRailDecoupler.class, TrackDecoupler.class),
    REINFORCED(Module.TRACKS_REINFORCED, 8 + 16, "Reinforced Track", "reinforced", "", 16, null, TrackReinforced.class),
    REINFORCED_BOOSTER(Module.TRACKS_REINFORCED, 164, "Reinforced Booster Track", "reinforced.boost", "", 8, null, TrackReinforcedBooster.class),
    REINFORCED_JUNCTION(Module.TRACKS_REINFORCED, 8 + 32, "Reinforced Junction Track", "reinforced.junction", "", 8, null, TrackReinforcedJunction.class),
    REINFORCED_SWITCH(Module.TRACKS_REINFORCED, 8 * 16, "Reinforced Switch Track", "reinforced.switch", "", 8, null, TrackReinforcedSwitch.class),;
    private final Module module;
    private final String name;
    private final String tag;
    private final String tileTag;
    private final int recipeOutput;
    private final Class<? extends TileEntity> tile;
    private final byte id;
    private final int textureIndex;
    private final TrackSpec trackSpec;
    private static byte nextId = 0;
    private static final List<EnumTrack> creativeList = new ArrayList<EnumTrack>();

    static {
        creativeList.add(SWITCH);
        creativeList.add(JUNCTION);
        creativeList.add(CONTROL);
        creativeList.add(BOARDING);
        creativeList.add(BOARDING_TRAIN);
        creativeList.add(HOLDING);
        creativeList.add(HOLDING_TRAIN);
        creativeList.add(DISEMBARK);
        creativeList.add(COUPLER);
        creativeList.add(DECOUPLER);
        creativeList.add(ONEWAY);
        creativeList.add(GATED_ONEWAY);
        creativeList.add(GATED);
        creativeList.add(SUSPENDED);
        creativeList.add(PRIMING);
        creativeList.add(LAUNCHER);
        creativeList.add(SLOW);
        creativeList.add(SLOW_BOOSTER);
        creativeList.add(SLOW_SWITCH);
        creativeList.add(SLOW_JUNCTION);
        creativeList.add(SPEED);
        creativeList.add(SPEED_BOOST);
        creativeList.add(SPEED_TRANSITION);
        creativeList.add(SPEED_SWITCH);
        creativeList.add(REINFORCED);
        creativeList.add(REINFORCED_BOOSTER);
        creativeList.add(REINFORCED_JUNCTION);
    }

    private EnumTrack(Module module, int textureIndex, String name, String tag, String tileTag, int recipeOutput, Class<? extends TileEntity> tile, Class<? extends TrackBaseRailcraft> track)
    {
        this.module = module;
        this.textureIndex = textureIndex;
        this.name = name;
        this.tag = tag;
        this.tileTag = tileTag;
        this.recipeOutput = recipeOutput;
        this.tile = tile;
        id = getNextId();

        String textureFile = "/railcraft/textures/tracks.png";
        trackSpec = new TrackSpec(id, getTag(), textureFile, textureIndex, track);
        trackSpec.setTrackName("en_US", name);
        TrackRegistry.registerTrackSpec(trackSpec);
    }

    public boolean isEnabled()
    {
        return RailcraftModuleManager.isModuleLoaded(module) && RailcraftConfig.isSubBlockEnabled(getTag());
    }

    public ItemStack getItem()
    {
        return getItem(1);
    }

    public ItemStack getItem(int qty)
    {
        return new ItemStack(RailcraftBlocks.getBlockTrack(), qty, getId());
    }

    public TrackSpec getTrackSpec()
    {
        return trackSpec;
    }

    public String getName()
    {
        return name;
    }

    public String getTag()
    {
        return "track." + tag;
    }

    public String getTileTag()
    {
        return tileTag;
    }

    public Class<? extends TileEntity> getTileClass()
    {
        return tile;
    }

    public TileEntity getBlockEntity()
    {
        try {
            return tile.newInstance();
        } catch (InstantiationException ex) {
        } catch (IllegalAccessException ex) {
        }
        return null;
    }

    public byte getId()
    {
        return id;
    }

    public int getTextureIndex()
    {
        return textureIndex;
    }

    public static EnumTrack fromId(int id)
    {
        for(EnumTrack r : EnumTrack.values()) {
            if(r.getId() == id) {
                return r;
            }
        }
        return null;
    }

    private static byte getNextId()
    {
        byte i = nextId;
        nextId++;
        return i;
    }

    public static List<EnumTrack> getCreativeList()
    {
        return creativeList;
    }

    public ItemStack registerRecipe()
    {
        if(getItem() == null) {
            return null;
        }
        ItemStack output = getItem(recipeOutput);
        ItemStack railStandard = RailcraftPartItems.getRailStandard();
        ItemStack railAdvanced = RailcraftPartItems.getRailAdvanced();
        ItemStack railSpeed = RailcraftPartItems.getRailSpeed();
        ItemStack railReinforced = RailcraftPartItems.getRailReinforced();
        ItemStack woodTie = RailcraftPartItems.getTieWood();
        ItemStack woodRailbed = RailcraftPartItems.getRailbedWood();
        ItemStack stoneRailbed = RailcraftPartItems.getRailbedStone();
        ItemStack reinforcedRailbed = RailcraftConfig.useOldRecipes() ? new ItemStack(Block.OBSIDIAN) : RailcraftPartItems.getRailbedStone();
        ItemStack ingotSteel = RailcraftPartItems.getIngotSteel();

        ItemStack crowbar = RailcraftToolItems.getCrowbar();
        crowbar.setData(-1);

        switch (this) {
            case BOARDING:
                ModLoader.addRecipe(output, new Object[]{
                        "IrI",
                        "IbI",
                        "IsI",
                        Character.valueOf('I'), railAdvanced,
                        Character.valueOf('s'), woodRailbed,
                        Character.valueOf('r'), Item.REDSTONE,
                        Character.valueOf('b'), Block.STONE_PLATE,});
                break;
            case BOARDING_TRAIN:
                ModLoader.addRecipe(output, new Object[]{
                        "IrI",
                        "IbI",
                        "IsI",
                        Character.valueOf('I'), railAdvanced,
                        Character.valueOf('s'), woodRailbed,
                        Character.valueOf('r'), Item.DIODE,
                        Character.valueOf('b'), Block.STONE_PLATE,});
                break;
            case ONEWAY:
                ModLoader.addRecipe(output, new Object[]{
                        "IbI",
                        "IsI",
                        "IpI",
                        Character.valueOf('I'), railStandard,
                        Character.valueOf('s'), woodRailbed,
                        Character.valueOf('b'), Block.STONE_PLATE,
                        Character.valueOf('p'), Block.PISTON,});
                break;
            case CONTROL:
                ModLoader.addRecipe(output, new Object[]{
                        "IrI",
                        "GsG",
                        "IrI",
                        Character.valueOf('I'), railStandard,
                        Character.valueOf('G'), railAdvanced,
                        Character.valueOf('s'), woodRailbed,
                        Character.valueOf('r'), Item.REDSTONE,});
                break;
            case HOLDING:
                ModLoader.addRecipe(output, new Object[]{
                        "IsI",
                        "IbI",
                        "IrI",
                        Character.valueOf('I'), railAdvanced,
                        Character.valueOf('s'), woodRailbed,
                        Character.valueOf('r'), Item.REDSTONE,
                        Character.valueOf('b'), Block.STONE_PLATE,});
                break;
            case HOLDING_TRAIN:
                ModLoader.addRecipe(output, new Object[]{
                        "IsI",
                        "IbI",
                        "IrI",
                        Character.valueOf('I'), railAdvanced,
                        Character.valueOf('s'), woodRailbed,
                        Character.valueOf('r'), Item.DIODE,
                        Character.valueOf('b'), Block.STONE_PLATE,});
                break;
            case SPEED:
//                if(RailcraftConfig.useOldRecipes()) {
//                    ModLoader.addRecipe(output, new Object[]{
//                            "I I",
//                            "IGI",
//                            "IsI",
//                            Character.valueOf('I'), ingotSteel,
//                            Character.valueOf('G'), Block.blockGold,
//                            Character.valueOf('s'), stoneRailbed,});
//                } else {
                ModLoader.addRecipe(output, new Object[]{
                        "I I",
                        "IsI",
                        "I I",
                        Character.valueOf('I'), railSpeed,
                        Character.valueOf('s'), stoneRailbed,});
//                }
                break;
            case SPEED_BOOST:
//                if(RailcraftConfig.useOldRecipes()) {
//                    ModLoader.addRecipe(output, new Object[]{
//                            "IsI",
//                            "IGI",
//                            "IrI",
//                            Character.valueOf('I'), ingotSteel,
//                            Character.valueOf('G'), Block.blockGold,
//                            Character.valueOf('s'), stoneRailbed,
//                            Character.valueOf('r'), Item.redstone,});
//                } else {
                ModLoader.addRecipe(output, new Object[]{
                        "IrI",
                        "IsI",
                        "IrI",
                        Character.valueOf('I'), railSpeed,
                        Character.valueOf('s'), stoneRailbed,
                        Character.valueOf('r'), Item.REDSTONE,});
//                }
                break;
            case SPEED_TRANSITION:
//                if(RailcraftConfig.useOldRecipes()) {
//                    ModLoader.addRecipe(output, new Object[]{
//                            "IrI",
//                            "IGI",
//                            "IsI",
//                            Character.valueOf('I'), ingotSteel,
//                            Character.valueOf('G'), Block.blockGold,
//                            Character.valueOf('s'), stoneRailbed,
//                            Character.valueOf('r'), Item.redstone,});
//                } else {
                ModLoader.addRecipe(output, new Object[]{
                        "IrI",
                        "IrI",
                        "IsI",
                        Character.valueOf('I'), railSpeed,
                        Character.valueOf('s'), stoneRailbed,
                        Character.valueOf('r'), Item.REDSTONE,});
                ModLoader.addRecipe(output, new Object[]{
                        "IsI",
                        "IrI",
                        "IrI",
                        Character.valueOf('I'), railSpeed,
                        Character.valueOf('s'), stoneRailbed,
                        Character.valueOf('r'), Item.REDSTONE,});
//                }
                break;
            case SPEED_SWITCH:
//                if(RailcraftConfig.useOldRecipes()) {
//                ModLoader.addRecipe(output, new Object[]{
//                        "I#I",
//                        "IGI",
//                        "III",
//                        Character.valueOf('I'), ingotSteel,
//                        Character.valueOf('G'), Block.blockGold,
//                        Character.valueOf('#'), stoneRailbed,});
//                } else {
                ModLoader.addRecipe(output, new Object[]{
                        "IsI",
                        "III",
                        "III",
                        Character.valueOf('I'), railSpeed,
                        Character.valueOf('s'), stoneRailbed,});
//                }
                break;
            case LAUNCHER:
                ModLoader.addRecipe(output, new Object[]{
                        "IsI",
                        "BPB",
                        "IsI",
                        Character.valueOf('I'), railStandard,
                        Character.valueOf('B'), Block.IRON_BLOCK,
                        Character.valueOf('s'), woodRailbed,
                        Character.valueOf('P'), Block.PISTON,});
                break;
            case SLOW_BOOSTER:
                ModLoader.addRecipe(output, new Object[]{
                        "I I",
                        "G#G",
                        "IrI",
                        Character.valueOf('G'), Item.GOLD_INGOT,
                        Character.valueOf('I'), woodTie,
                        Character.valueOf('#'), woodRailbed,
                        Character.valueOf('r'), Item.REDSTONE,});
                break;
            case PRIMING:
                ModLoader.addRecipe(output, new Object[]{
                        "IpI",
                        "IsI",
                        "IfI",
                        Character.valueOf('I'), railStandard,
                        Character.valueOf('s'), woodRailbed,
                        Character.valueOf('p'), Block.STONE_PLATE,
                        Character.valueOf('f'), Item.FLINT_AND_STEEL,});
                break;
            case JUNCTION:
                ModLoader.addRecipe(output, new Object[]{
                        "III",
                        "I#I",
                        "III",
                        Character.valueOf('I'), railStandard,
                        Character.valueOf('#'), woodRailbed,});
                break;
            case SLOW_JUNCTION:
                ModLoader.addRecipe(output, new Object[]{
                        "III",
                        "I#I",
                        "III",
                        Character.valueOf('I'), woodTie,
                        Character.valueOf('#'), woodRailbed,});
                break;
            case SLOW:
                ModLoader.addRecipe(output, new Object[]{
                        "I I",
                        "I#I",
                        "I I",
                        Character.valueOf('I'), woodTie,
                        Character.valueOf('#'), woodRailbed,});
                break;
            case SWITCH:
                ModLoader.addRecipe(output, new Object[]{
                        "I#I",
                        "III",
                        "III",
                        Character.valueOf('I'), railStandard,
                        Character.valueOf('#'), woodRailbed,});
                break;
            case SLOW_SWITCH:
                ModLoader.addRecipe(output, new Object[]{
                        "I#I",
                        "III",
                        "III",
                        Character.valueOf('I'), woodTie,
                        Character.valueOf('#'), woodRailbed,});
                break;
            case DISEMBARK:
                ModLoader.addRecipe(output, new Object[]{
                        "IpI",
                        "I#I",
                        "IrI",
                        Character.valueOf('I'), railAdvanced,
                        Character.valueOf('#'), woodRailbed,
                        Character.valueOf('r'), Item.REDSTONE,
                        Character.valueOf('p'), Block.STONE_PLATE,});
                break;
            case SUSPENDED:
//                if(RailcraftConfig.useOldRecipes()) {
//                    ModLoader.addRecipe(output, new Object[]{
//                            "ItI",
//                            "ItI",
//                            "ItI",
//                            Character.valueOf('I'), railStandard,
//                            Character.valueOf('t'), Item.stick,});
//                } else {
                ModLoader.addRecipe(output, new Object[]{
                        "ItI",
                        "ItI",
                        "ItI",
                        Character.valueOf('I'), railStandard,
                        Character.valueOf('t'), woodTie,});
//                }
                break;
            case GATED:
                ModLoader.addRecipe(output, new Object[]{
                        "IgI",
                        "I#I",
                        "IgI",
                        Character.valueOf('I'), railStandard,
                        Character.valueOf('#'), woodRailbed,
                        Character.valueOf('g'), Block.FENCE_GATE,});
                break;
            case GATED_ONEWAY:
                ModLoader.addRecipe(output, new Object[]{
                        "IgI",
                        "G#G",
                        "IgI",
                        Character.valueOf('I'), railStandard,
                        Character.valueOf('#'), woodRailbed,
                        Character.valueOf('g'), Block.FENCE_GATE,
                        Character.valueOf('G'), railAdvanced,});
                break;
            case COUPLER:
                ModLoader.addRecipe(output, new Object[]{
                        "I I",
                        "I#I",
                        "IcI",
                        Character.valueOf('I'), railAdvanced,
                        Character.valueOf('#'), woodRailbed,
                        Character.valueOf('c'), crowbar});
                break;
            case DECOUPLER:
                ModLoader.addRecipe(output, new Object[]{
                        "IcI",
                        "I#I",
                        "I I",
                        Character.valueOf('I'), railAdvanced,
                        Character.valueOf('#'), woodRailbed,
                        Character.valueOf('c'), crowbar});
                break;
            case REINFORCED:
                ModLoader.addRecipe(output, new Object[]{
                        "I I",
                        "I#I",
                        "I I",
                        Character.valueOf('I'), railReinforced,
                        Character.valueOf('#'), reinforcedRailbed});
                break;
            case REINFORCED_BOOSTER:
                ModLoader.addRecipe(output, new Object[]{
                        "I I",
                        "I#I",
                        "IrI",
                        Character.valueOf('I'), railReinforced,
                        Character.valueOf('r'), new ItemStack(Item.REDSTONE),
                        Character.valueOf('#'), reinforcedRailbed});
                break;
            case REINFORCED_JUNCTION:
                ModLoader.addRecipe(output, new Object[]{
                        "III",
                        "I#I",
                        "III",
                        Character.valueOf('I'), railReinforced,
                        Character.valueOf('#'), reinforcedRailbed});
                break;
            case REINFORCED_SWITCH:
                ModLoader.addRecipe(output, new Object[]{
                        "I#I",
                        "III",
                        "III",
                        Character.valueOf('I'), railReinforced,
                        Character.valueOf('#'), reinforcedRailbed});
                break;
        }
        return output;
    }
}