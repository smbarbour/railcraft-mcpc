package railcraft.common.tracks;

import java.util.ArrayList;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockMinecartTrack;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityMinecart;
import net.minecraft.server.IBlockAccess;
import net.minecraft.server.ItemStack;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import forge.ITextureProvider;
import railcraft.Railcraft;
import railcraft.common.api.CartTools;

/**
 * Implementation of the iron ladder blocks.
 * Iron ladders act much like normal (wooden) ladders.
 * Climbing down iron ladders is a bit faster than climbing down normal ladders.
 * Additionally, minecarts can run down vertically on iron ladders,
 * as if they were vertical rail tracks.
 * @author DizzyDragon
 */
public class BlockRailElevator extends Block implements ITextureProvider
{

    /**
     * The upward velocity of an entity climbing the ladder.
     */
    public static double CLIMB_UP_VELOCITY = 0.2;
    /**
     * The downward velocity of an entity climbing the ladder
     */
    public static double CLIMB_DOWN_VELOCITY = -0.3;
    /**
     * The inverse of the downward motion an entity gets
     * within a single update of the game engine due to gravity.
     */
    public static double FALL_DOWN_CORRECTION = 0.039999999105930328D;
    ;
	 /**
	  * Metadata values for the direction the ladder is facing.
	  */
	 public static final int FACING_EAST_METADATA_VALUE = 2,
        FACING_WEST_METADATA_VALUE = 3,
        FACING_NORTH_METADATA_VALUE = 4,
        FACING_SOUTH_METADATA_VALUE = 5;
    /**
     * Velocity at which a minecart travels up on the rail when activated
     */
    public static final double RIDE_UP_VELOCITY = +0.4;
    /**
     * Velocity at which a minecart travels down on the rail when not activated
     */
    public static final double RIDE_DOWN_VELOCITY = -0.4;
    /**
     * The bits of the metadata that are used for storing the direction of the ladder.
     * Use the bitwise-and operator (&) on the metadata to discard all other data from
     * a metadata value.
     */
    public static final int BLOCK_FACING_DATA_METADATA_MASK = 0x0007;
    private int renderType;

    /**
     * Constructor of a iron ladder block
     * @param blockId The ID of the block
     * @param graphics the index of the texture in the texture file
     * @param name the name of this block
     */
    public BlockRailElevator(int blockId, int renderId)
    {
        super(blockId, 84, new MaterialElevator());
//		  setBlockName(name);
        c(1.05F);
        a(i);
        this.renderType = renderId;
    }

    @Override
    public MovingObjectPosition a(World world, int i, int j, int k, Vec3D vec3d, Vec3D vec3d1)
    {
        updateShape(world, i, j, k);
        return super.a(world, i, j, k, vec3d, vec3d1);
    }

    @Override
    public void updateShape(IBlockAccess world, int i, int j, int k)
    {
        int meta = getLadderFacingMetadata(world, i, j, k);
        float f = 0.125F;
        if(meta == 2) {
            a(0.0F, 0.0F, 1.0F - f, 1.0F, 1.0F, 1.0F);
        }
        if(meta == 3) {
            a(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, f);
        }
        if(meta == 4) {
            a(1.0F - f, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
        }
        if(meta == 5) {
            a(0.0F, 0.0F, 0.0F, f, 1.0F, 1.0F);
        }
    }

    @Override
    public AxisAlignedBB e(World world, int i, int j, int k)
    {
        return null;
    }

    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int i, int j, int k)
    {
        updateShape(world, i, j, k);
        return AxisAlignedBB.b((double)i + minX, (double)j + minY, (double)k + minZ, (double)i + maxX, (double)j + maxY, (double)k + maxZ);
    }

    @Override
    public String getTextureFile()
    {
        return "/railcraft/textures/tracks.png";
    }

    @Override
    public int c()
    {
        return renderType;
    }

    @Override
    public boolean isLadder(World world, int i, int j, int k)
    {
        return true;
    }

    @Override
    public boolean a()
    {
        return false;
    }

    @Override
    public boolean b()
    {
        return false;
    }

    public boolean isACube()
    {
        return false;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void addCreativeItems(ArrayList itemList)
    {
        itemList.add(new ItemStack(this));
    }

    @Override
    public int a(int side, int meta)
    {
        boolean powered = (meta & 8) != 0;
        if(!powered) {
            return textureId + 1;
        }
        return textureId;
    }

    @Override
    public boolean canPlace(World world, int i, int j, int k)
    {
        if(world.isBlockSolidOnSide(i - 1, j, k, 5)) {
            return true;
        }
        if(world.isBlockSolidOnSide(i + 1, j, k, 4)) {
            return true;
        }
        if(world.isBlockSolidOnSide(i, j, k - 1, 3)) {
            return true;
        }
        return world.isBlockSolidOnSide(i, j, k + 1, 2);
    }

    @Override
    public void postPlace(World world, int i, int j, int k, int l)
    {
        int i1 = world.getData(i, j, k);
        if((i1 == 0 || l == 2) && world.isBlockSolidOnSide(i, j, k + 1, 2)) {
            i1 = 2;
        }
        if((i1 == 0 || l == 3) && world.isBlockSolidOnSide(i, j, k - 1, 3)) {
            i1 = 3;
        }
        if((i1 == 0 || l == 4) && world.isBlockSolidOnSide(i + 1, j, k, 4)) {
            i1 = 4;
        }
        if((i1 == 0 || l == 5) && world.isBlockSolidOnSide(i - 1, j, k, 5)) {
            i1 = 5;
        }
        world.setData(i, j, k, i1);

        updateShape(world, i, j, k);

        if(BlockMinecartTrack.g(world, i, j - 1, k)) {
            int id = world.getTypeId(i, j - 1, k);
            BlockMinecartTrack railBlock = (BlockMinecartTrack)Block.byId[id];
            if(railBlock.canMakeSlopes(world, i, j - 1, k)) {
                int trackMeta = railBlock.getBasicRailMetadata(world, null, i, j - 1, k);
                int ladderMeta = getLadderFacingMetadata(world, i, j, k);

                int outputMeta = 0;
                if(trackMeta == 0 && ladderMeta == 2) {
                    outputMeta = 5;
                } else if(trackMeta == 0 && ladderMeta == 3) {
                    outputMeta = 4;
                } else if(trackMeta == 1 && ladderMeta == 4) {
                    outputMeta = 2;
                } else if(trackMeta == 1 && ladderMeta == 5) {
                    outputMeta = 3;
                }
                if(outputMeta != 0) {
                    if(railBlock.hasPowerBit(world, i, j - 1, k)) {
                        outputMeta = outputMeta | (world.getData(i, j - 1, k) & 8);
                    }
                    world.setData(i, j - 1, k, outputMeta);
                }
            }
        }

        int meta = world.getData(i, j, k);
        boolean powered = (meta & 8) != 0;
        if(powered ^ isPowered(world, i, j, k)) {
            world.setData(i, j, k, meta ^ 8);
        }
        world.notify(i, j, k);
    }

    @Override
    public void dropNaturally(World world, int i, int j, int k, int l, float f, int i1)
    {
//        System.out.println("dropping item");
        super.dropNaturally(world, i, j, k, l, f, i1);
    }

    @Override
    public void doPhysics(World world, int i, int j, int k, int l)
    {
        int meta = world.getData(i, j, k);
        int ladderMeta = getLadderFacingMetadata(world, i, j, k);
        boolean valid = false;
        if(ladderMeta == 2 && world.isBlockSolidOnSide(i, j, k + 1, 2)) {
            valid = true;
        }
        if(ladderMeta == 3 && world.isBlockSolidOnSide(i, j, k - 1, 3)) {
            valid = true;
        }
        if(ladderMeta == 4 && world.isBlockSolidOnSide(i + 1, j, k, 4)) {
            valid = true;
        }
        if(ladderMeta == 5 && world.isBlockSolidOnSide(i - 1, j, k, 5)) {
            valid = true;
        }
        if(!valid) {
            b(world, i, j, k, ladderMeta, 0);
            world.setTypeId(i, j, k, 0);
        } else {
            boolean powered = (meta & 8) != 0;
            if(powered ^ isPowered(world, i, j, k)) {
//					 System.out.println("Power change");
                world.setData(i, j, k, meta ^ 8);
                world.notify(i, j, k);
            }
        }
    }

    @Override
    public void a(World world, int i, int j, int k, Entity entity)
    {
        entity.fallDistance = 0;
        if(Railcraft.gameIsNotHost() || !(entity instanceof EntityMinecart)) {
            return;
        }
        minecartInteraction(world, (EntityMinecart)entity, i, j, k);
    }

    public int getLadderFacingMetadata(IBlockAccess world, int i, int j, int k)
    {
        return world.getData(i, j, k) & BLOCK_FACING_DATA_METADATA_MASK;
    }

    public boolean getPoweredBit(World world, int i, int j, int k)
    {
        return (world.getData(i, j, k) & 8) != 0;
    }

    ////////////////////////////////////////////////////////////////////////////
    // PROTECTED                                                                //
    ////////////////////////////////////////////////////////////////////////////
    protected boolean isPowered(World world, int i, int j, int k)
    {
        if(world.getTypeId(i, j, k) != id) {
            return false;
        }
        if(world.isBlockPowered(i, j, k) || world.isBlockIndirectlyPowered(i, j, k)
            || (getLadderFacingMetadata(world, i, j, k) == getLadderFacingMetadata(world, i, j + 1, k) && isPowered(world, i, j + 1, k))) {
            return true;
        }
        return false;
    }

    /**
     * Updates the state of a single minecart that is whithin the block's area of effect
     * according to the state of the block.
     * @param world the world in which the block resides
     * @param i the x-coordinate of the block
     * @param j the y-coordinate of the block
     * @param k the z-coordinate of the block
     * @param cart the minecart for which the state will be updated.
     *        It is assumed that the minecart is whithin the area of effect of the block
     */
    protected void minecartInteraction(World world, EntityMinecart cart, int i, int j, int k)
    {
        boolean powered = getPoweredBit(world, i, j, k);
        if(powered) {
            if(world.getTypeId(i, j + 1, k) == id || isOffloadRail(world, i, j + 1, k)) {
                boolean empty = true;
                for(EntityMinecart c : CartTools.getMinecartsAt(world, i, j + 1, k, 0.2f)) {
                    if(c != cart) {
                        empty = false;
                    }
                }
                if((getPoweredBit(world, i, j + 1, k) || isOffloadRail(world, i, j + 1, k)) && empty) {
                    cart.motY = RIDE_UP_VELOCITY + FALL_DOWN_CORRECTION;
                } else {
                    if(pushMinecartOntoRail(world, i, j, k, cart)) {
                        return;
                    } else {
                        cart.setPosition(cart.locX, j + 0.5f, cart.locZ);
                        cart.motY = FALL_DOWN_CORRECTION;
                    }
                }
            } else {
                cart.setPosition(cart.locX, j + 0.5f, cart.locZ);
            }
        } else {
            if(world.getTypeId(i, j - 1, k) != id) {
                pushMinecartOntoRail(world, i, j, k, cart);
                return;
            } else {
                boolean empty = true;
                for(EntityMinecart c : CartTools.getMinecartsAt(world, i, j - 1, k, 0.2f)) {
                    if(c != cart) {
                        empty = false;
                    }
                }
                if(empty) {
                    cart.motY = RIDE_DOWN_VELOCITY + FALL_DOWN_CORRECTION;
                } else {
                    cart.setPosition(cart.locX, j + 0.5f, cart.locZ);
                    cart.motY = FALL_DOWN_CORRECTION;
                }
            }
        }

        if(powered || !BlockMinecartTrack.g(world, i, j - 1, k)) {
            if(BlockMinecartTrack.g(world, i, j - 1, k) || BlockMinecartTrack.g(world, i, j - 2, k)) {
                cart.setCanUseRail(false);
            } else {
                cart.setCanUseRail(true);
            }
            keepMinecartConnected(world, i, j, k, cart);
        } else {
            cart.setCanUseRail(true);
        }

//        RailcraftUtils.resetFallDistance(cart);

        if(powered) {
            pushMinecartOnSupportingBlockIfPossible(world, i, j, k, cart);
        }
    }

    /**
     * Adjusts the motion and rotationyaw of a minecart so that it
     * stays in position and alligned to the iron ladder.
     * @param world the world in which the block resides
     * @param x the x-coordinate of the block
     * @param y the y-coordinate of the block
     * @param z the z-coordinate of the block
     * @param minecart the minecart for which motion and rotation will be adjusted
     */
    protected void keepMinecartConnected(World world, int i, int j, int k, EntityMinecart minecart)
    {
        minecart.motX = (i + 0.5) - minecart.locX;
        minecart.motZ = (k + 0.5) - minecart.locZ;

        allignMinecart(world, i, j, k, minecart);
    }

    /**
     * Alligns the minecart to the ladder to the ladder
     * @param world the world in which the block resides
     * @param x the x-coordinate of the block
     * @param y the y-coordinate of the block
     * @param z the z-coordinate of the block
     * @param minecart the minecart for which motion and rotation will be adjusted
     */
    protected void allignMinecart(World world, int i, int j, int k, EntityMinecart minecart)
    {
        switch (getLadderFacingMetadata(world, i, j, k)) {
            case FACING_NORTH_METADATA_VALUE:
            case FACING_SOUTH_METADATA_VALUE:
                if(minecart.yaw <= 90.0f || minecart.yaw > 270.0f) {
                    minecart.yaw = 0.0f;
                } else {
                    minecart.yaw = 180.0f;
                }
                return;

            case FACING_EAST_METADATA_VALUE:
            case FACING_WEST_METADATA_VALUE:
                if(minecart.yaw > 180.0f) {
                    minecart.yaw = 270.0f;
                } else {
                    minecart.yaw = 90.0f;
                }
                return;
        }
    }

    private boolean isOffloadRail(World world, int i, int j, int k)
    {
        if(world.getTypeId(i, j, k) != id) {
            switch (world.getData(i, j - 1, k) & BLOCK_FACING_DATA_METADATA_MASK) {
                case FACING_EAST_METADATA_VALUE:
                    if(BlockMinecartTrack.g(world, i, j, k + 1)) {
                        return true;
                    }

                case FACING_WEST_METADATA_VALUE:
                    if(BlockMinecartTrack.g(world, i, j, k - 1)) {

                        return true;
                    }

                case FACING_NORTH_METADATA_VALUE:
                    if(BlockMinecartTrack.g(world, i + 1, j, k)) {
                        return true;
                    }

                case FACING_SOUTH_METADATA_VALUE:
                    if(BlockMinecartTrack.g(world, i - 1, j, k)) {
                        return true;
                    }

                default:
                    return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Pushes a minecart onto the block on which the ladder is placed if it is possible.
     * It is only possible to push the minecart if there is air directly above the ladder
     * block and if the block directly above the supporting block is a rail.
     * @param world the world in which the block resides
     * @param i the x-coordinate of the ladder block
     * @param j the y-coordinate of the ladder block
     * @param k the z-coordinate of the ladder block
     * @param minecart the minecart that is pushed which onto the block if possible
     * @return true if the minecart can be pushed onto the supporting block,
     *         otherwise false
     */
    private boolean pushMinecartOnSupportingBlockIfPossible(World world, int i, int j, int k, EntityMinecart minecart)
    {
        if(!world.getMaterial(i, j, k).isSolid()) {
            switch (world.getData(i, j, k) & BLOCK_FACING_DATA_METADATA_MASK) {
                case FACING_EAST_METADATA_VALUE:
                    if(BlockMinecartTrack.g(world, i, j + 1, k + 1)) {
                        minecart.motY = RIDE_UP_VELOCITY;
                        minecart.motZ = RIDE_UP_VELOCITY;
                    }
                    return true;

                case FACING_WEST_METADATA_VALUE:
                    if(BlockMinecartTrack.g(world, i, j + 1, k - 1)) {
                        minecart.motY = RIDE_UP_VELOCITY;
                        minecart.motZ = -RIDE_UP_VELOCITY;
                    }
                    return true;

                case FACING_NORTH_METADATA_VALUE:
                    if(BlockMinecartTrack.g(world, i + 1, j + 1, k)) {
                        minecart.motY = RIDE_UP_VELOCITY;
                        minecart.motX = RIDE_UP_VELOCITY;
                    }
                    return true;

                case FACING_SOUTH_METADATA_VALUE:
                    if(BlockMinecartTrack.g(world, i - 1, j + 1, k)) {
                        minecart.motY = RIDE_UP_VELOCITY;
                        minecart.motX = -RIDE_UP_VELOCITY;
                    }
                    return true;

                default:
                    return false;
            }
        } else {
            return false;
        }
    }

    private boolean pushMinecartOntoRail(World world, int i, int j, int k, EntityMinecart cart)
    {
        cart.setCanUseRail(true);
        switch (world.getData(i, j, k) & BLOCK_FACING_DATA_METADATA_MASK) {
            case FACING_EAST_METADATA_VALUE:
                if(BlockMinecartTrack.g(world, i, j, k - 1)) {
                    cart.setPosition(cart.locX, j + 0.6f, cart.locZ);
                    cart.motY = FALL_DOWN_CORRECTION;
                    cart.motZ = -RIDE_UP_VELOCITY;
                    return true;
                }
                break;
            case FACING_WEST_METADATA_VALUE:
                if(BlockMinecartTrack.g(world, i, j, k + 1)) {
                    cart.setPosition(cart.locX, j + 0.6f, cart.locZ);
                    cart.motY = FALL_DOWN_CORRECTION;
                    cart.motZ = RIDE_UP_VELOCITY;
                    return true;
                }
                break;
            case FACING_NORTH_METADATA_VALUE:
                if(BlockMinecartTrack.g(world, i - 1, j, k)) {
                    cart.setPosition(cart.locX, j + 0.6f, cart.locZ);
                    cart.motY = FALL_DOWN_CORRECTION;
                    cart.motX = -RIDE_UP_VELOCITY;
                    return true;
                }
                break;
            case FACING_SOUTH_METADATA_VALUE:
                if(BlockMinecartTrack.g(world, i + 1, j, k)) {
                    cart.setPosition(cart.locX, j + 0.6f, cart.locZ);
                    cart.motY = FALL_DOWN_CORRECTION;
                    cart.motX = RIDE_UP_VELOCITY;
                    return true;
                }
                break;
            default:
                return false;
        }
        return false;
    }
}
