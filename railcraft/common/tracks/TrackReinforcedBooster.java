package railcraft.common.tracks;

import railcraft.common.api.tracks.ITrackPowered;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;

public class TrackReinforcedBooster extends TrackReinforced implements ITrackPowered
{

    private static final int POWER_PROPAGATION = 8;
    private static final double BOOST_FACTOR = 0.065;
    private static final double SLOW_FACTOR = 0.5;
    private static final double START_BOOST = 0.02;
    private static final double STALL_THRESHOLD = 0.03;
    private static final double BOOST_THRESHOLD = 0.01;
    private boolean powered = false;

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.REINFORCED_BOOSTER;
    }

    @Override
    public boolean isFlexibleRail()
    {
        return false;
    }

    @Override
    public int getTextureIndex()
    {
        if(!isPowered()) {
            return getTrackType().getTextureIndex() + 1;
        }
        return getTrackType().getTextureIndex();
    }

    @Override
    public void onMinecartPass(EntityMinecart cart)
    {
        int meta = tileEntity.k();

        int i = tileEntity.x;
        int j = tileEntity.y;
        int k = tileEntity.z;

        int dirMeta = meta & 7;
        double speed = Math.sqrt(cart.motX * cart.motX + cart.motZ * cart.motZ);
        if(powered) {
            if(speed > BOOST_THRESHOLD) {
                cart.motX += (cart.motX / speed) * BOOST_FACTOR;
                cart.motZ += (cart.motZ / speed) * BOOST_FACTOR;
            } else if(dirMeta == 1) {
                if(tileEntity.world.isBlockSolidOnSide(i - 1, j, k, 5)) {
                    cart.motX = START_BOOST;
                } else if(tileEntity.world.isBlockSolidOnSide(i + 1, j, k, 4)) {
                    cart.motX = -START_BOOST;
                }
            } else if(dirMeta == 0) {
                if(tileEntity.world.isBlockSolidOnSide(i, j, k - 1, 3)) {
                    cart.motZ = START_BOOST;
                } else if(tileEntity.world.isBlockSolidOnSide(i, j, k + 1, 2)) {
                    cart.motZ = -START_BOOST;
                }
            }
        } else {
            if(speed < STALL_THRESHOLD) {
                cart.motX = 0.0D;
                cart.motY = 0.0D;
                cart.motZ = 0.0D;
            } else {
                cart.motX *= SLOW_FACTOR;
                cart.motY = 0.0D;
                cart.motZ *= SLOW_FACTOR;
            }
        }
    }

    @Override
    public int getPowerPropagation()
    {
        return POWER_PROPAGATION;
    }

    @Override
    public boolean isPowered()
    {
        return powered;
    }

    @Override
    public void setPowered(boolean powered)
    {
        this.powered = powered;
    }

    @Override
    public void writeToNBT(NBTTagCompound data)
    {
        super.writeToNBT(data);

        data.setBoolean("powered", powered);
    }

    @Override
    public void readFromNBT(NBTTagCompound data)
    {
        super.readFromNBT(data);

        powered = data.getBoolean("powered");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(powered);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        powered = data.readBoolean();

        markBlockNeedsUpdate();
    }
}
