package railcraft.common.tracks;

import railcraft.common.api.tracks.EnumTrackMeta;
import net.minecraft.server.*;

public class TrackJunction extends TrackBaseRailcraft
{

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.JUNCTION;
    }

    @Override
    public int getTextureIndex()
    {
        return getTrackType().getTextureIndex();
    }

    @Override
    public boolean canMakeSlopes()
    {
        return false;
    }

    @Override
    public int getBasicRailMetadata(EntityMinecart cart)
    {
        if(cart == null) {
            return EnumTrackMeta.NORTH_SOUTH.getValue();
        }
        float yaw = cart.yaw;
        yaw = yaw % 180;
        while(yaw < 0) {
            yaw += 180;
        }
        if((yaw >= 45) && (yaw <= 135)) {
            return EnumTrackMeta.NORTH_SOUTH.getValue();
        }
        return EnumTrackMeta.EAST_WEST.getValue();
    }
}
