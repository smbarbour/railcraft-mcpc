package railcraft.common.tracks;

import railcraft.common.tracks.speedcontroller.SpeedControllerReinforced;

public class TrackReinforcedJunction extends TrackJunction
{

    public TrackReinforcedJunction()
    {
        speedController = SpeedControllerReinforced.getInstance();
    }

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.REINFORCED_JUNCTION;
    }
}
