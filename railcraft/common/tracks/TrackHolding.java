package railcraft.common.tracks;

import railcraft.common.api.tracks.ITrackPowered;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;
import railcraft.common.api.CartTools;
import railcraft.common.api.tracks.ITrackLockdown;

public class TrackHolding extends TrackBaseRailcraft implements ITrackPowered, ITrackLockdown
{

    protected static float DIR_THRESHOLD = 0.01f;
    protected static float START_BOOST = 0.04f;
    protected static float BOOST_FACTOR = 0.06f;
    protected EntityMinecart lockedCart;
    protected boolean launchForward = true;
    protected boolean powered = false;
    protected byte delay = 0;
    protected byte reset = 0;

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.HOLDING;
    }

    @Override
    public int getTextureIndex()
    {
        if(isPowered() || delay > 0) {
            return getTrackType().getTextureIndex();
        }
        return getTrackType().getTextureIndex() + 1;
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        if(lockedCart != null && lockedCart.dead) {
            lockedCart = null;
        }
        if(isPowered()) {
            delay = getDelayTIme();
            markBlockNeedsUpdate();
        } else if(delay > 0) {
            delay--;
            if(delay == 0) {
                lockedCart = null;
            }
            markBlockNeedsUpdate();
        }
        if(reset > 0) {
            reset--;
        }
    }

    protected byte getDelayTIme()
    {
        return 3;
    }

    protected void checkCart(EntityMinecart cart)
    {
        if(delay > 0 && (cart != lockedCart || reset == 0)) {
            delay = 0;
            lockedCart = cart;
            setLaunchDirection(cart);
            markBlockNeedsUpdate();
        }
        reset = 10;
    }

    @Override
    public void onMinecartPass(EntityMinecart cart)
    {
        checkCart(cart);
        int meta = tileEntity.k();
        if(isPowered() || delay > 0) {
            double speed = CartTools.getCartSpeedUncapped(cart);
            double boostX = START_BOOST;
            double boostZ = START_BOOST;
            if(speed > 0.005D) {
                boostX = (Math.abs(cart.motX) / speed) * BOOST_FACTOR;
                boostZ = (Math.abs(cart.motZ) / speed) * BOOST_FACTOR;
            }
            if(meta == 0 || meta == 4 || meta == 5) {
                if(launchForward) {
                    cart.motZ += boostZ;
                } else {
                    cart.motZ -= boostZ;
                }
            } else if(meta == 1 || meta == 2 || meta == 3) {
                if(launchForward) {
                    cart.motX += boostX;
                } else {
                    cart.motX -= boostX;
                }
            }
        } else {
            if(lockedCart == null) {
                lockedCart = cart;
                setLaunchDirection(cart);
            }
            if(lockedCart == cart) {
                cart.motX = 0.0D;
                cart.motZ = 0.0D;
                if(meta == 0 || meta == 4 || meta == 5) {
                    cart.setPosition(cart.locX, cart.locY, tileEntity.z + 0.5D);
                } else {
                    cart.setPosition(tileEntity.x + 0.5D, cart.locY, cart.locZ);
                }
            } else {
//                double distX = cart.posX - (i + 0.5D);
//                double distZ = cart.posZ - (k + 0.5D);
//                if(Math.abs(distX) < 1.0 && Math.abs(distX) > 0.0) {
//                    cart.motionX += (1.0 / distX) * 0.01;
//                }
//                if(Math.abs(distZ) < 1.0 && Math.abs(distZ) > 0.0) {
//                    cart.motionZ += (1.0 / distZ) * 0.01;
//                }
            }
        }
    }

    protected void setLaunchDirection(EntityMinecart cart)
    {
        int meta = tileEntity.k();
        double speed = CartTools.getCartSpeedUncapped(cart);
        if(speed > DIR_THRESHOLD) {
            if(meta == 0 || meta == 4 || meta == 5) {
                launchForward = cart.motZ > 0;
            } else if(meta == 1 || meta == 2 || meta == 3) {
                launchForward = cart.motX > 0;
            }
            markBlockNeedsUpdate();
        }
    }

    @Override
    public boolean isPowered()
    {
        return powered;
    }

    @Override
    public void setPowered(boolean powered)
    {
        this.powered = powered;
    }

    @Override
    public void writeToNBT(NBTTagCompound data)
    {
        super.writeToNBT(data);
        data.setBoolean("launchForward", launchForward);
        data.setBoolean("powered", powered);
//        data.setByte("delay", delay);
    }

    @Override
    public void readFromNBT(NBTTagCompound data)
    {
        super.readFromNBT(data);
        launchForward = data.getBoolean("launchForward");
        powered = data.getBoolean("powered");
//        delay = data.getByte("delay");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(launchForward);
        data.writeBoolean(powered);
        data.writeByte(delay);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        launchForward = data.readBoolean();
        powered = data.readBoolean();
        delay = data.readByte();

        markBlockNeedsUpdate();
    }

    @Override
    public boolean isCartLockedDown(EntityMinecart cart)
    {
        return !powered && lockedCart == cart && delay == 0;
    }
}
