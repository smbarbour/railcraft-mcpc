package railcraft.common.tracks;

import railcraft.common.api.tracks.ITrackReversable;
import railcraft.common.api.tracks.ITrackPowered;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.EntityMinecart;
import net.minecraft.server.NBTTagCompound;

public class TrackControl extends TrackBaseRailcraft implements ITrackPowered, ITrackReversable
{

    private boolean powered = false;
    private boolean reversed = false;
    private static final double BOOST_AMOUNT = 0.01;
    private static final double SLOW_AMOUNT = 0.02;

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.CONTROL;
    }

    @Override
    public int getTextureIndex()
    {
        int texture = getTrackType().getTextureIndex();
        if(isPowered() ^ reversed) {
            return texture + 1;
        }
        return texture;
    }

    @Override
    public int getPowerPropagation()
    {
        return 16;
    }

    @Override
    public void onMinecartPass(EntityMinecart cart)
    {
        int meta = tileEntity.k();
        if(meta == 0 || meta == 4 || meta == 5) {
            if(cart.motZ <= 0) {
                if(isPowered() ^ !reversed) {
                    cart.motZ -= BOOST_AMOUNT;
                } else {
                    cart.motZ += SLOW_AMOUNT;
                }
            } else if(cart.motZ >= 0) {
                if(!isPowered() ^ !reversed) {
                    cart.motZ += BOOST_AMOUNT;
                } else {
                    cart.motZ -= SLOW_AMOUNT;
                }
            }
        } else if(meta == 1 || meta == 2 || meta == 3) {
            if(cart.motX <= 0) {
                if(isPowered() ^ reversed) {
                    cart.motX -= BOOST_AMOUNT;
                } else {
                    cart.motX += SLOW_AMOUNT;
                }
            } else if(cart.motX >= 0) {
                if(!isPowered() ^ reversed) {
                    cart.motX += BOOST_AMOUNT;
                } else {
                    cart.motX -= SLOW_AMOUNT;
                }
            }
        }
    }

    @Override
    public boolean isPowered()
    {
        return powered;
    }

    @Override
    public void setPowered(boolean powered)
    {
        this.powered = powered;
    }

    @Override
    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setBoolean("powered", powered);
        nbttagcompound.setBoolean("reversed", reversed);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        powered = nbttagcompound.getBoolean("powered");
        reversed = nbttagcompound.getBoolean("reversed");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(powered);
        data.writeBoolean(reversed);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        powered = data.readBoolean();
        reversed = data.readBoolean();

        markBlockNeedsUpdate();
    }

    @Override
    public boolean isReversed()
    {
        return reversed;
    }

    @Override
    public void setReversed(boolean reversed)
    {
        this.reversed = reversed;
    }
}
