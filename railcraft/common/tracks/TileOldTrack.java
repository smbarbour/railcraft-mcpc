package railcraft.common.tracks;

import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import railcraft.common.RailcraftTileEntity;
import railcraft.common.api.tracks.ITrackInstance;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public abstract class TileOldTrack extends RailcraftTileEntity
{

    protected final ITrackInstance track;

    protected TileOldTrack(ITrackInstance t)
    {
        track = t;
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        TileEntity tile = new TileTrack(track);
        world.setTileEntity(x, y, z, tile);
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);

        track.readFromNBT(data);
    }

    @Override
    public int getId()
    {
        return -1;
    }
}
