package railcraft.common.tracks;

import railcraft.common.api.tracks.ITrackReversable;
import railcraft.common.api.tracks.ITrackPowered;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;

public class TrackSpeedTransition extends TrackSpeed implements ITrackPowered, ITrackReversable
{

    private boolean powered = false;
    private boolean reversed = false;
    private static final double BOOST_AMOUNT = 0.04;
    private static final double SLOW_FACTOR = 0.65;
    private static final double BOOST_THRESHOLD = 0.01;

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.SPEED_TRANSITION;
    }

    @Override
    public int getTextureIndex()
    {
        if(!isPowered()) {
            if(isReversed()) {
                return getTrackType().getTextureIndex() + 3;
            }
            return getTrackType().getTextureIndex() + 1;
        }
        if(isReversed()) {
            return getTrackType().getTextureIndex() + 2;
        }
        return getTrackType().getTextureIndex();
    }

    @Override
    public int getPowerPropagation()
    {
        return 16;
    }

    @Override
    public boolean isFlexibleRail()
    {
        return false;
    }

    @Override
    public void onMinecartPass(EntityMinecart cart)
    {
        testCartSpeed(cart);
        boolean highSpeed = cart.getEntityData().getBoolean("HighSpeed");
        if(powered) {
            double speed = Math.sqrt(cart.motX * cart.motX + cart.motZ * cart.motZ);
            if(speed > BOOST_THRESHOLD) {
                int meta = tileEntity.k();
                if(meta == 0 || meta == 4 || meta == 5) {
                    if(reversed ^ cart.motZ < 0) {
                        boostCartSpeed(cart, speed);
                    } else {
                        slowOrNormalCartSpeed(cart, highSpeed);
                    }
                } else if(meta == 1 || meta == 2 || meta == 3) {
                    if(!reversed ^ cart.motX < 0) {
                        boostCartSpeed(cart, speed);
                    } else {
                        slowOrNormalCartSpeed(cart, highSpeed);
                    }
                }
            }
        } else {
            if(highSpeed) {
                int meta = tileEntity.k();
                if(meta == 0 || meta == 4 || meta == 5) {
                    if(reversed ^ cart.motZ > 0) {
                        slowCartSpeed(cart);
                    }
                } else if(meta == 1 || meta == 2 || meta == 3) {
                    if(!reversed ^ cart.motX > 0) {
                        slowCartSpeed(cart);
                    }
                }
                cart.motY = 0.0D;
            } else {
                normalCartSpeed(cart);
            }
        }
    }

    private void boostCartSpeed(EntityMinecart cart, double currentSpeed)
    {
        cart.motX += (cart.motX / currentSpeed) * BOOST_AMOUNT;
        cart.motZ += (cart.motZ / currentSpeed) * BOOST_AMOUNT;
    }

    private void slowCartSpeed(EntityMinecart cart)
    {
        cart.motX *= SLOW_FACTOR;
        cart.motZ *= SLOW_FACTOR;
    }

    private void slowOrNormalCartSpeed(EntityMinecart cart, boolean highSpeed)
    {
        if(highSpeed) {
            slowCartSpeed(cart);
        } else {
            normalCartSpeed(cart);
        }
    }

    private void normalCartSpeed(EntityMinecart cart)
    {
        if(Math.abs(cart.motX) > 0.01) {
            cart.motX = Math.copySign(0.38f, cart.motX);
        }
        if(Math.abs(cart.motZ) > 0.01) {
            cart.motZ = Math.copySign(0.38f, cart.motZ);
        }
    }

    @Override
    public boolean isPowered()
    {
        return powered;
    }

    @Override
    public void setPowered(boolean powered)
    {
        this.powered = powered;
    }

    @Override
    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setBoolean("powered", powered);
        nbttagcompound.setBoolean("reversed", reversed);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        powered = nbttagcompound.getBoolean("powered");
        reversed = nbttagcompound.getBoolean("reversed");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(powered);
        data.writeBoolean(reversed);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        powered = data.readBoolean();
        reversed = data.readBoolean();

        markBlockNeedsUpdate();
    }

    @Override
    public boolean isReversed()
    {
        return reversed;
    }

    @Override
    public void setReversed(boolean r)
    {
        reversed = r;
    }
}
