package railcraft.common.tracks;

import railcraft.common.api.tracks.EnumTrackMeta;
import net.minecraft.server.*;
import railcraft.common.RailcraftBlocks;

public class TrackSuspended extends TrackBaseRailcraft
{

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.SUSPENDED;
    }

    @Override
    public int getTextureIndex()
    {
        return getTrackSpec().getTextureIndex();
    }

    @Override
    public boolean canMakeSlopes()
    {
        return false;
    }

    @Override
    public void onBlockPlaced(int side)
    {
        if(!isSupported()) {
            breakRail();
        }
    }

    @Override
    public void onNeighborBlockChange(int id)
    {
        World world = tileEntity.world;
        int i = tileEntity.x;
        int j = tileEntity.y;
        int k = tileEntity.z;
        if(isSupported()) {
            int myId = RailcraftBlocks.getBlockTrack().id;
            if(id != myId) {
                world.applyPhysics(i + 1, j, k, myId);
                world.applyPhysics(i - 1, j, k, myId);
                world.applyPhysics(i, j, k + 1, myId);
                world.applyPhysics(i, j, k - 1, myId);
            }
        } else {
            breakRail();
        }
    }

    private void breakRail()
    {
        BlockTrack block = (BlockTrack)RailcraftBlocks.getBlockTrack();
        block.b(tileEntity.world, tileEntity.x, tileEntity.y, tileEntity.z, 0, 0);
        tileEntity.world.setTypeId(tileEntity.x, tileEntity.y, tileEntity.z, 0);
    }

    private static boolean isSupportedRail(World world, int i, int j, int k, int meta)
    {
        if(!BlockMinecartTrack.g(world, i, j, k)) {
            return false;
        }
        if(isSupportedBelow(world, i, j, k)) {
            return true;
        }
        if(meta == EnumTrackMeta.NORTH_SOUTH.getValue()) {
            if(isSupportedBelow(world, i, j, k + 1)) {
                return true;
            }
            if(isSupportedBelow(world, i, j, k - 1)) {
                return true;
            }
            return false;
        } else if(meta == EnumTrackMeta.EAST_WEST.getValue()) {
            if(isSupportedBelow(world, i + 1, j, k)) {
                return true;
            }
            if(isSupportedBelow(world, i - 1, j, k)) {
                return true;
            }
            return false;
        }
        return false;
    }

    private static boolean isSupportedBelow(World world, int i, int j, int k)
    {
        if(!world.isLoaded(i, j, k)) {
            return true;
        }
        if(BlockMinecartTrack.g(world, i, j, k)) {
            return world.isBlockSolidOnSide(i, j - 1, k, 1);
        }
        return false;
    }

    private boolean isSupported()
    {
        int meta = tileEntity.k();
        return isSupported(tileEntity.world, tileEntity.x, tileEntity.y, tileEntity.z, meta);
    }

    private static boolean isSupported(World world, int i, int j, int k, int meta)
    {
        if(isSupportedRail(world, i, j, k, meta)) {
            return true;
        }
        if(meta == EnumTrackMeta.NORTH_SOUTH.getValue()) {
            if(isSupportedRail(world, i, j, k + 1, meta)
                || isSupportedRail(world, i, j, k - 1, meta)) {
                return true;
            }
            return false;
        } else if(meta == EnumTrackMeta.EAST_WEST.getValue()) {
            if(isSupportedRail(world, i + 1, j, k, meta)
                || isSupportedRail(world, i - 1, j, k, meta)) {
                return true;
            }
            return false;
        }
        return false;
    }

    @Override
    public boolean canPlaceRailAt(World world, int i, int j, int k)
    {
        if(world.getTypeId(i, j, k) != 0) {
            return false;
        }
        if(BlockMinecartTrack.g(world, i, j - 1, k)) {
            return false;
        }
        if(BlockMinecartTrack.g(world, i, j + 1, k)) {
            return false;
        }
        if(isSupported(world, i, j, k, 0) || isSupported(world, i, j, k, 1)) {
            return true;
        }
        return false;
    }
}
