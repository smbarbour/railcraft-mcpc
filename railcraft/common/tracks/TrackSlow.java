package railcraft.common.tracks;


import railcraft.common.tracks.speedcontroller.SpeedControllerSlow;

public class TrackSlow extends TrackBaseRailcraft
{
    public TrackSlow(){
        speedController = SpeedControllerSlow.getInstance();
    }

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.SLOW;
    }

    @Override
    public int getTextureIndex()
    {
        int meta = tileEntity.k();
        if(meta >= 6) {
            return getTrackType().getTextureIndex() - 16;
        }
        return getTrackType().getTextureIndex();
    }

    @Override
    public boolean isFlexibleRail()
    {
        return true;
    }
}
