package railcraft.common.tracks;

import railcraft.common.tracks.speedcontroller.SpeedControllerSlow;

public class TrackSlowJunction extends TrackJunction
{

    public TrackSlowJunction()
    {
        speedController = SpeedControllerSlow.getInstance();
    }

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.SLOW_JUNCTION;
    }
}
