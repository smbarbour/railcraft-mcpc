package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackDecoupler;
import railcraft.common.tracks.TrackDecoupler;

public class TileRailDecoupler extends TileOldTrack
{

    public TileRailDecoupler()
    {
        super(new TrackDecoupler());
    }
}
