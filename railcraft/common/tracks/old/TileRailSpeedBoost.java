package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackSpeedBoost;

public class TileRailSpeedBoost extends TileOldTrack
{

    public TileRailSpeedBoost()
    {
        super(new TrackSpeedBoost());
    }
}
