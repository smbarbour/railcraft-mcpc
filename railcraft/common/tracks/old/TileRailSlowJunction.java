package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackSlowJunction;

public class TileRailSlowJunction extends TileOldTrack
{

    public TileRailSlowJunction()
    {
        super(new TrackSlowJunction());
    }

}
