package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackSwitch;

public class TileRailSwitch extends TileOldTrack
{

    public TileRailSwitch()
    {
        super(new TrackSwitch());
    }
}
