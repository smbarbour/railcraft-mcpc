package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackPriming;

public class TileRailPriming extends TileOldTrack
{

    public TileRailPriming()
    {
        super(new TrackPriming());
    }
}
