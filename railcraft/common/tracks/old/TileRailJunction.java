package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackJunction;

public class TileRailJunction extends TileOldTrack
{

    public TileRailJunction()
    {
        super(new TrackJunction());
    }
}
