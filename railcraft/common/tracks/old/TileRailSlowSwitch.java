package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackSlowSwitch;

public class TileRailSlowSwitch extends TileOldTrack
{

    public TileRailSlowSwitch()
    {
        super(new TrackSlowSwitch());
    }
}
