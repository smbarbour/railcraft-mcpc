package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackHoldingTrain;

public class TileRailHoldingTrain extends TileOldTrack
{

    public TileRailHoldingTrain()
    {
        super(new TrackHoldingTrain());
    }
}
