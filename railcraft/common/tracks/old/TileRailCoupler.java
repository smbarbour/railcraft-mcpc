package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackCoupler;
import railcraft.common.tracks.TrackCoupler;

public class TileRailCoupler extends TileOldTrack
{

    public TileRailCoupler()
    {
        super(new TrackCoupler());
    }
}
