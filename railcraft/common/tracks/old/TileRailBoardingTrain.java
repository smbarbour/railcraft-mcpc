package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackBoardingTrain;
import railcraft.common.tracks.TrackBoardingTrain;

public class TileRailBoardingTrain extends TileOldTrack
{

    public TileRailBoardingTrain()
    {
        super(new TrackBoardingTrain());
    }
}
