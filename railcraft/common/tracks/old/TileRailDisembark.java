package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackDisembark;

public class TileRailDisembark extends TileOldTrack
{

    public TileRailDisembark()
    {
        super(new TrackDisembark());
    }
}
