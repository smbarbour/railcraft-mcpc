package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackGatedOneWay;

public class TileRailGatedOneWay extends TileOldTrack
{

    public TileRailGatedOneWay()
    {
        super(new TrackGatedOneWay());
    }
}
