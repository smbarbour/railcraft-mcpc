package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackOneWay;
import railcraft.common.tracks.TrackOneWay;

public class TileRailOneway extends TileOldTrack
{


    public TileRailOneway()
    {
        super(new TrackOneWay());
    }
}
