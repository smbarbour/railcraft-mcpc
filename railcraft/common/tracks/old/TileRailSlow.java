package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackSlow;

public class TileRailSlow extends TileOldTrack
{

    public static final float MAX_SPEED = 0.12f;

    public TileRailSlow()
    {
        super(new TrackSlow());
    }
}
