package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackSlowBooster;

public class TileRailSlowBooster extends TileOldTrack
{

    public TileRailSlowBooster()
    {
        super(new TrackSlowBooster());
    }
}
