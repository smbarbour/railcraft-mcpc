package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackSpeedTransition;

public class TileRailSpeedTransition extends TileOldTrack
{

    public TileRailSpeedTransition()
    {
        super(new TrackSpeedTransition());
    }
}
