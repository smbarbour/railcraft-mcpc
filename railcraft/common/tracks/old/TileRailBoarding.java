package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackBoarding;
import railcraft.common.tracks.TrackBoarding;

public class TileRailBoarding extends TileOldTrack
{

    public TileRailBoarding()
    {
        super(new TrackBoarding());
    }
}
