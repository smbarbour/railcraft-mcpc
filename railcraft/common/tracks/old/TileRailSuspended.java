package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackSuspended;

public class TileRailSuspended extends TileOldTrack
{

    public TileRailSuspended()
    {
        super(new TrackSuspended());
    }
}
