package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackGated;

public class TileRailGated extends TileOldTrack
{

    public TileRailGated()
    {
        super(new TrackGated());
    }
}
