package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackHolding;

public class TileRailHolding extends TileOldTrack
{

    public TileRailHolding()
    {
        super(new TrackHolding());
    }
}
