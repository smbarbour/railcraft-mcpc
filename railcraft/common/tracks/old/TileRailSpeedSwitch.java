package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackSpeedSwitch;

public class TileRailSpeedSwitch extends TileOldTrack
{

    public TileRailSpeedSwitch()
    {
        super(new TrackSpeedSwitch());
    }
}
