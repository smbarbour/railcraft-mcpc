package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackSpeed;

public class TileRailSpeed extends TileOldTrack
{

    public TileRailSpeed()
    {
        super(new TrackSpeed());
    }
}
