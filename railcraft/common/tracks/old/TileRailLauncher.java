package railcraft.common.tracks.old;

import railcraft.common.tracks.TileOldTrack;
import railcraft.common.tracks.TrackLauncher;

public class TileRailLauncher extends TileOldTrack
{

    public TileRailLauncher()
    {
        super(new TrackLauncher());
    }
}
