package railcraft.common.tracks;

import railcraft.common.tracks.old.TileRailGated;
import net.minecraft.server.*;

public class BlockFenceReplacement extends BlockFence
{

    public BlockFenceReplacement(int i, int j)
    {
        this(i, j, Material.WOOD);
    }

    public BlockFenceReplacement(int i, int j, Material material)
    {
        super(i, j, material);
        c(2.0F);
        b(5F);
        a(e);
        a("fence");
    }

    @Override
    public boolean c(IBlockAccess iblockaccess, int i, int j, int k)
    {
        int id = iblockaccess.getTypeId(i, j, k);
        if(id == this.id || id == Block.FENCE_GATE.id) {
            return true;
        }
        TileEntity tile = iblockaccess.getTileEntity(i, j, k);
        if(tile instanceof TileTrack) {
            if(((TileTrack)tile).getTrackInstance() instanceof TrackGated) {
                return true;
            }
        }
        Block block = Block.byId[id];
        if(block != null && block.material.j() && block.b()) {
            return block.material != Material.PUMPKIN;
        }
        return false;
    }
}
