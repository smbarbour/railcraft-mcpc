package railcraft.common.tracks;


import railcraft.common.api.tracks.EnumTrackMeta;
import railcraft.common.api.tracks.ITrackSwitch;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;
import railcraft.Railcraft;
import railcraft.common.api.CartTools;

public class TrackSwitch extends TrackBaseRailcraft implements ITrackSwitch
{

    private static final int SPRING_DURATION = 15;
    private byte spring;
    private boolean reversed;
    private boolean direction;
    private boolean switched;

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.SWITCH;
    }

    @Override
    public int getTextureIndex()
    {
        int texture = getTrackType().getTextureIndex();
        if(reversed) {
            texture += 2;
        }
        if(switched || spring > 0) {
            texture += 1;
        }
        return texture;
    }

    @Override
    public int getBasicRailMetadata(EntityMinecart cart)
    {
        int meta = tileEntity.k();
        if(cart != null && (switched || spring > 0)) {
            if(meta == EnumTrackMeta.NORTH_SOUTH.getValue()) {
                if(direction) {
                    if(reversed) {
                        meta = EnumTrackMeta.WEST_SOUTH_CORNER.getValue();
                    } else {
                        meta = EnumTrackMeta.WEST_NORTH_CORNER.getValue();
                    }
                } else {
                    if(reversed) {
                        meta = EnumTrackMeta.EAST_NORTH_CORNER.getValue();
                    } else {
                        meta = EnumTrackMeta.EAST_SOUTH_CORNER.getValue();
                    }
                }
            } else if(meta == EnumTrackMeta.EAST_WEST.getValue()) {
                if(direction) {
                    if(reversed) {
                        meta = EnumTrackMeta.WEST_NORTH_CORNER.getValue();
                    } else {
                        meta = EnumTrackMeta.EAST_NORTH_CORNER.getValue();
                    }
                } else {
                    if(reversed) {
                        meta = EnumTrackMeta.EAST_SOUTH_CORNER.getValue();
                    } else {
                        meta = EnumTrackMeta.WEST_SOUTH_CORNER.getValue();
                    }
                }
            }
        }
        return meta;
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        boolean springState = spring == 0;
        if(spring > 0) {
            spring--;
        }
        if(minecartOnDiverging()) {
            spring = SPRING_DURATION;
        }
        if(springState != (spring == 0)) {
            markBlockNeedsUpdate();
        }
    }

    @Override
    public void onBlockPlaced(int side)
    {
        if(Railcraft.gameIsHost()) {
            determineDirection();
            markBlockNeedsUpdate();
        }
    }

    @Override
    public void onNeighborBlockChange(int id)
    {
        if(BlockMinecartTrack.d(id) && Railcraft.gameIsHost()) {
            determineOrientation();
            determineDirection();
            markBlockNeedsUpdate();
        }
        super.onNeighborBlockChange(id);
    }

    private void determineOrientation()
    {
        int i = tileEntity.x;
        int j = tileEntity.y;
        int k = tileEntity.z;
        int meta = tileEntity.k();
        if(meta == EnumTrackMeta.NORTH_SOUTH.getValue()) {
            if(BlockMinecartTrack.g(tileEntity.world, i + 1, j, k) && BlockMinecartTrack.g(tileEntity.world, i - 1, j, k)) {
                tileEntity.world.setData(i, j, k, 1);
            }
        } else if(meta == EnumTrackMeta.EAST_WEST.getValue()) {
            if(BlockMinecartTrack.g(tileEntity.world, i, j, k + 1) && BlockMinecartTrack.g(tileEntity.world, i, j, k - 1)) {
                tileEntity.world.setData(i, j, k, 0);
            }
        }
    }

    private void determineDirection()
    {
        int i = tileEntity.x;
        int j = tileEntity.y;
        int k = tileEntity.z;
        int meta = tileEntity.k();
        if(meta == EnumTrackMeta.NORTH_SOUTH.getValue()) {
            int ii = i;
            if(BlockMinecartTrack.g(tileEntity.world, i - 1, j, k)) {
                ii--;
                direction = true; // West
            } else {
                ii++;
                direction = false; // East
            }
            if(BlockMinecartTrack.g(tileEntity.world, ii, j, k)) {
                int otherMeta = tileEntity.world.getData(ii, j, k);
                if(otherMeta == EnumTrackMeta.NORTH_SOUTH.getValue()) {
                    tileEntity.world.setData(ii, j, k, EnumTrackMeta.EAST_WEST.getValue());
                }
            }
        } else if(meta == EnumTrackMeta.EAST_WEST.getValue()) {
            if(BlockMinecartTrack.g(tileEntity.world, i, j, k - 1)) {
                direction = true; // North
            } else {
                direction = false; // South
            }
        }
    }

    private boolean minecartOnDiverging()
    {
        int i = tileEntity.x;
        int j = tileEntity.y;
        int k = tileEntity.z;
        int meta = tileEntity.k();
        if(meta == EnumTrackMeta.NORTH_SOUTH.getValue()) {
            if(direction) {
                i--;
            } else {
                i++;
            }
        } else if(meta == EnumTrackMeta.EAST_WEST.getValue()) {
            if(direction) {
                k--;
            } else {
                k++;
            }
        }
        return CartTools.isMinecartOnRailAt(tileEntity.world, i, j, k, 0.4f);
    }

    @Override
    public boolean isFlexibleRail()
    {
        return false;
    }

    @Override
    public boolean canMakeSlopes()
    {
        return false;
    }

    @Override
    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);

        nbttagcompound.setBoolean("Reversed", reversed);
        nbttagcompound.setBoolean("Direction", direction);
        nbttagcompound.setBoolean("Switched", switched);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);

        reversed = nbttagcompound.getBoolean("Reversed");
        direction = nbttagcompound.getBoolean("Direction");
        switched = nbttagcompound.getBoolean("Switched");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(reversed);
        data.writeBoolean(direction);
        data.writeBoolean(switched);
        data.writeByte(spring);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        reversed = data.readBoolean();
        direction = data.readBoolean();
        switched = data.readBoolean();
        spring = data.readByte();

        markBlockNeedsUpdate();
    }

    @Override
    public boolean isReversed()
    {
        return reversed;
    }

    @Override
    public void setReversed(boolean reversed)
    {
        this.reversed = reversed;
        markBlockNeedsUpdate();
    }

    @Override
    public boolean isSwitched()
    {
        return switched;
    }

    @Override
    public void setSwitched(boolean switched)
    {
        this.switched = switched;
        markBlockNeedsUpdate();
    }

    @Override
    public boolean getDirection()
    {
        return direction;
    }
}
