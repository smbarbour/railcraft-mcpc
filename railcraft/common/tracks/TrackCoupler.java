package railcraft.common.tracks;

import railcraft.common.api.tracks.ITrackPowered;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;
import railcraft.common.api.CartTools;
import railcraft.common.api.ILinkageManager;

public class TrackCoupler extends TrackBaseRailcraft implements ITrackPowered
{

    private EntityMinecart taggedCart;
    private boolean powered = false;

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.COUPLER;
    }

    @Override
    public int getTextureIndex()
    {
        if(isPowered()) {
            return getTrackType().getTextureIndex();
        }
        return getTrackType().getTextureIndex() + 1;
    }

    @Override
    public void onMinecartPass(EntityMinecart cart)
    {
        if(isPowered()) {
            ILinkageManager lm = CartTools.getLinkageManager();
            lm.createLink(taggedCart, cart);
            taggedCart = cart;
        }
    }

    @Override
    public boolean isPowered()
    {
        return powered;
    }

    @Override
    public void setPowered(boolean powered)
    {
        this.powered = powered;
    }

    @Override
    public int getPowerPropagation()
    {
        return 8;
    }

    @Override
    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setBoolean("powered", powered);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        powered = nbttagcompound.getBoolean("powered");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(powered);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        boolean p = data.readBoolean();

        if(p != powered) {
            powered = p;
            markBlockNeedsUpdate();
        }
    }
}
