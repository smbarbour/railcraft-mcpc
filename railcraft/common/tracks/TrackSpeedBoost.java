package railcraft.common.tracks;


import railcraft.common.api.tracks.ITrackPowered;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.EntityMinecart;
import net.minecraft.server.NBTTagCompound;

public class TrackSpeedBoost extends TrackSpeed implements ITrackPowered
{

    private boolean powered = false;
    private static final double BOOST_AMOUNT = 0.06;
    private static final double SLOW_FACTOR = 0.65;
    private static final double BOOST_THRESHOLD = 0.01;

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.SPEED_BOOST;
    }

    @Override
    public int getTextureIndex()
    {
        if(!isPowered()) {
            return getTrackType().getTextureIndex() + 1;
        }
        return getTrackType().getTextureIndex();
    }

    @Override
    public int getPowerPropagation()
    {
        return 16;
    }

    @Override
    public void onMinecartPass(EntityMinecart cart)
    {
        testCartSpeed(cart);
        if(powered) {
            double speed = Math.sqrt(cart.motX * cart.motX + cart.motZ * cart.motZ);
            if(speed > BOOST_THRESHOLD) {
                cart.motX += (cart.motX / speed) * BOOST_AMOUNT;
                cart.motZ += (cart.motZ / speed) * BOOST_AMOUNT;
            }
        } else {
            boolean highSpeed = cart.getEntityData().getBoolean("HighSpeed");
            if(highSpeed) {
                cart.motX *= SLOW_FACTOR;
                cart.motY = 0.0D;
                cart.motZ *= SLOW_FACTOR;
            } else {
                if(Math.abs(cart.motX) > 0) {
                    cart.motX = Math.copySign(0.38f, cart.motX);
                }
                if(Math.abs(cart.motZ) > 0) {
                    cart.motZ = Math.copySign(0.38f, cart.motZ);
                }
            }
        }
    }

    @Override
    public boolean isFlexibleRail()
    {
        return false;
    }

    @Override
    public boolean isPowered()
    {
        return powered;
    }

    @Override
    public void setPowered(boolean powered)
    {
        this.powered = powered;
    }

    @Override
    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setBoolean("powered", powered);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        powered = nbttagcompound.getBoolean("powered");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(powered);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        powered = data.readBoolean();

        markBlockNeedsUpdate();
    }
}
