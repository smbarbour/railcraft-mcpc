package railcraft.common.tracks.speedcontroller;

import net.minecraft.server.EntityMinecart;
import railcraft.common.api.tracks.ITrackInstance;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class SpeedController
{

    private static SpeedController instance;

    public static SpeedController getInstance()
    {
        if(instance == null) {
            instance = new SpeedController();
        }
        return instance;
    }

    public float getMaxSpeed(ITrackInstance track, EntityMinecart cart)
    {
        return 0.4f;
    }
}
