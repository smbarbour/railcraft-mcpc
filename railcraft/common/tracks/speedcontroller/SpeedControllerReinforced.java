package railcraft.common.tracks.speedcontroller;

import net.minecraft.server.EntityMinecart;
import railcraft.common.api.tracks.EnumTrackMeta;
import railcraft.common.api.tracks.ITrackInstance;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class SpeedControllerReinforced extends SpeedController
{

    public static final float MAX_SPEED = 0.5f;
    public static final float CORNER_SPEED = 0.4f;
    private static SpeedControllerReinforced instance;

    public static SpeedControllerReinforced getInstance()
    {
        if(instance == null) {
            instance = new SpeedControllerReinforced();
        }
        return instance;
    }

    @Override
    public float getMaxSpeed(ITrackInstance track, EntityMinecart cart)
    {
        cart.setMaxSpeedRail(MAX_SPEED);
        switch (EnumTrackMeta.fromInt(track.getBasicRailMetadata(cart))) {
            case EAST_NORTH_CORNER:
            case EAST_SOUTH_CORNER:
            case WEST_NORTH_CORNER:
            case WEST_SOUTH_CORNER:
                return CORNER_SPEED;
            default:
                return MAX_SPEED;
        }
    }
}
