package railcraft.common.tracks.speedcontroller;

import net.minecraft.server.EntityMinecart;
import railcraft.common.api.tracks.ITrackInstance;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class SpeedControllerSlow extends SpeedController
{

    private static final float MAX_SPEED = 0.12f;
    private static SpeedControllerSlow instance;

    public static SpeedControllerSlow getInstance()
    {
        if(instance == null) {
            instance = new SpeedControllerSlow();
        }
        return instance;
    }

    @Override
    public float getMaxSpeed(ITrackInstance track, EntityMinecart cart)
    {
        return MAX_SPEED;
    }
}
