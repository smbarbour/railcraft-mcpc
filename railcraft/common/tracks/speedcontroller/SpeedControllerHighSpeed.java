package railcraft.common.tracks.speedcontroller;

import net.minecraft.server.Block;
import net.minecraft.server.BlockMinecartTrack;
import net.minecraft.server.EntityMinecart;
import net.minecraft.server.World;
import railcraft.common.RailcraftConfig;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.tracks.EnumTrackMeta;
import railcraft.common.api.tracks.ITrackInstance;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class SpeedControllerHighSpeed extends SpeedController
{

    private static final int CORNER_SLOW_DURATION = 80;
    private static final int LOOK_AHEAD_DIST = 2;
    private static float HIGH_MAX_SPEED_UP_SLOPE = 0.5f;
    private static float HIGH_MAX_SPEED_DOWN_SLOPE = 0.8f;
    private static float HIGH_MAX_SPEED_CORNER = 0.4f;
    private static SpeedController instance;

    public static SpeedController getInstance()
    {
        if(instance == null) {
            instance = new SpeedControllerHighSpeed();
        }
        return instance;
    }

    @Override
    public float getMaxSpeed(ITrackInstance track, EntityMinecart cart)
    {
        return calculateMaxSpeed(track.getWorld(), cart, track.getX(), track.getY(), track.getZ());
    }

    private float calculateMaxSpeed(World world, EntityMinecart cart, int i, int j, int k)
    {
        float max = RailcraftConfig.getMaxHighSpeed();
        if(cart == null) {
            return max;
        }
//        int corner = cart.getCustomIntVariable("HSCorners");
//        if(corner != 0)
//        {
//            System.out.println("slowed");
//            cart.putCustomIntVariable("HSCorners", corner - 1);
//            if(corner - 1 == 0)
//            {
//                System.out.println("faster");
//            }
//            max = HIGH_MAX_SPEED_CORNER;
//        } else
//        {
        int dir = 0;
        float yaw = cart.yaw;
        if(yaw >= 360) {
            yaw -= 360;
        }
        if(yaw < 0) {
            yaw += 360;
        }
        dir = Math.round((yaw * 4) / 360);
        if(dir == 1) {
            dir = 2;
        } else if(dir == 2) {
            dir = 4;
        } else if(dir == 3) {
            dir = 3;
        } else {
            dir = 5;
        }
        max = speedForCurrentTrack(world, cart, i, j, k, dir);
        if(max == RailcraftConfig.getMaxHighSpeed()) {
            max = speedForNextTrack(world, cart, i, j, k, dir);
        }
//        if(max == HIGH_MAX_SPEED)
//        {
//            System.out.println("faster");
//        } else
//        {
//            System.out.println("slower");
//        }

//        }
        return max;
    }

    private float speedForCurrentTrack(World world, EntityMinecart cart, int i, int j, int k, int dir)
    {
        int id = world.getTypeId(i, j, k);
        if(BlockMinecartTrack.d(id)) {
            int meta = ((BlockMinecartTrack)Block.byId[id]).getBasicRailMetadata(world, cart, i, j, k);
//            if(meta >= 6)
//            {
//                cart.putCustomIntVariable("HSCorners", CORNER_SLOW_DURATION);
//                return HIGH_MAX_SPEED_CORNER;
//            }
            if((EnumTrackMeta.EAST_SLOPE.isEqual(meta) && dir == 5)
                || (EnumTrackMeta.WEST_SLOPE.isEqual(meta) && dir == 4)
                || (EnumTrackMeta.NORTH_SLOPE.isEqual(meta) && dir == 2)
                || (EnumTrackMeta.SOUTH_SLOPE.isEqual(meta) && dir == 3)) {
                return HIGH_MAX_SPEED_UP_SLOPE;
            }
//            if((meta == 2 && dir == 4)
//                || (meta == 3 && dir == 5)
//                || (meta == 4 && dir == 3)
//                || (meta == 5 && dir == 2))
//            {
//                return HIGH_MAX_SPEED_DOWN_SLOPE;
//            }
        }
        return RailcraftConfig.getMaxHighSpeed();
    }

    private float speedForNextTrack(World world, EntityMinecart cart, int i, int j, int k, int dir)
    {
        for(int look = 0; look < LOOK_AHEAD_DIST; look++) {
            i = GeneralTools.getXOnSide(i, dir);
            k = GeneralTools.getZOnSide(k, dir);
            int id = world.getTypeId(i, j, k);
            if(!BlockMinecartTrack.d(id)) {
                id = world.getTypeId(i, j + 1, k);
                if(BlockMinecartTrack.d(id)) {
                    j = j + 1;
                } else {
                    id = world.getTypeId(i, j - 1, k);
                    if(BlockMinecartTrack.d(id)) {
                        j = j - 1;
                    }
                }
            }
            if(BlockMinecartTrack.d(id)) {
                int meta = ((BlockMinecartTrack)Block.byId[id]).getBasicRailMetadata(world, cart, i, j, k);
//                if(meta >= 6)
//                {
//                    cart.putCustomIntVariable("HSCorners", CORNER_SLOW_DURATION);
//                    return HIGH_MAX_SPEED_CORNER;
//                }
//                if(EnumTrackMeta.EAST_SLOPE.isEqual(meta)) System.out.println("east " + dir);
//                if(EnumTrackMeta.NORTH_SLOPE.isEqual(meta)) System.out.println("north " + dir);
                if((EnumTrackMeta.EAST_SLOPE.isEqual(meta) && dir == 5)
                    || (EnumTrackMeta.WEST_SLOPE.isEqual(meta) && dir == 4)
                    || (EnumTrackMeta.NORTH_SLOPE.isEqual(meta) && dir == 2)
                    || (EnumTrackMeta.SOUTH_SLOPE.isEqual(meta) && dir == 3)) {
                    return HIGH_MAX_SPEED_UP_SLOPE;
                }
//                if((meta == 2 && dir == 4)
//                    || (meta == 3 && dir == 5)
//                    || (meta == 4 && dir == 3)
//                    || (meta == 5 && dir == 2))
//                {
//                    return HIGH_MAX_SPEED_DOWN_SLOPE;
//                }
            } else {
                break;
            }
        }
        return RailcraftConfig.getMaxHighSpeed();
    }
}
