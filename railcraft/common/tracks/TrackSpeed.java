package railcraft.common.tracks;

import net.minecraft.server.EntityMinecart;
import railcraft.common.tracks.speedcontroller.SpeedControllerHighSpeed;
import railcraft.common.RailcraftConfig;

public class TrackSpeed extends TrackBaseRailcraft
{

    public TrackSpeed()
    {
        speedController = SpeedControllerHighSpeed.getInstance();
    }

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.SPEED;
    }

    @Override
    public int getTextureIndex()
    {
        int meta = tileEntity.k();
        if(meta >= 6) {
            return getTrackType().getTextureIndex() - 16;
        }
        return getTrackType().getTextureIndex();
    }

    @Override
    public boolean isFlexibleRail()
    {
        return true;
    }

    @Override
    public void onMinecartPass(EntityMinecart cart)
    {
        boolean highSpeed = cart.getEntityData().getBoolean("HighSpeed");
        float normalMax = 0.39f;
        if(!highSpeed) {
            cart.motX = Math.copySign(Math.min(normalMax, Math.abs(cart.motX)), cart.motX);
            cart.motZ = Math.copySign(Math.min(normalMax, Math.abs(cart.motZ)), cart.motZ);
        }
    }

    protected void testCartSpeed(EntityMinecart cart)
    {
        float normalMax = 0.39f;
        boolean highSpeed = cart.getEntityData().getBoolean("HighSpeed");
        if(!highSpeed) {
            if(Math.abs(cart.motX) > normalMax) {
                cart.motX = Math.copySign(normalMax, cart.motX);
                cart.getEntityData().setBoolean("HighSpeed", true);
                cart.setMaxSpeedRail(RailcraftConfig.getMaxHighSpeed());
            }
            if(Math.abs(cart.motZ) > normalMax) {
                cart.motZ = Math.copySign(normalMax, cart.motZ);
                cart.getEntityData().setBoolean("HighSpeed", true);
                cart.setMaxSpeedRail(RailcraftConfig.getMaxHighSpeed());
            }
        }
    }
}
