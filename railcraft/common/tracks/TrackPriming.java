package railcraft.common.tracks;

import railcraft.common.api.tracks.ITrackPowered;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;
import railcraft.GuiHandler;
import railcraft.common.EnumGui;
import railcraft.common.api.ICrowbar;
import railcraft.common.carts.IPrimableCart;
import railcraft.common.api.INetworkEntity;

public class TrackPriming extends TrackBaseRailcraft implements ITrackPowered, INetworkEntity
{

    private boolean powered = false;
    private short fuse = 80;
    public static final short MAX_FUSE = 500;
    public static final short MIN_FUSE = 0;

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.PRIMING;
    }

    @Override
    public int getTextureIndex()
    {
        if(!isPowered()) {
            return getTrackType().getTextureIndex() + 1;
        }
        return getTrackType().getTextureIndex();
    }

    @Override
    public boolean blockActivated(EntityHuman player)
    {
        ItemStack current = player.U();
        if(current != null && current.getItem() instanceof ICrowbar) {
            GuiHandler.openGui(EnumGui.RAIL_PRIMING, player, tileEntity.world, tileEntity.x, tileEntity.y, tileEntity.z);
            if(current.d()) {
                current.damage(1, player);
            }
            return true;
        }
        return false;
    }

    @Override
    public void onMinecartPass(EntityMinecart cart)
    {
        if(isPowered()) {
            if(cart instanceof IPrimableCart) {
                IPrimableCart tnt = (IPrimableCart)cart;
                tnt.setFuse(fuse);
                tnt.setPrimed(true);
            }
        }
    }

    @Override
    public boolean isPowered()
    {
        return powered;
    }

    @Override
    public void setPowered(boolean powered)
    {
        this.powered = powered;
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(powered);
        data.writeShort(fuse);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        powered = data.readBoolean();
        fuse = data.readShort();

        markBlockNeedsUpdate();
    }

    @Override
    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setBoolean("powered", powered);
        nbttagcompound.setShort("fuse", getFuse());
    }

    @Override
    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        powered = nbttagcompound.getBoolean("powered");
        setFuse(nbttagcompound.getShort("fuse"));
    }

    public short getFuse()
    {
        return fuse;
    }

    public void setFuse(short f)
    {
        f = (short)Math.max(f, MIN_FUSE);
        f = (short)Math.min(f, MAX_FUSE);
        fuse = f;
    }
}
