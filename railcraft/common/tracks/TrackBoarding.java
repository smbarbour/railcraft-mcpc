package railcraft.common.tracks;

import railcraft.common.api.tracks.ITrackReversable;
import railcraft.common.api.tracks.ITrackPowered;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;
import railcraft.common.api.CartTools;
import railcraft.common.api.tracks.ITrackLockdown;

public class TrackBoarding extends TrackBaseRailcraft implements ITrackPowered, ITrackReversable, ITrackLockdown
{

    protected boolean reversed = false;
    protected boolean powered = false;
    protected EntityMinecart lockedCart;
    protected byte delay = 0;
    protected static double START_BOOST = 0.04;
    protected static double BOOST_FACTOR = 0.06;

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.BOARDING;
    }


    @Override
    public int getTextureIndex()
    {
        if(isPowered() || delay > 0) {
            if(isReversed()) {
                return getTrackType().getTextureIndex() + 2;
            }
            return getTrackType().getTextureIndex();
        }
        if(isReversed()) {
            return getTrackType().getTextureIndex() + 3;
        }
        return getTrackType().getTextureIndex() + 1;
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        if(lockedCart != null && lockedCart.dead) {
            lockedCart = null;
        }
        if(isPowered()) {
            delay = getDelayTIme();
            markBlockNeedsUpdate();
        } else if(delay > 0) {
            delay--;
            if(delay == 0) {
                lockedCart = null;
            }
            markBlockNeedsUpdate();
        }
    }

    protected byte getDelayTIme()
    {
        return 3;
    }

    protected void checkCart(EntityMinecart cart)
    {
        if(delay > 0 && cart != lockedCart) {
            delay = 0;
            lockedCart = cart;
            markBlockNeedsUpdate();
        }
    }

    @Override
    public void onMinecartPass(EntityMinecart cart)
    {
        checkCart(cart);
        int meta = tileEntity.k();
        if(isPowered() || delay > 0) {
            double speed = CartTools.getCartSpeedUncapped(cart);
            double boostX = START_BOOST;
            double boostZ = START_BOOST;
            if(speed > 0.005D) {
                boostX = (Math.abs(cart.motX) / speed) * BOOST_FACTOR;
                boostZ = (Math.abs(cart.motZ) / speed) * BOOST_FACTOR;
            }
            if(meta == 0 || meta == 4 || meta == 5) {
                if(isReversed()) {
                    cart.motZ += boostZ;
                } else {
                    cart.motZ -= boostZ;
                }
            } else if(meta == 1 || meta == 2 || meta == 3) {
                if(isReversed()) {
                    cart.motX -= boostX;
                } else {
                    cart.motX += boostX;
                }
            }
        } else {
            if(lockedCart == null) {
                lockedCart = cart;
            }
            if(lockedCart == cart) {
                cart.motX = 0.0D;
                cart.motZ = 0.0D;
                if(meta == 0 || meta == 4 || meta == 5) {
                    cart.locZ = tileEntity.z + 0.5D;
                } else {
                    cart.locX = tileEntity.x + 0.5D;
                }
            } else {
//                double distX = cart.locX - (i + 0.5D);
//                double distZ = cart.locZ - (k + 0.5D);
//                if(Math.abs(distX) < 1.0 && Math.abs(distX) > 0.0) {
//                    cart.motionX += (1.0 / distX) * 0.01;
//                }
//                if(Math.abs(distZ) < 1.0 && Math.abs(distZ) > 0.0) {
//                    cart.motionZ += (1.0 / distZ) * 0.01;
//                }
            }
        }
    }

    @Override
    public boolean isReversed()
    {
        return reversed;
    }

    @Override
    public void setReversed(boolean reversed)
    {
        this.reversed = reversed;
    }

    @Override
    public boolean isPowered()
    {
        return powered;
    }

    @Override
    public void setPowered(boolean powered)
    {
        this.powered = powered;
    }

    @Override
    public void writeToNBT(NBTTagCompound data)
    {
        super.writeToNBT(data);
        data.setBoolean("direction", reversed);
        data.setBoolean("powered", powered);
//        data.setByte("delay", delay);
    }

    @Override
    public void readFromNBT(NBTTagCompound data)
    {
        super.readFromNBT(data);
        reversed = data.getBoolean("direction");
        powered = data.getBoolean("powered");
//        delay = data.getByte("delay");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(powered);
        data.writeBoolean(reversed);
        data.writeByte(delay);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        powered = data.readBoolean();
        reversed = data.readBoolean();
        delay = data.readByte();

        markBlockNeedsUpdate();
    }

    @Override
    public boolean isCartLockedDown(EntityMinecart cart)
    {
        return !powered && lockedCart == cart && delay == 0;
    }
}
