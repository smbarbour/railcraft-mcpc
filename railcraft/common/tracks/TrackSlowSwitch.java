package railcraft.common.tracks;

import railcraft.common.tracks.speedcontroller.SpeedControllerSlow;

public class TrackSlowSwitch extends TrackSwitch
{

    public TrackSlowSwitch()
    {
        speedController = SpeedControllerSlow.getInstance();
    }

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.SLOW_SWITCH;
    }
}
