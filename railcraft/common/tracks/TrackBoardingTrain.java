package railcraft.common.tracks;

import net.minecraft.server.*;

public class TrackBoardingTrain extends TrackBoarding
{

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.BOARDING_TRAIN;
    }

    @Override
    protected byte getDelayTIme()
    {
        return 10;
    }

    @Override
    protected void checkCart(EntityMinecart cart)
    {
        if(delay > 0) {
            delay = getDelayTIme();
        }
    }
}
