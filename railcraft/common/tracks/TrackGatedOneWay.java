package railcraft.common.tracks;

import net.minecraft.server.EntityMinecart;
import railcraft.common.api.tracks.ITrackPowered;
import railcraft.common.api.tracks.ITrackReversable;

public class TrackGatedOneWay extends TrackGated implements ITrackReversable, ITrackPowered
{

    private static final double MOTION_MIN = 0.2;

    @Override
    public EnumTrack getTrackType()
    {
        return EnumTrack.GATED_ONEWAY;
    }

    @Override
    public int getTextureIndex()
    {
        int texture = getTrackType().getTextureIndex();
        if(isReversed()) {
            return texture + 1;
        }
        return texture;
    }

    @Override
    public void onMinecartPass(EntityMinecart cart)
    {
        if(isGateOpen()) {
            int meta = tileEntity.k();
            if(meta == 0) {
                if(isReversed()) {
                    cart.motZ = Math.max(Math.abs(cart.motZ), MOTION_MIN);
                } else {
                    cart.motZ = -Math.max(Math.abs(cart.motZ), MOTION_MIN);
                }
            } else if(meta == 1) {
                if(isReversed()) {
                    cart.motX = -Math.max(Math.abs(cart.motX), MOTION_MIN);
                } else {
                    cart.motX = Math.max(Math.abs(cart.motX), MOTION_MIN);
                }
            }
        }
    }
}
