package railcraft.common;

import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.ModLoader;
import forge.MinecraftForge;
import forge.oredict.OreDictionary;
import railcraft.common.api.ItemRegistry;

public class RailcraftToolItems
{

    private static Item itemCrowbar;
    private static Item itemCoalCoke;

    public static void registerCrowbar()
    {
        if(itemCrowbar == null) {
            int id = RailcraftConfig.getItemId("item.crowbar");

            if(id > 0) {
                itemCrowbar = new ItemCrowbar(id);
                MinecraftForge.setToolClass(itemCrowbar, "crowbar", 0);
                RailcraftLanguage.getInstance().registerItemName(itemCrowbar, "item.crowbar");

                ModLoader.addRecipe(new ItemStack(itemCrowbar), new Object[]{
                        " RI",
                        "RIR",
                        "IR ",
                        Character.valueOf('I'), Item.IRON_INGOT,
                        Character.valueOf('R'), new ItemStack(Item.INK_SACK, 1, 1)
                    });
                MinecraftForge.addDungeonLoot(new ItemStack(itemCrowbar), RailcraftConfig.getLootChance("crowbar"));

                ItemRegistry.registerItem("item.crowbar", new ItemStack(itemCrowbar));
            }
        }
    }

    public static ItemStack getCrowbar()
    {
        if(itemCrowbar == null) {
            return null;
        }
        return new ItemStack(itemCrowbar);
    }

    public static void registerCoalCoke()
    {
        if(itemCoalCoke == null) {
            int id = RailcraftConfig.getItemId("item.coke");

            if(id > 0) {
                itemCoalCoke = new ItemRailcraft(id).d(70).a("coalCoke");
                RailcraftLanguage.getInstance().registerItemName(itemCoalCoke, "item.coal.coke");
                MinecraftForge.addDungeonLoot(new ItemStack(itemCoalCoke), RailcraftConfig.getLootChance("coke"), 4, 8);

                ItemRegistry.registerItem("item.coke", new ItemStack(itemCoalCoke));
                OreDictionary.registerOre("fuelCoke", new ItemStack(itemCoalCoke));
            }
        }
    }

    public static ItemStack getCoalCoke()
    {
        if(itemCoalCoke == null) {
            return null;
        }
        return new ItemStack(itemCoalCoke);
    }
}
