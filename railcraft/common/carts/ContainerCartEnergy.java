package railcraft.common.carts;

import net.minecraft.server.Container;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IInventory;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;
import railcraft.common.util.slots.SlotEnergy;
import railcraft.common.carts.EntityCartEnergy;

public class ContainerCartEnergy extends Container
{

    private EntityCartEnergy cart;

    public ContainerCartEnergy(PlayerInventory inventoryplayer, EntityCartEnergy device)
    {
    	this.setPlayer(inventoryplayer.player);
        this.cart = device;
        a(new SlotEnergy(device, 0, 56, 17));
        a(new SlotEnergy(device, 1, 56, 53));
        for(int i = 0; i < 3; i++) {
            for(int k = 0; k < 9; k++) {
                a(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++) {
            a(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        return cart.a(entityplayer);
    }

    @Override
    public ItemStack a(int i)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)e.get(i);
        if(slot != null && slot.c()) {
            ItemStack itemstack1 = slot.getItem();
            itemstack = itemstack1.cloneItemStack();
            if(i >= 2 && i < 38 && SlotEnergy.canPlaceItem(itemstack1)) {
                if(!a(itemstack1, 0, 2, false)) {
                    return null;
                }
            } else if(i >= 2 && i < 29) {
                if(!a(itemstack1, 29, 38, false)) {
                    return null;
                }
            } else if(i >= 29 && i < 38) {
                if(!a(itemstack1, 2, 29, false)) {
                    return null;
                }
            } else if(!a(itemstack1, 2, 38, false)) {
                return null;
            }
            if(itemstack1.count == 0) {
                slot.set(null);
            } else {
                slot.d();
            }
            if(itemstack1.count != itemstack.count) {
                slot.c(itemstack1);
            } else {
                return null;
            }
        }
        return itemstack;
    }
    
    public IInventory getInventory() {
    	return cart;
    }
}
