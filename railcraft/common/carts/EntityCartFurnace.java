package railcraft.common.carts;

import railcraft.common.api.CartBase;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.*;
import forge.ForgeHooks;
import forge.MinecraftForge;
import railcraft.common.RailcraftConfig;
import railcraft.common.api.ICartRenderInterface;

public class EntityCartFurnace extends CartBase implements ICartRenderInterface
{

    public EntityCartFurnace(World world)
    {
        super(world);
    }

    public EntityCartFurnace(World world, String player)
    {
        super(world, player);
    }

    public EntityCartFurnace(World world, double d, double d1, double d2, String player)
    {
        this(world, player);
        setPosition(d, d1 + (double)height, d2);
        lastX = d;
        lastY = d1;
        lastZ = d2;
        e = 0;
        b = 0;
        c = 0;
    }

    public boolean isActive()
    {
        return e > 0;
    }

    @Override
    protected void applyDragAndPushForces()
    {
        motX *= getDrag();
        motY *= 0.0D;
        motZ *= getDrag();

        double d27 = MathHelper.sqrt(b * b + c * c);
        if(d27 > 0.01D) {
            b /= d27;
            c /= d27;
            double d29 = 0.4;
            motX += b * d29;
            motZ += c * d29;
        }
    }

    @Override
    protected void updatePushForces()
    {
        double d30 = MathHelper.sqrt(b * b + c * c);
        if(d30 > 0.01D && motX * motX + motZ * motZ > 0.001D) {
            b /= d30;
            c /= d30;
            if(b * motX + c * motZ < 0.0D) {
                b = 0.0D;
                c = 0.0D;
            } else {
                b = motX;
                c = motZ;
            }
        }
    }

    @Override
    public void F_()
    {
        if(k() && random.nextInt(4) == 0) {
            world.a("largesmoke", locX, locY + 0.80000000000000004D, locZ, 0.0D, 0.0D, 0.0D);
        }

        super.F_();
    }

    @Override
    public List<ItemStack> getItemsDropped()
    {
        List<ItemStack> items = new ArrayList<ItemStack>();
        if(RailcraftConfig.doCartsBreakOnDrop()) {
            items.add(new ItemStack(Item.MINECART));
            items.add(new ItemStack(Block.FURNACE));
        } else {
            items.add(getCartItem());
        }
        return items;
    }

    @Override
    public double getDrag()
    {
        return DRAG_FACTOR;
    }

    @Override
    public float getMaxSpeedRail()
    {
        return 0.39f;
    }

    @Override
    public boolean isPoweredCart()
    {
        return true;
    }

    @Override
    public boolean canBeRidden()
    {
        return false;
    }

    @Override
    public int getSize()
    {
        return 0;
    }

    @Override
    public String getName()
    {
        return "Steamcart";
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        if(!ForgeHooks.onMinecartInteract(this, entityplayer)) {
            return true;
        }
        ItemStack itemstack = entityplayer.inventory.getItemInHand();
        if(itemstack != null && itemstack.id == Item.COAL.id) {
            if(--itemstack.count == 0) {
                entityplayer.inventory.setItem(entityplayer.inventory.itemInHandIndex, null);
            }
            e += 1200;

            b = locX - entityplayer.locX;
            c = locZ - entityplayer.locZ;
        }

        return true;
    }
    private static final double DRAG_FACTOR = 0.99;
    private static final double PUSH_FACTOR = 0.1D;

    @Override
    public Block getBlock()
    {
        return Block.FURNACE;
    }

    @Override
    public int getBlockMetadata()
    {
        return 0;
    }
}
