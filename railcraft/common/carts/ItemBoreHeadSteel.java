package railcraft.common.carts;

public class ItemBoreHeadSteel extends ItemBoreHead
{

    public ItemBoreHeadSteel(int i)
    {
        super(i);
        d(165);
        setMaxDurability(3000);
        a("boreHeadSteel");
    }

    @Override
    public String getBoreTexture()
    {
        return "/railcraft/textures/tunnel_bore_steel.png";
    }

    @Override
    public int getHarvestLevel()
    {
        return 2;
    }

    @Override
    public float getDigModifier()
    {
        return 1.2f;
    }
}
