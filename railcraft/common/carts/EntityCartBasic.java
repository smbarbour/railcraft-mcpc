package railcraft.common.carts;

import railcraft.common.api.CartBase;
import net.minecraft.server.*;
import railcraft.common.RailcraftConfig;

public class EntityCartBasic extends CartBase
{

    private static final double DRAG_FACTOR = 0.99199997901916504D;

    public EntityCartBasic(World world) {
    	super(world);
    }
    
    public EntityCartBasic(World world, String player)
    {
        super(world, player);
    }

    public EntityCartBasic(World world, double d, double d1, double d2, String player)
    {
        this(world, player);
        setPosition(d, d1 + (double)height, d2);
        motX = 0.0D;
        motY = 0.0D;
        motZ = 0.0D;
        lastX = d;
        lastY = d1;
        lastZ = d2;
    }
//
//    @Override
//    public List<ItemStack> getItemsDropped()
//    {
//        List<ItemStack> items = new ArrayList<ItemStack>();
//        if(RailcraftConfig.doCartsBreakOnDrop()) {
//            items.add(MinecraftForge.getItemForCart(EntityMinecart.class, 0));
//            items.add(new ItemStack(Block.chest));
//        } else {
//            items.add(getCartItem());
//        }
//        return items;
//    }

    @Override
    protected double getDrag()
    {
        if(RailcraftConfig.adjustBasicCartDrag()) {
            return DRAG_FACTOR;
        }
        if(passenger != null) {
            return 0.99699997901916504D;
        }
        return 0.95999997854232788D;
    }

    @Override
    public boolean isStorageCart()
    {
        return false;
    }

    @Override
    public boolean canBeRidden()
    {
        return true;
    }

    @Override
    public int getSize()
    {
        return 0;
    }

    @Override
    public String getName()
    {
        return "";
    }
}
