package railcraft.common.carts;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BlockMinecartTrack;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityItem;
import net.minecraft.server.EntityLiving;
import net.minecraft.server.EntityMinecart;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Material;
import net.minecraft.server.MathHelper;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import forge.IMinecartCollisionHandler;
import forge.IMinecartHandler;
import railcraft.Railcraft;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftConfig;
import railcraft.common.api.CartTools;
import railcraft.common.api.Vec2D;
import railcraft.common.api.tracks.ITrackInstance;
import railcraft.common.tracks.TileTrack;
import railcraft.common.tracks.TrackBaseRailcraft;
import railcraft.common.tracks.speedcontroller.SpeedControllerHighSpeed;

public class MinecartHooks implements IMinecartCollisionHandler, IMinecartHandler
{

    private static final float BLAST_RADIUS = 1.5F;
    protected static double DRAG_FACTOR_GROUND = 0.5;
    protected static double DRAG_FACTOR_AIR = 0.99999;
    protected static double OPTIMAL_DISTANCE = 2;
    protected static double OPTIMAL_DISTANCE_CART = 1.28;
    protected static double COEF_SPRING = 0.1;
    protected static double COEF_SPRING_CART = 0.3;
    protected static double COEF_RESTITUTION = 0.2;
    protected static double COEF_DAMPING = 0.1;
    protected static double ENTITY_REDUCTION = 0.25;
    protected static double CART_LENGTH = 1.22;
    protected static double CART_WIDTH = 0.98;
    protected static double COLLISION_EXPANSION = 0.2;
    protected static double MAX_INTERACT_DISTANCE = 9.0;
    private Random rand;

    public MinecartHooks()
    {
        rand = new Random();
    }

    @Override
    public void onEntityCollision(EntityMinecart cart, Entity other)
    {
        if(other == cart.passenger) {
            return;
        }

        if((other instanceof EntityLiving) && !(other instanceof EntityHuman)
            && cart.canBeRidden() && Railcraft.gameIsHost()
            && cart.motX * cart.motX + cart.motZ * cart.motZ > 0.01D
            && cart.passenger == null && other.vehicle == null) {
            int mountPrevention = cart.getEntityData().getInt("MountPrevention");
            if(mountPrevention <= 0) {
                other.mount(cart);
            }
        }

        int i = MathHelper.floor(cart.locX);
        int j = MathHelper.floor(cart.locY);
        int k = MathHelper.floor(cart.locZ);
        int id = cart.world.getTypeId(i, j, k);
        if(other instanceof EntityLiving && RailcraftBlocks.getBlockRailElevator() != null && id == RailcraftBlocks.getBlockRailElevator().id) {
            return;
        }

//        System.out.println(cart.getClass().getSimpleName() + ": " + cart.entityId + " collided with " + other.getClass().getSimpleName() + ": " + other.entityId);

        double distance = cart.i(other);
        double depth = 0;
        double spring = 0;
        if(other instanceof EntityMinecart) {
            depth = distance - OPTIMAL_DISTANCE_CART;
            spring = COEF_SPRING_CART;
        } else {
            depth = distance - OPTIMAL_DISTANCE;
            spring = COEF_SPRING;
        }

        Vec2D cartPos = new Vec2D(cart.locX, cart.locZ);
        Vec2D otherPos = new Vec2D(other.locX, other.locZ);

        Vec2D unit = Vec2D.subtract(otherPos, cartPos);
        unit.normalize();

        double forceX = 0;
        double forceZ = 0;

        if(depth < 0) {
            double penaltyX = spring * depth * unit.getX();
            double penaltyZ = spring * depth * unit.getY();

            forceX += penaltyX;
            forceZ += penaltyZ;
        }

        if(other instanceof EntityMinecart) {
            EntityMinecart otherCart = (EntityMinecart)other;

            if(depth < 0) {
                double impulseX = unit.getX();
                double impulseZ = unit.getY();
                impulseX *= -(1.0 + COEF_RESTITUTION);
                impulseZ *= -(1.0 + COEF_RESTITUTION);

                Vec2D cartVel = new Vec2D(cart.motX, cart.motZ);
                Vec2D otherVel = new Vec2D(other.motX, other.motZ);

                double dot = Vec2D.subtract(otherVel, cartVel).dotProduct(unit);

                impulseX *= dot;
                impulseZ *= dot;
                impulseX *= 0.5;
                impulseZ *= 0.5;

                forceX -= impulseX;
                forceZ -= impulseZ;
            }

//            Vec3D cart1Vel = Vec3D.createVector(cart.motionX + forceX, 0, cart.motionZ + forceZ);
//            Vec3D cart2Vel = Vec3D.createVector(other.motionX - forceX, 0, other.motionZ - forceZ);
//
//            double dot = cart2Vel.subtract(cart1Vel).dotProduct(unit);
//
//            double dampX = COEF_DAMPING * dot * unit.xCoord;
//            double dampZ = COEF_DAMPING * dot * unit.zCoord;

//            forceX -= dampX;
//            forceZ -= dampZ;

//                System.out.println("impulseX= " + impulseX + ", impulseZ= " + impulseZ + ", penaltyX= " + penaltyX + ", penaltyZ= " + penaltyZ);
            if(!cart.isPoweredCart() || otherCart.isPoweredCart()) {
                if(!CartTools.isCartLockedDown(cart)) {
                    cart.motX += forceX;
                    cart.motZ += forceZ;
                }
            }
            if(!otherCart.isPoweredCart() || cart.isPoweredCart()) {
                if(!CartTools.isCartLockedDown(otherCart)) {
                    other.motX -= forceX;
                    other.motZ -= forceZ;
                }
            }
        } else {
            if(!CartTools.isCartLockedDown(cart)) {
                cart.motX += forceX;
                cart.motZ += forceZ;
            }

            other.motX -= forceX * ENTITY_REDUCTION;
            other.motZ -= forceZ * ENTITY_REDUCTION;
        }
    }

    @Override
    public AxisAlignedBB getCollisionBox(EntityMinecart cart, Entity other)
    {
        boolean highSpeed = cart.getEntityData().getBoolean("HighSpeed");
        if(highSpeed && other instanceof EntityItem && RailcraftConfig.doCartsCollideWithItemsHighSpeed()) {
            return other.boundingBox;
        }
        if(!highSpeed && other instanceof EntityItem && RailcraftConfig.doCartsCollideWithItems()) {
            return other.boundingBox;
        }
        if(other instanceof EntityHuman) {
            return other.boundingBox;
        }
        return null;
    }

    @Override
    public AxisAlignedBB getMinecartCollisionBox(EntityMinecart cart)
    {
        double yaw = Math.toRadians(cart.yaw);
        double diff = ((CART_LENGTH - CART_WIDTH) / 2.0) + COLLISION_EXPANSION;
        double x = diff * Math.abs(Math.cos(yaw));
        double z = diff * Math.abs(Math.sin(yaw));
        return cart.boundingBox.grow(x, COLLISION_EXPANSION, z);
    }

    @Override
    public AxisAlignedBB getBoundingBox(EntityMinecart cart)
    {
        if(cart == null) {
            return null;
        }
        if(Railcraft.gameIsNotHost() || Railcraft.gameIsServer()) {
            int i = MathHelper.floor(cart.locX);
            int j = MathHelper.floor(cart.locY);
            int k = MathHelper.floor(cart.locZ);
            if(cart.world != null && RailcraftBlocks.getBlockRailElevator() != null && cart.world.getTypeId(i, j, k) == RailcraftBlocks.getBlockRailElevator().id) {
                return cart.boundingBox;
            } else {
                return null;
            }
        }
        return cart.boundingBox;
    }

    @Override
    public void onMinecartUpdate(EntityMinecart cart, int i, int j, int k)
    {
        int id = cart.world.getTypeId(i, j, k);
        if(BlockMinecartTrack.d(id)) {
            cart.fallDistance = 0;
            if(cart.passenger != null) {
                cart.passenger.fallDistance = 0;
            }
        } else {
            int launched = cart.getEntityData().getInt("Launched");
            if(launched == 1) {
                cart.getEntityData().setInt("Launched", 2);
                cart.setCanUseRail(true);
            } else if(launched == 2 && (cart.onGround || cart.a(Material.ORIENTABLE))) {
                cart.getEntityData().setInt("Launched", 0);
                cart.setMaxSpeedAirLateral(EntityMinecart.defaultMaxSpeedAirLateral);
                cart.setMaxSpeedAirVertical(EntityMinecart.defaultMaxSpeedAirVertical);
                cart.setDragAir(EntityMinecart.defaultDragAir);
            }
        }

//        if(cart.passenger instanceof EntityPlayer) {
//            EntityPlayer player = (EntityPlayer)cart.passenger;
//            float speed = (float)Math.sqrt(cart.motionX * cart.motionX + cart.motionZ * cart.motionZ);
//            if(BlockRail.isRailBlock(id)) {
//                speed = Math.min(Math.min(((BlockRail)Block.blocksList[id]).getRailMaxSpeed(cart.worldObj, cart, i, j, k), cart.getMaxSpeedRail()), speed);
//            } else if(cart.onGround) {
//                speed = Math.min(cart.getMaxSpeedGround(), speed);
//            } else {
//                speed = Math.min(cart.getMaxSpeedAirLateral(), speed);
//            }
//            player.landMovementFactor = speed;
//        }

//		  if(cart.worldObj.getTypeId(i, j, k) == mod_RailcraftCore.blockElevator.blockID){
//				cart.setCanBePushed(false);
//		  }else{
//				cart.setCanBePushed(true);
//		  }

//		  if (!BlockRail.isRailBlockAt(cart.worldObj, i, j, k)) {
//				if (BlockRail.isRailBlockAt(cart.worldObj, i, j - 1, k)) {
//					 j--;
//					 System.out.println("j-1");
//				} else if (BlockRail.isRailBlockAt(cart.worldObj, i, j - 2, k)) {
//					 j -= 2;
//					 System.out.println("j-2");
//				} else if (BlockRail.isRailBlockAt(cart.worldObj, i, j - 3, k)) {
//					 j -= 3;
//					 System.out.println("j-3");
//				}
//		  }


        int mountPrevention = cart.getEntityData().getInt("MountPrevention");
        if(mountPrevention > 0) {
            mountPrevention--;
        }
        cart.getEntityData().setInt("MountPrevention", mountPrevention);

        double prevMotionX = cart.getEntityData().getDouble("PrevMotionX");
        double prevMotionZ = cart.getEntityData().getDouble("PrevMotionZ");
        boolean highSpeed = cart.getEntityData().getBoolean("HighSpeed");
//		  if (highSpeed) {
//				System.out.println("[" + cart.posX + ", " + cart.posY + ", " + cart.posZ + "]");
//		  }
        if(highSpeed
            && (!isSpeedRailAt(cart.world, i, j, k)
            || (cart.motX == 0 && cart.motZ == 0)
            || (cart.motX == 0 ^ prevMotionX == 0)
            || (cart.motZ == 0 ^ prevMotionZ == 0))) {
//				System.out.println("Exploded in doUpdate: [" + i + ", " + j + ", " + k + "], rail = " + BlockRail.isRailBlockAt(cart.worldObj, i, j, k) + ", speed rail = " + GeneralTools.isSpeedRailAt(cart.worldObj, i, j, k) + ", mx = " + cart.motionX + ", pmx = " + prevMotionX + ", mz = " + cart.motionZ + ", pmz = " + prevMotionZ);
            explode(cart);
        }
        cart.getEntityData().setDouble("PrevMotionX", cart.motX);
        cart.getEntityData().setDouble("PrevMotionZ", cart.motZ);

        if(highSpeed && Math.abs(cart.motX) < 0.4f && Math.abs(cart.motZ) < 0.4f) {
            cart.getEntityData().setBoolean("HighSpeed", false);
        }

        cart.motX = Math.copySign(Math.min(Math.abs(cart.motX), 9.5), cart.motX);
        cart.motY = Math.copySign(Math.min(Math.abs(cart.motY), 9.5), cart.motY);
        cart.motZ = Math.copySign(Math.min(Math.abs(cart.motZ), 9.5), cart.motZ);
    }

    private boolean isSpeedRailAt(World world, int i, int j, int k)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileTrack) {
            ITrackInstance track = ((TileTrack)tile).getTrackInstance();
            if(track instanceof TrackBaseRailcraft) {
                return ((TrackBaseRailcraft)track).speedController instanceof SpeedControllerHighSpeed;
            }
        }
        return false;
    }

    @Override
    public void onMinecartEntityCollision(EntityMinecart cart, Entity other)
    {
        if(other == cart.passenger) {
            return;
        }

        testHighSpeed(cart, other);

        if(EntityMinecart.getCollisionHandler() != this) {
            int i = MathHelper.floor(cart.locX);
            int j = MathHelper.floor(cart.locY);
            int k = MathHelper.floor(cart.locZ);
            if(other instanceof EntityLiving && RailcraftBlocks.getBlockRailElevator() != null && cart.world.getTypeId(i, j, k) == RailcraftBlocks.getBlockRailElevator().id) {
                if(other.boundingBox.b < cart.boundingBox.e) {
                    other.move(0, cart.boundingBox.e - other.boundingBox.b, 0);
                    other.onGround = true;
                }
            }
        }
    }

    private void testHighSpeed(EntityMinecart cart, Entity other)
    {
        boolean highSpeed = cart.getEntityData().getBoolean("HighSpeed");
        if(highSpeed) {
            LinkageManager lm = LinkageManager.getInstance();
            EntityMinecart link = lm.getLinkedCartA(cart);
            if(link != null && (link == other || other == link.passenger)) {
                return;
            }
            link = lm.getLinkedCartB(cart);
            if(link != null && (link == other || other == link.passenger)) {
                return;
            }

            if(!(other instanceof EntityMinecart)) {
                explode(cart);
            } else if(other instanceof EntityMinecart) {
                boolean otherHighSpeed = other.getEntityData().getBoolean("HighSpeed");
                if(!otherHighSpeed || (cart.motX > 0 ^ other.motX > 0) || (cart.motZ > 0 ^ other.motZ > 0)) {
                    explode(cart);
                }
            }
        }
    }

    private void explode(EntityMinecart cart)
    {
        cart.getEntityData().setBoolean("HighSpeed", false);
        cart.motX = 0;
        cart.motZ = 0;
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        if(cart.passenger != null) {
            cart.passenger.mount(cart);
        }
        cart.world.explode(cart, cart.locX, cart.locY, cart.locZ, BLAST_RADIUS);
        if(rand.nextInt(3) == 0) {
            cart.die();
        }
    }

    @Override
    public boolean onMinecartInteract(EntityMinecart cart, EntityHuman player, boolean canceled)
    {
        if(!(cart instanceof EntityTunnelBore) && player.j(cart) > MAX_INTERACT_DISTANCE) {
            return false;
        }
        if(cart.dead) {
            return false;
        }
        if(cart.canBeRidden()) {
            if(cart.passenger != null && player.vehicle != cart) {
                return false;
            }
            if(player.vehicle != null && player.vehicle != cart) {
                return false;
            }
            if(player.vehicle != cart && player.t()) {
                return false;
            }
        }
        if(!player.h(cart)) {
            return false;
        }
        return true;
    }
}
