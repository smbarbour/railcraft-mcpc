package railcraft.common.carts;

import net.minecraft.server.Block;
import net.minecraft.server.EntityMinecart;
import net.minecraft.server.ItemStack;
import net.minecraft.server.World;
import railcraft.common.util.liquids.LiquidManager;

public enum EnumCart
{

    BASIC(1, 135, "/gui/items.png", "Minecart", EntityCartBasic.class, null),
    CHEST(2, 151, "/gui/items.png", "Chest Cart", EntityCartChest.class, new ItemStack(Block.CHEST)),
    FURNACE(3, 167, "/gui/items.png", "Furnace Cart", EntityCartFurnace.class, new ItemStack(Block.FURNACE)),
    TNT(4, 24, "/railcraft/textures/railcraft.png", "TNT Cart", EntityCartTNT.class, new ItemStack(Block.TNT)),
    TANK(5, 40, "/railcraft/textures/railcraft.png", "Tank Cart", EntityCartTank.class, new ItemStack(Block.GLASS, 8)),
    BATBOX(6, 198, "/railcraft/textures/railcraft.png", "Batbox Cart", EntityCartEnergy.class, null),
    MFE(6, 199, "/railcraft/textures/railcraft.png", "MFE Cart", EntityCartEnergy.class, null),
    MFSU(6, 200, "/railcraft/textures/railcraft.png", "MFSU Cart", EntityCartEnergy.class, null),
    ANCHOR(7, 197, "/railcraft/textures/railcraft.png", "Anchor Cart", EntityCartAnchor.class, null),
    WORK(8, 196, "/railcraft/textures/railcraft.png", "Work Cart", EntityCartWork.class, new ItemStack(Block.WORKBENCH)),
    TRACK_RELAYER(9, 195, "/railcraft/textures/railcraft.png", "Track Relayer", EntityCartTrackRelayer.class, null),
    UNDERCUTTER(10, 194, "/railcraft/textures/railcraft.png", "Undercutter", EntityCartUndercutter.class, null);
    private final int icon;
    private final String name;
    private final String spritesheet;
    private final Class<? extends EntityMinecart> type;
    private final byte id;
    private ItemStack contents = null;

    private EnumCart(int id, int icon, String spritesheet, String name, Class<? extends EntityMinecart> type, ItemStack contents)
    {
        this.name = name;
        this.id = (byte)id;
        this.icon = icon;
        this.spritesheet = spritesheet;
        this.type = type;
        this.contents = contents;
    }

    public byte getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public int getIconIndex()
    {
        return icon;
    }

    public String getSpritesheet()
    {
        return spritesheet;
    }

    public String getTag()
    {
        return "entity.cart." + name().toLowerCase().replace('_', '.');
    }

    public String getItemTag()
    {
        return "item.cart." + name().toLowerCase().replace('_', '.');
    }

    public Class<? extends EntityMinecart> getCartClass()
    {
        return type;
    }

    public void setContents(ItemStack stack)
    {
        contents = stack.cloneItemStack();
    }

    public ItemStack getContents()
    {
        if(contents == null){
            return null;
        }
        return contents.cloneItemStack();
    }

    public EntityMinecart makeCart(ItemStack item, World world, double i, double j, double k, String player)
    {
        if(this == CHEST) {
            return new EntityCartChest(world, i, j, k, player);
        } else if(this == FURNACE) {
            return new EntityCartFurnace(world, i, j, k, player);
        } else if(this == TNT) {
            return new EntityCartTNT(world, i, j, k, player);
        } else if(this == TANK) {
            EntityCartTank cart = new EntityCartTank(world, i, j, k, player);
            ItemStack filter = LiquidManager.getInstance().getItemFromFilterId(item.getData());
            cart.setItem(0, filter);
            return cart;
        } else if(this == BATBOX) {
            EntityMinecart cart = new EntityCartEnergy(world, i, j, k, player);
            cart.type = 0;
            return cart;
        } else if(this == MFE) {
            EntityMinecart cart = new EntityCartEnergy(world, i, j, k, player);
            cart.type = 1;
            return cart;
        } else if(this == MFSU) {
            EntityMinecart cart = new EntityCartEnergy(world, i, j, k, player);
            cart.type = 2;
            return cart;
        } else if(this == ANCHOR) {
            return new EntityCartAnchor(world, i, j, k, player);
        } else if(this == WORK) {
            return new EntityCartWork(world, i, j, k, player);
        } else if(this == TRACK_RELAYER) {
            return new EntityCartTrackRelayer(world, i, j, k, player);
        } else if(this == UNDERCUTTER) {
            return new EntityCartUndercutter(world, i, j, k, player);
        }
        return new EntityCartBasic(world, i, j, k, player);
    }
}
