package railcraft.common.carts;

import java.util.List;

import org.bukkit.craftbukkit.entity.CraftHumanEntity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.InventoryHolder;

import railcraft.common.util.slots.SlotOutput;
import railcraft.common.util.slots.SlotLiquidFilter;
import railcraft.common.util.slots.SlotLiquidContainer;

import net.minecraft.server.*;

public class ContainerTankCart extends Container
{
    private EntityCartTank tank;

    public ContainerTankCart(PlayerInventory inventoryplayer, EntityCartTank cart)
    {
    	this.setPlayer(inventoryplayer.player);
        this.tank = cart;
        a(new SlotLiquidFilter(cart, 0, 71, 39));
        a(new SlotLiquidContainer(cart, 1, 116, 21));
        a(new SlotOutput(cart, 2, 116, 56));
        for(int i = 0; i < 3; i++) {
            for(int k = 0; k < 9; k++) {
                a(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++) {
            a(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        return tank.a(entityplayer);
    }

    @Override
    public ItemStack a(int i)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)e.get(i);
        if(slot != null && slot.c()) {
            ItemStack itemstack1 = slot.getItem();
            itemstack = itemstack1.cloneItemStack();
            if(i >= 3 && i < 39 && SlotLiquidFilter.canPlaceItem(itemstack1)) {
                if(!a(itemstack1, 1, 2, false)) {
                    return null;
                }
            } else if(i >= 3 && i < 30) {
                if(!a(itemstack1, 29, 38, false)) {
                    return null;
                }
            } else if(i >= 30 && i < 39) {
                if(!a(itemstack1, 2, 29, false)) {
                    return null;
                }
            } else if(!a(itemstack1, 2, 38, false)) {
                return null;
            }
            if(itemstack1.count == 0) {
                slot.set(null);
            } else {
                slot.d();
            }
            if(itemstack1.count != itemstack.count) {
                slot.c(itemstack1);
            } else {
                return null;
            }
        }
        return itemstack;
    }
    
    public IInventory getInventory() {
    	return new IInventory(){

			@Override
			public boolean a(EntityHuman arg0) {
				return false;
			}

			@Override
			public void f() {
			}

			@Override
			public void g() {
			}

			@Override
			public ItemStack[] getContents() {
				return null;
			}

			@Override
			public ItemStack getItem(int arg0) {
				return null;
			}

			@Override
			public int getMaxStackSize() {
				return 0;
			}

			@Override
			public String getName() {
				return null;
			}

			@Override
			public InventoryHolder getOwner() {
				return null;
			}

			@Override
			public int getSize() {
				return 0;
			}

			@Override
			public List<HumanEntity> getViewers() {
				return null;
			}

			@Override
			public void onClose(CraftHumanEntity arg0) {
			}

			@Override
			public void onOpen(CraftHumanEntity arg0) {
			}

			@Override
			public void setItem(int arg0, ItemStack arg1) {
			}

			@Override
			public void setMaxStackSize(int arg0) {
			}

			@Override
			public ItemStack splitStack(int arg0, int arg1) {
				return null;
			}

			@Override
			public ItemStack splitWithoutUpdate(int arg0) {
				return null;
			}

			@Override
			public void update() {
			}};
    }
}
