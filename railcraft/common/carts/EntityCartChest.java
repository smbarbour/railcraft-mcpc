package railcraft.common.carts;

import railcraft.common.api.TransferCartBase;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.*;
import forge.ForgeHooks;
import railcraft.Railcraft;
import railcraft.common.RailcraftConfig;

public class EntityCartChest extends TransferCartBase
{

    private static final double DRAG_FACTOR = 0.99199997901916504D;

    public EntityCartChest(World world) {
    	super(world);
    }

    public EntityCartChest(World world, String player)
    {
        super(world, player);
    }

    public EntityCartChest(World world, double d, double d1, double d2, String player)
    {
        this(world, player);
        setPosition(d, d1 + (double)height, d2);
        motX = 0.0D;
        motY = 0.0D;
        motZ = 0.0D;
        lastX = d;
        lastY = d1;
        lastZ = d2;
    }

	@Override
    public List<ItemStack> getItemsDropped()
    {
        List<ItemStack> items = new ArrayList<ItemStack>();
        if(RailcraftConfig.doCartsBreakOnDrop()) {
            items.add(new ItemStack(Item.MINECART));
            items.add(new ItemStack(Block.CHEST));
        } else {
            items.add(getCartItem());
        }
        return items;
    }

    @Override
    protected double getDrag()
    {
        return DRAG_FACTOR;
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        if(!ForgeHooks.onMinecartInteract(this, entityplayer)) {
            return true;
        }
        if(Railcraft.gameIsHost()) {
            entityplayer.openContainer(this);
        }
        return true;
    }

    @Override
    public boolean isStorageCart()
    {
        return true;
    }

    @Override
    public boolean canBeRidden()
    {
        return false;
    }

    @Override
    public int getSize()
    {
        return 27;
    }

    @Override
    public String getName()
    {
        return "Chest Cart";
    }
}
