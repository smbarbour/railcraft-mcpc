package railcraft.common.carts;

import railcraft.common.api.CartBase;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.*;
import railcraft.Railcraft;
import railcraft.common.RailcraftConfig;
import railcraft.GuiHandler;
import railcraft.common.EnumGui;
import railcraft.common.api.INetworkEntity;

public class EntityCartTNT extends CartBase implements IPrimableCart, INetworkEntity
{

    private static final byte FUSE_DATA_ID = 20;
    private static final byte BLAST_DATA_ID = 21;
    private static final byte PRIMED_DATA_ID = 22;
    private static final double DRAG_FACTOR = 0.99199997901916504D;
    private final static float BLAST_RADIUS_MULTIPLIER = 0.5f;
    private final static byte BLAST_RADIUS_MIN = 4;
    private final static byte BLAST_RADIUS_MAX = 12;
    public static final short MAX_FUSE = 500;
    public static final short MIN_FUSE = 0;
    private double prevMotionX;
    private double prevMotionZ;
    private boolean prevOnGround;
    private boolean prevOnRail;
    private short timeInAir = 0;

    public EntityCartTNT(World world)
    {
        super(world);
    }

    public EntityCartTNT(World world, String player)
    {
        super(world, player);
        getEntityData().setBoolean("Explosive", true);
    }

    public EntityCartTNT(World world, double d, double d1, double d2, String player)
    {
        this(world, player);
        setPosition(d, d1 + (double)height, d2);
        motX = 0.0D;
        motY = 0.0D;
        motZ = 0.0D;
        lastX = d;
        lastY = d1;
        lastZ = d2;
    }

    @Override
    protected void b()
    {
        super.b();
        datawatcher.a(FUSE_DATA_ID, Short.valueOf((short)80));
        datawatcher.a(BLAST_DATA_ID, Byte.valueOf((byte)9));
        datawatcher.a(PRIMED_DATA_ID, Byte.valueOf((byte)0));
    }

    @Override
    public List<ItemStack> getItemsDropped()
    {
        List<ItemStack> items = new ArrayList<ItemStack>();
        if(RailcraftConfig.doCartsBreakOnDrop()) {
            items.add(new ItemStack(Item.MINECART));
            items.add(new ItemStack(Block.TNT));
        } else {
            items.add(getCartItem());
            System.out.println(getCartItem().getClass());
        }
        return items;
    }

    @Override
    public void F_()
    {
        super.F_();

        if(((motX == 0 ^ prevMotionX == 0) && Math.abs(prevMotionX) > 0.4f && motZ == 0)
            || ((motZ == 0 ^ prevMotionZ == 0) && Math.abs(prevMotionZ) > 0.4f && motX == 0)
            || (onGround && !prevOnGround && !prevOnRail && timeInAir > 5)) {
            if(Railcraft.gameIsHost()) {
                explode();
            }
        }
        prevMotionX = motX;
        prevMotionZ = motZ;
        prevOnGround = onGround;
        int i = MathHelper.floor(locX);
        int j = MathHelper.floor(locY);
        int k = MathHelper.floor(locZ);
        if(BlockMinecartTrack.g(world, i, j - 1, k)) {
            j--;
        }
        int id = world.getTypeId(i, j, k);
        prevOnRail = BlockMinecartTrack.d(id);
        if(!onGround && !prevOnRail) {
            timeInAir++;
        } else {
            timeInAir = 0;
        }

        if(isPrimed()) {
            setFuse((short)(getFuse() - 1));
            if(getFuse() <= 0) {
                if(Railcraft.gameIsHost()) {
                    explode();
                }
            } else {
                world.a("smoke", locX, locY + 0.8D, locZ, 0.0D, 0.0D, 0.0D);
            }
        }
    }

    @Override
    public void explode()
    {
        world.explode(this, locX, locY, locZ, getBlastRadius() * BLAST_RADIUS_MULTIPLIER);
        die();
    }

    @Override
    public boolean b(EntityHuman player)
    {
        ItemStack itemstack = player.inventory.getItemInHand();
        if(itemstack != null) {
            if(itemstack.id == Item.FLINT_AND_STEEL.id) {
                setPrimed(true);
            } else if(itemstack.id == Item.STRING.id) {
                player.inventory.splitStack(player.inventory.itemInHandIndex, 1);
                GuiHandler.openGui(EnumGui.CART_TNT_FUSE, player, world, this);
            } else if(itemstack.id == Item.SULPHUR.id) {
                player.inventory.splitStack(player.inventory.itemInHandIndex, 1);
                GuiHandler.openGui(EnumGui.CART_TNT_BLAST, player, world, this);
            }
        }
        return true;
    }

    @Override
    protected double getDrag()
    {
        return DRAG_FACTOR;
    }

    @Override
    public int getSize()
    {
        return 0;
    }

    @Override
    public boolean isStorageCart()
    {
        return false;
    }

    @Override
    public boolean canBeRidden()
    {
        return false;
    }

    @Override
    public String getName()
    {
        return "TNTcart";
    }

    @Override
    public boolean isPrimed()
    {
        return datawatcher.getByte(PRIMED_DATA_ID) != 0;
    }

    @Override
    public void setPrimed(boolean p)
    {
        datawatcher.watch(PRIMED_DATA_ID, p ? Byte.valueOf((byte)1) : Byte.valueOf((byte)0));
    }

    @Override
    public short getFuse()
    {
        return datawatcher.b(FUSE_DATA_ID);
    }

    @Override
    public void setFuse(short f)
    {
        f = (short)Math.max(f, MIN_FUSE);
        f = (short)Math.min(f, MAX_FUSE);
        datawatcher.watch(FUSE_DATA_ID, f);
    }

    @Override
    public byte getBlastRadius()
    {
        return datawatcher.getByte(BLAST_DATA_ID);
    }

    @Override
    public void setBlastRadius(int b)
    {
        b = Math.max(b, BLAST_RADIUS_MIN);
        b = Math.min(b, BLAST_RADIUS_MAX);
        datawatcher.watch(BLAST_DATA_ID, Byte.valueOf((byte)b));
    }

    @Override
    protected void b(NBTTagCompound data)
    {
        super.b(data);
        data.setShort("Fuse", getFuse());
        data.setByte("blastRadius", getBlastRadius());
        data.setBoolean("Primed", isPrimed());
    }

    @Override
    protected void a(NBTTagCompound data)
    {
        super.a(data);
        setFuse(data.getShort("Fuse"));
        setBlastRadius(data.getByte("blastRadius"));
        setPrimed(data.getBoolean("Primed"));
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        data.writeShort(getFuse());
        data.writeByte(getBlastRadius());
        data.writeBoolean(isPrimed());
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        setFuse(data.readShort());
        setBlastRadius(data.readByte());
        setPrimed(data.readBoolean());
    }

    @Override
    public World getWorld()
    {
        return world;
    }
}
