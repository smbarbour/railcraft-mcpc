package railcraft.common.carts;

public interface IPrimableCart
{

    public void setPrimed(boolean primed);

    public boolean isPrimed();

    public short getFuse();

    public void setFuse(short fuse);

    public byte getBlastRadius();

    public void setBlastRadius(int f);

    public void explode();
}
