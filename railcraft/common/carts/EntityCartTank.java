package railcraft.common.carts;

import forestry.api.liquids.LiquidStack;
import railcraft.common.api.CartBase;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.*;
import buildcraft.api.ILiquidContainer;
import buildcraft.api.ISpecialInventory;
import buildcraft.api.LiquidSlot;
import buildcraft.api.Orientations;
import forge.ForgeHooks;
import forge.ISidedInventory;
import forge.ISpawnHandler;
import forge.MinecraftForge;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.EnumGui;
import railcraft.common.InventoryCopy;
import railcraft.common.RailcraftConfig;
import railcraft.common.RailcraftLanguage;
import railcraft.common.api.EnumDirection;
import railcraft.common.api.ILiquidTransfer;
import railcraft.common.api.IMinecart;
import railcraft.common.api.InventoryMapper;
import railcraft.common.api.InventoryTools;
import railcraft.common.util.liquids.LiquidManager;

public class EntityCartTank extends CartBase implements ILiquidContainer, ILiquidTransfer, ISpawnHandler, ISpecialInventory, ISidedInventory, IMinecart
{

    private static final byte LIQUID_ID_DATA_ID = 20;
    private static final byte LIQUID_QTY_DATA_ID = 21;
    private static final byte FILLING_DATA_ID = 22;
    private static final double DRAG_FACTOR = 0.99199997901916504D;
    private static final int SLOT_INPUT = 1;
    private static final int SLOT_OUTPUT = 2;

    public EntityCartTank(World world)
    {
        super(world);
    }

   public EntityCartTank(World world, String player)
    {
        super(world, player);
    }

    public EntityCartTank(World world, double d, double d1, double d2, String player)
    {
        this(world, player);
        setPosition(d, d1 + (double)height, d2);
        motX = 0.0D;
        motY = 0.0D;
        motZ = 0.0D;
        lastX = d;
        lastY = d1;
        lastZ = d2;
    }

    @Override
    protected void b()
    {
        super.b();
        datawatcher.a(LIQUID_ID_DATA_ID, new Integer(0));
        datawatcher.a(LIQUID_QTY_DATA_ID, new Integer(0));
        datawatcher.a(FILLING_DATA_ID, Byte.valueOf((byte)0));
    }

    @Override
    public void F_()
    {
        super.F_();

        ItemStack topSlot = getItem(SLOT_INPUT);
        if(topSlot != null && !LiquidManager.getInstance().isContainer(topSlot)) {
            setItem(SLOT_INPUT, null);
            a(topSlot, 1);
        }

        ItemStack bottomSlot = getItem(SLOT_OUTPUT);
        if(bottomSlot != null && !LiquidManager.getInstance().isContainer(bottomSlot)) {
            setItem(SLOT_OUTPUT, null);
            a(bottomSlot, 1);
        }

        if(topSlot != null) {
            LiquidStack bucketLiquid = LiquidManager.getInstance().getLiquidInContainer(topSlot);
            if(bucketLiquid != null && (!topSlot.getItem().k() || bottomSlot == null)) {
                int used = fill(Orientations.YPos, LiquidManager.BUCKET_VOLUME, bucketLiquid.itemID, false);
                if(used >= LiquidManager.BUCKET_VOLUME) {
                    fill(Orientations.YPos, LiquidManager.BUCKET_VOLUME, bucketLiquid.itemID, true);
                    splitStack(SLOT_INPUT, 1);
                    if(topSlot.getItem().k()) {
                        setItem(SLOT_OUTPUT, new ItemStack(topSlot.getItem().j()));
                    }
                }
            } else if(bucketLiquid == null && LiquidManager.getInstance().isEmptyContainer(topSlot) && getLiquidQuantity() >= LiquidManager.BUCKET_VOLUME) {
                ItemStack filled = LiquidManager.getInstance().fillLiquidContainer(new LiquidStack(getLiquidId(), LiquidManager.BUCKET_VOLUME), topSlot);
                if(filled != null && (bottomSlot == null || (InventoryTools.isItemEqual(filled, bottomSlot) && bottomSlot.count < bottomSlot.getMaxStackSize()))) {
                    int drain = empty(LiquidManager.BUCKET_VOLUME, false);
                    if(drain == LiquidManager.BUCKET_VOLUME) {
                        empty(LiquidManager.BUCKET_VOLUME, true);
                        splitStack(SLOT_INPUT, 1);
                        if(bottomSlot == null) {
                            setItem(SLOT_OUTPUT, filled);
                        } else {
                            bottomSlot.count++;
                        }
                    }
                }
            }
        }
    }

    @Override
    public boolean b(EntityHuman player)
    {
        if(!ForgeHooks.onMinecartInteract(this, player)) {
            return true;
        }
        if(Railcraft.gameIsHost()) {
            GuiHandler.openGui(EnumGui.CART_TANK, player, world, this);
        }
//        if(!ForgeHooks.onMinecartInteract(this, player)) {
//            return true;
//        }
//        if(Railcraft.gameIsHost()) {
//            ItemStack current = player.getCurrentEquippedItem();
//
//            if(current != null) {
//                int lq = GeneralTools.getLiquidForFilledContainer(current);
//                if(lq > 0) {
//                    if(fill(Orientations.Unknown, GeneralTools.BUCKET_VOLUME, lq, true) > 0) {
//                        if(current.getItem().hasContainerItem()) {
//                            player.inventory.setInventorySlotContents(player.inventory.currentItem, new ItemStack(current.getItem().getContainerItem()));
//                        } else {
//                            player.inventory.setInventorySlotContents(player.inventory.currentItem, null);
//                        }
//                    }
//                } else if(current.itemID == Item.bucketEmpty.shiftedIndex) {
//                    if(empty(GeneralTools.BUCKET_VOLUME, false) == GeneralTools.BUCKET_VOLUME) {
//                        empty(GeneralTools.BUCKET_VOLUME, true);
//                        ItemStack filledBucket = GeneralToolsProto.getBucketForLiquid(getLiquidId());
//                        player.inventory.setInventorySlotContents(player.inventory.currentItem, filledBucket);
//                    }
//                }
//            }
//        }
        return true;
    }

    @Override
    public void die()
    {
        for(int slot = 1; slot < this.getSize(); ++slot) {
            ItemStack item = this.getItem(slot);

            if(item != null) {
                float var3 = this.random.nextFloat() * 0.8F + 0.1F;
                float var4 = this.random.nextFloat() * 0.8F + 0.1F;
                float var5 = this.random.nextFloat() * 0.8F + 0.1F;

                while(item.count > 0) {
                    int stackSize = this.random.nextInt(21) + 10;

                    if(stackSize > item.count) {
                        stackSize = item.count;
                    }

                    item.count -= stackSize;
                    EntityItem itemEntity = new EntityItem(this.world, this.locX + (double)var3, this.locY + (double)var4, this.locZ + (double)var5, new ItemStack(item.id, stackSize, item.getData()));

                    if(item.hasTag()) {
                        itemEntity.itemStack.setTag((NBTTagCompound)item.getTag().clone());
                    }

                    float var8 = 0.05F;
                    itemEntity.motX = (double)((float)this.random.nextGaussian() * var8);
                    itemEntity.motY = (double)((float)this.random.nextGaussian() * var8 + 0.2F);
                    itemEntity.motZ = (double)((float)this.random.nextGaussian() * var8);
                    this.world.addEntity(itemEntity);
                }
            }
        }

        this.dead = true;
    }

    @Override
    public ItemStack getCartItem()
    {
        ItemStack item = MinecraftForge.getItemForCart(this);
        if(hasFilter()) {
            item.setData(LiquidManager.getInstance().getLiquidFilterId(getFilter()));
        }
        return item;
    }

    @Override
    public List<ItemStack> getItemsDropped()
    {
        List<ItemStack> items = new ArrayList<ItemStack>();
        items.add(getCartItem());
        return items;
    }

    @Override
    protected double getDrag()
    {
        return DRAG_FACTOR;
    }

    @Override
    public boolean isStorageCart()
    {
        return false;
    }

    @Override
    public boolean canBeRidden()
    {
        return false;
    }

    @Override
    public int getSize()
    {
        return 3;
    }

    @Override
    public String getName()
    {
        return RailcraftLanguage.translate("entity.cart.tank");
    }

    @Override
    protected void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);
        setLiquidQuantity(nbttagcompound.getInt("Tank"));
        setLiquidId(nbttagcompound.getInt("Liquid"));
        if(getLiquidId() == 0) {
            setLiquidQuantity(0);
        }
    }

    @Override
    protected void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);
        nbttagcompound.setInt("Tank", getLiquidQuantity());
        nbttagcompound.setInt("Liquid", getLiquidId());
    }

    public int getGaugeScale(int i)
    {
        return (int)(((float)getLiquidQuantity() / (float)(getCapacity())) * (float)i);
    }

    @Override
    public int fill(Orientations from, int quantity, int id, boolean doFill)
    {
        if(hasFilter() && id != getFilterLiquid().itemID) {
            return 0;
        }
        if(getLiquidQuantity() != 0 && id != getLiquidId()) {
            return 0;
        }
        setLiquidId(id);

        int used = 0;
        if(getLiquidQuantity() + quantity <= getCapacity()) {
            if(doFill) {
                setLiquidQuantity(getLiquidQuantity() + quantity);
            }

            used = quantity;
        } else if(getLiquidQuantity() <= getCapacity()) {
            used = getCapacity() - getLiquidQuantity();

            if(doFill) {
                setLiquidQuantity(getCapacity());
            }
        }

        return used;
    }

    @Override
    public int empty(int quantityMax, boolean doEmpty)
    {
        int result = 0;
        if(getLiquidQuantity() >= quantityMax) {
            if(doEmpty) {
                setLiquidQuantity(getLiquidQuantity() - quantityMax);
            }
            result = quantityMax;
        } else {
            result = getLiquidQuantity();

            if(doEmpty) {
                setLiquidQuantity(0);
            }
        }

        return result;
    }

    @Override
    public void setFilling(boolean fill)
    {
        datawatcher.watch(FILLING_DATA_ID, Byte.valueOf(fill ? 1 : (byte)0));
    }

    @Override
    public boolean isFilling()
    {
        return datawatcher.getByte(FILLING_DATA_ID) != 0;
    }

    @Override
    public int getLiquidQuantity()
    {
        return datawatcher.getInt(LIQUID_QTY_DATA_ID);
    }

    @Override
    public int getCapacity()
    {
        return RailcraftConfig.getTankCartCapacity();
    }

    @Override
    public int getLiquidId()
    {
        return datawatcher.getInt(LIQUID_ID_DATA_ID);
    }

    public boolean hasFilter()
    {
        return getFilter() != null;
    }

    public LiquidStack getFilterLiquid()
    {
        ItemStack filter = getFilter();
        if(filter == null) {
            return null;
        }
        return LiquidManager.getInstance().getLiquidInContainer(filter);
    }

    public ItemStack getFilter()
    {
        return getItem(0);
    }

    @Override
    public LiquidSlot[] getLiquidSlots()
    {
        return new LiquidSlot[]{
                new LiquidSlot(getLiquidId(), getLiquidQuantity(), getCapacity())
            };
    }

    protected void setLiquidQuantity(int liquidQty)
    {
        datawatcher.watch(LIQUID_QTY_DATA_ID, liquidQty);
    }

    protected void setLiquidId(int liquidId)
    {
        datawatcher.watch(LIQUID_ID_DATA_ID, liquidId);
    }

    @Override
    public void writeSpawnData(DataOutputStream data) throws IOException
    {
        data.writeInt(getLiquidId());
        data.writeInt(getLiquidQuantity());

        data.writeInt(getFilter() != null ? getFilter().id : 0);
        data.writeInt(getFilter() != null ? getFilter().getData() : 0);
    }

    @Override
    public void readSpawnData(DataInputStream data) throws IOException
    {
        setLiquidId(data.readInt());
        setLiquidQuantity(data.readInt());

        int filterId = data.readInt();
        int filterMeta = data.readInt();
        if(filterId > 0) {
            ItemStack filter = new ItemStack(filterId, 1, filterMeta);
            setItem(0, filter);
        }
    }

    @Override
    public boolean addItem(ItemStack stack, boolean doAdd, Orientations from)
    {
        if(LiquidManager.getInstance().isContainer(stack)) {
            IInventory slot = new InventoryMapper(this, SLOT_INPUT, 1);
            if(!doAdd) {
                stack = stack.cloneItemStack();
                slot = new InventoryCopy(slot);
            }
            ItemStack ret = InventoryTools.moveItemStack(stack, slot);
            if(ret != null && stack.count != ret.count) {
                stack.count = ret.count;
                return true;
            } else if(ret == null) {
                stack.count = 0;
                return true;
            }
        }
        return false;
    }

    @Override
    public ItemStack extractItem(boolean doRemove, Orientations from)
    {
        IInventory slot = new InventoryMapper(this, SLOT_INPUT, 1);
        if(!doRemove) {
            slot = new InventoryCopy(slot);
        }
        return slot.splitStack(0, 1);
    }

    @Override
    public int getStartInventorySide(int side)
    {
        if(side == EnumDirection.UP.getValue()) {
            return SLOT_INPUT;
        }
        return SLOT_OUTPUT;
    }

    @Override
    public int getSizeInventorySide(int side)
    {
        return 1;
    }

    @Override
    public boolean doesCartMatchFilter(ItemStack stack, EntityMinecart cart)
    {
        if(ItemCart.getCartType(stack) == EnumCart.TANK) {
            return true;
        }
        return false;
    }

    @Override
    public int offerLiquid(Object source, int quantity, int id)
    {
        if(getLiquidQuantity() > 0 && getLiquidId() != 0 && getLiquidId() != id) {
            return 0;
        }
        int used = fill(Orientations.Unknown, quantity, id, true);

        if(used >= quantity) {
            return 0;
        }

        LinkageManager lm = LinkageManager.getInstance();

        EntityMinecart linkedCart = lm.getLinkedCartA(this);
        if(linkedCart != source && linkedCart instanceof ILiquidTransfer) {
            used += ((ILiquidTransfer)linkedCart).offerLiquid(this, quantity - used, id);
        }

        if(used >= quantity) {
            return 0;
        }

        linkedCart = lm.getLinkedCartB(this);
        if(linkedCart != source && linkedCart instanceof ILiquidTransfer) {
            used += ((ILiquidTransfer)linkedCart).offerLiquid(this, quantity - used, id);
        }

        return quantity - used;
    }

    @Override
    public int requestLiquid(Object source, int quantity, int id)
    {
        if(getLiquidId() != id) {
            return 0;
        }
        int offer = empty(quantity, true);

        if(offer >= quantity) {
            return offer;
        }

        LinkageManager lm = LinkageManager.getInstance();

        EntityMinecart linkedCart = lm.getLinkedCartA(this);
        if(linkedCart != source && linkedCart instanceof ILiquidTransfer) {
            offer += ((ILiquidTransfer)linkedCart).requestLiquid(this, quantity - offer, id);
        }

        if(offer >= quantity) {
            return offer;
        }

        linkedCart = lm.getLinkedCartB(this);
        if(linkedCart != source && linkedCart instanceof ILiquidTransfer) {
            offer += ((ILiquidTransfer)linkedCart).requestLiquid(this, quantity - offer, id);
        }

        return offer;
    }
}
