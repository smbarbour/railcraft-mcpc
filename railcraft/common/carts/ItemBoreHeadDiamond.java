package railcraft.common.carts;

public class ItemBoreHeadDiamond extends ItemBoreHead
{

    public ItemBoreHeadDiamond(int i)
    {
        super(i);
        d(168);
        setMaxDurability(6000);
        a("boreHeadDiamond");
    }

    @Override
    public String getBoreTexture()
    {
        return "/railcraft/textures/tunnel_bore_diamond.png";
    }

    @Override
    public int getHarvestLevel()
    {
        return 3;
    }

    @Override
    public float getDigModifier()
    {
        return 1.4f;
    }
}
