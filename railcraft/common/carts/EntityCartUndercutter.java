package railcraft.common.carts;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.*;
import buildcraft.api.ISpecialInventory;
import buildcraft.api.Orientations;
import forge.ForgeHooks;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.EnumGui;
import railcraft.common.RailcraftLanguage;
import railcraft.common.api.BukkitHandlers;
import railcraft.common.api.CartBase;
import railcraft.common.api.IItemTransfer;
import railcraft.common.api.InventoryTools;

public class EntityCartUndercutter extends CartBase implements ISpecialInventory
{

	private static final double DRAG_FACTOR = 0.9;
	private static final float MAX_SPEED = 0.1f;
	private static final int BLINK_DURATION = 3;
	private static final int DATA_ID_BLINK = 20;
	private static final int SLOT_A_EXIST = 0;
	private static final int SLOT_B_EXIST = 1;
	private static final int SLOT_REPLACE = 2;
	private static final int SLOT_STOCK = 3;

    public EntityCartUndercutter(World world)
    {
        super(world);
    }

	public EntityCartUndercutter(World world, String player)
	{
		super(world, player);
	}

	public EntityCartUndercutter(World world, double d, double d1, double d2, String player)
	{
		this(world, player);
		setPosition(d, d1 + (double)height, d2);
		motX = 0.0D;
		motY = 0.0D;
		motZ = 0.0D;
		lastX = d;
		lastY = d1;
		lastZ = d2;
	}

	@Override
	protected void b()
	{
		super.b();
		datawatcher.a(DATA_ID_BLINK, new Byte((byte)0));
	}

	private void blink()
	{
		datawatcher.watch(DATA_ID_BLINK, Byte.valueOf((byte)BLINK_DURATION));
	}

	private void setBlink(byte blink)
	{
		datawatcher.watch(DATA_ID_BLINK, Byte.valueOf((byte)blink));
	}

	private byte getBlink()
	{
		return datawatcher.getByte(DATA_ID_BLINK);
	}

	public boolean isBlinking()
	{
		return datawatcher.getByte(DATA_ID_BLINK) > 0;
	}

	@Override
	public List<ItemStack> getItemsDropped()
	{
		List<ItemStack> items = new ArrayList<ItemStack>();
		items.add(getCartItem());
		return items;
	}

	@Override
	protected double getDrag()
	{
		return DRAG_FACTOR;
	}

	@Override
	public float getMaxSpeedRail()
	{
		NBTTagCompound data = getEntityData();
		if(data.getBoolean("SpeedTest")) {
			return MAX_SPEED;
		}
		float speed = data.getFloat("TrainSpeed");
		return speed > 0 ? speed : MAX_SPEED;
	}

	@Override
	public void F_()
	{
		super.F_();
		if(Railcraft.gameIsNotHost()) {
			return;
		}

		if(isBlinking()) {
			setBlink((byte)(getBlink() - 1));
		}

		stock(SLOT_REPLACE, SLOT_STOCK);

		replace(SLOT_A_EXIST, SLOT_STOCK);
		replace(SLOT_B_EXIST, SLOT_STOCK);
	}

	private void offerOrDropItem(ItemStack stack)
	{
		EntityMinecart link_A = LinkageManager.getInstance().getLinkedCartA(this);
		EntityMinecart link_B = LinkageManager.getInstance().getLinkedCartB(this);

		if(stack != null && stack.count > 0 && link_A instanceof IItemTransfer) {
			stack = ((IItemTransfer)link_A).offerItem(this, stack);
		}
		if(stack != null && stack.count > 0 && link_B instanceof IItemTransfer) {
			stack = ((IItemTransfer)link_B).offerItem(this, stack);
		}

		if(stack != null && stack.count > 0) {
			a(stack, 1);
		}
	}

	private void stock(int slotReplace, int slotStock)
	{
		ItemStack trackReplace = getItem(slotReplace);

		ItemStack trackStock = getItem(slotStock);

		if(trackStock != null && !InventoryTools.isItemEqual(trackReplace, trackStock)) {
			offerOrDropItem(trackStock);
			setItem(slotStock, null);
		}

		if(trackReplace == null) {
			return;
		}

		trackStock = getItem(slotStock);

		EntityMinecart link_A = LinkageManager.getInstance().getLinkedCartA(this);
		EntityMinecart link_B = LinkageManager.getInstance().getLinkedCartB(this);

		if(trackStock == null || trackStock.count < trackStock.getMaxStackSize()) {
			ItemStack stack = null;
			if(link_A instanceof IItemTransfer) {
				stack = ((IItemTransfer)link_A).requestItem(this, trackReplace);
				if(stack != null) {
					if(trackStock == null) {
						setItem(slotStock, stack);
					} else {
						trackStock.count++;
					}
				}
			}
			if(stack == null && link_B instanceof IItemTransfer) {
				stack = ((IItemTransfer)link_B).requestItem(this, trackReplace);
				if(stack != null) {
					if(trackStock == null) {
						setItem(slotStock, stack);
					} else {
						trackStock.count++;
					}
				}
			}
		}
	}

	private void replace(int slotExist, int slotStock)
	{
		int i = MathHelper.floor(this.locX);
		int j = MathHelper.floor(this.locY);
		int k = MathHelper.floor(this.locZ);

		if(BlockMinecartTrack.g(this.world, i, j - 1, k)) {
			--j;
		}

		int id = this.world.getTypeId(i, j, k);

		if(BlockMinecartTrack.d(id)) {
			ItemStack exist = getItem(slotExist);
			ItemStack stock = getItem(slotStock);
			if(exist != null && stock != null) {
				j--;
				id = world.getTypeId(i, j, k);
				if(exist.id == id || (exist.id == Block.DIRT.id && id == Block.GRASS.id)) {
					if(!world.isEmpty(i, j - 1, k)) {
						int meta = world.getData(i, j, k);
						List<ItemStack> drops = Block.byId[id].getBlockDropped(world, i, j, k, meta, 0);


						meta = 0;
						if(stock.getItem().e()) {
							meta = stock.getData();
						}
						if (BukkitHandlers.placeBlockWithEvent(this.getBukkitEntity().getServer(), world, stock, i, j, k, i, j, k, true, BukkitHandlers.getEntityByName(world, this.owner))) { //world.setTypeIdAndData(i, j, k, stock.id, meta);
							world.makeSound(this, "step.gravel", (1f + 1.0F) / 2.0F, 1f * 0.8F);
							splitStack(slotStock, 1);
							for(ItemStack stack : drops) {
								offerOrDropItem(stack);
							}

							blink();
						}
					}
				}
			}
		}
	}

	@Override
	public boolean b(EntityHuman player)
	{
		if(!ForgeHooks.onMinecartInteract(this, player)) {
			return true;
		}
		if(Railcraft.gameIsHost()) {
			GuiHandler.openGui(EnumGui.CART_UNDERCUTTER, player, world, this);
		}
		return true;
	}

	@Override
	public boolean isStorageCart()
	{
		return false;
	}

	@Override
	public boolean canBeRidden()
	{
		return false;
	}

	@Override
	public int getSize()
	{
		return 4;
	}

	@Override
	public String getName()
	{
		return RailcraftLanguage.translate(EnumCart.UNDERCUTTER.getTag());
	}

	@Override
	public boolean addItem(ItemStack stack, boolean doAdd, Orientations from)
	{
		ItemStack trackReplace = getItem(SLOT_REPLACE);
		ItemStack trackStock = getItem(SLOT_STOCK);


		if(InventoryTools.isItemEqual(stack, trackReplace)) {
			if(trackStock == null) {
				if(doAdd) {
					setItem(SLOT_STOCK, stack);
					stack.count = 0;
				}
				return true;
			} else if(trackStock.count + stack.count <= trackStock.getMaxStackSize()) {
				if(doAdd) {
					trackStock.count += stack.count;
					stack.count = 0;
				}
				return true;
			}
		}

		return false;
	}

	@Override
	public ItemStack extractItem(boolean doRemove, Orientations from)
	{
		return null;
	}
}
