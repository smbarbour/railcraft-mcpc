package railcraft.common.carts;

import railcraft.common.api.CartBase;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.*;
import forge.ForgeHooks;
import forge.ISpawnHandler;
import ic2.api.ElectricItem;
import ic2.api.IElectricItem;
import ic2.api.Items;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.EnumGui;
import railcraft.common.IEnergyDevice;
import railcraft.common.RailcraftConfig;
import railcraft.common.api.APIErrorHandler;
import railcraft.common.api.CartTools;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.ICartRenderInterface;
import railcraft.common.api.IEnergyTransfer;
import railcraft.common.api.ILinkageManager;

public class EntityCartEnergy extends CartBase implements IEnergyDevice, ISpawnHandler, ICartRenderInterface, IEnergyTransfer
{

    private static final int ENERGY_DATA_ID = 20;
    private static final double DRAG_FACTOR = 0.99199997901916504D;
    private static final int[] CAPACITY = {40000, 600000, 10000000};
    private static final int[] TRANSFER_RATE = {128, 512, 2048};
    private int update;

    public EntityCartEnergy(World world)
    {
        super(world);
    }

    public EntityCartEnergy(World world, String player)
    {
        super(world, player);
    }

    public EntityCartEnergy(World world, double d, double d1, double d2, String player)
    {
        this(world, player);
        setPosition(d, d1 + (double)height, d2);
        motX = 0.0D;
        motY = 0.0D;
        motZ = 0.0D;
        lastX = d;
        lastY = d1;
        lastZ = d2;
    }

    @Override
    protected void b()
    {
        super.b();
        this.datawatcher.a(ENERGY_DATA_ID, new Integer(0));
    }

    @Override
    public List<ItemStack> getItemsDropped()
    {
        List<ItemStack> items = new ArrayList<ItemStack>();
        ItemStack stack = getIC2Item();
        if(RailcraftConfig.doCartsBreakOnDrop() && stack != null) {
            items.add(new ItemStack(Item.MINECART));
            items.add(stack);
        } else {
            items.add(getCartItem());
        }
        return items;
    }

    @Override
    public void F_()
    {
        update++;

        super.F_();

        if(Railcraft.gameIsNotHost()) {
            return;
        }

        if(getEnergy() > getCapacity()) {
            setEnergy(getCapacity());
        }

        ItemStack stack = getItem(0);
        if(stack != null && stack.getItem() instanceof IElectricItem && getEnergy() > 0) {
            setEnergy(getEnergy() - ElectricItem.charge(stack, getEnergy(), getTier(), false, false));
        }

        stack = getItem(1);
        if(stack != null && stack.getItem() instanceof IElectricItem && ((IElectricItem)stack.getItem()).canProvideEnergy() && getEnergy() < getCapacity()) {
            setEnergy(getEnergy() + ElectricItem.discharge(stack, getCapacity() - getEnergy(), getTier(), false, false));
        }
    }

    @Override
    public int getTier()
    {
        return type + 1;
    }

    @Override
    protected double getDrag()
    {
        return DRAG_FACTOR;
    }

    @Override
    public boolean b(EntityHuman player)
    {
        if(!ForgeHooks.onMinecartInteract(this, player)) {
            return true;
        }
        if(Railcraft.gameIsHost()) {
            GuiHandler.openGui(EnumGui.CART_ENERGY, player, world, this);
        }
        return true;
    }

    @Override
    public boolean isStorageCart()
    {
        return false;
    }

    @Override
    public boolean canBeRidden()
    {
        return false;
    }

    @Override
    public int getSize()
    {
        return 2;
    }

    @Override
    public String getName()
    {
        switch (type) {
            case 1:
                return "MFE Cart";
            case 2:
                return "MFSU Cart";
            default:
                return "BatBox Cart";
        }
    }

    @Override
    protected void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);
        setEnergy(nbttagcompound.getInt("energy"));

        update = GeneralTools.getRand().nextInt();
    }

    @Override
    protected void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);
        nbttagcompound.setInt("energy", getEnergy());
    }

    @Override
    public int injectEnergy(Object source, int amount, int tier, boolean ignoreTransferLimit, boolean simulate)
    {
        return injectEnergy(source, amount, tier, ignoreTransferLimit, simulate, false);
    }

    @Override
    public int injectEnergy(Object source, int amount, int tier, boolean ignoreTransferLimit, boolean simulate, boolean passAlong)
    {
        if(tier < getTier()) {
            return amount;
        }
        int extra = 0;
        if(!ignoreTransferLimit) {
            extra = Math.max(amount - getTransferLimit(), 0);
            amount = Math.min(amount, getTransferLimit());
        }
        int energy = getEnergy() + amount;
        int capacity = getCapacity();
        if(energy > capacity) {
            extra += energy - capacity;
            energy = capacity;
        }
        if(!simulate) {
            setEnergy(energy);
        }

        if(!passAlong) {
            return extra;
        }

        try {
            ILinkageManager lm = CartTools.getLinkageManager();

            EntityMinecart linkedCart = lm.getLinkedCartA(this);
            if(extra > 0 && linkedCart != source && linkedCart instanceof IEnergyTransfer) {
                extra = ((IEnergyTransfer)linkedCart).injectEnergy(this, extra, tier, ignoreTransferLimit, simulate, true);
            }

            linkedCart = lm.getLinkedCartB(this);
            if(extra > 0 && linkedCart != source && linkedCart instanceof IEnergyTransfer) {
                extra = ((IEnergyTransfer)linkedCart).injectEnergy(this, extra, tier, ignoreTransferLimit, simulate, true);
            }
        } catch (Throwable t) {
            APIErrorHandler.versionMismatch(IEnergyTransfer.class);
        }

        return extra;
    }

    @Override
    public int extractEnergy(Object source, int amount, int tier, boolean ignoreTransferLimit, boolean simulate)
    {
        return extractEnergy(source, amount, tier, ignoreTransferLimit, simulate, false);
    }

    @Override
    public int extractEnergy(Object source, int amount, int tier, boolean ignoreTransferLimit, boolean simulate, boolean passAlong)
    {
        if(tier < getTier()) {
            return 0;
        }
        if(!ignoreTransferLimit) {
            amount = Math.min(amount, getTransferLimit());
        }
        int provide = 0;
        int energy = getEnergy();
        provide = Math.min(amount, energy);
        energy -= provide;
        if(energy < 0) {
            energy = 0;
        }
        if(!simulate) {
            setEnergy(energy);
        }

        if(!passAlong) {
            return provide;
        }

        ILinkageManager lm = CartTools.getLinkageManager();

        EntityMinecart linkedCart = lm.getLinkedCartA(this);
        if(provide < amount && linkedCart != source && linkedCart instanceof IEnergyTransfer) {
            provide += ((IEnergyTransfer)linkedCart).extractEnergy(this, amount - provide, tier, ignoreTransferLimit, simulate, true);
        }

        linkedCart = lm.getLinkedCartB(this);
        if(provide < amount && linkedCart != source && linkedCart instanceof IEnergyTransfer) {
            provide += ((IEnergyTransfer)linkedCart).extractEnergy(this, amount - provide, tier, ignoreTransferLimit, simulate, true);
        }

        return provide;
    }

    public ItemStack getIC2Item()
    {
        switch (type) {
            case 1:
                return Items.getItem("mfeUnit");
            case 2:
                return Items.getItem("mfsUnit");
            default:
                return Items.getItem("batBox");
        }
    }

    @Override
    public Block getBlock()
    {
        ItemStack stack = getIC2Item();
        if(stack != null) {
            return Block.byId[stack.id];
        }
        return null;
    }

    @Override
    public int getBlockMetadata()
    {
        ItemStack stack = getIC2Item();
        if(stack != null) {
            return stack.getData();
        }
        return 0;
    }

    @Override
    public int getCapacity()
    {
        return CAPACITY[type];
    }

    @Override
    public int getTransferLimit()
    {
        return TRANSFER_RATE[type];
    }

    @Override
    public int getEnergyBarScaled(int scale)
    {
        return (getEnergy() * scale) / getCapacity();
    }

    @Override
    public int getEnergy()
    {
        return datawatcher.getInt(ENERGY_DATA_ID);
    }

    protected void setEnergy(int energy)
    {
        datawatcher.watch(ENERGY_DATA_ID, energy);
    }

    @Override
    public void writeSpawnData(DataOutputStream data)
    {
        try {
            data.writeByte((byte)type);
        } catch (IOException ex) {
        }
    }

    @Override
    public void readSpawnData(DataInputStream data)
    {
        try {
            type = data.readByte();
        } catch (IOException ex) {
        }
    }

    @Override
    public boolean canExtractEnergy()
    {
        return true;
    }

    @Override
    public boolean canInjectEnergy()
    {
        return true;
    }
}
