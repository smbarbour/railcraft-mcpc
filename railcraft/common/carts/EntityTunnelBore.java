// 5.4.7 - Source port
package railcraft.common.carts;

import railcraft.common.api.IBoreHead;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.craftbukkit.block.CraftBlockState;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.bukkit.event.block.BlockPlaceEvent;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockMinecartTrack;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityItem;
import net.minecraft.server.EntityLiving;
import net.minecraft.server.EntityMinecart;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.IInventory;
import net.minecraft.server.Item;
import net.minecraft.server.ItemInWorldManager;
import net.minecraft.server.ItemStack;
import net.minecraft.server.MathHelper;
import net.minecraft.server.ModLoader;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import forge.ForgeHooks;
import forge.ISpawnHandler;
import railcraft.Railcraft;
import railcraft.common.RailcraftConfig;
import railcraft.GuiHandler;
import railcraft.common.api.EnumDirection;
import railcraft.common.EnumGui;
import railcraft.common.api.BallastRegistry;
import railcraft.common.api.BukkitHandlers;
import railcraft.common.api.EnumItemType;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.IMineable;
import railcraft.common.api.ICrowbar;
import railcraft.common.api.IItemTransfer;
import railcraft.common.api.ILinkableCart;
import railcraft.common.api.InventoryMapper;
import railcraft.common.api.InventoryTools;
import railcraft.common.api.RailTools;
import railcraft.common.api.tracks.EnumTrackMeta;

// Referenced classes of package net.minecraft.server:
//            Entity, IInventory, ItemStack, World,
//            Item, EntityItem, Block, MathHelper,
//            BlockMinecartTrack, Vec3D, AxisAlignedBB, NBTTagCompound,
//            NBTTagList, EntityLiving, EntityPlayer, InventoryPlayer
public class EntityTunnelBore extends EntityMinecart implements IInventory, ISpawnHandler, ILinkableCart
{

    public static final float SPEED = 0.008F;
    public static final float LENGTH = 6f;
    public static final int MAX_FILL_DEPTH = 10;
    public static final int FAIL_DELAY = 200;
    public static final int FUEL_CONSUMPTION = 8;
    public static final float HARDNESS_MULTIPLER = 20;
    protected static final int WATCHER_ID_MOVING = 20;
    protected static final int WATCHER_ID_BORE_HEAD = 21;
    protected static final int WATCHER_ID_FACING = 5;
    protected static final int WATCHER_ID_BURN_TIME = 22;
    protected boolean degreeCalc = false;
    protected int delay = 0;
    private boolean active;
    private int update;
    private boolean hasInit;
    protected boolean placeRail = false;
    protected boolean placeBallast = false;
    protected boolean boreLayer = false;
    protected final IInventory invFuel = new InventoryMapper(this, 1, 6);
    protected final IInventory invBallast = new InventoryMapper(this, 7, 9);
    protected final IInventory invRails = new InventoryMapper(this, 16, 9);
    protected int boreRotationAngle = 0;
    private static final Set<Mineable> mineableBlocks = new HashSet<Mineable>();
    private static final int[] mineable = {
        Block.STONE.id,
        Block.GRASS.id,
        Block.DIRT.id,
        Block.COBBLESTONE.id,
        Block.SAPLING.id,
        Block.SAND.id,
        Block.GRAVEL.id,
        Block.GOLD_ORE.id,
        Block.IRON_ORE.id,
        Block.COAL_ORE.id,
        Block.WOOD.id,
        Block.LEAVES.id,
        Block.LAPIS_ORE.id,
        Block.SANDSTONE.id,
        //        Block.railPowered.blockID,
        //        Block.railDetector.blockID,
        Block.WEB.id,
        Block.LONG_GRASS.id,
        Block.DEAD_BUSH.id,
        Block.YELLOW_FLOWER.id,
        Block.RED_ROSE.id,
        Block.BROWN_MUSHROOM.id,
        Block.RED_MUSHROOM.id,
        Block.MOSSY_COBBLESTONE.id,
        Block.OBSIDIAN.id,
        Block.TORCH.id,
        Block.FIRE.id,
        Block.DIAMOND_ORE.id,
        Block.CROPS.id,
        Block.SOIL.id,
        //        Block.rail.blockID,
        Block.REDSTONE_ORE.id,
        Block.GLOWING_REDSTONE_ORE.id,
        Block.SNOW.id,
        Block.ICE.id,
        Block.SNOW_BLOCK.id,
        Block.CACTUS.id,
        Block.CLAY.id,
        Block.SUGAR_CANE_BLOCK.id,
        Block.PUMPKIN.id,
        Block.NETHERRACK.id,
        Block.SOUL_SAND.id,
        Block.GLOWSTONE.id,
        Block.VINE.id,
        Block.WATER_LILY.id,
        Block.MELON.id,
        Block.MELON_STEM.id,
        Block.NETHER_WART.id,
        Block.BIG_MUSHROOM_1.id,
        Block.BIG_MUSHROOM_2.id,
        Block.MYCEL.id,
        Block.PUMPKIN_STEM.id,
        Block.WHITESTONE.id,};
    public static final Set<Integer> replaceableBlocks = new HashSet<Integer>();
    private static final int[] replaceable = {
        Block.TORCH.id,
        Block.LONG_GRASS.id,
        Block.DEAD_BUSH.id,
        Block.VINE.id,
        Block.BROWN_MUSHROOM.id,
        Block.RED_MUSHROOM.id,
        Block.YELLOW_FLOWER.id,
        Block.RED_ROSE.id,};
    private String owner;
    private boolean playerActivated = false;

    static {
        for(int id : mineable) {
            addMineableBlock(id);
        }
        for(int id : replaceable) {
            replaceableBlocks.add(id);
        }
    }

    private static class Mineable
    {

        public int id;
        public int meta;

        public Mineable(int id, int meta)
        {
            this.id = id;
            this.meta = meta;
        }

        @Override
        public boolean equals(Object obj)
        {
            if(obj == null) {
                return false;
            }
            if(getClass() != obj.getClass()) {
                return false;
            }
            final Mineable other = (Mineable)obj;
            if(this.id != other.id) {
                return false;
            }
            if(this.meta != other.meta) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode()
        {
            int hash = 7;
            hash = 41 * hash + this.id;
            hash = 41 * hash + this.meta;
            return hash;
        }
    }

    public EntityTunnelBore(World world, double i, double j, double k, EntityHuman owner)
    {
        this(world, i, j, k, 0, owner);
    }

    public EntityTunnelBore(World world, double i, double j, double k, int f, EntityHuman owner)
    {
        super(world);
        hasInit = true;
        setPosition(i, j + (double)height, k);
        motX = 0.0D;
        motY = 0.0D;
        motZ = 0.0D;
        lastX = i;
        lastY = j;
        lastZ = k;
        items = new ItemStack[25];
        setFacing((byte)f);
        b(LENGTH, 2.7F);

        update = GeneralTools.getRand().nextInt();
        if (owner instanceof EntityHuman)
        {
        	this.owner = owner.name;
        }
    }

    public EntityTunnelBore(World world)
    {
        this(world, 0, 0, 0, 0, null);
    }

    public static void addMineableBlock(int id)
    {
        addMineableBlock(id, -1);
    }

    public static void addMineableBlock(int id, int meta)
    {
        mineableBlocks.add(new Mineable(id, meta));
    }

    private boolean isMinableBlock(int id, int meta)
    {
        if(RailcraftConfig.boreMinesAllBlocks()) {
            return true;
        }
        Mineable mine = new Mineable(id, -1);
        if(mineableBlocks.contains(mine)) {
            return true;
        }
        mine.meta = meta;
        if(mineableBlocks.contains(mine)) {
            return true;
        }
        return false;
    }

    @Override
    protected void b()
    {
        super.b();
//        dataWatcher.addObject(WATCHER_ID_DELAY, Integer.valueOf(0));
        datawatcher.a(WATCHER_ID_MOVING, Byte.valueOf((byte)0));
        datawatcher.a(WATCHER_ID_BORE_HEAD, Integer.valueOf(-1));
        datawatcher.a(WATCHER_ID_FACING, Byte.valueOf((byte)0));
        datawatcher.a(WATCHER_ID_BURN_TIME, Integer.valueOf(0));
    }

    private void setYaw()
    {
        float yaw = 0;
        switch (getFacing()) {
            case 0:
                yaw = 90;
                break;
            case 1:
                yaw = 0;
                break;
            case 2:
                yaw = 270;
                break;
            case 3:
                yaw = 180;
                break;
        }
        c(yaw, pitch);
    }

    @Override
    public int getSize()
    {
        return items.length;
    }

    @Override
    public boolean e_()
    {
        return false;
    }

    @Override
    public void setPosition(double i, double j, double k)
    {
        if(!hasInit) {
            super.setPosition(i, j, k);
            return;
        }

        locX = i;
        locY = j;
        locZ = k;
        double w = 2.7 / 2.0;
        double h = 2.7;
        double l = LENGTH / 2.0;
        double x1 = i;
        double x2 = i;
        double z1 = k;
        double z2 = k;
        if(getFacing() == 1 || getFacing() == 3) {
            x1 -= l;
            x2 += l;
            z1 -= w;
            z2 += w;
        } else {
            x1 -= w;
            x2 += w;
            z1 -= l;
            z2 += l;
        }

        boundingBox.c(x1, (j - (double)height) + (double)bO, z1, x2, (j - (double)height) + (double)bO + h, z2);
    }

    @Override
    public void F_()
    {
        update++;

        if(Railcraft.gameIsHost()) {
            if(update % 100 == 0) {
                forceUpdateBoreHead();
                a(false);
                setMoving(false);
            } else {
                updateBoreHead();
            }

            stockBallast();
            stockTracks();
        }

        super.F_();

        if(Railcraft.gameIsHost()) {
            if(hasFuel() && getDelay() == 0) {
                setActive(true);
//            System.out.println("Yaw = " + MathHelper.floor(rotationYaw));

                int i = MathHelper.floor(locX);
                int j = MathHelper.floor(locY);
                int k = MathHelper.floor(locZ);
                EnumTrackMeta dir = EnumTrackMeta.NORTH_SOUTH;
                if(getFacing() == 1 || getFacing() == 3) {
                    dir = EnumTrackMeta.EAST_WEST;
                }

                if(getDelay() == 0) {
                    float offset = 1.5f;
                    i = MathHelper.floor(getXAhead(locX, offset));
                    k = MathHelper.floor(getZAhead(locZ, offset));

                    if(placeBallast) {
                        boolean placed = placeBallast(i, j - 1, k);
                        if(placed) {
                            setDelay(5);
                        } else {
                            setDelay(FAIL_DELAY);
                            setActive(false);
                        }
                        placeBallast = false;
                    } else if(!world.isBlockSolidOnSide(i, j - 1, k, EnumDirection.UP.getValue())) {
                        placeBallast = true;
                        setDelay(20);
                    }
                }

                if(getDelay() == 0) {
                    float offset = 0.8f;
                    i = MathHelper.floor(getXAhead(locX, offset));
                    k = MathHelper.floor(getZAhead(locZ, offset));

                    if(placeRail) {
                        boolean placed = placeTrack(i, j, k, dir);
                        if(placed) {
                            setDelay(5);
                        } else {
                            setDelay(FAIL_DELAY);
                            setActive(false);
                        }
                        placeRail = false;
                    } else if(BlockMinecartTrack.g(world, i, j, k)) {
                        if(!dir.isEqual(((BlockMinecartTrack)Block.byId[world.getTypeId(i, j, k)]).getBasicRailMetadata(world, this, i, j, k))) {
                            world.setRawData(i, j, k, dir.getValue());
                            setDelay(5);
                        }
                    } else {
                        int id = world.getTypeId(i, j, k);
                        if(id == 0 || replaceableBlocks.contains(id)) {
                            placeRail = true;
                            setDelay(5);
                        } else {
                            setDelay(FAIL_DELAY);
                            setActive(false);
                        }
                    }
                }

                if(getDelay() == 0) {
                    float offset = 3.3f;
                    i = MathHelper.floor(getXAhead(locX, offset));
                    k = MathHelper.floor(getZAhead(locZ, offset));

                    if(boreLayer) {
                        boolean bored = boreLayer(i, j, k, dir);
                        if(bored) {
                            setDelay(60);
                        } else {
                            setDelay(FAIL_DELAY);
                            setActive(false);
                        }
                        boreLayer = false;
                    } else {
                        if(checkForLava(i, j, k, dir)) {
                            setDelay(FAIL_DELAY);
                            setActive(false);
                        } else {
                            setDelay((int)Math.ceil(getLayerHardness(i, j, k, dir)));
                            if(getDelay() != 0) {
                                boreLayer = true;
                            }
                        }
                    }
                }
            }

            if(k()) {
                double i = getXAhead(locX, 3.3);
                double k = getZAhead(locZ, 3.3);
                double size = 0.8;
                List entities = world.getEntities(this, AxisAlignedBB.b(i - size, locY, k - size, i + size, locY + 2, k + size));
                for(Object e : entities) {
                    if(e instanceof EntityLiving) {
                        EntityLiving ent = (EntityLiving)e;
                        ent.damageEntity(DamageSource.GENERIC, 1);
                    }
                }
            }

            setMoving(hasFuel() && getDelay() == 0);

            if(getDelay() > 0) {
                setDelay(getDelay() - 1);
            }
        }

        if(isMoving()) {
            float factorX = MathHelper.cos((float)Math.toRadians(yaw));
            float factorZ = -MathHelper.sin((float)Math.toRadians(yaw));
            motX = SPEED * factorX;
            motZ = SPEED * factorZ;
        } else {
            motX = 0.0D;
            motZ = 0.0D;
        }

        emitParticles(getFacing());

        if(k()) {
            boreRotationAngle += 5;
        }
    }

    @Override
    public float getMaxSpeedRail()
    {
        return SPEED;
    }

    @Override
    protected void updateFuel()
    {
        if(Railcraft.gameIsHost()) {
            if(k()) {
                spendFuel();
            }
            stockFuel();
            if(outOfFuel()) {
                addFuel();
            }
            a(hasFuel() && isActive() && playerActivated);
        }
    }

    protected double getXAhead(double x, double offset)
    {
        if(getFacing() == 1) {
            x += offset;
        } else if(getFacing() == 3) {
            x -= offset;
        }
        return x;
    }

    protected double getZAhead(double z, double offset)
    {
        if(getFacing() == 0) {
            z -= offset;
        } else if(getFacing() == 2) {
            z += offset;
        }
        return z;
    }

    protected void emitParticles(int yaw)
    {
        if(k()) {
            double randomFactor = 0.125;

            double forwardOffset = -0.35;
            double smokeYOffset = 2.4;
            double flameYOffset = 0.7;
            double smokeSideOffset = 0.92;
            double flameSideOffset = 1.14;
            double smokeX1 = locX;
            double smokeX2 = locX;
            double smokeZ1 = locZ;
            double smokeZ2 = locZ;

            double flameX1 = locX;
            double flameX2 = locX;
            double flameZ1 = locZ;
            double flameZ2 = locZ;
            if(getFacing() == 0) {
                smokeX1 += smokeSideOffset;
                smokeX2 -= smokeSideOffset;
                smokeZ1 += forwardOffset;
                smokeZ2 += forwardOffset;

                flameX1 += flameSideOffset;
                flameX2 -= flameSideOffset;
                flameZ1 += forwardOffset + (random.nextGaussian() * randomFactor);
                flameZ2 += forwardOffset + (random.nextGaussian() * randomFactor);
            } else if(getFacing() == 1) {
                smokeX1 -= forwardOffset;
                smokeX2 -= forwardOffset;
                smokeZ1 += smokeSideOffset;
                smokeZ2 -= smokeSideOffset;

                flameX1 -= forwardOffset + (random.nextGaussian() * randomFactor);
                flameX2 -= forwardOffset + (random.nextGaussian() * randomFactor);
                flameZ1 += flameSideOffset;
                flameZ2 -= flameSideOffset;
            } else if(getFacing() == 2) {
                smokeX1 += smokeSideOffset;
                smokeX2 -= smokeSideOffset;
                smokeZ1 -= forwardOffset;
                smokeZ2 -= forwardOffset;

                flameX1 += flameSideOffset;
                flameX2 -= flameSideOffset;
                flameZ1 -= forwardOffset + (random.nextGaussian() * randomFactor);
                flameZ2 -= forwardOffset + (random.nextGaussian() * randomFactor);
            } else if(getFacing() == 3) {
                smokeX1 += forwardOffset;
                smokeX2 += forwardOffset;
                smokeZ1 += smokeSideOffset;
                smokeZ2 -= smokeSideOffset;

                flameX1 += forwardOffset + (random.nextGaussian() * randomFactor);
                flameX2 += forwardOffset + (random.nextGaussian() * randomFactor);
                flameZ1 += flameSideOffset;
                flameZ2 -= flameSideOffset;
            }

            if(random.nextInt(4) == 0) {
                world.a("largesmoke", smokeX1, locY + smokeYOffset, smokeZ1, 0.0D, 0.0D, 0.0D);
                world.a("flame", flameX1, locY + flameYOffset + (random.nextGaussian() * randomFactor), flameZ1, 0.0D, 0.0D, 0.0D);
            }
            if(random.nextInt(4) == 0) {
                world.a("largesmoke", smokeX2, locY + smokeYOffset, smokeZ2, 0.0D, 0.0D, 0.0D);
                world.a("flame", flameX2, locY + flameYOffset + (random.nextGaussian() * randomFactor), flameZ2, 0.0D, 0.0D, 0.0D);
            }
        }
    }

    protected void stockBallast()
    {
        EntityMinecart link = LinkageManager.getInstance().getLinkedCartA(this);
        if(link instanceof IItemTransfer) {
            IItemTransfer tranfer = (IItemTransfer)link;

            for(int slot = 0; slot < invBallast.getSize(); slot++) {
                ItemStack stack = invBallast.getItem(slot);
                if(stack != null && !BallastRegistry.isItemBallast(stack)) {
                    stack = tranfer.offerItem(this, stack);
                    invBallast.setItem(slot, stack);
                    return;
                }
                if(stack == null) {
                    stack = tranfer.requestItem(this, EnumItemType.BALLAST);
                    InventoryTools.moveItemStack(stack, invBallast);
                    return;
                }
            }
        }
    }

    protected boolean placeBallast(int i, int j, int k)
    {
        if(!world.isBlockSolidOnSide(i, j, k, EnumDirection.UP.getValue())) {
            for(int inv = 0; inv < invBallast.getSize(); inv++) {
                ItemStack stack = invBallast.getItem(inv);
                if(stack != null && BallastRegistry.isItemBallast(stack)) {
                    for(int y = j; y > j - MAX_FILL_DEPTH; y--) {
                        if(world.isBlockSolidOnSide(i, y, k, EnumDirection.UP.getValue())) {
                            if (BukkitHandlers.placeBlockWithEvent(this.getBukkitEntity().getServer(), world, stack, i, j, k, i, j-1, k, true, BukkitHandlers.getEntityByName(world, this.owner))) {
                            	playerActivated = false;
                            	return false;
                            } else {
                            invBallast.splitStack(inv, 1);
                            return true;
                            }
                        }
                    }
                    return false;
                }
            }
        }
        return false;
    }

    protected void stockTracks()
    {
        EntityMinecart link = LinkageManager.getInstance().getLinkedCartA(this);
        if(link instanceof IItemTransfer) {
            IItemTransfer tranfer = (IItemTransfer)link;

            for(int slot = 0; slot < invRails.getSize(); slot++) {
                ItemStack stack = invRails.getItem(slot);
                if(stack != null && !InventoryTools.isItemType(stack, EnumItemType.RAIL)) {
                    stack = tranfer.offerItem(this, stack);
                    invRails.setItem(slot, stack);
                    return;
                }
                if(stack == null) {
                    stack = tranfer.requestItem(this, EnumItemType.RAIL);
                    InventoryTools.moveItemStack(stack, invRails);
                    return;
                }
            }
        }
    }

    protected boolean placeTrack(int i, int j, int k, EnumTrackMeta meta)
    {
        int id = world.getTypeId(i, j, k);
        if(replaceableBlocks.contains(id)) {
            int m = world.getData(i, j, k);
            world.setTypeId(i, j, k, 0);
            Block.byId[id].b(world, i, j, k, m, 0);
            id = world.getTypeId(i, j, k);
        }
        if(id == 0 && world.e(i, j - 1, k)) {
            for(int inv = 0; inv < invRails.getSize(); inv++) {
                ItemStack stack = invRails.getItem(inv);
                if(stack != null) {
                    boolean placed = RailTools.placeRailAt(stack, world, i, j, k, this.owner);
                    if(placed) {
                        world.setData(i, j, k, meta.getValue());
                        invRails.splitStack(inv, 1);
                    } else {
                    	playerActivated = false;
                    }
                    return placed;
                }
            }
        }
        return false;
    }

    protected boolean checkForLava(int i, int j, int k, EnumTrackMeta dir)
    {
        int xStart = i - 1;
        int zStart = k - 1;
        int xEnd = i + 1;
        int zEnd = k + 1;
        if(dir == EnumTrackMeta.NORTH_SOUTH) {
            xStart = i - 2;
            xEnd = i + 2;
        } else {
            zStart = k - 2;
            zEnd = k + 2;
        }

        for(int jj = j; jj < j + 4; jj++) {
            for(int ii = xStart; ii <= xEnd; ii++) {
                for(int kk = zStart; kk <= zEnd; kk++) {
                    int id = world.getTypeId(ii, jj, kk);
                    if(id == Block.STATIONARY_LAVA.id || id == Block.LAVA.id) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    protected boolean boreLayer(int i, int j, int k, EnumTrackMeta dir)
    {
        boolean clear = true;
        int ii = i;
        int kk = k;
        for(int jj = j; jj < j + 3; jj++) {
            clear = clear && mineBlock(ii, jj, kk, dir);
        }

        if(dir == EnumTrackMeta.NORTH_SOUTH) {
            ii--;
        } else {
            kk--;
        }
        for(int jj = j; jj < j + 3; jj++) {
            clear = clear && mineBlock(ii, jj, kk, dir);
        }

        ii = i;
        kk = k;
        if(dir == EnumTrackMeta.NORTH_SOUTH) {
            ii++;
        } else {
            kk++;
        }
        for(int jj = j; jj < j + 3; jj++) {
            clear = clear && mineBlock(ii, jj, kk, dir);
        }
        return clear;
    }

    protected boolean mineBlock(int i, int j, int k, EnumTrackMeta dir)
    {
        int id = world.getTypeId(i, j, k);
        Block block = Block.byId[id];
        if(id == 0) {
            return true;
        } else if(BlockMinecartTrack.d(id)) {
            int trackMeta = ((BlockMinecartTrack)block).getBasicRailMetadata(world, this, i, j, k);
            if(dir.isEqual(trackMeta)) {
                return true;
            }
        } else if(id == Block.TORCH.id) {
            return true;
        }

        ItemStack head = getItem(0);
        if(head == null) {
            return false;
        }

        int meta = world.getData(i, j, k);

        if(!canMineBlock(i, j, k, id, meta)) {
            return false;
        }

        ArrayList<ItemStack> items = block.getBlockDropped(world, i, j, k, meta, 0);
        EntityMinecart link = LinkageManager.getInstance().getLinkedCartA(this);

        for(ItemStack item : items) {
            if(InventoryTools.isItemType(item, EnumItemType.FUEL)) {
                item = InventoryTools.moveItemStack(item, invFuel);
            }

            if(item != null && item.count > 0 && item.id == Block.GRAVEL.id) {
                item = InventoryTools.moveItemStack(item, invBallast);
            }

            if(item != null && item.count > 0 && link instanceof IItemTransfer) {
                item = ((IItemTransfer)link).offerItem(this, item);
            }

            if(item != null && item.count > 0 && !RailcraftConfig.boreDestroysBlocks()) {
                float f = 0.7F;
                double xr = (double)(world.random.nextFloat() - 0.5D) * f;
                double yr = (double)(world.random.nextFloat() - 0.5D) * f;
                double zr = (double)(world.random.nextFloat() - 0.5D) * f;
                EntityItem entityitem = new EntityItem(world, getXAhead(locX, -3.2) + xr, locY + 0.3 + yr, getZAhead(locZ, -3.2) + zr, item);
                world.addEntity(entityitem);
            }
        }

        world.setTypeId(i, j, k, 0);

        head.setData(head.getData() + 1);
        if(head.getData() > head.i()) {
            setItem(0, null);
        }
        return true;
    }

    private boolean canMineBlock(int i, int j, int k, int id, int meta)
    {
        Block block = Block.byId[id];
        if(block instanceof IMineable) {
            ItemStack head = getItem(0);
            if(head == null) {
                return false;
            }
            
            if (BukkitHandlers.removeBlockWithEvent(this.getBukkitEntity().getServer(), this.world, i, j, k, this.owner)) {
                return ((IMineable)block).canMineBlock(world, i, j, k, this, head);            	
            } else {
            	playerActivated = false;
            	return false;
            }
        }
        return isMinableBlock(id, meta) && canHeadHarvestBlock(id, meta);
    }

	private boolean canHeadHarvestBlock(int id, int meta)
    {
        ItemStack head = getItem(0);
        if(head == null) {
            return false;
        }

        try {
            if(head.getItem() instanceof IBoreHead) {
                IBoreHead boreHead = (IBoreHead)head.getItem();

                Field harvest = ForgeHooks.class.getDeclaredField("toolHarvestLevels");
                harvest.setAccessible(true);
                Map harvestMap = (Map)harvest.get(null);

                boolean mappingExists = false;

                Integer blockHarvestLevel = (Integer)harvestMap.get(Arrays.asList(id, meta, "pickaxe"));
                if(blockHarvestLevel != null) {
                    if(boreHead.getHarvestLevel() >= blockHarvestLevel) {
                        return true;
                    }
                    mappingExists = true;
                }

                blockHarvestLevel = (Integer)harvestMap.get(Arrays.asList(id, meta, "axe"));
                if(blockHarvestLevel != null) {
                    if(boreHead.getHarvestLevel() >= blockHarvestLevel) {
                        return true;
                    }
                    mappingExists = true;
                }

                blockHarvestLevel = (Integer)harvestMap.get(Arrays.asList(id, meta, "shovel"));
                if(blockHarvestLevel != null) {
                    if(boreHead.getHarvestLevel() >= blockHarvestLevel) {
                        return true;
                    }
                    mappingExists = true;
                }

                if(mappingExists) {
                    return false;
                }
            }
        } catch (Throwable t) {
            System.out.println("Reflection Exception in EntityTunnelBore: " + t);
        }

        return true;
    }

    protected float getLayerHardness(int i, int j, int k, EnumTrackMeta dir)
    {
        float hardness = 0;
        int ii = i;
        int kk = k;
        for(int jj = j; jj < j + 3; jj++) {
            hardness += getBlockHardness(ii, jj, kk, dir);
        }

        if(dir == EnumTrackMeta.NORTH_SOUTH) {
            ii--;
        } else {
            kk--;
        }
        for(int jj = j; jj < j + 3; jj++) {
            hardness += getBlockHardness(ii, jj, kk, dir);
        }

        ii = i;
        kk = k;
        if(dir == EnumTrackMeta.NORTH_SOUTH) {
            ii++;
        } else {
            kk++;
        }
        for(int jj = j; jj < j + 3; jj++) {
            hardness += getBlockHardness(ii, jj, kk, dir);
        }

        hardness *= HARDNESS_MULTIPLER;

        if(items[0] != null && items[0].getItem() instanceof IBoreHead) {
            IBoreHead head = (IBoreHead)items[0].getItem();
            float dig = 2f - head.getDigModifier();
            hardness = hardness * dig;
        }

        return hardness;
    }

    protected float getBlockHardness(int i, int j, int k, EnumTrackMeta dir)
    {
        int id = world.getTypeId(i, j, k);
        Block block = Block.byId[id];
        if(id == 0) {
            return 0;
        } else if(BlockMinecartTrack.g(world, i, j, k)) {
            int trackMeta = ((BlockMinecartTrack)block).getBasicRailMetadata(world, this, i, j, k);
            if(dir.isEqual(trackMeta)) {
                return 0;
            }
        } else if(id == Block.TORCH.id) {
            return 0;
        }
        int meta = world.getData(i, j, k);
        if(!canMineBlock(i, j, k, id, meta)) {
            return 0.1f;
        }
        float hardness = block.getHardness(meta);
        if(hardness <= 0) {
            hardness = 0.1f;
        }
        return hardness;
    }

    @Override
    public AxisAlignedBB b_(Entity other)
    {
        if(other instanceof EntityLiving) {
            return other.boundingBox;
        }
        return null;
    }

    @Override
    public AxisAlignedBB h()
    {
        return boundingBox;
    }

    @Override
    public String getName()
    {
        return "Tunnel Bore";
    }

    public float getBoreRotationAngle()
    {
        return (float)Math.toRadians(boreRotationAngle);
    }

    @Override
    protected void b(NBTTagCompound data)
    {
//        fuel = getFuel();
        super.b(data);
        data.setString("owner", owner);
        data.setByte("facing", (byte)getFacing());
        data.setInt("delay", getDelay());
        data.setBoolean("active", isActive());
        data.setInt("burnTime", getBurnTime());
    }

    @Override
    protected void a(NBTTagCompound data)
    {
        super.a(data);
        setFacing(data.getByte("facing"));
        setDelay(data.getInt("delay"));
        setActive(data.getBoolean("active"));
        setBurnTime(data.getInt("burnTime"));
        this.owner = data.getString("owner");
//        setFuel(fuel);

        update = GeneralTools.getRand().nextInt();
    }

    protected void setDelay(int i)
    {
        delay = i;
//        dataWatcher.updateObject(WATCHER_ID_DELAY, Integer.valueOf(i));
    }

    protected int getDelay()
    {
        return delay;
//        return dataWatcher.getWatchableObjectInt(WATCHER_ID_DELAY);
    }

    protected boolean isActive()
    {
        return active;
//        return dataWatcher.getWatchableObjectByte(WATCHER_ID_ACTIVE) != 0;
    }

    protected void setActive(boolean active)
    {
        this.active = active;
//        dataWatcher.updateObject(WATCHER_ID_ACTIVE, Byte.valueOf((byte)(active ? 1 : 0)));
    }

    protected boolean isMoving()
    {
        return datawatcher.getByte(WATCHER_ID_MOVING) != 0;
    }

    protected void setMoving(boolean move)
    {
        datawatcher.watch(WATCHER_ID_MOVING, Byte.valueOf((byte)(move ? 1 : 0)));
    }

    protected int getBurnTime()
    {
        return datawatcher.getInt(WATCHER_ID_BURN_TIME);
    }

    protected void setBurnTime(int burnTime)
    {
        datawatcher.watch(WATCHER_ID_BURN_TIME, Integer.valueOf(burnTime));
    }

    public int getFuel()
    {
        return e;
//        return dataWatcher.getWatchableObjectInt(WATCHER_ID_FUEL);
    }

    public boolean outOfFuel()
    {
        return getFuel() <= FUEL_CONSUMPTION;
    }

    public boolean hasFuel()
    {
        return getFuel() > 0;
    }

    protected void stockFuel()
    {
        EntityMinecart link = LinkageManager.getInstance().getLinkedCartA(this);
        if(link instanceof IItemTransfer) {
            IItemTransfer tranfer = (IItemTransfer)link;

            for(int slot = 0; slot < invFuel.getSize(); slot++) {
                ItemStack stack = invFuel.getItem(slot);
                if(stack != null && !InventoryTools.isItemType(stack, EnumItemType.FUEL)) {
                    stack = tranfer.offerItem(this, stack);
                    invFuel.setItem(slot, stack);
                    return;
                }
                if(stack == null) {
                    stack = tranfer.requestItem(this, EnumItemType.FUEL);
                    if(stack != null) {
                        InventoryTools.moveItemStack(stack, invFuel);
                        return;
                    }
                }
            }
        }
    }

    protected void addFuel()
    {
        int burn = 0;
        for(int slot = 0; slot < invFuel.getSize(); slot++) {
            ItemStack stack = invFuel.getItem(slot);
            if(stack != null) {
                burn = GeneralTools.getItemBurnTime(stack);
                if(burn > 0) {
                    if(stack.getItem().k()) {
                        invFuel.setItem(slot, new ItemStack(stack.getItem().j()));
                    } else {
                        invFuel.splitStack(slot, 1);
                    }
                    break;
                }
            }
        }
        if(burn > 0) {
            setBurnTime(burn + getFuel());
            setFuel(getFuel() + burn);
        }
    }

    public int getBurnProgressScaled(int i)
    {
        int burnTime = getBurnTime();
        if(burnTime == 0) {
            return 0;
        }

        return getFuel() * i / burnTime;
    }

    protected void setFuel(int i)
    {
        e = i;
//        dataWatcher.updateObject(WATCHER_ID_FUEL, Integer.valueOf(i));
    }

    protected void spendFuel()
    {
        setFuel(getFuel() - FUEL_CONSUMPTION);
    }

    protected void updateBoreHead()
    {
        int id = -1;
        if(items[0] != null) {
            id = items[0].id;
        }
        datawatcher.watch(WATCHER_ID_BORE_HEAD, Integer.valueOf(id));
    }

    protected void forceUpdateBoreHead()
    {
        datawatcher.watch(WATCHER_ID_BORE_HEAD, Integer.valueOf(-1));
        updateBoreHead();
    }

    public IBoreHead getBoreHead()
    {
        int id = datawatcher.getInt(WATCHER_ID_BORE_HEAD);
        IBoreHead head = null;
        if(id > 0 && id < Item.byId.length) {
            if(Item.byId[id] instanceof IBoreHead) {
                head = (IBoreHead)Item.byId[id];
            }
        }
        return head;
    }

    @Override
    protected void applyDragAndPushForces()
    {
        motX *= getDrag();
        motY *= 0.0D;
        motZ *= getDrag();
    }

    @Override
    protected void updatePushForces()
    {
    }

    @Override
    public List<ItemStack> getItemsDropped()
    {
        List<ItemStack> items = new ArrayList<ItemStack>();
        items.add(getCartItem());
        return items;
    }

    @Override
    public boolean isPoweredCart()
    {
        return true;
    }

    @Override
    public boolean b(EntityHuman player)
    {
//        boolean canInteract = true;
//        for(IMinecartHooks hook : MCLHooks.getMinecartHooks()) {
//            canInteract = canInteract && hook.canInteract(this, entityplayer);
//        }
//        if(!canInteract) {
//            return true;
//        }

        if(player.U() != null && player.U().getItem() instanceof ICrowbar) {
            return false;
        }

        if(Railcraft.gameIsHost()) {
            GuiHandler.openGui(EnumGui.CART_BORE, player, world, this);
            playerActivated = true;
        }
        return true;
    }

    @Override
    public void update()
    {
        if(!isActive()) {
            setDelay(5);
        }
    }

    protected final byte getFacing()
    {
        return datawatcher.getByte(WATCHER_ID_FACING);
    }

    protected final void setFacing(byte facing)
    {
        datawatcher.watch(WATCHER_ID_FACING, Byte.valueOf(facing));

        setYaw();
    }

    @Override
    public void writeSpawnData(DataOutputStream data) throws IOException
    {
        data.writeByte(getFacing());
    }

    @Override
    public void readSpawnData(DataInputStream data) throws IOException
    {
        setFacing(data.readByte());

        setYaw();
    }

    @Override
    public boolean isLinkable()
    {
        return true;
    }

    @Override
    public boolean canLinkWithCart(EntityMinecart cart)
    {
        double x = getXAhead(locX, -LENGTH / 2);
        double z = getZAhead(locZ, -LENGTH / 2);

        if(cart.f(x, locY, z) < LinkageManager.LINKAGE_DISTANCE * 2) {
            return true;
        }
        return false;
    }

    @Override
    public boolean hasTwoLinks()
    {
        return false;
    }

    @Override
    public float getLinkageDistance(EntityMinecart cart)
    {
        return 4f;
    }

    @Override
    public float getOptimalDistance(EntityMinecart cart)
    {
        return 3.1f;
    }

    @Override
    public void onLinkCreated(EntityMinecart cart)
    {
    }

    @Override
    public void onLinkBroken(EntityMinecart cart)
    {
    }

    @Override
    public boolean canBeAdjusted(EntityMinecart cart)
    {
        return false;
    }

    @Override
    public boolean shouldDoRailFunctions()
    {
        return false;
    }

    public IInventory getInventoryFuel()
    {
        return invFuel;
    }

    public IInventory getInventoryGravel()
    {
        return invBallast;
    }

    public IInventory getInventoryRails()
    {
        return invRails;
    }
}
