package railcraft.common.carts;

import net.minecraft.server.Entity;
import net.minecraft.server.EntityMinecart;
import net.minecraft.server.EntityHuman;
import forge.IMinecartHandler;
import railcraft.common.api.CartTools;
import railcraft.common.api.ICrowbar;
import railcraft.common.api.ILinkableCart;
import railcraft.common.api.Vec2D;

public class LinkageHandler implements IMinecartHandler
{

    public static final float MAX_DISTANCE = 16f;
    private static final float STIFFNESS = 0.65f;
//    private static final float TRANSFER = 0.15f;
    private static final float DAMPING = 0.4f;
    private static final float FORCE_LIMITER = 6f;
    private static LinkageHandler instance;

    private LinkageHandler()
    {
    }

    public static LinkageHandler getInstance()
    {
        if(instance == null) {
            instance = new LinkageHandler();
        }
        return instance;
    }

    /**
     * Returns the optimal distance between two linked carts
     * that the LinkageHandler will attempt to maintain at all times.
     *
     * @param cart1
     * @param cart2
     * @return The optimal distance
     */
    private float getOptimalDistance(EntityMinecart cart1, EntityMinecart cart2)
    {
        float dist = 0;
        if(cart1 instanceof ILinkableCart) {
            dist += ((ILinkableCart)cart1).getOptimalDistance(cart2);
        } else {
            dist += LinkageManager.OPTIMAL_DISTANCE;
        }
        if(cart2 instanceof ILinkableCart) {
            dist += ((ILinkableCart)cart2).getOptimalDistance(cart1);
        } else {
            dist += LinkageManager.OPTIMAL_DISTANCE;
        }
        return dist;
    }

    private boolean canCartBeAdjustedBy(EntityMinecart cart1, EntityMinecart cart2)
    {
        if(cart1 == cart2) {
            return false;
        }
        if(cart1 instanceof ILinkableCart && !((ILinkableCart)cart1).canBeAdjusted(cart2)) {
            return false;
        }
        if(CartTools.isCartLockedDown(cart1)) {
            return false;
        }
        return true;
    }

    protected void adjustVelocity(EntityMinecart cart1, EntityMinecart cart2)
    {
        if(cart1 == null || cart2 == null) {
            return;
        }
        double dist = cart1.i(cart2);

        if(dist > MAX_DISTANCE) {
            LinkageManager.getInstance().breakLink(cart1, cart2);
            return;
        }

        boolean adj1 = canCartBeAdjustedBy(cart1, cart2);
        boolean adj2 = canCartBeAdjustedBy(cart2, cart1);

        Vec2D cart1Pos = new Vec2D(cart1.locX, cart1.locZ);
        Vec2D cart2Pos = new Vec2D(cart2.locX, cart2.locZ);

        Vec2D unit = Vec2D.subtract(cart2Pos, cart1Pos);
        unit.normalize();

        // Energy transfer

//        double transX = TRANSFER * (cart2.motionX - cart1.motionX);
//        double transZ = TRANSFER * (cart2.motionZ - cart1.motionZ);
//
//        transX = limitForce(transX);
//        transZ = limitForce(transZ);
//
//        if(adj1) {
//            cart1.motionX += transX;
//            cart1.motionZ += transZ;
//        }
//
//        if(adj2) {
//            cart2.motionX -= transX;
//            cart2.motionZ -= transZ;
//        }

        // Spring force

        float optDist = getOptimalDistance(cart1, cart2);
        double stretch = dist - optDist;
//        if(Math.abs(stretch) > 0.5) {
//            stretch *= 2;
//        }

        double springX = STIFFNESS * stretch * unit.getX();
        double springZ = STIFFNESS * stretch * unit.getY();

        springX = limitForce(springX);
        springZ = limitForce(springZ);

        if(adj1) {
            cart1.motX += springX;
            cart1.motZ += springZ;
        }

        if(adj2) {
            cart2.motX -= springX;
            cart2.motZ -= springZ;
        }

        // Damping

        Vec2D cart1Vel = new Vec2D(cart1.motX, cart1.motZ);
        Vec2D cart2Vel = new Vec2D(cart2.motX, cart2.motZ);

        double dot = Vec2D.subtract(cart2Vel, cart1Vel).dotProduct(unit);

        double dampX = DAMPING * dot * unit.getX();
        double dampZ = DAMPING * dot * unit.getY();

        dampX = limitForce(dampX);
        dampZ = limitForce(dampZ);

        if(adj1) {
            cart1.motX += dampX;
            cart1.motZ += dampZ;
        }

        if(adj2) {
            cart2.motX -= dampX;
            cart2.motZ -= dampZ;
        }
    }

    private float getTrainMaxSpeed(EntityMinecart cart)
    {
        LinkageManager lm = LinkageManager.getInstance();
        EntityMinecart linkA = lm.getLinkedCartA(cart);
        EntityMinecart linkB = lm.getLinkedCartB(cart);

        cart.getEntityData().setBoolean("SpeedTest", true);
        float speed = cart.getMaxSpeedRail();
        cart.getEntityData().setBoolean("SpeedTest", false);

        float min1 = speed;
        min1 = Math.min(min1, getTrainMaxSpeedRecursive(linkA, cart));

        float min2 = speed;
        min2 = Math.min(min2, getTrainMaxSpeedRecursive(linkB, cart));


        return Math.min(min1, min2);
    }

    private float getTrainMaxSpeedRecursive(EntityMinecart cart, EntityMinecart prev)
    {
        if(cart == null) {
            return Float.MAX_VALUE;
        }
        LinkageManager lm = LinkageManager.getInstance();
        EntityMinecart linkA = lm.getLinkedCartA(cart);
        EntityMinecart linkB = lm.getLinkedCartB(cart);

        cart.getEntityData().setBoolean("SpeedTest", true);
        float speed = cart.getMaxSpeedRail();
        cart.getEntityData().setBoolean("SpeedTest", false);

        float min1 = speed;
        if(linkA != prev) {
            min1 = Math.min(min1, getTrainMaxSpeedRecursive(linkA, cart));
        }

        float min2 = speed;
        if(linkB != prev) {
            min2 = Math.min(min2, getTrainMaxSpeedRecursive(linkB, cart));
        }

        return Math.min(min1, min2);
    }

    private void setTrainMaxSpeed(EntityMinecart cart, float trainSpeed)
    {
        LinkageManager lm = LinkageManager.getInstance();
        EntityMinecart linkA = lm.getLinkedCartA(cart);
        EntityMinecart linkB = lm.getLinkedCartB(cart);

        setTrainMaxSpeedRecursive(linkA, cart, trainSpeed);

        setTrainMaxSpeedRecursive(linkB, cart, trainSpeed);

        cart.getEntityData().setFloat("TrainSpeed", trainSpeed);
    }

    private void setTrainMaxSpeedRecursive(EntityMinecart cart, EntityMinecart prev, float trainSpeed)
    {
        if(cart == null) {
            return;
        }
        LinkageManager lm = LinkageManager.getInstance();
        EntityMinecart linkA = lm.getLinkedCartA(cart);
        EntityMinecart linkB = lm.getLinkedCartB(cart);

        if(linkA != prev) {
            setTrainMaxSpeedRecursive(linkA, cart, trainSpeed);
        }

        if(linkB != prev) {
            setTrainMaxSpeedRecursive(linkB, cart, trainSpeed);
        }

        cart.getEntityData().setFloat("TrainSpeed", trainSpeed);
    }

    private double limitForce(double force)
    {
        return Math.copySign(Math.min(Math.abs(force), FORCE_LIMITER), force);
    }

    @Override
    public void onMinecartUpdate(EntityMinecart cart, int x, int y, int z)
    {
        LinkageManager lm = LinkageManager.getInstance();
        if(cart.dead) {
            lm.breakLinks(cart);
            lm.removeLinkageId(cart);
            return;
        }

        // Causes a link id cache store
        lm.getLinkageId(cart);

        EntityMinecart link_A = lm.getLinkedCartA(cart);
        if(link_A != null) {
            if(link_A.dead) {
                lm.breakLinkA(cart);
                lm.removeLinkageId(link_A);
            } else {
                adjustVelocity(cart, link_A);
            }
        }

        EntityMinecart link_B = lm.getLinkedCartB(cart);
        if(link_B != null) {
            if(link_B.dead) {
                lm.breakLinkB(cart);
                lm.removeLinkageId(link_B);
            } else {
                adjustVelocity(cart, link_B);
            }
        }

        if(link_A == null && link_B != null || link_A != null && link_B == null) {
            float trainSpeed = getTrainMaxSpeed(cart);
            setTrainMaxSpeed(cart, trainSpeed);
        }
    }

    @Override
    public void onMinecartEntityCollision(EntityMinecart cart, Entity entity)
    {
    }

    @Override
    public boolean onMinecartInteract(EntityMinecart cart, EntityHuman player, boolean canceled)
    {
        if(player.U() != null && player.U().getItem() instanceof ICrowbar) {
            return false;
        }
        return true;
    }
}
