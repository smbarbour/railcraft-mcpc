package railcraft.common.carts;

import net.minecraft.server.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import forge.ISpecialResistance;

public class CartCrash extends Explosion
{

    private static final float BLAST_RADIUS = 1.5f;
    private Random explosionRNG = new Random();
    private World worldObj;

    public CartCrash(World world, Entity source, double i, double j, double k)
    {
        super(world, source, i, j, k, BLAST_RADIUS);
        this.worldObj = world;
        this.a = true;
    }
    
    /**
     * Does the first part of the explosion (destroy blocks)
     */
    public void doExplosionA()
    {
        float var1 = this.size;
        byte var2 = 16;
        int var3;
        int var4;
        int var5;
        double var15;
        double var17;
        double var19;

        for(var3 = 0; var3 < var2; ++var3) {
            for(var4 = 0; var4 < var2; ++var4) {
                for(var5 = 0; var5 < var2; ++var5) {
                    if(var3 == 0 || var3 == var2 - 1 || var4 == 0 || var4 == var2 - 1 || var5 == 0 || var5 == var2 - 1) {
                        double var6 = (double)((float)var3 / ((float)var2 - 1.0F) * 2.0F - 1.0F);
                        double var8 = (double)((float)var4 / ((float)var2 - 1.0F) * 2.0F - 1.0F);
                        double var10 = (double)((float)var5 / ((float)var2 - 1.0F) * 2.0F - 1.0F);
                        double var12 = Math.sqrt(var6 * var6 + var8 * var8 + var10 * var10);
                        var6 /= var12;
                        var8 /= var12;
                        var10 /= var12;
                        float var14 = this.size * (0.7F + this.worldObj.random.nextFloat() * 0.6F);
                        var15 = this.posX;
                        var17 = this.posY;
                        var19 = this.posZ;

                        for(float var21 = 0.3F; var14 > 0.0F; var14 -= var21 * 0.75F) {
                            int var22 = MathHelper.floor(var15);
                            int var23 = MathHelper.floor(var17);
                            int var24 = MathHelper.floor(var19);
                            int var25 = this.worldObj.getTypeId(var22, var23, var24);

                            if(var25 > 0) {
                                if(Block.byId[var25] instanceof ISpecialResistance) {
                                    ISpecialResistance isr = (ISpecialResistance)Block.byId[var25];
                                    var14 -= (isr.getSpecialExplosionResistance(worldObj, var22, var23, var24, posX, posY, posZ, source) + 0.3F) * var21;
                                } else {
                                    var14 -= (Block.byId[var25].a(this.source) + 0.3F) * var21;
                                }
                            }

                            if(var14 > 0.0F) {
                                this.blocks.add(new ChunkPosition(var22, var23, var24));
                            }

                            var15 += var6 * (double)var21;
                            var17 += var8 * (double)var21;
                            var19 += var10 * (double)var21;
                        }
                    }
                }
            }
        }

        this.size *= 2.0F;
        var3 = MathHelper.floor(this.posX - (double)this.size - 1.0D);
        var4 = MathHelper.floor(this.posX + (double)this.size + 1.0D);
        var5 = MathHelper.floor(this.posY - (double)this.size - 1.0D);
        int var29 = MathHelper.floor(this.posY + (double)this.size + 1.0D);
        int var7 = MathHelper.floor(this.posZ - (double)this.size - 1.0D);
        int var30 = MathHelper.floor(this.posZ + (double)this.size + 1.0D);
        List var9 = this.worldObj.getEntities(this.source, AxisAlignedBB.b((double)var3, (double)var5, (double)var7, (double)var4, (double)var29, (double)var30));
        Vec3D var31 = Vec3D.create(this.posX, this.posY, this.posZ);

        for(int var11 = 0; var11 < var9.size(); ++var11) {
            Entity var32 = (Entity)var9.get(var11);
            double var13 = var32.f(this.posX, this.posY, this.posZ) / (double)this.size;

            if(var13 <= 1.0D) {
                var15 = var32.locX - this.posX;
                var17 = var32.locY - this.posY;
                var19 = var32.locZ - this.posZ;
                double var35 = (double)MathHelper.sqrt(var15 * var15 + var17 * var17 + var19 * var19);
                var15 /= var35;
                var17 /= var35;
                var19 /= var35;
                double var34 = (double)this.worldObj.a(var31, var32.boundingBox);
                double var36 = (1.0D - var13) * var34;
                var32.damageEntity(DamageSource.EXPLOSION, (int)((var36 * var36 + var36) / 2.0D * 8.0D * (double)this.size + 1.0D));
                var32.motX += var15 * var36;
                var32.motY += var17 * var36;
                var32.motZ += var19 * var36;
            }
        }

        this.size = var1;
        ArrayList var33 = new ArrayList();
        var33.addAll(this.blocks);
    }

    /**
     * Does the second part of the explosion (sound, particles, drop spawn)
     */
    public void doExplosionB(boolean par1)
    {
        this.worldObj.makeSound(this.posX, this.posY, this.posZ, "random.explode", 0.5F, (1.0F + (this.worldObj.random.nextFloat() - this.worldObj.random.nextFloat()) * 0.2F) * 0.7F);
        this.worldObj.a("largeexplode", this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D);
        ArrayList var2 = new ArrayList();
        var2.addAll(this.blocks);
        int var3;
        ChunkPosition var4;
        int var5;
        int var6;
        int var7;
        int var8;

        for(var3 = var2.size() - 1; var3 >= 0; --var3) {
            var4 = (ChunkPosition)var2.get(var3);
            var5 = var4.x;
            var6 = var4.y;
            var7 = var4.z;
            var8 = this.worldObj.getTypeId(var5, var6, var7);

            if(par1) {
                double var9 = (double)((float)var5 + this.worldObj.random.nextFloat());
                double var11 = (double)((float)var6 + this.worldObj.random.nextFloat());
                double var13 = (double)((float)var7 + this.worldObj.random.nextFloat());
                double var15 = var9 - this.posX;
                double var17 = var11 - this.posY;
                double var19 = var13 - this.posZ;
                double var21 = (double)MathHelper.sqrt(var15 * var15 + var17 * var17 + var19 * var19);
                var15 /= var21;
                var17 /= var21;
                var19 /= var21;
                double var23 = 0.5D / (var21 / (double)this.size + 0.1D);
                var23 *= (double)(this.worldObj.random.nextFloat() * this.worldObj.random.nextFloat() + 0.3F);
                var15 *= var23;
                var17 *= var23;
                var19 *= var23;
                this.worldObj.a("explode", (var9 + this.posX * 1.0D) / 2.0D, (var11 + this.posY * 1.0D) / 2.0D, (var13 + this.posZ * 1.0D) / 2.0D, var15, var17, var19);
                this.worldObj.a("smoke", var9, var11, var13, var15, var17, var19);
            }

            if(var8 > 0) {
                Block.byId[var8].dropNaturally(this.worldObj, var5, var6, var7, this.worldObj.getData(var5, var6, var7), 0.3F, 0);
                this.worldObj.setTypeId(var5, var6, var7, 0);
                Block.byId[var8].wasExploded(this.worldObj, var5, var6, var7);
            }
        }

        if(this.a) {
            for(var3 = var2.size() - 1; var3 >= 0; --var3) {
                var4 = (ChunkPosition)var2.get(var3);
                var5 = var4.x;
                var6 = var4.y;
                var7 = var4.z;
                var8 = this.worldObj.getTypeId(var5, var6, var7);
                int var25 = this.worldObj.getTypeId(var5, var6 - 1, var7);

                if(var8 == 0 && Block.n[var25] && this.explosionRNG.nextInt(3) == 0) {
                    this.worldObj.setTypeId(var5, var6, var7, Block.FIRE.id);
                }
            }
        }
    }
}
