package railcraft.common.carts;

import net.minecraft.server.Container;
import net.minecraft.server.CraftingManager;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IInventory;
import net.minecraft.server.InventoryCraftResult;
import net.minecraft.server.InventoryCrafting;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;
import net.minecraft.server.SlotResult;

public class ContainerWorkCart extends Container
{

    /** The crafting matrix inventory (3x3). */
    public InventoryCrafting craftMatrix = new InventoryCrafting(this, 3, 3);
    public IInventory craftResult = new InventoryCraftResult();
    private EntityCartWork cart;

    public ContainerWorkCart(PlayerInventory inv, EntityCartWork cart)
    {
    	this.setPlayer(inv.player);
        this.cart = cart;
        this.a(new SlotResult(inv.player, this.craftMatrix, this.craftResult, 0, 124, 35));
        int var6;
        int var7;

        for(var6 = 0; var6 < 3; ++var6) {
            for(var7 = 0; var7 < 3; ++var7) {
                this.a(new Slot(this.craftMatrix, var7 + var6 * 3, 30 + var7 * 18, 17 + var6 * 18));
            }
        }

        for(var6 = 0; var6 < 3; ++var6) {
            for(var7 = 0; var7 < 9; ++var7) {
                this.a(new Slot(inv, var7 + var6 * 9 + 9, 8 + var7 * 18, 84 + var6 * 18));
            }
        }

        for(var6 = 0; var6 < 9; ++var6) {
            this.a(new Slot(inv, var6, 8 + var6 * 18, 142));
        }

        this.a(this.craftMatrix);
    }

    /**
     * Callback for when the crafting matrix is changed.
     */
    @Override
    public void a(IInventory par1IInventory)
    {
        this.craftResult.setItem(0, CraftingManager.getInstance().craft(this.craftMatrix));
    }

    /**
     * Callback for when the crafting gui is closed.
     */
    @Override
    public void a(EntityHuman par1EntityPlayer)
    {
        super.a(par1EntityPlayer);

        if(!cart.world.isStatic) {
            for(int var2 = 0; var2 < 9; ++var2) {
                ItemStack var3 = this.craftMatrix.splitWithoutUpdate(var2);

                if(var3 != null) {
                    par1EntityPlayer.drop(var3);
                }
            }
        }
    }

    @Override
    public boolean b(EntityHuman player)
    {
        return player.e((double)cart.locX + 0.5D, (double)cart.locY + 0.5D, (double)cart.locZ + 0.5D) <= 64.0D;
    }

    /**
     * Called to transfer a stack from one inventory to the other eg. when shift clicking.
     */
    @Override
    public ItemStack a(int par1)
    {
        ItemStack var2 = null;
        Slot var3 = (Slot)this.e.get(par1);

        if(var3 != null && var3.c()) {
            ItemStack var4 = var3.getItem();
            var2 = var4.cloneItemStack();

            if(par1 == 0) {
                if(!this.a(var4, 10, 46, true)) {
                    return null;
                }

//                var3.func_48433_a(var4, var2);
            } else if(par1 >= 10 && par1 < 37) {
                if(!this.a(var4, 37, 46, false)) {
                    return null;
                }
            } else if(par1 >= 37 && par1 < 46) {
                if(!this.a(var4, 10, 37, false)) {
                    return null;
                }
            } else if(!this.a(var4, 10, 46, false)) {
                return null;
            }

            if(var4.count == 0) {
                var3.set((ItemStack)null);
            } else {
                var3.d();
            }

            if(var4.count == var2.count) {
                return null;
            }

            var3.c(var4);
        }

        return var2;
    }

    public IInventory getInventory() {
    	return cart;
    }
}
