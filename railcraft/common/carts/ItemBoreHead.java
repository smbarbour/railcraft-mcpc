package railcraft.common.carts;

import railcraft.common.api.IBoreHead;
import net.minecraft.server.Item;
import forge.ITextureProvider;

public abstract class ItemBoreHead extends Item implements ITextureProvider, IBoreHead
{

    protected ItemBoreHead(int i)
    {
        super(i);
        maxStackSize = 1;
    }

    @Override
    public String getTextureFile()
    {
        return "/railcraft/textures/railcraft.png";
    }
}
