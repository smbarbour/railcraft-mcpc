package railcraft.common.carts;

import railcraft.common.api.CartBase;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.Block;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.World;
import forge.ForgeHooks;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.EnumGui;
import railcraft.common.RailcraftConfig;
import railcraft.common.api.ICartRenderInterface;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class EntityCartWork extends CartBase implements ICartRenderInterface
{

    private static final double DRAG_FACTOR = 0.99199997901916504D;

    public EntityCartWork(World world)
    {
        super(world);
    }

    public EntityCartWork(World world, String player)
    {
        super(world, player);
    }

    public EntityCartWork(World world, double d, double d1, double d2, String player)
    {
        this(world, player);
        setPosition(d, d1 + (double)height, d2);
        motX = 0.0D;
        motY = 0.0D;
        motZ = 0.0D;
        lastX = d;
        lastY = d1;
        lastZ = d2;
    }

    @Override
    public List<ItemStack> getItemsDropped()
    {
        List<ItemStack> items = new ArrayList<ItemStack>();
        if(RailcraftConfig.doCartsBreakOnDrop()) {
            items.add(new ItemStack(Item.MINECART));
            items.add(new ItemStack(Block.WORKBENCH));
        } else {
            items.add(getCartItem());
        }
        return items;
    }

    @Override
    protected double getDrag()
    {
        return DRAG_FACTOR;
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        if(!ForgeHooks.onMinecartInteract(this, entityplayer)) {
            return true;
        }
        if(Railcraft.gameIsHost()) {
            //GuiHandler.openGui(EnumGui.CART_WORK, entityplayer, world, this);
        	entityplayer.getBukkitEntity().openWorkbench(this.getBukkitEntity().getLocation(), true);
        }
        return true;
    }

    @Override
    public boolean isStorageCart()
    {
        return false;
    }

    @Override
    public boolean canBeRidden()
    {
        return false;
    }

    @Override
    public int getSize()
    {
        return 0;
    }

    @Override
    public String getName()
    {
        return "Work Cart";
    }

    @Override
    public Block getBlock()
    {
        return Block.WORKBENCH;
    }

    @Override
    public int getBlockMetadata()
    {
        return 0;
    }
}
