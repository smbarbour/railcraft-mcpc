package railcraft.common.carts;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.*;
import buildcraft.api.ISpecialInventory;
import buildcraft.api.Orientations;
import forge.ForgeHooks;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.EnumGui;
import railcraft.common.RailcraftLanguage;
import railcraft.common.api.BukkitHandlers;
import railcraft.common.api.CartBase;
import railcraft.common.api.IItemTransfer;
import railcraft.common.api.InventoryTools;
import railcraft.common.api.RailTools;
import railcraft.common.api.tracks.ITrackItem;

public class EntityCartTrackRelayer extends CartBase implements ISpecialInventory
{

    private static final double DRAG_FACTOR = 0.9;
    private static final float MAX_SPEED = 0.1f;
    private static final int BLINK_DURATION = 3;
    private static final int DATA_ID_BLINK = 20;
    private static final int SLOT_A_EXIST = 0;
    private static final int SLOT_A_REPLACE = 1;
    private static final int SLOT_A_STOCK = 2;
//    private static final int SLOT_TRACK_B_EXIST = 4;
//    private static final int SLOT_TRACK_B_REPLACE = 5;
//    private static final int SLOT_TRACK_B_STOCK = 6;

    public EntityCartTrackRelayer(World world) {
    	super(world);
    }

    public EntityCartTrackRelayer(World world, String player)
    {
        super(world, player);
    }

    public EntityCartTrackRelayer(World world, double d, double d1, double d2, String player)
    {
        this(world, player);
        setPosition(d, d1 + (double)height, d2);
        motX = 0.0D;
        motY = 0.0D;
        motZ = 0.0D;
        lastX = d;
        lastY = d1;
        lastZ = d2;
    }

    @Override
    protected void b()
    {
        super.b();
        datawatcher.a(DATA_ID_BLINK, new Byte((byte)0));
    }

    private void blink()
    {
        datawatcher.watch(DATA_ID_BLINK, Byte.valueOf((byte)BLINK_DURATION));
    }

    private void setBlink(byte blink)
    {
        datawatcher.watch(DATA_ID_BLINK, Byte.valueOf((byte)blink));
    }

    private byte getBlink()
    {
        return datawatcher.getByte(DATA_ID_BLINK);
    }

    public boolean isBlinking()
    {
        return datawatcher.getByte(DATA_ID_BLINK) > 0;
    }

    @Override
    public List<ItemStack> getItemsDropped()
    {
        List<ItemStack> items = new ArrayList<ItemStack>();
        items.add(getCartItem());
        return items;
    }

    @Override
    protected double getDrag()
    {
        return DRAG_FACTOR;
    }

    @Override
    public float getMaxSpeedRail()
    {
        NBTTagCompound data = getEntityData();
        if(data.getBoolean("SpeedTest")) {
            return MAX_SPEED;
        }
        float speed = data.getFloat("TrainSpeed");
        return speed > 0 ? speed : MAX_SPEED;
    }

    @Override
    public void F_()
    {
        super.F_();
        if(Railcraft.gameIsNotHost()) {
            return;
        }

        if(isBlinking()) {
            setBlink((byte)(getBlink() - 1));
        }

        stock(SLOT_A_REPLACE, SLOT_A_STOCK);
//        stockTrack(SLOT_TRACK_B_REPLACE, SLOT_TRACK_B_STOCK);

        replace(SLOT_A_EXIST, SLOT_A_STOCK);
//        replaceTrack(SLOT_TRACK_B_EXIST, SLOT_TRACK_B_STOCK);
    }

    private void offerOrDropItem(ItemStack stack)
    {
        EntityMinecart link_A = LinkageManager.getInstance().getLinkedCartA(this);
        EntityMinecart link_B = LinkageManager.getInstance().getLinkedCartB(this);

        if(stack != null && stack.count > 0 && link_A instanceof IItemTransfer) {
            stack = ((IItemTransfer)link_A).offerItem(this, stack);
        }
        if(stack != null && stack.count > 0 && link_B instanceof IItemTransfer) {
            stack = ((IItemTransfer)link_B).offerItem(this, stack);
        }

        if(stack != null && stack.count > 0) {
            a(stack, 1);
        }
    }

    private void stock(int slotReplace, int slotStock)
    {
        ItemStack trackReplace = getItem(slotReplace);

        ItemStack trackStock = getItem(slotStock);

        if(trackStock != null && !InventoryTools.isItemEqual(trackReplace, trackStock)) {
            offerOrDropItem(trackStock);
            setItem(slotStock, null);
        }

        if(trackReplace == null) {
            return;
        }

        trackStock = getItem(slotStock);

        EntityMinecart link_A = LinkageManager.getInstance().getLinkedCartA(this);
        EntityMinecart link_B = LinkageManager.getInstance().getLinkedCartB(this);

        if(trackStock == null || trackStock.count < trackStock.getMaxStackSize()) {
            ItemStack stack = null;
            if(link_A instanceof IItemTransfer) {
                stack = ((IItemTransfer)link_A).requestItem(this, trackReplace);
                if(stack != null) {
                    if(trackStock == null) {
                        setItem(slotStock, stack);
                    } else {
                        trackStock.count++;
                    }
                }
            }
            if(stack == null && link_B instanceof IItemTransfer) {
                stack = ((IItemTransfer)link_B).requestItem(this, trackReplace);
                if(stack != null) {
                    if(trackStock == null) {
                        setItem(slotStock, stack);
                    } else {
                        trackStock.count++;
                    }
                }
            }
        }
    }

    private void replace(int slotExist, int slotStock)
    {
        int i = MathHelper.floor(this.locX);
        int j = MathHelper.floor(this.locY);
        int k = MathHelper.floor(this.locZ);

        if(BlockMinecartTrack.g(this.world, i, j - 1, k)) {
            --j;
        }

        int id = this.world.getTypeId(i, j, k);

        if(BlockMinecartTrack.d(id)) {
            ItemStack trackExist = getItem(slotExist);
            ItemStack trackStock = getItem(slotStock);
            if(trackExist != null && trackStock != null) {
                if(trackExist.getItem() instanceof ITrackItem) {
                	//System.out.println("replace (ITrackItem)");
                    ITrackItem trackItem = (ITrackItem)trackExist.getItem();
                    if(trackItem.getPlacedBlockId() == id) {
                        TileEntity tile = world.getTileEntity(i, j, k);
                        if(trackItem.isPlacedTileEntity(trackExist, tile)) {
                            int meta = removeOldTrack(i, j, k, id);
                            if (meta != -1) {
                            	placeNewTrack(i, j, k, slotStock, meta);	
                            }
                        }
                    }
                } else if(trackExist.getItem() instanceof ItemBlock) {
                	//System.out.println("replace (ItemBlock)");
                    if(trackExist.id == id) {
                        int meta = removeOldTrack(i, j, k, id);
                        if (meta != -1) {
                        	placeNewTrack(i, j, k, slotStock, meta);
                        }
                    }
                }
            }
        }
    }

    private int removeOldTrack(int i, int j, int k, int id)
    {
        List<ItemStack> drops = Block.byId[id].getBlockDropped(world, i, j, k, 0, 0);

        int meta = world.getData(i, j, k);
        if(((BlockMinecartTrack)Block.byId[id]).hasPowerBit(world, i, j, k)) {
            meta = meta & 7;
        }
        if (BukkitHandlers.removeBlockWithEvent(this.getBukkitEntity().getServer(), this.world, i, j, k, this.owner)) {
            for(ItemStack stack : drops) {
                offerOrDropItem(stack);
            }
            return meta;
        } else {
        	return -1;
        }
        //world.setTypeId(i, j, k, 0);
    }

    private void placeNewTrack(int i, int j, int k, int slotStock, int meta)
    {
        ItemStack trackStock = getItem(slotStock);
        if(trackStock != null) {
            if(RailTools.placeRailAt(trackStock, world, i, j, k, owner)) {
                world.setData(i, j, k, meta);
                int id = world.getTypeId(i, j, k);
                Block.byId[id].doPhysics(world, i, j, k, id);
                splitStack(slotStock, 1);
                blink();
            }
        }
    }

    @Override
    public boolean b(EntityHuman player)
    {
        if(!ForgeHooks.onMinecartInteract(this, player)) {
            return true;
        }
        if(Railcraft.gameIsHost()) {
            GuiHandler.openGui(EnumGui.CART_TRACK_RELAYER, player, world, this);
        }
        return true;
    }

    @Override
    public boolean isStorageCart()
    {
        return false;
    }

    @Override
    public boolean canBeRidden()
    {
        return false;
    }

    @Override
    public int getSize()
    {
        return 3;
    }

    @Override
    public String getName()
    {
        return RailcraftLanguage.translate(EnumCart.TRACK_RELAYER.getTag());
    }

    @Override
    public boolean addItem(ItemStack stack, boolean doAdd, Orientations from)
    {
        ItemStack trackReplace_A = getItem(SLOT_A_REPLACE);
//        ItemStack trackReplace_B = getItem(SLOT_TRACK_B_REPLACE);
        ItemStack trackStock_A = getItem(SLOT_A_STOCK);
//        ItemStack trackStock_B = getItem(SLOT_TRACK_B_STOCK);


        if(InventoryTools.isItemEqual(stack, trackReplace_A)) {
            if(trackStock_A == null) {
                if(doAdd) {
                    setItem(SLOT_A_STOCK, stack);
                    stack.count = 0;
                }
                return true;
            } else if(trackStock_A.count + stack.count <= trackStock_A.getMaxStackSize()) {
                if(doAdd) {
                    trackStock_A.count += stack.count;
                    stack.count = 0;
                }
                return true;
            }
        }

//        if(InventoryTools.isItemEqual(stack, trackReplace_B)) {
//            if(trackStock_B == null) {
//                if(doAdd) {
//                    setItem(SLOT_TRACK_B_STOCK, stack);
//                    stack.stackSize = 0;
//                }
//                return true;
//            } else if(trackStock_B.stackSize + stack.stackSize <= trackStock_B.getMaxStackSize()) {
//                if(doAdd) {
//                    trackStock_B.stackSize += stack.stackSize;
//                    stack.stackSize = 0;
//                }
//                return true;
//            }
//        }

        return false;
    }

    @Override
    public ItemStack extractItem(boolean doRemove, Orientations from)
    {
        return null;
    }
}
