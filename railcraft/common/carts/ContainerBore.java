package railcraft.common.carts;

import railcraft.common.util.slots.SlotBore;
import net.minecraft.server.*;
import railcraft.common.util.slots.SlotFuel;
import railcraft.common.util.slots.SlotBallast;
import railcraft.common.util.slots.SlotTrack;

public class ContainerBore extends Container
{

    private EntityTunnelBore bore;

    public ContainerBore(PlayerInventory playerInv, EntityTunnelBore bore)
    {
    	this.setPlayer(playerInv.player);
        this.bore = bore;

        a(new SlotBore(bore, 0, 17, 36));

        for(int i = 0; i < 6; i++) {
            a(new SlotFuel(bore, i + 1, 62 + i * 18, 36));
        }

        for(int i = 0; i < 9; i++) {
            a(new SlotBallast(bore, i + 7, 8 + i * 18, 72));
        }

        for(int i = 0; i < 9; i++) {
            a(new SlotTrack(bore, i + 16, 8 + i * 18, 108));
        }

        for(int i = 0; i < 3; i++) {
            for(int k = 0; k < 9; k++) {
                a(new Slot(playerInv, k + i * 9 + 9, 8 + k * 18, 140 + i * 18));
            }
        }

        for(int i = 0; i < 9; i++) {
            a(new Slot(playerInv, i, 8 + i * 18, 198));
        }
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        return bore.a(entityplayer);
    }

    @Override
    public ItemStack a(int i)
    {
        ItemStack stackCopy = null;
        Slot slot = (Slot)e.get(i);
        if(slot != null && slot.c()) {
            ItemStack stack = slot.getItem();
            stackCopy = stack.cloneItemStack();
            if(i < 25) {
                if(!a(stack, 25, e.size(), true)) {
                    return null;
                }
            } else {
                if(SlotBore.canPlaceItem(stack)) {
                    if(!a(stack, 0, 1, false)) {
                        return null;
                    }
                } else if(SlotFuel.canPlaceItem(stack)) {
                    if(!a(stack, 1, 7, false)) {
                        return null;
                    }
                } else if(SlotBallast.canPlaceItem(stack)) {
                    if(!a(stack, 7, 16, false)) {
                        return null;
                    }
                } else if(SlotTrack.canPlaceItem(stack)) {
                    if(!a(stack, 16, 25, false)) {
                        return null;
                    }
                } else {
                    return null;
                }
            }
            if(stack.count == 0) {
                slot.set(null);
            } else {
                slot.d();
            }
        }
        return stackCopy;
    }
    
    public IInventory getInventory() {
    	return bore;
    }
}
