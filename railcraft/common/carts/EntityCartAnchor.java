package railcraft.common.carts;

import railcraft.common.api.TransferCartBase;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.*;
import railcraft.Railcraft;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftConfig;
import railcraft.common.ChunkManager;
import railcraft.common.api.ICartRenderInterface;
import railcraft.common.cube.EnumCube;

public class EntityCartAnchor extends TransferCartBase implements ICartRenderInterface
{

    private static final double DRAG_FACTOR = 0.99199997901916504D;
//    private int update;

    public EntityCartAnchor(World world) {
    	super(world);
    }

    public EntityCartAnchor(World world, String player)
    {
        super(world, player);
//        update = GeneralTools.getRand().nextInt();
//        System.out.println("creating cart");
    }

    public EntityCartAnchor(World world, double d, double d1, double d2, String player)
    {
        this(world, player);
        setPosition(d, d1 + (double)height, d2);
        lastX = d;
        lastY = d1;
        lastZ = d2;
        e = 0;
        b = 0;
        c = 0;
    }

    @Override
    public void F_()
    {
//        update++;
        super.F_();

        ChunkManager cm = ChunkManager.getInstance();

        if(dead) {
            cm.unregisterAnchor(this);
        } else {
            cm.registerAnchor(this, 1);
        }

        if(Railcraft.gameIsHost()) {
            int x = MathHelper.floor(locX) >> 4;
            int z = MathHelper.floor(locZ) >> 4;
            cm.loadChunksAround(world, x, z, 1);
        }

//        if(update % 20 == 0) {
//            System.out.println("Anchor update: " + entityId);
//        }
    }

    @Override
    public void die()
    {
        ChunkManager.getInstance().unregisterAnchor(this);
        super.die();
    }

    @Override
    public List<ItemStack> getItemsDropped()
    {
        List<ItemStack> items = new ArrayList<ItemStack>();
        if(RailcraftConfig.doCartsBreakOnDrop()) {
            items.add(new ItemStack(Item.MINECART));
            items.add(new ItemStack(getBlock()));
        } else {
            items.add(getCartItem());
        }
        return items;
    }

    @Override
    public double getDrag()
    {
        return DRAG_FACTOR;
    }

    @Override
    public boolean canBeRidden()
    {
        return false;
    }

    @Override
    public int getSize()
    {
        return 0;
    }

    @Override
    public String getName()
    {
        return "Anchorcart";
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        return false;
    }

    @Override
    public Block getBlock()
    {
        return RailcraftBlocks.getBlockCube();
    }

    @Override
    public int getBlockMetadata()
    {
        return EnumCube.WORLD_ANCHOR.getId();
    }
}
