package railcraft.common.carts;

public class ItemBoreHeadIron extends ItemBoreHead
{

    public ItemBoreHeadIron(int i)
    {
        super(i);
        d(152);
        setMaxDurability(1500);
        a("boreHeadIron");
    }

    @Override
    public String getBoreTexture()
    {
        return "/railcraft/textures/tunnel_bore_iron.png";
    }

    @Override
    public int getHarvestLevel()
    {
        return 2;
    }

    @Override
    public float getDigModifier()
    {
        return 1.0f;
    }
}
