package railcraft.common.carts;

import net.minecraft.server.IInventory;
import net.minecraft.server.Container;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;
import railcraft.common.util.slots.SlotCopy;
import railcraft.common.util.slots.SlotBlockSingle;

public class ContainerUndercutter extends Container
{

    private EntityCartUndercutter cart;
    private SlotCopy copySlot;

    public ContainerUndercutter(PlayerInventory inventoryplayer, EntityCartUndercutter cart)
    {
    	this.setPlayer(inventoryplayer.player);
        this.cart = cart;
        a(new SlotBlockSingle(cart, 0, 17, 43));
        a(new SlotBlockSingle(cart, 1, 35, 43));
        a(new SlotBlockSingle(cart, 2, 80, 43));
        copySlot = new SlotCopy(cart, 3, 130, 43, 2);
        a(copySlot);
        for(int i = 0; i < 3; i++) {
            for(int k = 0; k < 9; k++) {
                a(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++) {
            a(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        return cart.a(entityplayer);
    }

    @Override
    public ItemStack a(int i)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)e.get(i);
        if(slot != null && slot.c()) {
            ItemStack itemstack1 = slot.getItem();
            itemstack = itemstack1.cloneItemStack();
            if(i >= 4 && i < 40 && copySlot.isAllowed(itemstack1)) {
                if(!a(itemstack1, 3, 4, false)) {
                    return null;
                }
            } else if(i >= 4 && i < 31) {
                if(!a(itemstack1, 31, 39, false)) {
                    return null;
                }
            } else if(i >= 31 && i < 40) {
                if(!a(itemstack1, 3, 30, false)) {
                    return null;
                }
            } else if(!a(itemstack1, 3, 39, false)) {
                return null;
            }
            if(itemstack1.count == 0) {
                slot.set(null);
            } else {
                slot.d();
            }
            if(itemstack1.count != itemstack.count) {
                slot.c(itemstack1);
            } else {
                return null;
            }
        }
        return itemstack;
    }
    
    public IInventory getInventory() {
    	return cart;
    }
}
