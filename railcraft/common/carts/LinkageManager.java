package railcraft.common.carts;

import railcraft.common.api.ILinkageManager;
import java.util.HashMap;
import java.util.Map;
import net.minecraft.server.EntityMinecart;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import railcraft.Railcraft;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.ILinkableCart;

/**
 * The LinkageManager contains all the functions needed to link and interacted
 * with linked carts.
 *
 * One concept if import is that of the Linkage Id. Every cart is given a unique
 * identifier by the LinkageManager the first time it encounters the cart.
 *
 * This identifier is stored in the entity's NBT data between world loads
 * so that links are persistent rather than transitory.
 *
 * Links are also stored in NBT data as an Integer value that contains the Linkage Id
 * of the cart it is linked to.
 *
 * Generally you can ignore most of this and use the functions that don't require
 * or return Linkage Ids.
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class LinkageManager implements ILinkageManager
{

    private final Map<Integer, EntityMinecart> carts = new HashMap<Integer, EntityMinecart>();
    private static final String LINK_A = "linkA";
    private static final String LINK_B = "linkB";
    private static final String LINK_ID = "linkageId";
    private static LinkageManager instance;
    private World activeWorld;

    private LinkageManager()
    {
    }

    /**
     * Return an instance of the LinkageManager
     *
     * @return LinkageManager
     */
    public static LinkageManager getInstance()
    {
        if(instance == null) {
            instance = new LinkageManager();
        }
        return instance;
    }

    /**
     * Removes a id:cart pairing from the linkage registry.
     *
     * You should not need to call this function ever,
     * it is needed only by the LinkageHandler (internal Railcraft code)
     * in order to clean up dead links left by dead carts.
     *
     * @param cart The cart to remove
     */
    public void removeLinkageId(EntityMinecart cart)
    {
        carts.remove(getLinkageId(cart));
    }

    /**
     * Returns the linkage id of the cart and adds the cart the linkage registry.
     *
     * @param cart The EntityMinecart
     * @return The linkage id
     */
    public int getLinkageId(EntityMinecart cart)
    {
        if(cart == null) {
            return 0;
        }
        NBTTagCompound data = cart.getEntityData();
        int id = data.getInt(LINK_ID);
        if(id == 0) {
            id = GeneralTools.getRand().nextInt();
            data.setInt(LINK_ID, id);
//            System.out.println("Creating new Linkage ID");
        }
        if(id != 0) {
            EntityMinecart storedCart = carts.get(id);
            if(storedCart == null) {
                carts.put(id, cart);
            } else if(storedCart != cart) {
                data.setInt(LINK_ID, 0);
                return getLinkageId(cart);
            }
        }
        return id;
    }


    public void updateTick(World world)
    {
        if(Railcraft.gameIsHost() && activeWorld != world) {
            carts.clear();
            activeWorld = world;
        }
    }

    /**
     * Returns a minecart from a linkage id.
     *
     * @param id
     * @return
     */
    public EntityMinecart getCartFromLinkageId(int id)
    {
        if(id == 0) {
            return null;
        }
        return carts.get(id);
    }

    /**
     * Returns the square of the max distance two carts can be and still be linkable.
     *
     * @param cart1
     * @param cart2
     * @return The square of the linkage distance
     */
    private float getLinkageDistanceSq(EntityMinecart cart1, EntityMinecart cart2)
    {
        float dist = 0;
        if(cart1 instanceof ILinkableCart) {
            dist += ((ILinkableCart)cart1).getLinkageDistance(cart2);
        } else {
            dist += LINKAGE_DISTANCE;
        }
        if(cart2 instanceof ILinkableCart) {
            dist += ((ILinkableCart)cart2).getLinkageDistance(cart1);
        } else {
            dist += LINKAGE_DISTANCE;
        }
        return dist * dist;
    }

    /**
     * Returns true if there is nothing preventing the two carts from being linked.
     *
     * @param cart1
     * @param cart2
     * @return True if can be linked
     */
    private boolean canLinkCarts(EntityMinecart cart1, EntityMinecart cart2)
    {
        if(cart1 == null || cart2 == null) {
            return false;
        }

        if(cart1 == cart2) {
            return false;
        }

        if(cart1 instanceof ILinkableCart) {
            ILinkableCart link = (ILinkableCart)cart1;
            if(!link.isLinkable() || !link.canLinkWithCart(cart2)) {
                return false;
            }
        }

        if(cart2 instanceof ILinkableCart) {
            ILinkableCart link = (ILinkableCart)cart2;
            if(!link.isLinkable() || !link.canLinkWithCart(cart1)) {
                return false;
            }
        }

        if(areLinked(cart1, cart2)) {
            return false;
        }

        if(cart1.j(cart2) > getLinkageDistanceSq(cart1, cart2)) {
            return false;
        }

        if(!hasFreeLink(cart1) || !hasFreeLink(cart2)) {
            return false;
        }

        return true;
    }

    /**
     * Creates a link between two carts,
     * but only if there is nothing preventing such a link.
     *
     * @param cart1
     * @param cart2
     * @return True if the link succeeded.
     */
    @Override
    public boolean createLink(EntityMinecart cart1, EntityMinecart cart2)
    {
        if(canLinkCarts(cart1, cart2)) {
            setLink(cart1, cart2);
            setLink(cart2, cart1);

            if(cart1 instanceof ILinkableCart) {
                ((ILinkableCart)cart1).onLinkCreated(cart2);
            }
            if(cart2 instanceof ILinkableCart) {
                ((ILinkableCart)cart2).onLinkCreated(cart1);
            }

            return true;
        }
        return false;
    }

    private boolean hasFreeLink(EntityMinecart cart)
    {
        return getLinkedCartA(cart) == null || (hasLinkB(cart) && getLinkedCartB(cart) == null);
    }

    private boolean hasLinkB(EntityMinecart cart)
    {
        if(cart instanceof ILinkableCart) {
            return ((ILinkableCart)cart).hasTwoLinks();
        }
        return true;
    }

    private void setLink(EntityMinecart cart1, EntityMinecart cart2)
    {
        if(getLinkedCartA(cart1) == null) {
            setLinkA(cart1, cart2);
        } else if(hasLinkB(cart1) && getLinkedCartB(cart1) == null) {
            setLinkB(cart1, cart2);
        }
    }

    public int getLinkA(EntityMinecart cart)
    {
        if(cart == null) {
            return 0;
        }
        return cart.getEntityData().getInt(LINK_A);
    }

    private void setLinkA(EntityMinecart cart1, EntityMinecart cart2)
    {
        cart1.getEntityData().setInt(LINK_A, getLinkageId(cart2));
    }

    /**
     * Returns the cart linked to Link A or null if nothing is currently
     * occupying Link A.
     *
     * @param cart The cart for which to get the link
     * @return The linked cart or null
     */
    @Override
    public EntityMinecart getLinkedCartA(EntityMinecart cart)
    {
        return getCartFromLinkageId(getLinkA(cart));
    }

    public int getLinkB(EntityMinecart cart)
    {
        if(cart == null) {
            return 0;
        }
        return cart.getEntityData().getInt(LINK_B);
    }

    private void setLinkB(EntityMinecart cart1, EntityMinecart cart2)
    {
        if(!hasLinkB(cart1)) {
            return;
        }
        cart1.getEntityData().setInt(LINK_B, getLinkageId(cart2));
    }

    /**
     * Returns the cart linked to Link B or null if nothing is currently
     * occupying Link B.
     *
     * @param cart The cart for which to get the link
     * @return The linked cart or null
     */
    @Override
    public EntityMinecart getLinkedCartB(EntityMinecart cart)
    {
        return getCartFromLinkageId(getLinkB(cart));
    }

    /**
     * Returns true if the two carts are linked to each other.
     *
     * @param cart1
     * @param cart2
     * @return True if linked
     */
    @Override
    public boolean areLinked(EntityMinecart cart1, EntityMinecart cart2)
    {
        if(cart1 == null || cart2 == null) {
            return false;
        }
        if(cart1 == cart2) {
            return false;
        }

//        System.out.println("cart2 id = " + getLinkageId(cart2));
//        System.out.println("cart2 A = " + getLinkA(cart2));
//        System.out.println("cart2 B = " + getLinkB(cart2));
//        System.out.println("cart1 id = " + getLinkageId(cart1));
//        System.out.println("cart1 A = " + getLinkA(cart1));
//        System.out.println("cart1 B = " + getLinkB(cart1));

        boolean cart1Linked = false;
        int id = getLinkageId(cart2);
        if(getLinkA(cart1) == id || getLinkB(cart1) == id) {
            cart1Linked = true;
//            System.out.println("cart1 linked");
        }

        boolean cart2Linked = false;
        id = getLinkageId(cart1);
        if(getLinkA(cart2) == id || getLinkB(cart2) == id) {
            cart2Linked = true;
//            System.out.println("cart2 linked");
        }

        return cart1Linked && cart2Linked;
    }

    /**
     * Breaks a link between two carts, if any link exists.
     *
     * @param cart1
     * @param cart2
     */
    @Override
    public void breakLink(EntityMinecart cart1, EntityMinecart cart2)
    {
        int link = getLinkageId(cart2);
        if(getLinkA(cart1) == link) {
            breakLinkA(cart1);
        }

        if(getLinkB(cart1) == link) {
            breakLinkB(cart1);
        }
    }

    /**
     * Breaks all links the passed cart has.
     *
     * @param cart
     */
    @Override
    public void breakLinks(EntityMinecart cart)
    {
        breakLinkA(cart);
        breakLinkB(cart);
    }

    /**
     * Break only link A.
     *
     * @param cart
     */
    @Override
    public void breakLinkA(EntityMinecart cart)
    {
        int link = getLinkA(cart);
        cart.getEntityData().setInt(LINK_A, 0);
        EntityMinecart other = getCartFromLinkageId(link);
        if(other != null) {
            breakLink(other, cart);
        }
        if(cart instanceof ILinkableCart) {
            ((ILinkableCart)cart).onLinkBroken(other);
        }
    }

    /**
     * Break only link B.
     *
     * @param cart
     */
    @Override
    public void breakLinkB(EntityMinecart cart)
    {
        int link = getLinkB(cart);
        cart.getEntityData().setInt(LINK_B, 0);
        EntityMinecart other = getCartFromLinkageId(link);
        if(other != null) {
            breakLink(other, cart);
        }
        if(cart instanceof ILinkableCart) {
            ((ILinkableCart)cart).onLinkBroken(other);
        }
    }

    /**
     * Counts how many carts are in the train (untested).
     *
     * @param cart Any cart in the train
     * @return The number of carts in the train
     */
    @Override
    public int countCartsInTrain(EntityMinecart cart)
    {
        EntityMinecart linkA = getLinkedCartA(cart);
        EntityMinecart linkB = getLinkedCartB(cart);

        int count1 = 0;
        count1 += countLinksRecursive(linkA, cart);

        int count2 = 0;
        count2 += countLinksRecursive(linkB, cart);


        return count1 + count2 + 1;
    }

    private int countLinksRecursive(EntityMinecart cart, EntityMinecart prev)
    {
        if(cart == null) {
            return 0;
        }

        EntityMinecart linkA = getLinkedCartA(cart);
        EntityMinecart linkB = getLinkedCartB(cart);

        int count1 = 0;
        if(linkA != prev) {
            count1 += countLinksRecursive(linkA, cart);
        }

        int count2 = 0;
        if(linkB != prev) {
            count2 += countLinksRecursive(linkB, cart);
        }

        return count1 + count2 + 1;
    }
}
