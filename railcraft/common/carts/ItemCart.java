package railcraft.common.carts;

import railcraft.common.api.CartTools;
import net.minecraft.server.*;
import forge.ITextureProvider;
import railcraft.Railcraft;
import railcraft.common.RailcraftConfig;
import railcraft.common.api.IMinecartItem;

public class ItemCart extends ItemMinecart implements ITextureProvider, IMinecartItem
{

    private EnumCart type;

    public ItemCart(int i, EnumCart cart)
    {
        super(i, 3);
        maxStackSize = RailcraftConfig.getMinecartStackSize();
        this.type = cart;
        d(cart.getIconIndex());
        a(cart.getTag());
        setMaxDurability(0);
        a(true);
    }

    @Override
    public String getTextureFile()
    {
        return type.getSpritesheet();
    }

    @Override
    public boolean interactWith(ItemStack stack, EntityHuman player, World world, int i, int j, int k, int l)
    {
        if(Railcraft.gameIsNotHost()) {
            return false;
        }
        String name;
        if (player == null) {
        	name = "[Railcraft]";
        } else {
        	name = player.name; 
        }
        boolean placed = placeCart(stack, world, i, j, k, name);
        if(placed) {
            stack.count--;
        }
        return placed;
    }

    public EnumCart getCartType()
    {
        return type;
    }

    @Override
    public boolean canBePlacedByNonPlayer(ItemStack cart)
    {
        return true;
    }

    @Override
    public boolean placeCart(ItemStack cart, World world, int i, int j, int k, String player)
    {
        int i1 = world.getTypeId(i, j, k);
        if(BlockMinecartTrack.d(i1)) {
            if(!CartTools.isMinecartAt(world, i, j, k, 0, null, true)) {
                return world.addEntity(type.makeCart(cart, world, (float)i + 0.5F, (float)j + 0.5F, (float)k + 0.5F, player));
            }
        }
        return false;
    }

    public static EnumCart getCartType(ItemStack cart)
    {
        if(cart != null && cart.getItem() instanceof ItemCart) {
            return ((ItemCart)cart.getItem()).getCartType();
        }
        return null;
    }

	@Override
	public boolean placeCart(ItemStack cart, World world, int i, int j, int k) {
		return placeCart(cart, world, i, j, k, "[Railcraft]");
	}
}
