package railcraft.common.carts;

import net.minecraft.server.*;
import forge.ITextureProvider;
import railcraft.Railcraft;
import railcraft.common.api.CartTools;
import railcraft.common.api.IMinecartItem;

public class ItemTunnelBore extends ItemMinecart implements ITextureProvider, IMinecartItem
{

    public ItemTunnelBore(int id)
    {
        super(id, 3);
        maxStackSize = 1;
        a = 3;
        a("tunnelBore");
        d(7);
    }

    @Override
    public String getTextureFile()
    {
        return "/railcraft/textures/railcraft.png";
    }

    @Override
    public boolean interactWith(ItemStack itemstack, EntityHuman entityplayer, World world, int i, int j, int k, int l)
    {
        int id = world.getTypeId(i, j, k);
        if(BlockMinecartTrack.d(id)) {
            if(Railcraft.gameIsHost() && !CartTools.isMinecartAt(world, i, j, k, 0, null, true)) {
                int meta = ((BlockMinecartTrack)Block.byId[id]).getBasicRailMetadata(world, null, i, j, k);
                if(meta == 0 || meta == 1) {
                    int playerYaw = -90 - MathHelper.floor(entityplayer.yaw);
                    for(; playerYaw > 360; playerYaw -= 360);
                    for(; playerYaw < 0; playerYaw += 360);
                    int facing = 1;
                    if(Math.abs(90 - playerYaw) <= 45) {
                        facing = 0;
                    } else if(Math.abs(180 - playerYaw) <= 45) {
                        facing = 3;
                    } else if(Math.abs(270 - playerYaw) <= 45) {
                        facing = 2;
                    }

                    if(meta == 0 && facing == 1) {
                        facing = 0;
                    } else if(meta == 0 && facing == 3) {
                        facing = 2;
                    } else if(meta == 1 && facing == 0) {
                        facing = 1;
                    } else if(meta == 1 && facing == 2) {
                        facing = 3;
                    }

//					System.out.println("PlayerYaw = " + playerYaw + " Yaw = " + yaw + " Meta = " + meta);

                    world.addEntity(new EntityTunnelBore(world, (float)i + 0.5F, (float)j + 0.5F, (float)k + 0.5F, facing, entityplayer));
                }
            }
            itemstack.count--;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean canBePlacedByNonPlayer(ItemStack cart)
    {
        return false;
    }

    @Override
    public boolean placeCart(ItemStack cart, World world, int i, int j, int k)
    {
        return false;
    }

	@Override
	public boolean placeCart(ItemStack cart, World world, int i, int j, int k, String player) {
		return false;
	}
}
