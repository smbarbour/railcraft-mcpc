package railcraft.common.modules;

import net.minecraft.server.Block;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.ModLoader;
import forge.MinecraftForge;
import railcraft.common.EnumColor;
import railcraft.common.modules.RailcraftModuleManager.Module;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftConfig;
import railcraft.common.RailcraftLanguage;
import railcraft.common.RailcraftPartItems;
import railcraft.common.cube.EnumCube;
import railcraft.common.structures.EnumStructure;
import railcraft.common.structures.ItemMetalPost;
import railcraft.common.utility.EnumUtility;
import railcraft.common.util.crafting.RollingMachineCraftingManager;

public class ModuleStructures extends RailcraftModule
{

    @Override
    public void doFirst()
    {
        RailcraftBlocks.registerBlockStructure();

        Block blockStructure = RailcraftBlocks.getBlockStructure();
        if(blockStructure != null) {

            // Define Iron Post
            EnumStructure structure = EnumStructure.METAL_POST;
            int metalPostId = RailcraftConfig.getItemId(structure.getTag());
            if(RailcraftConfig.isSubBlockEnabled(structure.getTag()) && metalPostId > 0) {
                MinecraftForge.setBlockHarvestLevel(blockStructure, structure.getId(), "pickaxe", 2);
                ModLoader.registerTileEntity(structure.getTileClass(), "RCTileStructureIronPost");

                Item item = structure.setItem(new ItemMetalPost(metalPostId));

                ItemStack stack = structure.getItem(16);
                if(RailcraftModuleManager.isModuleLoaded(Module.FACTORY)
                    && RailcraftBlocks.getBlockUtility() != null
                    && RailcraftConfig.isSubBlockEnabled(EnumUtility.ROLLING_MACHINE.getTag())) {
                    RollingMachineCraftingManager.getInstance().addRecipe(stack, new Object[]{
                            "III",
                            " I ",
                            "III",
                            Character.valueOf('I'), Item.IRON_INGOT,});
                    RollingMachineCraftingManager.getInstance().addRecipe(stack, new Object[]{
                            "I I",
                            "III",
                            "I I",
                            Character.valueOf('I'), Item.IRON_INGOT,});
                    if(RailcraftPartItems.getIngotSteel().id != Item.IRON_INGOT.id) {
                        stack = stack.cloneItemStack();
                        stack.count = 32;
                        RollingMachineCraftingManager.getInstance().addRecipe(stack, new Object[]{
                                "III",
                                " I ",
                                "III",
                                Character.valueOf('I'), RailcraftPartItems.getIngotSteel(),});
                        RollingMachineCraftingManager.getInstance().addRecipe(stack, new Object[]{
                                "I I",
                                "III",
                                "I I",
                                Character.valueOf('I'), RailcraftPartItems.getIngotSteel(),});
                    }
                } else {
                    ModLoader.addRecipe(stack, new Object[]{
                            "III",
                            " I ",
                            "III",
                            Character.valueOf('I'), Item.IRON_INGOT,});
                }
                RailcraftLanguage.getInstance().registerItemName(stack, structure.getTag());

                stack = new ItemStack(item, 1, -1);
                for(int dye = 0; dye < 16; dye++) {
                    ItemStack colorStack = new ItemStack(item, 8, dye + 1);
                    ModLoader.addRecipe(colorStack, new Object[]{
                            "III",
                            "IDI",
                            "III",
                            Character.valueOf('I'), stack,
                            Character.valueOf('D'), new ItemStack(Item.INK_SACK, 1, dye),});
                    RailcraftLanguage.getInstance().registerItemName(colorStack, structure.getTag() + "." + EnumColor.fromId(dye + 1).getBasicTag());
                }
            }
            // Define Wood Post
            structure = EnumStructure.WOOD_POST;
            if(RailcraftConfig.isSubBlockEnabled(structure.getTag())) {
                MinecraftForge.setBlockHarvestLevel(blockStructure, structure.getId(), "axe", 0);
                ModLoader.registerTileEntity(structure.getTileClass(), "RCTileStructureWoodPost");

                RailcraftLanguage.getInstance().registerItemName(structure.getItem(), structure.getTag());

                ItemStack stack = structure.getItem(4);
                ModLoader.addShapelessRecipe(stack, new Object[]{
                        RailcraftPartItems.getTieWood()
                    });
                RailcraftLanguage.getInstance().registerItemName(stack, structure.getTag());
            }

            // Define Stone Post
            structure = EnumStructure.STONE_POST;
            if(RailcraftConfig.isSubBlockEnabled(structure.getTag())) {
                MinecraftForge.setBlockHarvestLevel(blockStructure, structure.getId(), "pickaxe", 2);
                ModLoader.registerTileEntity(structure.getTileClass(), "RCTileStructureStonePost");

                RailcraftLanguage.getInstance().registerItemName(structure.getItem(), structure.getTag());

                ItemStack stack = structure.getItem(8);
                ModLoader.addRecipe(stack, new Object[]{
                        "SIS",
                        "SIS",
                        "SIS",
                        Character.valueOf('I'), RailcraftPartItems.getRebar(),
                        Character.valueOf('S'), Block.STONE,});
            }
        }

        EnumCube cubeType = EnumCube.CONCRETE_BLOCK;
        if(RailcraftConfig.isSubBlockEnabled(cubeType.getTag())) {
            RailcraftBlocks.registerBlockCube();
            Block cube = RailcraftBlocks.getBlockCube();
            if(cube != null) {
                ItemStack stack = cubeType.getItem();
                RailcraftLanguage.getInstance().registerItemName(stack, cubeType.getTag());
                if(RailcraftModuleManager.isModuleLoaded(Module.FACTORY)
                    && RailcraftBlocks.getBlockUtility() != null
                    && RailcraftConfig.isSubBlockEnabled(EnumUtility.ROLLING_MACHINE.getTag())) {
                    stack.count = 8;
                    ModLoader.addRecipe(stack, new Object[]{
                            "SIS",
                            "ISI",
                            "SIS",
                            Character.valueOf('I'), RailcraftPartItems.getRebar(),
                            Character.valueOf('S'), Block.STONE,});
                } else {
                    stack.count = 4;
                    ModLoader.addRecipe(stack, new Object[]{
                            " S ",
                            "SIS",
                            " S ",
                            Character.valueOf('I'), Item.IRON_INGOT,
                            Character.valueOf('S'), Block.STONE,});
                }
            }
        }


//        cubeType = EnumCube.SANDY_BRICK;
//        if(RailcraftConfig.isSubBlockEnabled(cubeType.getTag())) {
//            RailcraftBlocks.registerBlockCube();
//            Block cube = RailcraftBlocks.getBlockCube();
//            if(cube != null) {
//                ItemStack stack = cubeType.getItem();
//                RailcraftLanguage.getInstance().registerItemName(stack, cubeType.getTag());
//                ModLoader.addRecipe(stack, new Object[]{
//                        "BMB",
//                        "M M",
//                        "BMB",
//                        Character.valueOf('B'), new ItemStack(Item.brick),
//                        Character.valueOf('M'), new ItemStack(Block.sand)
//                    });
//            }
//        }

        cubeType = EnumCube.INFERNAL_BRICK;
        if(RailcraftConfig.isSubBlockEnabled(cubeType.getTag())) {
            RailcraftBlocks.registerBlockCube();
            Block cube = RailcraftBlocks.getBlockCube();
            if(cube != null) {
                ItemStack stack = cubeType.getItem(4);
                RailcraftLanguage.getInstance().registerItemName(stack, cubeType.getTag());
                ModLoader.addRecipe(stack, new Object[]{
                        "MBM",
                        "BMB",
                        "MBM",
                        Character.valueOf('B'), new ItemStack(Block.NETHER_BRICK),
                        Character.valueOf('M'), new ItemStack(Block.SOUL_SAND)
                    });
            }
        }

//        cubeType = EnumCube.BANDED_PLANKS;
//        if(RailcraftConfig.isSubBlockEnabled(cubeType.getTag())) {
//            RailcraftBlocks.registerBlockCube();
//            Block cube = RailcraftBlocks.getBlockCube();
//            if(cube != null) {
//                ItemStack stack = cubeType.getItem(8);
//                RailcraftLanguage.getInstance().registerItemName(stack, cubeType.getTag());
//                ModLoader.addRecipe(stack, new Object[]{
//                        "WWW",
//                        "III",
//                        "WWW",
//                        Character.valueOf('I'), new ItemStack(Item.IRON_INGOT),
//                        Character.valueOf('W'), Block.planks});
//            }
//        }
    }

    @Override
    public void doSecond()
    {
    }
}
