package railcraft.common.modules;

import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftRails;
import railcraft.common.tracks.EnumTrack;

public class ModuleTracksWood extends RailcraftModule
{

    @Override
    public void doFirst()
    {
        RailcraftBlocks.registerBlockTrack();

        if(RailcraftBlocks.getBlockTrack() != null) {
            RailcraftRails.registerRail(EnumTrack.SLOW);
            RailcraftRails.registerRail(EnumTrack.SLOW_BOOSTER);
            RailcraftRails.registerRail(EnumTrack.SLOW_JUNCTION);
        }
    }

    @Override
    public void doSecond()
    {
    }
}
