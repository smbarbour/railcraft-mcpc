package railcraft.common.modules;

import net.minecraft.server.Block;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.ModLoader;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftConfig;
import railcraft.common.ItemRailcraft;
import railcraft.common.RailcraftLanguage;
import railcraft.common.RailcraftRails;
import railcraft.common.api.ItemRegistry;
import railcraft.common.tracks.EnumTrack;
import railcraft.common.structures.EnumStructure;
import railcraft.common.structures.ItemSignalBlockSurveyor;
import railcraft.common.structures.ItemSignalTuner;

public class ModuleSignals extends RailcraftModule
{

    @Override
    public void doFirst()
    {
        RailcraftBlocks.registerBlockStructure();

        Block blockStructure = RailcraftBlocks.getBlockStructure();
        if(blockStructure != null) {

            // Signal Parts
            Item itemPartSignalLamp = new ItemRailcraft(RailcraftConfig.getItemId("item.lamp")).d(134).a("signalLamp");
            Item itemPartControllerCircuit = new ItemRailcraft(RailcraftConfig.getItemId("item.circuit.controller")).d(150).a("controllerCircuit");
            Item itemPartReceiverCircuit = new ItemRailcraft(RailcraftConfig.getItemId("item.circuit.receiver")).d(166).a("receiverCircuit");

            int id = RailcraftConfig.getItemId("item.surveyor");
            if(id > 0) {
                Item item = new ItemSignalBlockSurveyor(id);
                RailcraftLanguage.getInstance().registerItemName(item, "item.surveyor");

                ModLoader.addRecipe(new ItemStack(item), new Object[]{
                        " C ",
                        "BGB",
                        " R ",
                        Character.valueOf('G'), Block.THIN_GLASS,
                        Character.valueOf('C'), Item.COMPASS,
                        Character.valueOf('B'), Block.STONE_BUTTON,
                        Character.valueOf('R'), Item.REDSTONE,});

                ItemRegistry.registerItem("item.surveyor", new ItemStack(item));

            }

            id = RailcraftConfig.getItemId("item.signal.tuner");
            if(id > 0) {
                Item item = new ItemSignalTuner(id);
                RailcraftLanguage.getInstance().registerItemName(item, "item.tuner");

                ModLoader.addRecipe(new ItemStack(item), new Object[]{
                        " T ",
                        "BRB",
                        "   ",
                        Character.valueOf('B'), Block.STONE_BUTTON,
                        Character.valueOf('R'), itemPartReceiverCircuit,
                        Character.valueOf('T'), Block.REDSTONE_TORCH_ON,});

                ItemRegistry.registerItem("item.signal.tuner", new ItemStack(item));
            }

            ItemStack stackSignalLamp = new ItemStack(itemPartSignalLamp);
            RailcraftLanguage.getInstance().registerItemName(stackSignalLamp, "part.signal.lamp");
            ModLoader.addRecipe(stackSignalLamp, new Object[]{
                    "PG ",
                    "PYT",
                    "PRS",
                    Character.valueOf('G'), new ItemStack(Item.INK_SACK, 1, 10),
                    Character.valueOf('Y'), new ItemStack(Item.INK_SACK, 1, 11),
                    Character.valueOf('R'), new ItemStack(Item.INK_SACK, 1, 1),
                    Character.valueOf('S'), Item.REDSTONE,
                    Character.valueOf('T'), Item.GLOWSTONE_DUST,
                    Character.valueOf('P'), Block.THIN_GLASS,});
            ItemRegistry.registerItem("part.signal.lamp", stackSignalLamp);

            ItemStack stackControllerCircuit = new ItemStack(itemPartControllerCircuit);
            RailcraftLanguage.getInstance().registerItemName(stackControllerCircuit, "part.circuit.controller");
            ModLoader.addRecipe(stackControllerCircuit, new Object[]{
                    "LRS",
                    "RGB",
                    "S# ",
                    Character.valueOf('L'), new ItemStack(Item.INK_SACK, 1, 4),
                    Character.valueOf('#'), Item.DIODE,
                    Character.valueOf('G'), Item.GOLD_INGOT,
                    Character.valueOf('S'), Block.SAND,
                    Character.valueOf('R'), Item.REDSTONE,
                    Character.valueOf('B'), Item.SLIME_BALL,});
            ItemRegistry.registerItem("part.circuit.controller", stackControllerCircuit);

            ItemStack stackReceiverCircuit = new ItemStack(itemPartReceiverCircuit);
            RailcraftLanguage.getInstance().registerItemName(stackReceiverCircuit, "part.circuit.receiver");
            ModLoader.addRecipe(stackReceiverCircuit, new Object[]{
                    " #S",
                    "BGR",
                    "SRL",
                    Character.valueOf('L'), new ItemStack(Item.INK_SACK, 1, 4),
                    Character.valueOf('#'), Item.DIODE,
                    Character.valueOf('G'), Item.GOLD_INGOT,
                    Character.valueOf('S'), Block.SAND,
                    Character.valueOf('R'), Item.REDSTONE,
                    Character.valueOf('B'), Item.SLIME_BALL,});
            ItemRegistry.registerItem("part.circuit.receiver", stackReceiverCircuit);

            // Define Block Signal
            EnumStructure structure = EnumStructure.BLOCK_SIGNAL;
            if(RailcraftConfig.isSubBlockEnabled(structure.getTag())) {
                ModLoader.registerTileEntity(structure.getTileClass(), "RCTileStructureBlockSignal");

                ItemStack stack = structure.getItem();
                ModLoader.addRecipe(stack, new Object[]{
                        "LCI",
                        " BI",
                        "   ",
                        Character.valueOf('C'), itemPartControllerCircuit,
                        Character.valueOf('I'), Item.IRON_INGOT,
                        Character.valueOf('L'), itemPartSignalLamp,
                        Character.valueOf('B'), new ItemStack(Item.INK_SACK, 1, 0),});
                RailcraftLanguage.getInstance().registerItemName(stack, structure.getTag());
            }

            // Define Dual Head Block Signal
            structure = EnumStructure.DUAL_HEAD_BLOCK_SIGNAL;
            if(RailcraftConfig.isSubBlockEnabled(structure.getTag())) {
                ModLoader.registerTileEntity(structure.getTileClass(), "RCTileStructureDualHeadBlockSignal");

                ItemStack stack = structure.getItem();
                ModLoader.addRecipe(stack, new Object[]{
                        "LCI",
                        " BI",
                        "LRI",
                        Character.valueOf('C'), itemPartControllerCircuit,
                        Character.valueOf('R'), itemPartReceiverCircuit,
                        Character.valueOf('I'), Item.IRON_INGOT,
                        Character.valueOf('L'), itemPartSignalLamp,
                        Character.valueOf('B'), new ItemStack(Item.INK_SACK, 1, 0),});
                RailcraftLanguage.getInstance().registerItemName(stack, structure.getTag());
            }

            // Define Distant Signal
            structure = EnumStructure.DISTANT_SIGNAL;
            if(RailcraftConfig.isSubBlockEnabled(structure.getTag())) {
                ModLoader.registerTileEntity(structure.getTileClass(), "RCTileStructureDistantSignal");
                ItemStack stack = structure.getItem();
                ModLoader.addRecipe(stack, new Object[]{
                        "LCI",
                        " BI",
                        "   ",
                        Character.valueOf('C'), itemPartReceiverCircuit,
                        Character.valueOf('I'), Item.IRON_INGOT,
                        Character.valueOf('L'), itemPartSignalLamp,
                        Character.valueOf('B'), new ItemStack(Item.INK_SACK, 1, 0),});
                RailcraftLanguage.getInstance().registerItemName(stack, structure.getTag());
            }

            // Define Switch Lever
            structure = EnumStructure.SWITCH_LEVER;
            if(RailcraftConfig.isSubBlockEnabled(structure.getTag())) {
                ModLoader.registerTileEntity(structure.getTileClass(), "RCTileStructureSwitchLever");

                ItemStack stack = structure.getItem();
                ModLoader.addRecipe(stack, new Object[]{
                        "RBW",
                        "PLI",
                        Character.valueOf('W'), new ItemStack(Item.INK_SACK, 1, 15),
                        Character.valueOf('I'), Item.IRON_INGOT,
                        Character.valueOf('L'), Block.LEVER,
                        Character.valueOf('P'), Block.PISTON,
                        Character.valueOf('B'), new ItemStack(Item.INK_SACK, 1, 0),
                        Character.valueOf('R'), new ItemStack(Item.INK_SACK, 1, 1),});
                ModLoader.addRecipe(stack, new Object[]{
                        "RBW",
                        "ILP",
                        Character.valueOf('W'), new ItemStack(Item.INK_SACK, 1, 15),
                        Character.valueOf('I'), Item.IRON_INGOT,
                        Character.valueOf('L'), Block.LEVER,
                        Character.valueOf('P'), Block.PISTON,
                        Character.valueOf('B'), new ItemStack(Item.INK_SACK, 1, 0),
                        Character.valueOf('R'), new ItemStack(Item.INK_SACK, 1, 1),});
                RailcraftLanguage.getInstance().registerItemName(stack, structure.getTag());
            }

            // Define Switch Motor
            structure = EnumStructure.SWITCH_MOTOR;
            if(RailcraftConfig.isSubBlockEnabled(structure.getTag())) {
                ModLoader.registerTileEntity(structure.getTileClass(), "RCTileStructureSwitchMotor");

                ItemStack stack = structure.getItem();
                ModLoader.addRecipe(stack, new Object[]{
                        "RBW",
                        "PCI",
                        Character.valueOf('W'), new ItemStack(Item.INK_SACK, 1, 15),
                        Character.valueOf('I'), Item.IRON_INGOT,
                        Character.valueOf('P'), Block.PISTON,
                        Character.valueOf('C'), itemPartReceiverCircuit,
                        Character.valueOf('B'), new ItemStack(Item.INK_SACK, 1, 0),
                        Character.valueOf('R'), new ItemStack(Item.INK_SACK, 1, 1),});
                ModLoader.addRecipe(stack, new Object[]{
                        "RBW",
                        "ICP",
                        Character.valueOf('W'), new ItemStack(Item.INK_SACK, 1, 15),
                        Character.valueOf('I'), Item.IRON_INGOT,
                        Character.valueOf('P'), Block.PISTON,
                        Character.valueOf('C'), itemPartReceiverCircuit,
                        Character.valueOf('B'), new ItemStack(Item.INK_SACK, 1, 0),
                        Character.valueOf('R'), new ItemStack(Item.INK_SACK, 1, 1),});
                RailcraftLanguage.getInstance().registerItemName(stack, structure.getTag());
            }

            // Define Receiver Box
            structure = EnumStructure.BOX_RECEIVER;
            if(RailcraftConfig.isSubBlockEnabled(structure.getTag())) {
                ModLoader.registerTileEntity(structure.getTileClass(), "RCTileStructureReceiverBox");

                ItemStack stack = structure.getItem();
                ModLoader.addRecipe(stack, new Object[]{
                        "ICI",
                        "IRI",
                        Character.valueOf('I'), Item.IRON_INGOT,
                        Character.valueOf('R'), Item.REDSTONE,
                        Character.valueOf('C'), itemPartReceiverCircuit,});
                RailcraftLanguage.getInstance().registerItemName(stack, structure.getTag());
            }

            // Define Controller Box
            structure = EnumStructure.BOX_CONTROLLER;
            if(RailcraftConfig.isSubBlockEnabled(structure.getTag())) {
                ModLoader.registerTileEntity(structure.getTileClass(), "RCTileStructureControllerBox");

                ItemStack stack = structure.getItem();
                ModLoader.addRecipe(stack, new Object[]{
                        "ICI",
                        "IRI",
                        Character.valueOf('I'), Item.IRON_INGOT,
                        Character.valueOf('R'), Item.REDSTONE,
                        Character.valueOf('C'), itemPartControllerCircuit,});
                RailcraftLanguage.getInstance().registerItemName(stack, structure.getTag());
            }

//            EnumStructure structurre = EnumStructure.CART_BALANCER;
//            id = RailcraftConfig.getItemId(structure.getTag());
//            if(id > 0) {
//                ModLoader.RegisterTileEntity(TileCartBalancer.class, "RCCartBalancerTile");
//                Railcraft.registerGui(RailcraftConfig.getGuiId("net.gui.structure.minecart.balancer"));
//                Item item = structurre.createOldItem(id);
//                ItemStack stack = new ItemStack(item);
//                Railcraft.registerStructureRenderer(structure);
//                ModLoader.addRecipe(stack, new Object[]{
//                        "O O",
//                        "SRS",
//                        "OCO",
//                        Character.valueOf('O'), Block.obsidian,
//                        Character.valueOf('S'), Block.blockSteel,
//                        Character.valueOf('R'), Item.eyeOfEnder,
//                        Character.valueOf('C'), Block.chest,});
//                RailcraftLanguage.getInstance().registerItemName(stack, "Cart Balancer");
//            }
        }

        RailcraftRails.registerRail(EnumTrack.SWITCH);

        if(RailcraftModuleManager.isModuleLoaded(RailcraftModuleManager.Module.TRACKS_WOOD)) {
            RailcraftRails.registerRail(EnumTrack.SLOW_SWITCH);
        }

        if(RailcraftModuleManager.isModuleLoaded(RailcraftModuleManager.Module.TRACKS_HIGHSPEED)) {
            RailcraftRails.registerRail(EnumTrack.SPEED_SWITCH);
        }
    }

    @Override
    public void doSecond()
    {
    }
}
