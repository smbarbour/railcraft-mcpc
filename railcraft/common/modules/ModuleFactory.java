package railcraft.common.modules;

import cpw.mods.fml.common.registry.FMLRegistry;
import java.util.HashMap;
import net.minecraft.server.Block;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.ModLoader;
import forge.MinecraftForge;
import forge.oredict.OreDictionary;
import ic2.api.Ic2Recipes;
import ic2.api.Items;
import railcraft.Railcraft;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftConfig;
import railcraft.common.RailcraftPartItems;
import railcraft.common.RailcraftToolItems;
import railcraft.common.modules.orehandlers.FactoryOreHandler;
import railcraft.common.RailcraftLanguage;
import railcraft.common.util.liquids.LiquidItems;
import railcraft.common.cube.EnumCube;
import railcraft.common.util.crafting.BlastFurnaceCraftingManager;
import railcraft.common.util.crafting.CokeOvenCraftingManager;
import railcraft.common.util.crafting.RockCrusherCraftingManager;
import railcraft.common.utility.EnumUtility;
import railcraft.common.util.crafting.RollingMachineCraftingManager;
import railcraft.common.cube.EntityFallingCube;

public class ModuleFactory extends RailcraftModule
{

    @Override
    public void doFirst()
    {
        RailcraftBlocks.registerBlockUtility();
        RailcraftBlocks.registerBlockCube();

        Block blockUtility = RailcraftBlocks.getBlockUtility();
        if(blockUtility == null) {
            defineAlternativeRollingRecipes();
            return;
        }

        RailcraftToolItems.registerCoalCoke();

        EnumUtility typeUtility = EnumUtility.COKE_OVEN;
        if(RailcraftConfig.isSubBlockEnabled(typeUtility.getTag())) {
            ItemStack stack = typeUtility.getItem();
            ModLoader.addRecipe(stack, new Object[]{
                    "MBM",
                    "BMB",
                    "MBM",
                    Character.valueOf('B'), new ItemStack(Item.CLAY_BRICK),
                    Character.valueOf('M'), new ItemStack(Block.SAND), //                        Character.valueOf('C'), new ItemStack(Item.clay)
                });

            RailcraftLanguage.getInstance().registerItemName(stack, typeUtility.getTag());

            if(RailcraftToolItems.getCoalCoke() != null) {
                CokeOvenCraftingManager.addRecipe(Item.COAL.id, 0, RailcraftToolItems.getCoalCoke(), LiquidItems.getCreosoteOilLiquid(500), 3000);
            }
            CokeOvenCraftingManager.addRecipe(Block.LOG.id, new ItemStack(Item.COAL, 1, 1), LiquidItems.getCreosoteOilLiquid(250), 3000);
        }

        typeUtility = EnumUtility.BLAST_FURNACE;
        if(RailcraftConfig.isSubBlockEnabled(typeUtility.getTag())) {
            ItemStack stack = typeUtility.getItem(4);
            ModLoader.addRecipe(stack, new Object[]{
                    "MBM",
                    "BPB",
                    "MBM",
                    Character.valueOf('B'), new ItemStack(Block.NETHER_BRICK),
                    Character.valueOf('M'), new ItemStack(Block.SOUL_SAND),
                    Character.valueOf('P'), Item.MAGMA_CREAM,});

            RailcraftLanguage.getInstance().registerItemName(stack, typeUtility.getTag());

            BlastFurnaceCraftingManager.addRecipe(Item.IRON_INGOT.id, 320, RailcraftPartItems.getIngotSteel(1));
        }

        typeUtility = EnumUtility.ROCK_CRUSHER;
        if(RailcraftConfig.isSubBlockEnabled(typeUtility.getTag())) {
            ItemStack stack = typeUtility.getItem(4);
            ModLoader.addRecipe(stack, new Object[]{
                    "PDP",
                    "DSD",
                    "PDP",
                    Character.valueOf('D'), new ItemStack(Item.DIAMOND),
                    Character.valueOf('P'), new ItemStack(Block.PISTON),
                    Character.valueOf('S'), EnumCube.STEEL_BLOCK.getItem(),});

            RailcraftLanguage.getInstance().registerItemName(stack, typeUtility.getTag());

            HashMap<ItemStack, Float> output = new HashMap<ItemStack, Float>();
            output.put(EnumCube.CRUSHED_OBSIDIAN.getItem(), 1.0f);
            output.put(RailcraftPartItems.getDustObsidian(), 0.25f);
            RockCrusherCraftingManager.addRecipe(Block.OBSIDIAN.id, 0, output);

            output = new HashMap<ItemStack, Float>();
            output.put(RailcraftPartItems.getDustObsidian(), 1.0f);
            RockCrusherCraftingManager.addRecipe(EnumCube.CRUSHED_OBSIDIAN.getItem(), output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Item.CLAY_BRICK, 3), 1.0f);
            output.put(new ItemStack(Item.CLAY_BRICK), 0.5f);
            output.put(new ItemStack(Block.SAND), 0.25f);
            output.put(new ItemStack(Block.SAND), 0.25f);
            output.put(new ItemStack(Block.SAND), 0.25f);
            output.put(new ItemStack(Block.SAND), 0.25f);
            output.put(new ItemStack(Block.SAND), 0.25f);
            RockCrusherCraftingManager.addRecipe(EnumUtility.COKE_OVEN.getItem(), output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Block.NETHER_BRICK), 0.75f);
            output.put(new ItemStack(Block.SOUL_SAND), 0.75f);
            output.put(new ItemStack(Item.BLAZE_POWDER), 0.05f);
            RockCrusherCraftingManager.addRecipe(EnumUtility.BLAST_FURNACE.getItem(), output);

            output = new HashMap<ItemStack, Float>();
            output.put(EnumCube.CRUSHED_OBSIDIAN.getItem(), 1.0f);
            output.put(EnumCube.CRUSHED_OBSIDIAN.getItem(), 0.5f);
            output.put(new ItemStack(Item.DIAMOND), 0.5f);
            output.put(new ItemStack(Block.OBSIDIAN), 0.25f);
            output.put(RailcraftPartItems.getDustObsidian(), 0.25f);
            output.put(new ItemStack(Item.GOLD_NUGGET, 16), 1.0f);
            output.put(new ItemStack(Item.GOLD_NUGGET, 4), 0.5f);
            output.put(new ItemStack(Item.GOLD_NUGGET, 4), 0.5f);
            output.put(new ItemStack(Item.GOLD_NUGGET, 2), 0.25f);
            output.put(new ItemStack(Item.GOLD_NUGGET, 2), 0.25f);
            output.put(new ItemStack(Item.GOLD_NUGGET, 1), 0.10f);
            output.put(new ItemStack(Item.GOLD_NUGGET, 1), 0.10f);
            output.put(new ItemStack(Item.GOLD_NUGGET, 1), 0.10f);
            output.put(new ItemStack(Item.GOLD_NUGGET, 1), 0.10f);
            RockCrusherCraftingManager.addRecipe(EnumCube.WORLD_ANCHOR.getItem(), output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Block.GRAVEL), 1.0f);
            output.put(new ItemStack(Item.FLINT), 0.10f);
            RockCrusherCraftingManager.addRecipe(Block.COBBLESTONE.id, 0, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Block.GRAVEL), 1.0f);
            output.put(new ItemStack(Block.VINE), 0.10f);
            RockCrusherCraftingManager.addRecipe(Block.MOSSY_COBBLESTONE.id, 0, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Block.SAND), 1.0f);
            output.put(new ItemStack(Item.GOLD_NUGGET), 0.005f);
            output.put(new ItemStack(Item.DIAMOND), 0.00025f);
            RockCrusherCraftingManager.addRecipe(Block.GRAVEL.id, 0, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Block.COBBLESTONE), 1.0f);
            RockCrusherCraftingManager.addRecipe(Block.STONE.id, 0, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Block.SAND), 1.0f);
            RockCrusherCraftingManager.addRecipe(Block.SANDSTONE.id, -1, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Item.CLAY_BRICK, 3), 1.0f);
            output.put(new ItemStack(Item.CLAY_BRICK), 0.5f);
            RockCrusherCraftingManager.addRecipe(Block.BRICK.id, 0, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Item.CLAY_BALL, 4), 1.0f);
            RockCrusherCraftingManager.addRecipe(Block.CLAY.id, 0, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Block.COBBLESTONE), 1.0f);
            RockCrusherCraftingManager.addRecipe(Block.SMOOTH_BRICK.id, -1, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Block.GRAVEL), 1.0f);
            RockCrusherCraftingManager.addRecipe(Block.COBBLESTONE_STAIRS.id, 0, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Block.COBBLESTONE), 1.0f);
            RockCrusherCraftingManager.addRecipe(Block.STONE_STAIRS.id, 0, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Block.NETHER_BRICK), 1.0f);
            RockCrusherCraftingManager.addRecipe(Block.NETHER_BRICK_STAIRS.id, 0, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Item.CLAY_BRICK, 4), 1.0f);
            output.put(new ItemStack(Item.CLAY_BRICK), 0.5f);
            output.put(new ItemStack(Item.CLAY_BRICK), 0.5f);
            RockCrusherCraftingManager.addRecipe(Block.BRICK_STAIRS.id, 0, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Block.COBBLESTONE), 0.45f);
            RockCrusherCraftingManager.addRecipe(Block.STEP.id, 0, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Block.SAND), 0.45f);
            RockCrusherCraftingManager.addRecipe(Block.STEP.id, 1, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Block.GRAVEL), 0.45f);
            RockCrusherCraftingManager.addRecipe(Block.STEP.id, 3, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Item.CLAY_BRICK), 1.0f);
            output.put(new ItemStack(Item.CLAY_BRICK), 0.75f);
            RockCrusherCraftingManager.addRecipe(Block.STEP.id, 4, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Block.COBBLESTONE), 0.45f);
            RockCrusherCraftingManager.addRecipe(Block.STEP.id, 5, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Block.SNOW_BLOCK), 0.85f);
            output.put(new ItemStack(Item.SNOW_BALL), 0.25f);
            RockCrusherCraftingManager.addRecipe(Block.ICE.id, 0, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Block.NETHER_BRICK), 1.0f);
            RockCrusherCraftingManager.addRecipe(Block.NETHER_FENCE.id, 0, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Item.GLOWSTONE_DUST, 3), 1.0f);
            output.put(new ItemStack(Item.GLOWSTONE_DUST), 0.75f);
            RockCrusherCraftingManager.addRecipe(Block.GLOWSTONE.id, 0, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Item.GLOWSTONE_DUST, 3), 1.0f);
            output.put(new ItemStack(Item.GLOWSTONE_DUST), 0.75f);
            output.put(new ItemStack(Item.REDSTONE, 3), 1.0f);
            output.put(new ItemStack(Item.REDSTONE), 0.75f);
            RockCrusherCraftingManager.addRecipe(Block.REDSTONE_LAMP_OFF.id, 0, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Item.INK_SACK, 4, 15), 1.0f);
            RockCrusherCraftingManager.addRecipe(Item.BONE.id, 0, output);

            output = new HashMap<ItemStack, Float>();
            output.put(new ItemStack(Item.BLAZE_POWDER, 2), 1.0f);
            output.put(new ItemStack(Item.BLAZE_POWDER), 0.25f);
            output.put(new ItemStack(Item.BLAZE_POWDER), 0.25f);
            output.put(new ItemStack(Item.BLAZE_POWDER), 0.25f);
            RockCrusherCraftingManager.addRecipe(Item.BLAZE_ROD.id, 0, output);
        }


        typeUtility = EnumUtility.ROLLING_MACHINE;
        if(RailcraftConfig.isSubBlockEnabled(typeUtility.getTag())) {
            ItemStack stack = typeUtility.getItem();
            ModLoader.addRecipe(stack, new Object[]{
                    "IPI",
                    "PCP",
                    "IPI",
                    Character.valueOf('I'), Item.IRON_INGOT,
                    Character.valueOf('P'), Block.PISTON,
                    Character.valueOf('C'), Block.WORKBENCH
                });
            RailcraftLanguage.getInstance().registerItemName(stack, typeUtility.getTag());

            if(!RailcraftConfig.useOldRecipes()) {
                RollingMachineCraftingManager.getInstance().addRecipe(RailcraftPartItems.getRailStandard(8), RailcraftPartItems.getRailStandardRecipe(new ItemStack(Item.IRON_INGOT)));
                if(RailcraftPartItems.getIngotSteel().id != Item.IRON_INGOT.id) {
                    RollingMachineCraftingManager.getInstance().addRecipe(RailcraftPartItems.getRailStandard(16), RailcraftPartItems.getRailStandardRecipe(RailcraftPartItems.getIngotSteel()));
                }
                RollingMachineCraftingManager.getInstance().addRecipe(RailcraftPartItems.getRailAdvanced(8), RailcraftPartItems.getRailAdvancedRecipe());
                RollingMachineCraftingManager.getInstance().addRecipe(RailcraftPartItems.getRailSpeed(8), RailcraftPartItems.getRailSpeedRecipe());
                RollingMachineCraftingManager.getInstance().addRecipe(RailcraftPartItems.getRailReinforced(8), RailcraftPartItems.getRailReinforcedRecipe());
            }

            ItemStack rebar = RailcraftPartItems.getRebar(4);
            RollingMachineCraftingManager.getInstance().addRecipe(rebar, RailcraftPartItems.getRebarRecipe(new ItemStack(Item.IRON_INGOT)));
            RollingMachineCraftingManager.getInstance().addRecipe(rebar, RailcraftPartItems.getRebarRecipeAlt(new ItemStack(Item.IRON_INGOT)));
            if(RailcraftPartItems.getIngotSteel().id != Item.IRON_INGOT.id) {
                rebar = RailcraftPartItems.getRebar(8);
                RollingMachineCraftingManager.getInstance().addRecipe(rebar, RailcraftPartItems.getRebarRecipe(RailcraftPartItems.getIngotSteel()));
                RollingMachineCraftingManager.getInstance().addRecipe(rebar, RailcraftPartItems.getRebarRecipeAlt(RailcraftPartItems.getIngotSteel()));
            }
        } else {
            defineAlternativeRollingRecipes();
        }

        if(RailcraftBlocks.getBlockCube() != null) {
            EnumCube type = EnumCube.STEEL_BLOCK;
            if(RailcraftConfig.isSubBlockEnabled(type.getTag())) {
                ItemStack stack = type.getItem();
                RailcraftLanguage.getInstance().registerItemName(stack, type.getTag());
                ItemStack ingotSteel = RailcraftPartItems.getIngotSteel();
                if(ingotSteel != null && ingotSteel.getItem() != Item.IRON_INGOT) {
                    ModLoader.addRecipe(stack, new Object[]{
                            "III",
                            "III",
                            "III",
                            Character.valueOf('I'), ingotSteel,});

                    ModLoader.addShapelessRecipe(RailcraftPartItems.getIngotSteel(9), new Object[]{stack});

                    MinecraftForge.addDungeonLoot(stack, RailcraftConfig.getLootChance("steel.block"));
                }
            }

            type = EnumCube.CRUSHED_OBSIDIAN;
            if(RailcraftConfig.isSubBlockEnabled(type.getTag())) {
                ItemStack stack = type.getItem();
                RailcraftLanguage.getInstance().registerItemName(stack, type.getTag());

                FMLRegistry.registerEntityID(EntityFallingCube.class, "FallingCube", RailcraftConfig.getEntityId("entity.falling.cube"));
                MinecraftForge.registerEntity(EntityFallingCube.class, Railcraft.getMod(), 52, 160, 20, true);

                if(RailcraftModuleManager.isIC2Loaded()) {
                    Ic2Recipes.addMaceratorRecipe(new ItemStack(Block.OBSIDIAN), stack);
                }
            }
        }
    }

    @Override
    public void doSecond()
    {
        if(RailcraftModuleManager.isModuleLoaded(RailcraftModuleManager.Module.STRUCTURES) && RailcraftBlocks.getBlockCube() != null) {
            if(EnumUtility.BLAST_FURNACE.isEnabled() && EnumCube.INFERNAL_BRICK.isEnabled()) {

                ItemStack stack = EnumUtility.BLAST_FURNACE.getItem(4);
                ModLoader.addRecipe(stack, new Object[]{
                        " B ",
                        "BPB",
                        " B ",
                        Character.valueOf('B'), new ItemStack(RailcraftBlocks.getBlockCube(), 1, EnumCube.INFERNAL_BRICK.getId()),
                        Character.valueOf('P'), Item.MAGMA_CREAM,});
            }
        }
    }

    @Override
    public void doLast()
    {
        if(RailcraftConfig.defineDictionaryRecipes()) {
            OreDictionary.registerOreHandler(new FactoryOreHandler());
        }
        if(RailcraftModuleManager.isIC2Loaded()) {
            HashMap<ItemStack, Float> output = new HashMap<ItemStack, Float>();
            ItemStack dust = Items.getItem("ironDust");
            if(dust != null) {
                dust = dust.cloneItemStack();
                dust.count = 2;
                output.put(dust, 1.0f);
                RockCrusherCraftingManager.addRecipe(Block.IRON_ORE.id, 0, output);
            }

            output = new HashMap<ItemStack, Float>();
            dust = Items.getItem("goldDust");
            if(dust != null) {
                dust = dust.cloneItemStack();
                dust.count = 2;
                output.put(dust, 1.0f);
                RockCrusherCraftingManager.addRecipe(Block.GOLD_ORE.id, 0, output);
            }
        }
    }

    public static int getFuelValues(int id)
    {
        if(RailcraftToolItems.getCoalCoke() != null && id == RailcraftToolItems.getCoalCoke().id) {
            return 3200;
        }
        return 0;
    }

    private static void defineAlternativeRollingRecipes()
    {
        ItemStack rebar = RailcraftPartItems.getRebar(4);
        ModLoader.addRecipe(rebar, RailcraftPartItems.getRebarRecipe(new ItemStack(Item.IRON_INGOT)));
        ModLoader.addRecipe(rebar, RailcraftPartItems.getRebarRecipeAlt(new ItemStack(Item.IRON_INGOT)));
        if(RailcraftPartItems.getIngotSteel().id != Item.IRON_INGOT.id) {
            rebar = RailcraftPartItems.getRebar(8);
            ModLoader.addRecipe(rebar, RailcraftPartItems.getRebarRecipe(RailcraftPartItems.getIngotSteel()));
            ModLoader.addRecipe(rebar, RailcraftPartItems.getRebarRecipeAlt(RailcraftPartItems.getIngotSteel()));
        }

        if(!RailcraftConfig.useOldRecipes()) {
            ModLoader.addRecipe(RailcraftPartItems.getRailStandard(8), RailcraftPartItems.getRailStandardRecipe(new ItemStack(Item.IRON_INGOT)));
            if(RailcraftPartItems.getIngotSteel().id != Item.IRON_INGOT.id) {
                ModLoader.addRecipe(RailcraftPartItems.getRailStandard(16), RailcraftPartItems.getRailStandardRecipe(RailcraftPartItems.getIngotSteel()));
            }
            ModLoader.addRecipe(RailcraftPartItems.getRailAdvanced(8), RailcraftPartItems.getRailAdvancedRecipe());
            ModLoader.addRecipe(RailcraftPartItems.getRailSpeed(8), RailcraftPartItems.getRailSpeedRecipe());
            ModLoader.addRecipe(RailcraftPartItems.getRailReinforced(8), RailcraftPartItems.getRailReinforcedRecipe());
        }
    }
}
