package railcraft.common.modules;

import net.minecraft.server.Block;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.ModLoader;
import railcraft.Railcraft;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftConfig;
import railcraft.common.RailcraftLanguage;
import railcraft.common.RailcraftRails;
import railcraft.common.RailcraftToolItems;
import railcraft.common.detector.BlockDetector.Detector;
import railcraft.common.tracks.EnumTrack;
import railcraft.common.utility.EnumUtility;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class ModuleTrain extends RailcraftModule
{

    @Override
    public void doFirst()
    {
        RailcraftBlocks.registerBlockDetector();
        RailcraftBlocks.registerBlockUtility();
        RailcraftBlocks.registerBlockTrack();

        RailcraftRails.registerRail(EnumTrack.BOARDING_TRAIN);
        RailcraftRails.registerRail(EnumTrack.HOLDING_TRAIN);
        RailcraftRails.registerRail(EnumTrack.COUPLER);
        RailcraftRails.registerRail(EnumTrack.DECOUPLER);

        if(RailcraftBlocks.getBlockDetector() != null) {
            ModLoader.addRecipe(new ItemStack(RailcraftBlocks.getBlockDetector(), 1, Detector.TRAIN.getMeta()), new Object[]{
                    "XXX",
                    "XPX",
                    "XXX",
                    Character.valueOf('X'), Block.NETHER_BRICK,
                    Character.valueOf('P'), Block.STONE_PLATE,});
        }

        EnumUtility type = EnumUtility.TRAIN_DISPENSER;
        if(RailcraftConfig.isSubBlockEnabled(type.getTag()) && EnumUtility.MINECART_DISPENSER.isEnabled()) {
            ItemStack trainDispenser = type.getItem(1);
            ItemStack crowbar = RailcraftToolItems.getCrowbar();
            crowbar.setData(-1);
            ModLoader.addRecipe(trainDispenser, new Object[]{
                    "rcr",
                    "cdc",
                    "rcr",
                    Character.valueOf('d'), EnumUtility.MINECART_DISPENSER.getItem(),
                    Character.valueOf('c'), crowbar,
                    Character.valueOf('r'), Item.REDSTONE});
            RailcraftLanguage.getInstance().registerItemName(trainDispenser, type.getTag());
        }
    }
}
