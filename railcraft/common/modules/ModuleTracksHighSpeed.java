package railcraft.common.modules;

import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftRails;
import railcraft.common.tracks.EnumTrack;

public class ModuleTracksHighSpeed extends RailcraftModule
{

    @Override
    public void doFirst()
    {
        RailcraftBlocks.registerBlockTrack();

        if(RailcraftBlocks.getBlockTrack() != null) {
            RailcraftRails.registerRail(EnumTrack.SPEED);
            RailcraftRails.registerRail(EnumTrack.SPEED_BOOST);
            RailcraftRails.registerRail(EnumTrack.SPEED_TRANSITION);
        }
    }

    @Override
    public void doSecond()
    {
    }
}
