package railcraft.common.modules.orehandlers;

import java.util.logging.Level;
import net.minecraft.server.Block;
import net.minecraft.server.ItemStack;
import net.minecraft.server.ModLoader;
import forge.IOreHandler;
import railcraft.Railcraft;
import railcraft.common.RailcraftBlocks;
import railcraft.common.detector.BlockDetector.Detector;

public class IC2OreHandler implements IOreHandler
{

    @Override
    public void registerOre(String oreClass, ItemStack ore)
    {
        if(ore == null) {
            return;
        }
        if(oreClass.equals("ingotTin")) {
            Railcraft.log(Level.FINER, "IC2 Module: Tin detected, adding Energy Detector recipe: id={0}", ore.id);

            RailcraftBlocks.registerBlockDetector();
            Block blockDetector = RailcraftBlocks.getBlockDetector();

            if(blockDetector != null) {
                ItemStack stack = new ItemStack(blockDetector, 1, Detector.ENERGY.getMeta());
                ModLoader.addRecipe(stack, new Object[]{
                        "XXX",
                        "XPX",
                        "XXX",
                        Character.valueOf('X'), ore,
                        Character.valueOf('P'), Block.STONE_PLATE,});
            }
        }
    }
}
