package railcraft.common.modules.orehandlers;

import java.util.logging.Level;
import net.minecraft.server.ItemBlock;
import net.minecraft.server.ItemStack;
import forge.IOreHandler;
import railcraft.Railcraft;
import railcraft.common.carts.EntityTunnelBore;

public class BoreOreHandler implements IOreHandler
{

    @Override
    public void registerOre(String oreClass, ItemStack ore)
    {
        if(ore == null) {
            return;
        }
        if(ore.getItem() instanceof ItemBlock) {
            Railcraft.log(Level.FINER, "Automation Module: Ore Detected, adding to blocks Tunnel Bore can mine: {0}, id={1} meta={2}", new Object[]{oreClass, ore.id, ore.getData()});
            EntityTunnelBore.addMineableBlock(ore.id, ore.getData());
        }
    }
}
