package railcraft.common.modules.orehandlers;

import java.util.HashMap;
import java.util.logging.Level;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import forge.IOreHandler;
import ic2.api.Items;
import railcraft.Railcraft;
import railcraft.common.RailcraftConfig;
import railcraft.common.RailcraftPartItems;
import railcraft.common.api.InventoryTools;
import railcraft.common.util.crafting.RockCrusherCraftingManager;
import railcraft.common.modules.RailcraftModuleManager;
import railcraft.common.modules.RailcraftModuleManager.Module;
import railcraft.common.structures.EnumStructure;
import railcraft.common.util.crafting.RollingMachineCraftingManager;

public class FactoryOreHandler implements IOreHandler
{

    @Override
    public void registerOre(String oreClass, ItemStack ore)
    {
        if(ore == null || RailcraftConfig.useOldRecipes()) {
            return;
        }
        if(oreClass.equals("ingotBronze")) {
            Railcraft.log(Level.FINER, "Factory Module: Bronze detected, adding alternative recipes: {0}, id={1}", new Object[]{oreClass, ore.id});

            ItemStack rail = RailcraftPartItems.getRailStandard(6);
            RollingMachineCraftingManager.getInstance().addRecipe(rail, RailcraftPartItems.getRailStandardRecipe(ore));

            ItemStack rebar = RailcraftPartItems.getRebar(4);
            if(rebar.id != Item.IRON_INGOT.id) {
                RollingMachineCraftingManager.getInstance().addRecipe(rebar, RailcraftPartItems.getRebarRecipe(ore));
                RollingMachineCraftingManager.getInstance().addRecipe(rebar, RailcraftPartItems.getRebarRecipeAlt(ore));
            }

            ItemStack post = EnumStructure.METAL_POST.getItem(12);
            if(RailcraftModuleManager.isModuleLoaded(Module.STRUCTURES) && post != null) {
                RollingMachineCraftingManager.getInstance().addRecipe(post, new Object[]{
                        "III",
                        " I ",
                        "III",
                        Character.valueOf('I'), ore,});
                RollingMachineCraftingManager.getInstance().addRecipe(post, new Object[]{
                        "I I",
                        "III",
                        "I I",
                        Character.valueOf('I'), ore,});
            }
        } else if(oreClass.equals("ingotSteel") && !InventoryTools.isItemEqual(ore, RailcraftPartItems.getIngotSteel())) {
            Railcraft.log(Level.FINER, "Factory Module: Steel detected, adding alternative recipes: {0}, id={1}", new Object[]{oreClass, ore.id});

            ItemStack rail = RailcraftPartItems.getRailStandard(14);
            RollingMachineCraftingManager.getInstance().addRecipe(rail, RailcraftPartItems.getRailStandardRecipe(ore));

            rail = RailcraftPartItems.getRailSpeed(6);
            RollingMachineCraftingManager.getInstance().addRecipe(rail, new Object[]{
                    "IBG",
                    "IBG",
                    "IBG",
                    Character.valueOf('I'), ore,
                    Character.valueOf('I'), Item.GOLD_INGOT,
                    Character.valueOf('B'), Item.BLAZE_POWDER
                });

            ItemStack rebar = RailcraftPartItems.getRebar(10);
            if(rebar.id != Item.IRON_INGOT.id) {
                RollingMachineCraftingManager.getInstance().addRecipe(rebar, RailcraftPartItems.getRebarRecipe(ore));
                RollingMachineCraftingManager.getInstance().addRecipe(rebar, RailcraftPartItems.getRebarRecipeAlt(ore));
            }

            ItemStack post = EnumStructure.METAL_POST.getItem(28);
            if(RailcraftModuleManager.isModuleLoaded(Module.STRUCTURES) && post != null) {
                RollingMachineCraftingManager.getInstance().addRecipe(post, new Object[]{
                        "III",
                        " I ",
                        "III",
                        Character.valueOf('I'), ore,});
                RollingMachineCraftingManager.getInstance().addRecipe(post, new Object[]{
                        "I I",
                        "III",
                        "I I",
                        Character.valueOf('I'), ore,});
            }
        } else if(oreClass.equals("ingotRefinedIron")) {
            Railcraft.log(Level.FINER, "Factory Module: Refined Iron detected, adding alternative recipes: {0}, id={1}", new Object[]{oreClass, ore.id});

            ItemStack rail = RailcraftPartItems.getRailStandard(10);
            RollingMachineCraftingManager.getInstance().addRecipe(rail, RailcraftPartItems.getRailStandardRecipe(ore));

            ItemStack rebar = RailcraftPartItems.getRebar(8);
            if(rebar.id != Item.IRON_INGOT.id) {
                RollingMachineCraftingManager.getInstance().addRecipe(rebar, RailcraftPartItems.getRebarRecipe(ore));
                RollingMachineCraftingManager.getInstance().addRecipe(rebar, RailcraftPartItems.getRebarRecipeAlt(ore));
            }

            ItemStack post = EnumStructure.METAL_POST.getItem(20);
            if(RailcraftModuleManager.isModuleLoaded(Module.STRUCTURES) && post != null) {
                RollingMachineCraftingManager.getInstance().addRecipe(post, new Object[]{
                        "III",
                        " I ",
                        "III",
                        Character.valueOf('I'), ore,});
                RollingMachineCraftingManager.getInstance().addRecipe(post, new Object[]{
                        "I I",
                        "III",
                        "I I",
                        Character.valueOf('I'), ore,});
            }
        }
        if(RailcraftModuleManager.isIC2Loaded()) {
            if(oreClass.equals("oreCopper")) {
                HashMap<ItemStack, Float> output = new HashMap<ItemStack, Float>();
                ItemStack dust = Items.getItem("copperDust");
                if(dust != null) {
                    dust = dust.cloneItemStack();
                    dust.count = 2;
                    output.put(dust, 1.0f);
                    RockCrusherCraftingManager.addRecipe(ore, output);
                }
            } else if(oreClass.equals("oreTin")) {
                HashMap<ItemStack, Float> output = new HashMap<ItemStack, Float>();
                ItemStack dust = Items.getItem("tinDust");
                if(dust != null) {
                    dust = dust.cloneItemStack();
                    dust.count = 2;
                    output.put(dust, 1.0f);
                    RockCrusherCraftingManager.addRecipe(ore, output);
                }
            } else if(oreClass.equals("oreSilver")) {
                HashMap<ItemStack, Float> output = new HashMap<ItemStack, Float>();
                ItemStack dust = Items.getItem("silverDust");
                if(dust != null) {
                    dust = dust.cloneItemStack();
                    dust.count = 2;
                    output.put(dust, 1.0f);
                    RockCrusherCraftingManager.addRecipe(ore, output);
                }
            }
        }
    }
}
