package railcraft.common.modules.orehandlers;

import java.util.logging.Level;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.ModLoader;
import forge.IOreHandler;
import railcraft.Railcraft;

public class CoreOreHandler implements IOreHandler
{

    @Override
    public void registerOre(String oreClass, ItemStack ore)
    {
        if(ore == null) {
            return;
        }
        if(oreClass.equals("ingotBronze")) {
            Railcraft.log(Level.FINER, "Core Module: Bronze detected, adding alternative minecart recipe: {0}, id={1}", new Object[]{oreClass, ore.id});

            ItemStack cart = new ItemStack(Item.MINECART);
            ModLoader.addRecipe(cart, new Object[]{
                    "I I",
                    "III",
                    Character.valueOf('I'), ore,});
        }
    }
}
