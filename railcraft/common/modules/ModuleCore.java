package railcraft.common.modules;

import forestry.api.fuels.EngineBronzeFuel;
import forestry.api.fuels.FuelManager;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import net.minecraft.server.Block;
import net.minecraft.server.CraftingManager;
import net.minecraft.server.EntityMinecart;
import net.minecraft.server.FurnaceRecipes;
import net.minecraft.server.CraftingRecipe;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.ModLoader;
import forge.MinecraftForge;
import forge.oredict.OreDictionary;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.ChunkManager;
import railcraft.common.CraftingHandler;
import railcraft.EntityFakeBlock;
import railcraft.common.CrowbarHandler;
import railcraft.common.FuelHandler;
import railcraft.common.RailcraftConfig;
import railcraft.common.RailcraftLanguage;
import railcraft.common.RailcraftPartItems;
import railcraft.common.RailcraftToolItems;
import railcraft.common.api.CartTools;
import railcraft.common.carts.EntityCartBasic;
import railcraft.common.carts.EntityCartChest;
import railcraft.common.carts.EntityCartFurnace;
import railcraft.common.carts.EnumCart;
import railcraft.common.carts.ItemCart;
import railcraft.common.carts.LinkageHandler;
import railcraft.common.carts.MinecartHooks;
import railcraft.common.util.liquids.LiquidItems;
import railcraft.common.util.network.RailcraftConnectionHandler;
import railcraft.common.modules.orehandlers.CoreOreHandler;
import railcraft.common.utility.EnumUtility;

public class ModuleCore extends RailcraftModule
{

    @Override
    public void doFirst()
    {
        MinecraftForge.registerConnectionHandler(new RailcraftConnectionHandler());

        MinecraftForge.setGuiHandler(Railcraft.getMod(), new GuiHandler());

        MinecraftForge.registerChunkLoadHandler(ChunkManager.getInstance().getHandler());

        MinecraftForge.registerFuelHandler(new FuelHandler());

        MinecraftForge.setDungeonLootTries(MinecraftForge.getDungeonLootTries() + 2);

        RailcraftToolItems.registerCrowbar();
        MinecraftForge.registerEntityInteractHandler(CrowbarHandler.getInstance());

        ModLoader.registerEntityID(EntityFakeBlock.class, "fakeBlock", RailcraftConfig.getEntityId("entity.fakeBlock"));
        MinecraftForge.registerEntity(EntityFakeBlock.class, Railcraft.getMod(), 51, 80, 3, true);

        MinecartHooks cartHooks = new MinecartHooks();
        MinecraftForge.registerMinecartHandler(cartHooks);
        MinecraftForge.registerMinecartHandler(LinkageHandler.getInstance());

        if(RailcraftConfig.useCollisionHandler()) {
            if(EntityMinecart.getCollisionHandler() != null) {
                Railcraft.log(Level.CONFIG, "Existing Minecart Collision Handler detected, overwriting. Please check your configs to ensure this is desired behavior.");
            }
            EntityMinecart.setCollisionHandler(cartHooks);
        }

        MinecraftForge.registerCraftingHandler(new CraftingHandler());


//        testSet.add(itemIdCartBasic);
//        testSet.add(itemIdCartChest);
//        testSet.add(itemIdCartSteam);
        if(!RailcraftConfig.useOldRecipes()) {
            Set<Integer> testSet = new HashSet<Integer>();
            testSet.add(Block.RAILS.id);
            testSet.add(Block.GOLDEN_RAIL.id);
            testSet.add(Block.DETECTOR_RAIL.id);

            Iterator it = CraftingManager.getInstance().getRecipies().iterator();
            while(it.hasNext()) {
                CraftingRecipe r = (CraftingRecipe)it.next();
                ItemStack output = r.b();
                if(testSet.contains(output.id)) {
                    it.remove();
                }
            }
        }

        // Items
        int itemIdOffset = Item.IRON_SPADE.id;
        int itemIdCartBasic = Item.MINECART.id;
        int itemIdCartChest = Item.STORAGE_MINECART.id;
        int itemIdCartSteam = Item.POWERED_MINECART.id;


        Item.byId[itemIdCartBasic] = null;
        Item.byId[itemIdCartChest] = null;
        Item.byId[itemIdCartSteam] = null;

        Item itemCartBasic = new ItemCart(itemIdCartBasic - itemIdOffset, EnumCart.BASIC);
        Item itemCartChest = new ItemCart(itemIdCartChest - itemIdOffset, EnumCart.CHEST);
        Item itemCartSteam = new ItemCart(itemIdCartSteam - itemIdOffset, EnumCart.FURNACE);

        MinecraftForge.removeMinecart(EntityMinecart.class, 0);
        MinecraftForge.removeMinecart(EntityMinecart.class, 1);
        MinecraftForge.removeMinecart(EntityMinecart.class, 2);

        MinecraftForge.registerMinecart(EntityCartBasic.class, new ItemStack(itemCartBasic));
        MinecraftForge.registerMinecart(EntityCartChest.class, new ItemStack(itemCartChest));
        MinecraftForge.registerMinecart(EntityCartFurnace.class, new ItemStack(itemCartSteam));

        String tag = "entity.cart.basic";
        int entityId = RailcraftConfig.getEntityId(tag);
        CartTools.registerMinecart(Railcraft.getMod(), EntityCartBasic.class, tag, entityId, EnumCart.BASIC.getId());

        tag = "entity.cart.chest";
        entityId = RailcraftConfig.getEntityId(tag);
        CartTools.registerMinecart(Railcraft.getMod(), EntityCartChest.class, tag, entityId, EnumCart.CHEST.getId());

        tag = "entity.cart.furnace";
        entityId = RailcraftConfig.getEntityId(tag);
        CartTools.registerMinecart(Railcraft.getMod(), EntityCartFurnace.class, tag, entityId, EnumCart.FURNACE.getId());

        MinecraftForge.setBlockHarvestLevel(Block.RAILS, "crowbar", 0);
        MinecraftForge.setBlockHarvestLevel(Block.RAILS, "pickaxe", 0);
        MinecraftForge.setBlockHarvestLevel(Block.GOLDEN_RAIL, "crowbar", 0);
        MinecraftForge.setBlockHarvestLevel(Block.GOLDEN_RAIL, "pickaxe", 0);
        MinecraftForge.setBlockHarvestLevel(Block.DETECTOR_RAIL, "crowbar", 0);
        MinecraftForge.setBlockHarvestLevel(Block.DETECTOR_RAIL, "pickaxe", 0);

        // Define Recipies

        // Old rails
        if(!RailcraftConfig.useOldRecipes()) {
            ItemStack stackRailNormal = new ItemStack(Block.RAILS, 16);
            ItemStack stackRailBooster = new ItemStack(Block.GOLDEN_RAIL, 8);
            ItemStack stackRailDetector = new ItemStack(Block.DETECTOR_RAIL, 8);

            ModLoader.addRecipe(stackRailNormal, new Object[]{
                    "I I",
                    "I#I",
                    "I I",
                    Character.valueOf('I'), RailcraftPartItems.getRailStandard(),
                    Character.valueOf('#'), RailcraftPartItems.getRailbedWood(),});
            ModLoader.addRecipe(stackRailBooster, new Object[]{
                    "I I",
                    "I#I",
                    "IrI",
                    Character.valueOf('I'), RailcraftPartItems.getRailAdvanced(),
                    Character.valueOf('#'), RailcraftPartItems.getRailbedWood(),
                    Character.valueOf('r'), Item.REDSTONE,});
            ModLoader.addRecipe(stackRailDetector, new Object[]{
                    "IsI",
                    "I#I",
                    "IrI",
                    Character.valueOf('I'), RailcraftPartItems.getRailStandard(),
                    Character.valueOf('#'), Block.STONE_PLATE,
                    Character.valueOf('r'), Item.REDSTONE,
                    Character.valueOf('s'), RailcraftPartItems.getRailbedWood(),});

            ModLoader.addShapelessRecipe(RailcraftPartItems.getRailStandard(2), new Object[]{
                    Block.RAILS,
                    Block.RAILS,
                    Block.RAILS,
                    Block.RAILS,
                    Block.RAILS,
                    Block.RAILS,
                    Block.RAILS,
                    Block.RAILS
                });

            RailcraftLanguage.getInstance().registerItemName(stackRailNormal, "track.standard");
            RailcraftLanguage.getInstance().registerItemName(stackRailBooster, "track.powered");
            RailcraftLanguage.getInstance().registerItemName(stackRailDetector, "track.detector");
        }


        // Old Carts
        ItemStack stackCartBasic = new ItemStack(itemCartBasic, 1);
        ItemStack stackCartChest = new ItemStack(itemCartChest, 1);
        ItemStack stackCartSteam = new ItemStack(itemCartSteam, 1);

//        ModLoader.addRecipe(stackCartBasic, new Object[]{
//                "I I",
//                "III",
//                Character.valueOf('I'), Item.ingotIron,});
//        ModLoader.addRecipe(stackCartChest, new Object[]{
//                "C",
//                "M",
//                Character.valueOf('C'), Block.chest,
//                Character.valueOf('M'), itemCartBasic,});
//        ModLoader.addRecipe(stackCartSteam, new Object[]{
//                "F",
//                "M",
//                Character.valueOf('F'), Block.stoneOvenIdle,
//                Character.valueOf('M'), itemCartBasic,});

        ModLoader.addShapelessRecipe(new ItemStack(Item.MINECART), new Object[]{
                itemCartChest,});
        ModLoader.addShapelessRecipe(new ItemStack(Item.MINECART), new Object[]{
                itemCartSteam,});

        RailcraftLanguage.getInstance().registerItemName(stackCartBasic, EnumCart.BASIC.getTag());
        RailcraftLanguage.getInstance().registerItemName(stackCartChest, EnumCart.CHEST.getTag());
        RailcraftLanguage.getInstance().registerItemName(stackCartSteam, EnumCart.FURNACE.getTag());

        MinecraftForge.addDungeonLoot(stackCartBasic, RailcraftConfig.getLootChance("cart.basic"));
        MinecraftForge.addDungeonLoot(stackCartChest, RailcraftConfig.getLootChance("cart.chest"));
        MinecraftForge.addDungeonLoot(new ItemStack(Block.RAILS), RailcraftConfig.getLootChance("rail"), 8, 32);

        if(RailcraftConfig.defineDictionaryRecipes()) {
            OreDictionary.registerOreHandler(new CoreOreHandler());
        }
    }

    @Override
    public void doSecond()
    {
        if(RailcraftConfig.useCreosoteFurnaceRecipes() || !EnumUtility.COKE_OVEN.isEnabled()) {
            FurnaceRecipes.getInstance().addSmelting(Item.COAL.id, 0, LiquidItems.getCreosoteOilBottle(2));
            FurnaceRecipes.getInstance().addSmelting(Item.COAL.id, 1, LiquidItems.getCreosoteOilBottle(1));
        }else{
            ItemStack creosoteOil = LiquidItems.getCreosoteOil();
            FuelManager.bronzeEngineFuel.put(creosoteOil.id, new EngineBronzeFuel(creosoteOil, 4, 15000, 1));
        }
    }
}
