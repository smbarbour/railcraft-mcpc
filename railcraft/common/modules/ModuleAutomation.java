package railcraft.common.modules;

import net.minecraft.server.Block;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.ModLoader;
import forge.MinecraftForge;
import forge.oredict.OreDictionary;
import railcraft.Railcraft;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftConfig;
import railcraft.common.modules.orehandlers.BoreOreHandler;
import railcraft.common.RailcraftLanguage;
import railcraft.common.RailcraftPartItems;
import railcraft.common.api.CartTools;
import railcraft.common.carts.EntityCartAnchor;
import railcraft.common.carts.EntityCartTrackRelayer;
import railcraft.common.carts.EntityCartUndercutter;
import railcraft.common.carts.EntityTunnelBore;
import railcraft.common.carts.EnumCart;
import railcraft.common.carts.ItemBoreHeadDiamond;
import railcraft.common.carts.ItemBoreHeadIron;
import railcraft.common.carts.ItemBoreHeadSteel;
import railcraft.common.carts.ItemCart;
import railcraft.common.carts.ItemTunnelBore;
import railcraft.common.cube.EnumCube;
import railcraft.common.detector.BlockDetector.Detector;
import railcraft.common.utility.EnumUtility;

public class ModuleAutomation extends RailcraftModule
{

    @Override
    public void doFirst()
    {
        RailcraftBlocks.registerBlockDetector();
        RailcraftBlocks.registerBlockCube();

        Block blockDetector = RailcraftBlocks.getBlockDetector();

        if(blockDetector != null) {
            ModLoader.addRecipe(new ItemStack(blockDetector, 1, Detector.STORAGE.getMeta()), new Object[]{
                    "XXX",
                    "XPX",
                    "XXX",
                    Character.valueOf('X'), Block.WOOD,
                    Character.valueOf('P'), Block.STONE_PLATE,});
            ModLoader.addRecipe(new ItemStack(blockDetector, 1, Detector.ANY.getMeta()), new Object[]{
                    "XXX",
                    "XPX",
                    "XXX",
                    Character.valueOf('X'), Block.STONE,
                    Character.valueOf('P'), Block.STONE_PLATE,});
            ModLoader.addRecipe(new ItemStack(blockDetector, 1, Detector.EMPTY.getMeta()), new Object[]{
                    "XXX",
                    "XPX",
                    "XXX",
                    Character.valueOf('X'), new ItemStack(Block.SMOOTH_BRICK, 1, 0),
                    Character.valueOf('P'), Block.STONE_PLATE,});
            ModLoader.addRecipe(new ItemStack(blockDetector, 1, Detector.MOB.getMeta()), new Object[]{
                    "XXX",
                    "XPX",
                    "XXX",
                    Character.valueOf('X'), new ItemStack(Block.SMOOTH_BRICK, 1, 1),
                    Character.valueOf('P'), Block.STONE_PLATE,});
            ModLoader.addRecipe(new ItemStack(blockDetector, 1, Detector.MOB.getMeta()), new Object[]{
                    "XXX",
                    "XPX",
                    "XXX",
                    Character.valueOf('X'), Block.MOSSY_COBBLESTONE,
                    Character.valueOf('P'), Block.STONE_PLATE,});
            ModLoader.addRecipe(new ItemStack(blockDetector, 1, Detector.POWERED.getMeta()), new Object[]{
                    "XXX",
                    "XPX",
                    "XXX",
                    Character.valueOf('X'), Block.COBBLESTONE,
                    Character.valueOf('P'), Block.STONE_PLATE,});
            ModLoader.addRecipe(new ItemStack(blockDetector, 1, Detector.PLAYER.getMeta()), new Object[]{
                    "XXX",
                    "XPX",
                    "XXX",
                    Character.valueOf('X'), new ItemStack(Block.STEP, 1, 0),
                    Character.valueOf('P'), Block.STONE_PLATE,});
            ModLoader.addRecipe(new ItemStack(blockDetector, 1, Detector.EXPLOSIVE.getMeta()), new Object[]{
                    "XXX",
                    "XPX",
                    "XXX",
                    Character.valueOf('X'), new ItemStack(Block.STEP, 1, 2),
                    Character.valueOf('P'), Block.STONE_PLATE,});
            ModLoader.addRecipe(new ItemStack(blockDetector, 1, Detector.ANIMAL.getMeta()), new Object[]{
                    "XXX",
                    "XPX",
                    "XXX",
                    Character.valueOf('X'), new ItemStack(Block.LOG, 1, 0),
                    Character.valueOf('P'), Block.STONE_PLATE,});
            ModLoader.addRecipe(new ItemStack(blockDetector, 1, Detector.GROWTH.getMeta()), new Object[]{
                    "XXX",
                    "XPX",
                    "XXX",
                    Character.valueOf('X'), new ItemStack(Block.LOG, 1, 1),
                    Character.valueOf('P'), Block.STONE_PLATE,});
            ModLoader.addRecipe(new ItemStack(blockDetector, 1, Detector.ADVANCED.getMeta()), new Object[]{
                    "XXX",
                    "XPX",
                    "XXX",
                    Character.valueOf('X'), Item.IRON_INGOT,
                    Character.valueOf('P'), Block.STONE_PLATE,});
            ModLoader.addRecipe(new ItemStack(blockDetector, 1, Detector.TANK.getMeta()), new Object[]{
                    "XXX",
                    "XPX",
                    "XXX",
                    Character.valueOf('X'), Item.CLAY_BRICK,
                    Character.valueOf('P'), Block.STONE_PLATE,});
            ModLoader.addRecipe(new ItemStack(blockDetector, 1, Detector.SHEEP.getMeta()), new Object[]{
                    "XXX",
                    "XPX",
                    "XXX",
                    Character.valueOf('X'), Block.WOOL,
                    Character.valueOf('P'), Block.STONE_PLATE,});
        }


        EnumUtility utility = EnumUtility.MINECART_DISPENSER;
        if(RailcraftConfig.isSubBlockEnabled(utility.getTag())) {
            RailcraftBlocks.registerBlockUtility();
            Block blockUtility = RailcraftBlocks.getBlockUtility();
            if(blockUtility != null) {
                ItemStack cartDispenser = utility.getItem(1);
                ModLoader.addRecipe(cartDispenser, new Object[]{
                        "ML",
                        Character.valueOf('M'), Item.MINECART,
                        Character.valueOf('L'), Block.DISPENSER,});
                RailcraftLanguage.getInstance().registerItemName(cartDispenser, utility.getTag());
            }
        }

        utility = EnumUtility.FEED_STATION;
        if(RailcraftConfig.isSubBlockEnabled(utility.getTag())) {
            RailcraftBlocks.registerBlockUtility();
            Block blockUtility = RailcraftBlocks.getBlockUtility();
            if(blockUtility != null) {
                MinecraftForge.setBlockHarvestLevel(blockUtility, utility.getId(), "axe", 1);
                ItemStack stack = utility.getItem(1);
                ModLoader.addRecipe(stack, new Object[]{
                        "PWP",
                        "PWP",
                        "PPP",
                        Character.valueOf('P'), Block.WOOD,
                        Character.valueOf('W'), Item.WHEAT,});
                RailcraftLanguage.getInstance().registerItemName(stack, utility.getTag());
            }
        }

        EnumCube type = EnumCube.WORLD_ANCHOR;
        if(RailcraftBlocks.getBlockCube() != null && RailcraftConfig.isSubBlockEnabled(type.getTag())) {
            ItemStack stack = type.getItem();
            RailcraftLanguage.getInstance().registerItemName(stack, type.getTag());
            ModLoader.addRecipe(stack, new Object[]{
                    "gog",
                    "dod",
                    "gog",
                    Character.valueOf('d'), Item.DIAMOND,
                    Character.valueOf('g'), Item.GOLD_INGOT,
                    Character.valueOf('o'), Block.OBSIDIAN,});
        }

        // Define Bore
        int entityId = RailcraftConfig.getEntityId("entity.cart.bore");

        if(entityId > 0) {
            CartTools.registerMinecart(Railcraft.getMod(), EntityTunnelBore.class, "TunnelBore", entityId, 50);

            int id = RailcraftConfig.getItemId("item.cart.bore");

            if(id > 0) {
                Item itemCartBore = new ItemTunnelBore(id);
                RailcraftLanguage.getInstance().registerItemName(itemCartBore, "entity.cart.bore");
                MinecraftForge.registerMinecart(EntityTunnelBore.class, new ItemStack(itemCartBore));

                ItemStack metal = null;
                if(RailcraftModuleManager.isModuleLoaded(RailcraftModuleManager.Module.FACTORY)
                    && RailcraftBlocks.getBlockCube() != null
                    && RailcraftConfig.isSubBlockEnabled(EnumCube.STEEL_BLOCK.getTag())) {
                    metal = new ItemStack(RailcraftBlocks.getBlockCube().id, 1, EnumCube.STEEL_BLOCK.getId());
                } else {
                    metal = new ItemStack(Block.IRON_BLOCK);
                }

                ModLoader.addRecipe(new ItemStack(itemCartBore), new Object[]{
                        "ICI",
                        "FCF",
                        " S ",
                        Character.valueOf('I'), metal,
                        Character.valueOf('S'), Item.STORAGE_MINECART,
                        Character.valueOf('F'), Block.FURNACE,
                        Character.valueOf('C'), Item.MINECART
                    });

                id = RailcraftConfig.getItemId("cart.bore.head.diamond");
                if(id > 0) {
                    Item item = new ItemBoreHeadDiamond(id);
                    RailcraftLanguage.getInstance().registerItemName(item, "item.bore.head.diamond");
                    ModLoader.addRecipe(new ItemStack(item), new Object[]{
                            "III",
                            "IDI",
                            "III",
                            Character.valueOf('I'), RailcraftPartItems.getIngotSteel(),
                            Character.valueOf('D'), Block.DIAMOND_BLOCK,});
                }

                id = RailcraftConfig.getItemId("cart.bore.head.steel");
                if(id > 0 && metal.id != Block.IRON_BLOCK.id) {
                    Item item = new ItemBoreHeadSteel(id);
                    RailcraftLanguage.getInstance().registerItemName(item, "item.bore.head.steel");
                    ModLoader.addRecipe(new ItemStack(item), new Object[]{
                            "III",
                            "IDI",
                            "III",
                            Character.valueOf('I'), RailcraftPartItems.getIngotSteel(),
                            Character.valueOf('D'), metal,});
                }

                id = RailcraftConfig.getItemId("cart.bore.head.iron");
                if(id > 0) {
                    Item item = new ItemBoreHeadIron(id);
                    RailcraftLanguage.getInstance().registerItemName(item, "item.bore.head.iron");
                    ModLoader.addRecipe(new ItemStack(item), new Object[]{
                            "III",
                            "IDI",
                            "III",
                            Character.valueOf('I'), RailcraftPartItems.getIngotSteel(),
                            Character.valueOf('D'), Block.IRON_BLOCK,});
                }

                OreDictionary.registerOreHandler(new BoreOreHandler());
            }
        }

        // Define Anchor Cart

        EnumCart cart = EnumCart.ANCHOR;
        String tag = cart.getTag();
        entityId = RailcraftConfig.getEntityId(tag);

        if(entityId > 0) {
            CartTools.registerMinecart(Railcraft.getMod(), EntityCartAnchor.class, "Anchorcart", entityId, cart.getId());

            int itemId = RailcraftConfig.getItemId(cart.getItemTag());

            Item anchorCart = new ItemCart(itemId, cart);
            ItemStack stack = new ItemStack(anchorCart);
            ItemStack anchor = EnumCube.WORLD_ANCHOR.getItem();
            ModLoader.addRecipe(stack, new Object[]{
                    "A",
                    "M",
                    Character.valueOf('A'), anchor,
                    Character.valueOf('M'), Item.MINECART
                });
            ModLoader.addShapelessRecipe(new ItemStack(Item.MINECART), new Object[]{
                    stack,});
            RailcraftLanguage.getInstance().registerItemName(stack, tag);

            MinecraftForge.registerMinecart(EntityCartAnchor.class, stack);

            EnumCart.ANCHOR.setContents(anchor);
        }

        // Define Track Relayer Cart

        cart = EnumCart.TRACK_RELAYER;
        tag = cart.getTag();
        entityId = RailcraftConfig.getEntityId(tag);

        if(entityId > 0) {
            CartTools.registerMinecart(Railcraft.getMod(), EntityCartTrackRelayer.class, "TrackRelayer", entityId, cart.getId());

            int itemId = RailcraftConfig.getItemId(cart.getItemTag());

            Item item = new ItemCart(itemId, cart);
            ItemStack stack = new ItemStack(item);
            ItemStack steel = EnumCube.STEEL_BLOCK.isEnabled() ? EnumCube.STEEL_BLOCK.getItem() : new ItemStack(Block.IRON_BLOCK);
            ModLoader.addRecipe(stack, new Object[]{
                    "YLY",
                    "RSR",
                    "DMD",
                    Character.valueOf('L'), new ItemStack(Block.REDSTONE_LAMP_OFF),
                    Character.valueOf('Y'), new ItemStack(Item.INK_SACK, 1, 11),
                    Character.valueOf('R'), new ItemStack(Item.BLAZE_ROD),
                    Character.valueOf('D'), new ItemStack(Item.DIAMOND_PICKAXE),
                    Character.valueOf('S'), steel,
                    Character.valueOf('M'),  new ItemStack(Item.MINECART)
                });
            RailcraftLanguage.getInstance().registerItemName(stack, tag);

            MinecraftForge.registerMinecart(EntityCartTrackRelayer.class, stack);
        }

        // Define Undercutter Cart

        cart = EnumCart.UNDERCUTTER;
        tag = cart.getTag();
        entityId = RailcraftConfig.getEntityId(tag);

        if(entityId > 0) {
            CartTools.registerMinecart(Railcraft.getMod(), EntityCartUndercutter.class, "Undercutter", entityId, cart.getId());

            int itemId = RailcraftConfig.getItemId(cart.getItemTag());

            Item item = new ItemCart(itemId, cart);
            ItemStack stack = new ItemStack(item);
            ItemStack steel = EnumCube.STEEL_BLOCK.isEnabled() ? EnumCube.STEEL_BLOCK.getItem() : new ItemStack(Block.IRON_BLOCK);
            ModLoader.addRecipe(stack, new Object[]{
                    "YLY",
                    "RSR",
                    "DMD",
                    Character.valueOf('L'), new ItemStack(Block.REDSTONE_LAMP_OFF),
                    Character.valueOf('Y'), new ItemStack(Item.INK_SACK, 1, 11),
                    Character.valueOf('R'), new ItemStack(Block.PISTON),
                    Character.valueOf('D'), new ItemStack(Item.DIAMOND_SPADE),
                    Character.valueOf('S'), steel,
                    Character.valueOf('M'),  new ItemStack(Item.MINECART)
                });
            RailcraftLanguage.getInstance().registerItemName(stack, tag);

            MinecraftForge.registerMinecart(EntityCartUndercutter.class, stack);
        }
    }

    @Override
    public void doSecond()
    {
    }
}
