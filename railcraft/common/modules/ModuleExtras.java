package railcraft.common.modules;

import net.minecraft.server.Block;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.ModLoader;
import forge.MinecraftForge;
import railcraft.Railcraft;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftConfig;
import railcraft.common.RailcraftLanguage;
import railcraft.common.RailcraftPartItems;
import railcraft.common.RailcraftRails;
import railcraft.common.api.CartTools;
import railcraft.common.carts.EntityCartTNT;
import railcraft.common.carts.EntityCartWork;
import railcraft.common.carts.EnumCart;
import railcraft.common.carts.ItemCart;
import railcraft.common.tracks.EnumTrack;

public class ModuleExtras extends RailcraftModule
{

    @Override
    public void doFirst()
    {
        RailcraftBlocks.registerBlockTrack();
        RailcraftBlocks.registerBlockRailElevator();

        Block blockTrack = RailcraftBlocks.getBlockTrack();

        ItemStack railStack = RailcraftPartItems.getRailStandard();

        // Define TNT Cart
        int entityId = RailcraftConfig.getEntityId("entity.cart.tnt");

        if(entityId > 0) {
            CartTools.registerMinecart(Railcraft.getMod(), EntityCartTNT.class, "TNTcart", entityId, EnumCart.TNT.getId());

            int itemIdCartTNT = RailcraftConfig.getItemId("item.cart.tnt");

            Item item = new ItemCart(itemIdCartTNT, EnumCart.TNT);
            ItemStack stack = new ItemStack(item);
            ModLoader.addRecipe(stack, new Object[]{
                    "T",
                    "M",
                    Character.valueOf('T'), Block.TNT,
                    Character.valueOf('M'), Item.MINECART
                });
            ModLoader.addShapelessRecipe(new ItemStack(Item.MINECART), new Object[]{
                    item,});
            RailcraftLanguage.getInstance().registerItemName(stack, EnumCart.TNT.getTag());

            MinecraftForge.registerMinecart(EntityCartTNT.class, stack);
            MinecraftForge.addDungeonLoot(stack.cloneItemStack(), RailcraftConfig.getLootChance("cart.tnt"));
        }

        // Define Work Cart
        entityId = RailcraftConfig.getEntityId("entity.cart.work");

        if(entityId > 0) {
            EnumCart cart = EnumCart.WORK;
            CartTools.registerMinecart(Railcraft.getMod(), EntityCartWork.class, "Workcart", entityId, cart.getId());

            int itemId = RailcraftConfig.getItemId("item.cart.work");

            Item item = new ItemCart(itemId, cart);
            ItemStack stack = new ItemStack(item);
            ModLoader.addRecipe(stack, new Object[]{
                    "B",
                    "M",
                    Character.valueOf('B'), Block.WORKBENCH,
                    Character.valueOf('M'), Item.MINECART
                });
            ModLoader.addShapelessRecipe(new ItemStack(Item.MINECART), new Object[]{
                    item,});
            RailcraftLanguage.getInstance().registerItemName(stack, cart.getTag());

            MinecraftForge.registerMinecart(EntityCartWork.class, stack);
            MinecraftForge.addDungeonLoot(stack.cloneItemStack(), RailcraftConfig.getLootChance("cart.work"));
        }

        if(blockTrack != null) {
            RailcraftRails.registerRail(EnumTrack.PRIMING);
            RailcraftRails.registerRail(EnumTrack.LAUNCHER);
            RailcraftRails.registerRail(EnumTrack.SUSPENDED);
        }
    }

    @Override
    public void doSecond()
    {
    }
}
