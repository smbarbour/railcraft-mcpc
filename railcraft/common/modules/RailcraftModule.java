package railcraft.common.modules;

public abstract class RailcraftModule
{

    public void doFirst()
    {
    }

    public void doSecond()
    {
    }

    public void doLast()
    {
    }

    public boolean canModuleLoad()
    {
        return true;
    }

    public void printLoadError()
    {
    }
}
