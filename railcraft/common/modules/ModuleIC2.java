package railcraft.common.modules;

import java.util.logging.Level;
import net.minecraft.server.Block;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.ModLoader;
import forge.MinecraftForge;
import forge.oredict.OreDictionary;
import ic2.api.Items;
import railcraft.Railcraft;
import railcraft.common.modules.orehandlers.IC2OreHandler;
import railcraft.common.ItemRailcraft;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftConfig;
import railcraft.common.RailcraftLanguage;
import railcraft.common.api.CartTools;
import railcraft.common.carts.EntityCartEnergy;
import railcraft.common.carts.EnumCart;
import railcraft.common.carts.ItemCart;
import railcraft.common.detector.BlockDetector.Detector;
import railcraft.common.utility.EnumUtility;
import railcraft.common.utility.TileEnergyLoader;
import railcraft.common.utility.TileEnergyUnloader;

public class ModuleIC2 extends RailcraftModule
{

    private static boolean initialized = false;
    private static boolean integrated = false;
    private static Item lapotronUpgrade;
    private static Item creosoteWood;

    @Override
    public boolean canModuleLoad()
    {
        return RailcraftModuleManager.isIC2Loaded();
    }

    @Override
    public void printLoadError()
    {
        Railcraft.log(Level.FINE, "Railcraft IC2 Module: IC2 not detected, skipping Railcraft IC2 Module.");
    }

    @Override
    public void doFirst()
    {
        if(initialized || !canModuleLoad()) {
            return;
        }

        RailcraftBlocks.registerBlockDetector();
        RailcraftBlocks.registerBlockUtility();

        EnumUtility type = EnumUtility.ENERGY_LOADER;
        if(type.isEnabled()) {
            ModLoader.registerTileEntity(TileEnergyLoader.class, "RCLoaderTileEnergy");

            ItemStack stack = type.getItem();
            RailcraftLanguage.getInstance().registerItemName(stack, type.getTag());
        }

        type = EnumUtility.ENERGY_UNLOADER;
        if(type.isEnabled()) {
            ModLoader.registerTileEntity(TileEnergyUnloader.class, "RCUnloaderTileEnergy");

            ItemStack stack = type.getItem();
            RailcraftLanguage.getInstance().registerItemName(stack, type.getTag());
        }

        int id = RailcraftConfig.getItemId("item.ic2.upgrade.lapotron");
        if(id > 0) {
            lapotronUpgrade = new ItemRailcraft(id).a("lapotronUpgrade").d(183).e(9);

            RailcraftLanguage.getInstance().registerItemName(lapotronUpgrade, "item.ic2.upgrade.lapotron");
        }

        int entityId = RailcraftConfig.getEntityId("entity.cart.energy");
        if(entityId > 0) {
            CartTools.registerMinecart(Railcraft.getMod(), EntityCartEnergy.class, "EnergyCart", entityId, EnumCart.BATBOX.getId());

            id = RailcraftConfig.getItemId("item.cart.batbox");
            if(id > 0) {
                Item item = new ItemCart(id, EnumCart.BATBOX);
                ItemStack stack = new ItemStack(item);
                MinecraftForge.registerMinecart(EntityCartEnergy.class, 0, stack);
                RailcraftLanguage.getInstance().registerItemName(stack, EnumCart.BATBOX.getTag());
            }

            id = RailcraftConfig.getItemId("item.cart.mfe");
            if(id > 0) {
                Item item = new ItemCart(id, EnumCart.MFE);
                ItemStack stack = new ItemStack(item);
                MinecraftForge.registerMinecart(EntityCartEnergy.class, 1, stack);
                RailcraftLanguage.getInstance().registerItemName(stack, EnumCart.MFE.getTag());
            }

            id = RailcraftConfig.getItemId("item.cart.mfsu");
            if(id > 0) {
                Item item = new ItemCart(id, EnumCart.MFSU);
                ItemStack stack = new ItemStack(item);
                MinecraftForge.registerMinecart(EntityCartEnergy.class, 2, stack);
                RailcraftLanguage.getInstance().registerItemName(stack, EnumCart.MFSU.getTag());
            }
        }

//        id = RailcraftConfig.getItemId("item.creosote.wood");
//        if(id > 0){
//            creosoteWood = new ItemRailcraft(id).setItemName("creosoteWood").setIconIndex(184);
//            ItemStack wood = new ItemStack(creosoteWood);
//            RailcraftLanguage.getInstance().registerItemName(creosoteWood, "Creosote Wood");
//
//            ItemStack oil = RailcraftPartItems.getCreosoteOil(2);
//            Ic2Recipes.addExtractorRecipe(wood, oil);
//
//            CropCard bush = new CreosoteBush(wood);
//
//            if(!CropCard.registerCrop(bush, 156)){
//                CropCard.registerCrop(bush);
//            }
//        }

        OreDictionary.registerOreHandler(new IC2OreHandler());

        initialized = true;
    }

    public static void integrateWithIC2()
    {
        if(!integrated && initialized) {
            ItemStack mfeStack = Items.getItem("mfeUnit");

            if(mfeStack != null) {
                Railcraft.log(Level.FINE, "IC2 Module: IC2 Detected, creating Energy Cart and Energy Loader recipes.");

                createRecipes();
            } else {
                Railcraft.log(Level.FINE, "IC2 Module: IC2 not detected, skipping Energy Cart and Energy Loader recipes.");
            }

            integrated = true;
        }
    }

    private static void createRecipes()
    {
        Block blockDetector = RailcraftBlocks.getBlockDetector();

        ItemStack batbox = Items.getItem("batBox");
        if(batbox != null) {
            EnumCart.BATBOX.setContents(batbox);
            ItemStack cart = MinecraftForge.getItemForCart(EntityCartEnergy.class, 0);
            ModLoader.addRecipe(cart, new Object[]{
                    "E",
                    "M",
                    Character.valueOf('E'), batbox,
                    Character.valueOf('M'), Item.MINECART
                });
            ModLoader.addShapelessRecipe(new ItemStack(Item.MINECART), new Object[]{cart});
        }

        ItemStack mfe = Items.getItem("mfeUnit");
        if(mfe != null) {
            EnumCart.MFE.setContents(mfe);
            ItemStack cart = MinecraftForge.getItemForCart(EntityCartEnergy.class, 1);
            ModLoader.addRecipe(cart, new Object[]{
                    "E",
                    "M",
                    Character.valueOf('E'), mfe,
                    Character.valueOf('M'), Item.MINECART
                });
            ModLoader.addShapelessRecipe(new ItemStack(Item.MINECART), new Object[]{cart});
        }

        ItemStack mfsu = Items.getItem("mfsUnit");
        if(mfsu != null) {
            EnumCart.MFSU.setContents(mfsu);
            ItemStack cart = MinecraftForge.getItemForCart(EntityCartEnergy.class, 2);
            ModLoader.addRecipe(cart, new Object[]{
                    "E",
                    "M",
                    Character.valueOf('E'), mfsu,
                    Character.valueOf('M'), Item.MINECART
                });
            ModLoader.addShapelessRecipe(new ItemStack(Item.MINECART), new Object[]{cart});
        }


        ItemStack battery = Items.getItem("reBattery");
        ItemStack machine = Items.getItem("machine");

        ItemStack detector = null;
        if(blockDetector != null) {
            detector = new ItemStack(blockDetector, 1, Detector.ENERGY.getMeta());
        } else {
            detector = new ItemStack(Block.STONE_PLATE);
        }

        if(battery != null && machine != null) {
            if(EnumUtility.ENERGY_LOADER.isEnabled()) {
                ModLoader.addRecipe(EnumUtility.ENERGY_LOADER.getItem(), new Object[]{
                        "BLB",
                        "BIB",
                        "BDB",
                        Character.valueOf('D'), detector,
                        Character.valueOf('B'), battery,
                        Character.valueOf('I'), machine,
                        Character.valueOf('L'), new ItemStack(Block.DISPENSER, 1, 0)});
            }

            if(EnumUtility.ENERGY_UNLOADER.isEnabled()) {
                ModLoader.addRecipe(EnumUtility.ENERGY_UNLOADER.getItem(), new Object[]{
                        "BDB",
                        "BIB",
                        "BLB",
                        Character.valueOf('D'), detector,
                        Character.valueOf('B'), battery,
                        Character.valueOf('I'), machine,
                        Character.valueOf('L'), new ItemStack(Block.DISPENSER, 1, 0),});
            }
        }

        int id = RailcraftConfig.getItemId("item.ic2.upgrade.lapotron");
        if(id > 0) {
            ItemStack lapotron = Items.getItem("lapotronCrystal");
            ItemStack glassCable = Items.getItem("glassFiberCableItem");
            ItemStack circuit = Items.getItem("advancedCircuit");

            if(lapotron != null && glassCable != null && circuit != null) {
                lapotron.cloneItemStack();
                lapotron.setData(-1);
                ModLoader.addRecipe(new ItemStack(lapotronUpgrade), new Object[]{
                        "GGG",
                        "wLw",
                        "GCG",
                        Character.valueOf('G'), new ItemStack(Block.GLASS, 1, 0),
                        Character.valueOf('w'), glassCable,
                        Character.valueOf('C'), circuit,
                        Character.valueOf('L'), lapotron,});
            }
        }
    }

    @Override
    public void doSecond()
    {
    }

    public static ItemStack getLapotronUpgrade()
    {
        if(lapotronUpgrade == null) {
            return null;
        }
        return new ItemStack(lapotronUpgrade);
    }
}
