package railcraft.common.modules;

import forestry.api.liquids.LiquidContainer;
import java.util.HashSet;
import java.util.Set;
import net.minecraft.server.Block;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.ModLoader;
import forge.MinecraftForge;
import railcraft.Railcraft;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftConfig;
import railcraft.common.RailcraftLanguage;
import railcraft.common.RailcraftPartItems;
import railcraft.common.api.CartTools;
import railcraft.common.carts.EntityCartTank;
import railcraft.common.carts.EnumCart;
import railcraft.common.carts.ItemCart;
import railcraft.common.detector.BlockDetector.Detector;
import railcraft.common.util.liquids.LiquidManager;
import railcraft.common.utility.EnumUtility;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class ModuleTransport extends RailcraftModule
{

    Item itemCartTank = null;

    @Override
    public void doFirst()
    {

        int entityId = RailcraftConfig.getEntityId("entity.cart.tank");
        if(entityId > 0) {
            CartTools.registerMinecart(Railcraft.getMod(), EntityCartTank.class, "Tankcart", entityId, EnumCart.TANK.getId());

            int id = RailcraftConfig.getItemId("item.cart.tank");
            if(id > 0) {
                itemCartTank = new ItemCart(id, EnumCart.TANK);
                ItemStack stackCartTank = new ItemStack(itemCartTank);
                MinecraftForge.registerMinecart(EntityCartTank.class, stackCartTank);
                Railcraft.registerTankCartItemRenderer(itemCartTank.id);
                RailcraftLanguage.getInstance().registerItemName(stackCartTank, EnumCart.TANK.getTag());
                ModLoader.addRecipe(stackCartTank, new Object[]{
                        "GGG",
                        "GMG",
                        "GGG",
                        Character.valueOf('G'), new ItemStack(Block.GLASS),
                        Character.valueOf('M'), Item.MINECART
                    });
                ModLoader.addShapelessRecipe(new ItemStack(Item.MINECART), new Object[]{
                        stackCartTank,});
            }
        }

        EnumUtility utility = EnumUtility.WATER_TANK;
        if(RailcraftConfig.isSubBlockEnabled(utility.getTag())) {
            RailcraftBlocks.registerBlockUtility();
            Block blockUtility = RailcraftBlocks.getBlockUtility();
            if(blockUtility != null) {
                ItemStack stack = utility.getItem();
                RailcraftLanguage.getInstance().registerItemName(stack, utility.getTag());
                ModLoader.addRecipe(utility.getItem(6), new Object[]{
                        "WWW",
                        "ISI",
                        "WWW",
                        Character.valueOf('I'), new ItemStack(Item.IRON_INGOT),
                        Character.valueOf('S'), new ItemStack(Item.SLIME_BALL),
                        Character.valueOf('W'), Block.WOOD});
            }
        }

        utility = EnumUtility.ITEM_LOADER;
        if(RailcraftConfig.isSubBlockEnabled(utility.getTag())) {
            RailcraftBlocks.registerBlockUtility();
            Block blockUtility = RailcraftBlocks.getBlockUtility();
            if(blockUtility != null) {
                ItemStack stack = utility.getItem(1);
                Block blockDetector = RailcraftBlocks.getBlockDetector();
                if(blockDetector != null) {
                    ModLoader.addRecipe(stack, new Object[]{
                            "L",
                            "D",
                            Character.valueOf('D'), new ItemStack(blockDetector, 1, Detector.STORAGE.getMeta()),
                            Character.valueOf('L'), Block.DISPENSER,});
                } else {
                    ModLoader.addRecipe(stack, new Object[]{
                            "L",
                            "D",
                            Character.valueOf('D'), Block.STONE_PLATE,
                            Character.valueOf('L'), Block.DISPENSER,});
                }
                RailcraftLanguage.getInstance().registerItemName(stack, utility.getTag());


                utility = EnumUtility.ITEM_LOADER_ADVANCED;
                if(RailcraftConfig.isSubBlockEnabled(utility.getTag())) {
                    ItemStack stackAdvanced = utility.getItem(1);
                    ModLoader.addRecipe(stackAdvanced, new Object[]{
                            "IRI",
                            "RLR",
                            "IDI",
                            Character.valueOf('I'), RailcraftPartItems.getIngotSteel(),
                            Character.valueOf('R'), Item.REDSTONE,
                            Character.valueOf('D'), Item.ENDER_PEARL,
                            Character.valueOf('L'), stack,});

                    RailcraftLanguage.getInstance().registerItemName(stackAdvanced, utility.getTag());
                }
            }
        }

        utility = EnumUtility.ITEM_UNLOADER;
        if(RailcraftConfig.isSubBlockEnabled(utility.getTag())) {
            RailcraftBlocks.registerBlockUtility();
            Block blockUtility = RailcraftBlocks.getBlockUtility();
            if(blockUtility != null) {
                ItemStack stack = utility.getItem(1);
                Block blockDetector = RailcraftBlocks.getBlockDetector();
                if(blockDetector != null) {
                    ModLoader.addRecipe(stack, new Object[]{
                            "D",
                            "L",
                            Character.valueOf('D'), new ItemStack(blockDetector, 1, Detector.STORAGE.getMeta()),
                            Character.valueOf('L'), Block.DISPENSER,});
                } else {
                    ModLoader.addRecipe(stack, new Object[]{
                            "D",
                            "L",
                            Character.valueOf('D'), Block.STONE_PLATE,
                            Character.valueOf('L'), Block.DISPENSER,});
                }
                RailcraftLanguage.getInstance().registerItemName(stack, utility.getTag());

                utility = EnumUtility.ITEM_UNLOADER_ADVANCED;
                if(RailcraftConfig.isSubBlockEnabled(utility.getTag())) {
                    ItemStack stackAdvanced = utility.getItem(1);
                    ModLoader.addRecipe(stackAdvanced, new Object[]{
                            "IRI",
                            "RLR",
                            "IDI",
                            Character.valueOf('I'), RailcraftPartItems.getIngotSteel(),
                            Character.valueOf('R'), Item.REDSTONE,
                            Character.valueOf('D'), Item.ENDER_PEARL,
                            Character.valueOf('L'), stack,});

                    RailcraftLanguage.getInstance().registerItemName(stackAdvanced, utility.getTag());
                }
            }
        }

        utility = EnumUtility.LIQUID_LOADER;
        if(RailcraftConfig.isSubBlockEnabled(utility.getTag())) {
            RailcraftBlocks.registerBlockUtility();
            Block blockUtility = RailcraftBlocks.getBlockUtility();
            if(blockUtility != null) {
                ItemStack stack = utility.getItem(1);
                Block blockDetector = RailcraftBlocks.getBlockDetector();
                ItemStack detector = null;
                if(blockDetector != null) {
                    detector = new ItemStack(blockDetector, 1, Detector.TANK.getMeta());
                } else {
                    detector = new ItemStack(Block.STONE_PLATE);
                }
                ModLoader.addRecipe(utility.getItem(), new Object[]{
                        "GLG",
                        "G G",
                        "GDG",
                        Character.valueOf('D'), detector,
                        Character.valueOf('G'), new ItemStack(Block.GLASS),
                        Character.valueOf('L'), Block.DISPENSER,});
                RailcraftLanguage.getInstance().registerItemName(stack, utility.getTag());
            }
        }

        utility = EnumUtility.LIQUID_UNLOADER;
        if(RailcraftConfig.isSubBlockEnabled(utility.getTag())) {
            RailcraftBlocks.registerBlockUtility();
            Block blockUtility = RailcraftBlocks.getBlockUtility();
            if(blockUtility != null) {
                ItemStack stack = utility.getItem(1);
                Block blockDetector = RailcraftBlocks.getBlockDetector();
                ItemStack detector = null;
                if(blockDetector != null) {
                    detector = new ItemStack(blockDetector, 1, Detector.TANK.getMeta());
                } else {
                    detector = new ItemStack(Block.STONE_PLATE);
                }
                ModLoader.addRecipe(utility.getItem(), new Object[]{
                        "GDG",
                        "G G",
                        "GLG",
                        Character.valueOf('D'), detector,
                        Character.valueOf('G'), new ItemStack(Block.GLASS),
                        Character.valueOf('L'), Block.DISPENSER,});
                RailcraftLanguage.getInstance().registerItemName(stack, utility.getTag());
            }
        }
    }

    @Override
    public void doLast()
    {
        if(itemCartTank != null) {
            ItemStack stackCartTank = new ItemStack(itemCartTank, 1, 0);

            Set<Integer> filtersDefined = new HashSet<Integer>();

            LiquidManager lm = LiquidManager.getInstance();
            for(LiquidContainer container : lm.getLiquidContainers()) {
                int filterId = lm.getLiquidFilterId(container.filled);
                if(filtersDefined.contains(filterId)){
                    continue;
                }

                ItemStack filteredCart = new ItemStack(itemCartTank, 1, filterId);
                ModLoader.addShapelessRecipe(filteredCart, new Object[]{stackCartTank, container.filled});
                ModLoader.addShapelessRecipe(stackCartTank, new Object[]{filteredCart});

                filtersDefined.add(filterId);
            }
        }
    }
}
