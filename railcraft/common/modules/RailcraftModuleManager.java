package railcraft.common.modules;

import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.ModContainer;
import java.io.File;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.minecraft.server.ItemStack;
import net.minecraft.server.ModLoader;
import forge.Configuration;
import forge.Property;
import railcraft.Railcraft;
import railcraft.common.EnumColor;
import railcraft.common.api.ItemRegistry;
import railcraft.common.cube.EnumCube;
import railcraft.common.structures.EnumStructure;
import railcraft.common.utility.EnumUtility;

public abstract class RailcraftModuleManager
{

    public static final String MODULE_CONFIG_FILE_NAME = "modules.cfg";
    public static final String CATEGORY_MODULES = "modules";

    public enum Module
    {

        CORE, FACTORY, EXTRAS, TRACKS, TRAIN, SIGNALS, STRUCTURES, AUTOMATION, TRANSPORT, IC2, TRACKS_HIGHSPEED, TRACKS_WOOD, TRACKS_REINFORCED
    };
    private static Map<Module, RailcraftModule> modules = new EnumMap<Module, RailcraftModule>(Module.class);
    private static Set<Module> loadedModules = EnumSet.noneOf(Module.class);

    static {
        registerModule(Module.CORE, new ModuleCore());
        registerModule(Module.AUTOMATION, new ModuleAutomation());
        registerModule(Module.SIGNALS, new ModuleSignals());
        registerModule(Module.STRUCTURES, new ModuleStructures());
        registerModule(Module.EXTRAS, new ModuleExtras());
        registerModule(Module.FACTORY, new ModuleFactory());
        registerModule(Module.TRANSPORT, new ModuleTransport());
        registerModule(Module.TRACKS, new ModuleRails());
        registerModule(Module.TRACKS_HIGHSPEED, new ModuleTracksHighSpeed());
        registerModule(Module.TRACKS_WOOD, new ModuleTracksWood());
        registerModule(Module.TRACKS_REINFORCED, new ModuleTracksReinforced());
        registerModule(Module.TRAIN, new ModuleTrain());
        registerModule(Module.IC2, new ModuleIC2());
    }

    public static void load()
    {
        Configuration config = new Configuration(new File(Railcraft.getConfigDir(), MODULE_CONFIG_FILE_NAME));

        config.categories.put(CATEGORY_MODULES, config.generalProperties);

        config.load();

        config.categories.remove(Configuration.CATEGORY_BLOCK);
        config.categories.remove(Configuration.CATEGORY_ITEM);
        config.categories.remove(Configuration.CATEGORY_GENERAL);

        for(Module m : Module.values()) {
            if(m == Module.CORE || isEnabled(config, m)) {
                RailcraftModule instance = modules.get(m);
                if(instance.canModuleLoad()) {
                    markModuleLoaded(m);
                } else {
                    instance.printLoadError();
                }
            }
        }
        config.save();

        for(Module m : loadedModules) {
            doFirst(m);
        }

        for(Module m : loadedModules) {
            doSecond(m);
        }

        // Finish initializing ItemRegistry

        for(EnumUtility type : EnumUtility.values()) {
            if(type.isEnabled()) {
                ItemRegistry.registerItem(type.getTag(), type.getItem());
            }
        }

        for(EnumStructure type : EnumStructure.values()) {
            if(type.isEnabled()) {
                switch (type) {
                    case METAL_POST:
                        ItemStack post = type.getItem();
                        for(EnumColor c : EnumColor.values()) {
                            ItemStack item = post.cloneItemStack();
                            item.setData(c.getId());
                            ItemRegistry.registerItem(type.getTag() + "." + c.getBasicTag(), item);
                        }
                        break;
                    case CONCRETE_BLOCK:
                    case CART_BALANCER:
                        break;
                    default:
                        ItemRegistry.registerItem(type.getTag(), type.getItem());
                }
            }
        }

        for(EnumCube type : EnumCube.values()) {
            if(type.isEnabled()) {
                ItemRegistry.registerItem(type.getTag(), type.getItem());
            }
        }

//        ItemRegistry.printItemTags();
    }

    public static void modsLoaded()
    {
        for(Module m : loadedModules) {
            doLast(m);
        }
    }

    private static boolean isEnabled(Configuration config, Module m)
    {
        boolean defaultValue = true;
        if(m == Module.TRAIN && Railcraft.gameIsServer()) {
            defaultValue = false;
        }
        Property prop = config.getOrCreateBooleanProperty(m.toString().toLowerCase().replace('_', '.'), CATEGORY_MODULES, defaultValue);
        return Boolean.valueOf(prop.value);
    }

    private static void markModuleLoaded(Module module)
    {
        loadedModules.add(module);
    }

    public static boolean isModuleLoaded(Module module)
    {
        return loadedModules.contains(module);
    }

    private static void registerModule(Module module, RailcraftModule instance)
    {
        modules.put(module, instance);
    }

    private static void doFirst(Module module)
    {
        RailcraftModule instance = modules.get(module);
        if(instance != null) {
            instance.doFirst();
        }
    }

    private static void doSecond(Module module)
    {
        RailcraftModule instance = modules.get(module);
        if(instance != null) {
            instance.doSecond();
        }
    }

    private static void doLast(Module module)
    {
        RailcraftModule instance = modules.get(module);
        if(instance != null) {
            instance.doLast();
        }
    }

    public static boolean isIC2Loaded()
    {
        List mods = ModLoader.getLoadedMods();
        for(Object o : mods) {
            if(o.getClass().getSimpleName().equals("mod_IC2")) {
                return true;
            }
        }
        return false;
    }

    public static boolean isBuildcraft3Loaded()
    {
        List<ModContainer> mods = Loader.getModList();
        for(ModContainer mod : mods) {
            if(mod.getName().equals("mod_BuildCraftCore")) {
                return mod.getVersion().startsWith("3.");
            }
        }
        return false;
    }
}
