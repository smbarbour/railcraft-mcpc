package railcraft.common.modules;

import net.minecraft.server.Block;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftRails;
import railcraft.common.tracks.BlockFenceReplacement;
import railcraft.common.tracks.EnumTrack;

public class ModuleRails extends RailcraftModule
{

    @Override
    public void doFirst()
    {
        RailcraftBlocks.registerBlockTrack();

        if(RailcraftBlocks.getBlockTrack() != null) {
            RailcraftRails.registerRail(EnumTrack.BOARDING);
            RailcraftRails.registerRail(EnumTrack.HOLDING);
            RailcraftRails.registerRail(EnumTrack.ONEWAY);
            RailcraftRails.registerRail(EnumTrack.CONTROL);
            RailcraftRails.registerRail(EnumTrack.JUNCTION);
            RailcraftRails.registerRail(EnumTrack.DISEMBARK);
            RailcraftRails.registerRail(EnumTrack.GATED);
            RailcraftRails.registerRail(EnumTrack.GATED_ONEWAY);

            Block.byId[Block.FENCE.id] = null;
            Block fence = new BlockFenceReplacement(85, 4);
        }
    }

    @Override
    public void doSecond()
    {
    }
}
