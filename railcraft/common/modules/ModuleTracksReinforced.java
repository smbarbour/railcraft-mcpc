package railcraft.common.modules;

import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftRails;
import railcraft.common.tracks.EnumTrack;

public class ModuleTracksReinforced extends RailcraftModule
{

    @Override
    public void doFirst()
    {
        RailcraftBlocks.registerBlockTrack();

        if(RailcraftBlocks.getBlockTrack() != null) {
            RailcraftRails.registerRail(EnumTrack.REINFORCED);
            RailcraftRails.registerRail(EnumTrack.REINFORCED_BOOSTER);
            RailcraftRails.registerRail(EnumTrack.REINFORCED_JUNCTION);
            RailcraftRails.registerRail(EnumTrack.REINFORCED_SWITCH);
        }
    }

    @Override
    public void doSecond()
    {
    }
}
