package railcraft.common;

import java.util.HashMap;
import java.util.Map;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityMinecart;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.ItemStack;
import forge.IEntityInteractHandler;
import railcraft.Railcraft;
import railcraft.common.api.ICrowbar;
import railcraft.common.api.ILinkableCart;
import railcraft.common.carts.LinkageManager;
import railcraft.common.modules.RailcraftModuleManager;
import railcraft.common.modules.RailcraftModuleManager.Module;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class CrowbarHandler implements IEntityInteractHandler
{

    private static Map<EntityHuman, EntityMinecart> linkMap = new HashMap<EntityHuman, EntityMinecart>();
    private static CrowbarHandler instance;

    public static IEntityInteractHandler getInstance()
    {
        if(instance == null) {
            instance = new CrowbarHandler();
        }
        return instance;
    }

    @Override
    public boolean onEntityInteract(EntityHuman player, Entity entity, boolean isAttack)
    {
        if(isAttack) {
            return true;
        }
        if(Railcraft.gameIsNotHost()) {
            return true;
        }
        if(!RailcraftModuleManager.isModuleLoaded(Module.TRAIN)) {
            return true;
        }
        boolean used = false;
        ItemStack stack = player.U();
        if(stack != null && stack.getItem() instanceof ICrowbar) {
            if(entity instanceof EntityMinecart) {
                EntityMinecart cart = (EntityMinecart)entity;
                boolean linkable = cart instanceof ILinkableCart;
                if(!linkable || (linkable && ((ILinkableCart)cart).isLinkable())) {
                    if(linkMap.containsKey(player)) {
                        LinkageManager lm = LinkageManager.getInstance();
                        EntityMinecart last = linkMap.remove(player);
                        if(lm.areLinked(cart, last)) {
                            lm.breakLink(cart, last);
                            used = true;
                            player.a("Link Broken");
                        } else {
                            used = lm.createLink((EntityMinecart)entity, (EntityMinecart)last);
                            if(used) {
                                player.a("Link Created");
                            }
                        }
                        if(!used) {
                            player.a("Link Failed");
                        }
                    } else {
                        linkMap.put(player, (EntityMinecart)entity);
                        player.a("Link Started");
                    }
                }
            }
        }
        if(used) {
            stack.damage(1, player);
        }
        return !used;
    }
}
