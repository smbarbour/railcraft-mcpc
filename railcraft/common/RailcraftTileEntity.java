package railcraft.common;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.Packet;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import railcraft.common.api.INetworkEntity;
import railcraft.common.util.network.PacketTileEntity;
import railcraft.common.util.network.RailcraftPacket;

public abstract class RailcraftTileEntity extends TileEntity implements INetworkEntity
{
	@Override
	public Packet d()
    {
//        System.out.println("Sending Tile Packet");
        RailcraftPacket packet = new PacketTileEntity(this);
        return packet.getPacket();
    }

    public void writePacketData(DataOutputStream data) throws IOException
    {
    }

    public void readPacketData(DataInputStream data) throws IOException
    {
    }

    public World getWorld()
    {
        return world;
    }

    public abstract int getId();
}
