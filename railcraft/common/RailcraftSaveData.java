package railcraft.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import net.minecraft.server.NBTCompressedStreamTools;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import railcraft.Railcraft;

public class RailcraftSaveData
{

    private static boolean hasChanged;
    private static File saveDir;
    private static File dataFile;
    private static NBTTagCompound data;

    public static NBTTagCompound getData(World world)
    {
        if(world == null) {
            return null;
        }
        if(!Railcraft.getWorldSaveDir(world).equals(saveDir)) {
            closeData();
        }
        if(data == null) {
            loadData(world);
        }
        return data;
    }

    public static void markHasChanged()
    {
        hasChanged = true;
    }

    private static File getDataFile(World world)
    {
        if(dataFile == null) {
            saveDir = Railcraft.getWorldSaveDir(world);
            dataFile = new File(saveDir, "railcraft.dat");
        }
        return dataFile;
    }

    private static void loadData(World world)
    {
        try {
            File file = getDataFile(world);
            if(file.exists()) {
                data = NBTCompressedStreamTools.a(new FileInputStream(file));
            } else {
                data = new NBTTagCompound();
            }
        } catch (IOException ex) {
            Railcraft.log(Level.WARNING, "Error loading Railcraft Save Data: " + ex.getMessage());
        }
    }

    public static void saveData()
    {
        if(data == null || dataFile == null || !hasChanged) {
            return;
        }

        try {
            if(!dataFile.exists()) {
                dataFile.createNewFile();
                Railcraft.log(Level.INFO, "Creating Railcraft Save File");
            }
            NBTCompressedStreamTools.a(data, new FileOutputStream(dataFile));
            hasChanged = false;
        } catch (IOException ex) {
            Railcraft.log(Level.WARNING, "Error saving Railcraft Save Data: " + ex.getMessage());
        }
    }

    public static void closeData()
    {
        saveData();
        saveDir = null;
        dataFile = null;
        data = null;
    }
}
