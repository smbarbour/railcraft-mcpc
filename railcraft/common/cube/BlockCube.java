package railcraft.common.cube;

import java.util.ArrayList;
import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Material;
import net.minecraft.server.World;
import forge.ISpecialResistance;
import forge.ITextureProvider;

public class BlockCube extends Block implements ITextureProvider, ISpecialResistance
{

    public BlockCube(int id)
    {
        super(id, Material.STONE);
        a("blockRailcraftCube");
        b(20);
        c(5);
        a(true);
    }

    @Override
    public String getTextureFile()
    {
        return "/railcraft/textures/railcraft.png";
    }

    @Override
    protected int getDropData(int meta)
    {
        return meta;
    }

    @Override
    public int a(int side, int meta)
    {
        return EnumCube.fromId(meta).getBlock().getBlockTextureFromSide(side);
    }

    public static String getBlockNameFromMetadata(int meta)
    {
        return EnumCube.fromId(meta).getTag();
    }

    @Override
    public void a(World world, int i, int j, int k, Random rand)
    {
        int meta = world.getData(i, j, k);

        EnumCube.fromId(meta).getBlock().a(world, i, j, k, rand);
    }

    @Override
    public void postPlace(World world, int i, int j, int k, int side)
    {
        int meta = world.getData(i, j, k);

        EnumCube.fromId(meta).getBlock().postPlace(world, i, j, k, side);
    }

    public void randomDisplayTick(World world, int i, int j, int k, Random rand)
    {
        int meta = world.getData(i, j, k);

        EnumCube.fromId(meta).getBlock().randomDisplayTick(world, i, j, k, rand);
    }

    @Override
    public void onPlace(World world, int i, int j, int k)
    {
        int meta = world.getData(i, j, k);

        EnumCube.fromId(meta).getBlock().onPlace(world, i, j, k);
    }

    @Override
    public void doPhysics(World world, int i, int j, int k, int id)
    {
        int meta = world.getData(i, j, k);

        EnumCube.fromId(meta).getBlock().doPhysics(world, i, j, k, id);
    }

    @Override
    public void remove(World world, int i, int j, int k)
    {
        int meta = world.getData(i, j, k);

        EnumCube.fromId(meta).getBlock().remove(world, i, j, k);
    }

    @Override
    public boolean removeBlockByPlayer(World world, EntityHuman player, int i, int j, int k)
    {
        int meta = world.getData(i, j, k);

        return EnumCube.fromId(meta).getBlock().removeBlockByPlayer(world, player, i, j, k);
    }

    @Override
    public void addCreativeItems(ArrayList itemList)
    {
        for(EnumCube type : EnumCube.getCreativeList()) {
            if(type.isEnabled()) {
                itemList.add(type.getItem());
            }
        }
    }

    @Override
    public float getSpecialExplosionResistance(World world, int i, int j, int k, double srcX, double srcY, double srcZ, Entity exploder)
    {
        int meta = world.getData(i, j, k);
        return EnumCube.fromId(meta).getBlock().getResistance();
    }
}
