package railcraft.common.cube;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockSand;
import net.minecraft.server.Material;
import net.minecraft.server.World;
import railcraft.common.RailcraftBlocks;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class CrushedObsidian extends CubeClass
{

    private static final int[] TEXTURE = {227};

    @Override
    public int[] getTexture()
    {
        return TEXTURE;
    }

    @Override
    public float getResistance()
    {
        return 500;
    }

    @Override
    public void onPlace(World world, int i, int j, int k)
    {
        world.c(i, j, k, RailcraftBlocks.getBlockCube().id, this.tickRate());
    }

    @Override
    public void doPhysics(World world, int i, int j, int k, int id)
    {
        world.c(i, j, k, RailcraftBlocks.getBlockCube().id, this.tickRate());
    }

    @Override
    public void a(World world, int i, int j, int k, Random rand)
    {
        this.tryToFall(world, i, j, k);
    }

    /**
     * How many world ticks before ticking
     */
    public int tickRate()
    {
        return 3;
    }

    /**
     * If there is space to fall below will start this block falling
     */
    private void tryToFall(World par1World, int i, int j, int k)
    {
        if(canFallBelow(par1World, i, j - 1, k) && j >= 0) {
            byte var8 = 32;

            if(!BlockSand.instaFall && par1World.a(i - var8, j - var8, k - var8, i + var8, j + var8, k + var8)) {
                if(!par1World.isStatic) {
                    EntityFallingCube var9 = new EntityFallingCube(par1World, (double)((float)i + 0.5F), (double)((float)j + 0.5F), (double)((float)k + 0.5F), RailcraftBlocks.getBlockCube().id, EnumCube.CRUSHED_OBSIDIAN.getId());
                    par1World.addEntity(var9);
                }
            } else {
                par1World.setTypeId(i, j, k, 0);

                while(canFallBelow(par1World, i, j - 1, k) && j > 0) {
                    --j;
                }

                if(j > 0) {
                    par1World.setTypeIdAndData(i, j, k, RailcraftBlocks.getBlockCube().id, EnumCube.CRUSHED_OBSIDIAN.getId());
                }
            }
        }
    }

    /**
     * Checks to see if the sand can fall into the block below it
     */
    public static boolean canFallBelow(World par0World, int par1, int par2, int par3)
    {
        int var4 = par0World.getTypeId(par1, par2, par3);

        if(var4 == 0) {
            return true;
        } else if(var4 == Block.FIRE.id) {
            return true;
        } else {
            Material var5 = Block.byId[var4].material;
            return var5 == Material.WATER ? true : var5 == Material.LAVA;
        }
    }
}
