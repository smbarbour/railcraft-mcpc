package railcraft.common.cube;

import net.minecraft.server.ItemBlock;
import net.minecraft.server.ItemStack;

public class ItemCube extends ItemBlock
{

    public ItemCube(int id)
    {
        super(id);
        setMaxDurability(0);
        a(true);
    }

    public int getIconFromDamage(int meta)
    {
        return EnumCube.fromId(meta).getBlock().getBlockTextureFromSide(2);
    }

    @Override
    public int filterData(int meta)
    {
        return meta;
    }

    @Override
    public String a(ItemStack stack)
    {
        return EnumCube.fromId(stack.getData()).getTag();
    }
}
