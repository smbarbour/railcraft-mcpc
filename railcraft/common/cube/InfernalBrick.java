package railcraft.common.cube;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class InfernalBrick extends CubeClass
{

    private static final int[] TEXTURE = {215};

    @Override
    public int[] getTexture()
    {
        return TEXTURE;
    }
}
