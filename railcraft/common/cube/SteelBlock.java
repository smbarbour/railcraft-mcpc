package railcraft.common.cube;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class SteelBlock extends CubeClass
{

    private static final int[] TEXTURE = {180};

    @Override
    public int[] getTexture()
    {
        return TEXTURE;
    }
}
