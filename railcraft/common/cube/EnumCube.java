package railcraft.common.cube;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.ItemStack;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftConfig;
import railcraft.common.modules.RailcraftModuleManager;
import railcraft.common.modules.RailcraftModuleManager.Module;

/**
 *
 * @author CovertJaguar
 */
public enum EnumCube
{

    WORLD_ANCHOR(Module.AUTOMATION, "World Anchor", "world.anchor", new WorldAnchor()),
    CONCRETE_BLOCK(Module.STRUCTURES, "Block of Concrete", "concrete.block", new ConcreteBlock()),
    STEEL_BLOCK(Module.FACTORY, "Block of Steel", "steel.block", new SteelBlock()),
    INFERNAL_BRICK(Module.STRUCTURES, "Infernal Brick", "brick.infernal", new InfernalBrick()),
    CRUSHED_OBSIDIAN(Module.FACTORY, "Crushed Obsidian", "crushed.obsidian", new CrushedObsidian()),;
//    SANDY_BRICK(Module.STRUCTURES, "Sandy Brick", "brick.sandy", new SandyBrick()),
//    BANDED_PLANKS(Module.STRUCTURES, "Banded Planks", "planks.banded", new BandedPlanks());
    private final Module module;
    private final byte id;
    private final String name;
    private final String tag;
    private final CubeClass block;
    private static byte nextId;
    private static final List<EnumCube> creativeList = new ArrayList<EnumCube>();

    static {
        creativeList.add(WORLD_ANCHOR);
        creativeList.add(STEEL_BLOCK);
        creativeList.add(CONCRETE_BLOCK);
//        creativeList.add(SANDY_BRICK);
        creativeList.add(INFERNAL_BRICK);
        creativeList.add(CRUSHED_OBSIDIAN);
//        creativeList.add(BANDED_PLANKS);
    }

    private EnumCube(Module module, String name, String tag, CubeClass block)
    {
        this.module = module;
        id = getNextId();
        this.name = name;
        this.tag = tag;
        this.block = block;
    }

    public static List<EnumCube> getCreativeList()
    {
        return creativeList;
    }

    public Module getModule()
    {
        return module;
    }

    public String getName()
    {
        return name;
    }

    public String getTag()
    {
        return "cube." + tag;
    }

    public CubeClass getBlock()
    {
        return block;
    }

    public byte getId()
    {
        return id;
    }

    public boolean isEnabled()
    {
        return RailcraftModuleManager.isModuleLoaded(getModule()) && RailcraftConfig.isSubBlockEnabled(getTag()) && RailcraftBlocks.getBlockCube() != null;
    }

    public static EnumCube fromId(int id)
    {
        for(EnumCube c : values()) {
            if(c.getId() == id) {
                return c;
            }
        }
        return WORLD_ANCHOR;
    }

    private static byte getNextId()
    {
        byte i = nextId;
        nextId++;
        return i;
    }

    public ItemStack getItem()
    {
        return getItem(1);
    }

    public ItemStack getItem(int qty)
    {
        return new ItemStack(RailcraftBlocks.getBlockCube(), qty, id);
    }
}
