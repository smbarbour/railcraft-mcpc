package railcraft.common.cube;

public class ConcreteBlock extends CubeClass
{

    private static final int[] TEXTURE = new int[]{103};

    @Override
    public int[] getTexture()
    {
        return TEXTURE;
    }
}
