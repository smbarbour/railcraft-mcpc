package railcraft.common.cube;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.Block;
import net.minecraft.server.BlockSand;
import net.minecraft.server.Entity;
import net.minecraft.server.ItemStack;
import net.minecraft.server.MathHelper;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import forge.ISpawnHandler;

public class EntityFallingCube extends Entity implements ISpawnHandler
{

    public int blockID;
    public int metadata;
    /** How long the block has been falling for. */
    public int fallTime = 0;

    public EntityFallingCube(World world)
    {
        super(world);
        this.bf = true;
        this.b(0.98F, 0.98F);
        this.height = this.height / 2.0F;
        this.motX = 0.0D;
        this.motY = 0.0D;
        this.motZ = 0.0D;
    }

    public EntityFallingCube(World world, double i, double j, double k, int blockID, int metadata)
    {
        super(world);
        this.blockID = blockID;
        this.metadata = metadata;
        this.bf = true;
        this.b(0.98F, 0.98F);
        this.height = this.height / 2.0F;
        this.setPosition(i, j, k);
        this.motX = 0.0D;
        this.motY = 0.0D;
        this.motZ = 0.0D;
        this.lastX = i;
        this.lastY = j;
        this.lastZ = k;
    }

    /**
     * returns if this entity triggers Block.onEntityWalking on the blocks they walk on. used for spiders and wolves to
     * prevent them from trampling crops
     */
    protected boolean canTriggerWalking()
    {
        return false;
    }

    @Override
    protected void b()
    {
    }

    /**
     * Returns true if other Entities should be prevented from moving through this Entity.
     */
    @Override
    public boolean o_()
    {
        return !this.dead;
    }

    /**
     * Called to update the entity's position/logic.
     */
    @Override
    public void F_()
    {
        if(this.blockID == 0) {
            this.die();
        } else {
            this.lastX = this.locX;
            this.lastY = this.locY;
            this.lastZ = this.locZ;
            ++this.fallTime;
            this.motY -= 0.03999999910593033D;
            this.move(this.motX, this.motY, this.motZ);
            this.motX *= 0.9800000190734863D;
            this.motY *= 0.9800000190734863D;
            this.motZ *= 0.9800000190734863D;
            int var1 = MathHelper.floor(this.locX);
            int var2 = MathHelper.floor(this.locY);
            int var3 = MathHelper.floor(this.locZ);

            if(this.fallTime == 1 && this.world.getTypeId(var1, var2, var3) == this.blockID) {
                this.world.setTypeId(var1, var2, var3, 0);
            } else if(!this.world.isStatic && this.fallTime == 1) {
                this.die();
            }

            if(this.onGround) {
                this.motX *= 0.699999988079071D;
                this.motZ *= 0.699999988079071D;
                this.motY *= -0.5D;

                if(this.world.getTypeId(var1, var2, var3) != Block.PISTON_MOVING.id) {
                    this.die();

                    if((!this.world.mayPlace(this.blockID, var1, var2, var3, true, 1) || BlockSand.canFall(this.world, var1, var2 - 1, var3) || !this.world.setTypeIdAndData(var1, var2, var3, this.blockID, this.metadata)) && !this.world.isStatic) {
                        this.a(new ItemStack(blockID, 1, metadata), 0);
                    }
                }
            } else if(this.fallTime > 100 && !this.world.isStatic && (var2 < 1 || var2 > 256) || this.fallTime > 600) {
                this.a(new ItemStack(blockID, 1, metadata), 0);
                this.die();
            }
        }
    }

    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    @Override
    protected void b(NBTTagCompound data)
    {
        data.setInt("blockID", this.blockID);
        data.setInt("metadata", this.metadata);
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    @Override
    protected void a(NBTTagCompound data)
    {
        this.blockID = data.getInt("blockID");
        this.metadata = data.getInt("metadata");
    }

    public float getShadowSize()
    {
        return 0.0F;
    }

    public World getWorld()
    {
        return this.world;
    }

    @Override
    public void writeSpawnData(DataOutputStream data) throws IOException
    {
        data.writeInt(blockID);
        data.writeInt(metadata);
    }

    @Override
    public void readSpawnData(DataInputStream data) throws IOException
    {
        blockID = data.readInt();
        metadata = data.readInt();
    }
}
