package railcraft.common.cube;

import java.util.Random;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.World;
import railcraft.common.RailcraftBlocks;

public abstract class CubeClass
{

    public int getBlockTextureFromSide(int side)
    {
        int[] texture = getTexture();
        if(side >= 0 && side < texture.length) {
            return texture[side];
        }
        return texture[0];
    }

    public void a(World world, int i, int j, int k, Random rand)
    {
    }

    public void doPhysics(World world, int i, int j, int k, int id)
    {
    }

    public void postPlace(World world, int i, int j, int k, int side)
    {
    }

    public void randomDisplayTick(World world, int i, int j, int k, Random rand)
    {
    }

    public void onPlace(World world, int i, int j, int k)
    {
    }

    public void remove(World world, int i, int j, int k)
    {
    }

    public boolean removeBlockByPlayer(World world, EntityHuman player, int i, int j, int k)
    {
        return world.setTypeId(i, j, k, 0);
    }

    public float getResistance()
    {
        return 20;
    }

    public int getBlockId()
    {
        return RailcraftBlocks.getBlockCube().id;
    }

    public abstract int[] getTexture();
}
