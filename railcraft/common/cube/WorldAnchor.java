package railcraft.common.cube;

import java.util.Random;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.World;
import railcraft.common.ChunkManager;

public class WorldAnchor extends CubeClass
{

    private static final byte ANCHOR_RADIUS = 1;
    private static final int[] TEXTURE = new int[]{185, 185, 201, 201, 201, 201};

    public WorldAnchor()
    {
    }

    @Override
    public int[] getTexture()
    {
        return TEXTURE;
    }

    @Override
    public void a(World world, int i, int j, int k, Random rand)
    {
        register(world, i, j, k);
    }

    @Override
    public void randomDisplayTick(World world, int i, int j, int k, Random rand)
    {
        register(world, i, j, k);
    }

    @Override
    public void postPlace(World world, int i, int j, int k, int side)
    {
        register(world, i, j, k);
    }

    @Override
    public void onPlace(World world, int i, int j, int k)
    {
        register(world, i, j, k);
    }

    @Override
    public void remove(World world, int i, int j, int k)
    {
        unregister(world, i, j, k);
    }

    public boolean removeBlockByPlayer(World world, EntityHuman player, int i, int j, int k)
    {
        unregister(world, i, j, k);
        return super.removeBlockByPlayer(world, player, i, j, k);
    }

    private void register(World world, int i, int j, int k)
    {
        ChunkManager.getInstance().registerAnchor(world, i, j, k, ANCHOR_RADIUS);
        world.c(i, j, k, getBlockId(), 600);
    }

    private void unregister(World world, int i, int j, int k)
    {
        ChunkManager.getInstance().unregisterAnchor(world, i, j, k);
    }
}
