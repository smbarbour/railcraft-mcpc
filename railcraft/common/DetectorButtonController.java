package railcraft.common;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class DetectorButtonController
{

    public enum ButtonMode
    {

        DISABLED(0, 0), CAPACITY(0, 4), RIDER(4, 5);
        public final byte sequenceStart;
        public final byte sequenceLength;

        private ButtonMode(int sequenceStart, int sequenceLength)
        {
            this.sequenceStart = (byte)sequenceStart;
            this.sequenceLength = (byte)sequenceLength;
        }
    }
    private ButtonMode mode = ButtonMode.DISABLED;
    private byte state = 0;

    public DetectorButtonController()
    {
    }

    public DetectorButtonController(ButtonMode mode)
    {
        this.mode = mode;
    }

    public void setMode(ButtonMode m)
    {
        this.mode = m;
        this.state = 0;
    }

    public ButtonMode getMode()
    {
        return mode;
    }

    public int incrementState()
    {
        state++;
        if(state >= mode.sequenceLength) {
            state = 0;
        }
        return state;
    }

    public void setState(int s)
    {
        if(s > mode.sequenceLength) {
            s = 0;
        }
        this.state = (byte)s;
    }

    public byte getState()
    {
        return state;
    }
}
