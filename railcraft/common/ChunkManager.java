package railcraft.common;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import net.minecraft.server.Chunk;
import net.minecraft.server.ChunkCoordIntPair;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityAnimal;
import net.minecraft.server.EntityItem;
import net.minecraft.server.EntityMinecart;
import net.minecraft.server.EntityMonster;
import net.minecraft.server.MathHelper;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.World;
import forge.IChunkLoadHandler;
import railcraft.Railcraft;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.WorldCoordinate;
import railcraft.common.carts.EntityCartAnchor;
import railcraft.common.cube.EnumCube;

/**
 * The ChunkManager is used to register anchor points
 * that work to keeps the chunks around them loaded.
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class ChunkManager
{

    /**
     * Default radius of chunks to keep loaded.
     */
    public static final byte DEFAULT_WINDOW_RADIUS = 1;
    /**
     * Max radius of chunks to keep loaded.
     */
    public static final byte MAX_WINDOW_RADIUS = 4;
    private static ChunkManager instance;
    private ChunkHandler handler;
    private World activeWorld;
    private Map<WorldCoordinate, Byte> anchors = new HashMap<WorldCoordinate, Byte>();
    private Map<Entity, Byte> entities = new HashMap<Entity, Byte>();
    private int update;
    private boolean init;
    private boolean displayChunks = false;
    private static final int PARTICLES = 250;

    /**
     * Returns an instance of ChunkManager
     * @return ChunkManager
     */
    public static ChunkManager getInstance()
    {
        if(instance == null) {
            instance = new ChunkManager();
        }
        return instance;
    }

    /**
     * Returns the IChunkLoadHandler.
     * @return IChunkLoadHandler
     */
    public IChunkLoadHandler getHandler()
    {
        if(handler == null) {
            handler = new ChunkHandler();
        }
        return handler;
    }

    private void printDebug(int level, String m, Object... args)
    {
        if(RailcraftConfig.printAnchorDebug() >= level) {
            Railcraft.log(Level.FINEST, m, args);
        }
    }

    private void printEntityRemoved(Entity e)
    {
        int x = MathHelper.floor(e.locX) >> 4;
        int z = MathHelper.floor(e.locZ) >> 4;
        printDebug(1, "Anchor Cart removed from <{0},{1},{2}> in chunk <{3}, {4}>", (int)e.locX, (int)e.locY, (int)e.locZ, x, z);
    }

    private ChunkManager()
    {
    }

    /**
     * @see IChunkLoadHander
     */
    private class ChunkHandler implements IChunkLoadHandler
    {

        private ChunkHandler()
        {
        }

        @Override
        public void addActiveChunks(World world, Set<ChunkCoordIntPair> chunkList)
        {
            if(Railcraft.gameIsNotHost()) {
                return;
            }

            for(Map.Entry<WorldCoordinate, Byte> anchor : anchors.entrySet()) {
                int x = anchor.getKey().x >> 4;
                int z = anchor.getKey().z >> 4;

                chunkList.addAll(getChunksAround(world, x, z, anchor.getValue()));
            }

            Iterator<Map.Entry<Entity, Byte>> it = entities.entrySet().iterator();
            while(it.hasNext()) {
                Map.Entry<Entity, Byte> anchor = it.next();
                Entity entity = anchor.getKey();
                if(entity.dead) {
                    it.remove();
                    printEntityRemoved(entity);
                    continue;
                }

                int x = MathHelper.floor(entity.locX) >> 4;
                int z = MathHelper.floor(entity.locZ) >> 4;

                chunkList.addAll(getChunksAround(world, x, z, anchor.getValue()));

            }
        }

        @Override
        public boolean canUnloadChunk(Chunk chunk)
        {
            if(Railcraft.gameIsNotHost()) {
                return true;
            }

            boolean canUnload = true;

            for(Map.Entry<WorldCoordinate, Byte> anchor : anchors.entrySet()) {
                if(!canUnloadChunk(chunk, anchor.getKey(), anchor.getValue())) {
                    canUnload = false;
                    break;
                }
            }

            Iterator<Map.Entry<Entity, Byte>> it = entities.entrySet().iterator();
            while(it.hasNext()) {
                Map.Entry<Entity, Byte> anchor = it.next();
                Entity entity = anchor.getKey();
                if(entity.dead) {
                    it.remove();
                    printEntityRemoved(entity);
                    continue;
                }
                if(!canUnloadChunk(chunk, entity, anchor.getValue())) {
                    canUnload = false;
                    break;
                }
            }

            if(!canUnload) {
                printDebug(2, "Chunk <{0}, {1}> prevented from unloading", chunk.x, chunk.z);
                return false;
            }

            return true;
        }

        private boolean canUnloadChunk(Chunk chunk, WorldCoordinate block, int radius)
        {
            int x = block.x >> 4;
            int z = block.z >> 4;

            if(chunk.world.worldProvider.dimension == block.dimension
                && Math.abs(x - chunk.x) <= radius && Math.abs(z - chunk.z) <= radius) {
                return false;
            }
            return true;
        }

        private boolean canUnloadChunk(Chunk chunk, Entity entity, int radius)
        {
            int x = MathHelper.floor(entity.locX) >> 4;
            int z = MathHelper.floor(entity.locZ) >> 4;

            if(chunk.world.worldProvider.dimension == entity.world.worldProvider.dimension
                && Math.abs(x - chunk.x) <= radius && Math.abs(z - chunk.z) <= radius) {
                return false;
            }
            return true;
        }

        @Override
        public boolean canUpdateEntity(Entity entity)
        {
            if(Railcraft.gameIsNotHost()) {
                return false;
            }

            if(entity instanceof EntityAnimal
                || entity instanceof EntityMinecart
                || entity instanceof EntityMonster
                || entity instanceof EntityItem) {
                if(entity.world != null) {
                    int x = MathHelper.floor(entity.locX);
                    int z = MathHelper.floor(entity.locZ);
                    byte r = 2;
                    boolean canUpdate = entity.world.a(x - r, 0, z - r, x + r, 0, z + r);
                    if(canUpdate) {
                        if(entity instanceof EntityMinecart) {
                            printDebug(4, "Entity updated: {0}", entity.getClass().getSimpleName());
                        }
                        return true;
                    }
                }
            }
            return false;
        }
    }

    /**
     * Checks for valid radius.
     * @param radius
     * @return
     */
    private byte validateWindow(int radius)
    {
        if(radius < 0) {
            radius = 0;
        }
        return (byte)Math.min(MAX_WINDOW_RADIUS, radius);
    }

    /**
     * Returns a Set of ChunkCoordIntPair containing the chunks around point [x, z].
     * Coordinates are in chunk coordinates, not world coordinates.
     * @param world World
     * @param x Chunk x-Coord
     * @param z Chunk z-Coord
     * @param radius Distance from [x, z] to include.
     * @return A set of chunks.
     */
    protected Set<ChunkCoordIntPair> getChunksAround(World world, int x, int z, int radius)
    {
        radius = validateWindow(radius);
        Set<ChunkCoordIntPair> chunkList = new HashSet<ChunkCoordIntPair>();
        for(int xx = x - radius; xx <= x + radius; xx++) {
            for(int zz = z - radius; zz <= z + radius; zz++) {
                chunkList.add(new ChunkCoordIntPair(xx, zz));
            }
        }
        return chunkList;
    }

    /**
     * Loads the chunks around point [x. z], with radius.
     * Coordinates are in chunk coordinates, not world coordinates.
     * @param world World
     * @param x Chunk x-Coord
     * @param z Chunk z-Coord
     * @param radius Distance from [x, z] to load.
     */
    public void loadChunksAround(World world, int x, int z, int radius)
    {
        for(ChunkCoordIntPair chunk : getChunksAround(world, x, z, radius)) {
            loadChunk(world, chunk.x, chunk.z);
        }
    }

    /**
     * Loads a Set of ChunkCoordIntPairs.
     * @param world
     * @param chunks
     */
    public void loadChunks(World world, Set<ChunkCoordIntPair> chunks)
    {
        for(ChunkCoordIntPair chunk : chunks) {
            loadChunk(world, chunk.x, chunk.z);
        }
    }

    /**
     * Loads a single chunk. Chunk coordinates are used, not world coordinates.
     * @param world World
     * @param x Chunk x-Coord
     * @param z Chunk z-Coord
     */
    private void loadChunk(World world, int x, int z)
    {
        if(!world.q().isChunkLoaded(x, z)) {
            printDebug(2, "Force loading chunk <{0}, {1}>", x, z);
        }
        world.q().getChunkAt(x, z);
    }

    /**
     * Checks that all the chunks around point [x. z], with radius are loaded.
     * Coordinates are in chunk coordinates, not world coordinates.
     * @param world World
     * @param x Chunk x-Coord
     * @param z Chunk z-Coord
     * @param radius Distance from [x, z] to check.
     */
    public boolean areChunksAroundLoaded(World world, int x, int z, int radius)
    {
        for(ChunkCoordIntPair chunk : getChunksAround(world, x, z, radius)) {
            if(!world.q().isChunkLoaded(chunk.x, chunk.z)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Registers a world coordinate as an anchor point that will keep the chunk
     * that coordinate is in loaded.
     * @param world World
     * @param i World x-Coord
     * @param j World y-Coord
     * @param k World z-Coord
     */
    public void registerAnchor(World world, int i, int j, int k)
    {
        registerAnchor(world, i, j, k, DEFAULT_WINDOW_RADIUS);
    }

    /**
     * Registers a world coordinate as an anchor point with a radius (in chunks)
     * that will keep the chunks around it loaded.
     * @param world World
     * @param i World x-Coord
     * @param j World y-Coord
     * @param k World z-Coord
     * @param radius Radius (in chunks) to keep loaded
     */
    public void registerAnchor(World world, int i, int j, int k, int radius)
    {
        byte r = validateWindow(radius);
        Byte ret = anchors.put(new WorldCoordinate(world.worldProvider.dimension, i, j, k), Byte.valueOf(r));
        if(ret == null) {
            printDebug(1, "World Anchor found at <{0},{1},{2}> in chunk <{3}, {4}>", i, j, k, i >> 4, k >> 4);
        }
    }

    /**
     * Unregisters an anchor point.
     * @param world World
     * @param i World x-Coord
     * @param j World y-Coord
     * @param k World z-Coord
     */
    public void unregisterAnchor(World world, int i, int j, int k)
    {
        Byte ret = anchors.remove(new WorldCoordinate(world.worldProvider.dimension, i, j, k));
        if(ret != null) {
            printDebug(1, "World Anchor removed from <{0},{1},{2}> in chunk <{3}, {4}>", i, j, k, i >> 4, k >> 4);
        }
    }

    /**
     * Registers an entity as an anchor point
     * that will keep the chunk it is in loaded.
     * @param entity The entity
     */
    public void registerAnchor(Entity entity)
    {
        registerAnchor(entity, DEFAULT_WINDOW_RADIUS);
    }

    /**
     * Registers an entity as an anchor point with a radius (in chunks)
     * that will keep the chunks around it loaded.
     * @param entity The entity
     * @param radius Radius (in chunks) to keep loaded
     */
    public void registerAnchor(Entity entity, int radius)
    {
        byte r = validateWindow(radius);
        Byte ret = entities.put(entity, Byte.valueOf(r));
        if(ret == null) {
            int x = MathHelper.floor(entity.locX) >> 4;
            int z = MathHelper.floor(entity.locZ) >> 4;
            printDebug(1, "Anchor Cart found at <{0},{1},{2}> in chunk <{3}, {4}>", (int)entity.locX, (int)entity.locY, (int)entity.locZ, x, z);
        }
    }

    /**
     * Unregisters an Entity as an anchor point.
     * @param entity The Entity
     */
    public void unregisterAnchor(Entity entity)
    {
        Byte ret = entities.remove(entity);
        if(ret != null) {
            printEntityRemoved(entity);
        }
    }

    /**
     * Saves Anchor points to railcraft.dat in the world folder
     */
    private void saveAnchors()
    {
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        World mainWorld = Railcraft.getWorld(0);
        if(!RailcraftConfig.loadAnchorsOnStart() || mainWorld == null) {
            return;
        }

        NBTTagCompound data = RailcraftSaveData.getData(mainWorld);

        if(data == null) {
            return;
        }

        NBTTagList blockData = new NBTTagList();

        int count = 0;

        for(WorldCoordinate wc : anchors.keySet()) {
            NBTTagCompound entry = new NBTTagCompound();
            entry.setInt("dim", wc.dimension);
            entry.setInt("x", wc.x);
            entry.setInt("y", wc.y);
            entry.setInt("z", wc.z);
            blockData.add(entry);
            count++;
        }

        NBTTagList entityData = new NBTTagList();
        for(Entity e : entities.keySet()) {
            NBTTagCompound entry = new NBTTagCompound();
            entry.setInt("dim", e.world.worldProvider.dimension);
            int x = MathHelper.floor(e.locX) >> 4;
            int z = MathHelper.floor(e.locZ) >> 4;
            entry.setInt("x", x);
            entry.setInt("z", z);
            entityData.add(entry);
            count++;
        }

        NBTTagCompound chunkData = new NBTTagCompound();
        chunkData.set("blockData", blockData);
        chunkData.set("entityData", entityData);

        data.set("chunkData", chunkData);

        RailcraftSaveData.markHasChanged();

        printDebug(3, "Saving {0} Anchors", count);
    }

    /**
     * Loads Anchors point from the railcraft.dat in the world folder
     */
    private void loadAnchors()
    {

        World mainWorld = Railcraft.getWorld(0);
        if(mainWorld == null) {
            return;
        }
        init = true;
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        if(!RailcraftConfig.loadAnchorsOnStart()) {
            return;
        }

        NBTTagCompound data = RailcraftSaveData.getData(mainWorld);

        if(data == null) {
            return;
        }

        NBTTagCompound chunkData = data.getCompound("chunkData");

        if(chunkData == null) {
            return;
        }

        int count = 0;

        NBTTagList blockData = chunkData.getList("blockData");

        for(int i = 0; i < blockData.size(); i++) {
            NBTTagCompound entry = (NBTTagCompound)blockData.get(i);
            int dimension = entry.getInt("dim");

            World world = Railcraft.getWorld(dimension);
            if(world != null) {
                int x = entry.getInt("x");
                int y = entry.getInt("y");
                int z = entry.getInt("z");
                loadChunk(world, x >> 4, z >> 4);
                if(world.getTypeId(x, y, z) == RailcraftBlocks.getBlockCube().id
                    && world.getData(x, y, z) == EnumCube.WORLD_ANCHOR.getId()) {
                    registerAnchor(world, x, y, z);
                    count++;
                }
                loadChunksAround(world, x >> 4, z >> 4, 1);
            }
        }

        NBTTagList entityData = chunkData.getList("entityData");

        for(int i = 0; i < entityData.size(); i++) {
            NBTTagCompound entry = (NBTTagCompound)entityData.get(i);
            int dimension = entry.getInt("dim");

            World world = Railcraft.getWorld(dimension);
            if(world != null) {
                int x = entry.getInt("x");
                int z = entry.getInt("z");
                Set<ChunkCoordIntPair> chunks = getChunksAround(world, x, z, 2);
                loadChunks(world, chunks);
                for(ChunkCoordIntPair c : chunks) {
                    Chunk chunk = world.getChunkAt(c.x, c.z);
                    if(chunk != null) {
                        for(List el : chunk.entitySlices) {
                            for(Entity e : (List<Entity>)el) {
                                if(e instanceof EntityCartAnchor) {
                                    registerAnchor(e);
                                }
                            }
                        }
                    }
                }
                count++;
            }
        }

        printDebug(-1, "Loaded {0} saved Anchors", count);
    }

    /**
     * Must be called once every tick.
     */
    public void updateTick()
    {
        update++;

        if(Railcraft.gameIsClient()) {
            World world = Railcraft.getWorld();
            if(activeWorld != world) {
                if(activeWorld != null) {
                    saveAnchors();
                }
                anchors.clear();
                entities.clear();
                activeWorld = world;
                init = false;
                printDebug(1, "New world detected, resetting Chunk Manager.");

                return;
            }

            if(displayChunks && world != null) {
                Random rand = GeneralTools.getRand();
                for(Map.Entry<WorldCoordinate, Byte> entry : anchors.entrySet()) {
                    if(entry.getKey().dimension != world.worldProvider.dimension){
                        continue;
                    }
                    double xCorner = entry.getKey().x >> 4;
                    xCorner -= entry.getValue();
                    xCorner *= 16;

                    double zCorner = entry.getKey().z >> 4;
                    zCorner -= entry.getValue();
                    zCorner *= 16;

                    for(int i = 0; i < PARTICLES; i++) {
                        double y = rand.nextDouble() * world.getHeight();
                        double x = xCorner;
                        double z = zCorner;
                        x += rand.nextDouble() * (entry.getValue() * 32 + 16);
                        z += rand.nextDouble() * (entry.getValue() * 32 + 16);

                        world.a("portal", x, y, z, 0.0D, 0.0D, 0.0D);
                    }
                }

                for(Map.Entry<Entity, Byte> entry : entities.entrySet()) {
                    if(world.worldProvider.dimension != entry.getKey().world.worldProvider.dimension){
                        continue;
                    }
                    double xCorner = MathHelper.floor(entry.getKey().locX) >> 4;
                    xCorner -= entry.getValue();
                    xCorner *= 16;

                    double zCorner = MathHelper.floor(entry.getKey().locZ) >> 4;
                    zCorner -= entry.getValue();
                    zCorner *= 16;

                    for(int i = 0; i < PARTICLES; i++) {
                        double y = rand.nextDouble() * world.getHeight();
                        double x = xCorner;
                        double z = zCorner;
                        x += rand.nextDouble() * (entry.getValue() * 32 + 16);
                        z += rand.nextDouble() * (entry.getValue() * 32 + 16);

                        world.a("portal", x, y, z, 0.0D, 0.0D, 0.0D);
                    }
                }
            }
        }

        if(!init) {
            loadAnchors();
        }

        if(update % 60 == 0) {
            saveAnchors();
        }
    }

    /**
     * Toggles the Anchor Aura
     */
    public void toggleDisplay()
    {
        displayChunks = !displayChunks;
        if(displayChunks) {
            printDebug(-1, "Anchor particles toggled on");
        } else {
            printDebug(-1, "Anchor particles toggled off");
        }
    }
}
