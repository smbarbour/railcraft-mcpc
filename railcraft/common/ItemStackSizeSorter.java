package railcraft.common;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import net.minecraft.server.ItemStack;

public class ItemStackSizeSorter implements Comparator<ItemStack>
{

    private static ItemStackSizeSorter instance;

    private static ItemStackSizeSorter getInstance()
    {
        if(instance == null) {
            instance = new ItemStackSizeSorter();
        }
        return instance;
    }

    public static void sort(List<ItemStack> list)
    {
        Collections.sort(list, getInstance());
    }

    @Override
    public int compare(ItemStack o1, ItemStack o2)
    {
        Integer size1 = o1.count;
        Integer size2 = o2.count;
        return size1.compareTo(size2);
    }
}
