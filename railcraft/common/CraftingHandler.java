package railcraft.common;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;
import forge.ICraftingHandler;
import railcraft.common.carts.EnumCart;
import railcraft.common.carts.ItemCart;
import railcraft.common.util.liquids.LiquidManager;

public class CraftingHandler implements ICraftingHandler
{

    @Override
    public void onTakenFromCrafting(EntityHuman player, ItemStack result, IInventory craftMatrix)
    {
        int count = 0;
        ItemStack cartItem = null;
        for(int i = 0; i < craftMatrix.getSize(); i++) {
            ItemStack stack = craftMatrix.getItem(i);
            if(stack != null) {
                count++;
                if(stack.getItem() != null && stack.getItem() instanceof ItemCart && ItemCart.getCartType(stack) != EnumCart.BASIC) {
                    cartItem = stack;
                }
            }
        }
        if(cartItem != null) {

            if(ItemCart.getCartType(result) == EnumCart.TANK && ItemCart.getCartType(cartItem) == EnumCart.TANK) {
                if(cartItem.getData() > 0) {
                    ItemStack filter = LiquidManager.getInstance().getItemFromFilterId(cartItem.getData());
                    if(!player.inventory.pickup(filter)) {
                        player.drop(filter);
                    }
                } else {
                    for(int i = 0; i < craftMatrix.getSize(); i++) {
                        ItemStack stack = craftMatrix.getItem(i);
                        if(stack != null && LiquidManager.getInstance().isBucket(stack)) {
                            craftMatrix.setItem(i, null);
                        }
                    }
                }
                return;
            }

            if(count == 1 && ItemCart.getCartType(result) == EnumCart.BASIC) {
                ItemStack contents = ItemCart.getCartType(cartItem).getContents();
                if(contents != null) {
                    if(!player.inventory.pickup(contents)) {
                        for(int j = 0; j < craftMatrix.getSize(); j++) {
                            ItemStack slot = craftMatrix.getItem(j);
                            if(slot == null) {
                                contents.count += 1;
                                craftMatrix.setItem(j, contents);
                                return;
                            }
                        }
                        player.drop(contents);
                    }
                }
            }
        }
    }
}
