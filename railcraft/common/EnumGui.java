package railcraft.common;

/**
 *
 * @author CovertJaguar
 */
public enum EnumGui
{

    LOADER_ITEM(true),
    LOADER_LIQUID(true),
    LOADER_ENERGY(true),
    UNLOADER_LIQUID(true),
    UNLOADER_ENERGY(true),
    DETECTOR_TANK(true),
    DETECTOR_ANIMAL(false),
    DETECTOR_SHEEP(true),
    DETECTOR_ADVANCED(false),
    DETECTOR_TRAIN(false),
    COKE_OVEN(true),
    BLAST_FURNACE(true),
    WATER_TANK(true),
    ROCK_CRUSHER(true),
    ROLLING_MACHINE(true),
    CART_DISPENSER(true),
    TRAIN_DISPENSER(true),
    FEED_STATION(true),
    RAIL_PRIMING(false),
    RAIL_LAUNCHER(false),
    CART_BORE(true),
    CART_TNT_FUSE(false),
    CART_TNT_BLAST(false),
    CART_WORK(true),
    CART_ENERGY(true),
    CART_TANK(true),
    CART_TRACK_RELAYER(true),
    CART_UNDERCUTTER(true),
    BOX_CONTROLLER(false),
    BOX_RECEIVER(false),
    SWITCH_MOTOR(false);
    private final byte id;
    private final boolean hasContainer;
    private static byte nextId = 0;

    private EnumGui(boolean hasContainer)
    {
        id = getNextId();
        this.hasContainer = hasContainer;
    }

    public byte getId()
    {
        return id;
    }

    private static byte getNextId()
    {
        return nextId++;
    }

    public boolean hasContainer()
    {
        return hasContainer;
    }

    public static EnumGui fromId(int i)
    {
        for(EnumGui e : values()) {
            if(e.getId() == i) {
                return e;
            }
        }
        return LOADER_ITEM;
    }
}
