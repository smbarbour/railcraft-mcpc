/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package railcraft.common;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public enum EnumColor
{

    NONE,
    BLACK,
    RED,
    GREEN,
    BROWN,
    BLUE,
    PURPLE,
    CYAN,
    LIGHT_GREY,
    GREY,
    PINK,
    LIGHT_GREEN,
    YELLOW,
    LIGHT_BLUE,
    MAGENTA,
    ORANGE,
    WHITE;
    private static byte nextId = 0;
    private final byte id;

    private EnumColor()
    {
        id = getNextId();
    }

    private static byte getNextId()
    {
        byte i = nextId;
        nextId++;
        return i;
    }

    public byte getId()
    {
        return id;
    }

    public static EnumColor fromId(int id)
    {
        for(EnumColor c : values()) {
            if(c.getId() == id) {
                return c;
            }
        }
        return NONE;
    }

    public String getTag()
    {
        return "color." + name().replace("_", ".").toLowerCase();
    }

    public String getBasicTag()
    {
        return name().replace("_", ".").toLowerCase();
    }

    @Override
    public String toString()
    {
        String s = name().replace("_", " ");
        String[] words = s.split(" ");
        StringBuilder b = new StringBuilder();
        for(String word : words) {
            b.append(word.charAt(0)).append(word.substring(1).toLowerCase()).append(" ");
        }
        return b.toString().trim();
    }
}
