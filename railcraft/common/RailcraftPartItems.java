package railcraft.common;

import railcraft.common.util.liquids.LiquidItems;
import cpw.mods.fml.common.registry.FMLRegistry;
import net.minecraft.server.Block;
import net.minecraft.server.FurnaceRecipes;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import forge.MinecraftForge;
import forge.oredict.OreDictionary;
import ic2.api.Ic2Recipes;
import railcraft.common.api.ItemRegistry;
import railcraft.common.cube.EnumCube;
import railcraft.common.modules.RailcraftModuleManager;
import railcraft.common.modules.RailcraftModuleManager.Module;
import railcraft.common.utility.EnumUtility;

public class RailcraftPartItems
{

    private static Item itemRailStandard;
    private static Item itemRailAdvanced;
    private static Item itemRailSpeed;
    private static Item itemRailReinforced;
    private static Item itemTieWood;
    private static Item itemTieStone;
    private static Item itemRailbedWood;
    private static Item itemRailbedStone;
    private static Item itemRebar;
    private static Item itemIngotSteel;
    private static Item itemDustObsidian;

    public static ItemStack getRailStandard()
    {
        return getRailStandard(1);
    }

    public static ItemStack getRailStandard(int qty)
    {
        if(RailcraftConfig.useOldRecipes()) {
            return new ItemStack(Item.IRON_INGOT, qty);
        }
        Item item = itemRailStandard;
        if(item != null) {
            return new ItemStack(item, qty);
        }

        String tag = "part.rail.standard";
        int id = RailcraftConfig.getItemId(tag);

        item = new ItemRailcraft(id).d(105).a(tag);
        RailcraftLanguage.getInstance().registerItemName(item, tag);

        if(!RailcraftModuleManager.isModuleLoaded(Module.FACTORY)) {
            FMLRegistry.addRecipe(new ItemStack(item, 8), getRailStandardRecipe(new ItemStack(Item.IRON_INGOT)));
        }

        ItemRegistry.registerItem(tag, new ItemStack(item));

        itemRailStandard = item;
        return new ItemStack(item, qty);
    }

    public static Object[] getRailStandardRecipe(ItemStack ingot)
    {
        return new Object[]{
                "I I",
                "I I",
                "I I",
                Character.valueOf('I'), ingot
            };
    }

    public static ItemStack getRailAdvanced()
    {
        return getRailAdvanced(1);
    }

    public static ItemStack getRailAdvanced(int qty)
    {
        if(RailcraftConfig.useOldRecipes()) {
            return new ItemStack(Item.GOLD_INGOT, qty);
        }
        Item item = itemRailAdvanced;
        if(item != null) {
            return new ItemStack(item, qty);
        }

        String tag = "part.rail.advanced";
        int id = RailcraftConfig.getItemId(tag);

        item = new ItemRailcraft(id).d(121).a(tag);
        RailcraftLanguage.getInstance().registerItemName(item, tag);

        if(!RailcraftModuleManager.isModuleLoaded(Module.FACTORY)) {
            FMLRegistry.addRecipe(new ItemStack(item, 8), getRailAdvancedRecipe());
        }

        ItemRegistry.registerItem(tag, new ItemStack(item));

        itemRailAdvanced = item;
        return new ItemStack(item, qty);
    }

    public static Object[] getRailAdvancedRecipe()
    {
        return new Object[]{
                "IRG",
                "IRG",
                "IRG",
                Character.valueOf('I'), getRailStandard(),
                Character.valueOf('R'), Item.REDSTONE,
                Character.valueOf('G'), Item.GOLD_INGOT
            };
    }

    public static ItemStack getRailSpeed()
    {
        return getRailSpeed(1);
    }

    public static ItemStack getRailSpeed(int qty)
    {
        if(RailcraftConfig.useOldRecipes()) {
            return getIngotSteel(qty);
        }
        Item item = itemRailSpeed;
        if(item != null) {
            return new ItemStack(item, qty);
        }

        String tag = "part.rail.speed";
        int id = RailcraftConfig.getItemId(tag);

        item = new ItemRailcraft(id).d(137).a(tag);
        RailcraftLanguage.getInstance().registerItemName(item, tag);

        if(!RailcraftModuleManager.isModuleLoaded(Module.FACTORY)) {
            FMLRegistry.addRecipe(new ItemStack(item, 8), getRailSpeedRecipe());
        }

        ItemRegistry.registerItem(tag, new ItemStack(item));

        itemRailSpeed = item;
        return new ItemStack(item, qty);
    }

    public static Object[] getRailSpeedRecipe()
    {
        return new Object[]{
                "IBG",
                "IBG",
                "IBG",
                Character.valueOf('I'), getIngotSteel(),
                Character.valueOf('G'), Item.GOLD_INGOT,
                Character.valueOf('B'), Item.BLAZE_POWDER
            };
    }

    public static ItemStack getRailReinforced()
    {
        return getRailReinforced(1);
    }

    public static ItemStack getRailReinforced(int qty)
    {
        if(RailcraftConfig.useOldRecipes()) {
            return getIngotSteel(qty);
        }
        Item item = itemRailReinforced;
        if(item != null) {
            return new ItemStack(item, qty);
        }

        String tag = "part.rail.reinforced";
        int id = RailcraftConfig.getItemId(tag);

        item = new ItemRailcraft(id).d(102).a(tag);
        RailcraftLanguage.getInstance().registerItemName(item, tag);

        if(!RailcraftModuleManager.isModuleLoaded(Module.FACTORY)) {
            FMLRegistry.addRecipe(new ItemStack(item, 8), getRailReinforcedRecipe());
        }

        ItemRegistry.registerItem(tag, new ItemStack(item));

        itemRailReinforced = item;
        return new ItemStack(item, qty);
    }

    public static Object[] getRailReinforcedRecipe()
    {
        return new Object[]{
                "IDI",
                "IDI",
                "IDI",
                Character.valueOf('I'), getIngotSteel(),
                Character.valueOf('D'), getDustObsidian()
            };
    }

    public static ItemStack getRebar()
    {
        return getRebar(1);
    }

    public static ItemStack getRebar(int qty)
    {
//        if(!EnumUtility.ROLLING_MACHINE.isEnabled()) {
//            return new ItemStack(Item.ingotIron, qty);
//        }
        Item item = itemRebar;
        if(item != null) {
            return new ItemStack(item, qty);
        }

        String tag = "part.rebar";
        int id = RailcraftConfig.getItemId(tag);

        if(id > 0) {
            item = new ItemRailcraft(id).d(153).a(tag);
            RailcraftLanguage.getInstance().registerItemName(item, tag);
            ItemRegistry.registerItem(tag, new ItemStack(item));
            itemRebar = item;
        } else {
            return new ItemStack(Item.IRON_INGOT, qty);
        }

        return new ItemStack(item, qty);
    }

    public static Object[] getRebarRecipe(ItemStack metal)
    {
        return new Object[]{
                "  I",
                " I ",
                "I  ",
                Character.valueOf('I'), metal,};
    }

    public static Object[] getRebarRecipeAlt(ItemStack metal)
    {
        return new Object[]{
                "I  ",
                " I ",
                "  I",
                Character.valueOf('I'), metal,};
    }

    public static ItemStack getTieWood()
    {
        Item item = itemTieWood;
        if(item == null) {
            String tag = "part.tie.wood";
            int id = RailcraftConfig.getItemId(tag);

            item = new ItemRailcraft(id).d(22).a(tag);
            RailcraftLanguage.getInstance().registerItemName(item, tag);

            ItemStack output = new ItemStack(item);
            FMLRegistry.addRecipe(output, new Object[]{
                    " O ",
                    "###",
                    Character.valueOf('O'), LiquidItems.getCreosoteOilBucket(),
                    Character.valueOf('#'), new ItemStack(Block.STEP, 1, 2)
                });
            FMLRegistry.addRecipe(output, new Object[]{
                    " O ",
                    "###",
                    Character.valueOf('O'), LiquidItems.getCreosoteOilBottle(),
                    Character.valueOf('#'), new ItemStack(Block.STEP, 1, 2)
                });
            FMLRegistry.addRecipe(output, new Object[]{
                    " O ",
                    "###",
                    Character.valueOf('O'), LiquidItems.getCreosoteOilCan(),
                    Character.valueOf('#'), new ItemStack(Block.STEP, 1, 2)
                });
            FMLRegistry.addRecipe(output, new Object[]{
                    " O ",
                    "###",
                    Character.valueOf('O'), LiquidItems.getCreosoteOilWax(),
                    Character.valueOf('#'), new ItemStack(Block.STEP, 1, 2)
                });
            FMLRegistry.addRecipe(output, new Object[]{
                    " O ",
                    "###",
                    Character.valueOf('O'), LiquidItems.getCreosoteOilRefactory(),
                    Character.valueOf('#'), new ItemStack(Block.STEP, 1, 2)
                });

            MinecraftForge.addDungeonLoot(new ItemStack(item), RailcraftConfig.getLootChance("tie.wood"), 4, 16);

            ItemRegistry.registerItem(tag, new ItemStack(item));

            itemTieWood = item;
        }
        return new ItemStack(item);
    }

    public static ItemStack getTieStone()
    {
        Item item = itemTieStone;
        if(item != null) {
            return new ItemStack(item);
        }

        String tag = "part.tie.stone";
        int id = RailcraftConfig.getItemId(tag);

        item = new ItemRailcraft(id).d(54).a(tag);
        RailcraftLanguage.getInstance().registerItemName(item, tag);

        ItemStack output = new ItemStack(item);
        FMLRegistry.addRecipe(output, new Object[]{
                " O ",
                "###",
                Character.valueOf('O'), getRebar(),
                Character.valueOf('#'), new ItemStack(Block.STEP, 1, 0)
            });

        ItemRegistry.registerItem(tag, new ItemStack(item));

        itemTieStone = item;
        return new ItemStack(item);
    }

    public static ItemStack getRailbedWood()
    {
        if(RailcraftConfig.useOldRecipes()) {
            return new ItemStack(Item.STICK);
        }
        Item item = itemRailbedWood;
        if(item != null) {
            return new ItemStack(item);
        }

        String tag = "part.railbed.wood";
        int id = RailcraftConfig.getItemId(tag);

        item = new ItemRailcraft(id).d(6).a(tag);
        RailcraftLanguage.getInstance().registerItemName(item, tag);

        ItemStack output = new ItemStack(item);
        FMLRegistry.addShapelessRecipe(output, new Object[]{
                getTieWood(),
                getTieWood(),
                getTieWood(),
                getTieWood()
            });

        ItemRegistry.registerItem(tag, new ItemStack(item));

        itemRailbedWood = item;
        return new ItemStack(item);
    }

    public static ItemStack getRailbedStone()
    {
        if(RailcraftConfig.useOldRecipes()) {
            return new ItemStack(Block.STEP, 1, 0);
        }
        Item item = itemRailbedStone;
        if(item != null) {
            return new ItemStack(item);
        }

        String tag = "part.railbed.stone";
        int id = RailcraftConfig.getItemId(tag);

        item = new ItemRailcraft(id).d(38).a(tag);
        RailcraftLanguage.getInstance().registerItemName(item, tag);

        ItemStack output = new ItemStack(item);
        FMLRegistry.addShapelessRecipe(output, new Object[]{
                getTieStone(),
                getTieStone(),
                getTieStone(),
                getTieStone()
            });

        ItemRegistry.registerItem(tag, new ItemStack(item));

        itemRailbedStone = item;
        return new ItemStack(item);
    }

    public static ItemStack getIngotSteel()
    {
        return getIngotSteel(1);
    }

    public static ItemStack getIngotSteel(int qty)
    {
        if(!EnumUtility.BLAST_FURNACE.isEnabled()) {
            return new ItemStack(Item.IRON_INGOT, qty);
        }
        Item item = itemIngotSteel;
        if(item != null) {
            return new ItemStack(item, qty);
        }

        String tag = "part.ingot.steel";
        int id = RailcraftConfig.getItemId(tag);

        if(id <= 0) {
            return new ItemStack(Item.IRON_INGOT, qty);
        }

        item = new ItemRailcraft(id).d(181).a(tag);
        RailcraftLanguage.getInstance().registerItemName(item, tag);

        FMLRegistry.addRecipe(new ItemStack(Item.MINECART, 2), new Object[]{
                "I I",
                "III",
                Character.valueOf('I'), new ItemStack(item)});

        ItemRegistry.registerItem(tag, new ItemStack(item));

        itemIngotSteel = item;

        OreDictionary.registerOre("ingotSteel", new ItemStack(item));
        MinecraftForge.addDungeonLoot(new ItemStack(itemIngotSteel), RailcraftConfig.getLootChance("steel.ingot"), 5, 9);

        return new ItemStack(item, qty);
    }

    public static ItemStack getDustObsidian()
    {
        return getDustObsidian(1);
    }

    public static ItemStack getDustObsidian(int qty)
    {
        Item item = itemDustObsidian;
        if(item != null) {
            return new ItemStack(item, qty);
        }

        String tag = "part.dust.obsidian";
        int id = RailcraftConfig.getItemId(tag);

        item = new ItemRailcraft(id).d(164).a(tag);
        RailcraftLanguage.getInstance().registerItemName(item, tag);


        ItemRegistry.registerItem(tag, new ItemStack(item));

        itemDustObsidian = item;

        OreDictionary.registerOre("dustObsidian", new ItemStack(item));

        if(RailcraftModuleManager.isIC2Loaded()) {
            Ic2Recipes.addMaceratorRecipe(EnumCube.CRUSHED_OBSIDIAN.getItem(), new ItemStack(item));
        }
        return new ItemStack(item, qty);
    }
}
