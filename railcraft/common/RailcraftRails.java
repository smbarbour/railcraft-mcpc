package railcraft.common;

import net.minecraft.server.ItemStack;
import net.minecraft.server.ModLoader;
import railcraft.common.api.ItemRegistry;
import railcraft.common.tracks.EnumTrack;

public class RailcraftRails
{

    public static void registerRail(EnumTrack rail)
    {
        RailcraftBlocks.registerBlockTrack();
        if(RailcraftBlocks.getBlockTrack() != null) {
            if(RailcraftConfig.isSubBlockEnabled(rail.getTag())) {
                if(rail.getTileClass() != null) {
                    ModLoader.registerTileEntity(rail.getTileClass(), rail.getTileTag());
                }
//                Item oldItem = rail.createItem(id);
                ItemStack stack = rail.registerRecipe();
                RailcraftLanguage.getInstance().registerItemName(stack, rail.getTag());
//                RailcraftLanguage.getInstance().registerItemName(oldItem, rail.getTag()) + " (Obsolete)";

                ItemRegistry.registerItem(rail.getTag(), stack);
            }
        }
    }
}
