package railcraft.common;

import java.io.PrintWriter;

public class RailcraftInstallationException extends RuntimeException
{

    public RailcraftInstallationException(String msg)
    {
        super(msg);
    }

    @Override
    public void printStackTrace(PrintWriter s)
    {
        s.println(this);
    }

    @Override
    public String toString()
    {
        String s = getClass().getSimpleName();
        String message = getLocalizedMessage();
        return (message != null) ? (s + ": " + message) : s;
    }
}
