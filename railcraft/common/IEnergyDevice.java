package railcraft.common;

import net.minecraft.server.IInventory;

public interface IEnergyDevice extends IInventory
{
    public int getEnergy();
    public int getCapacity();
    public int getEnergyBarScaled(int scale);
}
