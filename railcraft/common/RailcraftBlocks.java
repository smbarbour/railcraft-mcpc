package railcraft.common;

import railcraft.common.detector.ItemDetector;
import net.minecraft.server.*;
import net.minecraft.server.ItemStack;
import net.minecraft.server.ModLoader;
import forge.MinecraftForge;
import railcraft.Railcraft;
import railcraft.common.api.ItemRegistry;
import railcraft.common.cube.BlockCube;
import railcraft.common.cube.ItemCube;
import railcraft.common.detector.BlockDetector;
import railcraft.common.detector.BlockDetector.Detector;
import railcraft.common.tracks.BlockTrack;
import railcraft.common.tracks.BlockRailElevator;
import railcraft.common.tracks.ItemTrack;
import railcraft.common.structures.BlockStructure;
import railcraft.common.structures.ItemStructure;
import railcraft.common.utility.BlockUtility;
import railcraft.common.utility.ItemUtility;

public class RailcraftBlocks
{

    private static Block blockDetector;
    private static Block blockUtility;
    private static Block blockTrack;
    private static Block blockRailElevator;
    private static Block blockStructure;
    private static Block blockBuildcraftTank;
    private static Block blockCube;

    public static void registerBlockTrack()
    {
        if(blockTrack == null) {
            int id = RailcraftConfig.getBlockId("block.track");

            if(id > 0) {
                int renderId = Railcraft.getUniqueBlockModelID();
                blockTrack = new BlockTrack(id, renderId).a("railcraftTrack");
                ModLoader.registerBlock(blockTrack, ItemTrack.class);
                MinecraftForge.setBlockHarvestLevel(blockTrack, "crowbar", 0);
                MinecraftForge.setBlockHarvestLevel(blockTrack, "pickaxe", 0);
            }
        }
    }

    public static Block getBlockTrack()
    {
        return blockTrack;
    }

    public static void registerBlockRailElevator()
    {
        if(blockRailElevator == null) {
            int id = RailcraftConfig.getBlockId("block.elevator");

            // Define Elevator Rail
            if(id > 0) {
                int renderId = Railcraft.getUniqueBlockModelID();
                blockRailElevator = new BlockRailElevator(id, renderId).a("elevatorRail");
                ModLoader.registerBlock(blockRailElevator);
                MinecraftForge.setBlockHarvestLevel(blockRailElevator, "crowbar", 0);
                MinecraftForge.setBlockHarvestLevel(blockRailElevator, "pickaxe", 0);
                ItemStack stackElevator = new ItemStack(blockRailElevator, 8);
                ModLoader.addRecipe(stackElevator, new Object[]{
                        "IRI",
                        "III",
                        "IRI",
                        Character.valueOf('I'), Item.IRON_INGOT,
                        Character.valueOf('R'), Item.REDSTONE,});
                RailcraftLanguage.getInstance().registerItemName(stackElevator, "track.elevator");
            }
        }
    }

    public static Block getBlockRailElevator()
    {
        return blockRailElevator;
    }

    public static void registerBlockDetector()
    {
        if(blockDetector == null) {
            int id = RailcraftConfig.getBlockId("block.detector");

            if(id > 0) {
                blockDetector = new BlockDetector(id).a("cartDetector");
                ModLoader.registerBlock(blockDetector, ItemDetector.class);

                for(Detector d : Detector.values()) {
                    ItemStack stack = new ItemStack(blockDetector, 1, d.getMeta());
                    String tag = BlockDetector.getBlockNameFromMetadata(d.getMeta());
                    RailcraftLanguage.getInstance().registerItemName(stack, tag);

                    ItemRegistry.registerItem(tag, stack);
                }
            }
        }
    }

    public static Block getBlockDetector()
    {
        return blockDetector;
    }

    public static void registerBlockUtility()
    {
        if(blockUtility == null) {
            int id = RailcraftConfig.getBlockId("block.utility");

            if(id > 0) {
                blockUtility = new BlockUtility(id).a("blockUtility");
                ModLoader.registerBlock(blockUtility, ItemUtility.class);
            }
        }
    }

    public static Block getBlockUtility()
    {
        return blockUtility;
    }

    public static void registerBlockStructure()
    {
        if(blockStructure == null) {
            int id = RailcraftConfig.getBlockId("block.structure");

            if(id > 0) {
                int renderId = Railcraft.getUniqueBlockModelID();
                blockStructure = new BlockStructure(id, renderId);
                ModLoader.registerBlock(blockStructure, ItemStructure.class);
                Railcraft.registerStructureRenderer();
                MinecraftForge.setBlockHarvestLevel(blockStructure, "crowbar", 0);
            }
        }
    }

    public static Block getBlockStructure()
    {
        return blockStructure;
    }

    public static void registerBlockBuildcraftTank()
    {
        if(blockBuildcraftTank == null) {
            for(Block b : Block.byId) {
                if(b != null && b.getClass().getSimpleName().equals("BlockTank") && b.getClass().getName().contains("buildcraft")) {
                    blockBuildcraftTank = b;
                }
            }
        }
    }

    public static Block getBlockBuildcraftTank()
    {
        return blockBuildcraftTank;
    }

    public static Block getBlockCube()
    {
        return blockCube;
    }

    public static void registerBlockCube()
    {
        if(blockCube == null) {
            int id = RailcraftConfig.getBlockId("block.cube");

            if(id > 0) {
                blockCube = new BlockCube(id);
                ModLoader.registerBlock(blockCube, ItemCube.class);
                MinecraftForge.setBlockHarvestLevel(blockCube, "pickaxe", 2);
                MinecraftForge.setBlockHarvestLevel(blockCube, "crowbar", 0);
            }
        }
    }
}
