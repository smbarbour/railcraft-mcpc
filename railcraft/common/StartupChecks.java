package railcraft.common;

import java.util.logging.Level;
import net.minecraft.server.ModLoader;
import forge.MinecraftForge;
import railcraft.Railcraft;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class StartupChecks
{

    private static final int FORGE_BUILD = 157;

    public static void runChecks()
    {
//        if(ForgeHooks.getBuildVersion() < FORGE_BUILD) {
//            ModLoader.getLogger().log(Level.SEVERE, "{0}: Forge is too old, please update to build {1}", new Object[]{Railcraft.getNameAndVersion(), FORGE_BUILD});
//            throw new RailcraftInstallationException(Railcraft.getNameAndVersion() + ": Forge too old!"
//                + "\n************************************************************************************"
//                + "\n Forge is too old, please update to build " + FORGE_BUILD
//                + "\n************************************************************************************"
//                + "\n");
//        }

        MinecraftForge.versionDetect("Railcraft", 3, 3, 8);

        if(ModLoader.isModLoaded("mod_Railcraft")) {
            Railcraft.log(Level.SEVERE, "Railcraft is already loaded, please check your mod folder for old versions.");
            throw new RailcraftInstallationException(Railcraft.getNameAndVersion() + ": Mulitple versions found!"
                + "\n************************************************************************************"
                + "\n Railcraft is already loaded, please check your mod folder for old versions."
                + "\n************************************************************************************"
                + "\n");
        }
    }
}
