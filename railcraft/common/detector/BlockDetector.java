package railcraft.common.detector;

import java.util.ArrayList;
import net.minecraft.server.*;
import forge.*;
import railcraft.Railcraft;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.ICrowbar;
import railcraft.common.api.tracks.ITrackItem;
import railcraft.common.modules.RailcraftModuleManager;
import railcraft.common.modules.RailcraftModuleManager.Module;

public class BlockDetector extends BlockContainer implements ITextureProvider, IConnectRedstone
{

    protected final static int[][] texture = new int[Detector.values().length][3];

    public enum Detector
    {

        STORAGE(0),
        ANY(1),
        EMPTY(2),
        MOB(3),
        POWERED(4),
        PLAYER(5),
        EXPLOSIVE(6),
        ANIMAL(7),
        TANK(8),
        ADVANCED(9),
        ENERGY(10),
        GROWTH(11),
        TRAIN(12),
        SHEEP(13);
        private final int meta;

        private Detector(int meta)
        {
            this.meta = meta;
        }

        public int getMeta()
        {
            return meta;
        }

        public static Detector fromMeta(int meta)
        {
            for(Detector d : values()) {
                if(d.meta == meta) {
                    return d;
                }
            }
            return STORAGE;
        }
    }

    public BlockDetector(int blockID)
    {
        super(blockID, 12, Material.STONE);

        for(int i = 0; i < texture.length; i++) {
            for(int j = 0; j < texture[i].length; j++) {
                texture[i][j] = 13 + i * 16 + j;
            }
        }

        b(4.5F);
        c(2.0F);
        a(h);
        j();

        ModLoader.registerTileEntity(TileDetector.class, "RCDetectorTile");
        ModLoader.registerTileEntity(TileDetectorAdvanced.class, "RCDetectorAdvancedTile");
        ModLoader.registerTileEntity(TileDetectorTank.class, "RCDetectorTankTile");
        ModLoader.registerTileEntity(TileDetectorAnimal.class, "RCDetectorAnimalTile");
        ModLoader.registerTileEntity(TileDetectorTrain.class, "RCDetectorTrainTile");
        ModLoader.registerTileEntity(TileDetectorSheep.class, "RCDetectorSheepTile");

        MinecraftForge.setBlockHarvestLevel(this, "pickaxe", 2);
        MinecraftForge.setBlockHarvestLevel(this, "crowbar", 0);
    }

    @Override
    public String getTextureFile()
    {
        return "/railcraft/textures/railcraft.png";
    }

    public static String getBlockNameFromMetadata(int meta)
    {
        StringBuilder name = new StringBuilder("detector.");
        name.append(Detector.fromMeta(meta).name().toLowerCase());
        return name.toString();
    }

    @Override
    public boolean isBlockNormalCube(World world, int i, int j, int k)
    {
        return false;
    }

    @Override
    public boolean isBlockSolidOnSide(World world, int i, int j, int k, int side)
    {
        return true;
    }

    @Override
    protected int getDropData(int meta)
    {
        return meta;
    }

    @Override
    public TileEntity a_()
    {
        return null;
    }

    @Override
    public TileEntity getBlockEntity(int meta)
    {
        if(meta == Detector.ADVANCED.getMeta()) {
            return new TileDetectorAdvanced();
        }
        if(meta == Detector.TANK.getMeta()) {
            return new TileDetectorTank();
        }
        if(meta == Detector.ANIMAL.getMeta()) {
            return new TileDetectorAnimal();
        }
        if(meta == Detector.TRAIN.getMeta()) {
            return new TileDetectorTrain();
        }
        if(meta == Detector.SHEEP.getMeta()) {
            return new TileDetectorSheep();
        }
        return new TileDetector();
    }

    // Determine direction here
    @Override
    public void postPlace(World world, int i, int j, int k, EntityLiving entityliving)
    {
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileDetector) {
            ((TileDetector)tile).direction = GeneralTools.getSideClosestToPlayer(world, i, j, k, entityliving);
        }
    }

    @Override
    public boolean interact(World world, int i, int j, int k, EntityHuman player)
    {
        if(player.isSneaking()) {
            return false;
        }
        ItemStack current = player.U();
        if(current != null) {
            Item item = current.getItem();
            if(item instanceof ICrowbar) {
                if(Railcraft.gameIsNotHost()) {
                    return true;
                }
                TileEntity t = world.getTileEntity(i, j, k);
                if(t instanceof TileDetector) {
                    TileDetector tile = (TileDetector)t;
                    byte side = GeneralTools.getCurrentMousedOverSide(player);
                    if(tile.direction == side) {
                        tile.direction = GeneralTools.getOppositeSide(side);
                    } else {
                        tile.direction = side;
                    }
                    if(current.d()) {
                        current.damage(1, player);
                    }
                    world.notify(i, j, k);
                    return true;
                }
            } else if((item.id < Block.byId.length && BlockMinecartTrack.d(item.id))
                || item instanceof ITrackItem) {
                return false;
            }
        }
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileDetector) {
            return ((TileDetector)tile).blockActivated(player);
        }
        return false;
    }

    public int getBlockTexture(IBlockAccess world, int i, int j, int k, int side)
    {
        int meta = world.getData(i, j, k);
        if(meta >= texture.length || meta < 0) {
            meta = 0;
        }
        TileEntity t = world.getTileEntity(i, j, k);
        if(t instanceof TileDetector && ((TileDetector)t).direction == side) {
            if(((TileDetector)t).powered) {
                return texture[meta][2];
            }
            return texture[meta][1];
        }
        return texture[meta][0];
    }

    @Override
    public int a(int side, int meta)
    {
        if(meta >= texture.length || meta < 0) {
            return texture[0][1];
        }
        if(side == 3) {
            return texture[meta][2];
        }
        return texture[meta][0];
    }

    @Override
    public boolean isPowerSource()
    {
        return true;
    }

    @Override
    public boolean a(IBlockAccess world, int i, int j, int k, int side)
    {
        TileEntity t = world.getTileEntity(i, j, k);
        if(t instanceof TileDetector) {
            TileDetector tile = (TileDetector)t;
            if(tile.direction == GeneralTools.getOppositeSide(side)) {
                return tile.powered;
            }
        }
        return false;
    }

    @Override
    public boolean d(World world, int i, int j, int k, int oppositeSide)
    {
        return a(world, i, j, k, oppositeSide);
    }

    @Override
    public void onPlace(World world,
        int i,
        int j,
        int k)
    {
        super.onPlace(world, i, j, k);
        world.notify(i, j, k);
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        world.applyPhysics(i + 1, j, k, id);
        world.applyPhysics(i - 1, j, k, id);
        world.applyPhysics(i, j, k + 1, id);
        world.applyPhysics(i, j, k - 1, id);
        world.applyPhysics(i, j - 1, k, id);
        world.applyPhysics(i, j + 1, k, id);
        world.c(i, j, k, id, d());
    }

    @Override
    public void remove(World world, int i, int j, int k)
    {
        super.remove(world, i, j, k);
        world.notify(i, j, k);
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        world.applyPhysics(i + 1, j, k, id);
        world.applyPhysics(i - 1, j, k, id);
        world.applyPhysics(i, j, k + 1, id);
        world.applyPhysics(i, j, k - 1, id);
        world.applyPhysics(i, j - 1, k, id);
        world.applyPhysics(i, j + 1, k, id);
    }

    @Override
    public boolean canConnectRedstone(IBlockAccess world, int i, int j, int k, int dir)
    {
        TileEntity t = world.getTileEntity(i, j, k);
        if(t instanceof TileDetector) {
            TileDetector tile = (TileDetector)t;
            if(dir == 1 && tile.direction == 5) {
                return true;
            }
            if(dir == 3 && tile.direction == 4) {
                return true;
            }
            if(dir == 2 && tile.direction == 3) {
                return true;
            }
            if(dir == 0 && tile.direction == 2) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void addCreativeItems(ArrayList itemList)
    {
        for(Detector d : Detector.values()) {
            if(d == Detector.ENERGY && !RailcraftModuleManager.isModuleLoaded(Module.IC2)) {
                continue;
            }
            if(d == Detector.TRAIN && !RailcraftModuleManager.isModuleLoaded(Module.TRAIN)) {
                continue;
            }
            itemList.add(new ItemStack(id, 1, d.getMeta()));
        }
    }
}