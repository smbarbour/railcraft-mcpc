package railcraft.common.detector;

import forestry.api.liquids.LiquidStack;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;
import net.minecraft.server.*;
import buildcraft.api.ILiquidContainer;
import railcraft.Railcraft;
import railcraft.common.DetectorButtonController;
import railcraft.common.DetectorButtonController.ButtonMode;
import railcraft.common.EnumGui;
import railcraft.common.util.liquids.LiquidTankToolkit;
import railcraft.common.RailcraftLanguage;
import railcraft.common.api.CartTools;
import railcraft.common.carts.EntityCartTank;
import railcraft.common.util.liquids.LiquidManager;

public class TileDetectorTank extends TileDetectorItem
{

    private final DetectorButtonController buttonController = new DetectorButtonController(ButtonMode.CAPACITY);

    public TileDetectorTank()
    {
        super(RailcraftLanguage.translate("detector.tank"), 1);
    }

    public DetectorButtonController getButtonController()
    {
        return buttonController;
    }

    public LiquidStack getFilterLiquid()
    {
        ItemStack filter = getItem(0);
        if(filter != null) {
            return LiquidManager.getInstance().getLiquidInContainer(filter);
        }
        return null;
    }

    @Override
    public boolean testForCarts()
    {
        List<EntityMinecart> carts = CartTools.getMinecartsOnAllSides(world, x, y, z, 0);
        for(EntityMinecart cart : carts) {
            if(cart instanceof ILiquidContainer) {
                LiquidTankToolkit wrapper = new LiquidTankToolkit((ILiquidContainer)cart);
                boolean liquidMatches = false;
                LiquidStack filterLiquid = getFilterLiquid();
                if(filterLiquid == null) {
                    liquidMatches = true;
                } else if(cart instanceof EntityCartTank && wrapper.getLiquidQuantity() <= 0 && LiquidManager.getInstance().isLiquidEqual(filterLiquid, ((EntityCartTank)cart).getFilterLiquid())) {
                    liquidMatches = true;
                } else if(filterLiquid.isLiquidEqual(wrapper.getLiquid())) {
                    liquidMatches = true;
                }
                boolean quantityMatches = false;
                switch (buttonController.getState()) {
                    case 0:
                        quantityMatches = true;
                        break;
                    case 1:
                        if(filterLiquid != null && wrapper.isTankEmpty(filterLiquid.itemID)) {
                            quantityMatches = true;
                        } else if(filterLiquid == null && wrapper.getLiquidQuantity() <= 0) {
                            quantityMatches = true;
                        }
                        break;
                    case 2:
                        if(filterLiquid != null && !wrapper.isTankFull(filterLiquid.itemID) && !wrapper.isTankEmpty(filterLiquid.itemID)) {
                            quantityMatches = true;
                        } else if(filterLiquid == null && wrapper.isTankPartiallyFull()) {
                            quantityMatches = true;
                        }
                        break;
                    case 3:
                        if(filterLiquid != null && wrapper.isTankFull(filterLiquid.itemID)) {
                            quantityMatches = true;
                        } else if(filterLiquid == null && wrapper.isTankFull()) {
                            quantityMatches = true;
                        }
                        break;
                }
                return liquidMatches && quantityMatches;
            }
        }
        return false;
    }

    @Override
    public boolean blockActivated(EntityHuman player)
    {
        player.openGui(Railcraft.getMod(), EnumGui.DETECTOR_TANK.getId(), world, x, y, z);
        return true;
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);

        data.setByte("state", buttonController.getState());
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);

        buttonController.setState(data.getByte("state"));
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeByte(buttonController.getState());
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        buttonController.setState(data.readByte());
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
