package railcraft.common.detector;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;
import net.minecraft.server.*;
import railcraft.GuiHandler;
import railcraft.common.EnumGui;
import railcraft.common.api.CartTools;
import railcraft.common.carts.EntityCartTank;

// Referenced classes of package net.minecraft.src:
//            TileEntity, NBTTagCompound, World
public class TileDetectorAdvanced extends TileDetector
{

    public boolean storage;
    public boolean empty;
    public boolean mob;
    public boolean furnace;
    public boolean player;
    public boolean explosive;
    public boolean animal;
    public boolean tank;

    public TileDetectorAdvanced()
    {
    }

    @Override
    public boolean testForCarts()
    {
        List<EntityMinecart> carts = CartTools.getMinecartsOnAllSides(world, x, y, z, 0);
        for(EntityMinecart cart : carts) {
            if(storage && cart.isStorageCart()) {
                return true;
            } else if(empty && cart.canBeRidden() && cart.passenger == null) {
                return true;
            } else if(mob && cart.passenger instanceof IMonster) {
                return true;
            } else if(furnace && cart.isPoweredCart()) {
                return true;
            } else if(player && cart.passenger instanceof EntityPlayer) {
                return true;
            } else if(explosive && cart.getEntityData().getBoolean("Explosive")) {
                return true;
            } else if(animal && cart.passenger instanceof EntityAnimal) {
                return true;
            } else if(tank && cart instanceof EntityCartTank) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean blockActivated(EntityHuman player)
    {
        GuiHandler.openGui(EnumGui.DETECTOR_ADVANCED, player, world, x, y, z);
        return true;
    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);

        nbttagcompound.setBoolean("storage", storage);
        nbttagcompound.setBoolean("empty", empty);
        nbttagcompound.setBoolean("mob", mob);
        nbttagcompound.setBoolean("furnace", furnace);
        nbttagcompound.setBoolean("player", player);
        nbttagcompound.setBoolean("explosive", explosive);
        nbttagcompound.setBoolean("animal", animal);
        nbttagcompound.setBoolean("tank", tank);

    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);

        storage = nbttagcompound.getBoolean("storage");
        empty = nbttagcompound.getBoolean("empty");
        mob = nbttagcompound.getBoolean("mob");
        furnace = nbttagcompound.getBoolean("furnace");
        player = nbttagcompound.getBoolean("player");
        explosive = nbttagcompound.getBoolean("explosive");
        animal = nbttagcompound.getBoolean("animal");
        tank = nbttagcompound.getBoolean("tank");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(storage);
        data.writeBoolean(empty);
        data.writeBoolean(mob);
        data.writeBoolean(furnace);
        data.writeBoolean(player);
        data.writeBoolean(explosive);
        data.writeBoolean(animal);
        data.writeBoolean(tank);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        storage = data.readBoolean();
        empty = data.readBoolean();
        mob = data.readBoolean();
        furnace = data.readBoolean();
        player = data.readBoolean();
        explosive = data.readBoolean();
        animal = data.readBoolean();
        tank = data.readBoolean();
    }
}
