package railcraft.common.detector;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;
import net.minecraft.server.*;
import railcraft.GuiHandler;
import railcraft.common.EnumGui;
import railcraft.common.api.CartTools;
import railcraft.common.api.ILinkageManager;
import railcraft.common.carts.LinkageManager;

public class TileDetectorTrain extends TileDetector
{

    private short trainSize = 5;

    public TileDetectorTrain()
    {
    }

    @Override
    public boolean testForCarts()
    {
        ILinkageManager lm = LinkageManager.getInstance();
        List<EntityMinecart> carts = CartTools.getMinecartsOnAllSides(world, x, y, z, 0);
        for(EntityMinecart cart : carts) {
            if(lm.countCartsInTrain(cart) >= getTrainSize()) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected short getUpdateInterval()
    {
        return 5;
    }

    @Override
    public boolean blockActivated(EntityHuman player)
    {
        GuiHandler.openGui(EnumGui.DETECTOR_TRAIN, player, world, x, y, z);
        return true;
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);

        data.setShort("size", getTrainSize());
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);

        setTrainSize(data.getShort("size"));
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeShort(getTrainSize());
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        setTrainSize(data.readShort());
    }

    public short getTrainSize()
    {
        return trainSize;
    }

    public void setTrainSize(short trainSize)
    {
        this.trainSize = trainSize;
    }
}
