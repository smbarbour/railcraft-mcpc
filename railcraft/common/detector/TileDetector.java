package railcraft.common.detector;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;
import net.minecraft.server.*;
import ic2.api.IWrenchable;
import railcraft.Railcraft;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftTileEntity;
import railcraft.common.api.GeneralTools;
import railcraft.common.SafeNBTWrapper;
import railcraft.common.api.CartTools;
import railcraft.common.carts.EntityCartEnergy;
import railcraft.common.detector.BlockDetector.Detector;

// Referenced classes of package net.minecraft.src:
//            TileEntity, NBTTagCompound, World
public class TileDetector extends RailcraftTileEntity implements IWrenchable
{

    public byte direction = 0;
    public boolean powered = false;
    private int update = GeneralTools.getRand().nextInt();

    public TileDetector()
    {
    }

    public boolean testForCarts()
    {
        int meta = k();
        List<EntityMinecart> carts = CartTools.getMinecartsOnAllSides(world, x, y, z, 0);
        if(meta == Detector.STORAGE.getMeta()) {
            for(EntityMinecart cart : carts) {
                if(cart.isStorageCart()) {
                    return true;
                }
            }
        } else if(meta == Detector.ANY.getMeta()) {
            return !carts.isEmpty();
        } else if(meta == Detector.EMPTY.getMeta()) {
            for(EntityMinecart cart : carts) {
                if(cart.canBeRidden() && cart.passenger == null) {
                    return true;
                }
            }
        } else if(meta == Detector.MOB.getMeta()) {
            for(EntityMinecart cart : carts) {
                if(cart.passenger != null && cart.passenger instanceof IMonster) {
                    return true;
                }
            }
        } else if(meta == Detector.POWERED.getMeta()) {
            for(EntityMinecart cart : carts) {
                return cart.isPoweredCart();
            }
        } else if(meta == Detector.PLAYER.getMeta()) {
            for(EntityMinecart cart : carts) {
                return cart.passenger instanceof EntityHuman;
            }
        } else if(meta == Detector.EXPLOSIVE.getMeta()) {
            for(EntityMinecart cart : carts) {
                return cart.getEntityData().getBoolean("Explosive");
            }
        } else if(meta == Detector.ENERGY.getMeta()) {
            for(EntityMinecart cart : carts) {
                return cart instanceof EntityCartEnergy;
            }
        } else if(meta == Detector.GROWTH.getMeta()) {
            for(EntityMinecart cart : carts) {
                if(cart.passenger instanceof EntityAnimal) {
                    EntityAnimal animal = (EntityAnimal)cart.passenger;
                    return !animal.isBaby();
                }
            }
        }
        return false;
    }

    public boolean blockActivated(EntityHuman player)
    {
        return false;
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);
        data.setByte("direction", direction);
        data.setBoolean("powered", powered);
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);

        SafeNBTWrapper safe = new SafeNBTWrapper(data);

        direction = safe.getByte("direction");
        powered = data.getBoolean("powered");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(powered);
        data.writeByte(direction);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        powered = data.readBoolean();
        direction = data.readByte();

        world.notify(x, y, z);
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        update++;
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        if(getUpdateInterval() == 0 || update % getUpdateInterval() == 0) {
            boolean cartDetected = testForCarts();
            if(cartDetected != powered) {
                powered = cartDetected;
                world.notify(x, y, z);
                world.applyPhysics(x, y, z, RailcraftBlocks.getBlockDetector().id);
                GeneralTools.notifyBlocksOfNeighborChangeOnSide(world, x, y, z, RailcraftBlocks.getBlockDetector().id, direction);
            }
        }
    }

    protected short getUpdateInterval()
    {
        return 0;
    }

    @Override
    public boolean wrenchCanSetFacing(EntityHuman player, int side)
    {
        if(direction == side) {
            return false;
        }
        return true;
    }

    @Override
    public void setFacing(short facing)
    {
        direction = (byte)facing;
        world.notify(x, y, z);
    }

    @Override
    public short getFacing()
    {
        return (short)direction;
    }

    @Override
    public boolean wrenchCanRemove(EntityHuman entityPlayer)
    {
        return true;
    }

    @Override
    public float getWrenchDropRate()
    {
        return 0;
    }

    @Override
    public int getId()
    {
        return k();
    }
}
