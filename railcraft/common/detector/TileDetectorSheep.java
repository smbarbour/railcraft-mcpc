package railcraft.common.detector;

import java.util.List;
import net.minecraft.server.*;
import railcraft.GuiHandler;
import railcraft.common.EnumGui;
import railcraft.common.api.CartTools;

public class TileDetectorSheep extends TileDetectorItem
{

    public TileDetectorSheep()
    {
        super("Detector - Sheep", 1);
    }

    @Override
    public boolean testForCarts()
    {
        List<EntityMinecart> carts = CartTools.getMinecartsOnAllSides(world, x, y, z, 0);
        for(EntityMinecart cart : carts) {
            if(cart.passenger instanceof EntitySheep) {
                EntitySheep sheep = (EntitySheep)cart.passenger;
                ItemStack wool = getItem(0);
                if(!sheep.isBaby() && !sheep.isSheared() && (wool == null || sheep.getColor() == wool.getData())) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean blockActivated(EntityHuman player)
    {
        GuiHandler.openGui(EnumGui.DETECTOR_SHEEP, player, world, x, y, z);
        return true;
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
