package railcraft.common.detector;

import net.minecraft.server.ItemBlock;
import net.minecraft.server.ItemStack;
import railcraft.common.RailcraftBlocks;

public class ItemDetector extends ItemBlock
{

    public ItemDetector(int id)
    {
        super(id);
        setMaxDurability(0);
        a(true);
    }

    public int getIconFromDamage(int meta)
    {
        return RailcraftBlocks.getBlockDetector().a(2, meta);
    }

    @Override
    public int filterData(int meta)
    {
        return meta;
    }

    @Override
    public String a(ItemStack itemstack)
    {
        return BlockDetector.getBlockNameFromMetadata(itemstack.getData());
    }
}
