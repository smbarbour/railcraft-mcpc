package railcraft.common.detector;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public abstract class TileDetectorItem extends TileDetector implements IInventory
{
private final String invName;
    private ItemStack[] contents;

    protected TileDetectorItem(String invName, int invSize)
    {
        this.invName = invName;
        contents = new ItemStack[invSize];
    }

    @Override
    public void g()
    {
    }

    @Override
    public ItemStack splitStack(int i, int j)
    {
        if(contents[i] != null) {
            if(contents[i].count <= j) {
                ItemStack itemstack = contents[i];
                contents[i] = null;
                update();
                return itemstack;
            }
            ItemStack itemstack1 = contents[i].a(j);
            if(contents[i].count == 0) {
                contents[i] = null;
            }
            update();
            return itemstack1;
        } else {
            return null;
        }
    }

    @Override
    public String getName()
    {
        return invName;
    }

    @Override
    public int getMaxStackSize()
    {
        return 64;
    }

    @Override
    public int getSize()
    {
        return contents.length;
    }

    @Override
    public ItemStack getItem(int i)
    {
        return contents[i];
    }

    @Override
    public ItemStack splitWithoutUpdate(int slot)
    {
        if(contents[slot] != null) {
            ItemStack stack = contents[slot];
            contents[slot] = null;
            return stack;
        } else {
            return null;
        }
    }

    @Override
    public boolean a(EntityHuman entityplayer)
    {
        return true;
    }

    @Override
    public void f()
    {
    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);
        NBTTagList nbttaglist = nbttagcompound.getList("Items");
        contents = new ItemStack[getSize()];
        for(int i = 0; i < nbttaglist.size(); i++) {
            NBTTagCompound nbttagcompound1 = (NBTTagCompound)nbttaglist.get(i);
            int j = nbttagcompound1.getByte("Slot") & 255;
            if(j >= 0 && j < contents.length) {
                contents[j] = ItemStack.a(nbttagcompound1);
            }
        }
    }

    @Override
    public void setItem(int i, ItemStack itemstack)
    {
        contents[i] = itemstack;
        if(itemstack != null && itemstack.count > getMaxStackSize()) {
            itemstack.count = getMaxStackSize();
        }
        update();
    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);
        NBTTagList nbttaglist = new NBTTagList();
        for(int i = 0; i < contents.length; i++) {
            if(contents[i] != null) {
                NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                nbttagcompound1.setByte("Slot", (byte)i);
                contents[i].save(nbttagcompound1);
                nbttaglist.add(nbttagcompound1);
            }
        }
        nbttagcompound.set("Items", nbttaglist);
    }
}
