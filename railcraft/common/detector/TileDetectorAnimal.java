package railcraft.common.detector;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;
import net.minecraft.server.*;
import railcraft.GuiHandler;
import railcraft.common.EnumGui;
import railcraft.common.api.CartTools;

// Referenced classes of package net.minecraft.src:
//            TileEntity, NBTTagCompound, World
public class TileDetectorAnimal extends TileDetector
{

    public boolean chicken = true;
    public boolean cow = true;
    public boolean pig = true;
    public boolean sheep = true;
    public boolean mooshroom = true;
    public boolean wolf = true;
    public boolean other = true;

    public TileDetectorAnimal()
    {
    }

    @Override
    public boolean testForCarts()
    {
        List<EntityMinecart> carts = CartTools.getMinecartsOnAllSides(world, x, y, z, 0);
        for(EntityMinecart cart : carts) {
            if(cart.passenger instanceof EntityChicken) {
                if(chicken) {
                    return true;
                }
            } else if(cart.passenger instanceof EntityCow) {
                if(cow) {
                    return true;
                }
            } else if(cart.passenger instanceof EntityPig) {
                if(pig) {
                    return true;
                }
            } else if(cart.passenger instanceof EntitySheep) {
                if(sheep) {
                    return true;
                }
            } else if(cart.passenger instanceof EntityMushroomCow) {
                if(mooshroom) {
                    return true;
                }
            } else if(cart.passenger instanceof EntityWolf) {
                if(wolf) {
                    return true;
                }
            } else if(cart.passenger instanceof EntityAnimal) {
                if(other) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean blockActivated(EntityHuman player)
    {
        GuiHandler.openGui(EnumGui.DETECTOR_ANIMAL, player, world, x, y, z);
        return true;
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);

        data.setBoolean("chicken", chicken);
        data.setBoolean("cow", cow);
        data.setBoolean("pig", pig);
        data.setBoolean("sheep", sheep);
        data.setBoolean("mooshroom", mooshroom);
        data.setBoolean("wolf", wolf);
        data.setBoolean("other", other);
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);

        chicken = data.getBoolean("chicken");
        cow = data.getBoolean("cow");
        pig = data.getBoolean("pig");
        sheep = data.getBoolean("sheep");
        mooshroom = data.getBoolean("mooshroom");
        wolf = data.getBoolean("wolf");
        other = data.getBoolean("other");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        byte bits = 0;
        bits |= chicken ? 1 : 0;
        bits |= cow ? 2 : 0;
        bits |= pig ? 4 : 0;
        bits |= sheep ? 8 : 0;
        bits |= mooshroom ? 16 : 0;
        bits |= wolf ? 32 : 0;
        bits |= other ? 64 : 0;

        data.writeByte(bits);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        byte bits = data.readByte();
        chicken = (bits & 1) != 0;
        cow = (bits & 2) != 0;
        pig = (bits & 4) != 0;
        sheep = (bits & 8) != 0;
        mooshroom = (bits & 16) != 0;
        wolf = (bits & 32) != 0;
        other = (bits & 64) != 0;
    }
}
