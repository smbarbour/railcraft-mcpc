package railcraft.common.detector;

import net.minecraft.server.*;
import railcraft.common.util.slots.SlotLiquidFilter;
import railcraft.common.util.slots.SlotLiquidContainerSingle;

public class ContainerDetectorTank extends Container
{

    public TileDetectorTank tile;

    public ContainerDetectorTank(PlayerInventory inventoryplayer, TileDetectorTank tile)
    {
    	this.setPlayer(inventoryplayer.player);
        this.tile = tile;
        a(new SlotLiquidFilter(tile, 0, 60, 24));
//        addSlot(new Slot(tile, 0, 60, 24));
        for(int i = 0; i < 3; i++) {
            for(int k = 0; k < 9; k++) {
                a(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 58 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++) {
            a(new Slot(inventoryplayer, j, 8 + j * 18, 116));
        }
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        return tile.a(entityplayer);
    }

    @Override
    public ItemStack a(int i)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)e.get(i);
        if(slot != null && slot.c()) {
            ItemStack itemstack1 = slot.getItem();
            itemstack = itemstack1.cloneItemStack();
            if(i >= 1 && SlotLiquidContainerSingle.canPlaceItem(itemstack1)) {
                if(!a(itemstack1, 0, 1, false)) {
                    return null;
                }
            } else if(i >= 1 && i < 28) {
                if(!a(itemstack1, 28, 37, false)) {
                    return null;
                }
            } else if(i >= 28 && i < 37) {
                if(!a(itemstack1, 1, 28, false)) {
                    return null;
                }
            } else if(!a(itemstack1, 1, 37, false)) {
                return null;
            }
            if(itemstack1.count == 0) {
                slot.set(null);
            } else {
                slot.d();
            }
            if(itemstack1.count != itemstack.count) {
                slot.c(itemstack1);
            } else {
                return null;
            }
        }
        return itemstack;
    }
    
    public IInventory getInventory() {
    	return tile;
    }
}
