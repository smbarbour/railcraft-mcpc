package railcraft.common;

import cpw.mods.fml.common.FMLCommonHandler;
import java.io.File;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import net.minecraft.server.LocaleLanguage;
import forge.Configuration;
import forge.Property;
import railcraft.Railcraft;
import railcraft.common.carts.EnumCart;
import railcraft.common.cube.EnumCube;
import railcraft.common.detector.BlockDetector;
import railcraft.common.detector.BlockDetector.Detector;
import railcraft.common.tracks.EnumTrack;
import railcraft.common.structures.EnumStructure;
import railcraft.common.utility.EnumUtility;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class RailcraftLanguage
{

    public static final String LANGUAGE_FILE = "language.cfg";
    public static final String ENGLISH = "en_US";
    private Configuration languageFile;
    private Map<String, Map<String, String>> languageTable  = new TreeMap<String, Map<String, String>>();
    private static RailcraftLanguage instance;

    public RailcraftLanguage()
    {
        File file = new File(Railcraft.getConfigDir(), LANGUAGE_FILE);
        languageFile = new Configuration(file);

        languageFile.categories.remove(Configuration.CATEGORY_GENERAL);
        languageFile.categories.remove(Configuration.CATEGORY_BLOCK);
        languageFile.categories.remove(Configuration.CATEGORY_ITEM);

        languageFile.load();

        languageFile.categories.remove("english");

        loadLanguage(ENGLISH);

//        for(String lang : languageFile.categories.keySet()) {
//            if(lang.equals("en.us")) {
//                continue;
//            }
//            String[] temp = lang.split("\\.");
//            lang = temp[0] + "_" + temp[1].toUpperCase();
//            loadLanguage(lang);
//        }

//        languageFile.save();
    }

    public static RailcraftLanguage getInstance()
    {
        if(instance == null) {
            instance = new RailcraftLanguage();
        }
        return instance;
    }

    private void loadLanguage(String language)
    {
        for(EnumTrack type : EnumTrack.values()) {
            registerTag(type.getTag(), language, type.getName());
        }
        registerTag("track.elevator", language, "Elevator Track");
        registerTag("track.standard", language, "Track");
        registerTag("track.powered", language, "Booster Track");
        registerTag("track.detector", language, "Detector Track");

        for(EnumUtility type : EnumUtility.values()) {
            registerTag(type.getTag(), language, type.getName());
        }

        for(EnumStructure type : EnumStructure.values()) {
            registerTag(type.getTag(), language, type.getName());
        }

        for(EnumCube type : EnumCube.values()) {
            registerTag(type.getTag(), language, type.getName());
        }

        for(EnumCart type : EnumCart.values()) {
            registerTag(type.getTag(), language, type.getName());
        }
        registerTag("entity.cart.bore", language, "Tunnel Bore");

        registerTag("item.bore.head.diamond", language, "Diamond Bore Head");
        registerTag("item.bore.head.iron", language, "Iron Bore Head");
        registerTag("item.bore.head.steel", language, "Steel Bore Head");

        for(Detector d : Detector.values()) {
            String tag = BlockDetector.getBlockNameFromMetadata(d.getMeta());
            StringBuilder name = new StringBuilder("Detector - ");
            name.append(d.name().charAt(0)).append(d.name().substring(1).toLowerCase());
            registerTag(tag, language, name.toString());
        }

        for(EnumColor c : EnumColor.values()) {
            registerTag(EnumStructure.METAL_POST.getTag() + "." + c.getBasicTag(), language, c.toString() + " Metal Post");
        }

        registerTag("part.rail.standard", language, "Standard Rail");
        registerTag("part.rail.advanced", language, "Advanced Rail");
        registerTag("part.rail.speed", language, "H.S. Rail");
        registerTag("part.rail.reinforced", language, "Reinforced Rail");

        registerTag("part.signal.lamp", language, "Signal Lamp");

        registerTag("part.rebar", language, "Rebar");

        registerTag("part.tie.wood", language, "Wooden Tie");
        registerTag("part.tie.stone", language, "Stone Tie");

        registerTag("part.railbed.wood", language, "Wooden Railbed");
        registerTag("part.railbed.stone", language, "Stone Railbed");

        registerTag("part.circuit.controller", language, "Controller Circuit");
        registerTag("part.circuit.receiver", language, "Receiver Circuit");

        registerTag("part.ingot.steel", language, "Steel Ingot");

        registerTag("part.dust.obsidian", language, "Obsidian Dust");

        registerTag("item.crowbar", language, "Crowbar");

        registerTag("item.tuner", language, "Signal Tuner");
        registerTag("item.surveyor", language, "Signal Block Surveyor");

        registerTag("item.coal.coke", language, "Coal Coke");

        registerTag("item.ic2.upgrade.lapotron", language, "Lapotron Loader Upgrade");

        registerTag("item.creosote.liquid", language, "Creosote Oil");
        registerTag("item.creosote.bucket", language, "Creosote Bucket");
        registerTag("item.creosote.bottle", language, "Creosote Bottle");
        registerTag("item.creosote.can", language, "Creosote Can");
        registerTag("item.creosote.wax", language, "Creosote Capsule");
        registerTag("item.creosote.refactory", language, "Creosote Capsule");

        registerTag("gui.coke.oven", language, "Coke Oven");
        registerTag("gui.blast.furnace", language, "Blast Furnace");
        registerTag("gui.tank.water", language, "Water Tank");
        registerTag("gui.filter", language, "Filter");
        registerTag("gui.filters", language, "Filters");
        registerTag("gui.liquid.loader.empty", language, "Hold Empty");
        registerTag("gui.liquid.loader.fill", language, "Top Off");
        registerTag("gui.energy.loader.empty", language, "Hold Empty");
        registerTag("gui.energy.loader.fill", language, "Top Off");
        registerTag("gui.liquid.unloader.empty", language, "Empty");

        registerTag("gui.bore.head", language, "Head");
        registerTag("gui.bore.fuel", language, "Fuel");
        registerTag("gui.bore.ballast", language, "Ballast");
        registerTag("gui.bore.track", language, "Track");


        registerTag("gui.cart.track.relayer.pattern", language, "Pattern");
        registerTag("gui.cart.track.relayer.stock", language, "Stock");
        registerTag("gui.cart.undercutter.pattern", language, "Pattern");
        registerTag("gui.cart.undercutter.stock", language, "Stock");

        languageFile.save();
    }

    public static String translate(String tag)
    {
        return getInstance().translate_do(tag);
    }

    private String translate_do(String tag)
    {
        String lang = LocaleLanguage.a().getCurrentLanguage();
        return translate(tag, lang);
    }

    public String translate(String tag, String lang)
    {
        Map<String, String> languageSet = languageTable.get(lang);
        if(languageSet == null) {
            languageSet = new TreeMap<String, String>();
            languageTable.put(lang, languageSet);
            loadLanguage(lang);
        }
        String value = languageSet.get(tag);
        if(value == null){
            loadLanguage(lang);
            value = languageSet.get(tag);
        }
        if(value == null) {
            Railcraft.log(Level.WARNING, "Language Tag is unknown: {0}, {1}", lang, tag);
            if(!lang.equals(ENGLISH)) {
                return translate(tag, ENGLISH);
            }
            return tag;
        }
        return value;
    }

    private void registerTag(String tag, String lang, String defaultName)
    {
        Map<String, String> tags = languageTable.get(lang);
        if(tags == null) {
            tags = new TreeMap<String, String>();
            languageTable.put(lang, tags);
        }
        Property prop = languageFile.getOrCreateProperty(tag, lang.replace('_', '.'), defaultName);
        tags.put(tag, prop.value);
    }

    public void registerItemName(Object o, String tag)
    {
        for(String lang : languageFile.categories.keySet()) {
            String[] temp = lang.split("\\.");
            lang = temp[0] + "_" + temp[1].toUpperCase();
            FMLCommonHandler.instance().addNameForObject(o, lang, translate(tag, lang));
        }
    }
}
