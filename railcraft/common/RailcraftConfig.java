package railcraft.common;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import net.minecraft.server.Block;
import net.minecraft.server.Item;
import forge.Configuration;
import forge.Property;
import railcraft.Railcraft;
import railcraft.common.carts.EntityTunnelBore;
import railcraft.common.cube.EnumCube;
import railcraft.common.util.liquids.LiquidManager;
import railcraft.common.structures.EnumStructure;
import railcraft.common.tracks.EnumTrack;
import railcraft.common.utility.EnumUtility;

public class RailcraftConfig
{

    private static final String COMMENT_PREFIX = "\n\n   # ";
    private static final String CATEGORY_LOOT = "dungeon.loot";
    private static final String CATEGORY_ENTITY = "entity";
    private static final String CATEGORY_SUB_BLOCKS = "block.feature";
    private static boolean needsInit = true;
    private static int nextItemId = 7000;
    private static Set<Integer> usedItemIds = new HashSet<Integer>();
    private static Map<String, Integer> itemIDs = new HashMap<String, Integer>();
    private static Map<String, String> newItemTags = new TreeMap<String, String>();
    private static Map<String, Integer> blockIDs = new HashMap<String, Integer>();
    private static Map<String, Integer> entityIDs = new HashMap<String, Integer>();
    private static Map<String, Boolean> enabledSubBlocks = new HashMap<String, Boolean>();
    private static final Map<String, Float> lootChances = new HashMap<String, Float>();
    private static float maxHighSpeed = 1.1f;
    private static boolean useOldRecipes;
    private static boolean useAltCreosoteRecipes;
    private static boolean boreDestroysBlocks;
    private static boolean boreMinesAllBlocks;
    private static boolean defineDictionaryRecipes;
    private static boolean printSignalDebug;
    private static byte printAnchorDebug;
    private static boolean loadAnchorsOnStart;
    private static boolean minecartsBreakOnDrop;
    private static boolean adjustBasicCartDrag;
    private static boolean minecartsCollideWithItems;
    private static boolean minecartsCollideWithItemsHighSpeed;
    private static boolean registerCollisionHandler;
    private static boolean playSounds;
    private static int minecartTankCapacity = 16;
    private static int minecartTankFillRate = 25;
    private static int launchRailMaxForce;
    private static int cartDispenserDelay;
    private static int minecartStackSize;

    private static void checkInit()
    {
        if(needsInit) {
            Configuration config = new Configuration(Railcraft.getConfigFile());
            config.load();

            useOldRecipes = loadBooleanProperty(config, "tweaks.rails.useOldRecipes", false, "change to '{t}=true' to use recipes more similar to vanilla minecraft");
            defineDictionaryRecipes = loadBooleanProperty(config, "tweaks.rails.defineDictionaryRecipes", true, "change to '{t}=false' to prevent the game from defining alternate recipes using the Forge Ore Dictionary");
            useAltCreosoteRecipes = loadBooleanProperty(config, "tweaks.creosote.enableFurnaceRecipes", false, "change to '{t}=true' to add smelting recipes for Creosote Oil to the vanilla furnace");

            boreDestroysBlocks = loadBooleanProperty(config, "tweaks.minecarts.bore.destroyBlocks", false, "change to '{t}=true' to cause the Bore to destroy the blocks it mines instead of dropping them");
            boreMinesAllBlocks = loadBooleanProperty(config, "tweaks.minecarts.bore.mineAllBlocks", true, "change to '{t}=false' to enable mining checks, use true setting with caution, especially on servers");

            printSignalDebug = loadBooleanProperty(config, "tweaks.signals.printDebug", false, "change to '{t}=true' to log debug info for Signal Blocks");
            printAnchorDebug = (byte)loadIntegerProperty(config, "tweaks.anchor.printDebug", -1, "change to 1, 2, 3, or 4 to log debug info for Anchor Blocks and Carts, higher = finer log detail");
            loadAnchorsOnStart = loadBooleanProperty(config, "tweaks.anchor.loadOnStart", true, "change to '{t}=false' to disable force loading of chunks with Anchors when the game starts");
            minecartsBreakOnDrop = loadBooleanProperty(config, "tweaks.minecarts.breakOnDrop", false, "change to '{t}=true' to restore vanilla behavior");
            adjustBasicCartDrag = loadBooleanProperty(config, "tweaks.minecarts.basic.adjustDrag", true, "change to '{t}=true' to give empty basic carts the same drag as a ridden cart, disabled by default, after changing you must replace the carts to see any change in game");
            minecartsCollideWithItems = loadBooleanProperty(config, "tweaks.minecarts.collideWithItems", false, "change to '{t}=true' to restore minecart collisions with dropped items, ignored if 'register.handler.collision=false'");
            minecartsCollideWithItemsHighSpeed = loadBooleanProperty(config, "tweaks.minecarts.highspeed.collideWithItems", true, "change to '{t}=false' to prevent minecart collisions with dropped items at high speed, ignored if 'register.handler.collision=false'");
            playSounds = loadBooleanProperty(config, "tweaks.sounds.play", true, "change to '{t}=false' to prevent all mod sounds from playing");

            Property prop = config.getOrCreateProperty("tweaks.minecarts.highspeed.max", Configuration.CATEGORY_GENERAL, "1.0");
            prop.comment = COMMENT_PREFIX + "change 'tweaks.minecarts.highspeed.max' to limit max speed on high speed rails, useful if your computer can't keep up with chunk loading, min=0.4, default=1.0, max=1.2";
            maxHighSpeed = Float.valueOf(prop.value);
            maxHighSpeed = Math.min(1.2f, maxHighSpeed);
            maxHighSpeed = Math.max(0.4f, maxHighSpeed);
            prop.value = Float.toString(maxHighSpeed);

            prop = loadProperty(config, "tweaks.rails.launch.force.max", "30", "change the value to your desired max launch rail force, min=5, default=30, max=50");
            launchRailMaxForce = Integer.valueOf(prop.value);
            launchRailMaxForce = Math.max(launchRailMaxForce, 5);
            launchRailMaxForce = Math.min(launchRailMaxForce, 50);
            prop.value = Integer.toString(launchRailMaxForce);

            prop = loadProperty(config, "tweaks.utility.cart.dispenser.delay", "0", "set the minimum number of seconds between cart dispensing, default=0");
            cartDispenserDelay = Integer.valueOf(prop.value);
            cartDispenserDelay = Math.max(cartDispenserDelay, 0);
            prop.value = Integer.toString(cartDispenserDelay);

            prop = loadProperty(config, "tweaks.minecarts.maxStackSize", "3", "change the value to your desired minecart stack size, vanilla=1, default=3, max=64");
            minecartStackSize = Integer.valueOf(prop.value);
            minecartStackSize = Math.max(minecartStackSize, 1);
            minecartStackSize = Math.min(minecartStackSize, 64);
            prop.value = Integer.toString(minecartStackSize);

            boolean minecartTankCustomize = loadBooleanProperty(config, "tweaks.minecarts.tank.useCustomValues", false, "change to '{t}=true' to adjust the Tank Cart's capacity and fill rate");

            prop = loadProperty(config, "tweaks.minecarts.tank.capacity", "16", "change the value to your desired Tank Cart capacity in buckets, min=1, default=16, max=32, ignored if 'tweaks.minecarts.tank.useCustomValues=false'");
            if(minecartTankCustomize) {
                minecartTankCapacity = Integer.valueOf(prop.value);
                minecartTankCapacity = Math.max(minecartTankCapacity, 1);
                minecartTankCapacity = Math.min(minecartTankCapacity, 32);
                prop.value = Integer.toString(minecartTankCapacity);
            }

            prop = loadProperty(config, "tweaks.minecarts.tank.fillrate", "25", " change the value to your desired Tank Cart fill rate in milli-buckets per tick, min=5, default=25, max=50 \n   # there are 1000 milli-buckets in a bucket, ignored if 'tweaks.minecarts.tank.useCustomValues=false'");
            if(minecartTankCustomize) {
                minecartTankFillRate = Integer.valueOf(prop.value);
                minecartTankFillRate = Math.max(minecartTankFillRate, 5);
                minecartTankFillRate = Math.min(minecartTankFillRate, 50);
                prop.value = Integer.toString(minecartTankFillRate);
            }

            registerCollisionHandler = loadBooleanProperty(config, "register.handler.collision", true, "change to '{t}=false' to use a minecart collision handler from a different mod or vanilla behavior");

            loadEntities(config);
            loadBlocks(config);
            loadItems(config);
            loadBoreMineableBlocks(config);
            loadLoot(config);

            config.save();
        }
        needsInit = false;
    }

    private static void loadLoot(Configuration config)
    {
        loadLootProperty(config, "crowbar", 0.1f);
        loadLootProperty(config, "tie.wood", 0.2f);
        loadLootProperty(config, "cart.basic", 0.1f);
        loadLootProperty(config, "cart.chest", 0.1f);
        loadLootProperty(config, "cart.tnt", 0.05f);
        loadLootProperty(config, "cart.work", 0.08f);
        loadLootProperty(config, "coke", 0.2f);
        loadLootProperty(config, "creosote", 0.2f);
        loadLootProperty(config, "rail", 0.3f);
        loadLootProperty(config, "steel.ingot", 0.1f);
        loadLootProperty(config, "steel.block", 0.05f);
    }

    private static void loadEntities(Configuration config)
    {
        loadEntityProperty(config, "entity.cart.basic", 136, "");
        loadEntityProperty(config, "entity.cart.chest", 119, "");
        loadEntityProperty(config, "entity.cart.furnace", 129, "");
        loadEntityProperty(config, "entity.cart.tnt", 125, "change to '{t}=0' to disable");
        loadEntityProperty(config, "entity.cart.tank", 126, "change to '{t}=0' to disable \n   # if Buildcraft is not detected there will be no recipe to make the cart");
        loadEntityProperty(config, "entity.cart.bore", 131, "change to '{t}=0' to disable");
        loadEntityProperty(config, "entity.cart.energy", 132, "change to '{t}=0' to disable \n   # if IC2 is not detected there will be no recipe to make the cart");
        loadEntityProperty(config, "entity.cart.anchor", 134, "change to '{t}=0' to disable");
        loadEntityProperty(config, "entity.cart.work", 135, "change to '{t}=0' to disable");
        loadEntityProperty(config, "entity.fakeBlock", 137, "");
        loadEntityProperty(config, "entity.falling.cube", 138, "");
        loadEntityProperty(config, "entity.cart.track.relayer", 139, "change to '{t}=0' to disable");
        loadEntityProperty(config, "entity.cart.undercutter", 140, "change to '{t}=0' to disable");
    }

    private static void loadBlocks(Configuration config)
    {
        String tag = "block.detector";
        Property prop = config.getOrCreateBlockIdProperty(tag, 202);
        prop.comment = COMMENT_PREFIX + "change to '" + tag + "=0' to disable, loader/unloader recipe will use pressure plates instead of detectors";
        blockIDs.put(tag, Integer.valueOf(prop.value));

        tag = "block.utility";
        prop = config.getOrCreateBlockIdProperty(tag, 204);
        prop.comment = COMMENT_PREFIX + "change to '" + tag + "=0' to disable, this will disable ALL Loaders, Dispenser, Coke Oven, etc... \n   # to disable individual utility blocks, see the block.feature section below";
        blockIDs.put(tag, Integer.valueOf(prop.value));

        tag = "block.track";
        prop = config.blockProperties.remove("block.rail.advanced");
        if(prop != null) {
            prop.name = tag;
            config.blockProperties.put(tag, prop);
        }
        prop = config.getOrCreateBlockIdProperty(tag, 205);
        prop.comment = COMMENT_PREFIX + "change to '" + tag + "=0' to disable, this will disable ALL added rails except the elevator rail \n   # to disable individual rails, see the block.feature section below";
        blockIDs.put(tag, Integer.valueOf(prop.value));

        tag = "block.elevator";
        prop = config.getOrCreateBlockIdProperty(tag, 207);
        prop.comment = COMMENT_PREFIX + "change to '" + tag + "=0' to disable";
        blockIDs.put(tag, Integer.valueOf(prop.value));

        tag = "block.structure";
        prop = config.getOrCreateBlockIdProperty(tag, 208);
        prop.comment = COMMENT_PREFIX + "change to '" + tag + "=0' to disable, this will disable ALL added structures \n   # to disable individual structures, see the block.feature section below";
        blockIDs.put(tag, Integer.valueOf(prop.value));

        tag = "block.cube";
        prop = config.getOrCreateBlockIdProperty(tag, 209);
        prop.comment = COMMENT_PREFIX + "change to '" + tag + "=0' to disable, this will disable ALL added cubes";
        blockIDs.put(tag, Integer.valueOf(prop.value));

        for(EnumTrack type : EnumTrack.values()) {
            tag = type.getTag();
            prop = config.getOrCreateBooleanProperty(tag, CATEGORY_SUB_BLOCKS, true);
            enabledSubBlocks.put(tag, Boolean.valueOf(prop.value));
        }

        for(EnumCube type : EnumCube.values()) {
            tag = type.getTag();
            prop = config.getOrCreateBooleanProperty(tag, CATEGORY_SUB_BLOCKS, true);
            enabledSubBlocks.put(tag, Boolean.valueOf(prop.value));
        }

        for(EnumUtility type : EnumUtility.values()) {
            tag = type.getTag();
            prop = config.getOrCreateBooleanProperty(tag, CATEGORY_SUB_BLOCKS, true);
            enabledSubBlocks.put(tag, Boolean.valueOf(prop.value));
        }

        for(EnumStructure type : EnumStructure.values()) {
            tag = type.getTag();
            prop = config.getOrCreateBooleanProperty(tag, CATEGORY_SUB_BLOCKS, true);
            enabledSubBlocks.put(tag, Boolean.valueOf(prop.value));
        }
    }

    private static void loadItems(Configuration config)
    {
        loadItemProperty(config, "item.crowbar", "change to '{t}=0' to disable");

        loadItemProperty(config, "item.surveyor", "change to '{t}=0' to disable");

        loadItemProperty(config, "item.signal.tuner", "change to '{t}=0' to disable");

        Property prop = config.itemProperties.remove("item.creosote");
        if(prop != null) {
            prop.name = "item.creosote.bottle";
            config.itemProperties.put("item.creosote.bottle", prop);
        }
        loadItemProperty(config, "item.creosote.bottle");
        loadItemProperty(config, "item.creosote.liquid");
        loadItemProperty(config, "item.creosote.can");
        loadItemProperty(config, "item.creosote.wax");
        loadItemProperty(config, "item.creosote.refactory");
        loadItemProperty(config, "item.creosote.bucket");

//        loadItemProperty(config, "item.creosote.wood", "change to '{t}=0' to disable");

        loadItemProperty(config, "item.lamp");

        loadItemProperty(config, "item.circuit.controller");
        loadItemProperty(config, "item.circuit.receiver");

        loadItemProperty(config, "item.coke", "change to '{t}=0' to disable");

        loadItemProperty(config, "part.rail.standard");
        loadItemProperty(config, "part.rail.advanced");
        loadItemProperty(config, "part.rail.speed");
        loadItemProperty(config, "part.rail.reinforced");

        loadItemProperty(config, "part.rebar", "change to '{t}=0' to disable, iron ingots will be used instead for all recipes");

        loadItemProperty(config, "part.tie.wood");
        loadItemProperty(config, "part.tie.stone");

        loadItemProperty(config, "part.ingot.steel");

        loadItemProperty(config, "part.dust.obsidian");

        prop = config.itemProperties.remove("rail.roadbed.wood");
        if(prop != null) {
            prop.name = "part.railbed.wood";
            config.itemProperties.put("part.railbed.wood", prop);
        }
        loadItemProperty(config, "part.railbed.wood", "ignored if 'tweaks.rails.useOldRecipes=true'");

        prop = config.itemProperties.remove("rail.roadbed.stone");
        if(prop != null) {
            prop.name = "part.railbed.stone";
            config.itemProperties.put("part.railbed.stone", prop);
        }
        loadItemProperty(config, "part.railbed.stone", "ignored if 'tweaks.rails.useOldRecipes=true'");

        loadItemProperty(config, "item.ic2.upgrade.lapotron", "change to '{t}=0' to disable");

        loadItemProperty(config, "cart.bore.head.diamond", "ignored if 'entity.cart.bore=0'");
        loadItemProperty(config, "cart.bore.head.iron", "ignored if 'entity.cart.bore=0'");
        loadItemProperty(config, "cart.bore.head.steel", "ignored if 'entity.cart.bore=0'");

//        Iterator<Map.Entry<String, Property>> it = config.itemProperties.entrySet().iterator();
//        while(it.hasNext()){
//            Map.Entry<String, Property> entry = it.next();
//            if(entry.getKey().startsWith("rail.")){
//                Property prop = entry.getValue();
//                it.remove();
//            }
//        }

        // Get Rail Tags
//        for(EnumTrack r : EnumTrack.values()) {
//            String newTag = r.getTag();
//            String oldTag = newTag.replace("track", "rail");
//            prop = config.itemProperties.remove(oldTag);
//            if(prop != null) {
//                prop.name = newTag;
//                config.itemProperties.put(newTag, prop);
//            }
//            loadItemProperty(config, newTag, "change to '{t}=0' to disable");
//        }

        // Get Structure Tags
//        for(EnumStructure s : EnumStructure.values()) {
//            String tag = s.getTag();
//            loadItemProperty(config, tag, "change to '{t}=0' to disable");
//        }

        loadItemProperty(config, EnumStructure.METAL_POST.getTag(), "");

        String tag = "item.cart.tnt";
        prop = config.itemProperties.remove("cart.tnt");
        if(prop != null) {
            prop.name = tag;
            config.itemProperties.put(tag, prop);
        }
        loadItemProperty(config, "item.cart.tnt", "ignored if 'entity.cart.tnt=0'");

        tag = "item.cart.tank";
        prop = config.itemProperties.remove("cart.tank");
        if(prop != null) {
            prop.name = tag;
            config.itemProperties.put(tag, prop);
        }
        loadItemProperty(config, tag, "ignored if 'entity.cart.tnt=0'");

        tag = "item.cart.bore";
        prop = config.itemProperties.remove("cart.bore");
        if(prop != null) {
            prop.name = tag;
            config.itemProperties.put(tag, prop);
        }
        loadItemProperty(config, tag, "ignored if 'entity.cart.bore=0'");

        loadItemProperty(config, "item.cart.batbox", "ignored if 'entity.cart.energy=0'");
        loadItemProperty(config, "item.cart.mfe", "ignored if 'entity.cart.energy=0'");
        loadItemProperty(config, "item.cart.mfsu", "ignored if 'entity.cart.energy=0'");

        loadItemProperty(config, "item.cart.anchor", "ignored if 'entity.cart.anchor=0'");
        loadItemProperty(config, "item.cart.work", "ignored if 'entity.cart.work=0'");
        loadItemProperty(config, "item.cart.track.relayer", "ignored if 'entity.cart.track.relayer=0'");
        loadItemProperty(config, "item.cart.undercutter", "ignored if 'entity.cart.undercutter=0'");

        createNewItemProperties(config);
    }

    public static void loadBoreMineableBlocks(Configuration config)
    {
        String tag = "tweaks.minecarts.bore.mineableBlocks";
        Property prop = loadProperty(config, tag, "{}", "add block ids to '{t}' in a common seperated list to define non-vanilla blocks mineable by the tunnel bore \n   # ignored if 'tweaks.minecarts.bore.mineAllBlocks=true' \n   # metadata sensative entries can be defined in the form 'blockid:metadata' \n   # Example:{t}= { 123, 134:3 }");
        try {
            for(String segment : prop.value.replaceAll("[{} ]", "").split(",")) {
                if(segment.equals("")) {
                    continue;
                }
                String[] entry = segment.split(":");
                int block = Integer.valueOf(entry[0]);
                if(block <= 0 || block >= Block.byId.length) {
                    throw new Exception("Invalid Block ID = " + block);
                }
                int meta = entry.length > 1 ? Integer.valueOf(entry[1]) : -1;
                EntityTunnelBore.addMineableBlock(block, meta);

            }
        } catch (Exception ex) {
            Railcraft.log(Level.SEVERE, "Error in railcraft.cfg with {0}", tag);
            throw new RailcraftInstallationException(Railcraft.getNameAndVersion() + ": Configuration Error!"
                + "\n************************************************************************************"
                + "\n In railcraft.cfg there was an error with " + tag + "."
                + "\n " + ex.getMessage()
                + "\n************************************************************************************"
                + "\n");
        }
    }

    public static boolean useOldRecipes()
    {
        checkInit();
        return useOldRecipes;
    }

    public static boolean defineDictionaryRecipes()
    {
        checkInit();
        return defineDictionaryRecipes;
    }

    public static boolean boreDestroysBlocks()
    {
        checkInit();
        return boreDestroysBlocks;
    }

    public static boolean boreMinesAllBlocks()
    {
        checkInit();
        return boreMinesAllBlocks;
    }

    public static boolean printSignalDebug()
    {
        checkInit();
        return printSignalDebug;
    }

    public static byte printAnchorDebug()
    {
        checkInit();
        return printAnchorDebug;
    }

    public static boolean loadAnchorsOnStart()
    {
        checkInit();
        return loadAnchorsOnStart;
    }

    public static boolean doCartsBreakOnDrop()
    {
        checkInit();
        return minecartsBreakOnDrop;
    }

    public static boolean adjustBasicCartDrag()
    {
        checkInit();
        return adjustBasicCartDrag;
    }

    public static boolean doCartsCollideWithItems()
    {
        checkInit();
        return minecartsCollideWithItems;
    }

    public static boolean doCartsCollideWithItemsHighSpeed()
    {
        checkInit();
        return minecartsCollideWithItemsHighSpeed;
    }

    public static boolean useCreosoteFurnaceRecipes()
    {
        checkInit();
        return useAltCreosoteRecipes;
    }

    public static boolean useCollisionHandler()
    {
        checkInit();
        return registerCollisionHandler;
    }

    public static boolean playSounds()
    {
        checkInit();
        return playSounds;
    }

    public static float getMaxHighSpeed()
    {
        checkInit();
        return maxHighSpeed;
    }

    public static int getMinecartStackSize()
    {
        checkInit();
        return minecartStackSize;
    }

    public static int getLaunchRailMaxForce()
    {
        checkInit();
        return launchRailMaxForce;
    }

    public static int getCartDispenserMinDelay()
    {
        checkInit();
        return cartDispenserDelay;
    }

    public static int getTankCartFillRate()
    {
        checkInit();
        return minecartTankFillRate;
    }

    public static int getTankCartCapacity()
    {
        checkInit();
        return minecartTankCapacity * LiquidManager.BUCKET_VOLUME;
    }

    private static int getNextItemId()
    {
        while(Item.byId[nextItemId] != null || usedItemIds.contains(nextItemId)) {
            nextItemId++;
        }
        return nextItemId;
    }

    private static int getItemId(Property prop)
    {
        int id = Integer.valueOf(prop.value);
        if(id <= 0) {
            return 0;
        }
        usedItemIds.add(id);
        return id;
    }

    private static void loadItemProperty(Configuration config, String tag)
    {
        loadItemProperty(config, tag, "");
    }

    private static void loadItemProperty(Configuration config, String tag, String comment)
    {
        comment = COMMENT_PREFIX + comment.replace("{t}", tag);
        Property prop = config.itemProperties.get(tag);
        if(prop != null) {
            prop.comment = comment;
            itemIDs.put(tag, getItemId(prop));
        } else {
            newItemTags.put(tag, comment);
        }
    }

    private static void createNewItemProperties(Configuration config)
    {
        for(Map.Entry<String, String> entry : newItemTags.entrySet()) {
            Railcraft.log(Level.CONFIG, "RailcraftConfig: Auto-assigning item id for {0}", entry.getKey());
            Property prop = config.getOrCreateIntProperty(entry.getKey(), Configuration.CATEGORY_ITEM, getNextItemId());
            prop.comment = entry.getValue();
            itemIDs.put(entry.getKey(), getItemId(prop));
        }
    }

    public static int getItemId(String tag)
    {
        checkInit();
        Integer id = itemIDs.get(tag);
        if(id == null) {
            throw new IllegalArgumentException("RailcraftConfig: tag not found: " + tag);
        }
        return id;
    }

    public static int getBlockId(String tag)
    {
        checkInit();
        Integer id = blockIDs.get(tag);
        if(id == null) {
            return 0;
        }
        return id;
    }

    public static boolean isSubBlockEnabled(String tag)
    {
        checkInit();
        Boolean b = enabledSubBlocks.get(tag);
        if(b == null) {
            return false;
        }
        return b;
    }

    public static int getEntityId(String tag)
    {
        checkInit();
        Integer id = entityIDs.get(tag);
        if(id == null) {
            return 0;
        }
        return id;
    }

    public static float getLootChance(String tag)
    {
        checkInit();
        Float chance = lootChances.get(tag);
        if(chance == null) {
            throw new RuntimeException("Railcraft Loot Chance Entry does not exist: " + tag);
        }
        return chance;
    }

    private static boolean loadBooleanProperty(Configuration config, String tag, boolean defaultValue, String comment)
    {
        comment = COMMENT_PREFIX + comment.replace("{t}", tag);
        Property prop = config.getOrCreateBooleanProperty(tag, Configuration.CATEGORY_GENERAL, defaultValue);
        prop.comment = comment;
        return Boolean.valueOf(prop.value);
    }

    private static int loadIntegerProperty(Configuration config, String tag, int defaultValue, String comment)
    {
        comment = COMMENT_PREFIX + comment.replace("{t}", tag);
        Property prop = config.getOrCreateIntProperty(tag, Configuration.CATEGORY_GENERAL, defaultValue);
        prop.comment = comment;
        return Integer.valueOf(prop.value);
    }

    private static Property loadProperty(Configuration config, String tag, String defaultValue, String comment)
    {
        comment = COMMENT_PREFIX + comment.replace("{t}", tag);
        Property prop = config.getOrCreateProperty(tag, Configuration.CATEGORY_GENERAL, defaultValue);
        prop.comment = comment;
        return prop;
    }

    private static void loadLootProperty(Configuration config, String tag, float defaultValue)
    {
        Property prop = config.getOrCreateProperty(tag, CATEGORY_LOOT, Float.toString(defaultValue));
        lootChances.put(tag, Float.valueOf(prop.value));
    }

    private static void loadEntityProperty(Configuration config, String tag, int defaultValue, String comment)
    {
        comment = COMMENT_PREFIX + comment.replace("{t}", tag);
        Property prop = config.getOrCreateIntProperty(tag, CATEGORY_ENTITY, defaultValue);
        prop.comment = comment;
        entityIDs.put(tag, Integer.valueOf(prop.value));
    }
}
