package railcraft.common.utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;
import net.minecraft.server.*;
import forge.ISidedInventory;
import railcraft.Railcraft;
import railcraft.common.api.GeneralTools;

public abstract class TileMultiBlockOven extends TileMultiBlock implements ISidedInventory
{

    protected int cookTime;
    protected boolean cooking;
    private boolean wasBurning;
    protected int update = GeneralTools.getRand().nextInt();

    public TileMultiBlockOven(EnumUtility type, String name, int invNum)
    {
        super(type, name, invNum);
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        super.q_();
        update++;

        if(door) {
            if(update % 5 == 0) {
                updateLighting();

                if(Railcraft.gameIsHost()) {
//                    if(worldObj.blockExists(masterX, masterY, masterZ)) {
//                        TileMultiBlockOven masterOven = (TileMultiBlockOven)getMasterBlock();
//                        if(masterOven == null) {
//                            setDoor(false);
//                        }
//                    }
                    if(!isMyMasterValid()) {
                        setDoor(false);
                    }
                }
            }
        }
    }

    protected void updateLighting()
    {
        boolean b = isBurning();
        if(wasBurning != b) {
            wasBurning = b;
            world.b(EnumSkyBlock.BLOCK, x, y, z);
            world.notify(x, y, z);
        }
    }

    @Override
    public void randomDisplayTick(World world, Random random)
    {
        if(door && random.nextInt(100) < 20 && isBurning()) {
            float f = (float)x + 0.5F;
            float f1 = (float)y + 0.4375F + (random.nextFloat() * 3F / 16F);
            float f2 = (float)z + 0.5F;
            float f3 = 0.52F;
            float f4 = random.nextFloat() * 0.6F - 0.3F;
            world.a("flame", f - f3, f1, f2 + f4, 0.0D, 0.0D, 0.0D);
            world.a("flame", f + f3, f1, f2 + f4, 0.0D, 0.0D, 0.0D);
            world.a("flame", f + f4, f1, f2 - f3, 0.0D, 0.0D, 0.0D);
            world.a("flame", f + f4, f1, f2 + f3, 0.0D, 0.0D, 0.0D);
        }
    }

    @Override
    public int getBlockTexture(int side)
    {
        if(door) {
            if(isBurning()) {
                return getUtilityType().getTexture(7);
            }
            return getUtilityType().getTexture(6);
        }
        return getUtilityType().getTexture(0);
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);

        data.setInt("cookTime", cookTime);
        data.setBoolean("cooking", cooking);
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);

        cookTime = data.getInt("cookTime");
        cooking = data.getBoolean("cooking");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeInt(cookTime);
        data.writeBoolean(cooking);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        cookTime = data.readInt();
        cooking = data.readBoolean();

        world.notify(x, y, z);
    }

    public int getCookTime()
    {
        if(master || Railcraft.gameIsNotHost()) {
            return cookTime;
        }
        TileMultiBlockOven masterOven = (TileMultiBlockOven)getMasterBlock();
        if(masterOven != null) {
            return masterOven.getCookTime();
        }
        return -1;
    }

    public boolean isCooking()
    {
        if(!master) {
            TileMultiBlockOven masterOven = (TileMultiBlockOven)getMasterBlock();
            if(masterOven != null) {
                return masterOven.isCooking();
            }
            return false;
        }
        return cooking;
    }

    public boolean isBurning()
    {
        return isCooking();
    }

    public void setCooking(boolean c)
    {
        if(master) {
            if(cooking != c) {
                cooking = c;
                if(world != null && Railcraft.gameIsServer()) {
                    world.notify(x, y, z);
                }
            }
        } else {
            TileMultiBlockOven masterOven = (TileMultiBlockOven)getMasterBlock();
            if(masterOven != null) {
                masterOven.setCooking(c);
            }
        }
    }

    public void setCookTime(int i)
    {
        cookTime = i;
    }

    public abstract int getTotalCookTime();

    public int getCookProgressScaled(int i)
    {
        return (cookTime * i) / getTotalCookTime();
    }

    public abstract int getBurnProgressScaled(int i);

    @Override
    public int getLightValue()
    {
        if(door && isBurning()) {
            return 13;
        }
        return 0;
    }
}
