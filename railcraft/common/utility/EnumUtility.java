package railcraft.common.utility;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.ItemStack;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftConfig;
import railcraft.common.modules.RailcraftModuleManager;
import railcraft.common.modules.RailcraftModuleManager.Module;

/**
 *
 * @author CovertJaguar
 */
public enum EnumUtility
{

    ITEM_LOADER(Module.TRANSPORT, "loader.item", "Item Loader", TileItemLoader.class, new int[]{12, 10, 11, 11, 11, 11}),
    ITEM_UNLOADER(Module.TRANSPORT, "unloader.item", "Item Unloader", TileItemUnloader.class, new int[]{154, 156, 155, 155, 155, 155}),
    ITEM_LOADER_ADVANCED(Module.TRANSPORT, "loader.item.advanced", "Adv. Item Loader", TileItemLoaderAdvanced.class, new int[]{122, 122, 123, 124, 123, 123}),
    LIQUID_LOADER(Module.TRANSPORT, "loader.liquid", "Liquid Loader", TileLiquidLoader.class, new int[]{28, 26, 27, 27, 27, 27}),
    LIQUID_UNLOADER(Module.TRANSPORT, "unloader.liquid", "Liquid Unloader", TileLiquidUnloader.class, new int[]{26, 28, 27, 27, 27, 27}),
    ITEM_UNLOADER_ADVANCED(Module.TRANSPORT, "unloader.item.advanced", "Adv. Item Unloader", TileItemUnloaderAdvanced.class, new int[]{138, 138, 139, 140, 139, 139}),
    MINECART_DISPENSER(Module.AUTOMATION, "dispenser.cart", "Cart Dispenser", TileDispenserCart.class, new int[]{106, 106, 107, 108, 107, 107}),
    COKE_OVEN(Module.FACTORY, "coke.oven", "Coke Oven Brick", TileCokeOven.class, new int[]{170, 170, 170, 170, 170, 170, 171, 172}),
    ROLLING_MACHINE(Module.FACTORY, "rolling.machine", "Rolling Machine", TileRollingMachine.class, new int[]{186, 188, 187, 187, 187, 187}),
    ENERGY_LOADER(Module.IC2, "loader.energy", "Energy Loader", TileEnergyLoader.class, new int[]{202, 202, 203, 204, 203, 203}),
    ENERGY_UNLOADER(Module.IC2, "unloader.energy", "Energy Unloader", TileEnergyUnloader.class, new int[]{218, 218, 219, 220, 219, 219}),
    FEED_STATION(Module.AUTOMATION, "feed.station", "Feed Station", TileFeedStation.class, new int[]{234, 236, 235, 235, 235, 235}),
    BLAST_FURNACE(Module.FACTORY, "blast.furnace", "Blast Furnace Brick", TileBlastFurnace.class, new int[]{215, 215, 215, 215, 215, 215, 216, 217}),
    TRAIN_DISPENSER(Module.FACTORY, "dispenser.train", "Train Dispenser", TileDispenserTrain.class, new int[]{231, 231, 232, 233, 232, 232}),
    WATER_TANK(Module.TRANSPORT, "tank.water", "Water Tank Siding", TileWaterTank.class, new int[]{214, 214, 213, 213, 213, 213}),
    ROCK_CRUSHER(Module.FACTORY, "rock.crusher", "Rock Crusher", TileRockCrusher.class, new int[]{3, 5, 4, 4, 4, 4});
    private final int id;
    private final Module module;
    private final String tag;
    private final String name;
    private final Class<? extends TileUtility> tile;
    private final int[] texture;
    private static int nextId;
    private static final List<EnumUtility> creativeList = new ArrayList<EnumUtility>();

    static {
        creativeList.add(COKE_OVEN);
        creativeList.add(BLAST_FURNACE);
        creativeList.add(WATER_TANK);
        creativeList.add(ROLLING_MACHINE);
        creativeList.add(ROCK_CRUSHER);
        creativeList.add(FEED_STATION);
        creativeList.add(MINECART_DISPENSER);
        creativeList.add(TRAIN_DISPENSER);
        creativeList.add(ITEM_LOADER);
        creativeList.add(ITEM_UNLOADER);
        creativeList.add(ITEM_LOADER_ADVANCED);
        creativeList.add(ITEM_UNLOADER_ADVANCED);
        creativeList.add(LIQUID_LOADER);
        creativeList.add(LIQUID_UNLOADER);
        creativeList.add(ENERGY_LOADER);
        creativeList.add(ENERGY_UNLOADER);
    }

    private EnumUtility(Module module, String tag, String name, Class<? extends TileUtility> tile, int[] texture)
    {
        this.module = module;
        this.tile = tile;
        this.tag = tag;
        this.name = name;
        this.texture = texture;
        id = getNextId();
    }

    public int getTexture(int index)
    {
        if(index < 0 || index >= texture.length) {
            index = 0;
        }
        return texture[index];
    }

    public int getId()
    {
        return id;
    }

    public static EnumUtility fromId(int id)
    {
        for(EnumUtility r : EnumUtility.values()) {
            if(r.getId() == id) {
                return r;
            }
        }
        return ITEM_LOADER;
    }

    public static List<EnumUtility> getCreativeList()
    {
        return creativeList;
    }

    private static int getNextId()
    {
        int i = nextId;
        nextId++;
        return i;
    }

    public String getTag()
    {
        return "utility." + tag;
    }

    public String getName()
    {
        return name;
    }

    public TileUtility getBlockEntity()
    {
        try {
            return tile.newInstance();
        } catch (Exception ex) {
        }
        return null;
    }

    public ItemStack getItem()
    {
        return getItem(1);
    }

    public ItemStack getItem(int qty)
    {
        return new ItemStack(RailcraftBlocks.getBlockUtility().id, qty, id);
    }

    public Module getModule()
    {
        return module;
    }

    public boolean isEnabled()
    {
        return RailcraftModuleManager.isModuleLoaded(getModule()) && RailcraftBlocks.getBlockUtility() != null && RailcraftConfig.isSubBlockEnabled(getTag());
    }
}
