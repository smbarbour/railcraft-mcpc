package railcraft.common.utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.Block;
import net.minecraft.server.BlockMinecartTrack;
import net.minecraft.server.EntityLiving;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.ItemStack;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import forge.ISidedInventory;
import ic2.api.ElectricItem;
import ic2.api.IElectricItem;
import ic2.api.IWrenchable;
import ic2.api.Items;
import railcraft.Railcraft;
import railcraft.common.IEnergyDevice;
import railcraft.common.api.EnumDirection;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.ICrowbar;
import railcraft.common.modules.ModuleIC2;

public abstract class TileUtilityEnergy extends TileUtilityItem implements IWrenchable, IEnergyDevice, ISidedInventory
{

    private static final int TIER = 2;
    private static final int CAPACITY = 100000;
    private static final int MAX_OVERCLOCKS = 10;
    private static final int MAX_LAPOTRON = 9;
    protected int energy;
    private int prevEnergy;
    protected int transferRate;
    protected short storageUpgrades;
    protected short lapotronUpgrades;
    protected short transformerUpgrades;
    protected short overclockerUpgrades;
    private byte direction;
    protected int update;
    protected boolean powered;

    public TileUtilityEnergy(EnumUtility type, String name)
    {
        super(type, name, 6);
    }

    @Override
    public int getSizeInventorySide(int side)
    {
        return 1;
    }

    @Override
    public int getStartInventorySide(int side)
    {
        if(side == EnumDirection.UP.getValue()) {
            return 0;
        }
        return 1;
    }

    @Override
    public int getBlockTexture(int side)
    {
        if(direction == side) {
            return getUtilityType().getTexture(3);
        }
        if(side != 0 && side != 1) {
            return getUtilityType().getTexture(2);
        }
        return getUtilityType().getTexture(1);
    }

    @Override
    public void onBlockPlacedBy(World world, int i, int j, int k, EntityLiving entityliving)
    {
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        setFacing(GeneralTools.getSideClosestToPlayer(world, i, j, k, entityliving));
    }

    protected void countUpgrades()
    {
        ItemStack storage = Items.getItem("energyStorageUpgrade");
        ItemStack overclocker = Items.getItem("overclockerUpgrade");
        ItemStack transformer = Items.getItem("transformerUpgrade");
        ItemStack lapotron = ModuleIC2.getLapotronUpgrade();

        storageUpgrades = 0;
        overclockerUpgrades = 0;
        transformerUpgrades = 0;
        lapotronUpgrades = 0;

        for(int i = 2; i < 6; i++) {
            ItemStack stack = getItem(i);
            if(stack != null) {
                if(storage != null && stack.doMaterialsMatch(storage)) {
                    storageUpgrades += stack.count;
                } else if(overclocker != null && stack.doMaterialsMatch(overclocker)) {
                    overclockerUpgrades += stack.count;
                } else if(transformer != null && stack.doMaterialsMatch(transformer)) {
                    transformerUpgrades += stack.count;
                } else if(lapotron != null && stack.doMaterialsMatch(lapotron)) {
                    lapotronUpgrades += stack.count;
                }
            }
        }
        if(overclockerUpgrades > MAX_OVERCLOCKS) {
            overclockerUpgrades = MAX_OVERCLOCKS;
        }
        if(lapotronUpgrades > MAX_LAPOTRON) {
            lapotronUpgrades = MAX_LAPOTRON;
        }
    }

    @Override
    public void update()
    {
        super.update();
        countUpgrades();
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        update++;

        if(Railcraft.gameIsNotHost()) {
            return;
        }

        int capacity = getCapacity();
        if(energy > capacity) {
            energy = capacity;
        }

        if(Railcraft.gameIsServer() && prevEnergy != energy && update % 4 == 0) {
            prevEnergy = energy;
            world.notify(x, y, z);
        }

        if(contents[0] != null && contents[0].getItem() instanceof IElectricItem && energy > 0) {
            energy -= ElectricItem.charge(contents[0], energy, getTier(), false, false);
        }

        ItemStack battery = getItem(1);
        if(battery != null && battery.getItem() instanceof IElectricItem && ((IElectricItem)battery.getItem()).canProvideEnergy() && energy < capacity) {
            energy += ElectricItem.discharge(battery, capacity - energy, getTier(), false, false);
        }

        if(update % 20 == 0) {
            countUpgrades();
        }
    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);
        nbttagcompound.setInt("energy", energy);
        nbttagcompound.setShort("facing", direction);
    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);
        energy = nbttagcompound.getInt("energy");
        direction = (byte)nbttagcompound.getShort("facing");

        countUpgrades();

        update = GeneralTools.getRand().nextInt();
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeByte(getFacing());
        data.writeShort(storageUpgrades);
        data.writeShort(lapotronUpgrades);
        data.writeInt(energy);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        setFacing(data.readByte());
        storageUpgrades = data.readShort();
        lapotronUpgrades = data.readShort();
        energy = data.readInt();
    }

    @Override
    public boolean blockActivated(World world, int i, int j, int k, EntityHuman player)
    {
        ItemStack current = player.U();
        if(current != null && current.getItem() instanceof ICrowbar) {
            byte side = GeneralTools.getCurrentMousedOverSide(player);
            if(direction == side) {
                direction = GeneralTools.getOppositeSide(side);
            } else {
                direction = side;
            }
            if(current.d()) {
                current.damage(1, player);
            }
            world.notify(i, j, k);
            return true;
        }
        return super.blockActivated(world, i, j, k, player);
    }

    @Override
    public boolean wrenchCanSetFacing(EntityHuman entityPlayer, int side)
    {
        if(direction == side) {
            return false;
        }
        return true;
    }

    @Override
    public short getFacing()
    {
        return direction;
    }

    @Override
    public void setFacing(short side)
    {
        if(direction != side) {
            direction = (byte)side;
            if(world != null) {
                world.notify(x, y, z);
            }
        }
    }

    @Override
    public boolean wrenchCanRemove(EntityHuman entityPlayer)
    {
        return true;
    }

    @Override
    public float getWrenchDropRate()
    {
        return 0;
    }

    @Override
    public boolean isPoweringTo(int side)
    {
        if(!powered) {
            return false;
        }
        return isPoweringTo(world, x, y, z, side);
    }

    public static boolean isPoweringTo(World world, int i, int j, int k, int side)
    {
        int opSide = GeneralTools.getOppositeSide(side);
        int id = GeneralTools.getBlockIdOnSide(world, i, j, k, opSide);
        if(BlockMinecartTrack.d(id) || id == Block.REDSTONE_WIRE.id) {
            return true;
        }

        return false;
    }

    @Override
    public boolean canConnectRedstone(int dir)
    {
        return true;
    }

    protected void setPowered(boolean p)
    {
        if(powered ^ p) {
            powered = p;
            int i = x;
            int j = y;
            int k = z;
            if(world != null) {
                world.applyPhysics(i, j, k, getBlock().id);
                world.applyPhysics(i + 1, j, k, getBlock().id);
                world.applyPhysics(i - 1, j, k, getBlock().id);
                world.applyPhysics(i, j, k + 1, getBlock().id);
                world.applyPhysics(i, j, k - 1, getBlock().id);
                world.applyPhysics(i, j - 1, k, getBlock().id);
                world.applyPhysics(i, j + 1, k, getBlock().id);
//                worldObj.markBlockNeedsUpdate(i, j, k);
            }
        }
    }

    @Override
    public int getEnergy()
    {
        return energy;
    }

    @Override
    public int getCapacity()
    {
        int capacity = CAPACITY;
        capacity += storageUpgrades * 10000;
        capacity += lapotronUpgrades * 1000000;
        return capacity;
    }

    public int getTier()
    {
        return TIER + transformerUpgrades;
    }

    public int getTransferRate()
    {
        return transferRate;
    }

    @Override
    public int getEnergyBarScaled(int scale)
    {
        return (getEnergy() * scale) / getCapacity();
    }
}
