package railcraft.common.utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.EntityLiving;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.ItemStack;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import railcraft.common.api.EnumDirection;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.ICrowbar;

public class TileItemLoaderAdvanced extends TileItemLoader
{

    private byte direction = EnumDirection.NORTH.getValue();

    public TileItemLoaderAdvanced()
    {
        super(EnumUtility.ITEM_LOADER_ADVANCED, "Adv. Loader");
    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);

        nbttagcompound.setByte("direction", direction);
    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);

        direction = nbttagcompound.getByte("direction");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeByte(direction);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        direction = data.readByte();
    }

    @Override
    public int getBlockTexture(int side)
    {
        if(direction == side) {
            return getUtilityType().getTexture(3);
        }
        if(side != 0 && side != 1) {
            return getUtilityType().getTexture(2);
        }
        return getUtilityType().getTexture(1);
    }

    @Override
    public void onBlockPlacedBy(World world, int i, int j, int k, EntityLiving entityliving)
    {
        direction = GeneralTools.getSideClosestToPlayer(world, i, j, k, entityliving);
    }

    @Override
    public boolean blockActivated(World world, int i, int j, int k, EntityHuman player)
    {
        ItemStack current = player.U();
        if(current != null && current.getItem() instanceof ICrowbar) {
            byte side = GeneralTools.getCurrentMousedOverSide(player);
            if(direction == side) {
                direction = GeneralTools.getOppositeSide(side);
            } else {
                direction = side;
            }
            if(current.d()) {
                current.damage(1, player);
            }
            world.notify(i, j, k);
            return true;
        }
        return super.blockActivated(world, i, j, k, player);
    }

    @Override
    public boolean wrenchCanSetFacing(EntityHuman player, int side)
    {
        if(direction == side) {
            return false;
        }
        return true;
    }

    @Override
    public void setFacing(short facing)
    {
        direction = (byte)facing;
        world.notify(x, y, z);
    }

    @Override
    public short getFacing()
    {
        return direction;
    }
}
