package railcraft.common.utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;
import forge.ISidedInventory;
import railcraft.Railcraft;
import railcraft.common.RailcraftBlocks;
import railcraft.common.api.GeneralTools;

public abstract class TileMultiBlock extends TileUtilityItem implements ISidedInventory
{

    private enum MultiBlockState
    {

        VALID_PASS_ONE, VALID_PASS_TWO, INVALID, UNKNOWN
    };
    private static final int RECURSION_DEPTH = 12;
    protected boolean master;
    protected boolean door;
    private boolean tested;
    private TileMultiBlock masterBlock;

    public TileMultiBlock(EnumUtility type, String name, int invNum)
    {
        super(type, name, invNum);
    }

    public abstract int[][][] getBlockMap();

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        if(Railcraft.gameIsHost()) {
            if(!tested) {
                testIfMasterBlock();
//                Railcraft.getMod().totalMultiBlockUpdates++;
            }
        }
    }

    public void testIfMasterBlock()
    {
        tested = true;
//        System.out.println("testing oven");
        MultiBlockState state = getMasterBlockStatePassOne();
        if(state == MultiBlockState.INVALID && getBlockMap().length != getBlockMap()[0][0].length) {
            state = getMasterBlockStatePassTwo();
        }

        if(state == MultiBlockState.UNKNOWN) {
            tested = false;
            return;
        } else if(state == MultiBlockState.VALID_PASS_ONE) {
//             System.out.println("oven complete");
            for(int i = 0; i < getBlockMap().length - 2; i++) {
                for(int j = 0; j < getBlockMap()[0].length - 2; j++) {
                    for(int k = 0; k < getBlockMap()[0][0].length - 2; k++) {
                        if(isMapPositionIgnored(getBlockMap()[i + 1][j + 1][k + 1])) {
                            continue;
                        }
                        TileEntity tile = world.getTileEntity(i + x, j + y, k + z);
                        if(tile instanceof TileMultiBlock) {
                            TileMultiBlock multiBlock = (TileMultiBlock)tile;
                            multiBlock.masterBlock = this;
//                            mulitBlock.setMasterBlock(xCoord, yCoord, zCoord);
                            multiBlock.setDoor(isMapPositionDoor(getBlockMap()[i + 1][j + 1][k + 1]));
                            if(Railcraft.gameIsServer()) {
                                world.notify(i + x, j + y, k + z);
                            }
                        } else {
                            return;
                        }
                    }
                }
            }
            master = true;
        } else if(state == MultiBlockState.VALID_PASS_TWO) {
//             System.out.println("oven complete");
            for(int i = 0; i < getBlockMap()[0][0].length - 2; i++) {
                for(int j = 0; j < getBlockMap()[0].length - 2; j++) {
                    for(int k = 0; k < getBlockMap().length - 2; k++) {
                        if(isMapPositionIgnored(getBlockMap()[k + 1][j + 1][i + 1])) {
                            continue;
                        }
                        TileEntity tile = world.getTileEntity(i + x, j + y, k + z);
                        if(tile instanceof TileMultiBlock) {
                            TileMultiBlock multiBlock = (TileMultiBlock)tile;
                            multiBlock.masterBlock = this;
//                            mulitBlock.setMasterBlock(xCoord, yCoord, zCoord);
                            multiBlock.setDoor(isMapPositionDoor(getBlockMap()[k + 1][j + 1][i + 1]));
                            if(Railcraft.gameIsServer()) {
                                world.notify(i + x, j + y, k + z);
                            }
                        } else {
                            return;
                        }
                    }
                }
            }
            master = true;
        } else {
            if(master) {
                dropInventory();
                onMasterReset();
            }
            master = false;
        }
        if(Railcraft.gameIsServer()) {
            world.notify(x, y, z);
        }
    }

    protected void onMasterReset()
    {
    }

    protected boolean isMapPositionIgnored(int map)
    {
        if(map == 3) {
            return true;
        }
        return false;
    }

    protected boolean isMapPositionDoor(int map)
    {
        if(map == 2) {
            return true;
        }
        return false;
    }

    protected boolean isMapPositionValid(int i, int j, int k, int map)
    {
        int id = world.getTypeId(i, j, k);
        switch (map) {
            case 0:
                if(id == RailcraftBlocks.getBlockUtility().id && world.getData(i, j, k) == getUtilityType().getId()) {
                    return false;
                }
                break;
            case 1:
            case 2:
                if(id != RailcraftBlocks.getBlockUtility().id || world.getData(i, j, k) != getUtilityType().getId()) {
                    return false;
                }
                break;
            case 3:
                if(id != 0) {
                    return false;
                }
                break;
        }
        return true;
    }

    private MultiBlockState getMasterBlockStatePassOne()
    {
        for(int j = 0; j < getBlockMap()[0].length; j++) {
            for(int i = 0; i < getBlockMap().length; i++) {
                for(int k = 0; k < getBlockMap()[0][0].length; k++) {
                    int ii = i + x - 1;
                    int jj = j + y - 1;
                    int kk = k + z - 1;
                    if(!world.isLoaded(ii, jj, kk)) {
                        return MultiBlockState.UNKNOWN;
                    }
                    if(!isMapPositionValid(ii, jj, kk, getBlockMap()[i][j][k])) {
                        return MultiBlockState.INVALID;
                    }
                }
            }
        }

        return MultiBlockState.VALID_PASS_ONE;
    }

    private MultiBlockState getMasterBlockStatePassTwo()
    {
        for(int j = 0; j < getBlockMap()[0].length; j++) {
            for(int i = 0; i < getBlockMap()[0][0].length; i++) {
                for(int k = 0; k < getBlockMap().length; k++) {
                    int ii = i + x - 1;
                    int jj = j + y - 1;
                    int kk = k + z - 1;
                    if(!world.isLoaded(ii, jj, kk)) {
                        return MultiBlockState.UNKNOWN;
                    }
                    if(!isMapPositionValid(ii, jj, kk, getBlockMap()[k][j][i])) {
                        return MultiBlockState.INVALID;
                    }
                }
            }
        }

        return MultiBlockState.VALID_PASS_TWO;
    }

    private void dropInventory()
    {
        for(int l = 0; l < getSize(); l++) {
            ItemStack itemstack = getItem(l);
            if(itemstack != null) {
                float f = GeneralTools.getRand().nextFloat() * 0.8F + 0.1F;
                float f1 = GeneralTools.getRand().nextFloat() * 0.8F + 0.1F;
                float f2 = GeneralTools.getRand().nextFloat() * 0.8F + 0.1F;
                while(itemstack.count > 0) {
                    int i1 = GeneralTools.getRand().nextInt(21) + 10;
                    if(i1 > itemstack.count) {
                        i1 = itemstack.count;
                    }
                    itemstack.count -= i1;
                    EntityItem entityitem = new EntityItem(world, (float)x + f, (float)y + f1, (float)z + f2, new ItemStack(itemstack.id, i1, itemstack.getData()));
                    float f3 = 0.05F;
                    entityitem.motX = (float)GeneralTools.getRand().nextGaussian() * f3;
                    entityitem.motY = (float)GeneralTools.getRand().nextGaussian() * f3 + 0.2F;
                    entityitem.motZ = (float)GeneralTools.getRand().nextGaussian() * f3;
                    world.addEntity(entityitem);
                }
                setItem(l, null);
            }
        }
    }

    @Override
    public int getBlockTexture(int side)
    {
        return getUtilityType().getTexture(0);
    }

    @Override
    public int getSizeInventorySide(int side)
    {
        return 1;
    }

    @Override
    public ItemStack splitStack(int i, int j)
    {
        if(master || Railcraft.gameIsNotHost()) {
            return super.splitStack(i, j);
        }
        TileMultiBlock masterBlock = getMasterBlock();
        if(masterBlock != null) {
            return masterBlock.splitStack(i, j);
        }
        return null;
    }

    @Override
    public ItemStack getItem(int i)
    {
        if(master || Railcraft.gameIsNotHost()) {

            return super.getItem(i);
        }
        TileMultiBlock masterBlock = getMasterBlock();
        if(masterBlock != null) {
            return masterBlock.getItem(i);
        }
        return null;
    }

    @Override
    public void setItem(int i, ItemStack itemstack)
    {
        if(master || Railcraft.gameIsNotHost()) {
            super.setItem(i, itemstack);
        } else {
            TileMultiBlock masterBlock = getMasterBlock();
            if(masterBlock != null) {
                masterBlock.setItem(i, itemstack);
            }
        }
    }

    @Override
    public void onNeighborBlockChange(World world, int i, int j, int k, int id)
    {
        if(Railcraft.gameIsHost()) {
            if(id == RailcraftBlocks.getBlockUtility().id) {
                onBlockChange(0);
            }
        }
    }

    private void onBlockChange(int count)
    {
        count++;
        if(count > RECURSION_DEPTH) {
            return;
        }
        if(tested) {
            tested = false;
            if(masterBlock != null) {
                masterBlock.tested = false;
            }
            for(int side = 0; side < 6; side++) {
                TileEntity tile = GeneralTools.getBlockTileEntityFromSide(world, x, y, z, side);
                if(tile != null && tile.getClass() == getClass()) {
                    ((TileMultiBlock)tile).onBlockChange(count);
                }
            }
        }
    }

//    @Override
//    public void onBlockAdded(World world, int i, int j, int k)
//    {
//        testIfMasterBlock();
//    }
    @Override
    public void onBlockRemoval(World world, int i, int j, int k)
    {
        master = false;
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);

        data.setBoolean("master", master);
        data.setBoolean("door", door);

//        data.setInteger("masterX", masterX);
//        data.setInteger("masterY", masterY);
//        data.setInteger("masterZ", masterZ);
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);

        master = data.getBoolean("master");
        door = data.getBoolean("door");

//        masterX = data.getInteger("masterX");
//        masterY = data.getInteger("masterY");
//        masterZ = data.getInteger("masterZ");

        tested = false;
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        TileMultiBlock m = getMasterBlock();
        if(m != null) {
            data.writeInt(m.x);
            data.writeInt(m.y);
            data.writeInt(m.z);
        } else {
            data.writeInt(-1);
            data.writeInt(-1);
            data.writeInt(-1);
        }

        byte bits = 0;
        bits |= master ? 1 : 0;
        bits |= door ? 2 : 0;
        data.writeByte(bits);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        int masterX = data.readInt();
        int masterY = data.readInt();
        int masterZ = data.readInt();

        if(masterY != -1 && world != null) {
            TileEntity tile = world.getTileEntity(masterX, masterY, masterZ);
            if(tile != null && tile.getClass() == getClass()) {
                masterBlock = (TileMultiBlock)tile;
            }
        }

        byte bits = data.readByte();
        master = (bits & 1) != 0;
        door = (bits & 2) != 0;

        world.notify(x, y, z);
    }

    public boolean isMaster()
    {
        return master;
    }

    public void setMaster(boolean m)
    {
        master = m;
    }

    public void retestMasterBlock()
    {
        if(!master) {
            TileMultiBlock masterBlock = getMasterBlock();
            if(masterBlock != null) {
                masterBlock.retestMasterBlock();
            }
        }
        tested = false;
    }

    public boolean isMyMasterValid()
    {
        return masterBlock != null && !masterBlock.l() && masterBlock.isMaster();
    }

    public TileMultiBlock getMasterBlock()
    {
        if(isMyMasterValid()) {
            return masterBlock;
        }
        masterBlock = null;
        return null;
//        if(master) {
//            return this;
//        }
//        if(masterY < 0 || worldObj == null) {
//            return null;
//        }
//        TileEntity tile = worldObj.getBlockTileEntity(masterX, masterY, masterZ);
//        if(tile instanceof TileMultiBlock) {
//            TileMultiBlock multiBlock = (TileMultiBlock)tile;
//            if(multiBlock.isMaster()) {
//                return multiBlock;
//            }
//        }
//        return null;
    }

//    public void setMasterBlock(int i, int j, int k)
//    {
//        if(masterX != i || masterY != j || masterZ != k) {
//            masterX = i;
//            masterY = j;
//            masterZ = k;
//            if(worldObj != null && Railcraft.gameIsServer()) {
//                worldObj.notify(xCoord, yCoord, zCoord);
//            }
//        }
//    }
    public void setDoor(boolean d)
    {
        if(door != d) {
            door = d;
            if(world != null) {
                world.notify(x, y, z);
            }
        }
    }
}
