package railcraft.common.utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.EntityMinecart;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.ItemStack;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import ic2.api.Direction;
import ic2.api.IEnergySink;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.EnumGui;
import railcraft.common.api.CartTools;
import railcraft.common.api.IEnergyTransfer;

public class TileEnergyLoader extends TileUtilityEnergy implements IEnergySink
{

    private static final short[] INPUT_LEVELS = {512, 2048};
    private boolean transferedEnergy;
    private boolean waitTillFull = false;
    private boolean waitIfEmpty = true;
    private EntityMinecart currentCart = null;

    public TileEnergyLoader()
    {
        super(EnumUtility.ENERGY_LOADER, "Energy Loader");
    }

    @Override
    protected boolean openGui(EntityHuman player)
    {
        GuiHandler.openGui(EnumGui.LOADER_ENERGY, player, world, x, y, z);
        return true;
    }

    @Override
    public void q_()
    {
        super.q_();

        if(Railcraft.gameIsNotHost()) {
            return;
        }

        transferedEnergy = false;
        transferRate = 0;

        EntityMinecart cart = CartTools.getMinecartOnSide(world, x, y, z, 0.1f, getFacing());

        if(cart != currentCart) {
            setPowered(false);
            currentCart = cart;
        }

        if(cart == null) {
            return;
        }

        if(!(cart instanceof IEnergyTransfer)) {
            if(CartTools.cartVelocityIsLessThan(cart, Loader.STOP_VELOCITY)) {
                setPowered(true);
            }
            return;
        }

        IEnergyTransfer energyCart = (IEnergyTransfer)cart;

        if(!energyCart.canInjectEnergy() || energyCart.getTier() > getTier()) {
            if(CartTools.cartVelocityIsLessThan(cart, Loader.STOP_VELOCITY)) {
                setPowered(true);
            }
            return;
        }

        if(energy > 0 && energyCart.getEnergy() < energyCart.getCapacity()) {
            int usage = (int)(energyCart.getTransferLimit() * Math.pow(1.5, overclockerUpgrades));
            int injection = (int)(energyCart.getTransferLimit() * Math.pow(1.3, overclockerUpgrades));
            if(usage > energy) {
                double ratio = (double)energy / (double)usage;
                usage = energy;
                injection = (int)(injection * ratio);
            }

            transferRate = injection;
            int extra = energyCart.injectEnergy(this, injection, getTier(), true, false, false);
            energy -= usage - extra;
            transferedEnergy = extra != injection;
        }

        if(!transferedEnergy && !powered && CartTools.cartVelocityIsLessThan(cart, Loader.STOP_VELOCITY)) {
            if(!waitTillFull && energyCart.getEnergy() > 0) {
                setPowered(true);
            } else if(!waitIfEmpty && !waitTillFull && energyCart.getEnergy() == 0) {
                setPowered(true);
            } else if(energyCart.getEnergy() >= energyCart.getCapacity()) {
                setPowered(true);
            }
        }
    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);
        nbttagcompound.setBoolean("WaitIfEmpty", waitIfEmpty());
        nbttagcompound.setBoolean("WaitTillFull", waitTillFull());
    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);
        setWaitIfEmpty(nbttagcompound.getBoolean("WaitIfEmpty"));
        setWaitTillFull(nbttagcompound.getBoolean("WaitTillFull"));
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        byte bits = 0;
        bits |= waitIfEmpty ? 1 : 0;
        bits |= waitTillFull ? 2 : 0;
        data.writeByte(bits);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        byte bits = data.readByte();
        waitIfEmpty = (bits & 1) != 0;
        waitTillFull = (bits & 2) != 0;
    }

    @Override
    public boolean demandsEnergy()
    {
        return energy < getCapacity();
    }

    @Override
    public int injectEnergy(Direction directionFrom, int amount)
    {
        if(amount > INPUT_LEVELS[0] && transformerUpgrades <= 0) {
            world.explode(null, x, y, z, 2F);
            return 0;
        }
        energy += amount;
        int capacity = getCapacity();
        if(energy > capacity) {
            int extra = energy - capacity;
            energy = capacity;
            return extra;
        }
        return 0;
    }

    @Override
    public boolean acceptsEnergyFrom(TileEntity emitter, Direction direction)
    {
        if(getFacing() == direction.toSideValue()) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isAddedToEnergyNet()
    {
        return true;
    }

    public boolean waitTillFull()
    {
        return waitTillFull;
    }

    public void setWaitTillFull(boolean waitTillFull)
    {
        this.waitTillFull = waitTillFull;
    }

    public boolean waitIfEmpty()
    {
        return waitIfEmpty;
    }

    public void setWaitIfEmpty(boolean waitIfEmpty)
    {
        this.waitIfEmpty = waitIfEmpty;
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
