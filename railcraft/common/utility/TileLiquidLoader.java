package railcraft.common.utility;

import forestry.api.liquids.LiquidStack;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;
import buildcraft.api.ILiquidContainer;
import buildcraft.api.ISpecialInventory;
import buildcraft.api.Orientations;
import forge.ISidedInventory;
import ic2.api.IWrenchable;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.EntityFakeBlock;
import railcraft.common.RailcraftConfig;
import railcraft.common.api.EnumDirection;
import railcraft.common.EnumGui;
import railcraft.common.api.CartTools;
import railcraft.common.util.liquids.LiquidTankToolkit;
import railcraft.common.InventoryCopy;
import railcraft.common.api.EnumItemType;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.ILiquidTransfer;
import railcraft.common.api.InventoryMapper;
import railcraft.common.api.InventoryTools;
import railcraft.common.api.tracks.ITrackPowered;
import railcraft.common.carts.EntityCartTank;
import railcraft.common.util.liquids.LiquidManager;
import railcraft.common.tracks.TileTrack;
import railcraft.common.util.liquids.ILiquidTank;

public class TileLiquidLoader extends TileUtilityLiquid implements IWrenchable, ISidedInventory, ISpecialInventory, ILiquidTank
{

    private static final int TRANSFER_RATE = 20;
    private static final int CAPACITY = LiquidManager.BUCKET_VOLUME * 8;
    private static final double PIPE_OFFSET = 5 * 0.0625;
    private static final float PIPE_Y_OFFSET = 0.2f;
    private static final double MAX_PIPE_LENGTH = 15 * 0.0625;
    private static final double PIPE_INCREMENT = 0.01;
    private static final int[] PIPE_TEXTURE = new int[]{228, 228, 212, 212, 212, 212};
    private static final int SLOT_MINECART1 = 0;
    private static final int SLOT_MINECART2 = 1;
    private static final int SLOT_INPUT = 2;
    private static final int SLOT_OUTPUT = 3;
    private boolean powered;
    private boolean waitTillFull = true;
    private boolean waitIfEmpty = true;
    private EntityMinecart currentCart = null;
    private EntityFakeBlock pipe = null;
    private double pipeY = Double.NaN;
    private final IInventory invCartFilter = new InventoryMapper(this, SLOT_MINECART1, 2);
    private final IInventory invInput = new InventoryMapper(this, SLOT_INPUT, 1);

    public TileLiquidLoader()
    {
        super(EnumUtility.LIQUID_LOADER, "Liquid Loader", 5);
    }

    public IInventory getCartFilterInventory()
    {
        return invCartFilter;
    }

    public IInventory getInputInventory()
    {
        return invInput;
    }

    private void spawnNewPipe()
    {
        pipe = new EntityFakeBlock(world);
        pipe.texture = PIPE_TEXTURE;

        if(Double.isNaN(pipeY)) {
            pipeY = y;
        }

        pipe.sizeX = 1.0 - PIPE_OFFSET * 2.0;
        pipe.sizeY = y - pipeY + PIPE_Y_OFFSET;
        pipe.sizeZ = 1.0 - PIPE_OFFSET * 2.0;

        pipe.setPosition(x + PIPE_OFFSET, pipeY, z + PIPE_OFFSET);

        world.addEntity(pipe);
    }

    private void destroyPipe()
    {
        if(pipe != null) {
            Railcraft.removeEntity(pipe);
            pipe = null;
            pipeY = Double.NaN;
            if(Railcraft.gameIsHost() && world != null) {
                world.notify(x, y, z);
            }
        }
    }

    private void setPipeY(double localY)
    {
        pipeY = localY;
        if(Double.isNaN(localY)) {
            destroyPipe();
        } else if(pipe == null) {
            spawnNewPipe();
        }
        if(pipe != null) {
            pipe.sizeY = y - localY + PIPE_Y_OFFSET;
            pipe.setPosition(x + PIPE_OFFSET, pipeY, z + PIPE_OFFSET);
        }
        if(Railcraft.gameIsHost()) {
            world.notify(x, y, z);
        }
    }

    private void extendPipe()
    {
        double localY = pipeY - PIPE_INCREMENT;
        if(pipeIsExtended()) {
        	localY = y - MAX_PIPE_LENGTH;
        }
        setPipeY(localY);
    }

    private void retractPipe()
    {
        double localY = pipeY + PIPE_INCREMENT;
        if(pipeIsRetracted()) {
        	localY = y;
        }
        setPipeY(localY);
    }

    private boolean pipeIsExtended()
    {
        if(pipe != null && pipeY <= y - MAX_PIPE_LENGTH) {
            return true;
        }
        return false;
    }

    private boolean pipeIsRetracted()
    {
        if(pipe == null || pipeY >= y) {
            return true;
        }
        return false;
    }

    @Override
    public int getBlockTexture(int side)
    {
        return getUtilityType().getTexture(side);
    }

    @Override
    public boolean isPoweringTo(int side)
    {
        if(!powered) {
            return false;
        }
        return TileItemLoader.isPoweringTo(world, x, y, z, side);
    }

    @Override
    public boolean canConnectRedstone(int dir)
    {
        return true;
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        super.q_();
        if(Railcraft.gameIsNotHost()) {
            return;
        }

        ItemStack minecartSlot1 = getItem(SLOT_MINECART1);
        if(minecartSlot1 != null && !InventoryTools.isItemType(minecartSlot1, EnumItemType.MINECART)) {
            setItem(SLOT_MINECART1, null);
            dropItem(minecartSlot1);
        }

        ItemStack minecartSlot2 = getItem(SLOT_MINECART2);
        if(minecartSlot2 != null && !InventoryTools.isItemType(minecartSlot2, EnumItemType.MINECART)) {
            setItem(SLOT_MINECART2, null);
            dropItem(minecartSlot2);
        }

        ItemStack topSlot = getItem(SLOT_INPUT);
        if(topSlot != null && !LiquidManager.getInstance().isContainer(topSlot)) {
            setItem(SLOT_INPUT, null);
            dropItem(topSlot);
        }

        ItemStack bottomSlot = getItem(SLOT_OUTPUT);
        if(bottomSlot != null && !LiquidManager.getInstance().isContainer(bottomSlot)) {
            setItem(SLOT_OUTPUT, null);
            dropItem(bottomSlot);
        }

        if(topSlot != null) {
            LiquidStack bucketLiquid = LiquidManager.getInstance().getLiquidInContainer(topSlot);
            if(bucketLiquid != null && (!topSlot.getItem().k() || bottomSlot == null)) {
                int used = fill(Orientations.YPos, LiquidManager.BUCKET_VOLUME, bucketLiquid.itemID, false);
                if(used >= LiquidManager.BUCKET_VOLUME) {
                    fill(Orientations.YPos, LiquidManager.BUCKET_VOLUME, bucketLiquid.itemID, true);
                    splitStack(SLOT_INPUT, 1);
                    if(topSlot.getItem().k()) {
                        setItem(SLOT_OUTPUT, new ItemStack(topSlot.getItem().j()));
                    }
                }
            }
        }

        for(int side = 0; side < 6; side++) {
            if(EnumDirection.UP.getValue() == side) {
                continue;
            }
            TileEntity tile = GeneralTools.getBlockTileEntityFromSide(world, x, y, z, side);
            if(tile instanceof ILiquidContainer) {
                LiquidTankToolkit tankKit = new LiquidTankToolkit((ILiquidContainer)tile);
                if(tankKit.canExtractLiquid()) {
                    Orientations dir = Orientations.values()[side];
                    dir = dir.reverse();
                    int needed = fill(dir, TRANSFER_RATE, tankKit.getLiquidId(), false);
                    needed = tankKit.empty(needed, true);
                    fill(dir, needed, tankKit.getLiquidId(), true);
                }
            }
        }

        boolean needsPipe = false;

        EntityMinecart cart = CartTools.getMinecartOnSide(world, x, y, z, 0.1f, EnumDirection.DOWN.getValue());
        if(cart == null) {
            cart = CartTools.getMinecartOnSide(world, x, y - 1, z, 0.1f, EnumDirection.DOWN.getValue());
            needsPipe = true;
        }


        if(cart != currentCart) {
            if(currentCart instanceof ILiquidTransfer) {
                ((EntityCartTank)currentCart).setFilling(false);
            }
            setPowered(false);
            currentCart = cart;
        }

        if(cart == null) {
            if(pipeIsRetracted()) {
                destroyPipe();
            } else {
                retractPipe();
            }
            return;
        }

        if(!(cart instanceof ILiquidContainer)) {
            if(CartTools.cartVelocityIsLessThan(cart, Loader.STOP_VELOCITY)) {
                setPowered(true);
            }
            return;
        }

        if(minecartSlot1 != null || minecartSlot2 != null) {
            if(!CartTools.doesCartMatchFilter(minecartSlot1, cart) && !CartTools.doesCartMatchFilter(minecartSlot2, cart)) {
                if(CartTools.cartVelocityIsLessThan(cart, Loader.STOP_VELOCITY)) {
                    setPowered(true);
                }
                return;
            }
        }

        LiquidTankToolkit tankCart = new LiquidTankToolkit((ILiquidContainer)cart);

        boolean cartWillStay = getLiquidQuantity() > 0 && tankCart.canPutLiquid(Orientations.YPos, getLiquidId());
        if(needsPipe && pipe == null && cartWillStay) {
            spawnNewPipe();
        }

        if(pipe != null) {
            if(cartWillStay) {
                extendPipe();
            } else {
                retractPipe();
            }
        }

        int flow = 0;
        if(!needsPipe || pipeIsExtended()) {
            flow = empty(RailcraftConfig.getTankCartFillRate(), false);
            flow = tankCart.fill(Orientations.YPos, flow, getLiquidId(), true);
            empty(flow, true);
        }

        if(flow > 0) {
            if(cart instanceof ILiquidTransfer) {
                ((EntityCartTank)cart).setFilling(true);
            }
            setPowered(false);
        } else {
            if(cart instanceof ILiquidTransfer) {
                ((EntityCartTank)cart).setFilling(false);
            }
        }


        if(pipeIsRetracted() && flow <= 0 && shouldCartLeave(cart, tankCart)) {
            setPowered(true);
        }
    }

    private boolean shouldCartLeave(EntityMinecart cart, LiquidTankToolkit tankCart)
    {
        boolean leave = false;
        if(CartTools.cartVelocityIsLessThan(cart, Loader.STOP_VELOCITY)) {
            if(getLiquidQuantity() > 0 && !tankCart.canPutLiquid(Orientations.YPos, getLiquidId())) {
                leave = true;
            } else if(!waitTillFull && !tankCart.isTankEmpty(getLiquidId())) {
                leave = true;
            } else if(!waitIfEmpty && !waitTillFull && tankCart.getLiquidQuantity() == 0) {
                leave = true;
            } else if(tankCart.isTankFull(getLiquidId())) {
                leave = true;
            }
        }
        return leave;
    }

    protected void setPowered(boolean p)
    {
        if(p) {
            destroyPipe();
        }
        if(powered != p) {
            powered = p;
            int i = x;
            int j = y;
            int k = z;
            if(world != null) {
                world.applyPhysics(i, j, k, getBlock().id);
                world.applyPhysics(i + 1, j, k, getBlock().id);
                world.applyPhysics(i - 1, j, k, getBlock().id);
                world.applyPhysics(i, j, k + 1, getBlock().id);
                world.applyPhysics(i, j, k - 1, getBlock().id);
                world.applyPhysics(i, j - 1, k, getBlock().id);
                world.applyPhysics(i, j + 1, k, getBlock().id);
                world.notify(i, j, k);
                TileEntity tile = world.getTileEntity(i, j - 2, k);
                if(tile instanceof TileTrack) {
                    TileTrack track = (TileTrack)tile;
                    if(track.getTrackInstance() instanceof ITrackPowered) {
                        ((ITrackPowered)track.getTrackInstance()).setPowered(p);
                    }
                }
            }
        }
    }

    @Override
    public void j()
    {
        super.j();
        destroyPipe();
    }

    @Override
    public void onBlockRemoval(World world, int i, int j, int k)
    {
        super.onBlockRemoval(world, i, j, k);
        destroyPipe();
    }

    @Override
    public int getCapacity()
    {
        return CAPACITY;
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);

        data.setBoolean("Powered", powered);

        data.setBoolean("WaitIfEmpty", waitIfEmpty);
        data.setBoolean("WaitTillFull", waitTillFull);

        data.setDouble("pipeY", pipeY);
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);

        setPowered(data.getBoolean("Powered"));

        waitIfEmpty = data.getBoolean("WaitIfEmpty");
        waitTillFull = data.getBoolean("WaitTillFull");

        pipeY = data.getDouble("pipeY");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        byte bits = 0;
        bits |= waitIfEmpty ? 1 : 0;
        bits |= waitTillFull ? 2 : 0;
        bits |= powered ? 4 : 0;
        data.writeByte(bits);

        data.writeDouble(pipeY);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        byte bits = data.readByte();
        waitIfEmpty = (bits & 1) != 0;
        waitTillFull = (bits & 2) != 0;
        powered = (bits & 4) != 0;

        setPipeY(data.readDouble());
    }

    @Override
    public boolean wrenchCanSetFacing(EntityHuman entityPlayer, int side)
    {
        return false;
    }

    @Override
    public void setFacing(short facing)
    {
    }

    @Override
    public boolean wrenchCanRemove(EntityHuman entityPlayer)
    {
        return true;
    }

    @Override
    public float getWrenchDropRate()
    {
        return 0;
    }

    public boolean waitTillFull()
    {
        return waitTillFull;
    }

    public void setWaitTillFull(boolean waitTillFull)
    {
        this.waitTillFull = waitTillFull;
    }

    public boolean waitIfEmpty()
    {
        return waitIfEmpty;
    }

    public void setWaitIfEmpty(boolean waitIfEmpty)
    {
        this.waitIfEmpty = waitIfEmpty;
    }

    @Override
    public World getWorld()
    {
        return world;
    }

    @Override
    public short getFacing()
    {
        return EnumDirection.DOWN.getValue();
    }

    @Override
    protected boolean openGui(EntityHuman player)
    {
        GuiHandler.openGui(EnumGui.LOADER_LIQUID, player, world, x, y, z);
        return true;
    }

    @Override
    public int getStartInventorySide(int side)
    {
        if(side == EnumDirection.UP.getValue()) {
            return SLOT_INPUT;
        }
        return SLOT_OUTPUT;
    }

    @Override
    public int getSizeInventorySide(int side)
    {
        return 1;
    }

    @Override
    public boolean addItem(ItemStack stack, boolean doAdd, Orientations from)
    {
        if(LiquidManager.getInstance().isContainer(stack)) {
            IInventory slot = new InventoryMapper(this, SLOT_INPUT, 1);
            if(!doAdd) {
                stack = stack.cloneItemStack();
                slot = new InventoryCopy(slot);
            }
            ItemStack ret = InventoryTools.moveItemStack(stack, slot);
            if(ret != null && stack.count != ret.count) {
                stack.count = ret.count;
                return true;
            } else if(ret == null) {
                stack.count = 0;
                return true;
            }
        }
        return false;
    }

    @Override
    public ItemStack extractItem(boolean doRemove, Orientations from)
    {
        IInventory inv = this;
        if(!doRemove) {
            inv = new InventoryCopy(inv);
        }
        return inv.splitStack(SLOT_OUTPUT, 1);
    }

    @Override
    public boolean canExtractLiquid()
    {
        return false;
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
