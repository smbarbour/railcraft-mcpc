package railcraft.common.utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityAnimal;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IInventory;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.EnumGui;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.InventoryTools;
import railcraft.common.util.network.IUpdatableTile;
import railcraft.common.util.network.PacketTileUpdate;

public class TileFeedStation extends TileUtilityItem implements IUpdatableTile
{

    private static final int MIN_FEED_INTERVAL = 200;
    private static final int FEED_VARIANCE = 300;
    private static final Random rand = GeneralTools.getRand();
    private static ItemStack wheat;
    private int update;
    private int feedTime;
    private boolean powered;

    public TileFeedStation()
    {
        super(EnumUtility.FEED_STATION, "Feed Station", 1);
    }

    @Override
    protected boolean openGui(EntityHuman player)
    {
        GuiHandler.openGui(EnumGui.FEED_STATION, player, world, x, y, z);
        return true;
    }

    @Override
    public int getBlockTexture(int side)
    {
        return getUtilityType().getTexture(side);
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        update++;

        if(Railcraft.gameIsNotHost()) {
            return;
        }

        ItemStack feed = getItem(0);

        if(update % (MIN_FEED_INTERVAL / 4) == 0 && (feed == null || feed.count < feed.getMaxStackSize())) {
            List<IInventory> chests = InventoryTools.getAdjacentInventories(world, x, y, z);

            for(IInventory inv : chests) {
                if(InventoryTools.moveOneItem(inv, this, getWheat())) {
                    break;
                }
            }
        }

        feedTime--;
        if(!powered && feed != null && feed.count >= 2 && feedTime <= 0) {
            feedTime = MIN_FEED_INTERVAL + rand.nextInt(FEED_VARIANCE);

            List animals = world.a(EntityAnimal.class, AxisAlignedBB.b(x - 2, y - 1, z - 2, x + 5, y + 3, z + 5));

            if(!animals.isEmpty()) {
                EntityAnimal target = (EntityAnimal)animals.get(rand.nextInt(animals.size()));

                if(feedAnimal(target)) {
                    splitStack(0, 1);
                    if(Railcraft.gameIsServer()) {
                        sendFeedPacket(target);
                    }
                }
            }
        }
    }

    public void sendFeedPacket(EntityAnimal animal)
    {
        try {
            PacketTileUpdate pkt = new PacketTileUpdate(this);
            DataOutputStream data = pkt.getDataStream();
            data.writeInt(animal.id);

            Railcraft.sendPacketToPlayersAround(pkt.getPacket(), x, y, z, world.worldProvider.dimension);
        } catch (IOException ex) {
        }
    }

    @Override
    public void onUpdatePacket(DataInputStream data) throws IOException
    {
        Entity e = Railcraft.getEntity(world, data.readInt());
        if(e instanceof EntityAnimal) {
            feedAnimal((EntityAnimal)e);
        }
    }

    private boolean feedAnimal(EntityAnimal animal)
    {
        if(animal == null) {
            return false;
        }
        try {
            if(animal.getAge() == 0) {
                Field inLove = null;
                try {
                    inLove = EntityAnimal.class.getDeclaredField("love");
                } catch (Throwable ex1) {
                    inLove = EntityAnimal.class.getDeclaredField("inLove");
                }
                inLove.setAccessible(true);
                inLove.setInt(animal, 600);

                animal.setTarget(null);

                for(int i = 0; i < 7; i++) {
                    double d = rand.nextGaussian() * 0.02D;
                    double d1 = rand.nextGaussian() * 0.02D;
                    double d2 = rand.nextGaussian() * 0.02D;
                    world.a("heart", (animal.locX + (double)(rand.nextFloat() * animal.width * 2.0F)) - animal.width, animal.locY + 0.5D + (double)(rand.nextFloat() * animal.height), (animal.locZ + (double)(rand.nextFloat() * animal.width * 2.0F)) - animal.width, d, d1, d2);
                }
            }

            return true;
        } catch (Throwable ex) {
            Railcraft.log(Level.SEVERE, "Feed Station encountered error, {0}", ex);
        }
        return false;
    }

    @Override
    public void onNeighborBlockChange(World world, int i, int j, int k, int id)
    {
        powered = world.isBlockPowered(i, j, k) || world.isBlockIndirectlyPowered(i, j, k);
    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);
        powered = nbttagcompound.getBoolean("powered");

        update = GeneralTools.getRand().nextInt();
    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);

        nbttagcompound.setBoolean("powered", powered);
    }

    private static ItemStack getWheat()
    {
        if(wheat == null) {
            wheat = new ItemStack(Item.WHEAT);
        }
        return wheat;
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
