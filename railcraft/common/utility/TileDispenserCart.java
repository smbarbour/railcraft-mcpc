package railcraft.common.utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;
import forge.ISidedInventory;
import ic2.api.IWrenchable;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.RailcraftConfig;
import railcraft.common.EnumGui;
import railcraft.common.InventoryCopy;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.CartTools;
import railcraft.common.api.ICrowbar;
import railcraft.common.api.InventoryTools;

public class TileDispenserCart extends TileUtilityItem implements IWrenchable, ISidedInventory
{

    private byte direction;
    protected boolean powered;
    protected int timeSinceLastSpawn;

    public TileDispenserCart()
    {
        super(EnumUtility.MINECART_DISPENSER, "Cart Dispenser", 3);
    }

    protected TileDispenserCart(EnumUtility type, String name, int inv)
    {
        super(type, name, inv);
    }

    @Override
    public int getBlockTexture(int side)
    {
        if(direction == side) {
            return getUtilityType().getTexture(3);
        }
        if(side != 0 && side != 1) {
            return getUtilityType().getTexture(2);
        }
        return getUtilityType().getTexture(1);
    }

    @Override
    public boolean blockActivated(World world, int i, int j, int k, EntityHuman player)
    {
        ItemStack current = player.U();
        if(current != null && current.getItem() instanceof ICrowbar) {
            byte side = GeneralTools.getCurrentMousedOverSide(player);
            if(direction == side) {
                direction = GeneralTools.getOppositeSide(side);
            } else {
                direction = side;
            }
            if(current.d()) {
                current.damage(1, player);
            }
            world.notify(i, j, k);
            return true;
        }
        return super.blockActivated(world, i, j, k, player);
    }

    @Override
    protected boolean openGui(EntityHuman player)
    {
        GuiHandler.openGui(EnumGui.CART_DISPENSER, player, world, x, y, z);
        return true;
    }

    @Override
    public void onBlockPlacedBy(World world, int i, int j, int k, EntityLiving entityliving)
    {
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        byte dir = GeneralTools.getSideClosestToPlayer(world, i, j, k, entityliving);
        direction = dir;
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        if(timeSinceLastSpawn < Integer.MAX_VALUE) {
            timeSinceLastSpawn++;
        }
    }

    @Override
    public void onNeighborBlockChange(World world, int i, int j, int k, int id)
    {
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        boolean newPower = world.isBlockPowered(i, j, k) || world.isBlockIndirectlyPowered(i, j, k);
        if(!powered && newPower) {
            powered = newPower;
            EntityMinecart cart = CartTools.getMinecartOnSide(world, i, j, k, 0, direction);
            if(cart == null) {
                if(timeSinceLastSpawn > RailcraftConfig.getCartDispenserMinDelay() * 20) {
                    for(int ii = 0; ii < getSize(); ii++) {
                        ItemStack cartStack = getItem(ii);
                        if(cartStack != null) {
                            int localx = GeneralTools.getXOnSide(i, direction);
                            int localy = GeneralTools.getYOnSide(j, direction);
                            int localz = GeneralTools.getZOnSide(k, direction);
                            if(cartStack.getItem() instanceof ItemMinecart) {
                                boolean used = cartStack.getItem().interactWith(cartStack, null, world, localx, localy, localz, 0);
                                if(cartStack.count <= 0) {
                                    setItem(ii, null);
                                }
                                if(used) {
                                    timeSinceLastSpawn = 0;
                                    break;
                                }
                            } else {
                                float rx = GeneralTools.getRand().nextFloat() * 0.8F + 0.1F;
                                float ry = GeneralTools.getRand().nextFloat() * 0.8F + 0.1F;
                                float rz = GeneralTools.getRand().nextFloat() * 0.8F + 0.1F;
                                EntityItem item = new EntityItem(world, localx + rx, localy + ry, localz + rz, cartStack);
                                float factor = 0.05F;
                                item.motX = (float)GeneralTools.getRand().nextGaussian() * factor;
                                item.motY = (float)GeneralTools.getRand().nextGaussian() * factor + 0.2F;
                                item.motZ = (float)GeneralTools.getRand().nextGaussian() * factor;
                                if(world.addEntity(item)) {
                                    setItem(ii, null);
                                }
                            }
                        }
                    }
                }
            } else if(!cart.dead && cart.getCartItem() != null) {
                IInventory testInv = new InventoryCopy(this);
                ItemStack cartStack = InventoryTools.moveItemStack(cart.getCartItem(), testInv);
                if(cartStack == null) {
                    InventoryTools.moveItemStack(cart.getCartItem(), this);
                    if(cart.passenger != null) {
                        cart.passenger.mount(null);
                    }
                    cart.die();
                }
            }
        } else if(powered ^ newPower) {
            powered = newPower;
        }
    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);

        nbttagcompound.setBoolean("powered", powered);
        nbttagcompound.setByte("direction", direction);

        nbttagcompound.setInt("time", timeSinceLastSpawn);
    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);

        powered = nbttagcompound.getBoolean("powered");
        direction = nbttagcompound.getByte("direction");

        timeSinceLastSpawn = nbttagcompound.getInt("time");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeByte(direction);
//        data.writeBoolean(powered);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        direction = data.readByte();
//        powered = data.readBoolean();

        world.notify(x, y, z);
    }

    public boolean getPowered()
    {
        return powered;
    }

    public void setPowered(boolean power)
    {
        powered = power;
    }

    @Override
    public boolean wrenchCanSetFacing(EntityHuman player, int side)
    {
        if(direction == side) {
            return false;
        }
        return true;
    }

    @Override
    public void setFacing(short facing)
    {
        direction = (byte)facing;
        world.notify(x, y, z);
    }

    @Override
    public short getFacing()
    {
        return direction;
    }

    @Override
    public boolean wrenchCanRemove(EntityHuman entityPlayer)
    {
        return true;
    }

    @Override
    public float getWrenchDropRate()
    {
        return 0;
    }

    @Override
    public int getStartInventorySide(int side)
    {
        return 0;
    }

    @Override
    public int getSizeInventorySide(int side)
    {
        return contents.length;
    }

    @Override
    public int getMaxStackSize()
    {
        return 1;
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
