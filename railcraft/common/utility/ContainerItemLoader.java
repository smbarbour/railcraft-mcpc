package railcraft.common.utility;

import net.minecraft.server.*;


public class ContainerItemLoader extends Container
{

    public IItemLoader tile;

    public ContainerItemLoader(PlayerInventory inventoryplayer, IItemLoader tile)
    {
    	this.setPlayer(inventoryplayer.player);
        this.tile = tile;
        a(new Slot(tile, 0, 10, 30));

        a(new Slot(tile, 1, 74, 41));
        a(new Slot(tile, 2, 92, 41));
        a(new Slot(tile, 3, 74, 59));
        a(new Slot(tile, 4, 92, 59));

        for(int i = 0; i < 3; i++)
        {
            for(int k = 0; k < 9; k++)
            {
                a(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++)
        {
            a(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        return tile.a(entityplayer);
    }

    @Override
    public ItemStack a(int i)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)e.get(i);
        if(slot != null && slot.c())
        {
            ItemStack itemstack1 = slot.getItem();
            itemstack = itemstack1.cloneItemStack();
            if(i >= 6 && i < 32)
            {
                if(!a(itemstack1, 32, 41, false))
                {
                    return null;
                }
            } else if(i >= 32 && i < 41)
            {
                if(!a(itemstack1, 6, 32, false))
                {
                    return null;
                }
            } else if(!a(itemstack1, 6, 41, false))
            {
                return null;
            }
            if(itemstack1.count == 0)
            {
                slot.set(null);
            } else
            {
                slot.d();
            }
            if(itemstack1.count != itemstack.count)
            {
                slot.c(itemstack1);
            } else
            {
                return null;
            }
        }
        return itemstack;
    }
    
    public IInventory getInventory() {
    	return tile;
    }
}
