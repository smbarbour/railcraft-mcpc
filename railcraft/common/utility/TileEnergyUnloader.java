package railcraft.common.utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.EntityMinecart;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.ItemStack;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import ic2.api.Direction;
import ic2.api.EnergyNet;
import ic2.api.IEnergySource;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.EnumGui;
import railcraft.common.api.CartTools;
import railcraft.common.api.IEnergyTransfer;

public class TileEnergyUnloader extends TileUtilityEnergy implements IEnergySource
{

    private static final int[] OUTPUT_LEVELS = {512, 2048};
    private boolean transferedEnergy;
    private boolean waitTillEmpty = true;
    private EntityMinecart currentCart = null;

    public TileEnergyUnloader()
    {
        super(EnumUtility.ENERGY_UNLOADER, "Energy Unloader");
    }

    @Override
    protected boolean openGui(EntityHuman player)
    {
        GuiHandler.openGui(EnumGui.UNLOADER_ENERGY, player, world, x, y, z);
        return true;
    }

    @Override
    public void q_()
    {
        super.q_();

        if(Railcraft.gameIsNotHost()) {
            return;
        }

        EnergyNet eNet = EnergyNet.getForWorld(world);
        int emit = transformerUpgrades > 0 ? OUTPUT_LEVELS[1] : OUTPUT_LEVELS[0];
        if(energy > emit) {
            int extra = eNet.emitEnergyFrom(this, emit);
            energy -= emit - extra;
        }

        transferedEnergy = false;
        transferRate = 0;

        EntityMinecart cart = CartTools.getMinecartOnSide(world, x, y, z, 0.1f, getFacing());

        if(cart != currentCart) {
            setPowered(false);
            currentCart = cart;
        }

        if(cart == null) {
            return;
        }

        if(!(cart instanceof IEnergyTransfer)) {
            if(CartTools.cartVelocityIsLessThan(cart, Loader.STOP_VELOCITY)) {
                setPowered(true);
            }
            return;
        }

        IEnergyTransfer energyCart = (IEnergyTransfer)cart;

        if(!energyCart.canExtractEnergy() || energyCart.getTier() > getTier()) {
            if(CartTools.cartVelocityIsLessThan(cart, Loader.STOP_VELOCITY)) {
                setPowered(true);
            }
            return;
        }

        if(energy < getCapacity() && energyCart.getEnergy() > 0) {
            int usage = (int)(energyCart.getTransferLimit() * Math.pow(1.5, overclockerUpgrades));
            int injection = (int)(energyCart.getTransferLimit() * Math.pow(1.3, overclockerUpgrades));

            int room = getCapacity() - getEnergy();
            if(room < injection) {
                double ratio = (double)room / (double)injection;
                injection = room;
                usage = (int)(usage * ratio);
            }

            int extract = energyCart.extractEnergy(this, usage, getTier(), true, false, false);

            if(extract < usage) {
                double ratio = (double)extract / (double)usage;
                usage = extract;
                injection = (int)(injection * ratio);
            }

            transferRate = injection;
            energy += injection;
            transferedEnergy = extract > 0;
        }

        if(!transferedEnergy && !powered && CartTools.cartVelocityIsLessThan(cart, Loader.STOP_VELOCITY)) {
            if(!waitTillEmpty) {
                setPowered(true);
            } else if(energyCart.getEnergy() == 0) {
                setPowered(true);
            }
        }
    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);
        nbttagcompound.setBoolean("WaitTillEmpty", waitTillEmpty());
    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);
        setWaitTillEmpty(nbttagcompound.getBoolean("WaitTillEmpty"));
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(waitTillEmpty);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        waitTillEmpty = data.readBoolean();
    }

    @Override
    public boolean isAddedToEnergyNet()
    {
        return true;
    }

    public boolean waitTillEmpty()
    {
        return waitTillEmpty;
    }

    public void setWaitTillEmpty(boolean wait)
    {
        waitTillEmpty = wait;
    }

    @Override
    public int getMaxEnergyOutput()
    {
        return OUTPUT_LEVELS[1];
    }

    @Override
    public boolean emitsEnergyTo(TileEntity receiver, Direction direction)
    {
        if(getFacing() == direction.toSideValue()) {
            return false;
        }
        return true;
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
