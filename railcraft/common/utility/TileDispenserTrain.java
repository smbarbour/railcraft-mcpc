package railcraft.common.utility;

import java.util.Map;
import java.util.Set;
import net.minecraft.server.*;
import buildcraft.api.ISpecialInventory;
import buildcraft.api.Orientations;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.RailcraftConfig;
import railcraft.common.EnumGui;
import railcraft.common.InventoryCopy;
import railcraft.common.api.APIErrorHandler;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.CartTools;
import railcraft.common.api.InventoryMapper;
import railcraft.common.api.InventoryTools;
import railcraft.common.api.ItemStackSet;

public class TileDispenserTrain extends TileDispenserCart implements ISpecialInventory
{

    public final static int PATTERN_START = 0;
    public final static int PATTERN_SIZE = 9;
    public final static int BUFFER_START = 9;
    public final static int BUFFER_SIZE = 18;
    private byte patternIndex;
    private boolean spawningTrain = false;
    private EntityMinecart lastCart;
    private IInventory invPattern = new InventoryMapper(this, PATTERN_START, PATTERN_SIZE);
    private IInventory invBuffer = new InventoryMapper(this, BUFFER_START, BUFFER_SIZE);
    private int update = GeneralTools.getRand().nextInt();

    public TileDispenserTrain()
    {
        super(EnumUtility.TRAIN_DISPENSER, "Train Dispenser", 27);
    }

    @Override
    protected boolean openGui(EntityHuman player)
    {
        GuiHandler.openGui(EnumGui.TRAIN_DISPENSER, player, world, x, y, z);
        return true;
    }

    private boolean canBuildTrain()
    {
        try {
            Map<ItemStack, Integer> pattern = InventoryTools.getManifest(invPattern);
            Map<ItemStack, Integer> buffer = InventoryTools.getManifest(invBuffer);

            for(Map.Entry<ItemStack, Integer> entry : pattern.entrySet()) {
                Integer count = buffer.get(entry.getKey());
                if(count == null || count < entry.getValue()) {
                    return false;
                }
            }

            return true;
        } catch (Error e) {
            APIErrorHandler.versionMismatch(InventoryTools.class);
        }
        return false;
    }

    private boolean spawnNextCart()
    {
        ItemStack spawn = getItem(patternIndex);
        if(spawn == null || InventoryTools.countItems(invBuffer, spawn) == 0) {
            resetSpawnSequence();
            return false;
        }
        int localx = GeneralTools.getXOnSide(x, getFacing());
        int localy = GeneralTools.getYOnSide(y, getFacing());
        int localz = GeneralTools.getZOnSide(z, getFacing());
        if(spawn.getItem() instanceof ItemMinecart
            && CartTools.getMinecartOnSide(world, x, y, z, 0, getFacing()) == null) {
            boolean used = spawn.getItem().interactWith(spawn.cloneItemStack(), null, world, localx, localy, localz, 0);
            if(used) {
                InventoryTools.removeOneItem(invBuffer, spawn);
                EntityMinecart cart = CartTools.getMinecartOnSide(world, x, y, z, 0, getFacing());
                CartTools.getLinkageManager().createLink(cart, lastCart);
                lastCart = cart;
                patternIndex++;
                if(patternIndex >= 9) {
                    resetSpawnSequence();
                }
            }
            return used;
        }
        return false;
    }

    private void resetSpawnSequence()
    {
        patternIndex = 0;
        spawningTrain = false;
        timeSinceLastSpawn = 0;
    }

    @Override
    public void q_()
    {
        update++;
        super.q_();

        if(spawningTrain && update % 5 == 0) {
            spawnNextCart();
        }
    }

    @Override
    public void onNeighborBlockChange(World world, int i, int j, int k, int id)
    {
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        boolean newPower = world.isBlockPowered(i, j, k) || world.isBlockIndirectlyPowered(i, j, k);
        if(!powered && newPower) {
            powered = newPower;
            EntityMinecart cart = CartTools.getMinecartOnSide(world, i, j, k, 0, getFacing());
            if(cart == null) {
                if(!spawningTrain && canBuildTrain()) {
                    if(timeSinceLastSpawn > RailcraftConfig.getCartDispenserMinDelay() * 20) {
                        spawningTrain = true;
                    }
                }
            }
//            else if(!spawningTrain) {
//                ItemStack cartStack = InventoryTools.moveItemStack(cart.getCartItem(), invBuffer);
//                if(cartStack == null) {
//                    cart.setDead();
//                }
//            }
        } else if(powered ^ newPower) {
            powered = newPower;
        }
    }

    @Override
    public int getStartInventorySide(int side)
    {
        return BUFFER_START;
    }

    @Override
    public int getSizeInventorySide(int side)
    {
        return BUFFER_SIZE;
    }

    @Override
    public int getMaxStackSize()
    {
        return 64;
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);

        data.setBoolean("spawningTrain", spawningTrain);
        data.setByte("patternIndex", patternIndex);
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);

        spawningTrain = data.getBoolean("spawningTrain");
        patternIndex = data.getByte("patternIndex");
    }

    @Override
    public boolean addItem(ItemStack stack, boolean doAdd, Orientations from)
    {
        if(InventoryTools.countItems(invPattern, stack) == 0) {
            return false;
        }
        IInventory inv = invBuffer;
        if(!doAdd) {
            inv = new InventoryCopy(inv);
        }
        ItemStack leftOver = InventoryTools.moveItemStack(stack, inv);
        if(leftOver == null) {
            stack.count = 0;
        } else if(stack.count == leftOver.count) {
            return false;
        } else {
            stack.count = leftOver.count;
        }
        return true;
    }

    @Override
    public ItemStack extractItem(boolean doRemove, Orientations from)
    {
        Set<ItemStack> patternSet = new ItemStackSet();
        Set<ItemStack> bufferSet = new ItemStackSet();

        for(int slot = 0; slot < invPattern.getSize(); slot++) {
            ItemStack stack = invPattern.getItem(slot);
            if(stack != null) {
                patternSet.add(stack);
            }
        }

        for(int slot = 0; slot < invBuffer.getSize(); slot++) {
            ItemStack stack = invBuffer.getItem(slot);
            if(stack != null) {
                bufferSet.add(stack);
            }
        }

        bufferSet.removeAll(patternSet);

        IInventory inv = invBuffer;
        if(!doRemove) {
            inv = new InventoryCopy(inv);
        }

        for(ItemStack stack : bufferSet) {
            return InventoryTools.removeOneItem(inv, stack);
        }

        return InventoryTools.removeOneItem(inv);
    }

    public IInventory getBufferInventory()
    {
        return invBuffer;
    }
}
