package railcraft.common.utility;

import net.minecraft.server.EntityItem;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;

public abstract class TileUtilityItem extends TileUtility implements IInventory
{

    protected ItemStack[] contents;
    private final String name;

    protected TileUtilityItem(EnumUtility type, String name, int invSize)
    {
        super(type);
        contents = new ItemStack[invSize];
        this.name = name;
    }

    protected void dropItem(ItemStack stack)
    {
        EntityItem entityItem = new EntityItem(world, x, y + 1.5, z, stack);
        entityItem.pickupDelay = 10;
        this.world.addEntity(entityItem);
    }

    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public boolean a(EntityHuman entityplayer)
    {
        return true;
    }

    @Override
    public void f()
    {
    }

    @Override
    public void g()
    {
    }

    @Override
    public int getSize()
    {
        return contents.length;
    }

    @Override
    public ItemStack splitStack(int i, int j)
    {
        if(contents[i] != null) {
            if(contents[i].count <= j) {
                ItemStack itemstack = contents[i];
                contents[i] = null;
                update();
                return itemstack;
            }
            ItemStack itemstack1 = contents[i].a(j);
            if(contents[i].count == 0) {
                contents[i] = null;
            }
            update();
            return itemstack1;
        } else {
            return null;
        }
    }

    @Override
    public int getMaxStackSize()
    {
        return 64;
    }

    @Override
    public ItemStack getItem(int i)
    {
        return contents[i];
    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);
        NBTTagList nbttaglist = nbttagcompound.getList("Items");
        contents = new ItemStack[getSize()];
        for(int i = 0; i < nbttaglist.size(); i++) {
            NBTTagCompound nbttagcompound1 = (NBTTagCompound)nbttaglist.get(i);
            int j = nbttagcompound1.getByte("Slot") & 255;
            if(j >= 0 && j < contents.length) {
                contents[j] = ItemStack.a(nbttagcompound1);
            }
        }

    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);
        NBTTagList nbttaglist = new NBTTagList();
        for(int i = 0; i < contents.length; i++) {
            if(contents[i] != null) {
                NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                nbttagcompound1.setByte("Slot", (byte)i);
                contents[i].save(nbttagcompound1);
                nbttaglist.add(nbttagcompound1);
            }
        }
        nbttagcompound.set("Items", nbttaglist);
    }

    @Override
    public void setItem(int i, ItemStack itemstack)
    {
        contents[i] = itemstack;
        if(itemstack != null && itemstack.count > getMaxStackSize()) {
            itemstack.count = getMaxStackSize();
        }
        update();
    }

    @Override
    public ItemStack splitWithoutUpdate(int slot)
    {
        return null;
    }
}
