package railcraft.common.utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;
import net.minecraft.server.*;
import forge.ISidedInventory;
import railcraft.Railcraft;
import railcraft.common.RailcraftBlocks;
import railcraft.common.api.GeneralTools;

public abstract class TileOven extends TileUtilityItem implements ISidedInventory
{
    
    private enum OvenState
    {
        
        VALID, INVALID, UNKNOWN
    };
    private static final int VALIDATE_INTERVAL = 60;
    protected boolean master;
    protected boolean door;
    protected int cookTime;
    protected boolean cooking;
    private boolean wasCooking;
    protected boolean valid;
    private boolean tested;
    private boolean retest;
    protected int masterX = -1;
    protected int masterY = -1;
    protected int masterZ = -1;
    protected int update;
    
    public TileOven(EnumUtility type, String name, int invNum)
    {
        super(type, name, invNum);
        update = GeneralTools.getRand().nextInt();
    }
    
    public abstract int[][][] getOvenMap();
    
    @Override
    public boolean canUpdate()
    {
        return true;
    }
    
    @Override
    public void q_()
    {
        update++;
        if(Railcraft.gameIsHost()) {
            if((!tested || update % VALIDATE_INTERVAL == 0) && (!valid || retest) && (master || getMasterOven() == null)) {
                tested = true;
                retest = false;
                revalidateOven();
            }
            
            if(door) {
                if(update % 5 == 0) {
                    boolean c = isCooking();
                    if(wasCooking != c) {
                        wasCooking = c;
                        world.b(EnumSkyBlock.BLOCK, x, y, z);
                        world.notify(x, y, z);
                    }
                    
                    if(world.isLoaded(masterX, masterY, masterZ)) {
                        TileOven masterOven = getMasterOven();
                        if(masterOven == null) {
                            setDoor(false);
                        }
                    }
                }
            }
        }
    }
    
    public abstract int getHeight();
    
    public abstract int getWidth();
    
    public void revalidateOven()
    {
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        //System.out.println("testing oven");
        OvenState state = isMasterOvenBlock();
        if(state == OvenState.UNKNOWN) {
            return;
        } else if(state == OvenState.VALID) {
            // System.out.println("oven complete");
            for(int i = 0; i < getWidth(); i++) {
                for(int j = 0; j < getHeight(); j++) {
                    for(int k = 0; k < getWidth(); k++) {
                        if(getOvenMap()[i + 1][j + 1][k + 1] == 3) {
                            continue;
                        }
                        TileEntity tile = world.getTileEntity(i + x, j + y, k + z);
                        if(tile instanceof TileOven) {
                            TileOven oven = (TileOven)tile;
                            oven.setMasterOven(x, y, z);
                            oven.setDoor(getOvenMap()[i + 1][j + 1][k + 1] == 2);
                        } else {
                            return;
                        }
                    }
                }
            }
            master = true;
            valid = true;
        } else {
            if(master) {
                dropInventory();
            }
            master = false;
            valid = false;
        }
        if(Railcraft.gameIsServer()) {
            world.notify(x, y, z);
        }
    }
    
    private OvenState isMasterOvenBlock()
    {
        for(int i = 0; i < getWidth() + 2; i++) {
            for(int j = 0; j < getHeight() + 2; j++) {
                for(int k = 0; k < getWidth() + 2; k++) {
                    int ii = i + x - 1;
                    int jj = j + y - 1;
                    int kk = k + z - 1;
                    if(!world.isLoaded(ii, jj, kk)) {
                        return OvenState.UNKNOWN;
                    }
                    int id = world.getTypeId(ii, jj, kk);
                    switch (getOvenMap()[i][j][k]) {
                        case 0:
                            if(id == RailcraftBlocks.getBlockUtility().id && world.getData(ii, jj, kk) == getUtilityType().getId()) {
                                return OvenState.INVALID;
                            }
                            break;
                        
                        case 1:
                        case 2:
                            if(id != RailcraftBlocks.getBlockUtility().id || world.getData(ii, jj, kk) != getUtilityType().getId()) {
                                return OvenState.INVALID;
                            }
                            break;
                        
                        case 3:
                            if(id != 0) {
                                return OvenState.INVALID;
                            }
                            break;
                    }
                }
            }
        }
        
        return OvenState.VALID;
    }
    
    private void dropInventory()
    {
        for(int l = 0; l < getSize(); l++) {
            ItemStack itemstack = getItem(l);
            if(itemstack != null) {
                float f = GeneralTools.getRand().nextFloat() * 0.8F + 0.1F;
                float f1 = GeneralTools.getRand().nextFloat() * 0.8F + 0.1F;
                float f2 = GeneralTools.getRand().nextFloat() * 0.8F + 0.1F;
                while(itemstack.count > 0) {
                    int i1 = GeneralTools.getRand().nextInt(21) + 10;
                    if(i1 > itemstack.count) {
                        i1 = itemstack.count;
                    }
                    itemstack.count -= i1;
                    EntityItem entityitem = new EntityItem(world, (float)x + f, (float)y + f1, (float)z + f2, new ItemStack(itemstack.id, i1, itemstack.getData()));
                    float f3 = 0.05F;
                    entityitem.motX = (float)GeneralTools.getRand().nextGaussian() * f3;
                    entityitem.motY = (float)GeneralTools.getRand().nextGaussian() * f3 + 0.2F;
                    entityitem.motZ = (float)GeneralTools.getRand().nextGaussian() * f3;
                    world.addEntity(entityitem);
                }
                setItem(l, null);
            }
        }
    }
    
    @Override
    public void randomDisplayTick(World world, Random random)
    {
        if(door && random.nextInt(100) < 20 && isCooking()) {
            float f = (float)x + 0.5F;
            float f1 = (float)y + 0.4375F + (random.nextFloat() * 3F / 16F);
            float f2 = (float)z + 0.5F;
            float f3 = 0.52F;
            float f4 = random.nextFloat() * 0.6F - 0.3F;
            world.a("flame", f - f3, f1, f2 + f4, 0.0D, 0.0D, 0.0D);
            world.a("flame", f + f3, f1, f2 + f4, 0.0D, 0.0D, 0.0D);
            world.a("flame", f + f4, f1, f2 - f3, 0.0D, 0.0D, 0.0D);
            world.a("flame", f + f4, f1, f2 + f3, 0.0D, 0.0D, 0.0D);
        }
    }
    
    @Override
    public int getBlockTexture(int side)
    {
        if(door) {
            if(isCooking()) {
                return getUtilityType().getTexture(7);
            }
            return getUtilityType().getTexture(6);
        }
        return getUtilityType().getTexture(0);
    }
    
    @Override
    public int getSizeInventorySide(int side)
    {
        return 1;
    }
    
    @Override
    public ItemStack splitStack(int i, int j)
    {
        if(master || Railcraft.gameIsNotHost()) {
            return super.splitStack(i, j);
        }
        TileOven masterOven = getMasterOven();
        if(masterOven != null) {
            return masterOven.splitStack(i, j);
        }
        return null;
    }
    
    @Override
    public ItemStack getItem(int i)
    {
        if(master || Railcraft.gameIsNotHost()) {
            
            return super.getItem(i);
        }
        TileOven masterOven = getMasterOven();
        if(masterOven != null) {
            return masterOven.getItem(i);
        }
        return null;
    }
    
    @Override
    public void setItem(int i, ItemStack itemstack)
    {
        if(master || Railcraft.gameIsNotHost()) {
            super.setItem(i, itemstack);
        } else {
            TileOven masterOven = getMasterOven();
            if(masterOven != null) {
                masterOven.setItem(i, itemstack);
            }
        }
    }
    
    @Override
    public void onNeighborBlockChange(World world, int i, int j, int k, int id)
    {
        if(Railcraft.gameIsHost()) {
            if(id == RailcraftBlocks.getBlockUtility().id) {
                if(master) {
                    retest = true;
                } else {
                    TileOven masterOven = getMasterOven();
                    if(masterOven != null) {
                        masterOven.retest = true;
                    }
                }
            }
        }
    }
    
    @Override
    public void onBlockRemoval(World world, int i, int j, int k)
    {
        invalidateOven();
    }
    
    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);
        
        data.setBoolean("master", master);
        data.setBoolean("door", door);
        
        data.setInt("cookTime", cookTime);
        data.setBoolean("cooking", cooking);
        
        data.setInt("masterX", masterX);
        data.setInt("masterY", masterY);
        data.setInt("masterZ", masterZ);
    }
    
    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);
        
        master = data.getBoolean("master");
        door = data.getBoolean("door");
        
        cookTime = data.getInt("cookTime");
        cooking = data.getBoolean("cooking");
        
        masterX = data.getInt("masterX");
        masterY = data.getInt("masterY");
        masterZ = data.getInt("masterZ");
        
        retest = true;
        
        update = GeneralTools.getRand().nextInt();
    }
    
    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);
        
        data.writeInt(masterX);
        data.writeInt(masterY);
        data.writeInt(masterZ);
        data.writeInt(cookTime);
        
        byte bits = 0;
        bits |= master ? 1 : 0;
        bits |= door ? 2 : 0;
        bits |= cooking ? 4 : 0;
        bits |= valid ? 8 : 0;
        data.writeByte(bits);
    }
    
    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);
        
        masterX = data.readInt();
        masterY = data.readInt();
        masterZ = data.readInt();
        cookTime = data.readInt();
        
        byte bits = data.readByte();
        master = (bits & 1) != 0;
        door = (bits & 2) != 0;
        cooking = (bits & 4) != 0;
        valid = (bits & 8) != 0;
        
        world.notify(x, y, z);
    }
    
    public boolean isMaster()
    {
        return master;
    }
    
    public void setMaster(boolean m)
    {
        master = m;
    }
    
    public boolean isValidMasterOven()
    {
        return master && valid;
    }
    
    public void invalidateOven()
    {
        if(!master) {
            TileOven masterOven = getMasterOven();
            if(masterOven != null) {
                masterOven.invalidateOven();
            }
        }
        valid = false;
        tested = false;
    }
    
    public TileOven getMasterOven()
    {
        if(master) {
            return this;
        }
        if(masterY < 0 || world == null) {
            return null;
        }
        TileEntity tile = world.getTileEntity(masterX, masterY, masterZ);
        if(tile instanceof TileOven) {
            TileOven masterOven = (TileOven)tile;
            if(masterOven.isValidMasterOven()) {
                return masterOven;
            }
        }
        return null;
    }
    
    public void setMasterOven(int i, int j, int k)
    {
        if(masterX != i || masterY != j || masterZ != k) {
            masterX = i;
            masterY = j;
            masterZ = k;
            if(world != null && Railcraft.gameIsServer()) {
                world.notify(x, y, z);
            }
        }
    }
    
    public void setDoor(boolean d)
    {
        if(door != d) {
            door = d;
            if(world != null) {
                world.notify(x, y, z);
            }
        }
    }
    
    public int getCookTime()
    {
        if(master || Railcraft.gameIsNotHost()) {
            return cookTime;
        }
        TileOven masterOven = getMasterOven();
        if(masterOven != null) {
            return masterOven.getCookTime();
        }
        return -1;
    }
    
    public boolean isCooking()
    {
        if(!master) {
            TileOven masterOven = getMasterOven();
            if(masterOven != null) {
                return masterOven.isCooking();
            }
            return false;
        }
        return cooking;
    }
    
    public void setCooking(boolean c)
    {
        if(master) {
            if(cooking != c) {
                cooking = c;
                if(world != null && Railcraft.gameIsServer()) {
                    world.notify(x, y, z);
                }
            }
        } else {
            TileOven masterOven = getMasterOven();
            if(masterOven != null) {
                masterOven.setCooking(c);
            }
        }
    }
    
    public void setCookTime(int i)
    {
        cookTime = i;
    }
    
    public abstract int getTotalCookTime();
    
    public int getCookProgressScaled(int i)
    {
        return (cookTime * i) / getTotalCookTime();
    }
    
    public abstract int getBurnProgressScaled(int i);
    
    @Override
    public int getLightValue()
    {
        if(door && isCooking()) {
            return 13;
        }
        return 0;
    }
}
