package railcraft.common.utility;

import railcraft.common.util.slots.SlotMinecartSingle;
import net.minecraft.server.*;

public class ContainerDispenserCart extends Container
{

    public TileDispenserCart tile;

    public ContainerDispenserCart(PlayerInventory inventoryplayer, TileDispenserCart tile)
    {
    	this.setPlayer(inventoryplayer.player);
        this.tile = tile;
        a(new SlotMinecartSingle(tile, 0, 62, 24));
        a(new SlotMinecartSingle(tile, 1, 80, 24));
        a(new SlotMinecartSingle(tile, 2, 98, 24));
//        a(new Slot(tile, 0, 60, 24));
        for(int i = 0; i < 3; i++) {
            for(int k = 0; k < 9; k++) {
                a(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 58 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++) {
            a(new Slot(inventoryplayer, j, 8 + j * 18, 116));
        }
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        return tile.a(entityplayer);
    }

    @Override
    public ItemStack a(int i)
    {
        ItemStack startStack = null;
        Slot slot = (Slot)e.get(i);
        if(slot != null && slot.c()) {
            startStack = slot.getItem().cloneItemStack();
            if(!(startStack.getItem() instanceof ItemMinecart)){
                return null;
            }
            ItemStack moveStack = startStack.a(1);
            if(i >= tile.getSize()) {
                if(!a(moveStack, 0, tile.getSize(), false)){
                    return null;
                }
            } else {
                if(!a(moveStack, 3, e.size(), false)){
                    return null;
                }
            }
            if(moveStack.count == 0) {
                slot.c(moveStack);
            }
            if(startStack.count <= 0) {
                slot.set(null);
                return null;
            }else{
                slot.set(startStack);
            }
        }
        return startStack;
    }

    @Override
    protected boolean a(ItemStack moveStack, int start, int end, boolean flag)
    {
        boolean flag1 = false;
        int k = start;
        if(flag) {
            k = end - 1;
        }
        if(moveStack.isStackable()) {
            while(moveStack.count > 0 && (!flag && k < end || flag && k >= start)) {
                Slot slot = (Slot)e.get(k);
                ItemStack destStack = slot.getItem();
                if(destStack != null && destStack.id == moveStack.id && (!moveStack.usesData() || moveStack.getData() == destStack.getData())) {
                    int i1 = destStack.count + moveStack.count;
                    int max = Math.min(moveStack.getMaxStackSize(), slot.a());
                    if(i1 <= max) {
                        moveStack.count = 0;
                        destStack.count = i1;
                        slot.d();
                        flag1 = true;
                    } else if(destStack.count < max) {
                        moveStack.count -= moveStack.getMaxStackSize() - destStack.count;
                        destStack.count = moveStack.getMaxStackSize();
                        slot.d();
                        flag1 = true;
                    }
                }
                if(flag) {
                    k--;
                } else {
                    k++;
                }
            }
        }
        if(moveStack.count > 0) {
            int l;
            if(flag) {
                l = end - 1;
            } else {
                l = start;
            }
            do {
                if((flag || l >= end) && (!flag || l < start)) {
                    break;
                }
                Slot slot1 = (Slot)e.get(l);
                ItemStack itemstack2 = slot1.getItem();
                if(itemstack2 == null) {
                    slot1.set(moveStack.cloneItemStack());
                    slot1.d();
                    moveStack.count = 0;
                    flag1 = true;
                    break;
                }
                if(flag) {
                    l--;
                } else {
                    l++;
                }
            } while(true);
        }
        return flag1;
    }
    
    public IInventory getInventory() {
    	return tile;
    }
}
