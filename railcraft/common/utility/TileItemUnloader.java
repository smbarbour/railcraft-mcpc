package railcraft.common.utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import net.minecraft.server.*;
import buildcraft.api.ISpecialInventory;
import forge.ISidedInventory;
import ic2.api.IWrenchable;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.api.EnumDirection;
import railcraft.common.EnumGui;
import railcraft.common.api.InventoryMapper;
import railcraft.common.api.ItemStackMap;
import railcraft.common.SafeNBTWrapper;
import railcraft.common.api.CartTools;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.InventoryTools;

public class TileItemUnloader extends TileUtilityItem implements IWrenchable, ISidedInventory, IItemLoader
{

    private final InventoryMapper invMap;
    private byte percentToMove = 100;
    private short numberToMove = 1;
    private short numberMoved;
    private byte magnitude = 10;
    private ItemStackMap<Integer> movedItems = new ItemStackMap<Integer>();
    private boolean percentMode = true;
    private boolean waitTillComplete = false;
    private boolean movedItemCart = false;
    private boolean movedItemTile = false;
    private boolean powered;
    private final List<Byte> filters;
    private EntityMinecart currentCart = null;

    public TileItemUnloader()
    {
        this(EnumUtility.ITEM_UNLOADER, "Item Unloader");
    }

    protected TileItemUnloader(EnumUtility type, String name)
    {
        super(type, name, 5);
        filters = new ArrayList<Byte>();
        filters.add((byte)1);
        filters.add((byte)2);
        filters.add((byte)3);
        filters.add((byte)4);

        invMap = new InventoryMapper(this, 0, 1);
    }

    @Override
    public void setPercentMode(boolean m)
    {
        percentMode = m;
    }

    @Override
    public boolean isPercentMode()
    {
        return percentMode;
    }

    @Override
    public int getBlockTexture(int side)
    {
        return getUtilityType().getTexture(side);
    }

    @Override
    public boolean isPoweringTo(int side)
    {
        if(!powered) {
            return false;
        }
        return TileItemLoader.isPoweringTo(world, x, y, z, side);
    }

    @Override
    public boolean canConnectRedstone(int dir)
    {
        return true;
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        if(Railcraft.gameIsNotHost()) {
            return;
        }

        movedItemCart = false;
        movedItemTile = false;

        EntityMinecart cart = CartTools.getMinecartOnSide(world, x, y, z, 0.1f, getFacing());

        if(cart == null) {
            setPowered(false);
            currentCart = null;
        } else if(cart.getSize() == 0 || InventoryTools.isInventoryEmpty(cart)) {
            if(CartTools.cartVelocityIsLessThan(cart, Loader.STOP_VELOCITY) || cart.isPoweredCart()) {
                setPowered(true);
            }
        } else {
            if(cart != currentCart) {
                setPowered(false);
                currentCart = cart;
                numberMoved = 0;
                movedItems.clear();
            }

            boolean hasFilter = false;
            for(int i = 1; i < 5; i++) {
                ItemStack filter = getItem(i);
                if(filter != null) {
                    hasFilter = true;
                    break;
                }
            }

            if(percentMode && !(cart instanceof ISpecialInventory)) {
                if(hasFilter) {
                    for(int i = 0; i < 4; i++) {
                        ItemStack filter = getItem(i + 1);
                        if(filter == null) {
                            continue;
                        }
                        Integer value = movedItems.get(filter);
                        int numMoved = value != null ? value : 0;
                        float toMove = (numMoved + InventoryTools.countItems(cart, filter)) * (percentToMove / 100f);
                        if(numMoved < toMove && !isPercentMoveComplete(cart)) {
                            movedItemCart = InventoryTools.moveOneItem(cart, GeneralTools.getOppositeSide(getFacing()), invMap, getFacing(), filter);
                        }
                        if(movedItemCart) {
                            movedItems.put(filter, numMoved + 1);
                            break;
                        }
                    }
                } else {
                    ItemStack internal = getItem(0);
                    if(internal == null || internal.count < internal.getMaxStackSize()) {
                        for(int slot = 0; slot < cart.getSize(); slot++) {
                            ItemStack stack = cart.getItem(slot);
                            if(stack == null) {
                                continue;
                            }
                            Integer value = movedItems.get(stack);
                            int numMoved = value != null ? value : 0;
                            float toMove = (numMoved + InventoryTools.countItems(cart, stack)) * (percentToMove / 100f);
                            if(numMoved < toMove && !isPercentMoveComplete(cart)) {
                                movedItemCart = InventoryTools.moveOneItem(cart, GeneralTools.getOppositeSide(getFacing()), invMap, getFacing(), stack);
                            }
                            if(movedItemCart) {
                                movedItems.put(stack, numMoved + 1);
                                break;
                            }
                        }
                    }
                }
            } else {
                if(numberMoved < numberToMove || numberToMove == 0) {
                    movedItemCart = moveItemFromCart(cart, hasFilter);
                }
            }

            if(movedItemCart) {
                numberMoved++;
            }

            if(!powered && (CartTools.cartVelocityIsLessThan(cart, Loader.STOP_VELOCITY) || cart.isPoweredCart()))  {
                if(!movedItemCart && !waitTillComplete) {
                    setPowered(true);
                } else if(!isPercentMode() && numberMoved >= numberToMove && numberToMove > 0) {
                    setPowered(true);
                } else if(isPercentMode() && isPercentMoveComplete(cart)) {
                    setPowered(true);
                } else if(!movedItemCart && InventoryTools.isInventoryEmpty(cart, GeneralTools.getOppositeSide(getFacing()))) {
                    setPowered(true);
                }
            }
        }

        if(getItem(0) != null) {
            Map<Integer, IInventory> chests = InventoryTools.getAdjacentInventoryMap(world, x, y, z);
            for(Map.Entry<Integer, IInventory> entry : chests.entrySet()) {
                if(entry.getValue() instanceof TileItemUnloader) {
                    continue;
                }
                movedItemTile = InventoryTools.moveOneItem(invMap, entry.getKey(), entry.getValue(), GeneralTools.getOppositeSide(entry.getKey()));
                if(movedItemTile) {
                    break;
                }
            }
        }
    }

    private boolean isPercentMoveComplete(EntityMinecart cart)
    {
        return numberMoved >= (numberMoved + InventoryTools.countItems(cart, contents[1], contents[2], contents[3], contents[4])) * (percentToMove / 100f);
    }

    private boolean moveItemFromCart(EntityMinecart cart, boolean hasFilter)
    {
        if(hasFilter) {
            Collections.shuffle(filters);
            for(int i : filters) {
                ItemStack filter = getItem(i);
                if(filter == null) {
                    continue;
                }
                boolean ret = InventoryTools.moveOneItem(cart, GeneralTools.getOppositeSide(getFacing()), invMap, getFacing(), filter);
                if(ret) {
                    return true;
                }
            }
        } else {
            return InventoryTools.moveOneItem(cart, GeneralTools.getOppositeSide(getFacing()), invMap, EnumDirection.UP.getValue());
        }
        return false;
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);

        data.setBoolean("powered", powered);

        data.setBoolean("waitTillComplete", waitTillComplete);
        data.setBoolean("percentMode", percentMode);

        data.setByte("percentToMove", percentToMove);
        data.setShort("numberToMove", numberToMove);
        data.setShort("numberMoved", numberMoved);
        data.setByte("magnitude", magnitude);
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);

        SafeNBTWrapper safe = new SafeNBTWrapper(data);

        setPowered(data.getBoolean("powered"));

        waitTillComplete = data.getBoolean("waitTillComplete");
        percentMode = data.getBoolean("percentMode");


        percentToMove = safe.getByte("percentToMove");
        numberToMove = safe.getShort("numberToMove");
        numberMoved = safe.getShort("numberMoved");
        magnitude = safe.getByte("magnitude");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(waitTillComplete);
        data.writeBoolean(percentMode);
        data.writeByte(percentToMove);
        data.writeShort(numberToMove);
        data.writeByte(magnitude);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        waitTillComplete = data.readBoolean();
        percentMode = data.readBoolean();
        percentToMove = data.readByte();
        numberToMove = data.readShort();
        magnitude = data.readByte();
    }

    protected void setPowered(boolean p)
    {
        if(powered ^ p) {
            powered = p;
            int i = x;
            int j = y;
            int k = z;
            if(world != null) {
                world.applyPhysics(i, j, k, getBlock().id);
                world.applyPhysics(i + 1, j, k, getBlock().id);
                world.applyPhysics(i - 1, j, k, getBlock().id);
                world.applyPhysics(i, j, k + 1, getBlock().id);
                world.applyPhysics(i, j, k - 1, getBlock().id);
                world.applyPhysics(i, j - 1, k, getBlock().id);
                world.applyPhysics(i, j + 1, k, getBlock().id);
                world.notify(i, j, k);
            }
        }
    }

    @Override
    public boolean wrenchCanSetFacing(EntityHuman entityPlayer, int side)
    {
        return false;
    }

    @Override
    public void setFacing(short facing)
    {
    }

    @Override
    public boolean wrenchCanRemove(EntityHuman entityPlayer)
    {
        return true;
    }

    @Override
    public float getWrenchDropRate()
    {
        return 0;
    }

    @Override
    protected boolean openGui(EntityHuman player)
    {
        GuiHandler.openGui(EnumGui.LOADER_ITEM, player, world, x, y, z);
        return true;
    }

    @Override
    public boolean waitTillComplete()
    {
        return waitTillComplete;
    }

    @Override
    public void setWaitTillComplete(boolean wait)
    {
        waitTillComplete = wait;
    }

    @Override
    public short getFacing()
    {
        return EnumDirection.UP.getValue();
    }

    @Override
    public void setNumberToMove(int n)
    {
        numberToMove = (short)n;
    }

    @Override
    public int getNumberToMove()
    {
        return numberToMove;
    }

    @Override
    public void setPercentToMove(int p)
    {
        percentToMove = (byte)p;
    }

    @Override
    public int getPercentToMove()
    {
        return percentToMove;
    }

    @Override
    public int getStartInventorySide(int side)
    {
        return 0;
    }

    @Override
    public int getSizeInventorySide(int side)
    {
        return 1;
    }

    @Override
    public int getMagnitude()
    {
        return magnitude;
    }

    @Override
    public void setMagnitude(int magnitude)
    {
        this.magnitude = (byte)magnitude;
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
