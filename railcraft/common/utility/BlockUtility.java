package railcraft.common.utility;

import java.util.ArrayList;
import net.minecraft.server.*;
import forge.*;
import java.util.Random;

public class BlockUtility extends BlockContainer implements ITextureProvider, IConnectRedstone
{

    private Random random;

    public BlockUtility(int blockID)
    {
        super(blockID, 45, Material.STONE);

        random = new Random();

        b(4.5F);
        c(2.0F);
        a(h);
        j();
        a(true);

        ModLoader.registerTileEntity(TileItemLoader.class, "RCLoaderTile");
        ModLoader.registerTileEntity(TileItemLoaderAdvanced.class, "RCLoaderAdvancedTile");
        ModLoader.registerTileEntity(TileItemUnloader.class, "RCUnloaderTile");
        ModLoader.registerTileEntity(TileItemUnloaderAdvanced.class, "RCUnloaderAdvancedTile");
        ModLoader.registerTileEntity(TileLiquidLoader.class, "RCLoaderTileLiquid");
        ModLoader.registerTileEntity(TileLiquidUnloader.class, "RCUnloaderTileLiquid");
        ModLoader.registerTileEntity(TileDispenserCart.class, "RCMinecartDispenserTile");
        ModLoader.registerTileEntity(TileDispenserTrain.class, "RCTrainDispenserTile");
        ModLoader.registerTileEntity(TileCokeOven.class, "RCCokeOvenTile");
        ModLoader.registerTileEntity(TileBlastFurnace.class, "RCBlastFurnaceTile");
        ModLoader.registerTileEntity(TileRollingMachine.class, "RCRollingMachineTile");
        ModLoader.registerTileEntity(TileFeedStation.class, "RCFeedStationTile");
        ModLoader.registerTileEntity(TileWaterTank.class, "RCWaterTankTile");
        ModLoader.registerTileEntity(TileRockCrusher.class, "RCRockCrusherTile");

        MinecraftForge.setBlockHarvestLevel(this, "pickaxe", 2);
        MinecraftForge.setBlockHarvestLevel(this, "crowbar", 0);
    }

    @Override
    public String getTextureFile()
    {
        return "/railcraft/textures/railcraft.png";
    }

    public static String getBlockNameFromMetadata(int meta)
    {
        return EnumUtility.fromId(meta).getTag();
    }

    public int getBlockTexture(IBlockAccess world, int i, int j, int k, int side)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileUtility) {
            return ((TileUtility)tile).getBlockTexture(side);
        }
        int meta = world.getData(i, j, k);
        return getBlockTextureFromSideAndMetadata(side, meta);
    }

    public int getBlockTextureFromSideAndMetadata(int side, int meta)
    {
        return EnumUtility.fromId(meta).getTexture(side);
    }

    @Override
    public int getDropType(int meta, Random random, int j)
    {
        return id;
    }

    @Override
    public boolean interact(World world, int i, int j, int k, EntityHuman player)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileUtility) {
            return ((TileUtility)tile).blockActivated(world, i, j, k, player);
        }
        return false;
    }

    public void randomDisplayTick(World world, int i, int j, int k, Random random)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileUtility) {
            ((TileUtility)tile).randomDisplayTick(world, random);
        }
    }

    @Override
    public boolean isBlockNormalCube(World world, int i, int j, int k)
    {
        return false;
    }

    @Override
    public boolean isBlockSolidOnSide(World world, int i, int j, int k, int side)
    {
        return true;
    }

    @Override
    protected int getDropData(int meta)
    {
        return meta;
    }

    @Override
    public boolean isPowerSource()
    {
        return true;
    }

    @Override
    public boolean a(IBlockAccess world, int i, int j, int k, int side)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileUtility) {
            return ((TileUtility)tile).isPoweringTo(side);
        }
        return false;
    }

    @Override
    public boolean d(World world, int i, int j, int k, int side)
    {
        return a(world, i, j, k, side);
    }

    @Override
    public boolean canConnectRedstone(IBlockAccess world, int i, int j, int k, int dir)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileUtility) {
            ((TileUtility)tile).canConnectRedstone(dir);
        }
        return false;
    }

    @Override
    public void postPlace(World world, int i, int j, int k, EntityLiving entityliving)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileUtility) {
            ((TileUtility)tile).onBlockPlacedBy(world, i, j, k, entityliving);
        }
    }

    @Override
    public void doPhysics(World world, int i, int j, int k, int id)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileUtility) {
            ((TileUtility)tile).onNeighborBlockChange(world, i, j, k, id);
        }
    }

//    @Override
//    public void onBlockAdded(World world, int i, int j, int k)
//    {
//        TileEntity tile = world.getBlockTileEntity(i, j, k);
//        if(tile instanceof TileUtility) {
//            ((TileUtility)tile).onBlockAdded(world, i, j, k);
//        }
//    }
    @Override
    public void remove(World world, int i, int j, int k)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile != null) {
            if(tile instanceof IInventory) {
                IInventory inv = (IInventory)tile;
                for(int l = 0; l < inv.getSize(); l++) {
                    ItemStack itemstack = inv.getItem(l);
                    if(itemstack != null) {
                        float f = random.nextFloat() * 0.8F + 0.1F;
                        float f1 = random.nextFloat() * 0.8F + 0.1F;
                        float f2 = random.nextFloat() * 0.8F + 0.1F;
                        while(itemstack.count > 0) {
                            int i1 = random.nextInt(21) + 10;
                            if(i1 > itemstack.count) {
                                i1 = itemstack.count;
                            }
                            itemstack.count -= i1;
                            EntityItem entityitem = new EntityItem(world, (float)i + f, (float)j + f1, (float)k + f2, new ItemStack(itemstack.id, i1, itemstack.getData()));
                            float f3 = 0.05F;
                            entityitem.motX = (float)random.nextGaussian() * f3;
                            entityitem.motY = (float)random.nextGaussian() * f3 + 0.2F;
                            entityitem.motZ = (float)random.nextGaussian() * f3;
                            world.addEntity(entityitem);
                        }
                    }
                }
            }
            if(tile instanceof TileUtility) {
                ((TileUtility)tile).onBlockRemoval(world, i, j, k);
            }
        }
        world.applyPhysics(i + 1, j, k, id);
        world.applyPhysics(i - 1, j, k, id);
        world.applyPhysics(i, j, k + 1, id);
        world.applyPhysics(i, j, k - 1, id);
        world.applyPhysics(i, j - 1, k, id);
        world.applyPhysics(i, j + 1, k, id);
        super.remove(world, i, j, k);
    }

    @Override
    public TileEntity getBlockEntity(int md)
    {
        return EnumUtility.fromId(md).getBlockEntity();
    }

    @Override
    public TileEntity a_()
    {
        return null;
    }

    @Override
    public int getLightValue(IBlockAccess world, int i, int j, int k)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileUtility) {
            return ((TileUtility)tile).getLightValue();
        }
        return 0;
    }

    @Override
    public boolean hasTileEntity(int metadata)
    {
        return true;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void addCreativeItems(ArrayList list)
    {
        for(EnumUtility type : EnumUtility.getCreativeList()) {
            if(type.isEnabled()) {
                list.add(type.getItem());
            }
        }
    }
}