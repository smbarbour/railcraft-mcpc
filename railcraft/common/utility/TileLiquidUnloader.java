package railcraft.common.utility;

import forestry.api.liquids.LiquidStack;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;
import buildcraft.api.ILiquidContainer;
import buildcraft.api.ISpecialInventory;
import buildcraft.api.Orientations;
import forge.ISidedInventory;
import ic2.api.IWrenchable;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.RailcraftConfig;
import railcraft.common.api.EnumDirection;
import railcraft.common.EnumGui;
import railcraft.common.InventoryCopy;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.CartTools;
import railcraft.common.api.EnumItemType;
import railcraft.common.api.InventoryMapper;
import railcraft.common.api.InventoryTools;
import railcraft.common.util.liquids.ILiquidTank;
import railcraft.common.util.liquids.LiquidManager;

public class TileLiquidUnloader extends TileUtilityLiquid implements IWrenchable, ISidedInventory, ISpecialInventory, ILiquidTank
{

    private static final int CAPACITY = LiquidManager.BUCKET_VOLUME * 8;
    private static final int TRANSFER_RATE = 80;
    private static final int SLOT_LIQUID_FILTER = 0;
    private static final int SLOT_MINECART1 = 1;
    private static final int SLOT_MINECART2 = 2;
    private static final int SLOT_INPUT = 3;
    private static final int SLOT_OUTPUT = 4;
    private boolean powered;
    private boolean waitTillEmpty = true;
    private EntityMinecart currentCart = null;
    private final IInventory invCartFilter = new InventoryMapper(this, 1, 2);

    public TileLiquidUnloader()
    {
        super(EnumUtility.LIQUID_UNLOADER, "Liquid Unloader", 5);
    }

    @Override
    public int getBlockTexture(int side)
    {
        return getUtilityType().getTexture(side);
    }

    @Override
    public boolean isPoweringTo(int side)
    {
        if(!powered) {
            return false;
        }
        return TileItemLoader.isPoweringTo(world, x, y, z, side);
    }

    @Override
    public boolean canConnectRedstone(int dir)
    {
        return true;
    }

    public int getFilterLiquidId()
    {
        if(getItem(SLOT_LIQUID_FILTER) != null) {
            LiquidStack filterLiquid = LiquidManager.getInstance().getLiquidInContainer(getItem(SLOT_LIQUID_FILTER));
            return filterLiquid != null ? filterLiquid.itemID : 0;
        }
        return 0;
    }

    @Override
    public void q_()
    {
        super.q_();
        if(Railcraft.gameIsNotHost()) {
            return;
        }

        ItemStack filterSlot = getItem(SLOT_LIQUID_FILTER);
        if(filterSlot != null && !LiquidManager.getInstance().isFilledContainer(filterSlot)) {
            setItem(SLOT_LIQUID_FILTER, null);
            dropItem(filterSlot);
        }

        ItemStack minecartSlot1 = getItem(SLOT_MINECART1);
        if(minecartSlot1 != null && !InventoryTools.isItemType(minecartSlot1, EnumItemType.MINECART)) {
            setItem(SLOT_MINECART1, null);
            dropItem(minecartSlot1);
        }

        ItemStack minecartSlot2 = getItem(SLOT_MINECART2);
        if(minecartSlot2 != null && !InventoryTools.isItemType(minecartSlot2, EnumItemType.MINECART)) {
            setItem(SLOT_MINECART2, null);
            dropItem(minecartSlot2);
        }

        ItemStack topSlot = getItem(SLOT_INPUT);
        if(topSlot != null && !LiquidManager.getInstance().isContainer(topSlot)) {
            setItem(SLOT_INPUT, null);
            dropItem(topSlot);
        }

        ItemStack bottomSlot = getItem(SLOT_OUTPUT);
        if(bottomSlot != null && !LiquidManager.getInstance().isContainer(bottomSlot)) {
            setItem(SLOT_OUTPUT, null);
            dropItem(bottomSlot);
        }

        if(topSlot != null) {
            LiquidStack bucketLiquid = LiquidManager.getInstance().getLiquidInContainer(topSlot);
            if(bucketLiquid == null && LiquidManager.getInstance().isEmptyContainer(topSlot) && getLiquidQuantity() >= LiquidManager.BUCKET_VOLUME) {
                ItemStack filled = LiquidManager.getInstance().fillLiquidContainer(new LiquidStack(getLiquidId(), 0), topSlot);
                if(filled != null && (bottomSlot == null || (InventoryTools.isItemEqual(filled, bottomSlot) && bottomSlot.count < bottomSlot.getMaxStackSize()))) {
                    int drain = super.empty(LiquidManager.BUCKET_VOLUME, false);
                    if(drain == LiquidManager.BUCKET_VOLUME) {
                        super.empty(LiquidManager.BUCKET_VOLUME, true);
                        splitStack(SLOT_INPUT, 1);
                        if(bottomSlot == null) {
                            setItem(SLOT_OUTPUT, filled);
                        } else {
                            bottomSlot.count++;
                        }
                    }
                }
            }
        }


        for(int side = 0; side < 6; side++) {
            if(EnumDirection.UP.getValue() == side) {
                continue;
            }
            TileEntity tile = GeneralTools.getBlockTileEntityFromSide(world, x, y, z, side);
            if(!(tile instanceof TileLiquidUnloader) && tile instanceof ILiquidContainer) {
                ILiquidContainer lc = (ILiquidContainer)tile;
                Orientations dir = Orientations.values()[side];
                dir = dir.reverse();
                int available = super.empty(TRANSFER_RATE, false);
                int used = lc.fill(dir, available, getLiquidId(), true);
                super.empty(used, true);
            }
        }

        EntityMinecart cart = CartTools.getMinecartOnSide(world, x, y, z, 0.1f, EnumDirection.UP.getValue());

        if(cart != currentCart) {
            setPowered(false);
            currentCart = cart;
        }

        if(cart == null) {
            return;
        }

        if(!(cart instanceof ILiquidContainer)) {
            if(CartTools.cartVelocityIsLessThan(cart, Loader.STOP_VELOCITY)) {
                setPowered(true);
            }
            return;
        }

        if(minecartSlot1 != null || minecartSlot2 != null) {
            if(!CartTools.doesCartMatchFilter(minecartSlot1, cart) && !CartTools.doesCartMatchFilter(minecartSlot2, cart)) {
                if(CartTools.cartVelocityIsLessThan(cart, Loader.STOP_VELOCITY)) {
                    setPowered(true);
                }
                return;
            }
        }

        ILiquidContainer tankCart = (ILiquidContainer)cart;

        int flow = 0;
        if(getFilterLiquidId() == 0 || tankCart.getLiquidId() == getFilterLiquidId()) {
            flow = tankCart.empty(RailcraftConfig.getTankCartFillRate(), false);
            flow = super.fill(Orientations.YPos, flow, tankCart.getLiquidId(), true);
            tankCart.empty(flow, true);
        }

        if(flow > 0) {
            setPowered(false);
        }

        if(flow <= 0 && !powered && CartTools.cartVelocityIsLessThan(cart, Loader.STOP_VELOCITY)) {
            if(!waitTillEmpty()) {
                setPowered(true);
            } else if(tankCart.getLiquidQuantity() == 0) {
                setPowered(true);
            } else if(getFilterLiquidId() != 0 && tankCart.getLiquidId() != getFilterLiquidId()) {
                setPowered(true);
            }
        }
    }

    protected void setPowered(boolean p)
    {
        if(powered != p) {
            powered = p;
            int i = x;
            int j = y;
            int k = z;
            if(world != null) {
                world.applyPhysics(i, j, k, getBlock().id);
                world.applyPhysics(i + 1, j, k, getBlock().id);
                world.applyPhysics(i - 1, j, k, getBlock().id);
                world.applyPhysics(i, j, k + 1, getBlock().id);
                world.applyPhysics(i, j, k - 1, getBlock().id);
                world.applyPhysics(i, j - 1, k, getBlock().id);
                world.applyPhysics(i, j + 1, k, getBlock().id);
                world.notify(i, j, k);
            }
        }
    }

    @Override
    public int fill(Orientations from, int quantity, int id, boolean doFill)
    {
        return 0;
    }

    @Override
    public int empty(int quantityMax, boolean doEmpty)
    {
        return 0;
    }

    @Override
    public boolean canExtractLiquid()
    {
        return false;
    }

    @Override
    public int getCapacity()
    {
        return CAPACITY;
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);

        data.setBoolean("powered", powered);
        data.setBoolean("waitTillEmpty", waitTillEmpty);
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);

        setPowered(data.getBoolean("powered"));
        waitTillEmpty = data.getBoolean("waitTillEmpty");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(waitTillEmpty);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        waitTillEmpty = data.readBoolean();
    }

    @Override
    public boolean wrenchCanSetFacing(EntityHuman entityPlayer, int side)
    {
        return false;
    }

    @Override
    public void setFacing(short facing)
    {
    }

    @Override
    public boolean wrenchCanRemove(EntityHuman entityPlayer)
    {
        return true;
    }

    @Override
    public float getWrenchDropRate()
    {
        return 0;
    }

    public boolean waitTillEmpty()
    {
        return waitTillEmpty;
    }

    public void setWaitTillEmpty(boolean wait)
    {
        waitTillEmpty = wait;
    }

    @Override
    public short getFacing()
    {
        return EnumDirection.UP.getValue();
    }

    @Override
    protected boolean openGui(EntityHuman player)
    {
        GuiHandler.openGui(EnumGui.UNLOADER_LIQUID, player, world, x, y, z);
        return true;
    }

    @Override
    public int getStartInventorySide(int side)
    {
        if(side == EnumDirection.DOWN.getValue()) {
            return SLOT_OUTPUT;
        }
        return SLOT_INPUT;
    }

    @Override
    public int getSizeInventorySide(int side)
    {
        return 1;
    }

    @Override
    public boolean addItem(ItemStack stack, boolean doAdd, Orientations from)
    {
        if(LiquidManager.getInstance().isContainer(stack)) {
            IInventory slot = new InventoryMapper(this, SLOT_INPUT, 1);
            if(!doAdd) {
                stack = stack.cloneItemStack();
                slot = new InventoryCopy(slot);
            }
            ItemStack ret = InventoryTools.moveItemStack(stack, slot);
            if(ret != null && stack.count != ret.count) {
                stack.count = ret.count;
                return true;
            } else if(ret == null) {
                stack.count = 0;
                return true;
            }
        }
        return false;
    }

    @Override
    public ItemStack extractItem(boolean doRemove, Orientations from)
    {
        IInventory inv = this;
        if(!doRemove) {
            inv = new InventoryCopy(inv);
        }
        return inv.splitStack(SLOT_OUTPUT, 1);
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
