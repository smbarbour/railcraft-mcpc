package railcraft.common.utility;

import railcraft.common.util.slots.SlotBlastFurnaceFuel;
import railcraft.common.util.slots.SlotBlastFurnaceInput;
import net.minecraft.server.Container;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.ICrafting;
import net.minecraft.server.IInventory;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;
import net.minecraft.server.SlotResult2;

public class ContainerBlastFurnace extends Container
{

    private TileBlastFurnace furnace;
    private int lastCookTime = 0;
    private int lastBurnTime = 0;
    private int lastItemBurnTime = 0;

    public ContainerBlastFurnace(PlayerInventory player, TileBlastFurnace tile)
    {
    	this.setPlayer(player.player);
        this.furnace = tile;
        this.a(new SlotBlastFurnaceInput(tile, 0, 56, 17));
        this.a(new SlotBlastFurnaceFuel(tile, 1, 56, 53));
        this.a(new SlotResult2(player.player, tile, 2, 116, 35));
        int var3;

        for(var3 = 0; var3 < 3; ++var3) {
            for(int var4 = 0; var4 < 9; ++var4) {
                this.a(new Slot(player, var4 + var3 * 9 + 9, 8 + var4 * 18, 84 + var3 * 18));
            }
        }

        for(var3 = 0; var3 < 9; ++var3) {
            this.a(new Slot(player, var3, 8 + var3 * 18, 142));
        }
    }

    /**
     * Updates crafting matrix; called from onCraftMatrixChanged. Args: none
     */
    @Override
    public void a()
    {
        super.a();

        for(int var1 = 0; var1 < this.listeners.size(); ++var1) {
            ICrafting var2 = (ICrafting)this.listeners.get(var1);

            if(this.lastCookTime != this.furnace.getCookTime()) {
                var2.setContainerData(this, 0, this.furnace.getCookTime());
            }

            if(this.lastBurnTime != this.furnace.burnTime) {
                var2.setContainerData(this, 1, this.furnace.burnTime);
            }

            if(this.lastItemBurnTime != this.furnace.currentItemBurnTime) {
                var2.setContainerData(this, 2, this.furnace.currentItemBurnTime);
            }
        }

        this.lastCookTime = this.furnace.getCookTime();
        this.lastBurnTime = this.furnace.burnTime;
        this.lastItemBurnTime = this.furnace.currentItemBurnTime;
    }

    public void updateProgressBar(int par1, int par2)
    {
        if(par1 == 0) {
            this.furnace.setCookTime(par2);
        }

        if(par1 == 1) {
            this.furnace.burnTime = par2;
        }

        if(par1 == 2) {
            this.furnace.currentItemBurnTime = par2;
        }
    }

    @Override
    public boolean b(EntityHuman par1EntityPlayer)
    {
        return this.furnace.a(par1EntityPlayer);
    }

    /**
     * Called to transfer a stack from one inventory to the other eg. when shift clicking.
     */
    @Override
    public ItemStack a(int slotNum)
    {
        ItemStack original = null;
        Slot slot = (Slot)this.e.get(slotNum);

        if(slot != null && slot.c()) {
            ItemStack stack = slot.getItem();
            original = stack.cloneItemStack();

            if(slotNum >= 0 && slotNum < 3) {
                if(!this.a(stack, 3, 39, true)) {
                    return null;
                }
            } else if(slotNum >= 3 && slotNum < 39 && SlotBlastFurnaceInput.canPlaceItem(stack)) {
                if(!this.a(stack, 0, 1, false)) {
                    return null;
                }
            } else if(slotNum >= 3 && slotNum < 39 && SlotBlastFurnaceFuel.canPlaceItem(stack)) {
                if(!this.a(stack, 1, 2, false)) {
                    return null;
                }
            } else if(slotNum >= 3 && slotNum < 30) {
                if(!this.a(stack, 30, 39, false)) {
                    return null;
                }
            } else if(slotNum >= 30 && slotNum < 39) {
                if(!this.a(stack, 3, 30, false)) {
                    return null;
                }
            } else if(!this.a(stack, 3, 39, false)) {
                return null;
            }

            if(stack.count == 0) {
                slot.set((ItemStack)null);
            } else {
                slot.d();
            }

            if(stack.count == original.count) {
                return null;
            }

            slot.c(stack);
        }

        return original;
    }
    
    public IInventory getInventory() {
    	return furnace;
    }
}
