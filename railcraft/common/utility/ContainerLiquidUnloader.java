package railcraft.common.utility;

import net.minecraft.server.*;
import railcraft.common.util.slots.SlotLiquidFilter;
import railcraft.common.util.slots.SlotLiquidContainerEmpty;
import railcraft.common.util.slots.SlotLiquidMinecartSingle;
import railcraft.common.util.slots.SlotOutput;

public class ContainerLiquidUnloader extends Container
{

    private TileLiquidUnloader tile;
    private Slot bucket;
//    private Slot minecart;

    public ContainerLiquidUnloader(PlayerInventory inventoryplayer, TileLiquidUnloader tile)
    {
    	this.setPlayer(inventoryplayer.player);
        this.tile = tile;
        a(new SlotLiquidFilter(tile, 0, 53, 39));
        a(new SlotLiquidMinecartSingle(tile, 1, 89, 29));
        a(new SlotLiquidMinecartSingle(tile, 2, 89, 47));
        a(new SlotLiquidContainerEmpty(tile, 3, 134, 21));
        a(new SlotOutput(tile, 4, 134, 56));
        for(int i = 0; i < 3; i++) {
            for(int k = 0; k < 9; k++) {
                a(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++) {
            a(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }

//        minecart = (Slot)inventorySlots.get(1);
        bucket = (Slot)e.get(3);
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        return tile.a(entityplayer);
    }

    @Override
    public ItemStack a(int i)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)e.get(i);
        if(slot != null && slot.c()) {
            ItemStack itemstack1 = slot.getItem();
            itemstack = itemstack1.cloneItemStack();
            if(i >= 5 && bucket.isAllowed(itemstack1)) {
                if(!a(itemstack1, 3, 4, false)) {
                    return null;
                }
            } else if(i >= 5 && i < 32) {
                if(!a(itemstack1, 32, 41, false)) {
                    return null;
                }
            } else if(i >= 32 && i < 41) {
                if(!a(itemstack1, 5, 32, false)) {
                    return null;
                }
            } else if(!a(itemstack1, 5, 41, false)) {
                return null;
            }
            if(itemstack1.count == 0) {
                slot.set(null);
            } else {
                slot.d();
            }
            if(itemstack1.count != itemstack.count) {
                slot.c(itemstack1);
            } else {
                return null;
            }
        }
        return itemstack;
    }
    
    public IInventory getInventory() {
    	return tile;
    }
}
