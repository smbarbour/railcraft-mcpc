package railcraft.common.utility;

import railcraft.common.util.crafting.RollingMachineCraftingManager;
import railcraft.common.ItemStackSizeSorter;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.*;
import buildcraft.api.ISpecialInventory;
import buildcraft.api.Orientations;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.EnumGui;
import railcraft.common.api.InventoryTools;

public class TileRollingMachine extends TileUtilityItem implements ISpecialInventory
{

    private InventoryCrafting craftMatrix = new InventoryCrafting(new RollingContainer(), 3, 3);

    public TileRollingMachine()
    {
        super(EnumUtility.ROLLING_MACHINE, "Rolling Machine", 0);
    }

    private static class RollingContainer extends Container
    {

        @Override
        public boolean b(EntityHuman entityplayer)
        {
            return true;
        }
    }

    @Override
    public int getBlockTexture(int side)
    {
        return getUtilityType().getTexture(side);
    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);
        NBTTagList nbttaglist = nbttagcompound.getList("Items");
        for(int i = 0; i < craftMatrix.getSize(); i++) {
            craftMatrix.setItem(i, null);
        }
        for(int i = 0; i < nbttaglist.size(); i++) {
            NBTTagCompound nbttagcompound1 = (NBTTagCompound)nbttaglist.get(i);
            int j = nbttagcompound1.getByte("Slot") & 255;
            if(j >= 0 && j < craftMatrix.getSize()) {
                craftMatrix.setItem(j, ItemStack.a(nbttagcompound1));
            }
        }
    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);
        NBTTagList nbttaglist = new NBTTagList();
        for(int i = 0; i < craftMatrix.getSize(); i++) {
            if(craftMatrix.getItem(i) != null) {
                NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                nbttagcompound1.setByte("Slot", (byte)i);
                craftMatrix.getItem(i).save(nbttagcompound1);
                nbttaglist.add(nbttagcompound1);
            }
        }
        nbttagcompound.set("Items", nbttaglist);
    }

    @Override
    public boolean blockActivated(World world, int i, int j, int k, EntityHuman player)
    {
        if(Railcraft.gameIsHost() && !player.isSneaking() && (player.e(x + 0.5, y + 0.5, z + 0.5) <= 64D)) {
            GuiHandler.openGui(EnumGui.ROLLING_MACHINE, player, world, x, y, z);
        }
        return true;
    }

    @Override
    public void setItem(int i, ItemStack itemstack)
    {
        craftMatrix.setItem(i, itemstack);
    }

    @Override
    public void update()
    {
        craftMatrix.update();
    }

    @Override
    public ItemStack getItem(int i)
    {
        return craftMatrix.getItem(i);
    }

    @Override
    public int getSize()
    {
        return craftMatrix.getSize();
    }

    @Override
    public ItemStack splitStack(int i, int j)
    {
        return craftMatrix.splitStack(i, j);
    }

    public InventoryCrafting getCraftMatrix()
    {
        return craftMatrix;
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        List<IInventory> chests = InventoryTools.getAdjacentInventories(world, x, y, z, TileEntityChest.class);
        for(int slot = 0; slot < getSize(); slot++) {
            ItemStack stack = getItem(slot);
            if(stack != null && stack.isStackable() && stack.count == 1) {
                for(IInventory inv : chests) {
                    ItemStack request = InventoryTools.removeOneItem(inv, stack);
                    if(request != null) {
                        stack.count++;
                        break;
                    }
                }
                if(stack.count > 1) {
                    break;
                }
            }
        }
    }

    @Override
    public boolean addItem(ItemStack stack, boolean doAdd, Orientations from)
    {
        if(stack == null || !stack.isStackable() || stack.count <= 0) {
            return false;
        }
        List<ItemStack> slots = new ArrayList<ItemStack>();
        for(int i = 0; i < craftMatrix.getSize(); i++) {
            ItemStack slot = craftMatrix.getItem(i);
            if(slot != null && slot.doMaterialsMatch(stack)) {
                slots.add(slot);
            }
        }
        if(!slots.isEmpty()) {
            boolean added = false;
            while(stack.count > 0) {
                ItemStackSizeSorter.sort(slots);
                ItemStack slot = slots.get(0);
                if(slot.count < slot.getMaxStackSize()) {
                    if(!doAdd) {
                        return true;
                    }
                    slot.count++;
                    stack.count--;
                    added = true;
                } else {
                    return added;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public ItemStack extractItem(boolean doRemove, Orientations from)
    {
        return extractItem(doRemove, from, false);
    }

    public ItemStack extractItem(boolean doRemove, Orientations from, boolean removeLast)
    {
        InventoryCrafting matrix = new InventoryCrafting(new RollingContainer(), 3, 3);
        for(int i = 0; i < craftMatrix.getSize(); i++) {
            ItemStack slot = craftMatrix.getItem(i);
            if(slot != null) {
                matrix.setItem(i, slot.cloneItemStack());
            }
            if(!removeLast) {
                matrix.splitStack(i, 1);
            }
        }
        ItemStack result = RollingMachineCraftingManager.getInstance().findMatchingRecipe(matrix);
        if(result != null && doRemove) {
            for(int i = 0; i < craftMatrix.getSize(); i++) {
                craftMatrix.splitStack(i, 1);
            }
        }
        return result;
    }

    public boolean canMakeMore()
    {
        ItemStack result = extractItem(false, Orientations.Unknown, false);
        return result != null;
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
