package railcraft.common.utility;

import railcraft.common.util.slots.SlotUpgrade;
import railcraft.common.util.slots.SlotEnergy;
import railcraft.common.*;

import net.minecraft.server.*;

public class ContainerEnergyLoader extends Container
{

    private IEnergyDevice device;

    public ContainerEnergyLoader(PlayerInventory inventoryplayer, IEnergyDevice device)
    {
    	this.setPlayer(inventoryplayer.player);
        this.device = device;
        a(new SlotEnergy(device, 0, 8, 17));
        a(new SlotEnergy(device, 1, 8, 53));

        a(new SlotUpgrade(device, 2, 152, 8));
        a(new SlotUpgrade(device, 3, 152, 26));
        a(new SlotUpgrade(device, 4, 152, 44));
        a(new SlotUpgrade(device, 5, 152, 62));

        for(int i = 0; i < 3; i++) {
            for(int k = 0; k < 9; k++) {
                a(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++) {
            a(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        return device.a(entityplayer);
    }

    @Override
    public ItemStack a(int i)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)e.get(i);
        if(slot != null && slot.c()) {
            ItemStack itemstack1 = slot.getItem();
            itemstack = itemstack1.cloneItemStack();
            if(i >= 6 && i < 42 && SlotEnergy.canPlaceItem(itemstack1)) {
                if(!a(itemstack1, 0, 2, false)) {
                    return null;
                }
            }else if(i >= 6 && i < 42 && SlotUpgrade.canPlaceItem(itemstack1)) {
                if(!a(itemstack1, 2, 6, false)) {
                    return null;
                }
            } else if(i >= 6 && i < 33) {
                if(!a(itemstack1, 33, 42, false)) {
                    return null;
                }
            } else if(i >= 33 && i < 42) {
                if(!a(itemstack1, 6, 33, false)) {
                    return null;
                }
            } else if(!a(itemstack1, 6, 42, false)) {
                return null;
            }
            if(itemstack1.count == 0) {
                slot.set(null);
            } else {
                slot.d();
            }
            if(itemstack1.count != itemstack.count) {
                slot.c(itemstack1);
            } else {
                return null;
            }
        }
        return itemstack;
    }
    
    public IInventory getInventory() {
    	return device;
    }
}
