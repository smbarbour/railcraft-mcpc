package railcraft.common.utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.NBTTagCompound;
import buildcraft.api.IPowerReceptor;
import buildcraft.api.PowerFramework;
import buildcraft.api.PowerProvider;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.EnumGui;
import railcraft.common.InventoryCopy;
import railcraft.common.RailcraftLanguage;
import railcraft.common.api.EnumDirection;
import railcraft.common.api.InventoryMapper;
import railcraft.common.api.InventoryTools;
import railcraft.common.util.crafting.RockCrusherCraftingManager;
import railcraft.common.util.crafting.RockCrusherCraftingManager.CrusherRecipe;
import railcraft.common.modules.RailcraftModuleManager;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class TileRockCrusher extends TileMultiBlock implements IPowerReceptor
{

    private final static int PROCESS_TIME = 100;
    private final static int POWER = 15;
    private final static int[][][] CRUSHER_MAP = {
        {
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0}
        },
        {
            {0, 0, 0, 0, 0},
            {0, 1, 1, 1, 0},
            {0, 1, 1, 1, 0},
            {0, 0, 0, 0, 0}
        },
        {
            {0, 0, 0, 0, 0},
            {0, 1, 1, 1, 0},
            {0, 1, 1, 1, 0},
            {0, 0, 0, 0, 0}
        },
        {
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0}
        },};
    private int processTime;
    private IInventory inputInv = new InventoryMapper(this, 0, 9);
    private IInventory outputInv = new InventoryMapper(this, 9, 9);
    private PowerProvider powerProvider;
    private boolean hasRecipe = false;
    private boolean buildcraft = false;

    public TileRockCrusher()
    {
        super(EnumUtility.ROCK_CRUSHER, RailcraftLanguage.translate(EnumUtility.ROCK_CRUSHER.getTag()), 18);

        buildcraft = RailcraftModuleManager.isBuildcraft3Loaded();

        if(PowerFramework.currentFramework != null) {
            powerProvider = PowerFramework.currentFramework.createPowerProvider();
            powerProvider.configure(100, POWER, POWER, POWER, POWER * PROCESS_TIME);
        }
    }

    @Override
    public void q_()
    {
        super.q_();

        if(Railcraft.gameIsHost()) {
            if(isMaster()) {
                ItemStack input = null;
                CrusherRecipe recipe = null;
                for(int i = 0; i < 9; i++) {
                    input = inputInv.getItem(i);
                    if(input != null) {
                        recipe = RockCrusherCraftingManager.getRecipe(input);
                        if(recipe != null) {
                            break;
                        }
                    }
                }
                hasRecipe = recipe != null;
                if(hasRecipe) {
                    if(processTime >= PROCESS_TIME) {
                        boolean hasRoom = true;
                        IInventory tempInv = new InventoryCopy(outputInv);
                        for(ItemStack output : recipe.getPossibleOuput()) {
                            output = InventoryTools.moveItemStack(output, tempInv);
                            if(output != null) {
                                hasRoom = false;
                                break;
                            }
                        }

                        if(hasRoom) {
                            world.makeSound(x, y, z, "mob.irongolem.death", 1.5f, world.random.nextFloat() * 0.25F + 0.7F);
                            InventoryTools.removeOneItem(inputInv, input);
                            for(ItemStack output : recipe.getRandomizedOuput()) {
                                InventoryTools.moveItemStack(output, outputInv);
                            }
                            processTime = 0;
                        }
                    } else {
                        if(buildcraft) {
                            float energy = getPowerProvider().useEnergy(POWER, POWER, true);
                            if(energy >= POWER) {
//                                System.out.println("doWork=" + energy);
                                processTime++;
                            }
                        } else {
                            processTime++;
                        }
                    }
                } else {
                    processTime = 0;
                }
            }
        }
    }

    @Override
    protected void onMasterReset()
    {
        super.onMasterReset();
        processTime = 0;
        PowerProvider provider = getPowerProvider();
        if(provider != null) {
            provider.energyStored = 0;
        }
    }

    @Override
    protected boolean openGui(EntityHuman player)
    {
        TileMultiBlock masterBlock = getMasterBlock();
        if(masterBlock != null) {
            GuiHandler.openGui(EnumGui.ROCK_CRUSHER, player, world, masterBlock.x, masterBlock.y, masterBlock.z);
            return true;
        }
        return false;
    }

    @Override
    public int getBlockTexture(int side)
    {
        return getUtilityType().getTexture(side);
    }

    @Override
    public int[][][] getBlockMap()
    {
        return CRUSHER_MAP;
    }

    @Override
    public int getStartInventorySide(int side)
    {
        if(side == EnumDirection.UP.getValue()) {
            return 0;
        }
        return 9;
    }

    @Override
    public int getSizeInventorySide(int side)
    {
        return 9;
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);
        data.setInt("processTime", processTime);

        if(buildcraft && powerProvider != null) {
            PowerFramework.currentFramework.savePowerProvider(this, data);
        }
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);
        processTime = data.getInt("processTime");

        if(buildcraft) {
            PowerFramework.currentFramework.loadPowerProvider(this, data);
            powerProvider.configure(100, POWER, POWER, POWER, POWER * PROCESS_TIME);
        }
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);
        data.writeInt(processTime);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);
        processTime = data.readInt();
    }

    public int getProcessTime()
    {
        if(master || Railcraft.gameIsNotHost()) {
            return processTime;
        }
        TileRockCrusher mBlock = (TileRockCrusher)getMasterBlock();
        if(mBlock != null) {
            return mBlock.getProcessTime();
        }
        return -1;
    }

    public void setProcessTime(int processTime)
    {
        if(master || Railcraft.gameIsNotHost()) {
            this.processTime = processTime;
            return;
        }
        TileRockCrusher mBlock = (TileRockCrusher)getMasterBlock();
        if(mBlock != null) {
            mBlock.setProcessTime(processTime);
        }
    }

    public int getProgressScaled(int i)
    {
        return (getProcessTime() * i) / PROCESS_TIME;
    }

    @Override
    public void setPowerProvider(PowerProvider provider)
    {
        if(master) {
            this.powerProvider = provider;
            return;
        }
        TileRockCrusher mBlock = (TileRockCrusher)getMasterBlock();
        if(mBlock != null) {
            mBlock.powerProvider = provider;
        }
    }

    @Override
    public PowerProvider getPowerProvider()
    {
        if(!buildcraft) {
            return null;
        }
        if(master) {
            return powerProvider;
        }
        TileRockCrusher mBlock = (TileRockCrusher)getMasterBlock();
        if(mBlock != null) {
            return mBlock.powerProvider;
        }
        return null;
    }

    @Override
    public int powerRequest()
    {
//        System.out.println("Request Power");
        if(!buildcraft) {
//            System.out.println("Request: No Buildcraft");
            return 0;
        }
        PowerProvider provider = getPowerProvider();
//        System.out.println("Request: provider=" + provider);
        if(provider != null) {
//            System.out.println("Request: energyStored=" + provider.energyStored);
//            System.out.println("Request: maxEnergyStored=" + provider.maxEnergyStored);
            if(provider.energyStored < provider.maxEnergyStored) {
                int request = Math.min(POWER, provider.maxEnergyStored - (int)Math.floor(provider.energyStored));
//                System.out.println("Request=" + request);
                return request;
            }
        }
        return 0;
    }

    @Override
    public void doWork()
    {
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
