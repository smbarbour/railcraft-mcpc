package railcraft.common.utility;

import net.minecraft.server.Container;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IInventory;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.ItemMinecart;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Slot;
import railcraft.common.util.slots.SlotMinecart;
import railcraft.common.util.slots.SlotMinecartSingle;

public class ContainerDispenserTrain extends Container
{

    public TileDispenserTrain tile;

    public ContainerDispenserTrain(PlayerInventory playerInv, TileDispenserTrain tile)
    {
    	this.setPlayer(playerInv.player);
        this.tile = tile;

        for(int i = 0; i < 9; i++) {
            a(new SlotMinecartSingle(tile, i, 8 + i * 18, 31));
        }

        for(int i = 0; i < 2; i++) {
            for(int k = 0; k < 9; k++) {
                a(new SlotMinecart(tile, k + i * 9 + 9, 8 + k * 18, 67 + i * 18));
            }
        }

        for(int i = 0; i < 3; i++) {
            for(int k = 0; k < 9; k++) {
                a(new Slot(playerInv, k + i * 9 + 9, 8 + k * 18, 111 + i * 18));
            }
        }

        for(int j = 0; j < 9; j++) {
            a(new Slot(playerInv, j, 8 + j * 18, 169));
        }
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        return tile.a(entityplayer);
    }

    @Override
    public ItemStack a(int i)
    {
        ItemStack startStack = null;
        Slot slot = (Slot)e.get(i);
        if(slot != null && slot.c()) {
            ItemStack moveStack = slot.getItem();
            startStack = moveStack.cloneItemStack();
            if((i < 9 || i >= 27) && moveStack.getItem() instanceof ItemMinecart) {
                a(moveStack, 9, 27, false);
            }
            if(moveStack.count == startStack.count) {
                if(i >= 27 && i < 54) {
                    if(!a(moveStack, 54, 63, false)) {
                        return null;
                    }
                } else if(i < 27) {
                    if(!a(moveStack, 27, 63, false)) {
                        return null;
                    }
                } else if(i >= 54) {
                    if(!a(moveStack, 27, 54, false)) {
                        return null;
                    }
                }
            }

            if(moveStack.count == 0) {
                slot.set(null);
            } else {
                slot.d();
            }
            if(moveStack.count != startStack.count) {
                slot.c(moveStack);
            } else {
                return null;
            }
        }
        return startStack;
    }
    
    public IInventory getInventory() {
    	return tile;
    }
}
