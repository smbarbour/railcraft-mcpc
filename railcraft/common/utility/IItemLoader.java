package railcraft.common.utility;

import net.minecraft.server.IInventory;
import net.minecraft.server.World;

public interface IItemLoader extends IInventory
{

    public int getNumberToMove();

    public int getPercentToMove();

    public boolean isPercentMode();

    public void setNumberToMove(int n);

    public void setPercentMode(boolean m);

    public void setPercentToMove(int p);

    public int getMagnitude();

    public void setMagnitude(int m);

    public void setWaitTillComplete(boolean wait);

    public boolean waitTillComplete();

    public int getX();

    public int getY();

    public int getZ();

    public World getWorld();
}
