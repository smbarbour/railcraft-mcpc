package railcraft.common.utility;
import railcraft.common.util.slots.SlotLiquidContainer;
import railcraft.common.util.slots.SlotWaterOrEmpty;
import railcraft.common.util.slots.SlotOutput;

import net.minecraft.server.*;

public class ContainerWaterTank extends Container
{
    private TileWaterTank tank;

    public ContainerWaterTank(PlayerInventory inventoryplayer, TileWaterTank tile)
    {
    	this.setPlayer(inventoryplayer.player);
        this.tank = tile;
        a(new SlotWaterOrEmpty(tile, 0, 116, 21));
        a(new SlotOutput(tile, 1, 116, 56));
        for(int i = 0; i < 3; i++) {
            for(int k = 0; k < 9; k++) {
                a(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++) {
            a(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        return tank.a(entityplayer);
    }

    @Override
    public ItemStack a(int i)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)e.get(i);
        if(slot != null && slot.c()) {
            ItemStack itemstack1 = slot.getItem();
            itemstack = itemstack1.cloneItemStack();
            if(i >= 2 && i < 38 && SlotLiquidContainer.canPlaceItem(itemstack1)) {
                if(!a(itemstack1, 0, 1, false)) {
                    return null;
                }
            } else if(i >= 2 && i < 29) {
                if(!a(itemstack1, 29, 38, false)) {
                    return null;
                }
            } else if(i >= 29 && i < 38) {
                if(!a(itemstack1, 2, 29, false)) {
                    return null;
                }
            } else if(!a(itemstack1, 2, 38, false)) {
                return null;
            }
            if(itemstack1.count == 0) {
                slot.set(null);
            } else {
                slot.d();
            }
            if(itemstack1.count != itemstack.count) {
                slot.c(itemstack1);
            } else {
                return null;
            }
        }
        return itemstack;
    }
    
    public IInventory getInventory() {
    	return tank;
    }
}
