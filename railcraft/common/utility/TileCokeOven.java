package railcraft.common.utility;

import forestry.api.liquids.LiquidStack;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;
import buildcraft.api.ILiquidContainer;
import buildcraft.api.LiquidSlot;
import buildcraft.api.Orientations;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.api.EnumDirection;
import railcraft.common.EnumGui;
import railcraft.common.api.InventoryTools;
import railcraft.common.util.crafting.CokeOvenCraftingManager;
import railcraft.common.util.crafting.CokeOvenCraftingManager.CokeOvenRecipe;
import railcraft.common.util.liquids.LiquidManager;

public class TileCokeOven extends TileMultiBlockOven implements ILiquidContainer
{

//    private static final int COOK_TIME = 3600;
    private static final int COOK_STEP_LENGTH = 50;
    private static final int SLOT_INPUT = 0;
    private static final int SLOT_OUTPUT = 1;
    private static final int SLOT_LIQUID_OUTPUT = 2;
    private static final int SLOT_LIQUID_INPUT = 3;
    private static final int MAX_CAPACITY = 64 * LiquidManager.BUCKET_VOLUME;
    private final static int[][][] OVEN_MAP = {
        {
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0}
        },
        {
            {0, 0, 0, 0, 0},
            {0, 1, 1, 1, 0},
            {0, 1, 2, 1, 0},
            {0, 1, 1, 1, 0},
            {0, 0, 0, 0, 0}
        },
        {
            {0, 0, 0, 0, 0},
            {0, 1, 1, 1, 0},
            {0, 2, 3, 2, 0},
            {0, 1, 1, 1, 0},
            {0, 0, 0, 0, 0}
        },
        {
            {0, 0, 0, 0, 0},
            {0, 1, 1, 1, 0},
            {0, 1, 2, 1, 0},
            {0, 1, 1, 1, 0},
            {0, 0, 0, 0, 0}
        },
        {
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0}
        },};
    private int liquidId;
    private int liquidQty;
    private int finishedAt;
    private int cookTimeTotal = 3600;

    public TileCokeOven()
    {
        super(EnumUtility.COKE_OVEN, "Coke Oven", 4);
    }

    @Override
    public int[][][] getBlockMap()
    {
        return OVEN_MAP;
    }

    @Override
    public int getTotalCookTime()
    {
        if(!master) {
            TileCokeOven mBlock = (TileCokeOven)getMasterBlock();
            if(mBlock != null) {
                return mBlock.cookTimeTotal;
            }
            return 3600;
        }
        return cookTimeTotal;
    }

    @Override
    public int getBurnProgressScaled(int i)
    {
        return ((getTotalCookTime() - getCookTime()) * i) / getTotalCookTime();
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        super.q_();

        if(Railcraft.gameIsHost()) {
            if(isMaster()) {
                if(update > finishedAt + COOK_STEP_LENGTH + 5) {
                    if(cookTime <= 0) {
                        setCooking(false);
                    }
                }

                ItemStack input = getItem(SLOT_INPUT);
                if(input != null && input.count > 0) {
                    if(update % COOK_STEP_LENGTH == 0) {
                        ItemStack output = getItem(SLOT_OUTPUT);
                        CokeOvenRecipe recipe = CokeOvenCraftingManager.getRecipe(input.id, input.getData());

                        if(recipe != null
                            && (output == null || output.count + recipe.getOutput().count <= output.getMaxStackSize())
                            && internalFill(recipe.getLiquidOutput().liquidAmount, recipe.getLiquidOutput().itemID, false) >= recipe.getLiquidOutput().liquidAmount) {
                            cookTimeTotal = recipe.getCookTime();
                            cookTime += COOK_STEP_LENGTH;
                            setCooking(true);

                            if(cookTime >= recipe.getCookTime()) {
                                cookTime = 0;
                                finishedAt = update;
                                splitStack(SLOT_INPUT, 1);
                                if(output == null) {
                                    setItem(SLOT_OUTPUT, recipe.getOutput());
                                } else {
                                    output.count += recipe.getOutput().count;
                                }
                                internalFill(recipe.getLiquidOutput().liquidAmount, recipe.getLiquidOutput().itemID, true);
                            }
                            if(Railcraft.gameIsServer()) {
                                world.notify(x, y, z);
                            }
                        } else {
                            cookTime = 0;
                            setCooking(false);
                        }
                    }
                } else {
                    cookTime = 0;
                    setCooking(false);
                }

                ItemStack topSlot = getItem(SLOT_LIQUID_INPUT);
                if(topSlot != null && !LiquidManager.getInstance().isContainer(topSlot)) {
                    setItem(SLOT_LIQUID_INPUT, null);
                    dropItem(topSlot);
                }

                ItemStack bottomSlot = getItem(SLOT_LIQUID_OUTPUT);
                if(bottomSlot != null && !LiquidManager.getInstance().isContainer(bottomSlot)) {
                    setItem(SLOT_LIQUID_OUTPUT, null);
                    dropItem(bottomSlot);
                }

                if(topSlot != null) {
                    LiquidStack bucketLiquid = LiquidManager.getInstance().getLiquidInContainer(topSlot);
                    if(bucketLiquid != null && (!topSlot.getItem().k() || bottomSlot == null)) {
                        int used = internalFill(LiquidManager.BUCKET_VOLUME, bucketLiquid.itemID, false);
                        if(used >= LiquidManager.BUCKET_VOLUME) {
                            internalFill(LiquidManager.BUCKET_VOLUME, bucketLiquid.itemID, true);
                            splitStack(SLOT_LIQUID_INPUT, 1);
                            if(topSlot.getItem().k()) {
                                setItem(SLOT_LIQUID_OUTPUT, new ItemStack(topSlot.getItem().j()));
                            }
                        }
                    } else if(bucketLiquid == null && LiquidManager.getInstance().isEmptyContainer(topSlot) && getLiquidQuantity() >= LiquidManager.BUCKET_VOLUME) {
                        ItemStack filled = LiquidManager.getInstance().fillLiquidContainer(new LiquidStack(getLiquidId(), 0), topSlot);
                        if(filled != null && (bottomSlot == null || (InventoryTools.isItemEqual(filled, bottomSlot) && bottomSlot.count < bottomSlot.getMaxStackSize()))) {
                            int drain = empty(LiquidManager.BUCKET_VOLUME, false);
                            if(drain == LiquidManager.BUCKET_VOLUME) {
                                empty(LiquidManager.BUCKET_VOLUME, true);
                                splitStack(SLOT_LIQUID_INPUT, 1);
                                if(bottomSlot == null) {
                                    setItem(SLOT_LIQUID_OUTPUT, filled);
                                } else {
                                    bottomSlot.count++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    protected void onMasterReset()
    {
        super.onMasterReset();
        liquidId = 0;
        liquidQty = 0;
    }

    @Override
    public int getStartInventorySide(int side)
    {
        switch (EnumDirection.fromValue(side)) {
            case UP:
                return SLOT_INPUT;
            case DOWN:
                return SLOT_LIQUID_INPUT;
            default:
                return SLOT_OUTPUT;
        }
    }

    @Override
    public int getSizeInventorySide(int side)
    {
        switch (EnumDirection.fromValue(side)) {
            case UP:
                return 1;
            case DOWN:
                return 1;
            default:
                return 2;
        }
    }

    @Override
    public boolean openGui(EntityHuman player)
    {
        TileMultiBlock masterBlock = getMasterBlock();
        if(masterBlock != null) {
            GuiHandler.openGui(EnumGui.COKE_OVEN, player, world, masterBlock.x, masterBlock.y, masterBlock.z);
            return true;
        }
        return false;
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);

        data.setInt("liquidId", liquidId);
        data.setInt("liquidQty", liquidQty);
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);


        liquidId = data.getInt("liquidId");
        liquidQty = data.getInt("liquidQty");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeInt(liquidId);
        data.writeInt(liquidQty);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        liquidId = data.readInt();
        liquidQty = data.readInt();
    }

    @Override
    public int fill(Orientations from, int quantity, int id, boolean doFill)
    {
        return 0;
    }

    private int internalFill(int quantity, int id, boolean doFill)
    {
        if(!master) {
            TileCokeOven mBlock = (TileCokeOven)getMasterBlock();
            if(mBlock != null) {
                return mBlock.internalFill(quantity, id, doFill);
            }
            return 0;
        }
        if(liquidQty != 0 && id != liquidId) {
            return 0;
        }
        liquidId = id;

        int used = 0;
        if(liquidQty + quantity <= getCapacity()) {
            if(doFill) {
                liquidQty += quantity;
            }

            used = quantity;
        } else if(liquidQty <= getCapacity()) {
            used = getCapacity() - liquidQty;

            if(doFill) {
                liquidQty = getCapacity();
            }
        }
        return used;
    }

    @Override
    public int empty(int quantityMax, boolean doEmpty)
    {
        if(!master) {
            TileCokeOven mBlock = (TileCokeOven)getMasterBlock();
            if(mBlock != null) {
                return mBlock.empty(quantityMax, doEmpty);
            }
            return 0;
        }
        if(liquidQty >= quantityMax) {
            if(doEmpty) {
                liquidQty -= quantityMax;
            }
            return quantityMax;
        } else {
            int result = liquidQty;

            if(doEmpty) {
                liquidQty = 0;
            }

            return result;
        }
    }

    @Override
    public int getLiquidQuantity()
    {
        if(!master) {
            TileCokeOven mBlock = (TileCokeOven)getMasterBlock();
            if(mBlock != null) {
                return mBlock.liquidQty;
            }
            return 0;
        }
        return liquidQty;
    }

    @Override
    public int getLiquidId()
    {
        if(!master) {
            TileCokeOven mBlock = (TileCokeOven)getMasterBlock();
            if(mBlock != null) {
                return mBlock.liquidId;
            }
            return 0;
        }
        return liquidId;
    }

    @Override
    public int getCapacity()
    {
        return MAX_CAPACITY;
    }

    @Override
    public LiquidSlot[] getLiquidSlots()
    {
        return new LiquidSlot[]{
                new LiquidSlot(getLiquidId(), getLiquidQuantity(), getCapacity())
            };
    }

    public int getGaugeScale(int i)
    {
        return (int)Math.ceil((((float)getLiquidQuantity() / (float)(getCapacity())) * (float)i));
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
