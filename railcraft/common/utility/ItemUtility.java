package railcraft.common.utility;

import net.minecraft.server.*;
import forge.ITextureProvider;
import railcraft.common.RailcraftBlocks;

// Referenced classes of package net.minecraft.src:
//            ItemBlock, Block, BlockCloth, ItemDye,
//            ItemStack
public class ItemUtility extends ItemBlock implements ITextureProvider
{

    public ItemUtility(int i)
    {
        super(i);
        setMaxDurability(0);
        a(true);
        a("railcraftUtilityBlock");
    }

    @Override
    public String getTextureFile()
    {
        return "/railcraft/textures/railcraft.png";
    }

    public int getIconFromDamage(int damage)
    {
        return RailcraftBlocks.getBlockUtility().a(2, damage);
    }

    @Override
    public int filterData(int damage)
    {
        return damage;
    }

    @Override
    public String a(ItemStack stack)
    {
        return BlockUtility.getBlockNameFromMetadata(stack.getData());
    }
}
