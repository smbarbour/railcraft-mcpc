package railcraft.common.utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;

import buildcraft.api.ILiquidContainer;
import buildcraft.api.LiquidSlot;
import buildcraft.api.Orientations;
import railcraft.Railcraft;
import railcraft.common.api.GeneralTools;
import railcraft.common.util.liquids.LiquidManager;

public abstract class TileUtilityLiquid extends TileUtilityItem implements IInventory, ILiquidContainer
{

    private static final int NETWORK_UPDATE_INTERVAL = 10;
    private static final int MAX_CAPACITY = LiquidManager.BUCKET_VOLUME;
    private int liquidQty = 0;
    private int liquidId = 0;
    protected int update = GeneralTools.getRand().nextInt();

    protected TileUtilityLiquid(EnumUtility type, String name, int invSize)
    {
        super(type, name, invSize);
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        update++;

        if(Railcraft.gameIsServer() && update % NETWORK_UPDATE_INTERVAL == 0) {
            world.notify(x, y, z);
        }
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);

        data.setInt("liquidId", liquidId);
        data.setInt("liquidQty", liquidQty);

    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);

        liquidId = data.getInt("liquidId");
        liquidQty = data.getInt("liquidQty");
        if(liquidId == 0) {
            liquidQty = 0;
        }
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeInt(liquidId);
        data.writeInt(liquidQty);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        liquidId = data.readInt();
        liquidQty = data.readInt();
    }

    @Override
    public int fill(Orientations from, int quantity, int id, boolean doFill)
    {
        if(liquidQty != 0 && id != liquidId) {
            return 0;
        }
        liquidId = id;

        int used = 0;
        if(liquidQty + quantity <= getCapacity()) {
            if(doFill) {
                liquidQty += quantity;
            }

            used = quantity;
        } else if(liquidQty <= getCapacity()) {
            used = getCapacity() - liquidQty;

            if(doFill) {
                liquidQty = getCapacity();
            }
        }
        return used;
    }

    @Override
    public int empty(int quantityMax, boolean doEmpty)
    {
        if(liquidQty >= quantityMax) {
            if(doEmpty) {
                liquidQty -= quantityMax;
            }
            return quantityMax;
        } else {
            int result = liquidQty;

            if(doEmpty) {
                liquidQty = 0;
            }

            return result;
        }
    }

    @Override
    public int getLiquidQuantity()
    {
        return liquidQty;
    }

    @Override
    public int getCapacity()
    {
        return MAX_CAPACITY;
    }

    @Override
    public int getLiquidId()
    {
        return liquidId;
    }

    @Override
    public LiquidSlot[] getLiquidSlots()
    {
        return new LiquidSlot[]{
                new LiquidSlot(getLiquidId(), getLiquidQuantity(), getCapacity())
            };
    }

    public int getGaugeScale(int i)
    {
        return (int)(((float)getLiquidQuantity() / (float)(getCapacity())) * (float)i);
    }
}
