package railcraft.common.utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;
import buildcraft.api.ISpecialInventory;
import buildcraft.api.Orientations;
import forge.ISidedInventory;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.api.EnumDirection;
import railcraft.common.EnumGui;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftToolItems;
import railcraft.common.api.EnumItemType;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.InventoryTools;
import railcraft.common.util.crafting.BlastFurnaceCraftingManager;
import railcraft.common.util.crafting.BlastFurnaceCraftingManager.BlastFurnaceRecipe;

public class TileBlastFurnace extends TileMultiBlockOven implements ISidedInventory, ISpecialInventory
{

    private static final int FUEL_PER_TICK = 20;
    private static final int COOK_STEP_LENGTH = 10;
    private static final int SLOT_INPUT = 0;
    private static final int SLOT_FUEL = 1;
    private static final int SLOT_OUTPUT = 2;
    private final static int[][][] OVEN_MAP = {
        {
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0}
        },
        {
            {0, 0, 0, 0, 0},
            {0, 1, 2, 1, 0},
            {0, 1, 1, 1, 0},
            {0, 1, 1, 1, 0},
            {0, 1, 1, 1, 0},
            {0, 0, 0, 0, 0}
        },
        {
            {0, 0, 0, 0, 0},
            {0, 2, 1, 2, 0},
            {0, 1, 3, 1, 0},
            {0, 1, 3, 1, 0},
            {0, 1, 1, 1, 0},
            {0, 0, 0, 0, 0}
        },
        {
            {0, 0, 0, 0, 0},
            {0, 1, 2, 1, 0},
            {0, 1, 1, 1, 0},
            {0, 1, 1, 1, 0},
            {0, 1, 1, 1, 0},
            {0, 0, 0, 0, 0}
        },
        {
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0}
        },};
    /** The number of ticks that the furnace will keep burning */
    public int burnTime = 0;
    /**
     * The number of ticks that a fresh copy of the currently-burning item would keep the furnace burning for
     */
    public int currentItemBurnTime = 0;
    private int finishedAt;

    public TileBlastFurnace()
    {
        super(EnumUtility.BLAST_FURNACE, "Blast Furnace", 3);
    }

    @Override
    public int[][][] getBlockMap()
    {
        return OVEN_MAP;
    }

    @Override
    protected boolean isMapPositionValid(int i, int j, int k, int map)
    {
        int id = world.getTypeId(i, j, k);
        switch (map) {
            case 0:
                if(id != RailcraftBlocks.getBlockUtility().id || world.getData(i, j, k) != getUtilityType().getId()) {
                    return true;
                }
                break;
            case 1:
            case 2:
                if(id == RailcraftBlocks.getBlockUtility().id && world.getData(i, j, k) == getUtilityType().getId()) {
                    return true;
                }
                break;
            case 3:
                if(id == 0 || world.getMaterial(i, j, k) == Material.LAVA) {
                    return true;
                }
                break;
        }
        return false;
    }

    @Override
    protected void onMasterReset()
    {
        cookTime = 0;
        burnTime = 0;
        currentItemBurnTime = 0;
    }

    @Override
    public int getTotalCookTime()
    {
        ItemStack input = getItem(SLOT_INPUT);
        BlastFurnaceRecipe recipe = BlastFurnaceCraftingManager.getRecipe(input);
        if(recipe != null) {
            return recipe.getCookTime();
        }
        return 1;
    }

    @Override
    public int getBurnProgressScaled(int i)
    {
        if(currentItemBurnTime == 0) {
            return 0;
        }

        return burnTime * i / currentItemBurnTime;
    }

    private void setLavaIdle()
    {
        int xLava = x + 1;
        int yLava = y + 1;
        int zLava = z + 1;
        if(world.getTypeId(xLava, yLava, zLava) == 0) {
            world.setRawTypeIdAndData(xLava, yLava, zLava, Block.STATIONARY_LAVA.id, 7);
        }
    }

    private void setLavaBurn()
    {
        int xLava = x + 1;
        int yLava = y + 1;
        int zLava = z + 1;
        if(world.getTypeId(xLava, yLava, zLava) == 0) {
            world.setRawTypeIdAndData(xLava, yLava, zLava, Block.LAVA.id, 1);
        }
        yLava += 1;
        if(world.getTypeId(xLava, yLava, zLava) == 0) {
            world.setRawTypeIdAndData(xLava, yLava, zLava, Block.LAVA.id, 1);
        }
    }

    private void destroyLava()
    {
        int xLava = x + 1;
        int yLava = y + 2;
        int zLava = z + 1;
        if(world.getMaterial(xLava, yLava, zLava) == Material.LAVA) {
            world.setRawTypeId(xLava, yLava, zLava, 0);
        }
        yLava -= 1;
        if(world.getMaterial(xLava, yLava, zLava) == Material.LAVA) {
            world.setRawTypeId(xLava, yLava, zLava, 0);
        }
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        super.q_();

        if(Railcraft.gameIsHost()) {
            if(isMaster()) {
                if(update > finishedAt + COOK_STEP_LENGTH + 5) {
                    if(cookTime <= 0) {
                        setCooking(false);
                    }
                }

                if(burnTime >= FUEL_PER_TICK) {
                    burnTime -= FUEL_PER_TICK;
                } else {
                    if(burnTime != 0 && Railcraft.gameIsServer()) {
                        burnTime = 0;
                        world.notify(x, y, z);
                    } else {
                        burnTime = 0;
                    }
                }

                if(isBurning()) {
                    setLavaBurn();
                } else {
                    setLavaIdle();
                }

                ItemStack input = getItem(SLOT_INPUT);
                if(input != null && input.count > 0) {

                    ItemStack output = getItem(SLOT_OUTPUT);
                    BlastFurnaceRecipe recipe = BlastFurnaceCraftingManager.getRecipe(input);

                    if(recipe != null && recipe.isRoomForOutput(output)) {

                        if(burnTime <= FUEL_PER_TICK * 2) {
                            ItemStack fuel = getItem(SLOT_FUEL);
                            if(fuel != null && ((fuel.getItem() == Item.COAL && fuel.getData() == 1) || InventoryTools.isItemEqual(fuel, RailcraftToolItems.getCoalCoke()))) {
                                int itemBurnTime = GeneralTools.getItemBurnTime(fuel);
                                if(itemBurnTime > 0) {
                                    currentItemBurnTime = itemBurnTime + burnTime;
                                    burnTime = currentItemBurnTime;
                                    splitStack(SLOT_FUEL, 1);
                                }
                            }
                        }

                        if(update % COOK_STEP_LENGTH == 0 && isBurning()) {
                            cookTime += COOK_STEP_LENGTH;
                            setCooking(true);

                            if(cookTime >= recipe.getCookTime()) {
                                cookTime = 0;
                                finishedAt = update;
                                if(output == null) {
                                    setItem(SLOT_OUTPUT, recipe.getOutput());
                                } else {
                                    output.count += recipe.getOutputStackSize();
                                }
                                splitStack(SLOT_INPUT, 1);
                            }
                            if(Railcraft.gameIsServer()) {
                                world.notify(x, y, z);
                            }
                        }
                    } else {
                        cookTime = 0;
                        setCooking(false);
                    }
                } else {
                    cookTime = 0;
                    setCooking(false);
                }
            }
        }
    }

    @Override
    public int getStartInventorySide(int side)
    {
        if(side == EnumDirection.DOWN.getValue()) {
            return SLOT_FUEL;
        }
        if(side == EnumDirection.UP.getValue()) {
            return SLOT_INPUT;
        }
        return SLOT_OUTPUT;
    }

    @Override
    public int getSizeInventorySide(int side)
    {
        return 1;
    }

    @Override
    public boolean openGui(EntityHuman player)
    {
        TileMultiBlock masterBlock = getMasterBlock();
        if(masterBlock != null) {
            GuiHandler.openGui(EnumGui.BLAST_FURNACE, player, world, masterBlock.x, masterBlock.y, masterBlock.z);
            return true;
        }
        return false;
    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);

        nbttagcompound.setInt("burnTime", burnTime);
        nbttagcompound.setInt("currentItemBurnTime", currentItemBurnTime);
    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);

        burnTime = nbttagcompound.getInt("burnTime");
        currentItemBurnTime = nbttagcompound.getInt("currentItemBurnTime");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeInt(burnTime);
        data.writeInt(currentItemBurnTime);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        burnTime = data.readInt();
        currentItemBurnTime = data.readInt();
    }

    @Override
    public boolean isBurning()
    {
        if(master) {
            return burnTime > 0;
        }

        TileMultiBlock masterBlock = getMasterBlock();

        if(masterBlock instanceof TileBlastFurnace) {
            return ((TileBlastFurnace)masterBlock).isBurning();
        }
        return false;
    }

    @Override
    public boolean addItem(ItemStack stack, boolean doAdd, Orientations from)
    {
        if(stack == null) {
            return false;
        }
        if(InventoryTools.isItemType(stack, EnumItemType.FUEL)) {
            ItemStack fuel = getItem(SLOT_FUEL);
            if(fuel == null) {
                if(doAdd) {
                    setItem(SLOT_FUEL, stack.cloneItemStack());
                    stack.count = 0;
                }
                return true;
            } else if(InventoryTools.isItemEqual(stack, fuel)) {
                int room = fuel.getMaxStackSize() - fuel.count;
                if(room > 0) {
                    if(doAdd) {
                        int moved = room;
                        if(moved > stack.count) {
                            moved = stack.count;
                        }
                        stack.count -= moved;
                        fuel.count += moved;
                    }
                    return true;
                }
            }
            return false;
        }

        if(BlastFurnaceCraftingManager.getRecipe(stack) != null) {
            ItemStack input = getItem(SLOT_INPUT);
            if(input == null) {
                if(doAdd) {
                    setItem(SLOT_INPUT, stack.cloneItemStack());
                    stack.count = 0;
                }
                return true;
            } else if(InventoryTools.isItemEqual(stack, input)) {
                int room = input.getMaxStackSize() - input.count;
                if(room > 0) {
                    if(doAdd) {
                        int moved = room;
                        if(moved > stack.count) {
                            moved = stack.count;
                        }
                        stack.count -= moved;
                        input.count += moved;
                    }
                    return true;
                }
            }
            return false;
        }

        return false;
    }

    @Override
    public ItemStack extractItem(boolean doRemove, Orientations from)
    {
        ItemStack output = getItem(SLOT_OUTPUT);

        if(output != null && output.count >= 1) {
            output = output.cloneItemStack();
            output.count = 1;
        }

        if(doRemove) {
            splitStack(SLOT_OUTPUT, 1);
        }

        return output;
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public void setMaxStackSize(int arg0) {
		
	}
}
