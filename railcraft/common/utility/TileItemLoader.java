package railcraft.common.utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import net.minecraft.server.*;
import buildcraft.api.ISpecialInventory;
import forge.ISidedInventory;
import ic2.api.IWrenchable;
import railcraft.Railcraft;
import railcraft.GuiHandler;
import railcraft.common.api.EnumDirection;
import railcraft.common.EnumGui;
import railcraft.common.api.InventoryMapper;
import railcraft.common.api.ItemStackMap;
import railcraft.common.api.GeneralTools;
import railcraft.common.SafeNBTWrapper;
import railcraft.common.api.CartTools;
import railcraft.common.api.InventoryTools;

public class TileItemLoader extends TileUtilityItem implements IWrenchable, IItemLoader, ISidedInventory
{

    private final InventoryMapper invMap;
    private boolean powered;
    private byte percentToMove = 100;
    private short numberToMove = 1;
    private short numberMoved;
    private byte magnitude = 10;
    private ItemStackMap<Short> movedItems = new ItemStackMap<Short>();
    private boolean percentMode = true;
    private boolean waitTillComplete = false;
    private boolean movedItemCart = false;
    private final List<Byte> filters;
    private EntityMinecart currentCart = null;

    public TileItemLoader()
    {
        this(EnumUtility.ITEM_LOADER, "Item Loader");
    }

    protected TileItemLoader(EnumUtility type, String name)
    {
        super(type, name, 5);

        filters = new ArrayList<Byte>();
        filters.add((byte)1);
        filters.add((byte)2);
        filters.add((byte)3);
        filters.add((byte)4);

        invMap = new InventoryMapper(this, 0, 1);
    }

    @Override
    public int getBlockTexture(int side)
    {
        return getUtilityType().getTexture(side);
    }

    @Override
    public boolean isPoweringTo(int side)
    {
        if(!powered) {
            return false;
        }
        return isPoweringTo(world, x, y, z, side);
    }

    public static boolean isPoweringTo(World world, int i, int j, int k, int side)
    {
        int opSide = GeneralTools.getOppositeSide(side);
        int id = GeneralTools.getBlockIdOnSide(world, i, j, k, opSide);
        if(BlockMinecartTrack.d(id) || id == Block.REDSTONE_WIRE.id) {
            return true;
        }

        return false;
    }

    @Override
    public boolean canConnectRedstone(int dir)
    {
        return true;
    }

    @Override
    public short getFacing()
    {
        return EnumDirection.DOWN.getValue();
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        movedItemCart = false;

        EntityMinecart cart = CartTools.getMinecartOnSide(world, x, y, z, 0.1f, getFacing());

        if(cart != currentCart) {
            setPowered(false);
            currentCart = cart;
            numberMoved = 0;
            movedItems.clear();
        }

        if(cart == null) {
            return;
        }

        if(cart.getSize() == 0) {
            if(CartTools.cartVelocityIsLessThan(cart, Loader.STOP_VELOCITY) || cart.isPoweredCart()) {
                setPowered(true);
            }
            return;
        }

        Map<Integer, IInventory> chests = InventoryTools.getAdjacentInventoryMap(world, x, y, z);

        boolean allSpecial = true;
        for(IInventory chest : chests.values()) {
            if(!(chest instanceof ISpecialInventory)) {
                allSpecial = false;
            }
        }
        if(allSpecial) {
            percentMode = false;
        }

        chests.put(-1, invMap);

        boolean hasFilter = false;
        for(int i = 1; i < 5; i++) {
            ItemStack filter = getItem(i);
            if(filter != null) {
                hasFilter = true;
                break;
            }
        }

        if(percentMode) {
            if(hasFilter) {
                for(int i = 0; i < 4; i++) {
                    ItemStack filter = getItem(i + 1);
                    if(filter == null) {
                        continue;
                    }
                    Short value = movedItems.get(filter);
                    short numInCart = value != null ? value : 0;
                    float toMove = (numInCart + countItemsInInventories(chests.values(), filter)) * (percentToMove / 100f);
                    if(numInCart < toMove && !isPercentMoveComplete(chests.values())) {
                        movedItemCart = moveItemToCart(chests, cart, filter);
                    }
                    if(movedItemCart) {
                        movedItems.put(filter, (short)(numInCart + 1));
                        break;
                    }
                }
            } else {
                for(Map.Entry<Integer, IInventory> entry : chests.entrySet()) {
                    IInventory inv = entry.getValue();
                    if(inv instanceof ISpecialInventory) {
                        continue;
                    }
                    for(int slot = 0; slot < inv.getSize(); slot++) {
                        ItemStack stack = inv.getItem(slot);
                        if(stack == null) {
                            continue;
                        }
                        Short value = movedItems.get(stack);
                        short numInCart = value != null ? value : 0;
                        float toMove = (numInCart + countItemsInInventories(chests.values(), stack)) * (percentToMove / 100f);
                        if(numInCart < toMove && !isPercentMoveComplete(chests.values())) {
                            movedItemCart = moveItemToCart(chests, cart, stack);
                        }
                        if(movedItemCart) {
                            movedItems.put(stack, (short)(numInCart + 1));
                            break;
                        }
                    }
                    if(movedItemCart) {
                        break;
                    }
                }
            }
        } else {
            if(numberMoved < numberToMove || numberToMove == 0) {
                 for(Map.Entry<Integer, IInventory> entry : chests.entrySet()) {
                    movedItemCart = moveItemToCart(entry.getValue(), GeneralTools.getOppositeSide(entry.getKey()), cart, hasFilter);
                    if(movedItemCart) {
                        break;
                    }
                }
            }
        }

        if(movedItemCart) {
            numberMoved++;
            setPowered(false);
        }

        if(!powered && (CartTools.cartVelocityIsLessThan(cart, Loader.STOP_VELOCITY) || cart.isPoweredCart())) {
            if(!movedItemCart && !waitTillComplete) {
                setPowered(true);
            } else if(!isPercentMode() && numberMoved >= numberToMove && numberToMove > 0) {
                setPowered(true);
            } else if(!movedItemCart && InventoryTools.isInventoryFull(cart, GeneralTools.getOppositeSide(getFacing()))) {
                setPowered(true);
            } else if(isPercentMode() && numberMoved > 0 && isPercentMoveComplete(chests.values())) {
                setPowered(true);
            }
        }
    }

    private boolean isPercentMoveComplete(Collection<IInventory> chests)
    {
        return numberMoved >= (numberMoved + countItemsInInventories(chests, contents[1], contents[2], contents[3], contents[4])) * (percentToMove / 100f);
    }

    private int countItemsInInventories(Collection<IInventory> inventories, ItemStack... filter)
    {
        int count = 0;
        for(IInventory inv : inventories) {
            count += InventoryTools.countItems(inv, filter);
        }
        return count;
    }

    private boolean moveItemToCart(IInventory inv, int side, EntityMinecart cart, boolean hasFilter)
    {
        if(hasFilter) {
            Collections.shuffle(filters);
            for(int i : filters) {
                ItemStack filter = getItem(i);
                if(filter == null) {
                    continue;
                }
                boolean ret = InventoryTools.moveOneItem(inv, side, cart, GeneralTools.getOppositeSide(getFacing()), filter);
                if(ret) {
                    return true;
                }
            }
        } else {
            return InventoryTools.moveOneItem(inv, side, cart, GeneralTools.getOppositeSide(getFacing()));
        }
        return false;
    }

    private boolean moveItemToCart(Map<Integer, IInventory> chests, EntityMinecart cart, ItemStack filter)
    {
        for(Map.Entry<Integer, IInventory> entry : chests.entrySet()) {
            boolean ret = InventoryTools.moveOneItem(entry.getValue(), GeneralTools.getOppositeSide(entry.getKey()), cart, GeneralTools.getOppositeSide(getFacing()), filter);
            if(ret) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);

        data.setBoolean("powered", powered);

        data.setBoolean("waitTillComplete", waitTillComplete);
        data.setBoolean("percentMode", percentMode);

        data.setByte("percentToMove", percentToMove);
        data.setShort("numberToMove", numberToMove);
        data.setShort("numberMoved", numberMoved);
        data.setByte("magnitude", magnitude);
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);

        SafeNBTWrapper safe = new SafeNBTWrapper(data);

        setPowered(data.getBoolean("powered"));

        waitTillComplete = data.getBoolean("waitTillComplete");
        percentMode = data.getBoolean("percentMode");


        percentToMove = safe.getByte("percentToMove");
        numberToMove = safe.getShort("numberToMove");
        numberMoved = safe.getShort("numberMoved");
        magnitude = safe.getByte("magnitude");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeBoolean(waitTillComplete);
        data.writeBoolean(percentMode);
        data.writeByte(percentToMove);
        data.writeShort(numberToMove);
        data.writeByte(magnitude);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        waitTillComplete = data.readBoolean();
        percentMode = data.readBoolean();
        percentToMove = data.readByte();
        numberToMove = data.readShort();
        magnitude = data.readByte();
    }

    protected void setPowered(boolean p)
    {
        if(powered ^ p) {
            powered = p;
            int i = x;
            int j = y;
            int k = z;
            if(world != null) {
                world.applyPhysics(i, j, k, getBlock().id);
                world.applyPhysics(i + 1, j, k, getBlock().id);
                world.applyPhysics(i - 1, j, k, getBlock().id);
                world.applyPhysics(i, j, k + 1, getBlock().id);
                world.applyPhysics(i, j, k - 1, getBlock().id);
                world.applyPhysics(i, j + 1, k, getBlock().id);
                world.applyPhysics(i, j - 1, k, getBlock().id);
                world.notify(i, j, k);
            }
        }
    }

    @Override
    public boolean wrenchCanSetFacing(EntityHuman entityPlayer, int side)
    {
        return false;
    }

    @Override
    public void setFacing(short facing)
    {
    }

    @Override
    public boolean wrenchCanRemove(EntityHuman entityPlayer)
    {
        return true;
    }

    @Override
    public float getWrenchDropRate()
    {
        return 0;
    }

    @Override
    protected boolean openGui(EntityHuman player)
    {
        GuiHandler.openGui(EnumGui.LOADER_ITEM, player, world, x, y, z);
        return true;
    }

    @Override
    public boolean waitTillComplete()
    {
        return waitTillComplete;
    }

    @Override
    public void setWaitTillComplete(boolean wait)
    {
        waitTillComplete = wait;
    }

    @Override
    public void setNumberToMove(int n)
    {
        numberToMove = (short)n;
    }

    @Override
    public int getNumberToMove()
    {
        return numberToMove;
    }

    @Override
    public void setPercentToMove(int p)
    {
        percentToMove = (byte)p;
    }

    @Override
    public int getPercentToMove()
    {
        return percentToMove;
    }

    @Override
    public int getStartInventorySide(int side)
    {
        return 0;
    }

    @Override
    public int getSizeInventorySide(int side)
    {
        return 1;
    }

    @Override
    public boolean isPercentMode()
    {
        return percentMode;
    }

    @Override
    public void setPercentMode(boolean m)
    {
        percentMode = m;
    }

    @Override
    public int getMagnitude()
    {
        return magnitude;
    }

    @Override
    public void setMagnitude(int magnitude)
    {
        this.magnitude = (byte)magnitude;
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
