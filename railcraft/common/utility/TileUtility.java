package railcraft.common.utility;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockMinecartTrack;
import net.minecraft.server.EntityLiving;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.ItemStack;
import net.minecraft.server.World;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftTileEntity;
import railcraft.common.api.tracks.ITrackItem;

public abstract class TileUtility extends RailcraftTileEntity
{

    private final EnumUtility type;

    protected TileUtility(EnumUtility type)
    {
        super();
        this.type = type;
    }

    public boolean blockActivated(World world, int i, int j, int k, EntityHuman player)
    {
        if(player.isSneaking()) {
            return false;
        }
        ItemStack stack = player.U();
        if(stack != null) {
            if(stack.getItem() instanceof ITrackItem) {
                return false;
            }
            if(stack.id < Block.byId.length && BlockMinecartTrack.d(stack.id)) {
                return false;
            }
        }
        return openGui(player);
    }

    protected boolean openGui(EntityHuman player)
    {
        return false;
    }

    public Block getBlock()
    {
        return RailcraftBlocks.getBlockUtility();
    }

    public int getBlockTexture(int side)
    {
        return 0;
    }

    public int getLightValue()
    {
        return 0;
    }

    public EnumUtility getUtilityType()
    {
        return type;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public int getZ()
    {
        return z;
    }

    public boolean isPoweringTo(int side)
    {
        return false;
    }

    public boolean canConnectRedstone(int dir)
    {
        return false;
    }

    public void onBlockPlacedBy(World world, int i, int j, int k, EntityLiving entityliving)
    {
    }

//    public void onBlockAdded(World world, int i, int j, int k)
//    {
//    }

    public void onBlockRemoval(World world, int i, int j, int k)
    {
    }

    public void onNeighborBlockChange(World world, int i, int j, int k, int id)
    {
    }

    public void randomDisplayTick(World world, Random rand)
    {
    }

    @Override
    public int getId()
    {
        return getUtilityType().getId();
    }
}
