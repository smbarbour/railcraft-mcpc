package railcraft.common.utility;

import railcraft.common.util.slots.SlotRollingMachine;
import railcraft.common.util.crafting.RollingMachineCraftingManager;
import net.minecraft.server.*;

public class ContainerRollingMachine extends Container
{
    private TileRollingMachine tile;
    private InventoryCrafting craftMatrix;
    private IInventory craftResult;
//    private World worldObj;
//    private int posX;
//    private int posY;
//    private int posZ;

    public ContainerRollingMachine(PlayerInventory inventoryplayer, TileRollingMachine tile)
    {
    	this.setPlayer(inventoryplayer.player);
        this.tile = tile;
        craftMatrix = tile.getCraftMatrix();
        craftResult = new InventoryCraftResult();
//        worldObj = world;
//        posX = i;
//        posY = j;
//        posZ = k;
        a(new SlotRollingMachine(inventoryplayer.player, tile, craftResult, 0, 124, 35));
        for(int l = 0; l < 3; l++) {
            for(int k1 = 0; k1 < 3; k1++) {
                a(new Slot(craftMatrix, k1 + l * 3, 30 + k1 * 18, 17 + l * 18));
            }

        }

        for(int i1 = 0; i1 < 3; i1++) {
            for(int l1 = 0; l1 < 9; l1++) {
                a(new Slot(inventoryplayer, l1 + i1 * 9 + 9, 8 + l1 * 18, 84 + i1 * 18));
            }

        }

        for(int j1 = 0; j1 < 9; j1++) {
            a(new Slot(inventoryplayer, j1, 8 + j1 * 18, 142));
        }

        a(craftMatrix);
    }

    @Override
    public void a(IInventory iinventory)
    {
        craftResult.setItem(0, RollingMachineCraftingManager.getInstance().findMatchingRecipe(craftMatrix));
    }

    @Override
    public ItemStack clickItem(int i, int j, boolean flag, EntityHuman entityplayer)
    {
    	//System.out.println("RollingMachine clickItem - flag: " + flag);
        craftResult.setItem(0, RollingMachineCraftingManager.getInstance().findMatchingRecipe(craftMatrix));
        ItemStack stack = super.clickItem(i, j, flag, entityplayer);
        a(craftMatrix);
        return stack;
    }

    @Override
    public void a(EntityHuman entityplayer)
    {
        super.a(entityplayer);
//        if(worldObj.multiplayerWorld) {
//            return;
//        }
//        for(int i = 0; i < 9; i++) {
//            ItemStack itemstack = craftMatrix.getStackInSlot(i);
//            if(itemstack != null) {
//                entityplayer.dropPlayerItem(itemstack);
//            }
//        }

    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        return true;
//        return entityplayer.getDistanceSq((double)posX + 0.5D, (double)posY + 0.5D, (double)posZ + 0.5D) <= 64D;
    }

    public ItemStack a(int i)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)e.get(i);
        if(slot != null && slot.c()) {
            if(i == 0) {
                ItemStack stack = null;
                do {
                    if(slot.c()) {
                        stack = slot.getItem();
                        if(!a(stack, 10, 46, true)) {
                            return null;
                        }
                        slot.d();
                        slot.c(stack);
                        a(craftMatrix);
                    }
                } while(tile.canMakeMore());
                return null;
            }
            ItemStack itemstack1 = slot.getItem();
            itemstack = itemstack1.cloneItemStack();
            if(i >= 10 && i < 37) {
                if(!a(itemstack1, 37, 46, false)) {
                    return null;
                }
            } else if(i >= 37 && i < 46) {
                if(!a(itemstack1, 10, 37, false)) {
                    return null;
                }
            } else if(!a(itemstack1, 10, 46, false)) {
                return null;
            }
            if(itemstack1.count == 0) {
                slot.set(null);
            } else {
                slot.d();
            }
            if(itemstack1.count != itemstack.count) {
                slot.c(itemstack1);
            } else {
                return null;
            }
        }
        return itemstack;
    }
    
    public IInventory getInventory() {
    	return tile;
    }
}
