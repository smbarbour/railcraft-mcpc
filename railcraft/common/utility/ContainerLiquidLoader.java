package railcraft.common.utility;

import net.minecraft.server.*;
import railcraft.common.util.slots.SlotLiquidContainerFilled;
import railcraft.common.util.slots.SlotLiquidMinecartSingle;
import railcraft.common.util.slots.SlotOutput;

public class ContainerLiquidLoader extends Container
{

    private TileLiquidLoader tile;
    private Slot bucket;

    public ContainerLiquidLoader(PlayerInventory inventoryplayer, TileLiquidLoader tile)
    {
    	this.setPlayer(inventoryplayer.player);
        this.tile = tile;
        a(new SlotLiquidMinecartSingle(tile, 0, 45, 21));
        a(new SlotLiquidMinecartSingle(tile, 1, 63, 21));
        a(new SlotLiquidContainerFilled(tile, 2, 134, 21));
        a(new SlotOutput(tile, 3, 134, 56));
        for(int i = 0; i < 3; i++) {
            for(int k = 0; k < 9; k++) {
                a(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++) {
            a(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }

//        minecart = (Slot)inventorySlots.get(0);
        bucket = (Slot)e.get(2);
    }

    @Override
    public boolean b(EntityHuman entityplayer)
    {
        return tile.a(entityplayer);
    }

    @Override
    public ItemStack a(int i)
    {
        ItemStack startStack = null;
        Slot slot = (Slot)e.get(i);
        if(slot != null && slot.c()) {
            ItemStack moveStack = slot.getItem();
            startStack = moveStack.cloneItemStack();
            if(i >= 4 && bucket.isAllowed(moveStack) && !((Slot)e.get(2)).c()) {
                if(!a(moveStack, 2, 3, false)) {
                    return null;
                }
            } else if(i >= 4 && i < 31) {
                if(!a(moveStack, 31, 40, false)) {
                    return null;
                }
            } else if(i >= 31 && i < 40) {
                if(!a(moveStack, 4, 31, false)) {
                    return null;
                }
            } else if(!a(moveStack, 4, 40, false)) {
                return null;
            }
            if(moveStack.count == 0) {
                slot.set(null);
            } else {
                slot.d();
            }
            if(moveStack.count != startStack.count) {
                slot.c(moveStack);
            } else {
                return null;
            }
        }
        return startStack;
    }
    
    public IInventory getInventory() {
    	return tile;
    }
}
