package railcraft.common.utility;

import forestry.api.liquids.LiquidStack;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.Block;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.ItemStack;
import net.minecraft.server.MathHelper;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import buildcraft.api.ILiquidContainer;
import buildcraft.api.LiquidSlot;
import buildcraft.api.Orientations;
import railcraft.GuiHandler;
import railcraft.Railcraft;
import railcraft.common.EnumGui;
import railcraft.common.RailcraftLanguage;
import railcraft.common.api.EnumDirection;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.InventoryTools;
import railcraft.common.util.liquids.ILiquidTank;
import railcraft.common.util.liquids.LiquidManager;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class TileWaterTank extends TileMultiBlock implements ILiquidTank
{

    private final static int TANK_CAPACITY = LiquidManager.BUCKET_VOLUME * 400;
    private final static int REFILL_INTERVAL = 10;
    private final static float REFILL_RATE = 10f;
    private final static float REFILL_PENALTY_INSIDE = 0.5f;
    private final static float REFILL_PENALTY_SNOW = 0.5f;
    private final static float REFILL_BOOST_RAIN = 2.0f;
    private final static byte REFILL_RATE_MIN = 1;
    private final static int TRANSFER_RATE = 50;
    private final static int SLOT_INPUT = 0;
    private final static int SLOT_OUTPUT = 1;
    private final static int[][][] TANK_MAP = {
        {
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0}
        },
        {
            {0, 0, 0, 0, 0},
            {0, 1, 1, 1, 0},
            {0, 1, 1, 1, 0},
            {0, 1, 1, 1, 0},
            {0, 0, 0, 0, 0}
        },
        {
            {0, 0, 0, 0, 0},
            {0, 1, 1, 1, 0},
            {0, 1, 3, 1, 0},
            {0, 1, 1, 1, 0},
            {0, 0, 0, 0, 0}
        },
        {
            {0, 0, 0, 0, 0},
            {0, 1, 1, 1, 0},
            {0, 1, 1, 1, 0},
            {0, 1, 1, 1, 0},
            {0, 0, 0, 0, 0}
        },
        {
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0}
        },};
    private int liquidQty = 0;
    protected int update = GeneralTools.getRand().nextInt();

    public TileWaterTank()
    {
        super(EnumUtility.WATER_TANK, RailcraftLanguage.translate("gui.tank.water"), 2);
    }

    @Override
    public void q_()
    {
        super.q_();

        update++;

        if(Railcraft.gameIsHost()) {
            if(isMaster()) {
                if(world.worldProvider.dimension != -1 && update % REFILL_INTERVAL == 0) {
                    float rate = REFILL_RATE;
                    BiomeBase biome = world.getBiome(x, z);
                    float humidity = biome.G;
                    rate *= humidity;
//                    String debug = "Biome=" + biome.biomeName + ", Humidity=" + humidity;

                    boolean outside = false;
                    for(int localx = this.x; localx < x + 3; localx++) {
                        for(int localz = this.z; localz < z + 3; localz++) {
                            outside = world.isChunkLoaded(localx, y + 3, localz);
//                            System.out.println(x + ", " + (yCoord + 3) + ", " + z);
                            if(outside) {
                                break;
                            }
                        }
                    }

//                    debug += ", Outside=" + outside;

                    if(!outside) {
                        rate *= REFILL_PENALTY_INSIDE;
                    } else if(world.x()) {
                        if(biome.c()) {
                            rate *= REFILL_PENALTY_SNOW;
//                            debug += ", Snow=true";
                        } else {
                            rate *= REFILL_BOOST_RAIN;
//                            debug += ", Rain=true";
                        }
                    }
                    int rateFinal = MathHelper.floor(rate);
                    if(rateFinal < REFILL_RATE_MIN) {
                        rateFinal = REFILL_RATE_MIN;
                    }
//                    debug += ", Refill=" + rateFinal;
//                    System.out.println(debug);
                    fill(Orientations.YPos, rateFinal, getLiquidId(), true);
                }

                ItemStack topSlot = getItem(0);
                ItemStack bottomSlot = getItem(1);
                if(topSlot != null) {
                    LiquidStack bucketLiquid = LiquidManager.getInstance().getLiquidInContainer(topSlot);
                    if(bucketLiquid != null && (!topSlot.getItem().k() || bottomSlot == null)) {
                        int used = fill(Orientations.YPos, LiquidManager.BUCKET_VOLUME, bucketLiquid.itemID, false);
                        if(used >= LiquidManager.BUCKET_VOLUME) {
                            fill(Orientations.YPos, LiquidManager.BUCKET_VOLUME, bucketLiquid.itemID, true);
                            splitStack(SLOT_INPUT, 1);
                            if(topSlot.getItem().k()) {
                                setItem(SLOT_OUTPUT, new ItemStack(topSlot.getItem().j()));
                            }
                        }
                    } else if(bucketLiquid == null && LiquidManager.getInstance().isEmptyContainer(topSlot) && getLiquidQuantity() >= LiquidManager.BUCKET_VOLUME) {
                        ItemStack filled = LiquidManager.getInstance().fillLiquidContainer(new LiquidStack(getLiquidId(), 0), topSlot);
                        if(filled != null && (bottomSlot == null || (InventoryTools.isItemEqual(filled, bottomSlot) && bottomSlot.count < bottomSlot.getMaxStackSize()))) {
                            int drain = internalEmpty(LiquidManager.BUCKET_VOLUME, false);
                            if(drain == LiquidManager.BUCKET_VOLUME) {
                                internalEmpty(LiquidManager.BUCKET_VOLUME, true);
                                splitStack(SLOT_INPUT, 1);
                                if(bottomSlot == null) {
                                    setItem(SLOT_OUTPUT, filled);
                                } else {
                                    bottomSlot.count++;
                                }
                            }
                        }
                    }
                }

                if(update % 20 == 0) {
                    world.notify(x, y, z);
                }
            }

            TileWaterTank tank = this;
            if(!master) {
                tank = (TileWaterTank)getMasterBlock();
            }

            if(tank != null) {
                for(int side = 0; side < 6; side++) {
                    if(EnumDirection.UP.getValue() == side) {
                        continue;
                    }
                    TileEntity tile = GeneralTools.getBlockTileEntityFromSide(world, x, y, z, side);
                    if(!(tile instanceof TileWaterTank) && tile instanceof ILiquidContainer) {
                        ILiquidContainer lc = (ILiquidContainer)tile;
                        Orientations dir = Orientations.values()[side];
                        dir = dir.reverse();
                        int available = tank.internalEmpty(TRANSFER_RATE, false);
                        int used = lc.fill(dir, available, getLiquidId(), true);
                        tank.internalEmpty(used, true);
                    }
                }
            }
        }
    }

    @Override
    protected void onMasterReset()
    {
        super.onMasterReset();
        liquidQty = 0;
    }

    public int getGaugeScale(int i)
    {
        return (int)Math.ceil((((float)getLiquidQuantity() / (float)(getCapacity())) * (float)i));
    }

    @Override
    protected boolean openGui(EntityHuman player)
    {
        TileMultiBlock masterBlock = getMasterBlock();
        if(masterBlock != null) {
            GuiHandler.openGui(EnumGui.WATER_TANK, player, world, masterBlock.x, masterBlock.y, masterBlock.z);
            return true;
        }
        return false;
    }

    @Override
    public int getBlockTexture(int side)
    {
        return getUtilityType().getTexture(side);
    }

    @Override
    public int[][][] getBlockMap()
    {
        return TANK_MAP;
    }

    @Override
    public int getStartInventorySide(int side)
    {
        if(side == EnumDirection.UP.getValue()) {
            return 0;
        }
        return 1;
    }

    @Override
    public int getSizeInventorySide(int side)
    {
        return 1;
    }

    @Override
    public int fill(Orientations from, int quantity, int id, boolean doFill)
    {
        if(id != getLiquidId() || from != Orientations.YPos) {
            return 0;
        }

        TileWaterTank tank = this;
        if(!master) {
            tank = (TileWaterTank)getMasterBlock();
        }
        if(tank == null) {
            return 0;
        }
        int used = 0;
        if(tank.liquidQty + quantity <= TANK_CAPACITY) {
            if(doFill) {
                tank.liquidQty += quantity;
            }

            used = quantity;
        } else if(tank.liquidQty <= TANK_CAPACITY) {
            used = TANK_CAPACITY - tank.liquidQty;

            if(doFill) {
                tank.liquidQty = TANK_CAPACITY;
            }
        }
        return used;
    }

    @Override
    public boolean canExtractLiquid()
    {
        return false;
    }

    @Override
    public int empty(int quantityMax, boolean doEmpty)
    {
        return 0;
    }

    public int internalEmpty(int quantityMax, boolean doEmpty)
    {
        TileWaterTank tank = this;
        if(!master) {
            tank = (TileWaterTank)getMasterBlock();
        }
        if(tank == null) {
            return 0;
        }
        if(tank.liquidQty >= quantityMax) {
            if(doEmpty) {
                tank.liquidQty -= quantityMax;
            }
            return quantityMax;
        } else {
            int result = tank.liquidQty;

            if(doEmpty) {
                tank.liquidQty = 0;
            }

            return result;
        }
    }

    @Override
    public int getLiquidQuantity()
    {
        if(!master) {
            TileWaterTank masterTank = (TileWaterTank)getMasterBlock();
            if(masterTank != null) {
                return masterTank.liquidQty;
            }
            return 0;
        }
        return liquidQty;
    }

    @Override
    public int getCapacity()
    {
        return TANK_CAPACITY;
    }

    @Override
    public int getLiquidId()
    {
        return Block.STATIONARY_WATER.id;
    }

    @Override
    public LiquidSlot[] getLiquidSlots()
    {
        return new LiquidSlot[]{new LiquidSlot(getLiquidId(), getLiquidQuantity(), TANK_CAPACITY)};
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);

        liquidQty = data.getInt("liquidQty");
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);

        data.setInt("liquidQty", liquidQty);
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeInt(liquidQty);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        liquidQty = data.readInt();
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
