package railcraft.common;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class RailcraftConstants
{

    public static final String LOAD_ORDER = "after:mod_BuildCraftCore;after:mod_Forestry;after:mod_IC2";
    public static final String MAIN_TEXTURE_FILE = "/railcraft/textures/railcraft.png";
    public static final String TRACK_TEXTURE_FILE = "/railcraft/textures/tracks.png";
    public static final String LIQUID_TEXTURE_FILE = "/railcraft/textures/liquids.png";
}
