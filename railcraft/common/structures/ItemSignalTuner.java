package railcraft.common.structures;

import railcraft.common.api.ISignalController;
import railcraft.common.api.ISignalReceiver;
import net.minecraft.server.*;
import forge.ITextureProvider;
import ic2.api.IBoxable;
import railcraft.Railcraft;
import railcraft.common.api.WorldCoordinate;
import railcraft.common.api.SignalTools;

public class ItemSignalTuner extends Item implements ITextureProvider, IBoxable
{

    public ItemSignalTuner(int id)
    {
        super(id);
        setMaxDurability(0);
        a(true);
        e(1);
        d(118);
        a("signalTuner");
    }

    @Override
    public boolean onItemUseFirst(ItemStack item, EntityHuman player, World world, int i, int j, int k, int side)
    {
        TileEntity t = world.getTileEntity(i, j, k);
        if(t != null) {
            WorldCoordinate cPos = SignalTools.getSavedController(player, item);
            if(t instanceof ISignalReceiver && cPos != null) {
                if(Railcraft.gameIsHost()) {
                    ISignalReceiver receiver = (ISignalReceiver)t;
                    if(i != cPos.x || j != cPos.y || k != cPos.z) {
                        t = world.getTileEntity(cPos.x, cPos.y, cPos.z);
                        if(t != null && t instanceof ISignalController) {
                            ISignalController controller = (ISignalController)t;
                            if(receiver != controller && controller.attemptToPairWithReceiver(receiver)) {
                                controller.endReceiverPairing();
                                player.a(controller.getDescription() + " successfully paired with " + receiver.getDescription());
                                SignalTools.endControllerReceiverPairing(player, item);
                                return true;
                            }
                        } else if(world.isLoaded(cPos.x, cPos.y, cPos.z)) {
                            player.a("Controller no longer exists, abandoning pairing");
                            SignalTools.endControllerReceiverPairing(player, item);
                        } else {
                            player.a("Controller is not in a loaded Chunk");
                        }
                    }
                }
            } else if(t instanceof ISignalReceiver && !(t instanceof ISignalController)) {
                if(Railcraft.gameIsHost()) {
                    ISignalReceiver receiver = (ISignalReceiver)t;
                    if(receiver.isPairedWithController()) {
                        ISignalController controller = receiver.getController();
                        receiver.clearPairedController();
                        if(controller == null) {
                            player.a(receiver.getDescription() + " Controller pairing was removed");
                        } else {
                            controller.clearPairedReceiver();
                            player.a(controller.getDescription() + " is no longer paired with " + receiver.getDescription());
                        }
                    }
                }
            } else if(t instanceof ISignalController) {
                if(Railcraft.gameIsHost()) {
                    ISignalController controller = (ISignalController)t;
                    if(cPos == null || (i != cPos.x || j != cPos.y || k != cPos.z)) {
                        player.a("Started pairing " + controller.getDescription() + " with a Receiver");
                        SignalTools.startControllerReceiverPairing(player, item, controller);
                        player.inventory.setItem(player.inventory.itemInHandIndex, item.cloneItemStack());
                    } else {
                        player.a("Stopped pairing " + controller.getDescription() + " with a Receiver");
                        controller.endReceiverPairing();
                        SignalTools.endControllerReceiverPairing(player, item);
                    }
                }
            } else {
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public String getTextureFile()
    {
        return "/railcraft/textures/railcraft.png";
    }

    @Override
    public boolean canBeStoredInToolbox(ItemStack itemstack)
    {
        return true;
    }
}
