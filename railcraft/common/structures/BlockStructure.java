package railcraft.common.structures;

import java.util.ArrayList;
import java.util.Random;
import net.minecraft.server.*;
import forge.IConnectRedstone;
import forge.ITextureProvider;
import ic2.api.IPaintableBlock;
import railcraft.common.EnumColor;

public class BlockStructure extends BlockContainer implements ITextureProvider, IConnectRedstone, IPaintableBlock
{

    private final int renderType;

    public BlockStructure(int id, int renderType)
    {
        super(id, 0, new MaterialStructure());
        this.renderType = renderType;
        a("structureBlock");
        a(i);
        b(20);
    }

    @Override
    public String getTextureFile()
    {
        return "/railcraft/textures/railcraft.png";
    }

    @Override
    public int a(int side, int meta)
    {
        return EnumStructure.fromId(meta).getBlockTextureFromSide(side);
    }

    @Override
    public int getDropType(int meta, Random random, int j)
    {
        if(meta == EnumStructure.METAL_POST.getId()) {
            return ItemMetalPost.getInstance().id;
        }
        return id;
    }

    @Override
    protected int getDropData(int meta)
    {
        if(meta == EnumStructure.METAL_POST.getId()) {
            return 0;
        }
        return meta;
    }

    @Override
    public boolean interact(World world, int i, int j, int k, EntityHuman player)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileStructure) {
            return ((TileStructure)tile).blockActivated(world, i, j, k, player);
        }
        return false;
    }

    @Override
    public void postPlace(World world, int i, int j, int k, int l)
    {
        super.postPlace(world, i, j, k, l);
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileStructure) {
            ((TileStructure)tile).onBlockPlaced(world, i, j, k, l);
        }
    }

    @Override
    public void postPlace(World world, int i, int j, int k, EntityLiving entityliving)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileStructure) {
            ((TileStructure)tile).onBlockPlacedBy(world, i, j, k, entityliving);
        }
    }

    @Override
    public void doPhysics(World world, int i, int j, int k, int l)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileStructure) {
            TileStructure structure = (TileStructure)tile;
            if(structure.getStructureType().needsSupport() && !world.isBlockSolidOnSide(i, j - 1, k, 1)) {
                world.setTypeId(i, j, k, 0);
                b(world, i, j, k, structure.getStructureType().getId(), 0);
            }
            structure.onNeighborBlockChange(world, i, j, k, l);
        }
    }

    @Override
    public void remove(World world, int i, int j, int k)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileStructure) {
            ((TileStructure)tile).onBlockRemoval(world, i, j, k);
        }
        super.remove(world, i, j, k);
    }

    @Override
    public void updateShape(IBlockAccess world, int i, int j, int k)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileStructure) {
            ((TileStructure)tile).setBlockBoundsBasedOnState(world, i, j, k);
        } else {
            a(0, 0, 0, 1, 1, 1);
        }
    }

    @Override
    public AxisAlignedBB e(World world, int i, int j, int k)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileStructure) {
            return ((TileStructure)tile).getCollisionBoundingBoxFromPool(world, i, j, k);
        }
        a(0, 0, 0, 1, 1, 1);
        return super.e(world, i, j, k);
    }

    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int i, int j, int k)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileStructure) {
            return ((TileStructure)tile).getSelectedBoundingBoxFromPool(world, i, j, k);
        }
        return AxisAlignedBB.b((double)i + minX, (double)j + minY, (double)k + minZ, (double)i + maxX, (double)j + maxY, (double)k + maxZ);
    }

    @Override
    public int getLightValue(IBlockAccess world, int i, int j, int k)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof ISignalTile) {
            return ((ISignalTile)tile).getLightValue();
        }
        return 0;
    }

    @Override
    public float getHardness(int md)
    {
        return EnumStructure.fromId(md).getHardness();
    }

    @Override
    public boolean isBlockSolidOnSide(World world, int i, int j, int k, int side)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileStructure) {
            return ((TileStructure)tile).isBlockSolidOnSide(world, i, j, k, side);
        }
        return false;
    }

    @Override
    public boolean a()
    {
        return false;
    }

    public boolean renderAsNormalBlock()
    {
        return false;
    }

    public int getRenderType()
    {
        return renderType;
    }

//    public boolean connectToPostAt(IBlockAccess world, int i, int j, int k, EnumStructure structure)
//    {
//        boolean connect = false;
//        int id = world.getBlockId(i, j, k);
//        if(id == blockID)
//        {
//            int meta = world.getBlockMetadata(i, j, k);
//            connect = EnumStructure.fromId(meta).connectToPost();
//        }
//        if(id == Block.fenceGate.blockID && structure.isPost())
//        {
//            connect = true;
//        }
//        return connect;
//    }
    @Override
    public TileEntity a_()
    {
        return null;
    }

    @Override
    public TileEntity getBlockEntity(int md)
    {
        return EnumStructure.fromId(md).getBlockEntity();
    }

    @Override
    public boolean isPowerSource()
    {
        return true;
    }

    @Override
    public boolean canConnectRedstone(IBlockAccess world, int i, int j, int k, int dir)
    {
        int meta = world.getData(i, j, k);
        if(meta == EnumStructure.BOX_RECEIVER.getId()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isBlockNormalCube(World world, int i, int j, int k)
    {
        return false;
    }

    @Override
    public boolean a(IBlockAccess world, int i, int j, int k, int side)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof TileStructure) {
            return ((TileStructure)tile).isPoweringTo(world, i, j, k, side);
        }
        return false;
    }

    @Override
    public boolean d(World world, int i, int j, int k, int oppositeSide)
    {
        return a(world, i, j, k, oppositeSide);
    }

    @Override
    public boolean colorBlock(World world, int i, int j, int k, int color)
    {
        TileEntity t = world.getTileEntity(i, j, k);
        if(t instanceof IPaintableBlock) {
            IPaintableBlock tile = (IPaintableBlock)t;
            return tile.colorBlock(world, i, j, k, color);
        }
        return false;
    }

    @Override
    public int getFireSpreadSpeed(World world, int x, int y, int z, int metadata, int face)
    {
        if(EnumStructure.fromId(metadata) == EnumStructure.WOOD_POST) {
            return 300;
        }
        return 0;
    }

    @Override
    public int getFlammability(IBlockAccess world, int x, int y, int z, int metadata, int face)
    {
        if(EnumStructure.fromId(metadata) == EnumStructure.WOOD_POST) {
            return 5;
        }
        return 0;
    }

    @Override
    public boolean isFlammable(IBlockAccess world, int x, int y, int z, int metadata, int face)
    {
        if(EnumStructure.fromId(metadata) == EnumStructure.WOOD_POST) {
            return true;
        }
        return false;
    }

    @Override
    public boolean hasTileEntity(int metadata)
    {
        return true;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void addCreativeItems(ArrayList list)
    {
        for(EnumStructure type : EnumStructure.getCreativeList()) {
            if(type.isEnabled()) {
                list.add(type.getItem());
            }
        }
        if(EnumStructure.METAL_POST.isEnabled()) {
            for(EnumColor c : EnumColor.values()) {
                list.add(new ItemStack(ItemMetalPost.getInstance(), 1, c.getId()));
            }
        }
    }
}
