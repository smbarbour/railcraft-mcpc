package railcraft.common.structures;

import railcraft.common.api.EnumSignalAspect;
import railcraft.common.api.ISignalController;
import railcraft.common.api.IBlockSignal;
import railcraft.common.api.ISignalReceiver;
import java.util.List;
import java.util.logging.Level;
import net.minecraft.server.*;
import railcraft.Railcraft;
import railcraft.common.RailcraftConfig;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.CartTools;
import railcraft.common.api.SignalTools;

public class TileStructureBlockSignal extends TileStructureSignal implements ISignalController, IBlockSignal, ISignalTile, IStructurePost
{

    private static final float SIZE = 0.15f;
    private boolean isSignalBlockPaired;
    private boolean isSignalBlockBeingPaired;
    private boolean isPairedWithReceiver;
//    private boolean isBeingPairedWithReceiver;
    private boolean prevBlinkState;
    private int signalBlockX = -1;
    private int signalBlockY = -1;
    private int signalBlockZ = -1;
    private int receiverX = -1;
    private int receiverY = -1;
    private int receiverZ = -1;
    private int railX = -1;
    private int railY = -1;
    private int railZ = -1;
    private boolean signalBlockValid;

    public TileStructureBlockSignal()
    {
        super(EnumStructure.BLOCK_SIGNAL);
    }

    protected TileStructureBlockSignal(EnumStructure s)
    {
        super(s);
    }

    @Override
    public boolean connectToBlockAt(int i, int j, int k, int side)
    {
        if(world.isBlockSolidOnSide(i, j, k, side)) {
            return true;
        }
        TileEntity tile = world.getTileEntity(i, j, k);
        return tile instanceof IStructurePost || (side == 1 && tile instanceof IStructureBox);
    }

    private void printDebug(String msg)
    {
        if(RailcraftConfig.printSignalDebug()) {
           Railcraft.log(Level.FINEST, msg);
        }
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
        update++;
        if(Railcraft.gameIsClient() && update % 5 == 0 && aspect.isBlinkAspect() && prevBlinkState != EnumSignalAspect.isBlinkOff()) {
            prevBlinkState = EnumSignalAspect.isBlinkOff();
            world.b(EnumSkyBlock.BLOCK, getX(), getY(), getZ());
            world.notify(x, y, z);
        }
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        if(update % 10 == 0) {
            EnumSignalAspect prevAspect = aspect;
            if(isSignalBlockBeingPaired()) {
                aspect = EnumSignalAspect.BLINK_YELLOW;
            } else if(isSignalBlockPaired()) {
                if(!signalBlockValid) {
                    aspect = EnumSignalAspect.BLINK_YELLOW;
                } else {
                    aspect = determineAspect();
                }
            } else {
                aspect = EnumSignalAspect.BLINK_RED;
            }
            if(prevAspect != aspect) {
                world.notify(x, y, z);
            }
        }
        if(update % 40 == 0) {
            IBlockSignal pair = getSignalBlockPair();
            if(pair != null) {
                if(!pair.isSignalBlockPaired()) {
                    clearSignalBlockPairing("Signal Block dropped because pair dropped the pairing. [" + x + ", " + y + ", " + z + "]");
                } else {
                    IBlockSignal me = pair.getSignalBlockPair();
                    if(me != null) {
                        if(me.getX() != getX() || me.getY() != getY() || me.getZ() != getZ()) {
                            clearSignalBlockPairing("Signal Block dropped because pairing was different. [" + x + ", " + y + ", " + z + "] != [" + me.getX() + ", " + me.getY() + ", " + me.getZ() + "]");
                        }
                    }
                }
            }
            ISignalReceiver receiver = getReceiver();
            if(receiver != null) {
                if(!receiver.isPairedWithController()) {
                    clearPairedReceiver();
                } else {
                    ISignalController me = receiver.getController();
                    if(me != null) {
                        if(me.getX() != getX() || me.getY() != getY() || me.getZ() != getZ()) {
                            clearPairedReceiver();
                        }
                    }
                }
            }
        }
        if(update % 2400 == 0) {
            if(getRailY() == -1 && !locateRail()) {
                clearSignalBlockPairing("Signal Block dropped because no rail was found near Signal. [" + x + ", " + y + ", " + z + "]");
            } else if(validateRail() && isSignalBlockPaired()) {
                IBlockSignal pair = getSignalBlockPair();
                if(pair != null) {
                    if(SignalTools.isSignalBlockSectionValid(world, this, pair)) {
                        signalBlockValid = true;
                    } else {
                        if(!signalBlockValid) {
                            clearSignalBlockPairing("Signal Block dropped because rail between Signals was invalid. [" + x + ", " + y + ", " + z + "]");
                        }
                        signalBlockValid = false;
                    }
                }
            }

        }
    }

    @Override
    public EnumSignalAspect getSignalAspect()
    {
        return aspect;
    }

    @Override
    public int getDimension()
    {
        if(world == null) {
            return 0;
        }
        return world.worldProvider.dimension;
    }

    @Override
    public void onBlockPlaced(World world, int i, int j, int k, int l)
    {
        locateRail();
    }

    @Override
    public void onBlockRemoval(World world, int i, int j, int k)
    {
        IBlockSignal pair = getSignalBlockPair();
        if(pair != null) {
            pair.clearSignalBlockPairing("Signal Block pairing cleared because Signal was removed. [" + i + ", " + j + ", " + k + "]");
        }
    }

    @Override
    public boolean locateRail()
    {
        if(testForRail(x, y, z)) {
            return true;
        } else if(testForRail(x - 1, y, z)) {
            return true;
        } else if(testForRail(x + 1, y, z)) {
            return true;
        } else if(testForRail(x, y, z - 1)) {
            return true;
        } else if(testForRail(x, y, z + 1)) {
            return true;
        } else if(testForRail(x - 2, y, z)) {
            return true;
        } else if(testForRail(x + 2, y, z)) {
            return true;
        } else if(testForRail(x, y, z - 2)) {
            return true;
        } else if(testForRail(x, y, z + 2)) {
            return true;
        }
        return false;
    }

    private boolean testForRail(int i, int j, int k)
    {
        for(int jj = -1; jj < 4; jj++) {
            if(BlockMinecartTrack.g(world, i, j - jj, k)) {
                railX = i;
                railY = j - jj;
                railZ = k;
//                System.out.println("Found Rail");
                return true;
            }
        }
        return false;
    }

    private boolean validateRail()
    {
        if(!BlockMinecartTrack.g(world, railX, railY, railZ)) {
            railX = -1;
            railY = -1;
            railZ = -1;
            return false;
        }
        return true;
    }

    private EnumSignalAspect determineAspect()
    {
//        System.out.println("check aspect");
        IBlockSignal pair = getSignalBlockPair();
        if(pair == null) {
//            System.out.println("pair was null");
            return EnumSignalAspect.GREEN;
        }
        if(getRailY() == -1 || pair.getRailY() == -1) {
//            System.out.println("rail not set " + getRailY());
            return EnumSignalAspect.RED;
        }
        int i1 = Math.min(getRailX(), pair.getRailX());
        int j1 = Math.min(getRailY(), pair.getRailY());
        int k1 = Math.min(getRailZ(), pair.getRailZ());
        int i2 = Math.max(getRailX(), pair.getRailX()) + 1;
        int j2 = Math.max(getRailY(), pair.getRailY()) + 1;
        int k2 = Math.max(getRailZ(), pair.getRailZ()) + 1;

        List<EntityMinecart> carts = CartTools.getMinecartsIn(world, i1, j1, k1, i2, j2, k2);
//        System.out.printf("%d, %d, %d, %d, %d, %d\n", i1, j1, k1, i2, j2, k2);
//        System.out.println("carts = " + carts.size());
        EnumSignalAspect as = EnumSignalAspect.GREEN;
        for(EntityMinecart cart : carts) {
            int cartX = MathHelper.floor(cart.locX);
            int cartZ = MathHelper.floor(cart.locZ);
            int distX = cartX - getRailX();
            int distZ = cartZ - getRailZ();
            int distSq = distX * distX + distZ * distZ;
            if(distSq < 2) {
                continue;
            } else if(Math.abs(cart.motX) < 0.08 && Math.abs(cart.motZ) < 0.08) {
                return EnumSignalAspect.RED;
            } else if(cartX == getRailX()) {
                if(cartZ > getRailZ() && cart.motZ <= 0) {
                    return EnumSignalAspect.RED;
                } else if(cartZ < getRailZ() && cart.motZ >= 0) {
                    return EnumSignalAspect.RED;
                } else {
                    as = EnumSignalAspect.YELLOW;
                }
            } else if(cartZ == getRailZ()) {
                if(cartX > getRailX() && cart.motX <= 0) {
                    return EnumSignalAspect.RED;
                } else if(cartX < getRailX() && cart.motX >= 0) {
                    return EnumSignalAspect.RED;
                } else {
                    as = EnumSignalAspect.YELLOW;
                }
            }
        }
        return as;
    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);
        isSignalBlockPaired = nbttagcompound.getBoolean("BlockPaired");
        isPairedWithReceiver = nbttagcompound.getBoolean("ReceiverPaired");
        signalBlockX = nbttagcompound.getInt("BlockPairX");
        signalBlockY = nbttagcompound.getInt("BlockPairY");
        signalBlockZ = nbttagcompound.getInt("BlockPairZ");
        receiverX = nbttagcompound.getInt("ReceiverX");
        receiverY = nbttagcompound.getInt("ReceiverY");
        receiverZ = nbttagcompound.getInt("ReceiverZ");
        railX = nbttagcompound.getInt("RailX");
        railY = nbttagcompound.getInt("RailY");
        railZ = nbttagcompound.getInt("RailZ");

        signalBlockValid = isSignalBlockPaired;
        update = GeneralTools.getRand().nextInt();
    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);
        nbttagcompound.setBoolean("BlockPaired", isSignalBlockPaired);
        nbttagcompound.setBoolean("ReceiverPaired", isPairedWithReceiver);
        nbttagcompound.setInt("BlockPairX", signalBlockX);
        nbttagcompound.setInt("BlockPairY", signalBlockY);
        nbttagcompound.setInt("BlockPairZ", signalBlockZ);
        nbttagcompound.setInt("ReceiverX", receiverX);
        nbttagcompound.setInt("ReceiverY", receiverY);
        nbttagcompound.setInt("ReceiverZ", receiverZ);
        nbttagcompound.setInt("RailX", railX);
        nbttagcompound.setInt("RailY", railY);
        nbttagcompound.setInt("RailZ", railZ);

    }

    @Override
    public boolean attemptToPairWithReceiver(ISignalReceiver receiver)
    {
        if(receiver == null) {
            return false;
        }
        receiverX = receiver.getX();
        receiverY = receiver.getY();
        receiverZ = receiver.getZ();
        if(receiver.equals(getReceiver())) {
            boolean success = true;
            if(!this.equals(receiver.getController())) {
                success = receiver.attemptToPairWithController(this);
                if(success) {
                    endReceiverPairing();
                }
            }
            isPairedWithReceiver = success;
            return success;
        }
        return false;
    }

    @Override
    public void clearPairedReceiver()
    {
        receiverX = -1;
        receiverY = -1;
        receiverZ = -1;
        isPairedWithReceiver = false;
    }

    @Override
    public int getReceiverX()
    {
        return receiverX;
    }

    @Override
    public int getReceiverY()
    {
        return receiverY;
    }

    @Override
    public int getReceiverZ()
    {
        return receiverZ;
    }

    @Override
    public boolean isPairedWithReceiver()
    {
        return isPairedWithReceiver;
    }

    @Override
    public boolean attemptToPair(IBlockSignal other)
    {
        if(other == null) {
            return false;
        }
        locateRail();
        other.locateRail();
        if(SignalTools.isSignalBlockSectionValid(world, this, other)) {
            signalBlockX = other.getX();
            signalBlockY = other.getY();
            signalBlockZ = other.getZ();
            if(other.equals(getSignalBlockPair())) {
                boolean success = true;
                if(!this.equals(other.getSignalBlockPair())) {
                    success = other.attemptToPair(this);
                }
                if(success) {
                    endSignalBlockPairing();
                }
                isSignalBlockPaired = success;
                signalBlockValid = success;
                return success;
            }
        }
        return false;
    }

//    @Override
//    public boolean equals(Object obj)
//    {
//        if(obj == null)
//        {
//            return false;
//        }
//        if(obj == this)
//        {
//            return true;
//        }
//        if(this.getClass().isInstance(obj))
//        {
//            TileStructureBlockSignal tile = (TileStructureBlockSignal)obj;
//            if(this.xCoord == tile.xCoord && this.yCoord == tile.yCoord && this.zCoord == tile.zCoord)
//            {
//                return true;
//            }
//        }
//        return false;
//    }
    @Override
    public void clearSignalBlockPairing(String reason)
    {
        printDebug(reason);
        signalBlockX = -1;
        signalBlockY = -1;
        signalBlockZ = -1;
        isSignalBlockPaired = false;
    }

    @Override
    public boolean isSignalBlockPaired()
    {
        return isSignalBlockPaired;
    }

    @Override
    public IBlockSignal getSignalBlockPair()
    {
        if(world == null || getSignalBlockPairY() < 0) {
            return null;
        }
        int i = getSignalBlockPairX();
        int j = getSignalBlockPairY();
        int k = getSignalBlockPairZ();
        if(!world.isLoaded(i, j, k)) {
            return null;
        }
        TileEntity pair = world.getTileEntity(i, j, k);
        if(pair instanceof IBlockSignal) {
            return (IBlockSignal)pair;
        } else {
            String c = "null";
            if(pair != null) {
                c = pair.getClass().getSimpleName();
            }
            clearSignalBlockPairing("Signal Block dropped because pair tile entity was not a Signal Tile Entity.  [" + i + ", " + j + ", " + k + "] = " + c);
        }
        return null;
    }

    @Override
    public ISignalReceiver getReceiver()
    {
        return SignalTools.getReceiverFor(this);
    }

    @Override
    public int getSignalBlockPairX()
    {
        return signalBlockX;
    }

    @Override
    public int getSignalBlockPairY()
    {
        return signalBlockY;
    }

    @Override
    public int getSignalBlockPairZ()
    {
        return signalBlockZ;
    }

    @Override
    public int getRailX()
    {
        return railX;
    }

    @Override
    public int getRailY()
    {
        return railY;
    }

    @Override
    public int getRailZ()
    {
        return railZ;
    }

    @Override
    public void startSignalBlockPairing()
    {
        clearSignalBlockPairing("Signal Block pairing cleared in preperation to start a new pairing.  [" + x + ", " + y + ", " + z + "]");
        isSignalBlockBeingPaired = true;
    }

    @Override
    public void endSignalBlockPairing()
    {
        isSignalBlockBeingPaired = false;
    }

    @Override
    public boolean isSignalBlockBeingPaired()
    {
        return isSignalBlockBeingPaired;
    }

    @Override
    public void startReceiverPairing()
    {
        clearPairedReceiver();
//        isBeingPairedWithReceiver = true;
    }

    @Override
    public void endReceiverPairing()
    {
//        isBeingPairedWithReceiver = false;
    }

    @Override
    public int getLightValue()
    {
        if(aspect.isBlinkAspect() && EnumSignalAspect.isBlinkOff()) {
            return 0;
        }
        return 5;
    }

    @Override
    public String getDescription()
    {
        return "Block Signal";
    }
}
