package railcraft.common.structures;

import railcraft.common.api.IBlockSignal;
import net.minecraft.server.*;
import forge.ITextureProvider;
import ic2.api.IBoxable;
import railcraft.Railcraft;
import railcraft.common.api.WorldCoordinate;
import railcraft.common.api.SignalTools;

public class ItemSignalBlockSurveyor extends Item implements ITextureProvider, IBoxable
{

    public ItemSignalBlockSurveyor(int id)
    {
        super(id);
        setMaxDurability(0);
        a(true);
        e(1);
        d(86);
        a("signalBlockSurveyor");
    }

    @Override
    public boolean onItemUseFirst(ItemStack item, EntityHuman player, World world, int i, int j, int k, int side)
    {
//        System.out.println("click");
        TileEntity t = world.getTileEntity(i, j, k);
        if(t != null && t instanceof IBlockSignal) {
            if(Railcraft.gameIsHost()) {
//            System.out.println("target found");
                IBlockSignal tile = (IBlockSignal)t;
//            System.out.println(item.getItemDamage());
                WorldCoordinate p = SignalTools.getSignalBlockPair(player, item);
                tile.locateRail();
                if(p == null && tile.getRailY() > 0) {
                    player.a("Beginning Signal Block Survey");
                    SignalTools.startSignalBlockPairing(player, item, tile);
                    player.inventory.setItem(player.inventory.itemInHandIndex, item.cloneItemStack());
//                System.out.println(item.getItemDamage());
                } else if(tile.getRailY() == -1) {
                    player.a("No rail found near " + tile.getDescription());
                } else if(i != p.x || j != p.y || k != p.z) {
//                System.out.println("attept pairing");
                    t = world.getTileEntity(p.x, p.y, p.z);
                    if(t != null && t instanceof IBlockSignal) {
                        IBlockSignal pair = (IBlockSignal)t;
                        if(pair != tile && tile.attemptToPair(pair)) {
                            pair.endSignalBlockPairing();
                            tile.endSignalBlockPairing();
                            player.a("Successfully defined Signal Block");
                            SignalTools.endSignalBlockPairing(player, item);
                        } else {
                            player.a("Signal Block is not valid");
                        }
                    } else if(world.isLoaded(p.x, p.y, p.z)) {
                        player.a("First Signal no longer exists, abandoning Survey");
                        tile.endSignalBlockPairing();
                        SignalTools.endSignalBlockPairing(player, item);
                    } else {
                        player.a("First Signal is not in a loaded Chunk");
                    }
                } else {
                    player.a("Signal Block Survey abandoned");
                    tile.endSignalBlockPairing();
                    SignalTools.endSignalBlockPairing(player, item);
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public String getTextureFile()
    {
        return "/railcraft/textures/railcraft.png";
    }

    @Override
    public boolean canBeStoredInToolbox(ItemStack itemstack)
    {
        return true;
    }
}
