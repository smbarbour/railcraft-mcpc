package railcraft.common.structures;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import ic2.api.IPaintableBlock;
import ic2.api.IWrenchable;
import railcraft.common.SafeNBTWrapper;

public class TileStructureMetalPost extends TileStructurePost implements IPaintableBlock, IWrenchable
{

    private byte color = 0;

    public TileStructureMetalPost()
    {
        super(EnumStructure.METAL_POST);
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);

        data.setByte("Color", color);
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);
        SafeNBTWrapper safe = new SafeNBTWrapper(data);

        color = safe.getByte("Color");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeByte(color);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        color = data.readByte();

        world.notify(x, y, z);
    }

    @Override
    public boolean colorBlock(World world, int i, int j, int k, int c)
    {
        if(color != c + 1) {
            color = (byte)(c + 1);
            return true;
        }
        return false;
    }

    public void setColor(int c)
    {
        color = (byte)c;
    }

    @Override
    public boolean wrenchCanSetFacing(EntityHuman entityPlayer, int side)
    {
        return false;
    }

    @Override
    public void setFacing(short facing)
    {
    }

    @Override
    public short getFacing()
    {
        return 0;
    }

    @Override
    public boolean wrenchCanRemove(EntityHuman entityPlayer)
    {
        return true;
    }

    @Override
    public float getWrenchDropRate()
    {
        return 0;
    }

    public byte getColor()
    {
        return color;
    }
}
