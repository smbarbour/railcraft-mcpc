package railcraft.common.structures;

import railcraft.common.api.EnumSignalAspect;
import railcraft.common.api.ISignalReceiver;
import railcraft.common.api.ISignalController;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import railcraft.Railcraft;
import railcraft.GuiHandler;
import railcraft.common.EnumGui;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.SignalTools;

public class TileStructureControllerBox extends TileStructureBox implements ISignalController, IStructureBox
{

    private boolean isPairedWithReceiver;
    private boolean isBeingPairedWithReceiver;
    private boolean powered;
    private int receiverX = -1;
    private int receiverY = -1;
    private int receiverZ = -1;
    protected int update;
    public EnumSignalAspect defaultAspect = EnumSignalAspect.GREEN;
    public EnumSignalAspect poweredAspect = EnumSignalAspect.RED;
    private boolean prevBlinkState;

    public TileStructureControllerBox()
    {
        super(EnumStructure.BOX_CONTROLLER);
    }

    @Override
    public boolean blockActivated(World world, int i, int j, int k, EntityHuman player)
    {
        GuiHandler.openGui(EnumGui.BOX_CONTROLLER, player, world, x, y, z);
        return true;
    }

    @Override
    public void q_()
    {
        update++;
        if(update % 4 == 0) {
            if(Railcraft.gameIsHost()) {
                EnumSignalAspect prevAspect = aspect;
                if(isPairedWithReceiver()) {
                    aspect = determineAspect();
                } else if(isBeingPairedWithReceiver) {
                    aspect = EnumSignalAspect.BLINK_YELLOW;
                } else {
                    aspect = EnumSignalAspect.BLINK_RED;
                }
                if(prevAspect != aspect) {
                    world.notify(x, y, z);
                }
            }

            if(Railcraft.gameIsClient() && aspect.isBlinkAspect() && prevBlinkState != EnumSignalAspect.isBlinkOff()) {
                prevBlinkState = EnumSignalAspect.isBlinkOff();
                world.notify(x, y, z);
            }
        }
        if(Railcraft.gameIsHost() && update % 40 == 0) {
            ISignalReceiver receiver = getReceiver();
            if(receiver != null && !(receiver.isPairedWithController() && receiver.getController() == this)) {
                clearPairedReceiver();
            }
        }
    }

    @Override
    public void onNeighborBlockChange(World world, int i, int j, int k, int blockId)
    {
        if(Railcraft.gameIsNotHost()) {
            return;
        }
//        boolean p = worldObj.isBlockGettingPowered(i, j, k) || worldObj.isBlockIndirectlyGettingPowered(i, j, k);
        boolean p = world.isBlockPowered(i, j, k) || world.isBlockPowered(i, j - 1, k);
        if(p ^ powered) {
            powered = p;
            world.notify(i, j, k);
        }
    }

    private EnumSignalAspect determineAspect()
    {
        EnumSignalAspect newAspect = powered ? poweredAspect : defaultAspect;
        for(int side = 2; side < 6; side++) {
            TileEntity t = GeneralTools.getBlockTileEntityFromSide(world, getX(), getY(), getZ(), side);
            if(t instanceof IStructureBox && t instanceof ISignalReceiver) {
                IStructureBox tile = (IStructureBox)t;
                newAspect = EnumSignalAspect.mostRestrictive(newAspect, tile.getBoxSignalAspect());
            }
        }
        return newAspect;
    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);
        powered = nbttagcompound.getBoolean("Powered");

        defaultAspect = EnumSignalAspect.fromId(nbttagcompound.getInt("defaultAspect"));
        poweredAspect = EnumSignalAspect.fromId(nbttagcompound.getInt("PoweredAspect"));

        isPairedWithReceiver = nbttagcompound.getBoolean("ReceiverPaired");
        receiverX = nbttagcompound.getInt("ReceiverX");
        receiverY = nbttagcompound.getInt("ReceiverY");
        receiverZ = nbttagcompound.getInt("ReceiverZ");

        update = GeneralTools.getRand().nextInt();
    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);
        nbttagcompound.setBoolean("Powered", powered);

        nbttagcompound.setInt("defaultAspect", defaultAspect.getId());
        nbttagcompound.setInt("PoweredAspect", poweredAspect.getId());

        nbttagcompound.setBoolean("ReceiverPaired", isPairedWithReceiver());
        nbttagcompound.setInt("ReceiverX", getReceiverX());
        nbttagcompound.setInt("ReceiverY", getReceiverY());
        nbttagcompound.setInt("ReceiverZ", getReceiverZ());
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeByte(defaultAspect.getId());
        data.writeByte(poweredAspect.getId());
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        defaultAspect = EnumSignalAspect.fromId(data.readByte());
        poweredAspect = EnumSignalAspect.fromId(data.readByte());
    }

    @Override
    public boolean attemptToPairWithReceiver(ISignalReceiver receiver)
    {
        if(receiver == null) {
            return false;
        }
        receiverX = receiver.getX();
        receiverY = receiver.getY();
        receiverZ = receiver.getZ();
        if(receiver.equals(getReceiver())) {
            boolean success = true;
            if(!this.equals(receiver.getController())) {
                success = receiver.attemptToPairWithController(this);
            }
            isPairedWithReceiver = success;
            return success;
        }
        return false;
    }

    @Override
    public void clearPairedReceiver()
    {
        receiverX = -1;
        receiverY = -1;
        receiverZ = -1;
        isPairedWithReceiver = false;
    }

    @Override
    public int getReceiverX()
    {
        return receiverX;
    }

    @Override
    public int getReceiverY()
    {
        return receiverY;
    }

    @Override
    public int getReceiverZ()
    {
        return receiverZ;
    }

    @Override
    public boolean isPairedWithReceiver()
    {
        return isPairedWithReceiver;
    }

    @Override
    public ISignalReceiver getReceiver()
    {
        return SignalTools.getReceiverFor(this);
    }

    @Override
    public int getDimension()
    {
        if(world == null) {
            return 0;
        }
        return world.worldProvider.dimension;
    }

    @Override
    public String getDescription()
    {
        return "Signal Controller Box";
    }

    @Override
    public boolean connectToBoxAt(int i, int j, int k, int side)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof IStructureBox && tile instanceof ISignalReceiver) {
            return true;
        }
        return false;
    }

    @Override
    public EnumSignalAspect getSignalAspect()
    {
        return getBoxSignalAspect();
    }

    @Override
    public void startReceiverPairing()
    {
        clearPairedReceiver();
        isBeingPairedWithReceiver = true;
    }

    @Override
    public void endReceiverPairing()
    {
        isBeingPairedWithReceiver = false;
    }
}
