package railcraft.common.structures;

import railcraft.common.api.EnumSignalAspect;
import railcraft.common.api.ISignalReceiver;
import railcraft.common.api.ISignalController;
import net.minecraft.server.*;
import railcraft.Railcraft;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.SignalTools;

public class TileStructureDistantSignal extends TileStructureSignal implements ISignalReceiver
{

    private static final float SIZE = 0.15f;
    private boolean isPairedWithController;
    private int controllerX = -1;
    private int controllerY = -1;
    private int controllerZ = -1;
    private boolean prevBlinkState;

    public TileStructureDistantSignal()
    {
        super(EnumStructure.DISTANT_SIGNAL);
    }

    @Override
    public void q_()
    {
        update++;
        if(Railcraft.gameIsClient() && update % 5 == 0 && aspect.isBlinkAspect() && prevBlinkState != EnumSignalAspect.isBlinkOff()) {
            prevBlinkState = EnumSignalAspect.isBlinkOff();
            world.b(EnumSkyBlock.BLOCK, getX(), getY(), getZ());
            world.notify(x, y, z);
        }
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        if(update % 10 == 0) {
            EnumSignalAspect prevAspect = aspect;
            if(isPairedWithController()) {
                ISignalController controller = getController();
                if(controller != null) {
                    aspect = controller.getSignalAspect();
                } else {
                    aspect = EnumSignalAspect.BLINK_YELLOW;
                }
            } else {
                aspect = EnumSignalAspect.BLINK_RED;
            }
            if(prevAspect != aspect) {
                world.notify(x, y, z);
            }
        }
        if(update % 40 == 0) {
            ISignalController controller = getController();
            if(controller != null) {
                if(!controller.isPairedWithReceiver()) {
                    clearPairedController();
                } else {
                    ISignalReceiver me = controller.getReceiver();
                    if(me != null) {
                        if(me.getX() != getX() || me.getY() != getY() || me.getZ() != getZ()) {
                            clearPairedController();
                        }
                    }
                }
            }
        }
    }

    @Override
    public void setBlockBoundsBasedOnState(IBlockAccess world, int i, int j, int k)
    {
        getBlock().a(0.15f, 0f, 0.15f, 0.85f, 1f, 0.85f);
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return AxisAlignedBB.b(i + 0.15f, j, k + 0.15f, i + 0.85f, j + 1f, k + 0.85f);
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return AxisAlignedBB.b(i + SIZE, j, k + SIZE, i + 1 - SIZE, j + 1, k + 1 - SIZE);
    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);

        nbttagcompound.setBoolean("ControllerPaired", isPairedWithController());
        nbttagcompound.setInt("ControllerX", getControllerX());
        nbttagcompound.setInt("ControllerY", getControllerY());
        nbttagcompound.setInt("ControllerZ", getControllerZ());
    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);

        isPairedWithController = nbttagcompound.getBoolean("ControllerPaired");
        controllerX = nbttagcompound.getInt("ControllerX");
        controllerY = nbttagcompound.getInt("ControllerY");
        controllerZ = nbttagcompound.getInt("ControllerZ");

        update = GeneralTools.getRand().nextInt();
    }

    @Override
    public boolean attemptToPairWithController(ISignalController controller)
    {
        if(controller == null) {
            return false;
        }
        controllerX = controller.getX();
        controllerY = controller.getY();
        controllerZ = controller.getZ();
        if(controller.equals(getController())) {
            boolean success = true;
            if(!this.equals(controller.getReceiver())) {
                success = controller.attemptToPairWithReceiver(this);
            }
            isPairedWithController = success;
            return success;
        }
        return false;
    }

    @Override
    public void clearPairedController()
    {
        controllerX = -1;
        controllerY = -1;
        controllerZ = -1;
        isPairedWithController = false;
    }

    @Override
    public int getControllerX()
    {
        return controllerX;
    }

    @Override
    public int getControllerY()
    {
        return controllerY;
    }

    @Override
    public int getControllerZ()
    {
        return controllerZ;
    }

    @Override
    public boolean isPairedWithController()
    {
        return isPairedWithController;
    }

    @Override
    public ISignalController getController()
    {
        return SignalTools.getControllerFor(this);
    }

    @Override
    public int getLightValue()
    {
        if(aspect.isBlinkAspect() && EnumSignalAspect.isBlinkOff()) {
            return 0;
        }
        return 5;
    }

    @Override
    public String getDescription()
    {
        return "Distant Signal";
    }

    @Override
    public boolean doesActionOnAspect(EnumSignalAspect aspect)
    {
        return true;
    }

    @Override
    public void doActionOnAspect(EnumSignalAspect aspect, boolean trigger)
    {
    }

    @Override
    public int getDimension()
    {
        if(world == null) {
            return 0;
        }
        return world.worldProvider.dimension;
    }
}
