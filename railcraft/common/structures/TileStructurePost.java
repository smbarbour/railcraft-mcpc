package railcraft.common.structures;

import net.minecraft.server.*;
import railcraft.common.RailcraftBlocks;
import railcraft.common.api.IPostConnection;
import railcraft.common.cube.EnumCube;

public abstract class TileStructurePost extends TileStructure implements IStructurePost
{

    private static final float SIZE = 0.15f;
    private static final float SELECT = 0.1f;

    protected TileStructurePost(EnumStructure s)
    {
        super(s);
    }

    @Override
    public void setBlockBoundsBasedOnState(IBlockAccess world, int i, int j, int k)
    {
        getBlock().a(0.2F, 0.0F, 0.2F, 0.8F, 1.0F, 0.8F);
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
    {
        if(!world.isEmpty(i, j - 1, k)
            && !(world.getTileEntity(i, j - 1, k) instanceof TileStructurePost)
            && !BlockMinecartTrack.g(world, i, j + 1, k)) {
            return AxisAlignedBB.b(i + SIZE, j, k + SIZE, i + 1 - SIZE, j + 1.5, k + 1 - SIZE);
        }
        return AxisAlignedBB.b(i + SIZE, j, k + SIZE, i + 1 - SIZE, j + 1, k + 1 - SIZE);
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return AxisAlignedBB.b(i + SELECT, j, k + SELECT, i + 1 - SELECT, j + 1.0F, k + 1 - SELECT);
    }

    @Override
    public boolean isBlockSolidOnSide(World world, int i, int j, int k, int side)
    {
        if(side == 1 || side == 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean connectToBlockAt(int i, int j, int k, int side)
    {
        int id = world.getTypeId(i, j, k);
        if(id == Block.FENCE_GATE.id) {
            return true;
        }

        if(id == Block.GLOWSTONE.id) {
            return true;
        }

        if(id == Block.REDSTONE_LAMP_ON.id || id == Block.REDSTONE_LAMP_OFF.id) {
            return true;
        }

        Block cube = RailcraftBlocks.getBlockCube();
        if(cube != null && cube.id == id) {
            int meta = world.getData(i, j, k);
            if(meta == EnumCube.CONCRETE_BLOCK.getId()) {
                return true;
            }
        }

        if(Block.byId[id] instanceof IPostConnection) {
            ((IPostConnection)Block.byId[id]).connectsAt(world, i, j, k, side);
        }

        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof IStructurePost) {
            return true;
        }

        return false;
    }
}
