package railcraft.common.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftConfig;
import railcraft.common.modules.RailcraftModuleManager;
import railcraft.common.modules.RailcraftModuleManager.Module;

public enum EnumStructure
{

    // Name (texture, hardness, light, needsSupport, connectToPost, isPost, tile)
    METAL_POST(Module.STRUCTURES, 169, 240, 3, false, true, true, "Metal Post", "structure.post.metal", TileStructureMetalPost.class),
    DUAL_HEAD_BLOCK_SIGNAL(Module.SIGNALS, 56, 56, 8, false, true, false, "Duel-Head Block Signal", "structure.signal.block.dual", TileStructureDualHeadBlockSignal.class),
    SWITCH_MOTOR(Module.SIGNALS, 42, 42, 8, true, false, false, "Switch Motor", "structure.switch.motor", TileStructureSwitchMotor.class),
    BLOCK_SIGNAL(Module.SIGNALS, 55, 55, 8, false, true, false, "Block Signal", "structure.signal.block", TileStructureBlockSignal.class),
    SWITCH_LEVER(Module.SIGNALS, 42, 42, 8, true, false, false, "Switch Lever", "structure.switch.lever", TileStructureSwitchLever.class),
    WOOD_POST(Module.STRUCTURES, 87, 87, 3, false, true, true, "Wooden Post", "structure.post.wood", TileStructureWoodPost.class),
    STONE_POST(Module.STRUCTURES, 103, 103, 3, false, true, true, "Stone Post", "structure.post.stone", TileStructureStonePost.class),
    CONCRETE_BLOCK(Module.STRUCTURES, 103, 103, 3, false, true, false, "Concrete Block (Obsolete)", "structure.block.concrete", null),
    BOX_RECEIVER(Module.SIGNALS, 88, 120, 3, true, false, false, "Signal Receiver Box", "structure.box.receiver", TileStructureReceiverBox.class),
    BOX_CONTROLLER(Module.SIGNALS, 88, 119, 3, true, false, false, "Signal Controller Box", "structure.box.controller", TileStructureControllerBox.class),
    DISTANT_SIGNAL(Module.SIGNALS, 55, 55, 8, false, true, false, "Distant Signal", "structure.signal.distant", TileStructureDistantSignal.class),
    CART_BALANCER(Module.EXTRAS, 202, 224, 8, false, false, false, "Cart Balancer", "structure.balancer", TileCartBalancer.class);
    private final Module module;
    private final int id;
    private final int primaryTexture;
    private final int secondaryTexture;
    private final float hardness;
    private final boolean needsSupport;
    private final boolean connectToPost;
    private final boolean isPost;
    private final String name;
    private final String tag;
    private int altTextureOffset = 0;
    private final Class<? extends TileStructure> tile;
    private Item item;
    private static int nextId = 0;
    private boolean useAltTexture;
    private int[] textures = new int[6];
    private static Map<Integer, EnumStructure> structures = new HashMap<Integer, EnumStructure>();
    private static final List<EnumStructure> creativeList = new ArrayList<EnumStructure>();

    static {
        creativeList.add(SWITCH_LEVER);
        creativeList.add(SWITCH_MOTOR);
        creativeList.add(BLOCK_SIGNAL);
        creativeList.add(DISTANT_SIGNAL);
        creativeList.add(DUAL_HEAD_BLOCK_SIGNAL);
        creativeList.add(BOX_CONTROLLER);
        creativeList.add(BOX_RECEIVER);
        creativeList.add(WOOD_POST);
        creativeList.add(STONE_POST);
    }

    EnumStructure(Module module, int primaryTexture, int secondaryTexture, float hardness, boolean needsSupport, boolean connectToPost, boolean isPost, String name, String tag, Class<? extends TileStructure> tile)
    {
        this.module = module;
        this.primaryTexture = primaryTexture;
        this.secondaryTexture = secondaryTexture;
        this.hardness = hardness;
        this.needsSupport = needsSupport;
        this.connectToPost = connectToPost;
        this.isPost = isPost;
        this.tile = tile;
        this.name = name;
        this.tag = tag;
        setUseAltTextures(false);
        id = getNextId();
    }

//    public void createOldItem()
//    {
//        if(item != null) {
//            return;
//        }
//        int itemId = RailcraftConfig.getItemId(getTag());
//        if(itemId > 0) {
//            item = new ItemStructureOld(itemId, this);
//            Railcraft.registerStructureRenderer(this);
//            RailcraftLanguage.getInstance().registerItemName(item, RailcraftLanguage.getTranslatedString(getTag()) + " (Obsolete)");
//
//        }
//    }

    public Item setItem(Item item)
    {
        this.item = item;
        return item;
    }

    public Item getOldItem()
    {
        return item;
    }

    public ItemStack getItem()
    {
        return getItem(1);
    }

    public ItemStack getItem(int qty)
    {
        if(this == METAL_POST) {
            return new ItemStack(ItemMetalPost.getInstance(), qty, 0);
        }
        return new ItemStack(RailcraftBlocks.getBlockStructure().id, qty, id);
    }

    public String getTag()
    {
        return tag;
    }

    public String getName()
    {
        return name;
    }

    public Module getModule()
    {
        return module;
    }

    public Class<? extends TileStructure> getTileClass()
    {
        return tile;
    }

    public TileStructure getBlockEntity()
    {
        if(tile == null) {
            return null;
        }
        try {
            return tile.newInstance();
        } catch (Exception ex) {
        }
        return null;
    }

    public float getHardness()
    {
        return hardness;
    }

    public int getId()
    {
        return id;
    }

    public int getBlockTexture()
    {
        if(useAltTexture) {
            return textures[0] + altTextureOffset;
        }
        return primaryTexture;
    }

    public int getBlockTextureFromSide(int side)
    {
        if(useAltTexture) {
            return textures[side] + altTextureOffset;
        }
        return textures[side];
    }

    public int getOffsetTexture(int offset)
    {
        return primaryTexture + offset;
    }

    public int getPrimaryTexture()
    {
        return primaryTexture;
    }

    public int getSecondaryTexture()
    {
        return secondaryTexture;
    }

    public static EnumStructure fromId(int id)
    {
        EnumStructure s = structures.get(id);
        if(s != null) {
            return s;
        }
        for(EnumStructure t : EnumStructure.values()) {
            if(t.getId() == id) {
                structures.put(id, t);
                return t;
            }
        }
        return METAL_POST;
    }

    private static int getNextId()
    {
        int i = nextId;
        nextId++;
        return i;
    }

    public static List<EnumStructure> getCreativeList()
    {
        return creativeList;
    }

    public boolean useAltTexture()
    {
        return useAltTexture;
    }

    public void setUseAltTextures(boolean use)
    {
        for(int i = 0; i < textures.length; i++) {
            textures[i] = primaryTexture;
        }
        altTextureOffset = 0;
        useAltTexture = use;
    }

    public void setUseAltTextureOnSide(int side, int texture)
    {
        textures[side] = texture;
    }

    public void setUseOffsetTextureOnSide(int side, int offset)
    {
        textures[side] = primaryTexture + offset;
    }

    public void setAltTextureIndex(int index)
    {
        for(int i = 0; i < textures.length; i++) {
            textures[i] = index;
        }
    }

    public void setAltTextureOffset(int altTextureOffset)
    {
        this.altTextureOffset = altTextureOffset;
    }

    public boolean connectToPost()
    {
        return connectToPost;
    }

    public boolean isPost()
    {
        return isPost;
    }

    public boolean needsSupport()
    {
        return needsSupport;
    }

    public boolean isEnabled()
    {
        return RailcraftModuleManager.isModuleLoaded(getModule()) && RailcraftBlocks.getBlockStructure() != null && RailcraftConfig.isSubBlockEnabled(getTag());
    }
}
