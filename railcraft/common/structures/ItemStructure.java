package railcraft.common.structures;

import net.minecraft.server.*;
import forge.*;
import railcraft.common.RailcraftBlocks;
import railcraft.common.api.BukkitHandlers;

public class ItemStructure extends ItemBlock implements ITextureProvider
{

    public ItemStructure(int id)
    {
        super(id);
        setMaxDurability(0);
        a(true);
        a("railcraftStructure");
    }

    public EnumStructure getStructureType(ItemStack stack)
    {
        return EnumStructure.fromId(stack.getData());
    }

    public int getIconFromDamage(int damage)
    {
        return RailcraftBlocks.getBlockStructure().a(2, damage);
    }

    @Override
    public int filterData(int damage)
    {
        return damage;
    }

    @Override
    public String a(ItemStack stack)
    {
        return getStructureType(stack).getTag();
    }

    public String getTextureFile()
    {
        return "/railcraft/textures/railcraft.png";
    }

    @Override
    public boolean interactWith(ItemStack stack, EntityHuman entityplayer, World world, int i, int j, int k, int side)
    {
        int id = world.getTypeId(i, j, k);
        if(id == Block.SNOW.id) {
            side = 0;
        } else if(id != Block.VINE.id) {
            if(side == 0) {
                j--;
            }
            if(side == 1) {
                j++;
            }
            if(side == 2) {
                k--;
            }
            if(side == 3) {
                k++;
            }
            if(side == 4) {
                i--;
            }
            if(side == 5) {
                i++;
            }
        }
        if(stack.count == 0) {
            return false;
        }
        if(j == 128 - 1 && RailcraftBlocks.getBlockStructure().material.isSolid()) {
            return false;
        }
        EnumStructure type = getStructureType(stack);
        if(world.mayPlace(RailcraftBlocks.getBlockStructure().id, i, j, k, false, side) && (!type.needsSupport() || world.isBlockSolidOnSide(i, j - 1, k, 1))) {
            Block structure = RailcraftBlocks.getBlockStructure();
            ItemStack tempStack = new ItemStack(structure, 1, type.getId());
            boolean placed = BukkitHandlers.placeBlockWithEvent(world.getServer(), world, tempStack, i, j, k, i, j, k, true, entityplayer); //world.setTypeIdAndData(i, j, k, structure.id, type.getId());
            if(placed) {
                if(world.getTypeId(i, j, k) == structure.id) {
                    structure.postPlace(world, i, j, k, side);
                    structure.postPlace(world, i, j, k, entityplayer);
                }
                world.makeSound((float)i + 0.5F, (float)j + 0.5F, (float)k + 0.5F, structure.stepSound.getName(), (structure.stepSound.getVolume1() + 1.0F) / 2.0F, structure.stepSound.getVolume2() * 0.8F);
                stack.count--;
            }
            return true;
        } else {
            return false;
        }
    }
}
