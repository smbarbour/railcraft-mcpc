package railcraft.common.structures;

import java.util.List;

import org.bukkit.craftbukkit.entity.CraftHumanEntity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.InventoryHolder;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;

public class BalancerChannel implements IInventory
{

    private static final int INV_SIZE = 21;
    private ItemStack[] contents;
    private int channel;

    public BalancerChannel(int channel)
    {
        contents = new ItemStack[INV_SIZE];
        this.channel = channel;
    }

    @Override
    public int getSize()
    {
        return getChannelSize();
    }

    public static int getChannelSize()
    {
        return INV_SIZE;
    }

    @Override
    public ItemStack getItem(int slot)
    {
        if(slot < 0 || slot >= contents.length) {
            return null;
        }
        return contents[slot];
    }

    @Override
    public ItemStack splitStack(int slot, int amount)
    {
        if(slot < 0 || slot >= contents.length) {
            return null;
        }
        if(contents[slot] != null) {
            if(contents[slot].count <= amount) {
                ItemStack itemstack = contents[slot];
                contents[slot] = null;
                BalancerNetworkManager.markHasChanged();
                return itemstack;
            }
            ItemStack itemstack1 = contents[slot].a(amount);
            if(contents[slot].count == 0) {
                contents[slot] = null;
            }
            BalancerNetworkManager.markHasChanged();
            return itemstack1;
        } else {
            return null;
        }
    }

    @Override
    public void setItem(int slot, ItemStack stack)
    {
        if(slot < 0 || slot >= contents.length) {
            return;
        }
        contents[slot] = stack;
        BalancerNetworkManager.markHasChanged();
    }

    @Override
    public String getName()
    {
        return new StringBuilder("Channel").append(channel).toString();
    }

    @Override
    public int getMaxStackSize()
    {
        return 64;
    }

    @Override
    public void update()
    {
    }

    @Override
    public boolean a(EntityHuman entityplayer)
    {
        return true;
    }

    @Override
    public void f()
    {
    }

    @Override
    public void g()
    {
    }

    @Override
    public ItemStack splitWithoutUpdate(int var1)
    {
        return null;
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public InventoryHolder getOwner() {
		return null;
	}

	@Override
	public List<HumanEntity> getViewers() {
		return null;
	}

	@Override
	public void onClose(CraftHumanEntity arg0) {
	}

	@Override
	public void onOpen(CraftHumanEntity arg0) {
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
