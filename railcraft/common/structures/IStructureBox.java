package railcraft.common.structures;

import railcraft.common.api.EnumSignalAspect;

public interface IStructureBox
{

    public EnumSignalAspect getBoxSignalAspect();

    public EnumStructure getStructureType();

    public boolean connectToBoxAt(int i, int j, int k, int side);
}
