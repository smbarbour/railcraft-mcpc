package railcraft.common.structures;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.EntityLiving;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IBlockAccess;
import net.minecraft.server.World;
import railcraft.common.RailcraftBlocks;
import railcraft.common.RailcraftTileEntity;

public abstract class TileStructure extends RailcraftTileEntity
{

    private EnumStructure structureType;

    protected TileStructure(EnumStructure s)
    {
        structureType = s;
    }

    public final EnumStructure getStructureType()
    {
        return structureType;
    }

    public boolean blockActivated(World world, int i, int j, int k, EntityHuman player)
    {
        return false;
    }

    public void onBlockPlaced(World world, int i, int j, int k, int l)
    {
    }

    public void onBlockPlacedBy(World world, int i, int j, int k, EntityLiving entityliving)
    {
    }

    public void onNeighborBlockChange(World world, int i, int j, int k, int blockId)
    {
    }

    public void onBlockRemoval(World world, int i, int j, int k)
    {
    }

    public void setBlockBoundsBasedOnState(IBlockAccess world, int i, int j, int k)
    {
        RailcraftBlocks.getBlockStructure().a(0, 0, 0, 1, 1, 1);
    }

    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return AxisAlignedBB.b(i, j, k, i + 1, j + 1, k + 1);
    }

    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return AxisAlignedBB.b(i, j, k, i + 1, j + 1, k + 1);
    }

    public boolean isBlockSolidOnSide(World world, int i, int j, int k, int side)
    {
        return false;
    }

    public boolean isPoweringTo(IBlockAccess world, int i, int j, int k, int side)
    {
        return false;
    }

    public Block getBlock()
    {
        return RailcraftBlocks.getBlockStructure();
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public int getZ()
    {
        return z;
    }

    @Override
    public int getId()
    {
        return (byte)getStructureType().getId();
    }
}
