package railcraft.common.structures;

import railcraft.common.api.EnumSignalAspect;
import railcraft.common.api.ISignalController;
import railcraft.common.api.ISignalReceiver;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;
import railcraft.Railcraft;
import railcraft.common.api.SignalTools;

public class TileStructureDualHeadBlockSignal extends TileStructureBlockSignal implements ISignalReceiver
{

    private static final float SIZE = 0.15f;
    private boolean isPairedWithController;
    private int controllerX = -1;
    private int controllerY = -1;
    private int controllerZ = -1;
    private EnumSignalAspect distantAspect = EnumSignalAspect.BLINK_RED;
    private boolean prevBlinkState;

    public TileStructureDualHeadBlockSignal()
    {
        super(EnumStructure.DUAL_HEAD_BLOCK_SIGNAL);
    }

    public EnumSignalAspect getDistantSignalAspect()
    {
        return distantAspect;
    }

    @Override
    public void q_()
    {
        super.q_();
        if(Railcraft.gameIsClient() && update % 5 == 0 && distantAspect.isBlinkAspect() && prevBlinkState != EnumSignalAspect.isBlinkOff()) {
            prevBlinkState = EnumSignalAspect.isBlinkOff();
            world.b(EnumSkyBlock.BLOCK, getX(), getY(), getZ());
            world.notify(x, y, z);
        }
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        if(update % 10 == 0) {
            EnumSignalAspect prevAspect = distantAspect;
            if(isPairedWithController()) {
                ISignalController controller = getController();
                if(controller != null) {
                    distantAspect = controller.getSignalAspect();
                } else {
                    distantAspect = EnumSignalAspect.BLINK_YELLOW;
                }
            } else {
                distantAspect = EnumSignalAspect.BLINK_RED;
            }
            if(prevAspect != distantAspect) {
                world.notify(x, y, z);
            }
        }
        if(update % 40 == 0) {
            ISignalController controller = getController();
            if(controller != null) {
                if(!controller.isPairedWithReceiver()) {
                    clearPairedController();
                } else {
                    ISignalReceiver me = controller.getReceiver();
                    if(me != null) {
                        if(me.getX() != getX() || me.getY() != getY() || me.getZ() != getZ()) {
                            clearPairedController();
                        }
                    }
                }
            }
        }
    }

    @Override
    public void setBlockBoundsBasedOnState(IBlockAccess world, int i, int j, int k)
    {
        getBlock().a(0.15f, 0f, 0.15f, 0.85f, 1f, 0.85f);
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return AxisAlignedBB.b(i + 0.15f, j, k + 0.15f, i + 0.85f, j + 1f, k + 0.85f);
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return AxisAlignedBB.b(i + SIZE, j, k + SIZE, i + 1 - SIZE, j + 1, k + 1 - SIZE);
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);

        data.setBoolean("ControllerPaired", isPairedWithController());
        data.setInt("ControllerX", getControllerX());
        data.setInt("ControllerY", getControllerY());
        data.setInt("ControllerZ", getControllerZ());
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);

        isPairedWithController = data.getBoolean("ControllerPaired");
        controllerX = data.getInt("ControllerX");
        controllerY = data.getInt("ControllerY");
        controllerZ = data.getInt("ControllerZ");
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeByte(distantAspect.getId());
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        distantAspect = EnumSignalAspect.fromId(data.readByte());
    }

    @Override
    public boolean attemptToPairWithController(ISignalController controller)
    {
        if(controller == null) {
            return false;
        }
        controllerX = controller.getX();
        controllerY = controller.getY();
        controllerZ = controller.getZ();
        if(controller.equals(getController())) {
            boolean success = true;
            if(!this.equals(controller.getReceiver())) {
                success = controller.attemptToPairWithReceiver(this);
            }
            isPairedWithController = success;
            return success;
        }
        return false;
    }

    @Override
    public void clearPairedController()
    {
        controllerX = -1;
        controllerY = -1;
        controllerZ = -1;
        isPairedWithController = false;
    }

    @Override
    public int getControllerX()
    {
        return controllerX;
    }

    @Override
    public int getControllerY()
    {
        return controllerY;
    }

    @Override
    public int getControllerZ()
    {
        return controllerZ;
    }

    @Override
    public boolean isPairedWithController()
    {
        return isPairedWithController;
    }

    @Override
    public ISignalController getController()
    {
        return SignalTools.getControllerFor(this);
    }

    @Override
    public int getLightValue()
    {
        if(aspect.isBlinkAspect() && distantAspect.isBlinkAspect() && EnumSignalAspect.isBlinkOff()) {
            return 0;
        }
        return 5;
    }

    @Override
    public String getDescription()
    {
        return "Dual-Head Block Signal";
    }

    @Override
    public boolean doesActionOnAspect(EnumSignalAspect aspect)
    {
        return true;
    }

    @Override
    public void doActionOnAspect(EnumSignalAspect aspect, boolean trigger)
    {
    }
}
