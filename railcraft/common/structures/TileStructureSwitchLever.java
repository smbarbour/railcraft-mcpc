package railcraft.common.structures;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BlockMinecartTrack;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IBlockAccess;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import railcraft.Railcraft;
import railcraft.common.api.EnumDirection;
import railcraft.common.RailcraftConfig;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.tracks.ITrackInstance;
import railcraft.common.api.tracks.ITrackSwitch;
import railcraft.common.tracks.TileTrack;

public class TileStructureSwitchLever extends TileStructure
{

    private byte facing = EnumDirection.NORTH.getValue();
    private boolean powered;
    private boolean reversed;
    protected int update;

    public TileStructureSwitchLever()
    {
        super(EnumStructure.SWITCH_LEVER);
    }

    protected TileStructureSwitchLever(EnumStructure s)
    {
        super(s);
    }

    @Override
    public void setBlockBoundsBasedOnState(IBlockAccess world, int i, int j, int k)
    {
        getBlock().a(0.2f, 0f, 0.2f, 0.8f, 0.8f, 0.8f);
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return AxisAlignedBB.b(i + 0.2f, j, k + 0.2f, i + 0.8f, j + 0.4F, k + 0.8f);
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return AxisAlignedBB.b(i + 0.2f, j, k + 0.2f, i + 0.8f, j + 0.8f, k + 0.8f);
    }

    @Override
    public boolean blockActivated(World world, int i, int j, int k, EntityHuman player)
    {
        findRail();
        powered = !powered;
        switchRail(powered);
        return true;
    }

    @Override
    public void onBlockPlaced(World world, int i, int j, int k, int l)
    {
        findRail();
    }

    @Override
    public void q_()
    {
        update++;
        if(update % 20 == 0) {
            if(facing < 2 || facing > 5) {
                findRail();
            }
        }
    }

    @Override
    public void onNeighborBlockChange(World world, int i, int j, int k, int blockId)
    {
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        if(BlockMinecartTrack.d(blockId)) {
            return;
        }
        findRail();
    }

    protected boolean findRail()
    {
        if(world == null) {
            return false;
        }
        for(byte side = 2; side < 6; side++) {
            int i = GeneralTools.getXOnSide(x, side);
            int j = y;
            int k = GeneralTools.getZOnSide(z, side);
            TileEntity tile = world.getTileEntity(i, j, k);
            if(tile instanceof TileTrack) {
                ITrackInstance track = ((TileTrack)tile).getTrackInstance();
                if(track instanceof ITrackSwitch) {
                    facing = side;
                    reversed = ((ITrackSwitch)track).isReversed();
                    world.notify(x, y, z);
                    return true;
                }
            }
        }
        return false;
    }

    protected void switchRail(boolean switched)
    {
        if(world == null) {
            return;
        }
        TileEntity tile = GeneralTools.getBlockTileEntityFromSide(world, x, y, z, getFacing());
        if(tile instanceof TileTrack) {
            ITrackInstance track = ((TileTrack)tile).getTrackInstance();
            if(track instanceof ITrackSwitch) {
                ITrackSwitch switchTrack = (ITrackSwitch)track;
                if(switched != switchTrack.isSwitched()) {
                    if(RailcraftConfig.playSounds()) {
                        if(switched) {
                            world.makeSound(x, y, z, "tile.piston.in", 0.25f, world.random.nextFloat() * 0.25F + 0.7F);
                        } else {
                            world.makeSound(x, y, z, "tile.piston.out", 0.25f, world.random.nextFloat() * 0.25F + 0.7F);
                        }
                    }
                    switchTrack.setSwitched(switched);
                }
            }
        }
    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);

        nbttagcompound.setBoolean("Powered", isPowered());
        nbttagcompound.setBoolean("Reversed", isReversed());
        nbttagcompound.setByte("Facing", facing);
    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);

        powered = nbttagcompound.getBoolean("Powered");
        reversed = nbttagcompound.getBoolean("Reversed");
        facing = nbttagcompound.getByte("Facing");

        update = GeneralTools.getRand().nextInt();
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeByte(facing);
        data.writeBoolean(powered);
        data.writeBoolean(reversed);

    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        facing = data.readByte();
        powered = data.readBoolean();
        reversed = data.readBoolean();

        world.notify(x, y, z);
    }

    @Override
    public boolean canUpdate()
    {
        if(Railcraft.gameIsHost()) {
            return true;
        }
        return false;
    }

    public byte getFacing()
    {
        return facing;
    }

    public void setFacing(byte facing)
    {
        this.facing = facing;
    }

    public boolean isPowered()
    {
        return powered;
    }

    protected void setPowered(boolean p)
    {
        powered = p;
    }

    public boolean isReversed()
    {
        return reversed;
    }

    protected void setReversed(boolean r)
    {
        reversed = r;
    }
}
