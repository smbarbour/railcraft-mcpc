package railcraft.common.structures;

import railcraft.common.api.EnumSignalAspect;
import railcraft.common.api.ISignalController;
import railcraft.common.api.ISignalReceiver;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IBlockAccess;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import railcraft.Railcraft;
import railcraft.GuiHandler;
import railcraft.common.EnumGui;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.SignalTools;

public class TileStructureReceiverBox extends TileStructureBox implements ISignalReceiver
{

    private boolean isPairedWithController;
    private int controllerX = -1;
    private int controllerY = -1;
    private int controllerZ = -1;
    protected int update;
    private boolean prevBlinkState;
    private boolean[] powerOnAspects = new boolean[EnumSignalAspect.values().length];

    public TileStructureReceiverBox()
    {
        super(EnumStructure.BOX_RECEIVER);
        powerOnAspects[EnumSignalAspect.GREEN.getId()] = true;
        powerOnAspects[EnumSignalAspect.YELLOW.getId()] = false;
    }

    @Override
    public boolean blockActivated(World world, int i, int j, int k, EntityHuman player)
    {
        GuiHandler.openGui(EnumGui.BOX_RECEIVER, player, world, x, y, z);
        return true;
    }

    @Override
    public void q_()
    {
        update++;
        if(update % 4 == 0) {
            if(Railcraft.gameIsHost()) {
                EnumSignalAspect prevAspect = aspect;
                if(isPairedWithController()) {
                    ISignalController controller = getController();
                    if(controller != null) {
                        aspect = controller.getSignalAspect();
                    } else {
                        aspect = EnumSignalAspect.BLINK_YELLOW;
                    }
                } else {
                    aspect = EnumSignalAspect.BLINK_RED;
                }
                if(prevAspect != aspect) {
                    world.notify(x, y, z);
                    world.applyPhysics(x, y, z, getBlock().id);
                    world.applyPhysics(x + 1, y, z, getBlock().id);
                    world.applyPhysics(x - 1, y, z, getBlock().id);
                    world.applyPhysics(x, y, z + 1, getBlock().id);
                    world.applyPhysics(x, y, z - 1, getBlock().id);
                    world.applyPhysics(x, y - 1, z, getBlock().id);
                    world.applyPhysics(x, y + 1, z, getBlock().id);
                }
            }
            if(Railcraft.gameIsClient() && aspect.isBlinkAspect() && prevBlinkState != EnumSignalAspect.isBlinkOff()) {
                prevBlinkState = EnumSignalAspect.isBlinkOff();
                world.notify(x, y, z);
            }

        }
        if(Railcraft.gameIsHost() && update % 40 == 0) {
            ISignalController controller = getController();
            if(controller != null && !(controller.isPairedWithReceiver() && controller.getReceiver() == this)) {
                clearPairedController();
            }
        }
    }

    @Override
    public boolean isPoweringTo(IBlockAccess world, int i, int j, int k, int side)
    {
        TileEntity tile = GeneralTools.getBlockTileEntityFromSide(this.world, i, j, k, GeneralTools.getOppositeSide(side));
        if(tile instanceof IStructureBox) {
            return false;
        }
        return powerOnAspects[aspect.getId()];
    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);

        if(nbttagcompound.hasKey("PowerOnAspect")) {
            byte[] array = nbttagcompound.getByteArray("PowerOnAspect");
            for(int i = 0; i < powerOnAspects.length; i++) {
                powerOnAspects[i] = array[i] == 1;
            }
        }

        isPairedWithController = nbttagcompound.getBoolean("ControllerPaired");
        controllerX = nbttagcompound.getInt("ControllerX");
        controllerY = nbttagcompound.getInt("ControllerY");
        controllerZ = nbttagcompound.getInt("ControllerZ");

        update = GeneralTools.getRand().nextInt();
    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);

        byte[] array = new byte[powerOnAspects.length];
        for(int i = 0; i < powerOnAspects.length; i++) {
            array[i] = (byte)(powerOnAspects[i] ? 1 : 0);
        }
        nbttagcompound.setByteArray("PowerOnAspect", array);

        nbttagcompound.setBoolean("ControllerPaired", isPairedWithController());
        nbttagcompound.setInt("ControllerX", getControllerX());
        nbttagcompound.setInt("ControllerY", getControllerY());
        nbttagcompound.setInt("ControllerZ", getControllerZ());
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        byte bits = 0;
        for(int i = 0; i < powerOnAspects.length; i++) {
            bits |= (powerOnAspects[i] ? 1 : 0) << i;
        }
        data.writeByte(bits);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        byte bits = data.readByte();
        for(int bit = 0; bit < powerOnAspects.length; bit++) {
            powerOnAspects[bit] = ((bits >> bit) & 1) == 1;
        }
    }

    @Override
    public boolean attemptToPairWithController(ISignalController controller)
    {
        if(controller == null) {
            return false;
        }
        controllerX = controller.getX();
        controllerY = controller.getY();
        controllerZ = controller.getZ();
        if(controller.equals(getController())) {
            boolean success = true;
            if(!this.equals(controller.getReceiver())) {
                success = controller.attemptToPairWithReceiver(this);
            }
            isPairedWithController = success;
            return success;
        }
        return false;
    }

    @Override
    public void clearPairedController()
    {
        controllerX = -1;
        controllerY = -1;
        controllerZ = -1;
        isPairedWithController = false;
    }

    @Override
    public int getControllerX()
    {
        return controllerX;
    }

    @Override
    public int getControllerY()
    {
        return controllerY;
    }

    @Override
    public int getControllerZ()
    {
        return controllerZ;
    }

    @Override
    public boolean isPairedWithController()
    {
        return isPairedWithController;
    }

    @Override
    public ISignalController getController()
    {
        return SignalTools.getControllerFor(this);
    }

    @Override
    public int getDimension()
    {
        if(world == null) {
            return 0;
        }
        return world.worldProvider.dimension;
    }

    @Override
    public String getDescription()
    {
        return "Signal Receiver Box";
    }

    @Override
    public boolean connectToBoxAt(int i, int j, int k, int side)
    {
        TileEntity tile = world.getTileEntity(i, j, k);
        if(tile instanceof IStructureBox && tile instanceof ISignalController) {
            return true;
        }
        return false;
    }

    @Override
    public boolean doesActionOnAspect(EnumSignalAspect aspect)
    {
        return powerOnAspects[aspect.getId()];
    }

    @Override
    public void doActionOnAspect(EnumSignalAspect aspect, boolean trigger)
    {
        powerOnAspects[aspect.getId()] = trigger;
    }
}
