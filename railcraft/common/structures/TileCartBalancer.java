package railcraft.common.structures;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.IInventory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.Packet;
import buildcraft.api.ISpecialInventory;
import buildcraft.api.Orientations;
import forge.ISidedInventory;
import forge.MinecraftForge;
import railcraft.common.InventoryCopy;
import railcraft.common.api.InventoryTools;

public class TileCartBalancer extends TileStructure implements IInventory, ISidedInventory, ISpecialInventory
{

    private ItemStack crystal;

    public TileCartBalancer()
    {
        super(EnumStructure.CART_BALANCER);
    }

    @Override
    public Packet d()
    {
        return null;
    }

    @Override
    public int getSize()
    {
        return BalancerChannel.getChannelSize() + 1;
    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);
        if(nbttagcompound.hasKey("Crystal")) {
            NBTTagCompound item = nbttagcompound.getCompound("Crystal");
            crystal.c(item);
        }
    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);
        if(crystal != null) {
            NBTTagCompound item = new NBTTagCompound();
            crystal.save(item);
            nbttagcompound.set("Crystal", item);
        }
    }

    private int getChannelIndex()
    {
        if(crystal == null) {
            return -1;
        }
        return crystal.getData();
    }

    @Override
    public ItemStack getItem(int i)
    {
        if(i == 0) {
            return crystal;
        }
        i--;
        BalancerChannel channel = BalancerNetworkManager.getChannelContents(world, getChannelIndex());
        if(channel == null) {
            return null;
        }
        return channel.getItem(i);
    }

    @Override
    public ItemStack splitStack(int slot, int amount)
    {
        if(slot < 0) {
            return null;
        }
        if(slot == 0) {
            if(crystal != null) {
                if(crystal.count <= amount) {
                    ItemStack itemstack = crystal;
                    crystal = null;
                    update();
                    return itemstack;
                }
                ItemStack itemstack1 = crystal.a(amount);
                if(crystal.count == 0) {
                    crystal = null;
                }
                update();
                return itemstack1;
            } else {
                return null;
            }
        }
        slot--;
        BalancerChannel channel = BalancerNetworkManager.getChannelContents(world, getChannelIndex());
        if(channel == null) {
            return null;
        }
        return channel.splitStack(slot, amount);
    }

    @Override
    public void setItem(int i, ItemStack itemstack)
    {
        if(i == 0) {
            crystal = itemstack;
            update();
        } else {
            i--;
            BalancerChannel channel = BalancerNetworkManager.getChannelContents(world, getChannelIndex());
            if(channel == null) {
                return;
            }
            channel.setItem(i, itemstack);
        }
    }

    @Override
    public String getName()
    {
        return "Cart Balancer";
    }

    @Override
    public int getMaxStackSize()
    {
        return 64;
    }

    @Override
    public boolean a(EntityHuman entityplayer)
    {
        return true;
    }

    @Override
    public void f()
    {
    }

    @Override
    public void g()
    {
    }

    @Override
    public int getStartInventorySide(int side)
    {
        return 1;
    }

    @Override
    public int getSizeInventorySide(int side)
    {
        return BalancerChannel.getChannelSize();
    }

    @Override
    public boolean addItem(ItemStack stack, boolean doAdd, Orientations from)
    {
        if(stack == null){
            return false;
        }
        if(MinecraftForge.getCartClassForItem(stack) == null) {
            return false;
        }
        IInventory channel = BalancerNetworkManager.getChannelContents(world, getChannelIndex());
        if(channel == null) {
            return false;
        }
        if(!doAdd){
            channel = new InventoryCopy(channel);
            stack = stack.cloneItemStack();
        }
        int size = stack.count;
        ItemStack ret = InventoryTools.moveItemStack(stack, channel);
        if(ret == null || ret.count != size) {
            return true;
        }
        return false;
    }

    @Override
    public ItemStack extractItem(boolean doRemove, Orientations from)
    {
        BalancerChannel channel = BalancerNetworkManager.getChannelContents(world, getChannelIndex());
        if(channel == null) {
            return null;
        }
        for(int i = channel.getSize() - 1; i > 0; i--) {
            ItemStack stack = channel.getItem(i);
            if(stack == null) {
                continue;
            }
            if(doRemove) {
                return channel.splitStack(i, 1);
            }
            stack = stack.cloneItemStack();
            stack.count = 1;
            return stack;
        }
        return null;
    }

    @Override
    public ItemStack splitWithoutUpdate(int var1)
    {
       return null;
    }

	@Override
	public ItemStack[] getContents() {
		return null;
	}

	@Override
	public void setMaxStackSize(int arg0) {
	}
}
