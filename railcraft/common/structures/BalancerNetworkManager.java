package railcraft.common.structures;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import net.minecraft.server.ItemStack;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.World;
import railcraft.Railcraft;
import railcraft.common.RailcraftSaveData;

public class BalancerNetworkManager
{

    private static final Map<Integer, BalancerChannel> channelMap = new HashMap<Integer, BalancerChannel>();
    private static File saveDir;
    private static NBTTagCompound data;
    private static boolean hasChanged;

    public static BalancerChannel getChannelContents(World world, int channel)
    {
        if(world == null || channel < 0) {
            return  null;
        }
        File dir = Railcraft.getWorldSaveDir(world);
        if(!dir.equals(saveDir)) {
            closeChannelData();
            saveDir = dir;
        }
        if(!channelMap.containsKey(channel)) {
            loadChannelData(world, channel);
        }
        if(!channelMap.containsKey(channel)) {
            return null;
        }
        return channelMap.get(channel);
    }

    public static void markHasChanged()
    {
        hasChanged = true;
    }

    private static void loadChannelData(World world, int channel)
    {
        data = RailcraftSaveData.getData(world);
        if(data == null) {
            return;
        }
        NBTTagCompound balancerData = data.getCompound("CartBalancer");
        String channelName = new StringBuilder("Channel").append(channel).toString();
        NBTTagList channelData = balancerData.getList(channelName);
        BalancerChannel contents = new BalancerChannel(channel);
        for(int i = 0; i < channelData.size(); i++) {
            NBTTagCompound nbttagcompound1 = (NBTTagCompound)channelData.get(i);
            int j = nbttagcompound1.getByte("Slot") & 255;
            if(j >= 0 && j < contents.getSize()) {
                contents.setItem(j, ItemStack.a(nbttagcompound1));
            }
        }
        channelMap.put(channel, contents);
    }

    private static void closeChannelData()
    {
        System.out.println("Closing Channel Data");
        saveChannelData();
        channelMap.clear();
        saveDir = null;
        data = null;
        hasChanged = false;
    }

    public static void flushChannelData()
    {
        System.out.println("Flushing Channel Data");
        if(hasChanged) {
            saveChannelData();
            RailcraftSaveData.saveData();
        }
    }

    public static void saveChannelData()
    {
        if(data == null || !hasChanged || channelMap.isEmpty()) {
            return;
        }

        NBTTagCompound balancerData = data.getCompound("CartBalancer");
        data.setCompound("CartBalancer", balancerData);

        for(Map.Entry<Integer, BalancerChannel> channel : channelMap.entrySet()) {
            NBTTagList channelData = new NBTTagList();
            for(int i = 0; i < channel.getValue().getSize(); i++) {
                ItemStack stack = channel.getValue().getItem(i);
                if(stack != null) {
                    NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                    nbttagcompound1.setByte("Slot", (byte)i);
                    stack.save(nbttagcompound1);
                    channelData.add(nbttagcompound1);
                }
            }

            String channelName = new StringBuilder("Channel").append(channel.getKey()).toString();
            balancerData.set(channelName, channelData);
        }

        RailcraftSaveData.markHasChanged();
        hasChanged = false;
    }
}
