
package railcraft.common.structures;


public interface ISignalTile
{
    public int getLightValue();
}
