package railcraft.common.structures;

import railcraft.common.api.EnumSignalAspect;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.IBlockAccess;
import net.minecraft.server.World;

public abstract class TileStructureBox extends TileStructure implements IStructureBox
{

    private static final float BOUND = 0.1f;
    protected EnumSignalAspect aspect = EnumSignalAspect.BLINK_RED;

    protected TileStructureBox(EnumStructure s)
    {
        super(s);
    }

    @Override
    public void setBlockBoundsBasedOnState(IBlockAccess world, int i, int j, int k)
    {
        getBlock().a(BOUND, 0, BOUND, 1 - BOUND, 1 - BOUND / 2, 1 - BOUND);
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return AxisAlignedBB.b(i + BOUND, j, k + BOUND, i + 1 - BOUND, j + 1 - BOUND / 2, k + 1 - BOUND);
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return AxisAlignedBB.b(i + BOUND, j, k + BOUND, i + 1 - BOUND, j + 1 - BOUND / 2, k + 1 - BOUND);
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public EnumSignalAspect getBoxSignalAspect()
    {
        return aspect;
    }

    @Override
    public boolean isBlockSolidOnSide(World world, int i, int j, int k, int side)
    {
        return false;
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeByte(aspect.getId());
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        aspect = EnumSignalAspect.fromId(data.readByte());

        world.notify(x, y, z);
    }
}
