package railcraft.common.structures;


public interface IStructurePost
{
    public boolean connectToBlockAt(int i, int j, int k, int side);
}
