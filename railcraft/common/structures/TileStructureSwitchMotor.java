package railcraft.common.structures;

import railcraft.common.api.EnumSignalAspect;
import railcraft.common.api.ISignalController;
import railcraft.common.api.ISignalReceiver;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.BlockMinecartTrack;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.ItemStack;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import railcraft.Railcraft;
import railcraft.GuiHandler;
import railcraft.common.EnumGui;
import railcraft.common.api.GeneralTools;
import railcraft.common.api.ICrowbar;
import railcraft.common.api.SignalTools;

public class TileStructureSwitchMotor extends TileStructureSwitchLever implements ISignalReceiver
{

    private boolean isPairedWithController;
    private int controllerX = -1;
    private int controllerY = -1;
    private int controllerZ = -1;
    private boolean[] switchOnAspects = new boolean[EnumSignalAspect.values().length];

    public TileStructureSwitchMotor()
    {
        super(EnumStructure.SWITCH_MOTOR);
        switchOnAspects[EnumSignalAspect.RED.getId()] = true;
    }

    @Override
    public boolean blockActivated(World world, int i, int j, int k, EntityHuman player)
    {
        ItemStack current = player.U();
        if(current != null && current.getItem() instanceof ICrowbar) {
            GuiHandler.openGui(EnumGui.SWITCH_MOTOR, player, world, x, y, z);
            if(current.d()) {
                current.damage(1, player);
            }
            return true;
        }
        return false;
    }

    @Override
    public void onBlockPlaced(World world, int i, int j, int k, int l)
    {
        findRail();
    }

    @Override
    public void q_()
    {
        update++;
        if(update % 4 == 0) {
            if(getFacing() < 2 || getFacing() > 5) {
                findRail();
            }
            if(powerHasChanged(world, x, y, z)) {
                findRail();
                switchRail(isPowered());
                world.notify(x, y, z);
            }
        }
        if(update % 40 == 0) {
            ISignalController controller = getController();
            if(controller != null && !(controller.isPairedWithReceiver() && controller.getReceiver() == this)) {
                clearPairedController();
            }
        }
    }

    @Override
    public void onNeighborBlockChange(World world, int i, int j, int k, int blockId)
    {
        if(Railcraft.gameIsNotHost()) {
            return;
        }
        if(BlockMinecartTrack.d(blockId)) {
            return;
        }
        findRail();
        if(powerHasChanged(world, i, j, k)) {
            switchRail(isPowered());
            world.notify(i, j, k);
        }
    }

    private boolean powerHasChanged(World world, int i, int j, int k)
    {
        boolean p = world.isBlockPowered(i, j, k) || world.isBlockIndirectlyPowered(i, j, k);
        if(!p && isPairedWithController()) {
            ISignalController controller = getController();
            if(controller != null && switchOnAspects[controller.getSignalAspect().getId()]) {
                p = true;
            }
        }
        if(isPowered() ^ p) {
            setPowered(p);
            world.notify(i, j, k);
            return true;
        }
        return false;
    }

    @Override
    public void b(NBTTagCompound nbttagcompound)
    {
        super.b(nbttagcompound);

        byte[] array = new byte[switchOnAspects.length];
        for(int i = 0; i < switchOnAspects.length; i++) {
            array[i] = (byte)(switchOnAspects[i] ? 1 : 0);
        }
        nbttagcompound.setByteArray("PowerOnAspect", array);

        nbttagcompound.setBoolean("ControllerPaired", isPairedWithController());
        nbttagcompound.setInt("ControllerX", getControllerX());
        nbttagcompound.setInt("ControllerY", getControllerY());
        nbttagcompound.setInt("ControllerZ", getControllerZ());
    }

    @Override
    public void a(NBTTagCompound nbttagcompound)
    {
        super.a(nbttagcompound);

        if(nbttagcompound.hasKey("PowerOnAspect")) {
            byte[] array = nbttagcompound.getByteArray("PowerOnAspect");
            for(int i = 0; i < switchOnAspects.length; i++) {
                switchOnAspects[i] = array[i] == 1;
            }
        }

        isPairedWithController = nbttagcompound.getBoolean("ControllerPaired");
        controllerX = nbttagcompound.getInt("ControllerX");
        controllerY = nbttagcompound.getInt("ControllerY");
        controllerZ = nbttagcompound.getInt("ControllerZ");

        update = GeneralTools.getRand().nextInt();
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        byte aspectBits = 0;
        for(int i = 0; i < switchOnAspects.length; i++) {
            aspectBits |= (switchOnAspects[i] ? 1 : 0) << i;
        }
        data.writeByte(aspectBits);
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        byte bits = data.readByte();
        for(int bit = 0; bit < switchOnAspects.length; bit++) {
            switchOnAspects[bit] = ((bits >> bit) & 1) == 1;
        }

        world.notify(x, y, z);
    }

    @Override
    public boolean canUpdate()
    {
        if(Railcraft.gameIsHost()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean attemptToPairWithController(ISignalController controller)
    {
        if(controller == null) {
            return false;
        }
        controllerX = controller.getX();
        controllerY = controller.getY();
        controllerZ = controller.getZ();
        if(controller.equals(getController())) {
            boolean success = true;
            if(!this.equals(controller.getReceiver())) {
                success = controller.attemptToPairWithReceiver(this);
            }
            isPairedWithController = success;
            return success;
        }
        return false;
    }

    @Override
    public void clearPairedController()
    {
        controllerX = -1;
        controllerY = -1;
        controllerZ = -1;
        isPairedWithController = false;
    }

    @Override
    public int getControllerX()
    {
        return controllerX;
    }

    @Override
    public int getControllerY()
    {
        return controllerY;
    }

    @Override
    public int getControllerZ()
    {
        return controllerZ;
    }

    @Override
    public boolean isPairedWithController()
    {
        return isPairedWithController;
    }

    @Override
    public ISignalController getController()
    {
        return SignalTools.getControllerFor(this);
    }

    @Override
    public int getDimension()
    {
        if(world == null) {
            return 0;
        }
        return world.worldProvider.dimension;
    }

    @Override
    public boolean doesActionOnAspect(EnumSignalAspect aspect)
    {
        return switchOnAspects[aspect.getId()];
    }

    @Override
    public void doActionOnAspect(EnumSignalAspect aspect, boolean trigger)
    {
        switchOnAspects[aspect.getId()] = trigger;
    }

    @Override
    public String getDescription()
    {
        return "Switch Motor";
    }
}
