package railcraft.common.structures;

import java.util.ArrayList;
import net.minecraft.server.*;
import forge.*;
import railcraft.Railcraft;
import railcraft.common.EnumColor;
import railcraft.common.RailcraftBlocks;
import railcraft.common.api.BukkitHandlers;

public class ItemMetalPost extends Item implements ITextureProvider
{

    private static ItemMetalPost instance;

    public static ItemMetalPost getInstance()
    {
        return instance;
    }

    public ItemMetalPost(int id)
    {
        super(id);
        d(EnumStructure.METAL_POST.getBlockTexture());
        setMaxDurability(0);
        a(true);
        a("metalPost");
        Railcraft.registerMetalPostRenderer(id);
        instance = this;
    }

    @Override
    public String getTextureFile()
    {
        return "/railcraft/textures/railcraft.png";
    }

    public int getIconFromDamage(int i)
    {
        if(i == 0) {
            return EnumStructure.METAL_POST.getPrimaryTexture();
        }
        return EnumStructure.METAL_POST.getSecondaryTexture() + i - 1;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void addCreativeItems(ArrayList itemList)
    {
    }

    @Override
    public boolean interactWith(ItemStack itemstack, EntityHuman entityplayer, World world, int i, int j, int k, int side)
    {
        int id = world.getTypeId(i, j, k);
        if(id == Block.SNOW.id) {
            side = 0;
        } else if(id != Block.VINE.id) {
            if(side == 0) {
                j--;
            }
            if(side == 1) {
                j++;
            }
            if(side == 2) {
                k--;
            }
            if(side == 3) {
                k++;
            }
            if(side == 4) {
                i--;
            }
            if(side == 5) {
                i++;
            }
        }
        if(itemstack.count == 0) {
            return false;
        }
        if(j == 128 - 1 && RailcraftBlocks.getBlockStructure().material.isSolid()) {
            return false;
        }
        EnumStructure type = EnumStructure.METAL_POST;
        if(world.mayPlace(RailcraftBlocks.getBlockStructure().id, i, j, k, false, side) && (!type.needsSupport() || world.isBlockSolidOnSide(i, j - 1, k, 1))) {
            Block post = RailcraftBlocks.getBlockStructure();
            ItemStack tempStack = new ItemStack(post, 1);
            boolean placed = BukkitHandlers.placeBlockWithEvent(world.getServer(), world, tempStack, i, j, k, i, j, k, false, entityplayer); //world.setTypeIdAndData(i, j, k, post.id, type.getId());
            if(placed) {
                if(world.getTypeId(i, j, k) == post.id) {
                    TileEntity tile = world.getTileEntity(i, j, k);
                    if(tile instanceof TileStructureMetalPost) {
                        ((TileStructureMetalPost)tile).setColor(itemstack.getData());
                    }
                    post.postPlace(world, i, j, k, side);
                    post.postPlace(world, i, j, k, entityplayer);
                }
                world.makeSound((float)i + 0.5F, (float)j + 0.5F, (float)k + 0.5F, post.stepSound.getName(), (post.stepSound.getVolume1() + 1.0F) / 2.0F, post.stepSound.getVolume2() * 0.8F);
                itemstack.count--;
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String a(ItemStack itemstack)
    {
        int damage = itemstack.getData();
        return getName() + "." + EnumColor.fromId(damage).getBasicTag();
    }
}
