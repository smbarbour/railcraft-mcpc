package railcraft.common.structures;

import railcraft.common.api.EnumSignalAspect;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.*;
import railcraft.common.api.GeneralTools;

public abstract class TileStructureSignal extends TileStructure implements ISignalTile, IStructurePost
{

    private static final float SIZE = 0.15f;
    private byte facing;
    protected EnumSignalAspect aspect = EnumSignalAspect.BLINK_RED;
    protected int update = 0;

    protected TileStructureSignal(EnumStructure s)
    {
        super(s);
        update = GeneralTools.getRand().nextInt();
    }

    @Override
    public void setBlockBoundsBasedOnState(IBlockAccess world, int i, int j, int k)
    {
        getBlock().a(0.15f, 0.35f, 0.15f, 0.85f, 1f, 0.85f);
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return AxisAlignedBB.b(i + 0.15f, j + 0.35f, k + 0.15f, i + 0.85f, j + 1f, k + 0.85f);
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return AxisAlignedBB.b(i + SIZE, j + 0.35f, k + SIZE, i + 1 - SIZE, j + 1, k + 1 - SIZE);
    }

    @Override
    public boolean connectToBlockAt(int i, int j, int k, int side)
    {
        if(world.isBlockSolidOnSide(i, j, k, side)) {
            return true;
        }
        TileEntity tile = world.getTileEntity(i, j, k);
        return tile instanceof IStructurePost || (side == 1 && tile instanceof IStructureBox);
    }

    @Override
    public boolean canUpdate()
    {
        return true;
    }

    @Override
    public void q_()
    {
    }

    public void setFacing(byte facing)
    {
        this.facing = facing;
    }

    public byte getFacing()
    {
        return facing;
    }

    public EnumSignalAspect getSignalAspect()
    {
        return aspect;
    }

    public int getDimension()
    {
        if(world == null) {
            return 0;
        }
        return world.worldProvider.dimension;
    }

    @Override
    public void onBlockPlacedBy(World world, int i, int j, int k, EntityLiving entityliving)
    {
        int dir = MathHelper.floor((double)((entityliving.yaw * 4.0F) / 360.0F) + 0.5) & 3;
        if(dir == 0) {
            facing = 2;
        }
        if(dir == 1) {
            facing = 5;
        }
        if(dir == 2) {
            facing = 3;
        }
        if(dir == 3) {
            facing = 4;
        }
    }

    @Override
    public void b(NBTTagCompound data)
    {
        super.b(data);
        data.setByte("Facing", facing);
    }

    @Override
    public void a(NBTTagCompound data)
    {
        super.a(data);
        facing = data.getByte("Facing");

        update = GeneralTools.getRand().nextInt();
    }

    @Override
    public void writePacketData(DataOutputStream data) throws IOException
    {
        super.writePacketData(data);

        data.writeByte(facing);
        data.writeByte(aspect.getId());
    }

    @Override
    public void readPacketData(DataInputStream data) throws IOException
    {
        super.readPacketData(data);

        facing = data.readByte();
        aspect = EnumSignalAspect.fromId(data.readByte());

        world.notify(x, y, z);
    }

    @Override
    public int getLightValue()
    {
        if(aspect.isBlinkAspect() && EnumSignalAspect.isBlinkOff()) {
            return 0;
        }
        return 5;
    }
}
