package railcraft.common;

import net.minecraft.server.ItemStack;
import forge.IFuelHandler;
import railcraft.common.api.InventoryTools;

/**
 *
 * @author CovertJaguar <railcraft.wikispaces.com>
 */
public class FuelHandler implements IFuelHandler
{

    @Override
    public int getItemBurnTime(ItemStack stack)
    {
        if(InventoryTools.isItemEqual(stack, RailcraftToolItems.getCoalCoke())) {
            return 3200;
        }
        return 0;
    }
}
